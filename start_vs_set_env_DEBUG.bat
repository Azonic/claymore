set "OSG_BIN_DIR=%OSG_PATH%\bin"

set "THIRD_PARTY_BIN_DIR=%OSG_THIRD_PARTY_DIR%\bin"

set "SIXENSE_BIN_DIR=%SIXENSE_SDK_PATH%\bin"

set "OPENMESH_BIN_DIR=%OPENMESH_PATH%\bin"

set "OPENVR_BIN_DIR=%OPENVR_PATH%\bin\win64"

set "QT_BIN_DIR=%QT_PATH%\bin"

set "VS15_DIR=%VS140COMNTOOLS%\..\"

set "VS15_EXE=IDE\devenv.exe"

echo "Using 64-bit environment"

::set "VCVARS32_BAT=VC\bin\vcvars32.bat"

::echo "Setting environment for 32-bit development"

::call "%VS10_DIR%/%VCVARS32_BAT%"

echo "Setting PATH variable"

set "PATH=%OSG_BIN_DIR%;%THIRD_PARTY_BIN_DIR%;%SIXENSE_BIN_DIR%;%OPENMESH_BIN_DIR%;%OPENVR_BIN_DIR%;%QT_BIN_DIR%"

echo "PATH: %PATH%"

echo "Calling Visual Studio 2015"

call "%VS15_DIR%/%VS15_EXE%" 
