// #version 400
// layout( location = 0 ) out vec4 FragColor;

// // in vec3 Position;
// // in vec3 Normal;
// in vec2 UV;
// uniform sampler2D textureSampler;

// void main() {
	// vec4 texColor = texture( textureSampler, UV );
	// FragColor = texColor;
// }

//Backup050816
// varying vec2 UV;

// uniform sampler2D textureSampler;
// uniform vec2 focalLength;
// uniform vec2 opticalCenter;
// uniform vec4 distortionCoefficients;

// void main(void)
// {
	// //vec2 focalLength = vec2(624.340, 624.635);
    // //vec2 opticalCenter = vec2(724.177, 548.907);
    // //vec4 distortionCoefficients = vec4(-0.234922, 0.055024, -0.000967, -0.000489);

    // const vec2 imageSize = vec2(1920.0, 1440.0);

	// vec2 focalLengthUV = focalLength / imageSize;
    // vec2 opticalCenterUV = opticalCenter / imageSize;

    // vec2 shiftedUVCoordinates = (UV - opticalCenterUV);

    // vec2 lensCoordinates = shiftedUVCoordinates / focalLengthUV;

    // float radiusSquared = dot(lensCoordinates, lensCoordinates);
    // float radiusQuadrupled = radiusSquared * radiusSquared;

    // float coefficientTerm = distortionCoefficients.x * radiusSquared + distortionCoefficients.y * radiusQuadrupled;

    // vec2 distortedUV = ((lensCoordinates + lensCoordinates * (coefficientTerm))) * focalLengthUV;

    // vec2 resultUV = (distortedUV + opticalCenterUV);
	
	// gl_FragColor = texture2D(textureSampler, resultUV);
// }

//Last working block from 18092016
varying vec2 UV;

uniform sampler2D textureSampler;
// uniform vec2 focalLength;
// uniform vec2 opticalCenter;
// uniform vec4 distortionCoefficients;

void main(void)
{
    // const vec2 imageSize = vec2(1920.0, 1444.0);
	
	// vec2 lensCoordinates = (UV - opticalCenter) / focalLength;

    // float radiusSquared = sqrt(dot(lensCoordinates, lensCoordinates));
    // float radiusQuadrupled = radiusSquared * radiusSquared;

    // float coefficientTerm = distortionCoefficients.x * radiusSquared + distortionCoefficients.y * radiusQuadrupled;

    // vec2 distorted = ((lensCoordinates + lensCoordinates * (coefficientTerm))) * focalLength;

    // vec2 result = (distorted + opticalCenter);
	
	gl_FragColor = texture2D(textureSampler, UV);
}

// varying vec2 UV;

// uniform sampler2D textureSampler;
// uniform vec2 focalLength;
// uniform vec2 opticalCenter;
// uniform vec4 distortionCoefficients;

// void main(void)
// {
    // const vec2 imageSize = vec2(1920.0, 1444.0);
	
	// vec2 xy = UV;
	// float r = dot(xy, xy);
	// float r2 = r * r;
	// float r4 = r2 * r2;
	// float r6 = r4 * r2;
	
	// vec2 result;

	// result.x = xy.x * (distortionCoefficients.x * r2 + distortionCoefficients.y * r4 + distortionCoefficients.z * r6) + 2.0 * xy.x * xy.y + (r2 + 2.0 * xy.x * xy.x); //Taylor expansion 
	// result.y = xy.y * (distortionCoefficients.x * r2 + distortionCoefficients.y * r4 + distortionCoefficients.z * r6) + (r2 + 2 * xy.y * xy.y) + 2.0 * xy.x * xy.y; //Taylor expansion 
	
	// vec2 result2;
	
	// result2.x = focalLength.x * result.x + opticalCenter.x;
	// result2.y = focalLength.y * result.y + opticalCenter.y;

	
	// gl_FragColor = texture2D(textureSampler, result2);
// }


