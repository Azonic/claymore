#extension GL_EXT_gpu_shader4 : require
//flat varying vec3 N;
//varying vec3 v;

uniform bool useWireframe;

varying in vec3 norm;
varying in vec3 vert;
varying in vec4 vertexColor;

varying in vec3 baryCoords;

varying in float fragmentDepth;

void main(void)
{
	vec3 L = normalize(gl_LightSource[0].position.xyz - vert);  
	vec4 Idiff; 
	if (gl_FrontFacing) // is the fragment part of a front face?
    {
	   Idiff = max(dot(norm,L), 0.0); 
    }
	else
	{
		Idiff = max(dot(-norm,L), 0.0); 
	}
   Idiff = clamp(Idiff, 0.2, 1.0); 
		
	float finalFragDepth = 0.5 * fragmentDepth + 0.5;
	
	if(finalFragDepth > 0.3)
		finalFragDepth = 0.3;
		
	if(any(lessThan(baryCoords,vec3((1.0 - finalFragDepth) * 0.02))))
	{
		gl_FragColor = vertexColor;
		gl_FragColor.a = 1.0;
		if (!gl_FrontFacing)
			gl_FragColor.a = (1.0 - 0.6 * (0.5 * fragmentDepth + 0.5));

	}
	else{
		discard;
	}	
}

// void main(){
        // vec3 normal = normalize(N); 

        // // voila, we can use our flat normal! This will colour our fragment with the current normal for debug purposes.
        // gl_FragColor = vec4((normal + vec3(1.0, 1.0, 1.0)) / 2.0,1.0);
// }