uniform sampler2D projectionMap0;

varying vec4 projCoord0;


void main()
{
    vec4 dividedCoord0 = projCoord0 / projCoord0.w;
    vec4 color0 =  texture2D(projectionMap0, dividedCoord0.st);
	
	//This is for avoiding "mirroring" on the opposite site of the sphere
	//if(projCoord0.q < 0.0)
	//	color0 = vec4(0.0, 0.0, 0.0, 0.0);
	
	//color0 = color0 + vec4(0.00001, 0.00001,0.00001,0.00001);
	//color1 = color1 + vec4(0.00001, 0.00001,0.00001,0.00001);
	//color2 = color2 + vec4(0.00001, 0.00001,0.00001,0.00001);
	//color3 = color3 + vec4(0.00001, 0.00001,0.00001,0.00001);
	//color4 = color4 + vec4(0.00001, 0.00001,0.00001,0.00001);
	
	
	// if((color0.x + color0.y + color0.z) > 0.0)
		// color0.w = 1.0;
	// else 
		// color0.w = 0.0;
	
	
	vec4 finalColor = color0;
	// if (finalColor.w > 1.0)
	// {
		// finalColor = finalColor / finalColor.w;
	// }

	gl_FragColor =   finalColor;
}