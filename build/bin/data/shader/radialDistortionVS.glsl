// #version 400

// layout (location = 0) in vec3 VertexPosition;
// // layout (location = 1) in vec3 VertexNormal;
// layout (location = 1) in vec2 VertexTexCoord;

// // out vec3 Position;
// // out vec3 Normal;
// out vec2 UV;

// // uniform mat4 ModelViewMatrix;
// // uniform mat3 NormalMatrix;
// // uniform mat4 ProjectionMatrix;
// // uniform mat4 MVP;

// void main()
// {
	// UV = VertexTexCoord;
	// // Normal = normalize( NormalMatrix * VertexNormal);
	// // Position = vec3( ModelViewMatrix *	vec4(VertexPosition, 1.0) );
	// // gl_Position = MVP * vec4(VertexPosition, 1.0);
	// gl_Position = vec4(VertexPosition, 1.0);
// }

varying vec2 UV;
void main(void){
	gl_Position = gl_ModelViewProjectionMatrix * gl_Vertex;
	UV = gl_MultiTexCoord0.xy;
}