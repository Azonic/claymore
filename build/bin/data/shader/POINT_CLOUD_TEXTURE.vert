varying vec2 UV;
varying float depth;

void main(void){
	UV = gl_MultiTexCoord0;
	gl_Position = gl_ModelViewProjectionMatrix * gl_Vertex;
	depth = 0.1 * gl_Vertex.z;
}