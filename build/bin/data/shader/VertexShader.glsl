varying vec4 projCoord0;

void main()
{
        projCoord0 = gl_TextureMatrix[0] * gl_Vertex;
		
        gl_Position = gl_ProjectionMatrix * gl_ModelViewMatrix * gl_Vertex;
        gl_FrontColor = gl_Color;
}