varying vec2 UV;
varying float depth;
uniform sampler2D textureSampler;

void main(void)
{
	//gl_FragColor = tex2D(textureSampler, UV);
	//gl_FragColor = vec4(1.0f, 0.0f, 0.0f, 1.0f);
	gl_FragColor = vec4(depth, depth, depth, 1.0f);
}
