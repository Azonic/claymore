varying vec4 projCoord0;
varying vec4 projCoord1;
varying vec4 projCoord2;
varying vec4 projCoord3;
varying vec4 projCoord4;
varying vec4 projCoord5;

void main()
{
        projCoord0 = gl_TextureMatrix[0] * gl_Vertex;
		projCoord1 = gl_TextureMatrix[1] * gl_Vertex;
		projCoord2 = gl_TextureMatrix[2] * gl_Vertex;
		projCoord3 = gl_TextureMatrix[3] * gl_Vertex;
		projCoord4 = gl_TextureMatrix[4] * gl_Vertex;
		projCoord5 = gl_TextureMatrix[5] * gl_Vertex;
		
        gl_Position = gl_ProjectionMatrix * gl_ModelViewMatrix * gl_Vertex;
        gl_FrontColor = gl_Color;
}