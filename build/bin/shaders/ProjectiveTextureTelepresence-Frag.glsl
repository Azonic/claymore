uniform sampler2D projectionMap0;
uniform sampler2D projectionMap1;
uniform sampler2D projectionMap2;
uniform sampler2D projectionMap3;
uniform sampler2D projectionMap4;
uniform sampler2D projectionMap5;

varying vec4 projCoord0;
varying vec4 projCoord1;
varying vec4 projCoord2;
varying vec4 projCoord3;
varying vec4 projCoord4;
varying vec4 projCoord5;

void main()
{
    vec4 dividedCoord0 = projCoord0 / projCoord0.w;
	vec4 dividedCoord1 = projCoord1 / projCoord1.w;
	vec4 dividedCoord2 = projCoord2 / projCoord2.w;
	vec4 dividedCoord3 = projCoord3 / projCoord3.w;
	vec4 dividedCoord4 = projCoord4 / projCoord4.w;
	vec4 dividedCoord5 = projCoord5 / projCoord5.w;
	
    vec4 color0 =  texture2D(projectionMap0, dividedCoord0.st);
	vec4 color1 =  texture2D(projectionMap1, dividedCoord1.st);
	vec4 color2 =  texture2D(projectionMap2, dividedCoord2.st);
	vec4 color3 =  texture2D(projectionMap3, dividedCoord3.st);
	vec4 color4 =  texture2D(projectionMap4, dividedCoord4.st);
	vec4 color5 =  texture2D(projectionMap5, dividedCoord5.st);
	

	
	//This is for avoiding "mirroring" on the opposite site of the sphere
	if(projCoord0.q < 0.0)
		color0 = vec4(0.0, 0.0, 0.0, 0.0);
	if(projCoord1.q < 0.0)
		color1 = vec4(0.0, 0.0, 0.0, 0.0);
	if(projCoord2.q < 0.0)
		color2 = vec4(0.0, 0.0, 0.0, 0.0);
	if(projCoord3.q < 0.0)
		color3 = vec4(0.0, 0.0, 0.0, 0.0);
	if(projCoord4.q < 0.0)
		color4 = vec4(0.0, 0.0, 0.0, 0.0);
	if(projCoord5.q < 0.0)
		color5 = vec4(0.0, 0.0, 0.0, 0.0);
	
	//color0 = color0 + vec4(0.00001, 0.00001,0.00001,0.00001);
	//color1 = color1 + vec4(0.00001, 0.00001,0.00001,0.00001);
	//color2 = color2 + vec4(0.00001, 0.00001,0.00001,0.00001);
	//color3 = color3 + vec4(0.00001, 0.00001,0.00001,0.00001);
	//color4 = color4 + vec4(0.00001, 0.00001,0.00001,0.00001);
	
	color0 = color0 * 1.9;
	color1 = color1 * 1.8;
	color3 = color3 * 1.7;
	color4 = color4 * 1.7;
	
	color5 = color5 + vec4(0.00001, 0.00001,0.00001,0.00001);
	color5 = color5 * 2.0;
	if((color0.x + color0.y + color0.z) > 0.0)
		color0.w = 1.0;
	else 
		color0.w = 0.0;
	
	if((color1.x + color1.y + color1.z) > 0.0)
		color1.w = 1.0;
	else 
		color1.w = 0.0;
	
	if((color2.x + color2.y + color2.z) > 0.0)
		color2.w = 1.0;
	else 
		color2.w = 0.0;
	
	if((color3.x + color3.y + color3.z) > 0.0)
		color3.w = 1.0;
	else 
		color3.w = 0.0;
	
	if((color4.x + color4.y + color4.z) > 0.0)
		color4.w = 1.0;
	else 
		color4.w = 0.0;
	
	if((color5.x + color5.y + color5.z) > 0.0)
		color5.w = 1.0;
	else 
		color5.w = 0.0;	
	
	vec4 finalColor = (color0 + color1 + color2 + color3 + color4 + color5);
	if (finalColor.w > 1.0)
	{
		finalColor = finalColor / finalColor.w;
	}

	gl_FragColor =   finalColor;
}