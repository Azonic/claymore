// ClayMore - Immersive Mesh Modelling --- Copyright (c) 2014-2017 Philipp Ladwig, Jannik Fiedler, Jan Beutgen
#pragma once

#include <osg/PositionAttitudeTransform>
#include "../cmOpenMeshBinding/OpenMeshGeometry.h"
#include "../cmGeometryManagement/GeometryVisualization.h"
#include "../cmUtil/ConfigReader.h"

#include "ConfigDllExportGeometryManagement.h"


class imGeometryManagement_DLL_import_export GeometryData : public osg::PositionAttitudeTransform
{
	// #### CONSTRUCTOR & DESTRUCTOR ###############
public:
	GeometryData(ConfigReader* configReaderIn, std::string name, osg::ref_ptr<osgShadow::ShadowMap> sm, osg::Matrix depthMVP);
	~GeometryData();

	// #### MEMBER FUNCTIONS ###############
	inline void setIsSelected(bool isSelected) { m_isSelected = isSelected; }
	inline bool getIsSelected() { return m_isSelected; }
	void setIsMarkedAsDeleted(bool isMarkedAsDeleted);
	bool getIsMarkedAsDeleted();

	inline void setIsWireframeActivated(bool valueIn) { m_isWireframeActivated = valueIn; }
	inline bool getIsWireframeActivated() { return m_isWireframeActivated; }

	inline void setShadowMap(osg::ref_ptr<osgShadow::ShadowMap> shadowMap);

	void initializeAndDisplay();
	inline osg::ref_ptr<osg::OpenMeshGeometry> getOpenMeshGeo() {
		//TODO3: Check, if this is the best way for checking the existence of m_openMeshGeom
		if (m_openMeshGeom) {
			return m_openMeshGeom;
		}
	};
	GeometryVisualization* getGeometryVisualization();
	GeometryVisualization* getFilthyLinesGeometryVisualization();
	GeometryVisualization* getFilthyPointsGeometryVisualization();

	void rebuildFilthyLinesGeometry();
	void rebuildFilthyPointsGeometry();

	void addFilthyEdge(int idxIn, int lastIdxIn);
	void removeFilthyEdges();

	void attachFilthyLinesGeode();
	void attachFilthyPointsGeode();

	void detachFilthyLinesGeode();
	void detachFilthyPointsGeode();

	void updateBoundingBox();

	inline osg::ref_ptr<osg::Geometry> getFilthyLinesGeometry() { return m_filthyLinesGeom; }
	inline osg::ref_ptr<osg::Geometry> getFilthyPointsGeometry() { return m_filthyPointsGeom; }

	// #### MEMBER VARIABLES ###############
private:
	ConfigReader* m_configReader;
	osg::ref_ptr<osg::Geode> m_geode;
	osg::ref_ptr<osg::Geode> m_filthyLinesGeode;
	osg::ref_ptr<osg::Geode> m_filthyPointsGeode;
	osg::ref_ptr<osg::OpenMeshGeometry> m_openMeshGeom;
	osg::ref_ptr<osg::Geometry> m_filthyLinesGeom;
	osg::ref_ptr<osg::Geometry> m_filthyPointsGeom;
	GeometryVisualization* m_geometryVisualization;
	GeometryVisualization* m_filthyLinesGeometryVisualization;
	GeometryVisualization* m_filthyPointsGeometryVisualization;

	osg::ref_ptr<osg::DrawElementsUInt> m_filthyLinesIndices;
	osg::ref_ptr<osg::DrawElementsUInt> m_filthyPointsIndices;
	osg::ref_ptr<osg::DrawElementsUInt> m_filthyEdgesIndcies;

	bool m_isSelected;
	bool m_isMarkedAsDeleted;
	bool m_isWireframeActivated;

	void createIndiciesForLinePrimitives();
	void createIndiciesForPointPrimitives();
};