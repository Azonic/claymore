// ClayMore - Immersive Mesh Modelling --- Copyright (c) 2014-2017 Philipp Ladwig, Jannik Fiedler, Jan Beutgen
#include "RenderTechniques.h"
#include "../cmUtil/ConfigReader.h"

#include "osg/LightModel"

#include <osg/Material>
#include "../cmUtil/ShaderUtils.h"

//FLAT_AND_WIRE_SHADING
void FlatAndWireShading::define_passes()
{
	//Shader Stuff
	osg::ref_ptr<osg::StateSet> ss = new osg::StateSet;

	//PL: LIGHT EXP 270215{
	ss->setMode(GL_LIGHT0, osg::StateAttribute::ON);

	//ss->setMode(GL_LIGHT1, osg::StateAttribute::ON);
	//ss->setMode(GL_LIGHT2, osg::StateAttribute::ON);
	//osg::LightModel* ltModel = new osg::LightModel;
	//NOTE: Set a two sides light model will decrease the framerate on geforce cards heavily!
	//There is now hardware acceleration on geforce cards for this.Only Quadros supports this
	//FIXME: Write a shader for a two sided light model
	//ltModel->setTwoSided(true);
	//ss->setAttribute(ltModel);
	//ss->setMode(GL_CULL_FACE, osg::StateAttribute::OFF);
	//PL: LIGHT EXP 270215


	osg::Program* flatWireProgram = new osg::Program;
	osg::Shader* flatWireVertex = new osg::Shader(osg::Shader::VERTEX);
	osg::Shader* flatWireFragment = new osg::Shader(osg::Shader::FRAGMENT);
	osg::Shader* flatWireGeometry = new osg::Shader(osg::Shader::GEOMETRY);
	flatWireProgram->addShader(flatWireFragment);
	flatWireProgram->addShader(flatWireVertex);
	flatWireProgram->addShader(flatWireGeometry);
	//flatWireProgram->setParameter(GL_PROVOKING_VERTEX, GL_LAST_VERTEX_CONVENTION);
	ShaderUtils::loadShaderSource(flatWireVertex, "./data/shader/FLAT_AND_WIRE_SHADING.vert");
	ShaderUtils::loadShaderSource(flatWireFragment, "./data/shader/FLAT_AND_WIRE_SHADING.frag");
	ShaderUtils::loadShaderSource(flatWireGeometry, "./data/shader/FLAT_AND_WIRE_SHADING.geom");

	flatWireProgram->setParameter(GL_GEOMETRY_VERTICES_OUT_EXT, 3);
	flatWireProgram->setParameter(GL_GEOMETRY_INPUT_TYPE_EXT, GL_TRIANGLES);
	flatWireProgram->setParameter(GL_GEOMETRY_OUTPUT_TYPE_EXT, GL_TRIANGLE_STRIP);

	//ss->setMode(GL_LIGHTING, osg::StateAttribute::OFF);
	//ss->setMode(GL_BLEND, osg::StateAttribute::ON);
	//ss->setRenderingHint(osg::StateSet::TRANSPARENT_BIN);
	//ss->setRenderBinDetails(18, "DepthSortedBin");

	ss->setAttributeAndModes(flatWireProgram, osg::StateAttribute::ON);

	addPass(ss.get());
}
//WIRE_SHADING
void WireShading::define_passes()
{
	osg::Program* wireProgram = new osg::Program;
	osg::Shader* wireVertex = new osg::Shader(osg::Shader::VERTEX);
	osg::Shader* wireFragment = new osg::Shader(osg::Shader::FRAGMENT);
	osg::Shader* wireGeometry = new osg::Shader(osg::Shader::GEOMETRY);
	wireProgram->addShader(wireFragment);
	wireProgram->addShader(wireVertex);
	wireProgram->addShader(wireGeometry);
	//wireProgram->setParameter(GL_PROVOKING_VERTEX, GL_LAST_VERTEX_CONVENTION);
	ShaderUtils::loadShaderSource(wireVertex, "./data/shader/WIRE_SHADING.vert");
	ShaderUtils::loadShaderSource(wireFragment, "./data/shader/WIRE_SHADING.frag");
	ShaderUtils::loadShaderSource(wireGeometry, "./data/shader/WIRE_SHADING.geom");

	wireProgram->setParameter(GL_GEOMETRY_VERTICES_OUT_EXT, 3);
	wireProgram->setParameter(GL_GEOMETRY_INPUT_TYPE_EXT, GL_TRIANGLES);
	wireProgram->setParameter(GL_GEOMETRY_OUTPUT_TYPE_EXT, GL_TRIANGLE_STRIP);

	osg::ref_ptr<osg::StateSet> ss = new osg::StateSet;
	//ss->setMode(GL_LIGHTING, osg::StateAttribute::OFF);
	//ss->setMode(GL_BLEND, osg::StateAttribute::ON);
	//ss->setRenderingHint(osg::StateSet::TRANSPARENT_BIN);
	//ss->setRenderBinDetails(18, "DepthSortedBin");

	ss->setAttributeAndModes(wireProgram, osg::StateAttribute::ON);

	addPass(ss.get());
}

//FLAT_SHADING
void FlatShading::define_passes()
{
	osg::Program* flatProgram = new osg::Program;
	osg::Shader* flatVertex = new osg::Shader(osg::Shader::VERTEX);
	osg::Shader* flatFragment = new osg::Shader(osg::Shader::FRAGMENT);
	osg::Shader* flatGeometry = new osg::Shader(osg::Shader::GEOMETRY);
	flatProgram->addShader(flatFragment);
	flatProgram->addShader(flatVertex);
	flatProgram->addShader(flatGeometry);
	//flatProgram->setParameter(GL_PROVOKING_VERTEX, GL_LAST_VERTEX_CONVENTION);
	ShaderUtils::loadShaderSource(flatVertex, "./data/shader/FLAT_SHADING.vert");
	ShaderUtils::loadShaderSource(flatFragment, "./data/shader/FLAT_SHADING.frag");
	ShaderUtils::loadShaderSource(flatGeometry, "./data/shader/FLAT_SHADING.geom");

	flatProgram->setParameter(GL_GEOMETRY_VERTICES_OUT_EXT, 3);
	flatProgram->setParameter(GL_GEOMETRY_INPUT_TYPE_EXT, GL_TRIANGLES);
	flatProgram->setParameter(GL_GEOMETRY_OUTPUT_TYPE_EXT, GL_TRIANGLE_STRIP);

	osg::ref_ptr<osg::StateSet> ss = new osg::StateSet;
	//ss->setMode(GL_LIGHTING, osg::StateAttribute::OFF);
	//ss->setMode(GL_BLEND, osg::StateAttribute::ON);
	//ss->setRenderingHint(osg::StateSet::TRANSPARENT_BIN);
	//ss->setRenderBinDetails(18, "DepthSortedBin");


	//osg::ref_ptr<osg::StateSet> ss = new osg::StateSet;
	osg::ref_ptr<osg::PolygonOffset> polyoffset = new osg::PolygonOffset;
	polyoffset->setFactor(0.95f);
	polyoffset->setUnits(1.0f);
	ss->setAttributeAndModes(polyoffset.get(), osg::StateAttribute::OVERRIDE | osg::StateAttribute::ON);


	ss->setAttributeAndModes(flatProgram, osg::StateAttribute::ON);
	addPass(ss.get());

	/////CODE FOR HUNTING INVALID ENUM OPENGL ERROR
	// create white material
	//osg::Material *material = new osg::Material();
	//material->setDiffuse(osg::Material::FRONT, osg::Vec4(1.0, 1.0, 1.0, 1.0));
	//material->setSpecular(osg::Material::FRONT, osg::Vec4(0.4, 0.4, 0.4, 1.0));
	//material->setAmbient(osg::Material::FRONT, osg::Vec4(0.9, 0.9, 0.9, 1.0));
	//material->setEmission(osg::Material::FRONT, osg::Vec4(0.1, 0.1, 0.1, 1.0));
	//material->setShininess(osg::Material::FRONT, 25.0);

	//osg::ref_ptr<osg::StateSet> ss = new osg::StateSet;
	//ss->setAttribute(material);

	//osg::ref_ptr<osg::PolygonOffset> polyoffset = new osg::PolygonOffset;
	//polyoffset->setFactor(0.95f);
	//polyoffset->setUnits(1.0f);
	//ss->setAttributeAndModes(polyoffset.get(), osg::StateAttribute::OVERRIDE | osg::StateAttribute::ON);

	//addPass(ss.get());
}

//FLAT_SHADOW_SHADING
void FlatShadowShading::define_passes()
{
	if (m_configReader->getStringFromStartupConfig("enable_shadow_mapping").compare("true") == 0)
	{
		osg::Uniform* shadowTextureSampler;
		osg::Uniform* ambientBiasUniform;
		osg::Uniform* m_depthMVPUniform;

		if (m_shadowMap)
		{
			shadowTextureSampler = new osg::Uniform("shadowTexture", m_shadowMap->getTextureUnit());
			ambientBiasUniform = new osg::Uniform("ambientBias", m_shadowMap->getAmbientBias());
		}
		else
		{
			std::cout << "Error in FlatShadowShading::define_passes - No Shadow map set!" << std::endl;
		}

		if (m_depthMVP.valid())
		{
			m_depthMVPUniform = new osg::Uniform("depthMVP", m_depthMVP);
		}
		else
		{
			std::cout << "Error in FlatShadowShading::define_passes - m_depthMVP not set!" << std::endl;
		}

		osg::Program* shadowFirstPassProgram = new osg::Program;
		osg::Shader* shadowFirstPassVertex = new osg::Shader(osg::Shader::VERTEX);
		osg::Shader* shadowFirstPassFragment = new osg::Shader(osg::Shader::FRAGMENT);
		shadowFirstPassProgram->addShader(shadowFirstPassVertex);
		shadowFirstPassProgram->addShader(shadowFirstPassFragment);

		ShaderUtils::loadShaderSource(shadowFirstPassVertex, "./data/shader/FLAT_SHADOW_SHADING_P1.vert");
		ShaderUtils::loadShaderSource(shadowFirstPassFragment, "./data/shader/FLAT_SHADOW_SHADING_P1.frag");

		osg::ref_ptr<osg::StateSet> ss = new osg::StateSet;

		ss->setAttributeAndModes(shadowFirstPassProgram, osg::StateAttribute::ON);

		ss->addUniform(m_depthMVPUniform);
		ss->addUniform(shadowTextureSampler);
		ss->addUniform(ambientBiasUniform);

		addPass(ss.get());
	}
}

//INDICATED_AS_SELECTED_SHADING
void IndicatedAsSelectedShading::define_passes()
{
	osg::Program* asSelectedProgram = new osg::Program;
	osg::Shader* asSelectedVertex = new osg::Shader(osg::Shader::VERTEX);
	osg::Shader* asSelectedFragment = new osg::Shader(osg::Shader::FRAGMENT);
	osg::Shader* asSelectedGeometry = new osg::Shader(osg::Shader::GEOMETRY);
	asSelectedProgram->addShader(asSelectedFragment);
	asSelectedProgram->addShader(asSelectedVertex);
	asSelectedProgram->addShader(asSelectedGeometry);
	//asSelectedProgram->setParameter(GL_PROVOKING_VERTEX, GL_LAST_VERTEX_CONVENTION);
	ShaderUtils::loadShaderSource(asSelectedVertex, "./data/shader/INDICATED_AS_SELECTED_SHADING.vert");
	ShaderUtils::loadShaderSource(asSelectedFragment, "./data/shader/INDICATED_AS_SELECTED_SHADING.frag");
	ShaderUtils::loadShaderSource(asSelectedGeometry, "./data/shader/INDICATED_AS_SELECTED_SHADING.geom");

	asSelectedProgram->setParameter(GL_GEOMETRY_VERTICES_OUT_EXT, 3);
	asSelectedProgram->setParameter(GL_GEOMETRY_INPUT_TYPE_EXT, GL_TRIANGLES);
	asSelectedProgram->setParameter(GL_GEOMETRY_OUTPUT_TYPE_EXT, GL_TRIANGLE_STRIP);

	osg::ref_ptr<osg::StateSet> ss = new osg::StateSet;
	//ss->setMode(GL_LIGHTING, osg::StateAttribute::OFF);
	//ss->setMode(GL_BLEND, osg::StateAttribute::ON);
	//ss->setRenderingHint(osg::StateSet::TRANSPARENT_BIN);
	//ss->setRenderBinDetails(18, "DepthSortedBin");

	ss->setAttributeAndModes(asSelectedProgram, osg::StateAttribute::ON);
	addPass(ss.get());
}

//INDICATED_AS_SELECTED_AND_SELECTED_SHADING
void IndicatedAsSelectedAndActiveShading::define_passes()
{
	osg::Program* asSelectedActiveProgram = new osg::Program;
	osg::Shader* asSelectedActiveVertex = new osg::Shader(osg::Shader::VERTEX);
	osg::Shader* asSelectedActiveFragment = new osg::Shader(osg::Shader::FRAGMENT);
	osg::Shader* asSelectedActiveGeometry = new osg::Shader(osg::Shader::GEOMETRY);
	asSelectedActiveProgram->addShader(asSelectedActiveFragment);
	asSelectedActiveProgram->addShader(asSelectedActiveVertex);
	asSelectedActiveProgram->addShader(asSelectedActiveGeometry);
	//asSelectedActiveProgram->setParameter(GL_PROVOKING_VERTEX, GL_LAST_VERTEX_CONVENTION);
	ShaderUtils::loadShaderSource(asSelectedActiveVertex, "./data/shader/INDICATED_AS_SELECTED_AND_ACTIVE_SHADING.vert");
	ShaderUtils::loadShaderSource(asSelectedActiveFragment, "./data/shader/INDICATED_AS_SELECTED_AND_ACTIVE_SHADING.frag");
	ShaderUtils::loadShaderSource(asSelectedActiveGeometry, "./data/shader/INDICATED_AS_SELECTED_AND_ACTIVE_SHADING.geom");

	asSelectedActiveProgram->setParameter(GL_GEOMETRY_VERTICES_OUT_EXT, 3);
	asSelectedActiveProgram->setParameter(GL_GEOMETRY_INPUT_TYPE_EXT, GL_TRIANGLES);
	asSelectedActiveProgram->setParameter(GL_GEOMETRY_OUTPUT_TYPE_EXT, GL_TRIANGLE_STRIP);

	osg::ref_ptr<osg::StateSet> ss = new osg::StateSet;
	//ss->setMode(GL_LIGHTING, osg::StateAttribute::OFF);
	//ss->setMode(GL_BLEND, osg::StateAttribute::ON);
	//ss->setRenderingHint(osg::StateSet::TRANSPARENT_BIN);
	//ss->setRenderBinDetails(18, "DepthSortedBin");

	ss->setAttributeAndModes(asSelectedActiveProgram, osg::StateAttribute::ON);
	addPass(ss.get());
}

//FILTHY_LINES_SHADING
void FilthyLinesShading::define_passes()
{
	//osg::Program* asSelectedActiveProgram = new osg::Program;
	//osg::Shader* asSelectedActiveVertex = new osg::Shader(osg::Shader::VERTEX);
	//osg::Shader* asSelectedActiveFragment = new osg::Shader(osg::Shader::FRAGMENT);
	////osg::Shader* asSelectedActiveGeometry = new osg::Shader(osg::Shader::GEOMETRY);
	//asSelectedActiveProgram->addShader(asSelectedActiveFragment);
	//asSelectedActiveProgram->addShader(asSelectedActiveVertex);
	////asSelectedActiveProgram->addShader(asSelectedActiveGeometry);
	////asSelectedActiveProgram->setParameter(GL_PROVOKING_VERTEX, GL_LAST_VERTEX_CONVENTION);
	//ShaderUtils::loadShaderSource(asSelectedActiveVertex, "./data/shader/FILTHY_LINES.vert");
	//ShaderUtils::loadShaderSource(asSelectedActiveFragment, "./data/shader/FILTHY_LINES.frag");
	////ShaderUtils::loadShaderSource(asSelectedActiveGeometry, "./data/shader/FILTHY_LINES.geom");

	////asSelectedActiveProgram->setParameter(GL_GEOMETRY_VERTICES_OUT_EXT, 2);
	////asSelectedActiveProgram->setParameter(GL_GEOMETRY_INPUT_TYPE_EXT, GL_LINES);
	////asSelectedActiveProgram->setParameter(GL_GEOMETRY_OUTPUT_TYPE_EXT, GL_LINES);

	//osg::ref_ptr<osg::StateSet> ss = new osg::StateSet;
	////ss->setMode(GL_LIGHTING, osg::StateAttribute::OFF);
	////ss->setMode(GL_BLEND, osg::StateAttribute::ON);
	////ss->setRenderingHint(osg::StateSet::TRANSPARENT_BIN);
	////ss->setRenderBinDetails(18, "DepthSortedBin");

	//ss->setAttributeAndModes(asSelectedActiveProgram, osg::StateAttribute::ON);
	//addPass(ss.get());
	//osg::ref_ptr<osg::StateSet> ss = new osg::StateSet;
	//osg::ref_ptr<osg::PolygonOffset> polyoffset = new osg::PolygonOffset;
	//polyoffset->setFactor(1.0f);
	//polyoffset->setUnits(1.0f);
	//ss->setAttributeAndModes(polyoffset.get(), osg::StateAttribute::OVERRIDE | osg::StateAttribute::ON);
	//ss->setAttributeAndModes(surfaceMaterial.get(), osg::StateAttribute::OVERRIDE | osg::StateAttribute::ON);
	//osg::ref_ptr<osg::ShadeModel> shadeModel = new osg::ShadeModel(osg::ShadeModel::FLAT);
	//ss->setAttribute(shadeModel.get());

	//No effect for lines and points
	//osg::ref_ptr<osg::StateSet> ss = new osg::StateSet;
	//osg::ref_ptr<osg::PolygonOffset> polyoffset = new osg::PolygonOffset;
	//polyoffset->setFactor(1.9f);
	//polyoffset->setUnits(1.0f);
	//ss->setAttributeAndModes(polyoffset.get(), osg::StateAttribute::OVERRIDE | osg::StateAttribute::ON);

	osg::ref_ptr<osg::StateSet> ss = new osg::StateSet;
	osg::ref_ptr<osg::LineWidth> wireframeLineWidth = new osg::LineWidth();
	wireframeLineWidth->setWidth(2.0);


	osg::ref_ptr<osg::PolygonMode> polymode = new osg::PolygonMode;
	polymode->setMode(osg::PolygonMode::FRONT_AND_BACK, osg::PolygonMode::LINE);
	ss->setAttributeAndModes(polymode.get(), osg::StateAttribute::OVERRIDE | osg::StateAttribute::ON);
	ss->setAttributeAndModes(wireframeLineWidth.get(), osg::StateAttribute::OVERRIDE | osg::StateAttribute::ON);
	//ss->setAttributeAndModes(wireframeMaterial.get(), osg::StateAttribute::OVERRIDE | osg::StateAttribute::ON);
	ss->setMode(GL_LIGHTING, osg::StateAttribute::OVERRIDE | osg::StateAttribute::OFF);
	ss->setTextureMode(0, GL_TEXTURE_2D, osg::StateAttribute::OVERRIDE | osg::StateAttribute::OFF);
	addPass(ss.get());
}

//FILTHY_POINTS_SHADING 
void FilthyPointsShading::define_passes()
{
	osg::ref_ptr<osg::StateSet> ss = new osg::StateSet;
	osg::ref_ptr<osg::PolygonMode> polymode = new osg::PolygonMode;
	polymode->setMode(osg::PolygonMode::FRONT_AND_BACK, osg::PolygonMode::POINT);
	ss->setAttributeAndModes(polymode.get(), osg::StateAttribute::OVERRIDE | osg::StateAttribute::ON);
	ss->setAttributeAndModes(new osg::Point(6.0), osg::StateAttribute::OVERRIDE | osg::StateAttribute::ON);
	ss->setMode(GL_LIGHTING, osg::StateAttribute::OVERRIDE | osg::StateAttribute::OFF);
	ss->setTextureMode(0, GL_TEXTURE_2D, osg::StateAttribute::OVERRIDE | osg::StateAttribute::OFF);
	addPass(ss.get());
}