// ClayMore - Immersive Mesh Modelling --- Copyright (c) 2014-2017 Philipp Ladwig, Jannik Fiedler, Jan Beutgen
#include "GeometryVisualization.h"
#include "RenderTechniques.h"
#include "../cmUtil/ConfigReader.h"

//OBSCURE: For what is this? Comes from osgFX and was part of an example (or of osgFX::Scribe?)
//#include <osgFX/Registry>
//osgFX::Registry::Proxy proxy(new GeometryVisualization);

GeometryVisualization::GeometryVisualization(ConfigReader* configReaderIn, osg::ref_ptr<osgShadow::ShadowMap> sm, osg::Matrix depthMVP) : m_configReader(configReaderIn)
{
	m_shadowMap = sm;
	m_depthMVP = depthMVP;
}

//TODO: Need a copy constuctor?
//GeometryVisualization::GeometryVisualization(const GeometryVisualization& copy, const osg::CopyOp& copyop)
//	: Effect(copy, copyop),
//	m_surfaceMaterial(static_cast<osg::Material*>(copyop(copy.m_surfaceMaterial.get()))),
//	m_wireframeMaterial(static_cast<osg::Material*>(copyop(copy.m_wireframeMaterial.get()))),
//	m_wireframeLineWidth(static_cast<osg::LineWidth*>(copyop(copy.m_wireframeLineWidth.get()))),
//	m_pointMaterial(static_cast<osg::Material*>(copyop(copy.m_pointMaterial.get())))
//{
//}

//osg::ref_ptr<osgShadow::ShadowMap> GeometryVisualization::getShadowMap()
//{
//	return m_shadowMap;
//}

bool GeometryVisualization::define_techniques()
{
	addTechnique(new FlatAndWireShading(m_configReader));
	addTechnique(new WireShading(m_configReader));
	addTechnique(new FlatShading(m_configReader));

	addTechnique(new FlatShadowShading(m_configReader, m_shadowMap, m_depthMVP));
	if (m_configReader->getStringFromStartupConfig("enable_shadow_mapping").compare("true") == 0)
	{
		if (!m_shadowMap)
			std::cout << "Error in GeometryVisualization::define_techniques - No Shadow Map found! Missing call to GeometryManager->setShadowMap()" << std::endl;
	}

	addTechnique(new IndicatedAsSelectedShading(m_configReader));
	addTechnique(new IndicatedAsSelectedAndActiveShading(m_configReader));
	addTechnique(new FilthyLinesShading(m_configReader));
	addTechnique(new FilthyPointsShading(m_configReader));
	return true;
}
