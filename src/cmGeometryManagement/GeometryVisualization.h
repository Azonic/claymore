// ClayMore - Immersive Mesh Modelling --- Copyright (c) 2014-2017 Philipp Ladwig, Jannik Fiedler, Jan Beutgen
#pragma once

#include "ConfigDllExportGeometryManagement.h"

#include <osg/LineWidth>
#include <osg/Point>
#include <osg/Material>

#include <osgFX/Effect>

#include <osgShadow/ShadowMap>

#include <iostream>

class ConfigReader;

enum RenderingType
{
	FLAT_AND_WIRE_SHADING,
	WIRE_SHADING,
	FLAT_SHADING,
	FLAT_SHADOW_SHADING,
	INDICATED_AS_SELECTED_SHADING,
	INDICATED_AS_SELECTED_AND_ACTIVE_SHADING,
	FILTHY_LINES,
	FILTHY_POINTS
};

class imGeometryManagement_DLL_import_export GeometryVisualization : public osgFX::Effect
{
	// #### CONSTRUCTOR & DESTRUCTOR ###############
public:
	GeometryVisualization(ConfigReader* configReaderIn);
	GeometryVisualization(ConfigReader* configReaderIn, osg::ref_ptr<osgShadow::ShadowMap> sm, osg::Matrix depthMVP);
	//~GeometryVisualization();


	// #### MEMBER VARIABLES ###############
private:
	ConfigReader* m_configReader;
	osg::ref_ptr<osg::Material> m_surfaceMaterial;
	osg::ref_ptr<osg::Material> m_wireframeMaterial;
	osg::ref_ptr<osg::LineWidth> m_wireframeLineWidth;
	float m_pointSize;
	osg::ref_ptr<osgShadow::ShadowMap> m_shadowMap;
	osg::Matrix m_depthMVP;

protected:
	virtual ~GeometryVisualization(){}
	//TODO: Do we need an equal operator? From an osg example
	//GeometryVisualization& operator=(const GeometryVisualization&) { return *this; }
	bool define_techniques();


	// #### MEMBER FUNCTIONS ###############
public:
	inline void selectTechnique(RenderingType renderingType)
	{
		Effect::selectTechnique(renderingType);
		//TODO1: OPEN GL ERROR:
		// There is in error "invalid enumrant" error before Glyph. It seems, that it has nothing to deal with osgText, is rather the
		// Shader, particulary the IndicatedAsSelectedAndActiveShading Shader. There is something wrong
		//std::cout << "#####################################renderingTypeEnum: " << renderingType << std::endl;
	}
	//void setUp();
	//osg::ref_ptr<osg::Group> getRoot();

	//osg::ref_ptr<osgShadow::ShadowMap> getShadowMap();

	const char *effectName() const        { return "ImmersiveModelling-RenderTechniques"; } \
	const char *effectDescription() const { return ""; } \
	const char *effectAuthor() const      { return "Philipp Ladwig"; }
};