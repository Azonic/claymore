// ClayMore - Immersive Mesh Modelling --- Copyright (c) 2014-2017 Philipp Ladwig, Jannik Fiedler, Jan Beutgen
#pragma once

#include "ConfigDllExportGeometryManagement.h"

#include "GeometryData.h"
#include "../cmGeometryProcessing/GeometryMainProcessor.h"

#include "Constraints.h"

class MiscDataStorage;

//This class provides a basic management over all geometries during runtime
//To edit and store a specific geometry in the scene, there is GeometryData
//To render GeometryData, there is the class GeometryVisualisation, which inherits from osgFX::Effect
class imGeometryManagement_DLL_import_export GeometryManager : public osg::Group
{
	// #### CONSTRUCTOR & DESTRUCTOR ###############
public:
	GeometryManager(ConfigReader* configReaderIn, MiscDataStorage* miscDataStorageIn);
	~GeometryManager();


	// #### MEMBER VARIABLES ###############
private:
	ConfigReader* m_configReader;
	MiscDataStorage* m_miscDataStorage;

	GeometryMainProcessor* m_geometryProcessor;
	std::string m_activeGeometry;
	osg::Vec4 m_selectedColor, m_notSelectedColor, m_prospectColor, m_selectedAndProspectColor;
	bool m_toggleSelectOrDeselectAll;
	int m_geomCounter;
	float m_probEditRadius;
	float  m_probEditShape;	
	osg::ref_ptr<osgShadow::ShadowMap> m_shadowMap;
	osg::Matrix m_depthMVP;
	
	bool m_isSnappingActive;
	Constraints m_constraint;
	//Is needed for toggling and exchanging the states for the button highlighting of the constraint menu
	bool m_isContraintActiveX, m_isContraintActiveY, m_isContraintActiveZ;

	float m_radiusOfMeshItemSelectionSphere; //Edit Mode sphere
	float m_secondPowerOfRadiusOfGeometrySelectionSphere; //Object Mode sphere


	// #### MEMBER FUNCTIONS ###############
public:
	inline void setShadowMap(osg::ref_ptr<osgShadow::ShadowMap> shadowMap, osg::Matrix depthMVP) 
	{ 
		m_shadowMap = shadowMap;
		m_depthMVP = depthMVP;
	}
	
	osg::ref_ptr<GeometryData> getGeometryDataByName(std::string nameIn);

	//inline ApplicationMode	getCurrentApplicationMode() { return m_currentApplicationMode; }
	//inline void setCurrentApplicationMode(ApplicationMode modeIn) { m_currentApplicationMode = modeIn; }

	void setActiveGeometry(std::string nameIn);

	//EXP: Get the name of the active geometry from m_activeGeometry
	inline std::string getNameOfActiveGeometry() { return m_activeGeometry; }

	osg::ref_ptr<GeometryData> getActiveGeometryData();

	void addGeometry(osg::Vec3 positionIn);
	void deleteGeometry(std::string name);

	void deselectAllObjects();
	void updateShaderAssignments();

	//Will join the currently avtive geometry and the parameter of this function
	bool joinGeometries(osg::ref_ptr<osg::OpenMeshGeometry> geometrieToJoin);
	void deepCopySelectedObjects();
	void duplicateSelectedMeshArea();
	void garbageCollection();

	void sphereSelectVertex(osg::Matrix const & cursorMatrix, float radiusIn);
	void sphereSelectVertexConfirm(osg::Matrix const & cursorMatrix, float radiusIn);
	void sphereSelectVertexNegate(osg::Matrix const & cursorMatrix, float radiusIn);
	void sphereSelectCleanUpVerticesColor();

	void sphereSelectGeometry(osg::Matrix* cursorLeftMatrixLtWIn);
	void sphereSelectConfirmGeometry(osg::Matrix* cursorLeftMatrixLtWIn);
	void sphereSelectCleanUpGeometriesColor();

	void grabVerticesStart(osg::Vec3f grabStartPositionIn);
	void grabVertices(osg::Vec3f newPositionIn, bool isLocalCoordSysSelectedIn, osg::Matrix matrixWorldToLocalIn);

	void grabProbEditVerticesStart(osg::Vec3f grabStartPositionIn);
	void grabProbEditVertices(osg::Vec3f newPositionIn, bool isLocalCoordSysSelectedIn, osg::Matrix matrixWorldToLocalIn);
	void grabProbEditVerticesSetValues(float radiusIn, float shapeIn);

	void rotateSelectedVerticesStart();
	void rotateSelectedVertices(osg::Matrixd rotMatIn, osg::Vec3d barycentrumStartIn);
	void rotateEntireGeometry(osg::Matrixd rotMatIn, osg::Vec3d barycentrum);

	void scaleSelectedVerticesStart();
	void scaleSelectedVertices(float factorIn, bool isLocalIn);
	void resetScaleGeometry();
	void scaleEntireGeometry(float scaleValueIn);

	void deleteSelectedMeshElements(bool verticesIn, bool edgesIn, bool facesIn);

	void extrude();

	void subdivide();

	void calcNormals();
	
	void readFileFromDisk(std::string filename);
	void writeFileToDisk(std::string savePath);

	void grabEnd(float x, float y, float z);
	void grabStopAndRecoverPosition();
	
	bool isAnyVertexSelected();
	void toggleSelectOrDeselectAll();
	void deselectAllVerts();
	
	//Toggles the rendering between Flat and Wire shading of main geometry (Doesn't effect filthy geometries)
	void toggleWireframeOfActiveGeometry();
	
	//Activates for every geometry in the scene wireframe, ignoring the internal wireframe state of geometry data
	//Useful, for remedy the visibility problem of not-seeing open menus which are cluttered by scene geometries
	void activateWireframeForAllGeometries();

	//Maybe useful for something?
	void deactivateWireframeForAllGeometries();

	//Useful in combination with activateWireframeForAllGeometries() for Menu visibility, for restoring the original states back
	void updateWireframeStatesForAllGeometries();

	osg::Vec3d getBarycentrumOfALLVertsForActiveGeomData();
	osg::Vec3d getBarycentrumOfSELECTEDVertsForActiveGeomData();

	osg::Vec4f getSelectedColor();

	void sculptStart(float radiusIn, float strengthIn);
	void sculptAdd(float radiusIn, float strengthIn, osg::Vec3 controllerTrans, osg::Matrix geomLocal2World);
	void sculptSubtract(float radiusIn, float strengthIn, osg::Vec3 controllerTrans, osg::Matrix geomLocal2World);

	inline float getProbEditRadius() { return m_probEditRadius; }
	inline float getProbEditShape() { return m_probEditShape; }

	inline void setProbEditRadius(float probEditRadiusIn) { m_probEditRadius; }
	inline void setProbEditShape(float probEditShape) { m_probEditShape; }

	inline void activateSnapping() { m_isSnappingActive = true; }
	inline void deactivateSnapping() { m_isSnappingActive = false; }
	//inline void setSnapping(bool snapIn) { m_isSnappingActive = snapIn; }
	//inline bool getSnapping() { return m_isSnappingActive; }
	inline bool isSnappingActive() { return m_isSnappingActive; }
	inline bool* getBoolAdressOfSnapping() { return &m_isSnappingActive; }
	inline void toggleSnapping();

	void setConstraint(Constraints constraintIn);
	//Returns the global current constraint (X, Y, Z or no Constraint)
	inline Constraints getConstraint() { return m_constraint; }
	inline void toggleConstraint(Constraints constraintIn);
	inline void resetConstraint();
	inline bool* getBoolAddressFromConstraintX() { return &m_isContraintActiveX; }
	inline bool* getBoolAddressFromConstraintY() { return &m_isContraintActiveY; }
	inline bool* getBoolAddressFromConstraintZ() { return &m_isContraintActiveZ; }

	void cancelAnyTransformation();
	void storeMeshDataAsHistory();
	void undo();
	

	std::string checkIntersectionWithGeometry(const osg::Vec3d start, const osg::Vec3d end, bool* isIntersectionFound);

	void updateFilthyGeometries();
	void updateFilthyGeometriesOnlyColor();
	unsigned int getNumberOfSelectedVerts();

	void addEdgeBasedOnSelectedVerts();
	void addFaceBasedOnSelectedVerts();

	MiscDataStorage* getMiscDataStorage() { return m_miscDataStorage;  }
};