// ClayMore - Immersive Mesh Modelling --- Copyright (c) 2014-2017 Philipp Ladwig, Jannik Fiedler, Jan Beutgen
#include "GeometryData.h"

#include "../cmUtil/ConfigReader.h"

#include <osg/Geode>

//#include "../imGeometryManagement/GeometryVisualization.h"

//REPO: There is an Version (commit 5.July 15) with shadow mapping and Shader. But this never works good, and so its not part of the master anymore - I get much trouble with UVs and OpenMesh

GeometryData::GeometryData(ConfigReader* configReaderIn, std::string name, osg::ref_ptr<osgShadow::ShadowMap> sm, osg::Matrix depthMVP) :
	m_configReader(configReaderIn)
{
	setName(name);
	//std::stringstream ss;
	//for (int i = 0; i < 4; ++i)
	//{
	//	ss << name.at(i);
	//}
	//name = name.substr(8, 14);
	m_openMeshGeom = new osg::OpenMeshGeometry(GeometryType::MODELLING, m_configReader, name);

	m_filthyLinesGeom = new osg::Geometry();
	m_filthyLinesGeom->setUseDisplayList(false);
	m_filthyLinesGeom->setUseVertexBufferObjects(true);
	m_filthyLinesGeom->setDataVariance(DYNAMIC);

	m_filthyPointsGeom = new osg::Geometry();
	m_filthyPointsGeom->setUseDisplayList(false);
	m_filthyPointsGeom->setUseVertexBufferObjects(true);
	m_filthyPointsGeom->setDataVariance(DYNAMIC);

	m_geode = new osg::Geode();
	m_filthyLinesGeode = new osg::Geode();
	m_filthyPointsGeode = new osg::Geode();

	m_geometryVisualization = new GeometryVisualization(m_configReader, sm, depthMVP);
	m_filthyLinesGeometryVisualization = new GeometryVisualization(m_configReader, sm, depthMVP);
	m_filthyPointsGeometryVisualization = new GeometryVisualization(m_configReader, sm, depthMVP);

	m_filthyLinesIndices = new osg::DrawElementsUInt(osg::PrimitiveSet::LINES, 0);
	m_filthyEdgesIndcies = new osg::DrawElementsUInt(osg::PrimitiveSet::LINES, 0);
	m_filthyPointsIndices = new osg::DrawElementsUInt(osg::PrimitiveSet::POINTS, 0);

	m_isSelected = false;
	m_isMarkedAsDeleted = false;
	m_isWireframeActivated = false;
}

GeometryData::~GeometryData()
{

}

//EXP: First call addGeometry and for displaying call this. There must be a vertex array assigned before calling this function
void GeometryData::initializeAndDisplay()
{
	//std::cout << "How often is this called? - because of adding Primitives" << std::endl;

	m_geode->addDrawable(m_openMeshGeom);

	m_filthyLinesGeom->setVertexArray(m_openMeshGeom->getVertexArray());
	m_filthyLinesGeom->setColorArray(m_openMeshGeom->getColorArray());
	createIndiciesForLinePrimitives();
	m_filthyLinesGeom->addPrimitiveSet(m_filthyLinesIndices);
	//m_filthyLinesGeom->addPrimitiveSet(m_openMeshGeom->getPrimitiveSet(0));
	//m_filthyLinesGeode->addDrawable(m_filthyLinesGeom);

	m_filthyPointsGeom->setVertexArray(m_openMeshGeom->getVertexArray());
	m_filthyPointsGeom->setColorArray(m_openMeshGeom->getColorArray());
	createIndiciesForPointPrimitives();
	m_filthyPointsGeom->addPrimitiveSet(m_filthyPointsIndices);
	//m_filthyPointsGeode->addDrawable(m_filthyPointsGeom);

	m_geometryVisualization->addChild(m_geode);
	m_filthyLinesGeometryVisualization->addChild(m_filthyLinesGeode);
	m_filthyPointsGeometryVisualization->addChild(m_filthyPointsGeode);

	addChild(m_geometryVisualization);
	addChild(m_filthyLinesGeometryVisualization);
	addChild(m_filthyPointsGeometryVisualization);
}

void GeometryData::createIndiciesForLinePrimitives()
{
	m_filthyLinesIndices->clear();

	short faceIndex = 0;
	for (unsigned int i = 0; i < m_openMeshGeom->getPrimitiveSet(0)->getNumIndices(); ++i)
	{
		faceIndex++;
		if (faceIndex == 1 || faceIndex == 2)
		{
			m_filthyLinesIndices->push_back(m_openMeshGeom->getPrimitiveSet(0)->index(i));
			m_filthyLinesIndices->push_back(m_openMeshGeom->getPrimitiveSet(0)->index(i + 1));
		}
		else if (faceIndex == 3)
		{
			m_filthyLinesIndices->push_back(m_openMeshGeom->getPrimitiveSet(0)->index(i));
			m_filthyLinesIndices->push_back(m_openMeshGeom->getPrimitiveSet(0)->index(i - 2));
			faceIndex = 0;
		}
	}

	//Commented this out for UserTest2 -> Could lead to some ugly edges, if someone delete something
	//if (m_filthyEdgesIndcies->getNumIndices() >= 2)
	//{
	//	for (size_t i = 0; i < m_filthyEdgesIndcies->getNumIndices(); ++i)
	//	{
	//		m_filthyLinesIndices->push_back(m_filthyEdgesIndcies->at(i));
	//	}
	//}
}

void GeometryData::rebuildFilthyLinesGeometry()
{
	createIndiciesForLinePrimitives();
	m_filthyLinesGeom->setPrimitiveSet(0, m_filthyLinesIndices);
	m_filthyLinesGeom->getPrimitiveSet(0)->dirty();
}

void GeometryData::addFilthyEdge(int idxIn, int lastIdxIn)
{
	m_filthyEdgesIndcies->push_back(idxIn);
	m_filthyEdgesIndcies->push_back(lastIdxIn);
}

void GeometryData::removeFilthyEdges()
{
	m_filthyEdgesIndcies->clear();
}

void GeometryData::createIndiciesForPointPrimitives()
{
	m_filthyPointsIndices->clear();

	for (unsigned int i = 0; i < m_openMeshGeom->getVertexArray()->getNumElements(); ++i)
	{
		m_filthyPointsIndices->push_back(i);
	}
}

void GeometryData::rebuildFilthyPointsGeometry()
{
	createIndiciesForPointPrimitives();
	m_filthyPointsGeom->setPrimitiveSet(0, m_filthyPointsIndices);
	m_filthyPointsGeom->getPrimitiveSet(0)->dirty();
}

void GeometryData::attachFilthyLinesGeode()
{
	//Only add one (actually the one and only) geometry to the filthyLinesGeode
	if (m_filthyLinesGeode->getNumDrawables() == 0)
	{
		m_filthyLinesGeode->addChild(m_filthyLinesGeom);
	}
	//getFilthyLinesGeometryVisualization()->selectTechnique(FILTHY_LINES);
}

void GeometryData::attachFilthyPointsGeode()
{
	//Only add one (actually the one and only) geometry to the m_filthyPointsGeode
	if (m_filthyPointsGeode->getNumDrawables() == 0)
	{
		m_filthyPointsGeode->addChild(m_filthyPointsGeom);
	}
	//getFilthyLinesGeometryVisualization()->selectTechnique(FILTHY_POINTS);
}

void GeometryData::detachFilthyLinesGeode()
{
	//Seems to work not propobly, because sometimes the drawables remain after switching between object and edit mode....
	//m_filthyLinesGeode->removeDrawables(0, 1);
	m_filthyLinesGeode->removeDrawables(0, m_filthyLinesGeode->getNumDrawables());
}

void GeometryData::detachFilthyPointsGeode()
{
	//Seems to work not propobly, because sometimes the drawables remain after switching between object and edit mode....
	//m_filthyPointsGeode->removeDrawables(0, 1);
	m_filthyPointsGeode->removeDrawables(0, m_filthyPointsGeode->getNumDrawables());
}



//EXP: Get access to m_geometryVisualization to change RenderTechniques
GeometryVisualization* GeometryData::getGeometryVisualization()
{
	return m_geometryVisualization;
}

//EXP: Get access to m_geometryVisualization to change RenderTechniques
GeometryVisualization* GeometryData::getFilthyLinesGeometryVisualization()
{
	return m_filthyLinesGeometryVisualization;
}

//EXP: Get access to m_geometryVisualization to change RenderTechniques
GeometryVisualization* GeometryData::getFilthyPointsGeometryVisualization()
{
	return m_filthyPointsGeometryVisualization;
}

void GeometryData::setIsMarkedAsDeleted(bool isMarkedAsDeleted)
{
	m_isMarkedAsDeleted = isMarkedAsDeleted;
}

bool GeometryData::getIsMarkedAsDeleted()
{
	return m_isMarkedAsDeleted;
}

//CalyMore-method for updating BB
void GeometryData::updateBoundingBox()
{
	if (m_openMeshGeom != nullptr)
	{
		getOpenMeshGeo()->computeBound();
		getOpenMeshGeo()->dirtyBound();

		getFilthyLinesGeometry()->computeBound();
		getFilthyLinesGeometry()->dirtyBound();

		getFilthyPointsGeometry()->computeBound();
		getFilthyPointsGeometry()->dirtyBound();
	}
}