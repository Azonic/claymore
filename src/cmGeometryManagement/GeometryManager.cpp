// ClayMore - Immersive Mesh Modelling --- Copyright (c) 2014-2017 Philipp Ladwig, Jannik Fiedler, Jan Beutgen
#include "GeometryManager.h"

#include "cmCore\defines.h"

#include <osg/Geode>
#include <osg/PositionAttitudeTransform>
#include <osg/ShapeDrawable>

#include <osgUtil/LineSegmentIntersector>

#include "../cmUtil/MiscDataStorage.h"

GeometryManager::GeometryManager(ConfigReader* configReaderIn, MiscDataStorage* miscDataStorageIn) : m_configReader(configReaderIn), m_miscDataStorage(miscDataStorageIn)
{
	m_shadowMap = nullptr;

	m_geometryProcessor = new GeometryMainProcessor();
	m_geomCounter = 0;

	m_selectedColor = m_configReader->getVec4fFromStartupConfig("color_vertex_selected");
	m_notSelectedColor = m_configReader->getVec4fFromStartupConfig("color_vertex_not_selected");
	m_prospectColor = m_configReader->getVec4fFromStartupConfig("color_vertex_prospect");
	m_selectedAndProspectColor = m_configReader->getVec4fFromStartupConfig("color_vertex_selected_and_prospect");
	m_radiusOfMeshItemSelectionSphere = m_configReader->getFloatFromStartupConfig("radius_of_mesh_item_selection_sphere");
	m_secondPowerOfRadiusOfGeometrySelectionSphere = pow(m_configReader->getFloatFromStartupConfig("radius_of_geometry_object_selection_sphere"), 2); //Don't know if it \n
	//is used currently. Maybe better to treat it like m_radiusOfMeshItemSelectionSphere without the pow() in the CTOR here.

	m_toggleSelectOrDeselectAll = true;
	m_probEditRadius = 0.5f;
	m_probEditShape = 1.25f;

	m_constraint = Constraints::NO_CONSTRAIN;

	m_isContraintActiveX = false;
	m_isContraintActiveY = false;
	m_isContraintActiveZ = false;
}

GeometryManager::~GeometryManager()
{

}

//EXP: Adds a new GeometryData to GeometryManager. At this time it cant be displayed. First you have to call initializeAndDisplay() at the GeometryData object
//TODO: Insert initializeAndDisplay() in this function?
void GeometryManager::addGeometry(osg::Vec3 positionIn)
{
	std::stringstream ss;
	ss << m_geomCounter;
	osg::ref_ptr<GeometryData> geometryData = new GeometryData(m_configReader, "geom" + ss.str(), m_shadowMap, m_depthMVP);
	addChild(geometryData);
	setActiveGeometry("geom" + ss.str());
	m_geomCounter++;

	//std::cout << geometryData->getName() << std::endl;
	if (ConfigReader::DEBUG_LEVEL >= NOT_IN_LOOPS)
	{
		std::cout << "GeometryCounter set to " << m_geomCounter << std::endl;
	}
}

osg::ref_ptr<GeometryData> GeometryManager::getGeometryDataByName(std::string searchName)
{
	for (unsigned int i = 0; i < getNumChildren(); i++)
	{
		if (getChild(i)->getName().compare(searchName) == 0)
		{
			return dynamic_cast<GeometryData*>(getChild(i));
		}
	}
	//FIXME: Error handling when node can not be found
	//std::cout << "ERROR in finding Node" << std::endl;
	return NULL;
}

//EXP: Set the name of the geometry to m_activeGeometry
void GeometryManager::setActiveGeometry(std::string nameIn)
{
	m_activeGeometry = nameIn;

	if (!(getNameOfActiveGeometry().compare(g_stringNothingInSceneAssertion) == 0))
	{
		//Asks if is something selected
		if (!(nameIn.compare(g_stringNoSelectionAssertion) == 0))
		{
			//Every processor will be updated and get the current openMeshGeom
			m_geometryProcessor->setActiveGeometry(getActiveGeometryData()->getOpenMeshGeo());

			//Every geometry gets a FLAT_SHADING except the selected Geometry
			for (unsigned int i = 0; i < getNumChildren(); ++i)
			{
				dynamic_cast<GeometryData*>(getChild(i))->getGeometryVisualization()->selectTechnique(FLAT_SHADING);
			}
			getActiveGeometryData()->getGeometryVisualization()->selectTechnique(FLAT_AND_WIRE_SHADING);
		}
	}
}

osg::ref_ptr<GeometryData> GeometryManager::getActiveGeometryData()
{
	//std::cout << "m_activeGeometry: " << m_activeGeometry << std::endl;

	if (!(getNameOfActiveGeometry().compare(g_stringNothingInSceneAssertion) == 0) ||
		!(getNameOfActiveGeometry().compare(g_stringNoSelectionAssertion) == 0))
	{
		for (unsigned int i = 0; i < getNumChildren(); i++)
		{

			//std::cout << getChild(i)->getName() << std::endl;

			//std::cout << "	getChild(i)->getName(): " << getChild(i)->getName() << std::endl;
			if (getChild(i)->getName() == m_activeGeometry)
			{
				//std::cout << "YES to getChild(i)->getName() == m_activeGeometry " << std::endl;
				return dynamic_cast<GeometryData*>(getChild(i));
			}
		}
		//FIXME: Error handling when node can not be found
		std::cout << "ERROR: GeometryManager::getActiveGeometryData() in finding Node" << std::endl;
		return nullptr;
	}
	else
	{
		std::cout << "Nothing in Scene or no active object" << std::endl;
		return nullptr;
	}
}

//EXP :No internal vertex selection will be done. Colors of the vertices from the activeGeometry will change color, when they are within the sphere. OpenMesh is not used 
void GeometryManager::sphereSelectVertex(osg::Matrix const & cursorMatrix, float radiusIn)
{
	if (getActiveGeometryData()->getOpenMeshGeo()->getGeometryType() == GeometryType::MODELLING)
	{
		//PERFORMANCE: Use a static cast instead of dynamic?
		osg::Vec3Array *vertexArray = dynamic_cast<osg::Vec3Array*>(getActiveGeometryData()->getOpenMeshGeo()->getVertexArray());
		osg::Vec4Array *colorArray = dynamic_cast<osg::Vec4Array*>(getActiveGeometryData()->getOpenMeshGeo()->getColorArray());

		//std::cout << "Anzahl Verts:" << vertexArray->getNumElements() << std::endl;
		//std::cout << "Anzahl Indicies:" << getActiveGeometryData()->getOpenMeshGeo()->getPrimitiveSet(0)->getDrawElements()->getNumIndices() << std::endl;

		float squareRadius = pow(radiusIn * m_radiusOfMeshItemSelectionSphere, 2);

		//EXP: catch the Position, Rotation and Scale from geometryManagerPosAtt in OSGS with: getParentalNodePaths()[0]
		osg::Matrix geomMatrix = osg::computeLocalToWorld(getActiveGeometryData()->getParentalNodePaths()[0]);

		/*for (unsigned int i = 0; i < getActiveGeometryData()->getOpenMeshGeo()->getVertexArray()->getNumElements(); i++)
		{
			osg::Vec3 vec3 = vertexArray->at(i) * geomMatrix;

			float circle_equation = pow(cursorMatrix.getTrans().x() - vec3.x(), 2) +
				pow(cursorMatrix.getTrans().y() - (vec3.y()), 2) +
				pow(cursorMatrix.getTrans().z() - (vec3.z()), 2);

			if (circle_equation < squareRadius)
			{
				if (colorArray->at(i) == m_selectedColor)
				{
					colorArray->at(i) = m_selectedAndProspectColor;
				}
				if (!(colorArray->at(i) == m_selectedAndProspectColor))
				{
					colorArray->at(i) = m_prospectColor;
				}
			}
			if (circle_equation > squareRadius)
			{
				if (colorArray->at(i) == m_prospectColor)
				{
					colorArray->at(i) = m_notSelectedColor;
				}

				if (colorArray->at(i) == m_selectedAndProspectColor)
				{
					colorArray->at(i) = m_selectedColor;
				}
			}
		}*/

		getActiveGeometryData()->getOpenMeshGeo()->getColorArray()->dirty();
		updateFilthyGeometriesOnlyColor();
	}
	else if (getActiveGeometryData()->getOpenMeshGeo()->getGeometryType() == GeometryType::POINT_CLOUD)
	{
		//std::cout << "In Sphere Select Point Cloud" << std::endl;
	}
}

//EXP: User confirms the selection. An internal vertex selection will be done. Colors of the vertices from the activeGeometry will change color, when they are within the sphere. OpenMesh is not used 
void GeometryManager::sphereSelectVertexConfirm(osg::Matrix const & cursorMatrix, float radiusIn)
{
	osg::Vec3Array *vertexArray = dynamic_cast<osg::Vec3Array*>(getActiveGeometryData()->getOpenMeshGeo()->getVertexArray());
	osg::Vec4Array *colorArray = dynamic_cast<osg::Vec4Array*>(getActiveGeometryData()->getOpenMeshGeo()->getColorArray());

	float squareRadius = pow(radiusIn * m_radiusOfMeshItemSelectionSphere, 2);

	//catch the Position, Rotation and Scale from geometryManagerPosAtt in OSGS with: getParentalNodePaths()[2]
	osg::Matrix geomMatrix = osg::computeLocalToWorld(getActiveGeometryData()->getParentalNodePaths()[0]);

	bool somethingWasSelected = false;
	for (unsigned int i = 0; i < vertexArray->getNumElements(); ++i)
	{
		osg::Vec3 vec3 = vertexArray->at(i) * geomMatrix;

		float circle_equation = pow(cursorMatrix.getTrans().x() - vec3.x(), 2) +
			pow(cursorMatrix.getTrans().y() - (vec3.y()), 2) +
			pow(cursorMatrix.getTrans().z() - (vec3.z()), 2);

		if (colorArray->at(i) == m_selectedAndProspectColor) {
			colorArray->at(i) = m_selectedColor;
		}

		if (!(colorArray->at(i) == m_selectedColor)) {
			if (circle_equation < squareRadius)
			{
				colorArray->at(i) = m_selectedColor;
				somethingWasSelected = true;

				//TODO: Here is the place to set the status bit selected from OpenMesh-Mesh
				//getActiveGeometryData()->getOpenMeshGeo()->getMesh().status(
			}
			if (circle_equation > squareRadius)
			{
				colorArray->at(i) = m_notSelectedColor;
			}
		}
	}
	//Rumble the controller
	if (somethingWasSelected)
	{
		m_miscDataStorage->setRumbleShortState(true);
	}
	getActiveGeometryData()->getOpenMeshGeo()->getColorArray()->dirty();
	updateFilthyGeometriesOnlyColor();

	m_toggleSelectOrDeselectAll = false;
}

//EXP: User negate the selection. An internal vertex deselection will be done. Colors of the vertices from the activeGeometry will change color, when they are within the sphere. OpenMesh is not used 
void GeometryManager::sphereSelectVertexNegate(osg::Matrix const & cursorMatrix, float radiusIn)
{
	osg::Vec3Array *vertexArray = dynamic_cast<osg::Vec3Array*>(getActiveGeometryData()->getOpenMeshGeo()->getVertexArray());
	osg::Vec4Array *colorArray = dynamic_cast<osg::Vec4Array*>(getActiveGeometryData()->getOpenMeshGeo()->getColorArray());

	float squareRadius = pow(radiusIn * m_radiusOfMeshItemSelectionSphere, 2);

	//catch the Position, Rotation and Scale from geometryManagerPosAtt in OSGS with: getParentalNodePaths()[2]
	osg::Matrix geomMatrix = osg::computeLocalToWorld(getActiveGeometryData()->getParentalNodePaths()[0]);

	bool somethingWasSelected = false;
	for (unsigned int i = 0; i < vertexArray->getNumElements(); i++)
	{
		osg::Vec3 vec3 = vertexArray->at(i) * geomMatrix;

		float circle_equation = pow(cursorMatrix.getTrans().x() - vec3.x(), 2) +
			pow(cursorMatrix.getTrans().y() - (vec3.y()), 2) +
			pow(cursorMatrix.getTrans().z() - (vec3.z()), 2);

		if (colorArray->at(i) == m_selectedColor || colorArray->at(i) == m_selectedAndProspectColor) {
			if (circle_equation < squareRadius)
			{
				colorArray->at(i) = m_notSelectedColor;
				somethingWasSelected = true;
			}
		}
	}

	//Rumble the controller
	if (somethingWasSelected)
	{
		m_miscDataStorage->setRumbleShortState(true);
	}
	getActiveGeometryData()->getOpenMeshGeo()->getColorArray()->dirty();
	updateFilthyGeometriesOnlyColor();
}

//EXP: When the user stops to select, confirm or negate vertices, all vertices should be checked if they have the right color. 
//		E.g.: there should be no vertex left, that is in prospectColor when selection was stopped. OpenMesh is not used 
void GeometryManager::sphereSelectCleanUpVerticesColor()
{
	if (getActiveGeometryData() != nullptr && getActiveGeometryData()->getOpenMeshGeo() != nullptr && getActiveGeometryData()->getOpenMeshGeo()->getColorArray() != nullptr)
	{
		osg::Vec4Array *colorArray = dynamic_cast<osg::Vec4Array*>(getActiveGeometryData()->getOpenMeshGeo()->getColorArray());
		for (unsigned int i = 0; i < colorArray->getNumElements(); i++)
		{
			if (colorArray->at(i) == m_prospectColor)
			{
				colorArray->at(i) = m_notSelectedColor;
			}

			if (colorArray->at(i) == m_selectedAndProspectColor)
			{
				colorArray->at(i) = m_selectedColor;
			}
		}
		getActiveGeometryData()->getOpenMeshGeo()->getColorArray()->dirty();
		updateFilthyGeometriesOnlyColor();
	}
}

//EXP: No internal geometry selection will be done. The RenderTechnique of the geometry within the sphere will be changed. OpenMesh is not used 
void GeometryManager::sphereSelectGeometry(osg::Matrix* cursorLeftMatrixLtWIn)
{
	for (unsigned int i = 0; i < getNumChildren(); i++)
	{
		osg::Vec3Array *vertexArray = dynamic_cast<osg::Vec3Array*>(dynamic_cast<GeometryData*>(getChild(i))->getOpenMeshGeo()->getVertexArray());
		osg::Matrix geomMatrix = osg::computeLocalToWorld(dynamic_cast<GeometryData*>(getChild(i))->getParentalNodePaths()[0]);

		for (unsigned int j = 0; j < dynamic_cast<GeometryData*>(getChild(i))->getOpenMeshGeo()->getVertexArray()->getNumElements(); j++)
		{
			osg::Vec3 vec3 = vertexArray->at(j) * geomMatrix;

			float circle_equation = pow(cursorLeftMatrixLtWIn->getTrans().x() - vec3.x(), 2) +
				pow(cursorLeftMatrixLtWIn->getTrans().y() - (vec3.y()), 2) +
				pow(cursorLeftMatrixLtWIn->getTrans().z() - (vec3.z()), 2);

			if (circle_equation < m_secondPowerOfRadiusOfGeometrySelectionSphere)
			{
				if (!(getChild(i)->getName().compare(getNameOfActiveGeometry()) == 0))
				{
					//TODO: create enums for RenderTechniques
					dynamic_cast<GeometryData*>(getChild(i))->getGeometryVisualization()->selectTechnique(INDICATED_AS_SELECTED_SHADING);
				}
				break;
			}
			if (circle_equation > m_secondPowerOfRadiusOfGeometrySelectionSphere)
			{
				if (!(getChild(i)->getName().compare(getNameOfActiveGeometry()) == 0))
				{
					dynamic_cast<GeometryData*>(getChild(i))->getGeometryVisualization()->selectTechnique(FLAT_SHADING);
				}
			}
		}
	}
}

//EXP: User confirms the selection of a geometry. An internal geometry selection will be done. The RenderTechnique of the geometry within the sphere will be changed. OpenMesh is not used 
void GeometryManager::sphereSelectConfirmGeometry(osg::Matrix* cursorLeftMatrixLtWIn)
{
	for (unsigned int i = 0; i < getNumChildren(); i++)
	{
		osg::Vec3Array *vertexArray = dynamic_cast<osg::Vec3Array*>(dynamic_cast<GeometryData*>(getChild(i))->getOpenMeshGeo()->getVertexArray());

		osg::Matrix geomMatrix = osg::computeLocalToWorld(dynamic_cast<GeometryData*>(getChild(i))->getParentalNodePaths()[0]);

		for (unsigned int j = 0; j < dynamic_cast<GeometryData*>(getChild(i))->getOpenMeshGeo()->getVertexArray()->getNumElements(); j++)
		{
			osg::Vec3 vec3 = vertexArray->at(j) * geomMatrix;

			float circle_equation = pow(cursorLeftMatrixLtWIn->getTrans().x() - vec3.x(), 2) +
				pow(cursorLeftMatrixLtWIn->getTrans().y() - (vec3.y()), 2) +
				pow(cursorLeftMatrixLtWIn->getTrans().z() - (vec3.z()), 2);

			//Within sphere
			if (circle_equation < m_secondPowerOfRadiusOfGeometrySelectionSphere)
			{
				dynamic_cast<GeometryData*>(getChild(i))->getGeometryVisualization()->selectTechnique(FLAT_AND_WIRE_SHADING);
				setActiveGeometry(getChild(i)->getName());
				break;
			}
		}
	}
}

//EXP: When the user stops to select or confirm geometries, all geometries should be checked if they have the right RenderTechnique assigned. OpenMesh is not used 
void GeometryManager::sphereSelectCleanUpGeometriesColor()
{
	for (unsigned int i = 0; i < getNumChildren(); i++)
	{
		if ((getChild(i)->getName().compare(getNameOfActiveGeometry())) == 0)
		{
			dynamic_cast<GeometryData*>(getChild(i))->getGeometryVisualization()->selectTechnique(FLAT_AND_WIRE_SHADING);
		}
		else
		{
			dynamic_cast<GeometryData*>(getChild(i))->getGeometryVisualization()->selectTechnique(FLAT_SHADING);
		}
	}
}


void GeometryManager::grabVerticesStart(osg::Vec3f grabStartPositionIn)
{
	getActiveGeometryData()->getOpenMeshGeo()->translateSelColorToOMStatusBit();
	m_geometryProcessor->grabVerticesStart(grabStartPositionIn);
}


void GeometryManager::grabVertices(osg::Vec3f newPositionIn, bool isLocalCoordSysSelectedIn, osg::Matrix matrixWorldToLocalIn)
{
	m_geometryProcessor->grabVertices(newPositionIn, isLocalCoordSysSelectedIn, m_isSnappingActive, m_constraint, matrixWorldToLocalIn);
	updateFilthyGeometries();
}


void GeometryManager::grabProbEditVerticesStart(osg::Vec3f grabStartPositionIn)
{
	getActiveGeometryData()->getOpenMeshGeo()->translateSelColorToOMStatusBit();
	m_geometryProcessor->grabPropEditStart(grabStartPositionIn, m_probEditRadius, m_probEditShape);
}


void GeometryManager::grabProbEditVertices(osg::Vec3f newPositionIn, bool isLocalCoordSysSelectedIn, osg::Matrix matrixWorldToLocalIn)
{
	m_geometryProcessor->grabPropEdit(newPositionIn, isLocalCoordSysSelectedIn, m_constraint, matrixWorldToLocalIn);
	updateFilthyGeometries();
}


void GeometryManager::grabProbEditVerticesSetValues(float radiusIn, float shapeIn)
{
	m_probEditRadius = m_probEditRadius * radiusIn;
	//m_probEditShape += shapeIn / 30;

	//std::cout << "m_probEditRadius: " << m_probEditRadius << std::endl;
	//std::cout << "m_probEditShape: " << m_probEditShape << std::endl;
	if (m_probEditShape < 0)
	{
		m_probEditShape = 0;
	}
	m_geometryProcessor->grabProbEditSetValues(m_probEditRadius, m_probEditShape);
}

void GeometryManager::scaleSelectedVerticesStart()
{
	m_geometryProcessor->scaleVerticesStart();
}

//EXP: Selected vertices will be scaled. Barycentrum will be the mid point of scaling. OpenMesh is used (barycentrum and scale function)
void GeometryManager::scaleSelectedVertices(float factorIn, bool isLocalIn)
{
	osg::Matrix matrixWorldToLocal = osg::computeLocalToWorld(getActiveGeometryData()->getParentalNodePaths()[0]);
	osg::Matrix matrixLocalToWorld = osg::computeWorldToLocal(getActiveGeometryData()->getParentalNodePaths()[0]);
	getActiveGeometryData()->getOpenMeshGeo()->translateSelColorToOMStatusBit();
	//FIXME: Do we need the two matrices?
	m_geometryProcessor->scaleVertices(factorIn, isLocalIn, m_isSnappingActive, m_constraint, matrixWorldToLocal, matrixLocalToWorld, m_geometryProcessor->getBarycentrumOfSELECTEDVerts());
	updateFilthyGeometries();
}

void GeometryManager::resetScaleGeometry()
{
	m_geometryProcessor->resetScaleGeometry();
}

void GeometryManager::scaleEntireGeometry(float scaleValueIn)
{
	getActiveGeometryData()->getOpenMeshGeo()->translateSelColorToOMStatusBit();
	if (isAnyVertexSelected())
	{
		m_geometryProcessor->scaleEntireGeometryBasedOnBaryOfSELECTEDVerts(scaleValueIn);
	}
	else
	{
		m_geometryProcessor->scaleEntireGeometryBasedOnBaryOfALLVerts(scaleValueIn);
	}
}

//EXP: Start function for rotating selected vertices around their barycentrum. OpenMesh is used (coor selection will be translated to OpenMesh::Mesh.status.set_selected())
void GeometryManager::rotateSelectedVerticesStart()
{
	getActiveGeometryData()->getOpenMeshGeo()->translateSelColorToOMStatusBit();
	m_geometryProcessor->rotateVerticesStart();
	updateFilthyGeometries();
}

//EXP: Selected vertices will be rotated around their barycentrum. OpenMesh is used (barycentrum and rotate function)
void GeometryManager::rotateSelectedVertices(osg::Matrixd rotMatIn, osg::Vec3d barycentrumStartIn)
{
	m_geometryProcessor->rotateVertices(rotMatIn, m_isSnappingActive, m_constraint, barycentrumStartIn);
}

void GeometryManager::rotateEntireGeometry(osg::Matrixd rotMatIn, osg::Vec3d barycentrum)
{
	m_geometryProcessor->rotateEntireGeometry(rotMatIn, barycentrum);
}

//Only one parameter can be true ->Otherwise memory leak and freeze
void GeometryManager::deleteSelectedMeshElements(bool verticesIn, bool edgesIn, bool facesIn)
{
	getActiveGeometryData()->getOpenMeshGeo()->translateSelColorToOMStatusBit();
	m_geometryProcessor->deleteSelectedMeshElements(verticesIn, edgesIn, facesIn); //Only one parameter can be true ->Otherwise memory leak and freeze

	//NOTE: Important to dirty VertexArray and all PrimitiveSets
	getActiveGeometryData()->getOpenMeshGeo()->getVertexArray()->dirty();
	for (int i = 0; i < getActiveGeometryData()->getOpenMeshGeo()->getPrimitiveSetList().size(); i++)
	{
		getActiveGeometryData()->getOpenMeshGeo()->getPrimitiveSet(i)->dirty();
	}
	updateFilthyGeometries();
}

void GeometryManager::extrude()
{
	getActiveGeometryData()->getOpenMeshGeo()->translateSelColorToOMStatusBit();
	//m_geometryProcessor(getActiveGeometryData()->getOpenMeshGeo())->extrude();
	m_geometryProcessor->extrude();

	//FIXME: Create a function for this stuff
	getActiveGeometryData()->getOpenMeshGeo()->getVertexArray()->dirty();
	for (int i = 0; i < getActiveGeometryData()->getOpenMeshGeo()->getPrimitiveSetList().size(); i++)
	{
		getActiveGeometryData()->getOpenMeshGeo()->getPrimitiveSet(i)->dirty();
	}
	updateFilthyGeometries();
}

void GeometryManager::subdivide()
{
	m_geometryProcessor->subdivide();
}

void GeometryManager::calcNormals()
{
	m_geometryProcessor->calcNormals();
}

void GeometryManager::readFileFromDisk(std::string filename)
{
	m_geometryProcessor->readFileFromDisk(filename);
}

void GeometryManager::writeFileToDisk(std::string savePath)
{
	m_geometryProcessor->writeFileToDisk(savePath);
}

bool GeometryManager::isAnyVertexSelected()
{
	osg::Vec4Array *colorArray = dynamic_cast<osg::Vec4Array*>(getActiveGeometryData()->getOpenMeshGeo()->getColorArray());

	bool isAnyVertexSelected = false;
	for (unsigned int i = 0; i < getActiveGeometryData()->getOpenMeshGeo()->getVertexArray()->getNumElements(); i++)
	{
		if (colorArray->at(i) == m_selectedColor)
		{
			isAnyVertexSelected = true;
			break; //Immediately stop for loop if something was found
		}
	}
	return isAnyVertexSelected;
}

//EXP: Toggle all vertices to selected or deselected. OpenMesh is not used
void GeometryManager::toggleSelectOrDeselectAll()
{
	osg::Vec4Array *colorArray = dynamic_cast<osg::Vec4Array*>(getActiveGeometryData()->getOpenMeshGeo()->getColorArray());

	if (isAnyVertexSelected())
		m_toggleSelectOrDeselectAll = false;
	else
		m_toggleSelectOrDeselectAll = true;

	for (unsigned int i = 0; i < getActiveGeometryData()->getOpenMeshGeo()->getVertexArray()->getNumElements(); i++)
	{
		if (m_toggleSelectOrDeselectAll)
		{
			colorArray->at(i) = m_selectedColor;
		}
		else
		{
			colorArray->at(i) = m_notSelectedColor;
		}
	}
	getActiveGeometryData()->getOpenMeshGeo()->getColorArray()->dirty();

	if (m_toggleSelectOrDeselectAll)
	{
		m_toggleSelectOrDeselectAll = false;
	}
	else
	{
		m_toggleSelectOrDeselectAll = true;
	}
	updateFilthyGeometries();
}

void GeometryManager::deselectAllVerts()
{
	osg::Vec4Array *colorArray = dynamic_cast<osg::Vec4Array*>(getActiveGeometryData()->getOpenMeshGeo()->getColorArray());

	for (unsigned int i = 0; i < getActiveGeometryData()->getOpenMeshGeo()->getVertexArray()->getNumElements(); i++)
	{
		colorArray->at(i) = m_notSelectedColor;
	}
}

//EXP: Toggle the RenderTechnique between wireframe and surface-shaded. OpenMesh is not used
void GeometryManager::toggleWireframeOfActiveGeometry()
{
	if (getActiveGeometryData()->getIsWireframeActivated())
	{
		getActiveGeometryData()->getGeometryVisualization()->selectTechnique(FLAT_SHADING);
		getActiveGeometryData()->attachFilthyLinesGeode();
		getActiveGeometryData()->attachFilthyPointsGeode();
		getActiveGeometryData()->setIsWireframeActivated(false);
	}
	else
	{
		getActiveGeometryData()->getGeometryVisualization()->selectTechnique(WIRE_SHADING);
		getActiveGeometryData()->attachFilthyLinesGeode();
		getActiveGeometryData()->attachFilthyPointsGeode();
		getActiveGeometryData()->setIsWireframeActivated(true);
	}
}
//HWM: Ist eigentlich eher activateGeometryTransparencyForMenus
void GeometryManager::activateWireframeForAllGeometries()
{
	for (unsigned int i = 0; i < getNumChildren(); i++)
	{
		dynamic_cast<GeometryData*>(getChild(i))->getGeometryVisualization()->selectTechnique(WIRE_SHADING);
		dynamic_cast<GeometryData*>(getChild(i))->detachFilthyLinesGeode();
		dynamic_cast<GeometryData*>(getChild(i))->detachFilthyPointsGeode();
	}
}

void GeometryManager::deactivateWireframeForAllGeometries()
{
	for (unsigned int i = 0; i < getNumChildren(); i++)
	{
		dynamic_cast<GeometryData*>(getChild(i))->getGeometryVisualization()->selectTechnique(FLAT_SHADING);
	}
}

void GeometryManager::updateWireframeStatesForAllGeometries()
{
	//std::cout << "Test 1________" << std::endl;
	if (getActiveGeometryData() != nullptr)
	{
		if (getActiveGeometryData()->getName().compare(g_stringNoSelectionAssertion) != 0 &&
			getActiveGeometryData()->getName().compare(g_stringNothingInSceneAssertion) != 0 &&
			getNumChildren() != 0)
		{
			//std::cout << "Test 2" << std::endl;
			for (unsigned int i = 0; i < getNumChildren(); i++)
			{
				if (getChild(i) != nullptr)
				{
					if (dynamic_cast<GeometryData*>(getChild(i))->getIsSelected())
					{
						dynamic_cast<GeometryData*>(getChild(i))->getGeometryVisualization()->selectTechnique(INDICATED_AS_SELECTED_SHADING);
						//std::cout << "Test 3" << std::endl;
					}
					else
					{
						dynamic_cast<GeometryData*>(getChild(i))->getGeometryVisualization()->selectTechnique(FLAT_SHADING);
						//std::cout << "Test 4" << std::endl;
					}
				}
			}

			if (m_miscDataStorage->getGlobalApplicationMode() == OBJECT_MODE)
			{
				getActiveGeometryData()->getGeometryVisualization()->selectTechnique(INDICATED_AS_SELECTED_AND_ACTIVE_SHADING);
				//std::cout << "Test 5" << std::endl;
			}
			else if (m_miscDataStorage->getGlobalApplicationMode() == EDIT_MODE)
			{
				//std::cout << "Test 6" << std::endl;
				if (getActiveGeometryData() != nullptr)
				{
					if (getActiveGeometryData()->getIsWireframeActivated())
					{
						getActiveGeometryData()->getGeometryVisualization()->selectTechnique(WIRE_SHADING);
						//std::cout << "Test 7" << std::endl;
					}
					else
					{
						getActiveGeometryData()->getGeometryVisualization()->selectTechnique(FLAT_SHADING);
						//std::cout << "Test 8" << std::endl;
					}

					getActiveGeometryData()->attachFilthyLinesGeode();
					getActiveGeometryData()->attachFilthyPointsGeode();

					//TODO: Actually call is currently only once necessary, because there ar no other render techniques for FilthyGeoms
					getActiveGeometryData()->getFilthyLinesGeometryVisualization()->selectTechnique(FILTHY_LINES);
					getActiveGeometryData()->getFilthyPointsGeometryVisualization()->selectTechnique(FILTHY_POINTS);

					getActiveGeometryData()->updateBoundingBox();
				}
			}
		}
	}
}

//TODO: After deleting nothing is selected. How to indicate if nothing is selected? GeometryManager should handle as it the top of hierachy?
//TODO: What is if nothing is selected 
//IMPORTANT: OBJECT WILL ONLY BE DELETED, IF garbageCollection() IS CALLED AFTERWARDS!
void GeometryManager::deleteGeometry(std::string name)
{
	getGeometryDataByName(name)->setIsMarkedAsDeleted(true);
}

void GeometryManager::deselectAllObjects()
{
	for (unsigned int i = 0; i < getNumChildren(); i++)
	{
		dynamic_cast<GeometryData*>(getChild(i))->setIsSelected(false);
	}
}

void GeometryManager::updateShaderAssignments()
{
	for (unsigned int i = 0; i < getNumChildren(); i++)
	{
		if (dynamic_cast<GeometryData*>(getChild(i))->getIsSelected())
		{
			dynamic_cast<GeometryData*>(getChild(i))->getGeometryVisualization()->selectTechnique(INDICATED_AS_SELECTED_SHADING);
		}
		else {
			dynamic_cast<GeometryData*>(getChild(i))->getGeometryVisualization()->selectTechnique(FLAT_SHADING);
		}
	}
	getActiveGeometryData()->getGeometryVisualization()->selectTechnique(INDICATED_AS_SELECTED_AND_ACTIVE_SHADING);
}

void GeometryManager::garbageCollection()
{
	for (unsigned int i = 0; i < getNumChildren(); ++i)
	{
		if (dynamic_cast<GeometryData*>(getChild(i))->getIsMarkedAsDeleted())
		{
			removeChild(getChild(i));
			//Recursive, because if something is deleted, the loop must be restarted, because the start/-end parameters have changed
			garbageCollection();
			return;
		}
	}
}

void GeometryManager::grabEnd(float x, float y, float z)
{

}

void GeometryManager::grabStopAndRecoverPosition()
{

}

//EXP: Get all selectedVertices as a bool vector. This is often necessary to inform OpenMesh which vertices have to be processed, e.g.: for determining the barycentrum
//void GeometryManager::setSelectionToOpenMeshStatus()
//{
//	//osg::Vec4Array *colorArray = dynamic_cast<osg::Vec4Array*>(getActiveGeometryData()->getOpenMeshGeo()->getColorArray());
//	//std::vector<bool> selectedVertices;
//
//	//for (unsigned int j = 0; j < colorArray->getNumElements(); j++)
//	//{
//	//	if (colorArray->at(j) == m_selectedColor)
//	//	{
//	//		selectedVertices.push_back(true);
//	//	}
//	//	else
//	//	{
//	//		selectedVertices.push_back(false);
//	//	}
//	//}
//	//return selectedVertices;
//
//	//NOTE: slow!
//	//Change OpenMeshGeometry.h at Line: ~66 to     inline OpenMesh::OSG_TriMesh_BindableArrayKernelT getMesh(){ return mesh; };
//	//OpenMesh::OSG_TriMesh_BindableArrayKernelT::VertexIter vIt, vBegin, vEnd;
//	//vBegin = getActiveGeometryData()->getOpenMeshGeo()->getMesh().vertices_begin();
//	//vEnd = getActiveGeometryData()->getOpenMeshGeo()->getMesh().vertices_end();
//
//	//osg::Vec4Array *colorArray = dynamic_cast<osg::Vec4Array*>(getActiveGeometryData()->getOpenMeshGeo()->getColorArray());
//
//	//int i = 0;
//	//for (vIt = vBegin; vIt != vEnd; ++vIt)
//	//{
//	//	if (colorArray->at(i) == m_selectedColor)
//	//	{
//	//		getActiveGeometryData()->getOpenMeshGeo()->getMesh().status(*vIt).set_selected(true);
//	//	}
//	//	else
//	//	{
//	//		getActiveGeometryData()->getOpenMeshGeo()->getMesh().status(*vIt).set_selected(false);
//	//	}
//	//	i++;
//	//}
//	//return selectedVertices;
//
//	//NOTE: This is fast!
//	//Change OpenMeshGeometry.h at Line: ~66 to     inline OpenMesh::OSG_TriMesh_BindableArrayKernelT* getMesh(){ return &mesh; };
//	OpenMesh::OSG_TriMesh_BindableArrayKernelT* mesh = getActiveGeometryData()->getOpenMeshGeo()->getMesh();
//
//	OpenMesh::OSG_TriMesh_BindableArrayKernelT::VertexIter vIt, vBegin, vEnd;
//	vBegin = mesh->vertices_begin();
//	vEnd = mesh->vertices_end();
//
//	osg::Vec4Array *colorArray = dynamic_cast<osg::Vec4Array*>(getActiveGeometryData()->getOpenMeshGeo()->getColorArray());
//
//	int i = 0;
//	for (vIt = vBegin; vIt != vEnd; ++vIt)
//	{
//		if (colorArray->at(i) == m_selectedColor)
//		{
//			mesh->status(*vIt).set_selected(true);
//		}
//		else
//		{
//			mesh->status(*vIt).set_selected(false);
//		}
//		i++;
//	}
//	//return selectedVertices;
//}

// EXP: Get the barycentrum of the selected geometry in local space. OpenMesh is used 
osg::Vec3d GeometryManager::getBarycentrumOfSELECTEDVertsForActiveGeomData()
{
	getActiveGeometryData()->getOpenMeshGeo()->translateSelColorToOMStatusBit();
	return m_geometryProcessor->getBarycentrumOfSELECTEDVerts();
}

osg::Vec3d GeometryManager::getBarycentrumOfALLVertsForActiveGeomData()
{
	getActiveGeometryData()->getOpenMeshGeo()->translateSelColorToOMStatusBit();
	return m_geometryProcessor->getBarycentrumOfALLVerts();
}

osg::Vec4f GeometryManager::getSelectedColor()
{
	return m_selectedColor;
}

void GeometryManager::sculptStart(float radiusIn, float strengthIn)
{
	m_geometryProcessor->sculptStart(radiusIn, strengthIn);
	updateFilthyGeometries();
}

void GeometryManager::sculptAdd(float radiusIn, float strengthIn, osg::Vec3 controllerTrans, osg::Matrix geomLocal2World)
{
	m_geometryProcessor->sculptAddOrSubtract(radiusIn, strengthIn, controllerTrans, geomLocal2World, true);
	updateFilthyGeometries();
}

void GeometryManager::sculptSubtract(float radiusIn, float strengthIn, osg::Vec3 controllerTrans, osg::Matrix geomLocal2World)
{
	m_geometryProcessor->sculptAddOrSubtract(radiusIn, strengthIn, controllerTrans, geomLocal2World, false);
	updateFilthyGeometries();
}

inline void GeometryManager::toggleSnapping()
{
	if (m_isSnappingActive == true)
	{
		m_isSnappingActive = false;
	}
	else
	{
		m_isSnappingActive = true;
	}
}

void GeometryManager::setConstraint(Constraints constraintIn)
{
	m_constraint = constraintIn;
	if (constraintIn == Constraints::NO_CONSTRAIN)
	{
		m_isContraintActiveX = false;
		m_isContraintActiveY = false;
		m_isContraintActiveZ = false;
	}
	else if (constraintIn == Constraints::X_AXIS)
	{
		m_isContraintActiveX = true;
		m_isContraintActiveY = false;
		m_isContraintActiveZ = false;
	}
	else if (constraintIn == Constraints::Y_AXIS)
	{
		m_isContraintActiveX = false;
		m_isContraintActiveY = true;
		m_isContraintActiveZ = false;
	}
	else if (constraintIn == Constraints::Z_AXIS)
	{
		m_isContraintActiveX = false;
		m_isContraintActiveY = false;
		m_isContraintActiveZ = true;
	}
}

inline void GeometryManager::toggleConstraint(Constraints constraintIn)
{
	if (constraintIn == Constraints::NO_CONSTRAIN)
	{
		m_isContraintActiveX = false;
		m_isContraintActiveY = false;
		m_isContraintActiveZ = false;
	}
	else if (constraintIn == Constraints::X_AXIS)
	{
		if (!m_isContraintActiveX)
		{
			m_constraint = Constraints::X_AXIS;
			m_isContraintActiveX = true;
			m_isContraintActiveY = false;
			m_isContraintActiveZ = false;
		}
		else
		{
			m_constraint = Constraints::NO_CONSTRAIN;
			m_isContraintActiveX = false;
			m_isContraintActiveY = false;
			m_isContraintActiveZ = false;
		}

	}
	else if (constraintIn == Constraints::Y_AXIS)
	{
		if (!m_isContraintActiveY)
		{
			m_constraint = Constraints::Y_AXIS;
			m_isContraintActiveX = false;
			m_isContraintActiveY = true;
			m_isContraintActiveZ = false;
		}
		else
		{
			m_constraint = Constraints::NO_CONSTRAIN;
			m_isContraintActiveX = false;
			m_isContraintActiveY = false;
			m_isContraintActiveZ = false;
		}

	}
	else if (constraintIn == Constraints::Z_AXIS)
	{
		if (!m_isContraintActiveZ)
		{
			m_constraint = Constraints::Z_AXIS;
			m_isContraintActiveX = false;
			m_isContraintActiveY = false;
			m_isContraintActiveZ = true;
		}
		else
		{
			m_constraint = Constraints::NO_CONSTRAIN;
			m_isContraintActiveX = false;
			m_isContraintActiveY = false;
			m_isContraintActiveZ = false;
		}

	}
	/*if (constraintIn == Constraints::GRID)
	{
		if (m_toggleConstraintGRID)
		{
			std::cout << "m_toggleConstraintGRID true" << std::endl;
			m_constraint = Constraints::GRID;
			m_toggleConstraintGRID = false;
		}
		else
		{
			std::cout << "m_toggleConstraintGRID false" << std::endl;
			m_constraint = Constraints::NO_CONSTRAIN;
			m_toggleConstraintGRID = true;
		}
	}*/
}

inline void GeometryManager::resetConstraint()
{
	m_constraint = Constraints::NO_CONSTRAIN;
	m_isContraintActiveX = false;
	m_isContraintActiveY = false;
	m_isContraintActiveZ = false;

	m_isSnappingActive = false;
}

std::string GeometryManager::checkIntersectionWithGeometry(const osg::Vec3d start, const osg::Vec3d end, bool* isIntersectionFound)
{
	osg::ref_ptr<osgUtil::LineSegmentIntersector> intersector = new osgUtil::LineSegmentIntersector(start, end);
	osgUtil::IntersectionVisitor iv(intersector.get());
	accept(iv);
	osgUtil::LineSegmentIntersector::Intersections &result = intersector->getIntersections();

	if (intersector->containsIntersections())
	{
		*isIntersectionFound = true;

		for (unsigned int i = 0; i < getNumChildren(); ++i)
		{
			*isIntersectionFound = true;
			//setActiveGeometry();
		}


		return result.begin()->drawable->getName();
	}
	else
	{
		*isIntersectionFound = false;
		//Found noting
		return "";
	}
}

bool GeometryManager::joinGeometries(osg::ref_ptr<osg::OpenMeshGeometry> geometrieToJoin)
{
	m_geometryProcessor->joinGeometries(geometrieToJoin);
	updateFilthyGeometries();
	//TODO: Remove default value and use bool parameter, if there is some useful "use"
	return true;
}

//Don't work like intended since the osg Deep Copy function is some kind of broken. If I access the deep copied object it crashes...
void GeometryManager::deepCopySelectedObjects()
{
	//Plan:
	//Look what is selected
	//Deep Copy this and add it to the scene graph
	for (unsigned int i = 0; i < getNumChildren(); i++)
	{
		if (dynamic_cast<GeometryData*>(getChild(i))->getIsSelected())
		{
			std::stringstream ss;
			ss << m_geomCounter;
			osg::ref_ptr<GeometryData> mycopy = dynamic_cast<GeometryData*>(getChild(i)->clone(osg::CopyOp::DEEP_COPY_ALL));
			//Crash in following line
			//std::cout << "Anzahl an childs von copy:" << mycopy->getNumChildren() << std::endl;
			//std::cout << "Anzahl an childs von original:" << getChild(i)->getNumChildren() << std::endl;
			mycopy->setName("geom" + ss.str());
			addChild(mycopy);
			setActiveGeometry("geom" + ss.str());
			m_geomCounter++;

			std::cout << mycopy->getName() << std::endl;
			if (ConfigReader::DEBUG_LEVEL >= NOT_IN_LOOPS)
			{
				std::cout << "GeometryCounter: " << m_geomCounter << std::endl;
			}
		}
	}
}

void GeometryManager::duplicateSelectedMeshArea()
{
	getActiveGeometryData()->getOpenMeshGeo()->translateSelColorToOMStatusBit();
	m_geometryProcessor->duplicate();
	getActiveGeometryData()->getOpenMeshGeo()->translateSelOMStatusBitToColor();
	updateFilthyGeometries();
}

//EXP: Updates vertex and color arrays and rebuilt indicies of filthy geometry  -> Changed from DisplayLists to VBOs---> much better!!!
void GeometryManager::updateFilthyGeometries()
{
	//getActiveGeometryData()->getFilthyLinesGeometry()->setVertexArray(
	//	getActiveGeometryData()->getOpenMeshGeo()->getVertexArray());

	//getActiveGeometryData()->getFilthyLinesGeometry()->setColorArray(
	//	getActiveGeometryData()->getOpenMeshGeo()->getColorArray());


	//getActiveGeometryData()->getFilthyPointsGeometry()->setVertexArray(
	//	getActiveGeometryData()->getOpenMeshGeo()->getVertexArray());

	//getActiveGeometryData()->getFilthyPointsGeometry()->setColorArray(
	//	getActiveGeometryData()->getOpenMeshGeo()->getColorArray());

	//getActiveGeometryData()->getFilthyLinesGeometry()->getVertexArray()->dirty();
	//getActiveGeometryData()->getFilthyPointsGeometry()->getVertexArray()->dirty();

	getActiveGeometryData()->rebuildFilthyLinesGeometry();
	getActiveGeometryData()->rebuildFilthyPointsGeometry();

	getActiveGeometryData()->getFilthyLinesGeometry()->getVertexArray()->dirty();
	getActiveGeometryData()->getFilthyPointsGeometry()->getVertexArray()->dirty();

	getActiveGeometryData()->getFilthyLinesGeometry()->getColorArray()->dirty();
	getActiveGeometryData()->getFilthyPointsGeometry()->getColorArray()->dirty();
}

//EXP: Updates only color arrays of filthy geometry -> Changed from DisplayLists to VBOs---> much better!!!
void GeometryManager::updateFilthyGeometriesOnlyColor()
{
	//getActiveGeometryData()->getOpenMeshGeo()->getColorArray()->dirty();

	//getActiveGeometryData()->getOpenMeshGeo()->getColorArray()->dirty();

	//getActiveGeometryData()->getFilthyLinesGeometry()->setColorArray(
	//getActiveGeometryData()->getOpenMeshGeo()->getColorArray());

	//getActiveGeometryData()->getFilthyPointsGeometry()->setColorArray(
	//	getActiveGeometryData()->getOpenMeshGeo()->getColorArray());
}

unsigned int GeometryManager::getNumberOfSelectedVerts()
{
	//PERFORMANCE: Use a static cast instead of dynamic?
	osg::Vec4Array *colorArray = dynamic_cast<osg::Vec4Array*>(getActiveGeometryData()->getOpenMeshGeo()->getColorArray());

	unsigned int numSelectedVerts = 0;
	for (unsigned int i = 0; i < getActiveGeometryData()->getOpenMeshGeo()->getVertexArray()->getNumElements(); ++i)
	{
		if (colorArray->at(i) == m_selectedColor)
		{
			++numSelectedVerts;
		}
	}

	return numSelectedVerts;
}

void GeometryManager::addEdgeBasedOnSelectedVerts()
{
	//PERFORMANCE: Use a static cast instead of dynamic?
	osg::Vec4Array *colorArray = dynamic_cast<osg::Vec4Array*>(getActiveGeometryData()->getOpenMeshGeo()->getColorArray());

	std::vector<unsigned int> edgeIndiciesToConnect;
	for (unsigned int i = 0; i < getActiveGeometryData()->getOpenMeshGeo()->getVertexArray()->getNumElements(); ++i)
	{
		if (colorArray->at(i) == m_selectedColor)
		{
			edgeIndiciesToConnect.push_back(i);
		}
	}

	getActiveGeometryData()->addFilthyEdge(edgeIndiciesToConnect.at(0), edgeIndiciesToConnect.at(1));
}

void GeometryManager::addFaceBasedOnSelectedVerts()
{
	getActiveGeometryData()->getOpenMeshGeo()->translateSelColorToOMStatusBit();
	m_geometryProcessor->addFaceBasedOnSelectedVerts();
	getActiveGeometryData()->getOpenMeshGeo()->getPrimitiveSet(0)->dirty();
}

void GeometryManager::cancelAnyTransformation()
{
	m_geometryProcessor->cancelAnyTransformation();
	updateFilthyGeometries();
}

void GeometryManager::storeMeshDataAsHistory()
{
	m_geometryProcessor->storeMeshDataAsHistory();
}

void GeometryManager::undo()
{
	m_geometryProcessor->undo();
}



