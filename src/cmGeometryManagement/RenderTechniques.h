// ClayMore - Immersive Mesh Modelling --- Copyright (c) 2014-2017 Philipp Ladwig, Jannik Fiedler, Jan Beutgen
#pragma once

#include "ConfigDllExportGeometryManagement.h"

#include "cmUtil/ConfigReader.h"

#include <osg/LineWidth>
#include <osg/Point>
#include <osg/Material>
#include <osg/ShadeModel>
#include <osg/PolygonOffset>
#include <osg/PolygonMode>

#include <osgFX/Effect>
#include <osgFX/Registry>

#include <iostream>
#include <osgShadow\ShadowMap>



class ConfigReader;

bool loadShaderSource(osg::Shader* obj, const std::string& fileName);

//################################################################################
//EXP: EditMode technique class to visualize wireframe as GL_LINES and vertices as GL_POINTS
class FlatAndWireShading : public osgFX::Technique {
public:
	FlatAndWireShading(ConfigReader* configManagerIn) : m_configReader(configManagerIn) {}

	//TODO: A validation of the used OpenGL Version can be performed. Will it be usefull in future?
	//bool validate(osg::State&) const
	//{
	//	return strncmp((const char*)glGetString(GL_VERSION), "1.1", 3) >= 0;
	//}

protected:

	void define_passes();

private:
	ConfigReader* m_configReader;
	osg::ref_ptr<osg::Material> m_surfaceMaterial;
	osg::ref_ptr<osg::LineWidth> m_wireframeLineWidth;
	float m_pointSize;
};

//################################################################################
//EXP: EditMode technique class to visualize wireframe as GL_LINES and vertices as GL_POINTS
class FlatAndWireShadowShading : public osgFX::Technique {
public:
	FlatAndWireShadowShading(ConfigReader* configManagerIn, osg::ref_ptr<osgShadow::ShadowMap> sm) : m_configReader(configManagerIn), m_shadowMap(sm) {}

	//TODO: A validation of the used OpenGL Version can be performed. Will it be usefull in future?
	//bool validate(osg::State&) const
	//{
	//	return strncmp((const char*)glGetString(GL_VERSION), "1.1", 3) >= 0;
	//}

protected:

	void define_passes();

private:
	ConfigReader* m_configReader;
	osg::ref_ptr<osg::Material> m_surfaceMaterial;
	osg::ref_ptr<osg::LineWidth> m_wireframeLineWidth;
	osg::ref_ptr<osgShadow::ShadowMap> m_shadowMap;
	float m_pointSize;
};

//################################################################################
class WireShading : public osgFX::Technique {
public:
	WireShading(ConfigReader* configManagerIn) : m_configReader(configManagerIn) {}

protected:

	void define_passes();

private:
	ConfigReader* m_configReader;
	osg::ref_ptr<osg::LineWidth> m_wireframeLineWidth;
	float m_pointSize;

};

//################################################################################
// technique for geometries which are not be selected
class FlatShading : public osgFX::Technique {
public:
	FlatShading(ConfigReader* configManagerIn) : m_configReader(configManagerIn) {}

protected:

	void define_passes();

private:
	ConfigReader* m_configReader;
	osg::ref_ptr<osg::Material> m_surfaceMaterial;
};

//################################################################################
// technique for geometries which are not be selected
class FlatShadowShading : public osgFX::Technique {
public:
	FlatShadowShading(ConfigReader* configManagerIn, osg::ref_ptr<osgShadow::ShadowMap> sm, osg::Matrix depthMVP)
		: m_configReader(configManagerIn), m_shadowMap(sm), m_depthMVP(depthMVP){}

protected:

	void define_passes();

private:
	ConfigReader* m_configReader;
	osg::ref_ptr<osg::Material> m_surfaceMaterial;
	osg::ref_ptr<osgShadow::ShadowMap> m_shadowMap;
	osg::Matrix m_depthMVP;
};

//################################################################################
// technique for geometries which are in prospect for selecting
class IndicatedAsSelectedShading : public osgFX::Technique {
public:
	IndicatedAsSelectedShading(ConfigReader* configManagerIn) : m_configReader(configManagerIn) {}

protected:

	void define_passes();
	
private:
	ConfigReader* m_configReader;
	osg::ref_ptr<osg::Material> m_surfaceMaterial;
	osg::ref_ptr<osg::Material> m_wireframeMaterial;
	osg::ref_ptr<osg::LineWidth> m_wireframeLineWidth;
};

//################################################################################
// technique for geometries which are selected and active -> Last selected object
class IndicatedAsSelectedAndActiveShading : public osgFX::Technique {
public:
	IndicatedAsSelectedAndActiveShading(ConfigReader* configManagerIn) : m_configReader(configManagerIn) {}

protected:

	void define_passes();

private:
	ConfigReader* m_configReader;
	osg::ref_ptr<osg::Material> m_surfaceMaterial;
	osg::ref_ptr<osg::Material> m_wireframeMaterial;
	osg::ref_ptr<osg::LineWidth> m_wireframeLineWidth;
};

//################################################################################
// technique for rendering lines and "filthy" lines
class FilthyLinesShading : public osgFX::Technique {
public:
	FilthyLinesShading(ConfigReader* configManagerIn) : m_configReader(configManagerIn) {}

protected:

	void define_passes();

private:
	ConfigReader* m_configReader;
	//osg::ref_ptr<osg::Material> m_surfaceMaterial;
	//osg::ref_ptr<osg::Material> m_wireframeMaterial;
	//osg::ref_ptr<osg::LineWidth> m_wireframeLineWidth;
};

//################################################################################
// technique for rendering points and "filthy" points
class FilthyPointsShading : public osgFX::Technique {
public:
	FilthyPointsShading(ConfigReader* configManagerIn) : m_configReader(configManagerIn) {}

protected:

	void define_passes();

private:
	ConfigReader* m_configReader;
	//osg::ref_ptr<osg::Material> m_surfaceMaterial;
	//osg::ref_ptr<osg::Material> m_wireframeMaterial;
	//osg::ref_ptr<osg::LineWidth> m_wireframeLineWidth;
};