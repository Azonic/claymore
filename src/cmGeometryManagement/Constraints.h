// ClayMore - Immersive Mesh Modelling --- Copyright (c) 2014-2017 Philipp Ladwig, Jannik Fiedler, Jan Beutgen
#pragma once
enum Constraints
{
	NO_CONSTRAIN,
	X_AXIS,
	Y_AXIS,
	Z_AXIS
};
