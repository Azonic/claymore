// ClayMore - Immersive Mesh Modelling --- Copyright (c) 2014-2017 Philipp Ladwig, Jannik Fiedler, Jan Beutgen
#ifndef cmOpenMeshBinding_EXPORTS
	#define imOpenMeshBinding_DLL_import_export __declspec(dllimport)
#else
	#define imOpenMeshBinding_DLL_import_export __declspec(dllexport)
#endif