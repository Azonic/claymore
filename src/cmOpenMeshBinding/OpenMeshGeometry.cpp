// ClayMore - Immersive Mesh Modelling --- Copyright (c) 2014-2017 Philipp Ladwig, Jannik Fiedler, Jan Beutgen
#include "OpenMeshGeometry.h"
#include "OSG_PropertyT.h" 
#include <OpenMesh/Core/Mesh/TriMeshT.hh>

#include "../cmUtil/ConfigReader.h"

#include <iostream>
#include <fstream>

using namespace osg;

OpenMeshGeometry::OpenMeshGeometry(enum GeometryType geometryType, ConfigReader* configReader, std::string name) : Geometry()
{
	m_geometryType = m_geometryType;
	m_configReader = configReader;
	setName(name);

	setUseDisplayList(false);
	setUseVertexBufferObjects(true);
	setDataVariance(DYNAMIC);

	osg::Vec3f m_grabStartPosition = osg::Vec3f();
	//EXP: Used for restore the original topology of the mesh
	//--m_initialTopologyAtStartOfTransform = new Vec3Array();

	m_selectedColor.set(0.0f, 0.0f, 0.0f, 0.0f);
	m_notSelectedColor.set(0.0f, 0.0f, 0.0f, 0.0f);

	m_selectedColor = m_configReader->getVec4fFromStartupConfig("color_vertex_selected");
	m_selectedAndInProspect = m_configReader->getVec4fFromStartupConfig("color_vertex_selected_and_prospect");
	m_notSelectedColor = m_configReader->getVec4fFromStartupConfig("color_vertex_not_selected");

	mesh.request_face_normals();
	mesh.request_vertex_normals();
	//mesh.request_vertex_texcoords2D();
}

OpenMeshGeometry::~OpenMeshGeometry()
{
}

void OpenMeshGeometry::setVertexArray(Array* array)
{
	//Geometry is the base class
	Geometry::setVertexArray(array);
	VertexBufferObject *vbo = getOrCreateVertexBufferObject();
	//EXP: STREAM - STATIC - DYNAMIC as options ->Look OpenGL API (Maybe DYNAMIC will be a better choice)
	//Original:
	//vbo->setUsage(GL_STREAM_DRAW);
	//Changed at 020317 to:
	vbo->setUsage(GL_DYNAMIC_DRAW);

	Vec3Array *vertexArray = dynamic_cast<Vec3Array*>(array);
	if (vertexArray)
	{
		mesh.resize(vertexArray->size(), 0, 0);
		mesh.bind<Vec3>(Mesh::VProp, "v:points", *vertexArray);
	}

}

void OpenMeshGeometry::setTexCoordArray(unsigned int unit, Array* array)
{
	//Geometry is the base class
	Geometry::setTexCoordArray(unit, array);
	//Vec2Array *texCoordArray = dynamic_cast<Vec2Array*>(array);
	//if (texCoordArray)
	//{
	//	mesh.request_vertex_texcoords2D();
	//	mesh.bind<Vec2>(Mesh::VProp, "v:texcoords2D", *texCoordArray);
	//}
}

void OpenMeshGeometry::setColorArray(Array* array)
{
	//Geometry is the base class
	Geometry::setColorArray(array);
	Vec4Array *colorArray = dynamic_cast<Vec4Array*>(array);
	if (colorArray)
	{
		mesh.request_vertex_colors();
		mesh.bind<Vec4>(Mesh::VProp, "v:colors", *colorArray);
	}
}

bool OpenMeshGeometry::addPrimitiveSet(PrimitiveSet* primitiveSet)
{
	if (primitiveSet->getType() != PrimitiveSet::DrawElementsUIntPrimitiveType)
	{
		throw std::exception("Primitive type not supported!");
	}
	//Geometry is the base class
	if (Geometry::addPrimitiveSet(primitiveSet))
	{
		//PL: Error prone??!
		DrawElementsUInt* indices = dynamic_cast<DrawElementsUInt*>(primitiveSet);

		for (unsigned int i = 0; i < indices->getNumIndices(); ++i)
		{
			OpenMesh::VertexHandle v1((*indices)[i]);
			++i;
			OpenMesh::VertexHandle v2((*indices)[i]);
			++i;
			OpenMesh::VertexHandle v3((*indices)[i]);
			mesh.add_face(v1, v2, v3);
		}
		mesh.bind<GLuint>(Mesh::FProp, "indices", *indices);


		osg::ref_ptr<Vec3Array> normalArray = new Vec3Array();
		//Geometry is the base class
		Geometry::setNormalArray(normalArray);
		Geometry::setNormalBinding(osg::OpenMeshGeometry::BIND_PER_VERTEX);
		mesh.bind<Vec3>(Mesh::VProp, "v:normals", *normalArray);
		mesh.garbage_collection(true, true, true);
		mesh.update_normals();

		//Test addfilthyGeom
		//DrawElementsUInt* filthyPrimitiveSet = new osg::DrawElementsUInt(osg::PrimitiveSet::LINES, 0);
		//filthyPrimitiveSet->push_back(0);
		//filthyPrimitiveSet->push_back(1);
		////filthyPrimitiveSet->push_back(2);
		////filthyPrimitiveSet->push_back(3);
		//Geometry::addPrimitiveSet(filthyPrimitiveSet);

		return true;
	}
	return false;
}

//mesh.bind<GLuint>(Mesh::EProp, "indices", *indices);

void OpenMeshGeometry::translateSelColorToOMStatusBit()
{
	//std::cout << "translateSelColorToOMStatusBit" << std::endl;
	Mesh::VertexIter vIt, vBegin, vEnd;
	vBegin = mesh.vertices_begin();
	vEnd = mesh.vertices_end();

	osg::Vec4Array *colorArray = dynamic_cast<osg::Vec4Array*>(getColorArray());

	int i = 0;
	for (vIt = vBegin; vIt != vEnd; ++vIt)
	{
		if (colorArray->at(i) == m_selectedColor || colorArray->at(i) == m_selectedAndInProspect)
		{
			mesh.status(*vIt).set_selected(true);
		}
		else
		{
			mesh.status(*vIt).set_selected(false);
		}
		i++;
	}

	//Select Faces
	Mesh::FaceIter fIt, fBegin, fEnd;
	fBegin = mesh.faces_begin();
	fEnd = mesh.faces_end();

	//Iterate through all faces....
	for (fIt = fBegin; fIt != fEnd; ++fIt)
	{
		//...and iterate over all vertices of each face....
		int i = 0;

		//Old version with compiler warning:
		//Mesh::FaceVertexIter fvIter = mesh.fv_iter(*fIt);
		//for (; fvIter; ++fvIter)
		Mesh::FaceVertexIter fvIter = mesh.fv_iter(*fIt);
		Mesh::FaceVertexIter fvEnd = mesh.fv_end(*fIt);
		for (; fvIter!= fvEnd; ++fvIter)
		{
			//...and look if the vertex is selected. If yes, increase "i".
			if (mesh.status(*fvIter).selected())
			{
				//std::cout << "Selektiertes Vertex von Face erkannt" << std::endl;
				++i;
			}
		}

		//If there 3 vertices are selected, the face will be also marked as selected
		if (i == 3)
		{
			//std::cout << "Und jetzt wirds face selektiert" << std::endl;
			mesh.status(*fIt).set_selected(true);
		}
		else
		{
			mesh.status(*fIt).set_selected(false);
		}

	}
}

void OpenMeshGeometry::translateSelOMStatusBitToColor()
{
	Mesh::VertexIter vIt, vBegin, vEnd;
	vBegin = mesh.vertices_begin();
	vEnd = mesh.vertices_end();

	osg::Vec4Array *colorArray = dynamic_cast<osg::Vec4Array*>(getColorArray());

	int i = 0;
	for (vIt = vBegin; vIt != vEnd; ++vIt)
	{
		if (mesh.status(*vIt).selected())
		{
			colorArray->at(i) = m_selectedColor;
		}
		else
		{
			colorArray->at(i) = m_notSelectedColor;
		}
		i++;
	}
	colorArray->dirty();
}