// ClayMore - Immersive Mesh Modelling --- Copyright (c) 2014-2017 Philipp Ladwig, Jannik Fiedler, Jan Beutgen
#pragma once

#include "ConfigDllExportOpenMesh.h"

#include <OSG/Geometry>
#include "TriMesh_BindableAttribKernelT.h"

class ConfigReader;

enum GeometryType
{
	MODELLING,
	POINT_CLOUD
};

namespace osg {
	class imOpenMeshBinding_DLL_import_export OpenMeshGeometry : public Geometry
	{
	// #### CONSTRUCTOR & DESTRUCTOR ###############
	public:
		
		OpenMeshGeometry(enum GeometryType geometryType, ConfigReader* configReader, std::string name);
		~OpenMeshGeometry();

	// #### MEMBER VARIABLES ###############
	protected:
		typedef OpenMesh::OSG_TriMesh_BindableArrayKernelT Mesh;
		Mesh mesh;
		
	// #### MEMBER FUNCTIONS ###############
	public:
		// binds the a vertex array to the internal OpenMesh mesh
		void setVertexArray(Array* array);

		// binds a color array to the internal OpenMesh mesh
		void setColorArray(Array* array); 

		// binds a TexCoord array to the internal OpenMesh mesh - first param is texture unit, second texture coordinates array
		void setTexCoordArray(unsigned int unit, Array* array); 

		// Trouble with this function -> currently works only with osg::DrawElementsUInt with TRIANGLE primitive type
		// Work in progress with other Primitive types and multiple primitive sets attached to one geometry
		bool addPrimitiveSet(PrimitiveSet* primitiveset);

		inline OpenMesh::OSG_TriMesh_BindableArrayKernelT* getMesh() { return &mesh; };

		void translateSelColorToOMStatusBit();
		void translateSelOMStatusBitToColor();

		inline enum GeometryType getGeometryType() { return m_geometryType; }
		inline void setGeometryType(enum GeometryType geometryType) { m_geometryType = geometryType; }

	private:
		enum GeometryType m_geometryType;
		ConfigReader* m_configReader;
		osg::Vec4f m_selectedColor, m_selectedAndInProspect, m_notSelectedColor;
	};

}