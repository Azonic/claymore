// ClayMore - Immersive Mesh Modelling --- Copyright (c) 2014-2017 Philipp Ladwig, Jannik Fiedler, Jan Beutgen
#include "ViveController.h"
#include <iostream>

#include "cmCore\defines.h"

#include "cmUtil\Textcreator.h"

ViveController::ViveController(CONTROLLER_TYPE controllerType, vr::IVRSystem *ivrSystem) :
	Controller(-1, controllerType),
	m_ivrSystem(ivrSystem),
	m_lastButton(VIVE_CONTROLLER_NO_BUTTON)
{
	if (!m_controllerModel) {
		if (m_controllerType == PRIMARY)
		{
			m_controllerModel = osgDB::readNodeFile("./data/controller_models/vr_controller_vive_1_5_without_tracking_donut.obj");
		}
		else if (m_controllerType == SECONDARY)
		{
			m_controllerModel = osgDB::readNodeFile("./data/controller_models/vr_controller_vive_1_5.obj");
		}
		else if (m_controllerType == GENERIC_TRACKER)
		{
			m_controllerModel = osgDB::readNodeFile("./data/controller_models/generic_tracker.obj");
		}
	}

	if (!m_controllerModel)
	{
		osg::notify(osg::ALWAYS) << "Error in ViveController::ViveController: Didn't find controller model" << std::endl;
	}

	//Help Text  and Indication Stuff #####
	m_helMatTransform = new osg::MatrixTransform();
	m_helpTextGeode = new osg::Geode();
	m_helMatTransform->addChild(m_helpTextGeode);
	osg::Matrix matrix;
	matrix.makeRotate(M_PI, 1.0f, 0.0f, 0.0f);
	m_helMatTransform->setMatrix(matrix);
	m_helpCharakterSize = 0.01;

	m_linesForDescriptionText = new osg::Geometry();
	m_helpTextGeode->addDrawable(m_linesForDescriptionText);
	osg::ref_ptr<osg::Vec3Array> lineVertices = new osg::Vec3Array;
	osg::Vec3f firstVertex;
	osg::Vec3f secondVertex;
	osg::Vec4 color = osg::Vec4(1.0f, 0.0f, 0.0f, 1.0f);
	osg::ref_ptr<osg::DrawElementsUInt> line = new osg::DrawElementsUInt(osg::PrimitiveSet::LINES, 0);
	osg::Vec4Array* colorArray = new osg::Vec4Array;

	//Indication Stuff
	m_helpTextIndicationGeode = new osg::Geode();
	m_helMatTransform->addChild(m_helpTextIndicationGeode);
	m_linesForIndicationText = new osg::Geometry();
	m_helpTextIndicationGeode->addDrawable(m_linesForIndicationText);
	m_helpTextIndicationGeode->getOrCreateStateSet()->setMode(GL_LIGHTING, osg::StateAttribute::OVERRIDE | osg::StateAttribute::OFF);
	m_lineVerticesForIndication = new osg::Vec3Array;
	m_linesForIndicationText->setVertexArray(m_lineVerticesForIndication);
	m_linesForIndication = new osg::DrawElementsUInt(osg::PrimitiveSet::LINES, 0);
	m_colorArrayForIndication = new osg::Vec4Array;
	m_linesForIndicationText->setColorArray(m_colorArrayForIndication);
	m_linesForIndicationText->setColorBinding(osg::Geometry::BIND_PER_VERTEX);


	m_linesForIndicationText->setUseDisplayList(false);
	m_linesForIndicationText->setUseVertexBufferObjects(true);
	m_linesForIndicationText->setDataVariance(osg::Object::DYNAMIC);
	//Only the primary and secondary controller get help text. If the tracker get m_helMatTransform as a child there must be also assiged 
	// drawables, verts and so far to it (which is made for the controllers, but not for the tracker) Otherwise it crashes in Debug Mode because there
	// is an empty primitive set
	if (m_controllerType == PRIMARY || m_controllerType == SECONDARY)
	{
		m_controllerTransform->addChild(m_helMatTransform);
	}

	//Controller Coord. System for m_helMatTransform (mind a 180 degree rotation around x from the originaly coord sys) (Sketch is a vive controller, not a penis!)
	//
	//( O )	  y	O------> x			y axis goes from TouchPad to Trigger positiv
	// |o|		|
	// | |		|
	// \ /	  z	\/ 

	//Only Help Text 
	if (m_controllerType == PRIMARY)
	{
		m_helpTextGeode->addDrawable(TextCreator::createText(osg::Vec3(0.092f, 0.02f, -0.08f), "SQUEEZE", m_helpCharakterSize, osgText::TextBase::LEFT_CENTER));
		firstVertex.set(osg::Vec3(0.09f, 0.02f, -0.08f));
		secondVertex.set(osg::Vec3(0.01f, 0.015f, -0.095f));
		lineVertices->push_back(firstVertex);
		lineVertices->push_back(secondVertex);

		m_helpTextGeode->addDrawable(TextCreator::createText(osg::Vec3(-0.102f, 0.03f, -0.0f), "TRIGGER", m_helpCharakterSize, osgText::TextBase::RIGHT_CENTER));
		firstVertex.set(osg::Vec3(-0.1f, 0.03f, -0.0f));
		secondVertex.set(osg::Vec3(0.0f, 0.035f, -0.05f));
		lineVertices->push_back(firstVertex);
		lineVertices->push_back(secondVertex);

		//m_helpTextGeode->addDrawable(TextCreator::createText(osg::Vec3(0.042f, -0.04f, 0.04f), "LAST TOOL", m_helpCharakterSize, osgText::TextBase::LEFT_CENTER));
		//firstVertex.set(osg::Vec3(0.04f, -0.04f, 0.04f));
		//secondVertex.set(osg::Vec3(0.0f, -0.01f, -0.02f));
		//lineVertices->push_back(firstVertex);
		//lineVertices->push_back(secondVertex);

		m_linesForDescriptionText->setVertexArray(lineVertices);
	}
	if (m_controllerType == SECONDARY)
	{
		m_helpTextGeode->addDrawable(TextCreator::createText(osg::Vec3(-0.092f, 0.02f, -0.08f), "SQUEEZE", m_helpCharakterSize, osgText::TextBase::RIGHT_CENTER));
		firstVertex.set(osg::Vec3(-0.09f, 0.02f, -0.08f));
		secondVertex.set(osg::Vec3(-0.01f, 0.015f, -0.095f));
		lineVertices->push_back(firstVertex);
		lineVertices->push_back(secondVertex);

		m_helpTextGeode->addDrawable(TextCreator::createText(osg::Vec3(0.102f, 0.03f, -0.0f), "TRIGGER", m_helpCharakterSize, osgText::TextBase::LEFT_CENTER));
		firstVertex.set(osg::Vec3(0.1f, 0.03f, -0.0f));
		secondVertex.set(osg::Vec3(0.0f, 0.035f, -0.05f));
		lineVertices->push_back(firstVertex);
		lineVertices->push_back(secondVertex);

		//m_helpTextGeode->addDrawable(TextCreator::createText(osg::Vec3(-0.042f, -0.04f, 0.04f), "MAIN MENU", m_helpCharakterSize, osgText::TextBase::RIGHT_CENTER));
		//firstVertex.set(osg::Vec3(-0.04f, -0.04f, 0.04f));
		//secondVertex.set(osg::Vec3(0.0f, -0.01f, -0.02f));
		//lineVertices->push_back(firstVertex);
		//lineVertices->push_back(secondVertex);

		m_linesForDescriptionText->setVertexArray(lineVertices);
	}


	m_helpTextGeode->getOrCreateStateSet()->setMode(GL_LIGHTING, osg::StateAttribute::OVERRIDE | osg::StateAttribute::OFF);
	color = osg::Vec4(1.0f, 0.0f, 0.0f, 1.0f);
	//std::cout << "lineVertices: " << lineVertices->size() << std::endl;
	for (int i = 0; i < lineVertices->size(); i++)
	{
		line->push_back(i);
		i++;
		line->push_back(i);

		colorArray->push_back(color);
		colorArray->push_back(color);
	}
	m_linesForDescriptionText->addPrimitiveSet(line);
	m_linesForDescriptionText->setColorArray(colorArray);
	m_linesForDescriptionText->setColorBinding(osg::Geometry::BIND_PER_VERTEX);

}

ViveController::~ViveController()
{

}

//axisID: 0 -> Touchpad, 1 -> Trigger
//duration in miliseconds
void ViveController::triggerHapticEvent(HAPTIC_EVENT hapticEvent)
{
	switch (hapticEvent)
	{
	case JOYSTICK_SHORT:

		m_ivrSystem->TriggerHapticPulse(m_controllerID, 0, 2500);

		break;

	case JOYSTICK_LONG:

		m_ivrSystem->TriggerHapticPulse(m_controllerID, 0, 3500);

		break;

	case JOYSTICK_TINY:

		m_ivrSystem->TriggerHapticPulse(m_controllerID, 0, 400);

		break;

	case TRIGGER_SHORT:

		m_ivrSystem->TriggerHapticPulse(m_controllerID, 1, 2500);

		break;

	case TRIGGER_LONG:

		m_ivrSystem->TriggerHapticPulse(m_controllerID, 1, 3500);

		break;

	case TRIGGER_TINY:

		m_ivrSystem->TriggerHapticPulse(m_controllerID, 1, 400);

		break;

	default:
		break;
	}

}


void ViveController::renderHelpText()
{
	//If there are more children attached to m_helMatTransform than 1, don't do anything
	if (m_helMatTransform->getNumChildren() < 1)
	{
		m_helMatTransform->addChild(m_helpTextGeode);
	}
}

void ViveController::removeHelpText()
{
	if (m_helMatTransform->getNumChildren() > 0)
	{
		m_helMatTransform->removeChild(m_helpTextGeode);
	}
}

//Sets the controller ID by getting the right IDs directly from OpenVR (internal Controller IDs can vary)
void ViveController::setControllerIDByOpenVR()
{
	//OBSCURE and TODO: It is a "official" bug in OpenVR, that a tracker is marked as controller, when no other controllers are connected...
	// Google for more info: openVR Tracker identified as controller
	//It is possible to uniquely identify a tracker with its serial number -> serial number could be written in config.xml... ugly but it would work
	//Another solution is to set the tracker in SteamVR under "Burger Menu"->Devices->Manage Vive Trackers from "held in hand" in disabled
	unsigned int skipPrimary = 0;
	for (vr::TrackedDeviceIndex_t unDevice = vr::k_unTrackedDeviceIndex_Hmd + 1; unDevice < vr::k_unMaxTrackedDeviceCount; ++unDevice)
	{
		if (m_ivrSystem->GetTrackedDeviceClass(unDevice) == vr::TrackedDeviceClass_Controller)
		{
			skipPrimary++;
			if (m_controllerType == CONTROLLER_TYPE::PRIMARY)
			{
				m_controllerID = unDevice;
				//std::cout << "CONTROLLER_TYPE::PRIMARY" << unDevice << std::endl;
				skipPrimary++;
				return;
			}
			else if (m_controllerType == CONTROLLER_TYPE::SECONDARY && skipPrimary == 2)
			{
				m_controllerID = unDevice;
				//std::cout << "CONTROLLER_TYPE::SECONDARY" << unDevice << std::endl;
				skipPrimary++;
				return;
			}
		}
		else if (m_ivrSystem->GetTrackedDeviceClass(unDevice) == vr::TrackedDeviceClass_GenericTracker && m_controllerType == CONTROLLER_TYPE::GENERIC_TRACKER)
		{
			m_controllerID = unDevice;
			//std::cout << "CONTROLLER_TYPE::GENERIC_TRACKER" << unDevice << std::endl;
			return;
		}

	}
}

void ViveController::update(void *controllerData)
{
	vr::HmdMatrix34_t* pose = static_cast<vr::HmdMatrix34_t*>(controllerData);

	m_position.set(pose->m[0][3], pose->m[1][3], pose->m[2][3]);
	m_controllerTransform->setMatrix(osg::Matrix(
		pose->m[0][0], pose->m[1][0], pose->m[2][0], 0.0,
		pose->m[0][1], pose->m[1][1], pose->m[2][1], 0.0,
		pose->m[0][2], pose->m[1][2], pose->m[2][2], 0.0,
		pose->m[0][3], pose->m[1][3], pose->m[2][3], 1.0f
	));
	m_rotation.set(m_controllerTransform->getMatrix());

	vr::VRControllerState_t state;
	if (m_ivrSystem->GetControllerState(m_controllerID, &state, sizeof(state)))
	{
		VIVE_BUTTON_ID currentButton = static_cast<VIVE_BUTTON_ID>(state.ulButtonPressed);

		BUTTON_STATE triggerButtonState = getButtonState(REAR_TRIGGER);
		if (currentButton & VIVE_BUTTON_ID::VIVE_CONTROLLER_REAR_TRIGGER) //TODO: Trigger only triggers when it is "fully" pressed
			updateButtonState(REAR_TRIGGER, triggerButtonState == HOLD || triggerButtonState == PUSH ? HOLD : PUSH);

		else
			updateButtonState(REAR_TRIGGER, triggerButtonState == IDLE || triggerButtonState == RELEASE ? IDLE : RELEASE);


		BUTTON_STATE joystickButtonState = getButtonStateForAlteration(JOYSTICK);
		if (currentButton & VIVE_BUTTON_ID::VIVE_CONTROLLER_JOYSTICK)
			updateButtonState(JOYSTICK, joystickButtonState == HOLD || joystickButtonState == PUSH ? HOLD : PUSH);

		else
			updateButtonState(JOYSTICK, joystickButtonState == IDLE || joystickButtonState == RELEASE ? IDLE : RELEASE);

		BUTTON_STATE menuButtonState = getButtonStateForAlteration(MENU);
		if (currentButton & VIVE_BUTTON_ID::VIVE_CONTROLLER_MENU_BUTTON)
			updateButtonState(MENU, menuButtonState == HOLD || menuButtonState == PUSH ? HOLD : PUSH);

		else
			updateButtonState(MENU, menuButtonState == IDLE || menuButtonState == RELEASE ? IDLE : RELEASE);

		BUTTON_STATE squeezeButtonState = getButtonStateForAlteration(SQUEEZE);
		if (currentButton & VIVE_BUTTON_ID::VIVE_CONTROLLER_SQUEEZE_BUTTON)
			updateButtonState(SQUEEZE, squeezeButtonState == HOLD || squeezeButtonState == PUSH ? HOLD : PUSH);

		else
			updateButtonState(SQUEEZE, squeezeButtonState == IDLE || squeezeButtonState == RELEASE ? IDLE : RELEASE);

		m_joystickX = state.rAxis[0].x;
		m_joystickY = state.rAxis[0].y;
	}
}

void ViveController::updateIndicationGeometry(bool IsTriggerLeftIndicated, bool IsTriggerRightIndicated, bool IsSqueezeLeftIndicated, bool IsSqueezeRightIndicated)
{
	m_helpTextIndicationGeode->removeDrawables(0, m_helpTextIndicationGeode->getNumDrawables());
	m_linesForIndication->clear();
	m_lineVerticesForIndication->clear();
	m_colorArrayForIndication->clear();

	osg::Vec3f firstVertex;
	osg::Vec3f secondVertex;
	osg::Vec4 color = osg::Vec4(1.0f, 0.0f, 0.0f, 1.0f);

	//Only Indication Stuff
	if (m_controllerType == PRIMARY)
	{
		if (IsSqueezeLeftIndicated)
		{
			m_helpTextIndicationGeode->addDrawable(TextCreator::createText(osg::Vec3(0.092f, 0.02f, -0.08f), "SQUEEZE", m_helpCharakterSize, osgText::TextBase::LEFT_CENTER));
			firstVertex.set(osg::Vec3(0.09f, 0.02f, -0.08f));
			secondVertex.set(osg::Vec3(0.01f, 0.015f, -0.095f));
			m_lineVerticesForIndication->push_back(firstVertex);
			m_lineVerticesForIndication->push_back(secondVertex);
		}

		if (IsTriggerLeftIndicated)
		{
			m_helpTextIndicationGeode->addDrawable(TextCreator::createText(osg::Vec3(-0.102f, 0.03f, -0.0f), "TRIGGER", m_helpCharakterSize, osgText::TextBase::RIGHT_CENTER));
			firstVertex.set(osg::Vec3(-0.1f, 0.03f, -0.0f));
			secondVertex.set(osg::Vec3(0.0f, 0.035f, -0.05f));
			m_lineVerticesForIndication->push_back(firstVertex);
			m_lineVerticesForIndication->push_back(secondVertex);
		}
	}

	if (m_controllerType == SECONDARY)
	{
		if (IsSqueezeRightIndicated)
		{
			m_helpTextIndicationGeode->addDrawable(TextCreator::createText(osg::Vec3(-0.092f, 0.02f, -0.08f), "SQUEEZE", m_helpCharakterSize, osgText::TextBase::RIGHT_CENTER));
			firstVertex.set(osg::Vec3(-0.09f, 0.02f, -0.08f));
			secondVertex.set(osg::Vec3(-0.01f, 0.015f, -0.095f));
			m_lineVerticesForIndication->push_back(firstVertex);
			m_lineVerticesForIndication->push_back(secondVertex);
		}

		if (IsTriggerRightIndicated)
		{
			m_helpTextIndicationGeode->addDrawable(TextCreator::createText(osg::Vec3(0.102f, 0.03f, -0.0f), "TRIGGER", m_helpCharakterSize, osgText::TextBase::LEFT_CENTER));
			firstVertex.set(osg::Vec3(0.1f, 0.03f, -0.0f));
			secondVertex.set(osg::Vec3(0.0f, 0.035f, -0.05f));
			m_lineVerticesForIndication->push_back(firstVertex);
			m_lineVerticesForIndication->push_back(secondVertex);
		}
	}

	for (int i = 0; i < m_lineVerticesForIndication->size(); i++)
	{
		m_linesForIndication->push_back(i);
		i++;
		m_linesForIndication->push_back(i);

		m_colorArrayForIndication->push_back(color);
		m_colorArrayForIndication->push_back(color);
	}

	m_linesForIndicationText->addPrimitiveSet(m_linesForIndication);

	m_linesForIndicationText->setVertexArray(m_lineVerticesForIndication);
	m_linesForIndicationText->setColorArray(m_colorArrayForIndication);
	m_linesForIndicationText->setColorBinding(osg::Geometry::BIND_PER_VERTEX);

	m_helpTextIndicationGeode->addDrawable(m_linesForIndicationText);

	m_linesForIndication->dirty();
	m_lineVerticesForIndication->dirty();
	m_colorArrayForIndication->dirty();
}