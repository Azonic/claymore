// ClayMore - Immersive Mesh Modelling --- Copyright (c) 2014-2017 Philipp Ladwig, Jannik Fiedler, Jan Beutgen
#pragma once

#include "cmOsgOpenVRViewer/openvrdevice.h"
#include "ViveController.h"
#include "GraphicsWindowViewer.h"

#include "cmUtil/TimedOpenVRMatrix.h"
#include <boost/circular_buffer.hpp>

#include <atomic>

static struct test {
	int zahl;
};

class OpenVRConnection : public OpenVRDevice
{
public:
	OpenVRConnection(float nearClip, float farClip, const float worldUnitsPerMetre = 1.0f, const int samples = 0);
	~OpenVRConnection();

	void handleInput(std::vector<Controller*> const & primaryControllers,
		std::vector<Controller*> const & secondaryControllers,
		std::vector<Controller*> const & genericTrackerControllers);

	vr::IVRSystem* getIVRSystem() { return m_vrSystem; }

	inline boost::circular_buffer<TimedOpenVRMatrix>* getTimedCircularTrackerMatrixBuffer()
	{
		return m_timedCircularTrackerMatrixBuffer;
	};

	inline std::atomic<bool>* getTimedCircularTrackerMatrixBuffer_Lock()
	{
		return &m_timedCircularTrackerMatrixBuffer_Lock;
	};
	

private:
	vr::TrackedDevicePose_t m_trackedDevicePose[vr::k_unMaxTrackedDeviceCount];
	unsigned short m_numberOfLoopsWithAmissigController;
	unsigned int m_numberOfConnectedDevicesInLastFrame;
	
	std::atomic<bool> m_timedCircularTrackerMatrixBuffer_Lock;
	boost::circular_buffer<TimedOpenVRMatrix>* m_timedCircularTrackerMatrixBuffer;
};