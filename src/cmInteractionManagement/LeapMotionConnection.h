#include "Leap.h"
#include <osg/MatrixTransform>

namespace osgViewer {
	class Viewer;
}
class LeapMotionConnection : public Leap::Listener {
public:
	
	LeapMotionConnection(osg::ref_ptr<osg::Group> sceneRootIn);
	LeapMotionConnection(osgViewer::Viewer* m_mainViewer);
	virtual void onInit(const Leap::Controller&);
	virtual void onConnect(const Leap::Controller&);
	virtual void onDisconnect(const Leap::Controller&);
	virtual void onExit(const Leap::Controller&);
	virtual void onFrame(const Leap::Controller&);
	virtual void onFocusGained(const Leap::Controller&);
	virtual void onFocusLost(const Leap::Controller&);
	virtual void onDeviceChange(const Leap::Controller&);
	virtual void onServiceConnect(const Leap::Controller&);
	virtual void onServiceDisconnect(const Leap::Controller&);
	virtual void onServiceChange(const Leap::Controller&);
	virtual void onDeviceFailure(const Leap::Controller&);
	virtual void onLogMessage(const Leap::Controller&, Leap::MessageSeverity severity, int64_t timestamp, const char* msg);
	

private:

	osg::ref_ptr<osg::Group> m_sceneRoot;
	osgViewer::Viewer* m_mainViewer;
};