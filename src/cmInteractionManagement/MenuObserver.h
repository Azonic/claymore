// ClayMore - Immersive Mesh Modelling --- Copyright (c) 2014-2017 Philipp Ladwig, Jannik Fiedler, Jan Beutgen
#pragma once

#include "Observer.h"

#include "../cmMenuManagement/MenuManager.h"

class imInteractionManagement_DLL_import_export MenuObserver : public Observer
{
	public:

		MenuObserver();
		~MenuObserver();

		//Returns whether or not other observers on the controller should be called next or skipped (true => continue)
		//inline virtual bool notify(Controller* registeredControllerIn, Controller* otherControllerIn, BUTTON_STATE registeredButtonState = NONE) = 0;

		//inline std::vector<ControllerTool::CONTROLLER_TOOL_TYPE> & getRequiredControllerTools() { return m_requiredControllerTools; }

		inline MenuName getMenuName() { return m_menuName; }

	protected:
		MenuManager* m_menuManager;
		unsigned int m_lastButtonID;
		MenuName m_menuName;

		//Define these for every observer in it's constructor
		//std::vector<ControllerTool::CONTROLLER_TOOL_TYPE> m_requiredControllerTools;

		//bool m_deleteControllerToolOnDestruction;
};