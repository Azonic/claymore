#include "LeapMotionController.h"
#include "Leap.h"
#include <osg/ShapeDrawable>
#include <osg/Geode>
#include <osg/Geometry>
#include <osg/Vec3>
#include <osg/MatrixTransform>
#include <osg/PositionAttitudeTransform>
#include <osg/LineWidth>
#include <osg/Material>

//factor for scaling
float faktorX = 0.0125;
float faktorY = 0.0125;
float faktorZ = 0.0125;

//Variables
const std::string fingerNames[] = { "Thumb", "Index", "Middle", "Ring", "Pinky" };
const std::string boneNames[] = { "Metacarpal", "Proximal", "Middle", "Distal" };

//Geometry for the hand
osg::MatrixTransform* leftHandTransformation = new osg::MatrixTransform;
osg::MatrixTransform* rightHandTransformation = new osg::MatrixTransform;

osg::PositionAttitudeTransform* leftJointTransformation[23];
osg::PositionAttitudeTransform* rightJointTransformation[23];

osg::Geode* leftHandBones[23];
osg::Geode* rightHandBones[23];

osg::Geode* markSphere = new osg::Geode();
osg::PositionAttitudeTransform* markSphereTransformation;

osg::Geometry* linesGeomLeft = new osg::Geometry();
osg::Geometry* linesGeomRight = new osg::Geometry();
osg::Vec3Array* verticesLeft = new osg::Vec3Array(23);
osg::Vec3Array* verticesRight = new osg::Vec3Array(23);

osg::ref_ptr<osg::Vec3Array> leftPoints = new osg::Vec3Array;
osg::ref_ptr<osg::Vec3Array> rightPoints = new osg::Vec3Array;

osg::Geode* HMD = new osg::Geode();
osg::MatrixTransform* HMDTransformation = new osg::MatrixTransform;


LeapMotionController::LeapMotionController()
{

}

LeapMotionController::LeapMotionController(osg::ref_ptr<osg::Group> sceneRootIn) : m_sceneRoot(sceneRootIn)
{
	initialise();
}

void LeapMotionController::initialise()
{
	//creating the basic models for the bone and the stuff inbetween
	osg::ShapeDrawable* bone = new osg::ShapeDrawable(new osg::Sphere(osg::Vec3(0, 0, 0), 0.05));
	osg::ShapeDrawable* HMDSphere = new osg::ShapeDrawable(new osg::Cylinder(osg::Vec3(0, 0, 0), 0.05, 0.5));
	osg::ShapeDrawable* markSphereDrawable = new osg::ShapeDrawable(new osg::Sphere(osg::Vec3(0, 0, 0), 0.1));

	HMDTransformation->addChild(HMD);				//Adding the matching bone to the positionAttitudeTransform Node
	HMD->addDrawable(HMDSphere);
	//m_sceneRoot->addChild(HMDTransformation);

	markSphereTransformation = new osg::PositionAttitudeTransform;
	markSphereTransformation->addChild(markSphere);
	markSphere->addDrawable(markSphereDrawable);
	leftHandTransformation->addChild(markSphereTransformation);

	

	//current node structure handTransformation -> boneTransformation -> bone 
	//adding a new geode/positionAttitudeTransform for every bone
	for (int i = 0; i < 23; i++)
	{
		//everything for the LEFT hand
		leftHandBones[i] = new osg::Geode();
		leftJointTransformation[i] = new osg::PositionAttitudeTransform;
		leftJointTransformation[i]->addChild(leftHandBones[i]);				//Adding the matching bone to the positionAttitudeTransform Node
		leftHandBones[i]->addDrawable(bone);								//Adding the sphere to the bone
		leftHandTransformation->addChild(leftJointTransformation[i]);		//Adding the joint transformation to the hand transformation
		m_sceneRoot->addChild(leftHandTransformation);						//Adding everything to the scene

		rightHandBones[i] = new osg::Geode();
		rightJointTransformation[i] = new osg::PositionAttitudeTransform;
		rightJointTransformation[i]->addChild(rightHandBones[i]);			//Adding the matching bone to the positionAttitudeTransform Node
		rightHandBones[i]->addDrawable(bone);								//Adding the sphere to the bone
		rightHandTransformation->addChild(rightJointTransformation[i]);		//Adding the joint transformation to the hand transformation
		//m_sceneRoot->addChild(rightHandTransformation);						//Adding everything to the scene


	}

	std::cout << "Connected and Geometry initialised" << std::endl;
}

void LeapMotionController::updateHands(Leap::HandList hands)
{
	for (Leap::HandList::const_iterator hl = hands.begin(); hl != hands.end(); ++hl) {
		// Get the first hand
		const Leap::Hand hand = *hl;
		if (hand.isLeft()) {
			drawLeftLeapHand(hand);
			//std::cout << "Left Hand" << std::endl;
		}
		else if (hand.isRight())
		{
			drawRightLeapHand(hand);
			//std::cout << "Right Hand" << std::endl;
		}

		//if (hand.finger(0).isExtended() == false && hand.finger(1).isExtended() == true && hand.finger(2).isExtended() == false && hand.finger(3).isExtended() == false && hand.finger(4).isExtended() == false) {
		//	/*osg::Geode* brush = new osg::Geode();
		//	osg::ShapeDrawable* brushSphere = new osg::ShapeDrawable(new osg::Sphere(osg::Vec3(0, 0, 0), 0.05));
		//	brush->addChild(brushSphere);
		//	m_sceneRoot->addChild(brush);*/
		//	
		//	std::cout << "Pointing" << std::endl;
		//}




		/*std::cout << "Frame id: " << frame.id()
		<< ", timestamp: " << frame.timestamp()
		<< ", hands: " << frame.hands().count()
		<< ", extended fingers: " << frame.fingers().extended().count() << std::endl;

		// Get the hand's normal vector and direction
		const Leap::Vector normal = hand.palmNormal();
		const Leap::Vector direction = hand.direction();

		// Calculate the hand's pitch, roll, and yaw angles
		std::cout << std::string(2, ' ') << "pitch: " << direction.pitch() * Leap::RAD_TO_DEG << " degrees, "
		<< "roll: " << normal.roll() * Leap::RAD_TO_DEG << " degrees, "
		<< "yaw: " << direction.yaw() * Leap::RAD_TO_DEG << " degrees" << std::endl;

		// Get the Arm bone
		Leap::Arm arm = hand.arm();
		std::cout << std::string(2, ' ') << "Arm direction: " << arm.direction()
		<< " wrist position: " << arm.wristPosition()
		<< " elbow position: " << arm.elbowPosition() << std::endl;

		std::string handtype = hand.isLeft ? "left hand" : "right hand";
		std::cout << std::string(2, ' ') << handtype << ", id: " << hand.id()
		<< ", palm position: " << hand.palmPosition << std::endl;

		//Get fingers
		const Leap::FingerList fingers = hand.fingers();
		for (Leap::FingerList::const_iterator fl = fingers.begin(); fl != fingers.end(); ++fl) {
		const Leap::Finger finger = *fl;
		std::cout << std::string(4, ' ') << fingerNames[finger.type()]
		<< " finger, id: " << finger.id()
		<< ", length: " << finger.length()
		<< "mm, width: " << finger.width() << std::endl;

		//Get finger bones
		for (int b = 0; b < 4; ++b) {
		Leap::Bone::Type boneType = static_cast<Leap::Bone::Type>(b);
		Leap::Bone bone = finger.bone(boneType);
		std::cout << std::string(6, ' ') << boneNames[boneType]
		<< " bone, start: " << bone.prevJoint()
		<< ", end: " << bone.nextJoint()
		<< ", direction: " << bone.direction() << std::endl;
		}
		}
		}

		if (!frame.hands().isEmpty()) {
		std::cout << std::endl;*/
	}


}

osg::Vec3d leapVecToOsg(Leap::Vector vec) {
	return osg::Vec3d(vec.x * faktorX, vec.y * faktorY, vec.z * faktorZ);
}

void LeapMotionController::drawLeftLeapHand(const Leap::Hand hand)
{


	//going through every bone from the arm to the fingertips
	for (int boneID = 0; boneID <= 24; boneID++)
	{
		osg::Vec3d pos;
		osg::Matrix rot;
		osg::Quat quat;
		auto bone = leftJointTransformation[boneID];
		switch (boneID) {

			//elbow
		case 0:
			pos = leapVecToOsg(hand.arm().elbowPosition());
			bone->setPosition(pos);
			(*verticesLeft)[boneID].set(hand.arm().elbowPosition().x*faktorX, hand.arm().elbowPosition().y*faktorY, hand.arm().elbowPosition().z*faktorZ);
			/*rot.makeLookAt(bone->getPosition(), leapVecToOsg(hand.arm().wristPosition()), osg::Z_AXIS);
			quat.set(rot);
			bone->setScale(osg::Vec3d(1, 1, 3));
			bone->setAttitude(quat);*/
			break;

			//wrist
		case 1:
			bone->setPosition(osg::Vec3d(hand.arm().wristPosition().x*faktorX, hand.arm().wristPosition().y*faktorY, hand.arm().wristPosition().z*faktorZ));
			(*verticesLeft)[boneID].set(hand.arm().wristPosition().x*faktorX, hand.arm().wristPosition().y*faktorY, hand.arm().wristPosition().z*faktorZ);


			break;

			//palm position
		case 2:
			bone->setPosition(osg::Vec3d(hand.palmPosition().x*faktorX, hand.palmPosition().y*faktorY, hand.palmPosition().z*faktorZ));
			(*verticesLeft)[boneID].set(hand.palmPosition().x*faktorX, hand.palmPosition().y*faktorY, hand.palmPosition().z*faktorZ);
			break;

			//Distal ends of bones for each digit
		case 3:
			const Leap::FingerList fingers = hand.fingers();
			for (Leap::FingerList::const_iterator fingerList = fingers.begin(); fingerList != fingers.end(); ++fingerList) {
				const Leap::Finger finger = *fingerList;
				if (finger.isExtended() == false) {
					markSphereTransformation->setPosition(osg::Vec3d(hand.palmPosition().x*faktorX, hand.palmPosition().y*faktorY, hand.palmPosition().z*faktorZ));


					/*std::cout << "Marked" << std::endl;*/
				}
				for (int b = 0; b < 4; ++b) {
					Leap::Bone::Type boneType = static_cast<Leap::Bone::Type>(b);
					Leap::Bone bone = finger.bone(boneType);
					leftJointTransformation[boneID]->setPosition(osg::Vec3d(bone.nextJoint().x*faktorX, bone.nextJoint().y*faktorY, bone.nextJoint().z*faktorZ));
					(*verticesLeft)[boneID].set(bone.nextJoint().x*faktorX, bone.nextJoint().y*faktorY, bone.nextJoint().z*faktorZ);
					boneID++;
				}
			}
		}

	}
}

void LeapMotionController::drawRightLeapHand(const Leap::Hand hand)
{
	//going through every bone from the arm to the fingertips
	for (int boneID = 0; boneID <= 24; boneID++)
	{
		switch (boneID) {

			//elbow
		case 0:
			rightJointTransformation[boneID]->setPosition(osg::Vec3d(hand.arm().elbowPosition().x*faktorX, hand.arm().elbowPosition().y*faktorY, hand.arm().elbowPosition().z*faktorZ));
			(*verticesRight)[boneID].set(hand.arm().elbowPosition().x*faktorX, hand.arm().elbowPosition().y*faktorY, hand.arm().elbowPosition().z*faktorZ);
			break;

			//wrist
		case 1:
			rightJointTransformation[boneID]->setPosition(osg::Vec3d(hand.arm().wristPosition().x*faktorX, hand.arm().wristPosition().y*faktorY, hand.arm().wristPosition().z*faktorZ));
			(*verticesRight)[boneID].set(hand.arm().wristPosition().x*faktorX, hand.arm().wristPosition().y*faktorY, hand.arm().wristPosition().z*faktorZ);
			break;

			//palm position
		case 2:
			rightJointTransformation[boneID]->setPosition(osg::Vec3d(hand.palmPosition().x*faktorX, hand.palmPosition().y*faktorY, hand.palmPosition().z*faktorZ));
			(*verticesRight)[boneID].set(hand.palmPosition().x*faktorX, hand.palmPosition().y*faktorY, hand.palmPosition().z*faktorZ);
			break;

			//Distal ends of bones for each digit
		case 3:
			const Leap::FingerList fingers = hand.fingers();
			for (Leap::FingerList::const_iterator fingerList = fingers.begin(); fingerList != fingers.end(); ++fingerList) {
				const Leap::Finger finger = *fingerList;
				for (int b = 0; b < 4; ++b) {
					Leap::Bone::Type boneType = static_cast<Leap::Bone::Type>(b);
					Leap::Bone bone = finger.bone(boneType);
					rightJointTransformation[boneID]->setPosition(osg::Vec3d(bone.nextJoint().x*faktorX, bone.nextJoint().y*faktorY, bone.nextJoint().z*faktorZ));
					(*verticesRight)[boneID].set(bone.nextJoint().x*faktorX, bone.nextJoint().y*faktorY, bone.nextJoint().z*faktorZ);
					boneID++;
				}
			}
		}
	}
}

void LeapMotionController::updateHandsWithHMDPosition(osg::Matrixd hmdMatrixIn)
{

	osg::Matrix rot = osg::Matrix::rotate(osg::DegreesToRadians(90.0f), osg::Vec3(1, 0, 0));
	osg::Matrix scale = osg::Matrix::scale(-1, 1, -1);
	hmdMatrixIn = hmdMatrixIn * rot * scale;
	leftHandTransformation->setMatrix(osg::Matrix::inverse(hmdMatrixIn));
	rightHandTransformation->setMatrix(osg::Matrix::inverse(hmdMatrixIn));


	//HMDTransformation->setMatrix(osg::Matrix::inverse(hmdMatrixIn));

	//std::cout << "Hands updated" << std::endl;
}

