#include "Leap.h"
#include <osg/MatrixTransform>

class LeapMotionController {
public:

	LeapMotionController(osg::ref_ptr<osg::Group> sceneRootIn);
	LeapMotionController();
	void updateHands(Leap::HandList hands);
	void updateHandsWithHMDPosition(osg::Matrixd hmdMatrixIn);

private:

	void drawLeftLeapHand(const Leap::Hand hand);
	void drawRightLeapHand(const Leap::Hand hand);
	void initialise();
	osg::ref_ptr<osg::Group> m_sceneRoot;

};