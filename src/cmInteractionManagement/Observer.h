// ClayMore - Immersive Mesh Modelling --- Copyright (c) 2014-2017 Philipp Ladwig, Jannik Fiedler, Jan Beutgen
#pragma once

#include "ConfigDllExportInteractionManagement.h"

#include "../cmUtil/ConfigReader.h"

#include <osg/Vec3d>

#include "Controller.h"

class imInteractionManagement_DLL_import_export Observer
{
	public:

		Observer();
		~Observer();

		//Returns whether or not other observers on the controller should be called next or skipped (true => continue)
		inline virtual bool notify(Controller* registeredControllerIn, Controller* otherControllerIn, BUTTON_STATE registeredButtonState = NONE) = 0;

		inline std::vector<ControllerTool::CONTROLLER_TOOL_TYPE> & getRequiredControllerTools() { return m_requiredControllerTools; }

		enum OBSERVER_TYPE
		{
			TRIGGER,
			JOYSTICK,
			MENUBUTTON,
			SQUEEZE,
			CONSTANTLY_UPDATE,
			MODE_MENU,
			PRIMARY_JOYSTICK_MENU,
			SECONDARY_JOYSTICK_MENU
		};

	protected:

		//Define these for every observer in it's constructor
		std::vector<ControllerTool::CONTROLLER_TOOL_TYPE> m_requiredControllerTools;

		bool m_deleteControllerToolOnDestruction;
};