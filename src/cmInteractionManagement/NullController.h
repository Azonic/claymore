// ClayMore - Immersive Mesh Modelling --- Copyright (c) 2014-2017 Philipp Ladwig, Jannik Fiedler, Jan Beutgen
#pragma once

#include "ConfigDllExportInteractionManagement.h"

#include <osgDB/ReadFile>

#include "Controller.h"

class imInteractionManagement_DLL_import_export NullController : public Controller
{
	public:
		NullController(unsigned int controllerID, CONTROLLER_TYPE controllerType);
		~NullController();

		void update(void *controllerData);

	private:

		float m_lastTriggerPressure;
};