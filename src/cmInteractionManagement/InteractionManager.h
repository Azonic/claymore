// ClayMore - Immersive Mesh Modelling --- Copyright (c) 2014-2017 Philipp Ladwig, Jannik Fiedler, Jan Beutgen
#pragma once

#include "ConfigDllExportInteractionManagement.h"

#include <osg/MatrixTransform>

#include "Leap.h"
#include "Observer.h"

#include <atomic>
#include "cmUtil/TimedOpenVRMatrix.h"
#include <boost/circular_buffer.hpp>

class MiscDataStorage;
class ConfigReader;
class OpenVRConnection;
class LeapMotionConnection;
class KeyboardMouseConnection;

class imInteractionManagement_DLL_import_export InteractionManager
{
	public:
		//TODO#456: Giving g_sceneRoot here is only for Debug purposes for LeapMotion hand -> remove later and create something like primaryHandController
		InteractionManager(ConfigReader* configReaderIn,
			bool useKeyboardAndMouseIn,
			bool useViveIn,
			bool useLeapMotionIn,
			MiscDataStorage* miscDataStorageIn);
			//osg::ref_ptr<osgViewer::Viewer> spectatorWindowIn);
		~InteractionManager();

		void update();

		void activateKeyboardMouseConnection();
		void activateOpenVR(osg::ArgumentParser* argumentsIn);
		void activateLeapMotion();

		void registerObserver(CONTROLLER_TYPE controllerType, Observer::OBSERVER_TYPE observerType, Observer* observerIn);
		void unregisterObserver(CONTROLLER_TYPE controllerType, Observer::OBSERVER_TYPE observerType);

		//TODO: ULTRA UGLY BECAUSE THERE ARE STILL UGLY VECTORS FOR CONTROLLER IN g_interactionManager
		void addPrimaryController(Controller *controllerIn);
		void addSecondaryController(Controller *controllerIn);
		void addGenericTracker(Controller *controllerIn);

		inline KeyboardMouseConnection* getKeyboardMouseConnection() { return m_keyboardMouseConnection; }
		inline Controller* getPrimaryController();
		inline Controller* getSecondaryController();
		inline Controller* getGenericTracker();

		Observer* getRegisteredObserverFromPrimarayController(Observer::OBSERVER_TYPE typeIn);
		Observer* getRegisteredObserverFromSecondaryController(Observer::OBSERVER_TYPE typeIn);
		Observer* getRegisteredObserverFromGenericTracker(Observer::OBSERVER_TYPE typeIn);

		inline OpenVRViewer* getOpenVRViewer() { return m_openVRViewer; }
		inline osgViewer::Viewer* getMainViewer(){ return m_mainViewer; }

		void setSpectatorView(osg::ref_ptr<osgViewer::Viewer> spectatorViewerIn);
		inline osg::ref_ptr<osgViewer::Viewer> getSpectatorViewer() { return m_spectatorViewer; }

		bool initializeInteraction(osg::ArgumentParser* argumentsIn);

		inline boost::circular_buffer<TimedOpenVRMatrix>* getTimedCircularTrackerMatrixBufferFromOpenVR();
		inline std::atomic<bool>* getTimedCircularTrackerMatrixBuffer_Lock_FromOpenVR();


		void shutdown();

	private:
		ConfigReader * m_configReader;
		MiscDataStorage* m_miscDataStorage;

		osgViewer::Viewer* m_mainViewer;
		osg::ref_ptr<osgViewer::Viewer> m_spectatorViewer;
		OpenVRViewer* m_openVRViewer;
		osg::ref_ptr<osg::GraphicsContext> m_graphicsContext;

		void detectInteractionsAndNotify(Controller* registeredControllerIn, Controller* otherControllerIn, std::map<Observer::OBSERVER_TYPE, Observer*>* observerListIn);

		void activateToolsForObserver(Observer* observerIn, Controller *controllerIn);
		void notifyToolsDeactivateForObserver(Observer* observerIn, Controller *controllerIn);

		//KeyboardMouseConnection
		bool m_useKeyboardAndMouse;
		KeyboardMouseConnection* m_keyboardMouseConnection;

		//OpenVR
		bool m_useVive;
		osg::ref_ptr<OpenVRConnection> m_openVRConnection;
		std::vector<Controller*> m_primaryControllers;
		std::vector<Controller*> m_secondaryControllers;
		std::vector<Controller*> m_genericTrackers;
		std::map<Observer::OBSERVER_TYPE, Observer*> m_primaryControllerObservers;
		std::map<Observer::OBSERVER_TYPE, Observer*> m_secondaryControllerObservers;
		std::map<Observer::OBSERVER_TYPE, Observer*> m_genericControllerObservers;

		//LeapMotion
		bool m_useLeapMotion;
		LeapMotionConnection* m_leapMotionConnection;
		Leap::Controller* m_LeapMotionController;
};