// ClayMore - Immersive Mesh Modelling --- Copyright (c) 2014-2017 Philipp Ladwig, Jannik Fiedler, Jan Beutgen
#include "KeyboardMouseConnection.h"

#include <iostream>

KeyboardMouseConnection::KeyboardMouseConnection()
{
	m_guiEventAdapter = new osgGA::GUIEventAdapter();
}

KeyboardMouseConnection::~KeyboardMouseConnection()
{
}

/// \brief -
/// \details -
/// \note handle seems to be called twice per frame? Once for the actual event (e.g. KEYDOWN or KEYUP) and the second time for FRAME. <BR>
/// This makes it actually quite hard to recognize correctly the current event, cause the last event is always a "FRAME" event, which some <BR>
/// kind of idle event - Currently not sure whether I did it wrong or it is strange behavior of OSG
/// \param[in] -
/// \return -
bool KeyboardMouseConnection::handle(const osgGA::GUIEventAdapter& ea, osgGA::GUIActionAdapter& aa)
{
	//TODO: recognize multiple key events like: http://forum.openscenegraph.org/viewtopic.php?t=62
	if (ea.getEventType() == osgGA::GUIEventAdapter::KEYDOWN) // in int: 32
	{
		m_currentEventType = ea.getEventType();
		m_currentKey = (osgGA::GUIEventAdapter::KeySymbol) ea.getKey();
		m_lastEventType = m_currentEventType;
	}
	if (ea.getEventType() == osgGA::GUIEventAdapter::KEYUP) // in int: 64
	{
		m_currentEventType = ea.getEventType();
		m_currentKey = (osgGA::GUIEventAdapter::KeySymbol) ea.getKey();
		m_lastEventType = m_currentEventType;
	}
	return false;
}

/// \brief Return the current button state of the given button (OSG says "KeySymbol")
/// \details Unfortunately OSG does not differenciate between "KEY DOWN" and "HOLD KEY" - "HOLD KEY" is after the second frame still "Key DOWN"
/// This function implements an differnciation between them<BR>
/// \param[in] whichKeyIn For what KeySymbol (alias Button) would you like to know the state
/// \return The current button state "HOLD", "PUSH", "REALEASE" and so on
BUTTON_STATE KeyboardMouseConnection::getButtonState(osgGA::GUIEventAdapter::KeySymbol whichKeyIn)
{
	if (m_currentKey == whichKeyIn)
	{
		if (m_currentEventType == osgGA::GUIEventAdapter::KEYDOWN)
		{
			if (m_isKeyDown)
			{
				m_buttonState = BUTTON_STATE::HOLD;
			}
			else
			{
				m_buttonState = BUTTON_STATE::PUSH;
			}
			m_isKeyDown = true;
			return m_buttonState;
		}
		else if(m_currentEventType == osgGA::GUIEventAdapter::KEYUP && m_lastEventType != osgGA::GUIEventAdapter::KEYUP)
		{
			m_isKeyDown = false;
			m_buttonState = BUTTON_STATE::RELEASE;
			return m_buttonState;
		}
	}
	m_buttonState = BUTTON_STATE::IDLE;
	m_isKeyDown = false;
	return m_buttonState;
}