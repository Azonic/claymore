// ClayMore - Immersive Mesh Modelling --- Copyright (c) 2014-2017 Philipp Ladwig, Jannik Fiedler, Jan Beutgen
#include "InteractionManager.h"
#include <iostream>

#include "../cmUtil/MiscDataStorage.h"
#include "../cmUtil/ConfigReader.h"

#include "NullController.h"

#include <osgGA/TrackballManipulator>

#include "KeyboardMouseConnection.h"
#include "OpenVRConnection.h"
#include "LeapMotionConnection.h"

InteractionManager::InteractionManager(ConfigReader* configReaderIn,
	bool useKeyboardAndMouseIn,
	bool useViveIn,
	bool useLeapMotionIn,
	MiscDataStorage* miscDataStorageIn) :
	m_configReader(configReaderIn),
	m_miscDataStorage(miscDataStorageIn),
	m_openVRConnection(nullptr),
	m_keyboardMouseConnection(nullptr)
{
	m_openVRViewer = nullptr;
	m_graphicsContext = nullptr;

	m_useKeyboardAndMouse = useKeyboardAndMouseIn;
	m_useVive = useViveIn;
	m_useLeapMotion = useLeapMotionIn;

}

InteractionManager::~InteractionManager()
{
	std::cout << "In Destructor of Interaction Management" << std::endl;
	delete m_openVRConnection;
	//delete m_keyboardMouseConnection;
}

void InteractionManager::activateKeyboardMouseConnection()
{
	m_keyboardMouseConnection = new KeyboardMouseConnection();
	m_spectatorViewer->addEventHandler(m_keyboardMouseConnection);
}

void InteractionManager::activateOpenVR(osg::ArgumentParser* argumentsIn)
{
	// Exit if we do not have an HMD present
	if (!vr::VR_IsHmdPresent())
	{
		std::string errorMessage = " # ERROR in OpenVRConnection::initializeOpenVR() - No valid HMD present!";
		throw(errorMessage);
	}

	float nearClip = 0.001f;
	float farClip = 1000.0f;
	float worldUnitsPerMetre = 1.0f;
	int samples = 1;

	m_openVRConnection = new OpenVRConnection(nearClip, farClip, worldUnitsPerMetre, samples);

	// Exit if we fail to initialize the HMD device
	if (!m_openVRConnection->hmdInitialized())
	{
		std::string errorMessage = " # ERROR in OpenVRConnection::initializeOpenVR() - Couldn't init HMD, terminating!";
		throw(errorMessage);
	}

	// Get the suggested context traits
	osg::ref_ptr<osg::GraphicsContext::Traits> traits = m_openVRConnection->graphicsContextTraits();
	traits->windowName = "ClayMore";
	traits->windowDecoration = true;
	traits->screenNum = 0;

	// Create a graphic context based on our desired traits
	m_graphicsContext = osg::GraphicsContext::createGraphicsContext(traits);
	m_mainViewer = new GraphicsWindowViewer(*argumentsIn, dynamic_cast<osgViewer::GraphicsWindow*>(m_graphicsContext.get()));
	osg::ref_ptr<OpenVRRealizeOperation> openvrRealizeOperation = new OpenVRRealizeOperation(m_openVRConnection);
	m_mainViewer->setRealizeOperation(openvrRealizeOperation.get());

	//m_mainViewer->addEventHandler(new osgViewer::StatsHandler);
	m_mainViewer->addEventHandler(new OpenVREventHandler(m_openVRConnection));
	m_mainViewer->getCamera()->setGraphicsContext(m_graphicsContext);
	m_mainViewer->getCamera()->setViewport(0, 0, m_graphicsContext->getTraits()->width, m_graphicsContext->getTraits()->height);

	//Vive Viewer
	m_openVRViewer = new OpenVRViewer(m_mainViewer, m_openVRConnection, openvrRealizeOperation);
	m_openVRViewer->setName("openVRViewer");
}

void InteractionManager::activateLeapMotion()
{
	//TODO5: Maybe not sooo smart...it will crash, when m_useLeapMotionDevice is set to true during runtime 
	if (m_useLeapMotion)
	{
		if (m_useVive)
		{
			//Passing through the mainViewer for getting the HMD position and therefore the LeapMotion position
			m_leapMotionConnection = new LeapMotionConnection(m_mainViewer);
		}
		else
		{
			//If no HMD is used, then the leap is used without flag "POLICY_OPTIMIZE_HMD" and no mainViewer for HMD position is passed through
			// just only the sceneRoot for attaching the LeapMotion hand geometry
			m_leapMotionConnection = new LeapMotionConnection(m_mainViewer->getSceneData()->asGroup());
		}
		// Create a listener (here the leapMotionConnection) and controller

		m_LeapMotionController = new Leap::Controller();

		// Have the listener receive events from the controller
		m_LeapMotionController->addListener(*m_leapMotionConnection);

		m_LeapMotionController->setPolicy(Leap::Controller::POLICY_ALLOW_PAUSE_RESUME);

		if (m_useVive)
		{
			m_LeapMotionController->setPolicy(Leap::Controller::POLICY_OPTIMIZE_HMD);
		}
	}
}

void InteractionManager::addPrimaryController(Controller *controllerIn)
{
	if (m_openVRConnection)
	{
		//Get the internal tracking device number of the controller and add it to the ClayMore controllerID
		dynamic_cast<ViveController*>(controllerIn)->setControllerIDByOpenVR();
	}
	m_primaryControllers.push_back(controllerIn);

}

void InteractionManager::addSecondaryController(Controller *controllerIn)
{
	if (m_openVRConnection)
	{
		dynamic_cast<ViveController*>(controllerIn)->setControllerIDByOpenVR();
	}
	m_secondaryControllers.push_back(controllerIn);
}

void InteractionManager::addGenericTracker(Controller *controllerIn) {
	if (m_openVRConnection)
	{
		dynamic_cast<ViveController*>(controllerIn)->setControllerIDByOpenVR();
	}
	m_genericTrackers.push_back(controllerIn);
}

void InteractionManager::update()
{
	if (m_keyboardMouseConnection != nullptr)
	{
		//		m_keyboardMouseConnection->handle();
	}
	if (m_openVRConnection != nullptr)
	{
		m_openVRConnection->handleInput(m_primaryControllers, m_secondaryControllers, m_genericTrackers);

		//START: Render or remove Help Text and "indication geometry" from controllers 
		if (m_miscDataStorage->getIsHelpTextActivated())
		{
			dynamic_cast<ViveController*>(m_primaryControllers.at(0))->renderHelpText();
			dynamic_cast<ViveController*>(m_secondaryControllers.at(0))->renderHelpText();
		}
		else
		{
			dynamic_cast<ViveController*>(m_primaryControllers.at(0))->removeHelpText();
			dynamic_cast<ViveController*>(m_secondaryControllers.at(0))->removeHelpText();
		}

		dynamic_cast<ViveController*>(m_primaryControllers.at(0))->updateIndicationGeometry(m_miscDataStorage->getIsTriggerLeftIndicated(),
			m_miscDataStorage->getIsTriggerRightIndicated(),
			m_miscDataStorage->getIsSqueezeLeftIndicated(),
			m_miscDataStorage->getIsSqueezeRightIndicated());
		dynamic_cast<ViveController*>(m_secondaryControllers.at(0))->updateIndicationGeometry(m_miscDataStorage->getIsTriggerLeftIndicated(),
			m_miscDataStorage->getIsTriggerRightIndicated(),
			m_miscDataStorage->getIsSqueezeLeftIndicated(),
			m_miscDataStorage->getIsSqueezeRightIndicated());
	}
	//END: Render or remove Help Text and "indication geometry" from controllers 

	//Update all ControllerTools on both controllers and genericTracker
	for (int i = 0; i <= 2; ++i)
	{
		std::vector<Controller*>* controllerList;
		if (i == 0)
		{
			controllerList = &m_primaryControllers;
		}
		else if (i == 1)
		{
			controllerList = &m_secondaryControllers;
		}
		else if (i == 2)
		{
			controllerList = &m_genericTrackers;
		}

		std::vector<Controller*>::iterator coIt = controllerList->begin();
		if (coIt != controllerList->end())
		{
			if ((*coIt) != nullptr)
				(*coIt)->updateControllerToolPool();

			++coIt;
		}
	}

	//TODO: Do we really need lists of primary and secondary controllers?
	detectInteractionsAndNotify(m_primaryControllers[0], m_secondaryControllers[0], &m_primaryControllerObservers);
	detectInteractionsAndNotify(m_secondaryControllers[0], m_primaryControllers[0], &m_secondaryControllerObservers);
	//TODO: detectInteractionsAndNotify() is not implemented fpr generic Controller yet

	//Update LeapMotion
	if (m_useLeapMotion)
	{
		m_leapMotionConnection->onFrame(*m_LeapMotionController);
	}
}

void InteractionManager::registerObserver(CONTROLLER_TYPE controllerType, Observer::OBSERVER_TYPE observerType, Observer* observerIn)
{
	std::map<Observer::OBSERVER_TYPE, Observer*>* observerList = controllerType == CONTROLLER_TYPE::PRIMARY ? &m_primaryControllerObservers : &m_secondaryControllerObservers;

	std::map<Observer::OBSERVER_TYPE, Observer*>::iterator obIt = observerList->find(observerType);

	if (obIt != observerList->end())
	{
		Observer* lastObserver = (*obIt).second;
		notifyToolsDeactivateForObserver(lastObserver, controllerType == CONTROLLER_TYPE::PRIMARY ? m_primaryControllers[0] : m_secondaryControllers[0]);
		delete lastObserver;
		(*obIt).second = observerIn;
	}
	else
		observerList->insert(std::pair<Observer::OBSERVER_TYPE, Observer*>(observerType, observerIn));

	activateToolsForObserver(observerIn, controllerType == CONTROLLER_TYPE::PRIMARY ? m_primaryControllers[0] : m_secondaryControllers[0]);
}

void InteractionManager::unregisterObserver(CONTROLLER_TYPE controllerType, Observer::OBSERVER_TYPE observerType)
{
	std::map<Observer::OBSERVER_TYPE, Observer*>* observerList = controllerType == CONTROLLER_TYPE::PRIMARY ? &m_primaryControllerObservers : &m_secondaryControllerObservers;

	std::map<Observer::OBSERVER_TYPE, Observer*>::iterator obIt = observerList->find(observerType);

	if (obIt != observerList->end())
	{
		Observer* lastObserver = (*obIt).second;
		notifyToolsDeactivateForObserver(lastObserver, controllerType == CONTROLLER_TYPE::PRIMARY ? m_primaryControllers[0] : m_secondaryControllers[0]);
		delete lastObserver;
		observerList->erase(obIt);
	}
}

Controller* InteractionManager::getPrimaryController()
{
	return m_primaryControllers[0];
}

Controller* InteractionManager::getSecondaryController()
{
	return m_secondaryControllers[0];
}

Controller* InteractionManager::getGenericTracker()
{
	return m_genericTrackers[0];
}

Observer * InteractionManager::getRegisteredObserverFromPrimarayController(Observer::OBSERVER_TYPE typeIn)
{
	return m_primaryControllerObservers[typeIn];
}

Observer* InteractionManager::getRegisteredObserverFromSecondaryController(Observer::OBSERVER_TYPE typeIn)
{
	return m_secondaryControllerObservers[typeIn];
}

Observer* InteractionManager::getRegisteredObserverFromGenericTracker(Observer::OBSERVER_TYPE typeIn)
{
	return m_genericControllerObservers[typeIn];
}

void InteractionManager::setSpectatorView(osg::ref_ptr<osgViewer::Viewer> spectatorViewerIn)
{
	m_spectatorViewer = spectatorViewerIn;
}

bool InteractionManager::initializeInteraction(osg::ArgumentParser* argumentsIn)
{
	if (m_useVive)
	{
		if (ConfigReader::DEBUG_LEVEL >= NOT_IN_LOOPS)
			std::cout << "Input device: VIVE CONTROLLER" << std::endl << "Output device: VIVE HMD" << std::endl;
		try
		{

			activateOpenVR(argumentsIn);

			//Vive Controller
			ViveController* primaryController = new ViveController(CONTROLLER_TYPE::PRIMARY, m_openVRConnection->getIVRSystem());
			primaryController->getMatrixTransform()->setName("MatrixTransform-m_primaryController");
			ViveController* secondaryController = new ViveController(CONTROLLER_TYPE::SECONDARY, m_openVRConnection->getIVRSystem());
			secondaryController->getMatrixTransform()->setName("MatrixTransform-m_secondaryController");
			ViveController* tertiaryController = new ViveController(CONTROLLER_TYPE::GENERIC_TRACKER, m_openVRConnection->getIVRSystem());
			tertiaryController->getMatrixTransform()->setName("MatrixTransform-m_genericTrackers");

			addPrimaryController(primaryController);
			addSecondaryController(secondaryController);
			addGenericTracker(tertiaryController);

			//Load a yellow Button for the MainMenu/Escape Menu
			osg::Node* yellowMenuButton = new osg::Node();
			yellowMenuButton->setName("yellowMenuButton");
			yellowMenuButton = osgDB::readNodeFile("./data/controller_models/yellowMenuButtonController.obj");
			osg::StateSet * ss = yellowMenuButton->getOrCreateStateSet();
			ss->setMode(GL_LIGHTING, osg::StateAttribute::OFF);
			secondaryController->getMatrixTransformNotShadowed()->addChild(yellowMenuButton);

			if (m_graphicsContext && m_graphicsContext.valid())
			{
				m_graphicsContext->setClearColor(m_configReader->getVec4fFromStartupConfig("main_window_background_color"));
				m_graphicsContext->setClearMask(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
			}
			else
				std::cout << " # ERROR in initializeInteraction() - GraphicsWindow has not been created successfully" << std::endl;

			//TODO: Write a "AdjusterClass" for different input devices and set the following lines there
			//AND TODO: If Adjuster is written, move the configReader outside of InteractionManagement in order to reduce the dependencies of the lib (Whats about the MiscStorage?)
			//NOTE: Maybe we should consider to use only the configReader in imCore? Just in Main and distribute all the information? Anyway, it is bad, to ask and compare "heavy" strings again and again...
			//Set Pie menu correction for the Vive controller
			ConfigReader::PIE_MENU_CORRECTION_QUAT[0] = M_PI_4 / 8; //Angle in Rad
			ConfigReader::PIE_MENU_CORRECTION_QUAT[1] = 1.0f; //X
			ConfigReader::PIE_MENU_CORRECTION_QUAT[2] = 0.0f; //Y
			ConfigReader::PIE_MENU_CORRECTION_QUAT[3] = 0.0f; //Z

			ConfigReader::PIE_MENU_CORRECTION_POS[0] = 0.0f;
			ConfigReader::PIE_MENU_CORRECTION_POS[1] = -0.01f;
			ConfigReader::PIE_MENU_CORRECTION_POS[2] = 0.05f;

			ConfigReader::TILT_SHIFT_MENU_CORRECTION_QUAT[0] = M_PI_2 / 2; //Angle in Rad
			ConfigReader::TILT_SHIFT_MENU_CORRECTION_QUAT[1] = 1.0f; //X
			ConfigReader::TILT_SHIFT_MENU_CORRECTION_QUAT[2] = 0.0f; //Y
			ConfigReader::TILT_SHIFT_MENU_CORRECTION_QUAT[3] = 0.0f; //Z

			ConfigReader::TILT_SHIFT_MENU_CORRECTION_POS[0] = 0.0f;
			ConfigReader::TILT_SHIFT_MENU_CORRECTION_POS[1] = 0.0f;
			ConfigReader::TILT_SHIFT_MENU_CORRECTION_POS[2] = 0.0f;

			ConfigReader::TOUCH_MENU_CORRECTION_POS[0] = 0.0f;
			ConfigReader::TOUCH_MENU_CORRECTION_POS[1] = 0.01f;
			ConfigReader::TOUCH_MENU_CORRECTION_POS[2] = 0.05f;
		}
		catch (std::string errMsg)
		{
			std::cout << errMsg << std::endl;
			return false;
		}
	}
	else if (m_configReader->getStringFromStartupConfig("input_device").compare("MOUSE") == 0)
	{
		m_mainViewer = new osgViewer::Viewer();
		m_mainViewer->addEventHandler(new osgViewer::StatsHandler);
		m_mainViewer->setUpViewOnSingleScreen(0);
		m_mainViewer->getCamera()->setClearColor(m_configReader->getVec4fFromStartupConfig("main_window_background_color"));

		osg::ref_ptr< osg::GraphicsContext::Traits > traits = new osg::GraphicsContext::Traits((*m_mainViewer->getCamera()->getGraphicsContext()->getTraits()));
		traits->vsync = false;
		traits->screenNum = 0; //On which monitor should it placed when started //TODO: Add to config or at least to define.h
		traits->windowName = "ClayMore";
		traits->windowDecoration = true;
		osg::ref_ptr< osg::GraphicsContext > gc = osg::GraphicsContext::createGraphicsContext(traits.get());
		gc->realize();
		m_mainViewer->getCamera()->setGraphicsContext(gc);
		m_mainViewer->getCamera()->setProjectionMatrixAsPerspective(80.0f, 1.777f, 0.001f, 500.0f);
		m_mainViewer->setCameraManipulator(new osgGA::TrackballManipulator());
		m_mainViewer->getCameraManipulator()->setHomePosition(osg::Vec3f(0.0, 35.0, 0.0), osg::Vec3f(0.0, 0.0, 0.0), osg::Vec3f(1.0, 0.0, 0.0), false);

		NullController* primaryController = new NullController(1, CONTROLLER_TYPE::PRIMARY);
		NullController* secondaryController = new NullController(0, CONTROLLER_TYPE::SECONDARY);
		NullController* tertiaryController = new NullController(2, CONTROLLER_TYPE::GENERIC_TRACKER);

		addPrimaryController(primaryController);
		addSecondaryController(secondaryController);
		addGenericTracker(tertiaryController);
	}

	activateLeapMotion();
	return true;
}

void InteractionManager::detectInteractionsAndNotify(Controller* registeredControllerIn, Controller* otherControllerIn, std::map<Observer::OBSERVER_TYPE, Observer*>* observerListIn)
{
	std::map<Observer::OBSERVER_TYPE, Observer*>::iterator obIt;

	obIt = observerListIn->find(Observer::MODE_MENU);
	if (obIt != observerListIn->end())
	{
		BUTTON_STATE menuButtonState = registeredControllerIn->getButtonState(MENU);
		//If a menu is active, stop from notifying other observers
		//if (!(*obIt).second->notify(registeredControllerIn, otherControllerIn))
		if (!(*obIt).second->notify(registeredControllerIn, otherControllerIn, menuButtonState))
			return;
	}

	obIt = observerListIn->find(Observer::PRIMARY_JOYSTICK_MENU);
	if (obIt != observerListIn->end())
	{
		//If a menu is active, stop from notifying other observers
		if (!(*obIt).second->notify(registeredControllerIn, otherControllerIn))
			return;
	}

	obIt = observerListIn->find(Observer::SECONDARY_JOYSTICK_MENU);
	if (obIt != observerListIn->end())
	{
		//If a menu is active, stop from notifying other observers
		if (!(*obIt).second->notify(registeredControllerIn, otherControllerIn))
			return;
	}

	obIt = observerListIn->find(Observer::CONSTANTLY_UPDATE);
	if (obIt != observerListIn->end())
	{
		if (!(*obIt).second->notify(registeredControllerIn, otherControllerIn))
			return;
	}

	BUTTON_STATE triggerButtonState = registeredControllerIn->getButtonState(REAR_TRIGGER);
	if (triggerButtonState != IDLE)
	{
		obIt = observerListIn->find(Observer::TRIGGER);

		if (obIt != observerListIn->end())
			(*obIt).second->notify(registeredControllerIn, otherControllerIn, triggerButtonState);
	}

	BUTTON_STATE joystickButtonState = registeredControllerIn->getButtonState(JOYSTICK);
	//TODO: A problem is, if the TouchPad of Vive isn't pressed, but touched, Vive sends IDLE...
	//...so if it is only touched and the touch information is needed it won't call the assaigned observer
	//if (joystickButtonState != IDLE)
	//{
	obIt = observerListIn->find(Observer::JOYSTICK);

	if (obIt != observerListIn->end())
		(*obIt).second->notify(registeredControllerIn, otherControllerIn, joystickButtonState);
	//}

	BUTTON_STATE menuButtonState = registeredControllerIn->getButtonState(MENU);
	if (menuButtonState != IDLE)
	{
		obIt = observerListIn->find(Observer::MENUBUTTON);

		if (obIt != observerListIn->end())
			(*obIt).second->notify(registeredControllerIn, otherControllerIn, menuButtonState);
	}

	BUTTON_STATE squeezeButtonState = registeredControllerIn->getButtonState(SQUEEZE);
	if (squeezeButtonState != IDLE)
	{
		obIt = observerListIn->find(Observer::SQUEEZE);

		if (obIt != observerListIn->end())
			(*obIt).second->notify(registeredControllerIn, otherControllerIn, squeezeButtonState);
	}
}

void InteractionManager::activateToolsForObserver(Observer* observerIn, Controller* controllerIn)
{
	std::vector<ControllerTool::CONTROLLER_TOOL_TYPE> requiredControllerTools = observerIn->getRequiredControllerTools();

	std::vector<ControllerTool::CONTROLLER_TOOL_TYPE>::iterator reCoToIt = requiredControllerTools.begin();

	while (reCoToIt != requiredControllerTools.end())
	{
		controllerIn->activateControllerToolForType((*reCoToIt));
		++reCoToIt;
	}
}

void InteractionManager::notifyToolsDeactivateForObserver(Observer* observerIn, Controller *controllerIn)
{
	std::vector<ControllerTool::CONTROLLER_TOOL_TYPE> requiredControllerTools = observerIn->getRequiredControllerTools();

	std::vector<ControllerTool::CONTROLLER_TOOL_TYPE>::iterator reCoToIt = requiredControllerTools.begin();

	while (reCoToIt != requiredControllerTools.end())
	{
		controllerIn->notifyControllerToolDeactivateForType((*reCoToIt));
		++reCoToIt;
	}
}

void InteractionManager::shutdown()
{
	if (m_openVRConnection != NULL)
	{
		std::cout << std::endl << "Shutdown VR System... " << std::endl;
		m_openVRConnection->shutdown(m_graphicsContext.get());
		//delete g_interactionManager;
		//std::cout << std::endl << "Shutdown Qt Threads... " << std::endl;
		//qtCoreApp.quit();
		//qtCoreApp.exit();
	}
}

inline boost::circular_buffer<TimedOpenVRMatrix>* InteractionManager::getTimedCircularTrackerMatrixBufferFromOpenVR()
{
	return m_openVRConnection->getTimedCircularTrackerMatrixBuffer(); 
};

inline std::atomic<bool>* InteractionManager::getTimedCircularTrackerMatrixBuffer_Lock_FromOpenVR()
{
	return m_openVRConnection->getTimedCircularTrackerMatrixBuffer_Lock();
};
