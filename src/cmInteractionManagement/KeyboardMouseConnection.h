// ClayMore - Immersive Mesh Modelling --- Copyright (c) 2014-2017 Philipp Ladwig, Jannik Fiedler, Jan Beutgen

//FIXME: Do we really need this class/functionality? We should get more performance, when dont look after this Event Sender
#pragma once

#include <osgGA/GUIEventHandler>

#include "ConfigDllExportInteractionManagement.h"

//Note: Controller.h is just used for enum BUTTON STATE - Maybe it makes sense to implement a keyboard as ClayMore Controller? "We do need some more polymorphic structure"
#include "Controller.h"

class imInteractionManagement_DLL_import_export KeyboardMouseConnection : public osgGA::GUIEventHandler
{
	// #### CONSTRUCTOR & DESTRUCTOR ###############
public:
	KeyboardMouseConnection();
	~KeyboardMouseConnection();

	// #### MEMBER VARIABLES ###############
private:
	//osg::ref_ptr<osgGA::GUIEventAdapter> m_guiEventAdapter;
	osgGA::GUIEventAdapter* m_guiEventAdapter;
	osgGA::GUIEventAdapter::KeySymbol m_currentKey;
	osgGA::GUIEventAdapter::EventType m_currentEventType;
	osgGA::GUIEventAdapter::EventType m_lastEventType;
	BUTTON_STATE m_buttonState;
	bool m_isKeyDown;

	// #### MEMBER FUNCTIONS ###############
public:
	//virtual bool KeyboardMouseConnection::handleKeyDown(const osgGA::GUIEventAdapter& ea, osgGA::GUIActionAdapter& aa);
	virtual bool handle(const osgGA::GUIEventAdapter& ea, osgGA::GUIActionAdapter&);
	inline osgGA::GUIEventAdapter* getGUIEventAdapter() { return m_guiEventAdapter; }
	BUTTON_STATE getButtonState(osgGA::GUIEventAdapter::KeySymbol whichKeyIn);

	//void addMouseEvent(const osgGA::GUIEventAdapter& ea);
};