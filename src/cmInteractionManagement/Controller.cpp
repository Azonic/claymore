// ClayMore - Immersive Mesh Modelling --- Copyright (c) 2014-2017 Philipp Ladwig, Jannik Fiedler, Jan Beutgen
#include "Controller.h"
#include <osg\MatrixTransform>

//osg::ref_ptr<osg::Node> Controller::m_controllerModel = NULL;

Controller::Controller(unsigned int controllerID, CONTROLLER_TYPE controllerType) :
	m_controllerID(controllerID),
	m_controllerType(controllerType)
{
	m_controllerTransform = new osg::MatrixTransform();
	m_controllerTransformNotShadowed = new osg::MatrixTransform();

	m_controllerTransformNotShadowed->setMatrix(m_controllerTransform->getMatrix());

	//SPECIAL SHADOW TREATMENT:
	//1.:
	//If shadows are used, use this:
	//m_controllerToolPool = new ControllerToolPool(m_controllerTransformNotShadowed);
	//but this method have problems with the ruler tool world2local transformation->The ruler tool is shaking
	//2.:
	//If no shadows are used, use this:
	m_controllerToolPool = new ControllerToolPool(m_controllerTransform);
	//The ruler tool is fine now
	//I think it have something to do with post/pre multiplication and maybe the class MatrixTransform.
	//It seems that the computeLocalToWorld() in rulerControllerTool.cpp is executed to late or early... it get definitely not the correct data

	for (int buttonType = REAR_TRIGGER; buttonType != LAST; ++buttonType)
		m_buttonStates.insert(std::pair<int, BUTTON_STATE>(buttonType, IDLE)); //Initialize all objects in map so future checks are not necessary
}

Controller::~Controller()
{

}

void Controller::addToScenegraph(osg::ref_ptr<osg::Group> sceneRootIn, osg::ref_ptr<osg::Group> shadowedSceneIn)
{
	sceneRootIn->addChild(m_controllerTransformNotShadowed);
	shadowedSceneIn->addChild(m_controllerTransform);

	m_controllerTransform->addChild(m_controllerModel);
}

void Controller::updateMatrixTransform()
{
	m_controllerTransformNotShadowed->setMatrix(m_controllerTransform->getMatrix());
}

void Controller::addControllerToolForType(ControllerTool::CONTROLLER_TOOL_TYPE controllerToolTypeIn, ControllerTool* controllerToolIn)
{
	m_controllerToolPool->addControllerToolForType(controllerToolTypeIn, controllerToolIn);
}

void Controller::activateControllerToolForType(ControllerTool::CONTROLLER_TOOL_TYPE controllerToolTypeIn)
{
	m_controllerToolPool->activateControllerToolForType(controllerToolTypeIn);
}

void Controller::notifyControllerToolDeactivateForType(ControllerTool::CONTROLLER_TOOL_TYPE controllerToolTypeIn)
{
	m_controllerToolPool->notifyControllerToolDeactivateForType(controllerToolTypeIn);
}

ControllerTool * Controller::getControllerTool(ControllerTool::CONTROLLER_TOOL_TYPE controllerToolTypeIn)
{
	return m_controllerToolPool->getControllerTool(controllerToolTypeIn);
}