// ClayMore - Immersive Mesh Modelling --- Copyright (c) 2014-2017 Philipp Ladwig, Jannik Fiedler, Jan Beutgen
#include "NullController.h"
#include <iostream>

#include <osg\MatrixTransform>

NullController::NullController(unsigned int controllerID, CONTROLLER_TYPE controllerType) :
	Controller(controllerID, controllerType),
	m_lastTriggerPressure(0.0f)
{
	if (!m_controllerModel)
		m_controllerModel = osgDB::readNodeFile("./data/controller_models/vr_controller_vive_1_5.obj");
}

NullController::~NullController()
{

}

void NullController::update(void *controllerData)
{	
}