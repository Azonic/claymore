// ClayMore - Immersive Mesh Modelling --- Copyright (c) 2014-2017 Philipp Ladwig, Jannik Fiedler, Jan Beutgen
#include "ControllerToolPool.h"

#include <iostream>

ControllerToolPool::ControllerToolPool(osg::ref_ptr<osg::Group> parentNodeIn) :
	m_parentNode(parentNodeIn)
{ 
}

ControllerToolPool::~ControllerToolPool()
{
}

void ControllerToolPool::update()
{
	std::map<ControllerTool::CONTROLLER_TOOL_TYPE, ControllerTool*>::iterator coToIt = m_controllerTools.begin();

	while (coToIt != m_controllerTools.end())
	{
		if ((*coToIt).second->isActive())
			(*coToIt).second->update();

		++coToIt;
	}
}

void ControllerToolPool::addControllerToolForType(ControllerTool::CONTROLLER_TOOL_TYPE controllerToolTypeIn, ControllerTool* controllerToolIn)
{
	std::map<ControllerTool::CONTROLLER_TOOL_TYPE, ControllerTool*>::iterator coToIt = m_controllerTools.find(controllerToolTypeIn);

	if (coToIt != m_controllerTools.end())
		ConfigReader::printDebugMessage(" ### Warning in ControllerToolPool::addControllerToolForType() - There ist already a ControllerTool defined for this type and controller! Skipping this one...");

	else
		m_controllerTools.insert(std::pair<ControllerTool::CONTROLLER_TOOL_TYPE, ControllerTool*>(controllerToolTypeIn, controllerToolIn));
}

void ControllerToolPool::activateControllerToolForType(ControllerTool::CONTROLLER_TOOL_TYPE controllerToolTypeIn)
{
	std::map<ControllerTool::CONTROLLER_TOOL_TYPE, ControllerTool*>::iterator coToIt = m_controllerTools.find(controllerToolTypeIn);

	if (coToIt != m_controllerTools.end())
		((*coToIt).second->activate(m_parentNode));
}

void ControllerToolPool::notifyControllerToolDeactivateForType(ControllerTool::CONTROLLER_TOOL_TYPE controllerToolTypeIn)
{
	std::map<ControllerTool::CONTROLLER_TOOL_TYPE, ControllerTool*>::iterator coToIt = m_controllerTools.find(controllerToolTypeIn);

	if (coToIt != m_controllerTools.end())
		((*coToIt).second->notifyObserverDeactivated());
}

ControllerTool * ControllerToolPool::getControllerTool(ControllerTool::CONTROLLER_TOOL_TYPE controllerToolTypeIn)
{
	std::map<ControllerTool::CONTROLLER_TOOL_TYPE, ControllerTool*>::iterator coToIt = m_controllerTools.find(controllerToolTypeIn);

	if (coToIt != m_controllerTools.end())
		return (*coToIt).second;
	else
	{
		std::cout << "ERROR in ControllerToolPool: returning nullptr, because ControllerTool can't be found" << std::endl;
		return nullptr;
	}
	
}