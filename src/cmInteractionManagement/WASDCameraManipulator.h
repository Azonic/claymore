#pragma once

#include "ConfigDllExportInteractionManagement.h"

#include <osgGA/StandardManipulator>

class imInteractionManagement_DLL_import_export WASDCameraManipulator : public osgGA::StandardManipulator
{
public:
	// #### CONSTRUCTOR & DESTRUCTOR ###############
	WASDCameraManipulator(int flags = DEFAULT_SETTINGS);

	// #### MEMBER VARIABLES ###############
	osg::Vec3d m_eye;
	osg::Quat  m_rotation;
	float m_movementSpeed;

	// #### FUNCTIONS ###############
	virtual void setByMatrix(const osg::Matrixd& matrix);
	virtual void setByInverseMatrix(const osg::Matrixd& matrix);
	virtual osg::Matrixd getMatrix() const;
	virtual osg::Matrixd getInverseMatrix() const;

	virtual void setTransformation(const osg::Vec3d& eye, const osg::Quat& rotation);
	virtual void setTransformation(const osg::Vec3d& eye, const osg::Vec3d& center, const osg::Vec3d& up);
	virtual void getTransformation(osg::Vec3d& eye, osg::Quat& rotation) const;
	virtual void getTransformation(osg::Vec3d& eye, osg::Vec3d& center, osg::Vec3d& up) const;

	virtual void home(const osgGA::GUIEventAdapter& ea, osgGA::GUIActionAdapter& us);
	virtual void home(double);

	virtual void init(const osgGA::GUIEventAdapter& ea, osgGA::GUIActionAdapter& us);

protected:
	bool handleMouseDrag(const osgGA::GUIEventAdapter& ea, osgGA::GUIActionAdapter& us);
	bool performMovement();
	//bool handleKeyDown(const osgGA::GUIEventAdapter& ea, osgGA::GUIActionAdapter& us);
	bool handle(const osgGA::GUIEventAdapter& ea, osgGA::GUIActionAdapter& us);

	virtual bool performMovementLeftMouseButton(const double eventTimeDelta, const double dx, const double dy);
	virtual bool performMovementRightMouseButton(const double eventTimeDelta, const double dx, const double dy);
	virtual bool performMouseDeltaMovement(const float dx, const float dy);

	bool handleMouseWheel(const osgGA::GUIEventAdapter& ea, osgGA::GUIActionAdapter& us);

	std::vector<int> m_keysPressed;
};

