// ClayMore - Immersive Mesh Modelling --- Copyright (c) 2014-2017 Philipp Ladwig, Jannik Fiedler, Jan Beutgen
#include "ControllerTool.h"

ControllerTool::ControllerTool() :
	m_activeOnObserversCount(0)
{ 
	m_matrixTransform = new osg::MatrixTransform(); 
}
ControllerTool::~ControllerTool()
{
	detachFromSceneGraph();
}

void ControllerTool::attachToSceneGraph(osg::ref_ptr<osg::Group> parentNode)
{
	parentNode->addChild(m_matrixTransform);
}

void ControllerTool::detachFromSceneGraph()
{
	if (m_matrixTransform != nullptr)
	{
		std::vector<osg::Group*> parents = m_matrixTransform->getParents();

		std::vector<osg::Group*>::iterator paIt = parents.begin();

		while (paIt != parents.end())
		{
			(*paIt)->removeChild(m_matrixTransform);
			paIt++;
		}
	}
}

void ControllerTool::activate(osg::ref_ptr<osg::Group> parentNode)
{
	if (m_activeOnObserversCount == 0)
		attachToSceneGraph(parentNode);

	++m_activeOnObserversCount;
}

void ControllerTool::notifyObserverDeactivated()
{
	--m_activeOnObserversCount;

	if (m_activeOnObserversCount == 0)
		detachFromSceneGraph();
}
