// ClayMore - Immersive Mesh Modelling --- Copyright (c) 2014-2017 Philipp Ladwig, Jannik Fiedler, Jan Beutgen
#pragma once

#include "ConfigDllExportInteractionManagement.h"

#include <osgDB/ReadFile>

#include "Controller.h"
#include "ViveDefines.h"

#include <osg\MatrixTransform>

class imInteractionManagement_DLL_import_export ViveController : public Controller
{
public:
	ViveController(CONTROLLER_TYPE controllerType, vr::IVRSystem *ivrSystem);
	~ViveController();

	void setControllerIDByOpenVR();

	void update(void *controllerData = NULL);
	inline void setMatrixTransform(osg::ref_ptr<osg::MatrixTransform> matrixTransformIn)
	{
		m_controllerTransform->setMatrix(osg::Matrix::rotate(osg::DegreesToRadians(-90.0), osg::Vec3d(1.0, 0.0, 0.0)) * matrixTransformIn->getMatrix());
	}

	inline vr::IVRSystem * getVRSystem() { return m_ivrSystem; }

	void triggerHapticEvent(HAPTIC_EVENT hapticEvent);

	//TODO: We need a help and indication stuff class...this stuff here is not suitable for the controller class and should be more generic
	void renderHelpText();
	void removeHelpText();
	void updateIndicationGeometry(bool IsTriggerLeftIndicated, bool IsTriggerRightIndicated, bool IsSqueezeLeftIndicated, bool IsSqueezeRightIndicated);

private:

	vr::IVRSystem *m_ivrSystem;

	VIVE_BUTTON_ID m_lastButton;

	osg::ref_ptr<osg::MatrixTransform> m_helMatTransform;
	osg::ref_ptr<osg::Geode> m_helpTextGeode;
	float m_helpCharakterSize;
	osg::ref_ptr<osg::Geometry> m_linesForDescriptionText;

	osg::ref_ptr<osg::Geode> m_helpTextIndicationGeode;
	osg::ref_ptr<osg::Geometry> m_linesForIndicationText;

	osg::ref_ptr<osg::Vec3Array> m_lineVerticesForIndication;
	osg::ref_ptr<osg::DrawElementsUInt> m_linesForIndication;
	osg::ref_ptr<osg::Vec4Array> m_colorArrayForIndication;
};