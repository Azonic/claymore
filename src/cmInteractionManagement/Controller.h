// ClayMore - Immersive Mesh Modelling --- Copyright (c) 2014-2017 Philipp Ladwig, Jannik Fiedler, Jan Beutgen
#pragma once

#include "ConfigDllExportInteractionManagement.h"

#include "cmOsgOpenVRViewer/openvrviewer.h"
#include "cmOsgOpenVRViewer/openvreventhandler.h"

#include <osg/Vec3d>

#include <osg\MatrixTransform>

#include "ControllerToolPool.h"

#include "cmUtil/clayMoreGlobals.h"

enum BUTTON_STATE
{
	IDLE,		//Current and last frame no interaction
	HOLD,		//Current and last frame pressed
	PUSH,		//Current frame pressed and last frame no interaction
	RELEASE,	//Current frame no interaction and last frame pressed
	NONE		//Default; Required to check if any state was set
};

enum BUTTON_TYPE
{
	REAR_TRIGGER,
	JOYSTICK,		//On press on joystick
	MENU,			//Button above joystick
	SQUEEZE,
	LAST			//Leave this at the last position to allow iterating over all elements
};

enum HAPTIC_EVENT
{
	JOYSTICK_SHORT,
	JOYSTICK_LONG,
	JOYSTICK_TINY,
	TRIGGER_SHORT,
	TRIGGER_LONG,
	TRIGGER_TINY
};

class imInteractionManagement_DLL_import_export Controller
{
	public:
		Controller(unsigned int controllerID, CONTROLLER_TYPE controllerType);
		~Controller();

		virtual void update(void *controllerData) = 0;
		inline void updateControllerToolPool() { m_controllerToolPool->update(); }

		inline virtual void triggerHapticEvent(HAPTIC_EVENT hapticEvent) {}

		inline osg::Vec3d const & getPosition() { return m_position; }
		inline osg::Quat const & getRotation() { return m_rotation; }

		inline osg::ref_ptr<osg::Node> getControllerModel() { return m_controllerModel;  }
		inline unsigned int getControllerID() { return  m_controllerID; }

		inline osg::ref_ptr <osg::MatrixTransform> getMatrixTransform() { return m_controllerTransform; }
		inline osg::ref_ptr <osg::MatrixTransform> getMatrixTransformNotShadowed() { return m_controllerTransformNotShadowed; }

		void addToScenegraph(osg::ref_ptr<osg::Group> sceneRootIn, osg::ref_ptr<osg::Group> shadowedSceneIn);

		void updateMatrixTransform();

		float getJoystickX() { return m_joystickX; }
		float getJoystickY() { return m_joystickY; }

		void addControllerToolForType(ControllerTool::CONTROLLER_TOOL_TYPE controllerToolTypeIn, ControllerTool* controllerToolIn);
		void activateControllerToolForType(ControllerTool::CONTROLLER_TOOL_TYPE controllerToolTypeIn);
		void notifyControllerToolDeactivateForType(ControllerTool::CONTROLLER_TOOL_TYPE controllerToolTypeIn);
		ControllerTool * getControllerTool(ControllerTool::CONTROLLER_TOOL_TYPE controllerToolTypeIn);

		BUTTON_STATE getButtonState(BUTTON_TYPE buttonType) { return m_buttonStates.find(buttonType)->second; }

	protected:

		BUTTON_STATE & getButtonStateForAlteration(BUTTON_TYPE buttonType) { return m_buttonStates.find(buttonType)->second; }
		void updateButtonState(BUTTON_TYPE buttonType, BUTTON_STATE buttonState) { m_buttonStates.find(buttonType)->second = buttonState; }

		osg::Vec3d m_position;
		osg::Quat m_rotation;
		osg::ref_ptr<osg::MatrixTransform> m_controllerTransform;
		osg::ref_ptr<osg::MatrixTransform> m_controllerTransformNotShadowed;
		
		osg::ref_ptr<osg::Node> m_controllerModel;

		CONTROLLER_TYPE m_controllerType;
		unsigned int m_controllerID; //If using OpenVR- it will be set by OpenVR

		std::map<int, BUTTON_STATE> m_buttonStates;

		float m_joystickX, m_joystickY;

		ControllerToolPool* m_controllerToolPool;
};