// ClayMore - Immersive Mesh Modelling --- Copyright (c) 2014-2017 Philipp Ladwig, Jannik Fiedler, Jan Beutgen
#pragma once

#include "ConfigDllExportInteractionManagement.h"

#include "osg/Group"

#include "ControllerTool.h"

#include "..\cmUtil\ConfigReader.h"

class imInteractionManagement_DLL_import_export ControllerToolPool
{
	public:

		ControllerToolPool(osg::ref_ptr<osg::Group> parentNodeIn);
		~ControllerToolPool();

		//Will update all ControllerTools that are currently active
		void update();

		void addControllerToolForType(ControllerTool::CONTROLLER_TOOL_TYPE controllerToolType, ControllerTool* controllerToolIn);
		void activateControllerToolForType(ControllerTool::CONTROLLER_TOOL_TYPE controllerToolTypeIn);
		void notifyControllerToolDeactivateForType(ControllerTool::CONTROLLER_TOOL_TYPE controllerToolTypeIn);
		ControllerTool * getControllerTool(ControllerTool::CONTROLLER_TOOL_TYPE controllerToolTypeIn);

	private:

		osg::ref_ptr<osg::Group> m_parentNode;

		std::map<ControllerTool::CONTROLLER_TOOL_TYPE, ControllerTool*> m_controllerTools;
};