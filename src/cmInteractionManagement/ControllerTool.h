// ClayMore - Immersive Mesh Modelling --- Copyright (c) 2014-2017 Philipp Ladwig, Jannik Fiedler, Jan Beutgen
#pragma once

#include "ConfigDllExportInteractionManagement.h"

#include "osg/MatrixTransform"

class imInteractionManagement_DLL_import_export ControllerTool
{
	public:

		//On each controller, each CONTROLLER_TOOL_TYPE can only be used once in order to allow smooth activation/deactivation of ControllerTools
		enum CONTROLLER_TOOL_TYPE
		{
			SPHERE,
			PICK_RAY,
			VISUALIZE_AXIS_AND_GRID,
			VISUALIZE_BARYCENTRUM,
			RULER,
			DASHED_LINE,
			BOUNDING_BOX,
			THREE_D_ICON
		};

		ControllerTool();
		~ControllerTool();

		inline virtual void update() = 0;

		void activate(osg::ref_ptr<osg::Group> parentNode);
		void notifyObserverDeactivated();
		inline bool isActive() { return m_activeOnObserversCount > 0; }

	protected:

		//this will be incremented for each observer this tool is activated on
		//once this reaches 0, this tool will deactivate itself (similar principle to ref_ptr)
		int m_activeOnObserversCount;

		osg::ref_ptr<osg::MatrixTransform> m_matrixTransform;

	private:

		void attachToSceneGraph(osg::ref_ptr<osg::Group> parentNode);
		void detachFromSceneGraph();
};