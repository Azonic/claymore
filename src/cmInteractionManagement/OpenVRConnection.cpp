// ClayMore - Immersive Mesh Modelling --- Copyright (c) 2014-2017 Philipp Ladwig, Jannik Fiedler, Jan Beutgen
#include "OpenVRConnection.h"

#include <chrono>
#include <ctime>
#include <iomanip>

OpenVRConnection::OpenVRConnection(float nearClip, float farClip, const float worldUnitsPerMetre, const int sample) :
	OpenVRDevice(nearClip, farClip, worldUnitsPerMetre, sample)
{
	//TODO1: Move array size to config.xml
	std::cout << "### Sizeof TimedOpenVRMatrix: " << sizeof(TimedOpenVRMatrix) << std::endl;
	m_timedCircularTrackerMatrixBuffer = new boost::circular_buffer<TimedOpenVRMatrix>(100);
	m_timedCircularTrackerMatrixBuffer->push_back(TimedOpenVRMatrix(std::chrono::system_clock::now().time_since_epoch().count(), Matrix34()));
	m_timedCircularTrackerMatrixBuffer_Lock.store(false);
}

OpenVRConnection::~OpenVRConnection()
{

}

void OpenVRConnection::handleInput(std::vector<Controller*> const & primaryControllers,
	std::vector<Controller*> const & secondaryControllers,
	std::vector<Controller*> const & genericTrackerControllers)
{
	//vr::VRCompositor()->WaitGetPoses(m_trackedDevicePose, vr::k_unMaxTrackedDeviceCount, NULL, 0);
	vr::EVRCompositorError errorCode = vr::VRCompositor()->GetLastPoses(m_trackedDevicePose, vr::k_unMaxTrackedDeviceCount, NULL, 0);

	if (errorCode != 0)
	{
		std::cout << "#####################################################" << std::endl;
		std::cout << "Error Code OpenVR Connection: " << errorCode << std::endl;
		std::cout << "#####################################################" << std::endl;
	}

	bool foundPrimaryController = false;
	bool foundSecondaryController = false;
	bool isAgenericTrackerConnected = false;
	bool foundFirstGenericTracker = false;
	bool noticedNewConnectionOrDisconnectionOfDevice = false;
	unsigned int numberOfConnectedDevices = 0;

	for (vr::TrackedDeviceIndex_t unDevice = vr::k_unTrackedDeviceIndex_Hmd + 1; unDevice < vr::k_unMaxTrackedDeviceCount; ++unDevice)
	{
		if (m_trackedDevicePose[unDevice].bDeviceIsConnected) //If object is connected, process it
		{
			numberOfConnectedDevices++;
			vr::HmdMatrix34_t pose = m_trackedDevicePose[unDevice].mDeviceToAbsoluteTracking;

			if (vr::VRSystem()->GetTrackedDeviceClass(unDevice) == vr::TrackedDeviceClass_Controller)
			{
				for (std::vector<Controller*>::const_iterator CoIt = primaryControllers.cbegin(); CoIt != primaryControllers.cend(); CoIt++)
				{
					//std::cout << "OpenVRConnection -> TrackedDeviceClass_Controller + primaryControllers" << std::endl;
					Controller* controller = (*CoIt);
					if (unDevice == controller->getControllerID())
					{
						//std::cout << "OpenVRConnection -> Ja in primaryControllers" << std::endl;
						controller->update(&pose);
						foundPrimaryController = true;
						break;
					}
				}

				for (std::vector<Controller*>::const_iterator CoIt = secondaryControllers.cbegin(); CoIt != secondaryControllers.cend(); CoIt++)
				{
					//std::cout << "OpenVRConnection -> TrackedDeviceClass_Controller + secondaryControllers" << std::endl;
					Controller* controller = (*CoIt);
					if (unDevice == controller->getControllerID())
					{
						//std::cout << "OpenVRConnection -> Ja in secondaryControllers" << std::endl;
						controller->update(&pose);
						foundSecondaryController = true;
						break;
					}
				}
			}

			if (vr::VRSystem()->GetTrackedDeviceClass(unDevice) == vr::TrackedDeviceClass_GenericTracker)
			{
				isAgenericTrackerConnected = true;
				for (std::vector<Controller*>::const_iterator CoIt = genericTrackerControllers.cbegin(); CoIt != genericTrackerControllers.cend(); CoIt++)
				{
					//std::cout << "OpenVRConnection -> TrackedDeviceClass_GenericTracker" << std::endl;
					Controller* controller = (*CoIt);
					if (unDevice == controller->getControllerID())
					{
						//std::cout << "OpenVRConnection -> Ja in TrackedDeviceClass_GenericTracker" << std::endl;
						controller->update(&pose);

						auto now = std::chrono::system_clock::now();


						//auto ms = std::chrono::duration_cast<std::chrono::milliseconds>(now.time_since_epoch());
						//std::cout << "TrackerTime Milliseconds: " << std::fixed << ms.count() << std::endl;

						/*auto ns = std::chrono::duration_cast<std::chrono::nanoseconds>(now.time_since_epoch()) % 1000000000;
						std::cout << "TrackerTime Nanosec: " << ns.count() << std::endl;

						std::cout << "### Since 1970: " << std::fixed << now.time_since_epoch().count() << std::endl;*/

						Matrix34 m = *(Matrix34*)&pose;

						//double timestamp = std::chrono::duration_cast<std::chrono::milliseconds>(now.time_since_epoch()).count();
						unsigned long long timestamp = std::chrono::duration_cast<std::chrono::microseconds>(now.time_since_epoch()).count();
						TimedOpenVRMatrix timedOpenVRMatrix(timestamp, m);

						bool expected = false;
						const bool desired = true;
						if (m_timedCircularTrackerMatrixBuffer_Lock.compare_exchange_weak(expected, desired))
						{
							m_timedCircularTrackerMatrixBuffer->push_back(timedOpenVRMatrix);
							m_timedCircularTrackerMatrixBuffer_Lock.store(false);
						}



						/*std::cout << "##################" << std::endl;
						std::cout << "Front: " << m_timedCircularTrackerMatrixBuffer->front().timestamp << std::endl;
						std::cout << "Back: " << m_timedCircularTrackerMatrixBuffer->back().timestamp << std::endl;
						std::cout << "Diff: " << (m_timedCircularTrackerMatrixBuffer->back().timestamp - m_timedCircularTrackerMatrixBuffer->front().timestamp) / 1000000 << std::endl;
						std::cout << "##################" << std::endl;*/


						foundFirstGenericTracker = true;
						break;
					}
				}
			}
		}
	}

	//If these two vales not the same, then there was probably a connect/disconnect of an tracking device
	if (m_numberOfConnectedDevicesInLastFrame != numberOfConnectedDevices)
	{
		noticedNewConnectionOrDisconnectionOfDevice = true;
		//std::cout << "##################################Change!!!!!!" << std::endl;
	}
	m_numberOfConnectedDevicesInLastFrame = numberOfConnectedDevices;

	//Notice its outside of the 16x for loop
	//Check if one of both controllers are not tracked
	if (!foundPrimaryController || !foundSecondaryController || (isAgenericTrackerConnected
		&& !foundFirstGenericTracker) || noticedNewConnectionOrDisconnectionOfDevice)
	{
		//std::cout << "OpenVRConnection -> Controller fehlt oder Veränderung bemerkt" << std::endl;
		m_numberOfLoopsWithAmissigController++;
		if (m_numberOfLoopsWithAmissigController > 90) 	//If there are no controllers longer than at least one second (90 frames) -> try to find new IDs
		{
			for (std::vector<Controller*>::const_iterator CoIt = primaryControllers.cbegin(); CoIt != primaryControllers.cend(); CoIt++)
			{
				dynamic_cast<ViveController*>(*CoIt)->setControllerIDByOpenVR();
				std::cout << "					primary ControllerID zugewiesen: " << dynamic_cast<ViveController*>(*CoIt)->getControllerID() << std::endl;
			}
			for (std::vector<Controller*>::const_iterator CoIt = secondaryControllers.cbegin(); CoIt != secondaryControllers.cend(); CoIt++)
			{
				dynamic_cast<ViveController*>(*CoIt)->setControllerIDByOpenVR();
				std::cout << "					secondary ControllerID zugewiesen: " << dynamic_cast<ViveController*>(*CoIt)->getControllerID() << std::endl;
			}
			for (std::vector<Controller*>::const_iterator CoIt = genericTrackerControllers.cbegin(); CoIt != genericTrackerControllers.cend(); CoIt++)
			{
				dynamic_cast<ViveController*>(*CoIt)->setControllerIDByOpenVR();
				std::cout << "					tracker ControllerID zugewiesen: " << dynamic_cast<ViveController*>(*CoIt)->getControllerID() << std::endl;
			}
			m_numberOfLoopsWithAmissigController = 0;
		}
	}
	else
	{
		m_numberOfLoopsWithAmissigController = 0;
	}
}