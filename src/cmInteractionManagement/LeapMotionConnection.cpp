#include "LeapMotionConnection.h" 
#include "LeapMotionController.h"
#include "Leap.h"
#include <osg/ShapeDrawable>
#include <osgViewer/Viewer>


LeapMotionController leapMotionController;


//Constructor without mainViewer
LeapMotionConnection::LeapMotionConnection(osg::ref_ptr<osg::Group> sceneRootIn) : m_sceneRoot(sceneRootIn), m_mainViewer(nullptr)
{
	//a new Leap Motion connection is established!
}

//Constructor with mainViewer (For putting the Leap Motion hands in front of the HMD based on the HMD ViewMatrix from mainViewer)
LeapMotionConnection::LeapMotionConnection(osgViewer::Viewer* mainViewerIn)
	: m_sceneRoot(mainViewerIn->getSceneData()->asGroup()), m_mainViewer(mainViewerIn)
{

}

void LeapMotionConnection::onInit(const Leap::Controller& controller)
{
	std::cout << "LeapMotionConnection: Initialized" << std::endl;
}

void LeapMotionConnection::onConnect(const Leap::Controller& controller)
{
	leapMotionController = LeapMotionController(m_sceneRoot);
	std::cout << "LeapMotionConnection: LeapMotionController init" << std::endl;

}

void LeapMotionConnection::onDisconnect(const Leap::Controller& controller)
{
	// Note: not dispatched when running in a debugger.
	std::cout << "LeapMotionConnection: Disconnected" << std::endl;
}

void LeapMotionConnection::onExit(const Leap::Controller& controller)
{
	std::cout << "LeapMotionConnection: Exited" << std::endl;
}

void LeapMotionConnection::onFrame(const Leap::Controller& controller)
{
	//Getting the HMD-viewMatrix, if a HMD is used
	osg::Matrixd viewMatrix;
	if (m_mainViewer != nullptr)
	{
		std::vector<osg::Camera*> cameraList;
		m_mainViewer->getCameras(cameraList);

		if (cameraList.at(0))
		{
			//In viewMatrix.getTrans() and viewMatrix.getRotate() is stored the position of the HMD
			viewMatrix = cameraList.at(0)->getViewMatrix();
			leapMotionController.updateHandsWithHMDPosition(viewMatrix);
		}
	}
	
	/*std::cout << viewMatrix.getRotate().x() << std::endl;
	std::cout << viewMatrix.getRotate().y() << std::endl;
	std::cout << viewMatrix.getRotate().z() << std::endl;
	std::cout << viewMatrix.getRotate().w() << std::endl;*/

	// Get the most recent frame and report some basic information
	const Leap::Frame frame = controller.frame();
	Leap::HandList hands = frame.hands();

	//update the hands
	leapMotionController.updateHands(hands);
}

void LeapMotionConnection::onFocusGained(const Leap::Controller& controller)
{
	std::cout << "LeapMotionConnection: Focus Gained" << std::endl;
}

void LeapMotionConnection::onFocusLost(const Leap::Controller& controller)
{
	std::cout << "LeapMotionConnection: Focus Lost" << std::endl;
}

void LeapMotionConnection::onDeviceChange(const Leap::Controller& controller)
{
	std::cout << "LeapMotionConnection: Device Changed" << std::endl;
	const Leap::DeviceList devices = controller.devices();

	for (int i = 0; i < devices.count(); ++i) {
		std::cout << "id: " << devices[i].toString() << std::endl;
		std::cout << "  isStreaming: " << (devices[i].isStreaming() ? "true" : "false") << std::endl;
		std::cout << "  isSmudged:" << (devices[i].isSmudged() ? "true" : "false") << std::endl;
		std::cout << "  isLightingBad:" << (devices[i].isLightingBad() ? "true" : "false") << std::endl;
	}
}

void LeapMotionConnection::onServiceConnect(const Leap::Controller& controller) 
{
	std::cout << "LeapMotionConnection: Service Connected" << std::endl;
}

void LeapMotionConnection::onServiceDisconnect(const Leap::Controller& controller) 
{
	std::cout << "LeapMotionConnection: Service Disconnected" << std::endl;
}

void LeapMotionConnection::onServiceChange(const Leap::Controller& controller) 
{
	std::cout << "LeapMotionConnection: Service Changed" << std::endl;
}

void LeapMotionConnection::onDeviceFailure(const Leap::Controller& controller) 
{
	std::cout << "LeapMotionConnection: Device Error" << std::endl;
	const Leap::FailedDeviceList devices = controller.failedDevices();

	for (Leap::FailedDeviceList::const_iterator dl = devices.begin(); dl != devices.end(); ++dl) 
	{
		const Leap::FailedDevice device = *dl;
		std::cout << "  PNP ID:" << device.pnpId();
		std::cout << "    Failure type:" << device.failure();
	}
}

void LeapMotionConnection::onLogMessage(const Leap::Controller&, Leap::MessageSeverity s, int64_t t, const char* msg) 
{
	switch (s) {
	case Leap::MESSAGE_CRITICAL:
		std::cout << "[Critical]";
		break;
	case Leap::MESSAGE_WARNING:
		std::cout << "[Warning]";
		break;
	case Leap::MESSAGE_INFORMATION:
		std::cout << "[Info]";
		break;
	case Leap::MESSAGE_UNKNOWN:
		std::cout << "[Unknown]";
	}
	std::cout << "[" << t << "] ";
	std::cout << msg << std::endl;
}