// ClayMore - Immersive Mesh Modelling --- Copyright (c) 2014-2017 Philipp Ladwig, Jannik Fiedler, Jan Beutgen
#pragma once


void vertSelectMenuSelectVertices()
{
	SphereControllerTool* sphereControllerTool = static_cast<SphereControllerTool*>(g_interactionManager->getPrimaryController()->getControllerTool(ControllerTool::SPHERE));
	sphereControllerTool->setSphereColor(osg::Vec4(1.0f, 1.0f, 0.0f, 0.15f));

	registerSelectConfirmVerticesObserver();
}

void vertSelectMenuDeselectVertices()
{
	SphereControllerTool* sphereControllerTool = static_cast<SphereControllerTool*>(g_interactionManager->getPrimaryController()->getControllerTool(ControllerTool::SPHERE));
	sphereControllerTool->setSphereColor(osg::Vec4(0.8f, 0.0f, 0.0f, 0.15f));

	registerSelectNegateVerticesObserver();
}