// ClayMore - Immersive Mesh Modelling --- Copyright (c) 2014-2017 Philipp Ladwig, Jannik Fiedler, Jan Beutgen
#pragma once

//TODO3:This class can be avoided, if we can pass an argument to callback transmitting a bool for hideMenu true or false
//		If class would be vanish, we achive code reduction


void speechToggleSelectOrDeselectAllVertices()
{
	std::cout << "adadadasdad" << std::endl;
	if (g_geometryManager->getActiveGeometryData() != NULL)
		g_geometryManager->toggleSelectOrDeselectAll();
}

void speechToggleWireframe()
{
	if (g_geometryManager->getActiveGeometryData() != NULL)
		g_geometryManager->toggleWireframeOfActiveGeometry();
}

void speechToggleObjectEditMode()
{
	ObjectIntersectionObserver* objectIntersectionObserver = new ObjectIntersectionObserver();
	objectIntersectionObserver->setGeometryManager(g_geometryManager);

	unregisterObservers();
	g_interactionManager->registerObserver(CONTROLLER_TYPE::PRIMARY, Observer::CONSTANTLY_UPDATE, objectIntersectionObserver);
}

void speechLoadCube()
{
	g_dataManager->loadData("data/default_models/cube.obj");
}

void speechLoadSphere()
{
	g_dataManager->loadData("data/default_models/sphere.obj");
}

void speechLoadCylinder()
{
	g_dataManager->loadData("data/default_models/cylinder.obj");
}

void speechLoadCone()
{
	g_dataManager->loadData("data/default_models/cone.obj");
}

void speechDeleteSelectedVertices()
{
	if (g_geometryManager->getActiveGeometryData() != NULL)
		//TODO: Distinguish between delete vertices and faces command
		g_geometryManager->deleteSelectedMeshElements(true, true, true);

	registerSelectConfirmVerticesObserver();
}


void speechRegisterScaleVerticesObserver()
{
	ScaleVerticesObserver* scaleVerticesObserver = new ScaleVerticesObserver();
	scaleVerticesObserver->setGeometryManager(g_geometryManager);

	unregisterObservers();
	g_interactionManager->registerObserver(CONTROLLER_TYPE::PRIMARY, Observer::TRIGGER, scaleVerticesObserver);
}


void speechRegisterSelectVerticesObserver()
{
	SphereVertexProspectObserver* sphereVertexProspectObserver = new SphereVertexProspectObserver();
	sphereVertexProspectObserver->setGeometryManager(g_geometryManager);

	SelectVerticesObserver* selectVerticesObserver = new SelectVerticesObserver();
	selectVerticesObserver->setGeometryManager(g_geometryManager);

	SelectVerticesNegateObserver* selectVerticesNegateObserver = new SelectVerticesNegateObserver();
	selectVerticesNegateObserver->setGeometryManager(g_geometryManager);

	unregisterObservers();
	g_interactionManager->registerObserver(CONTROLLER_TYPE::PRIMARY, Observer::TRIGGER, selectVerticesObserver);
	g_interactionManager->registerObserver(CONTROLLER_TYPE::PRIMARY, Observer::MENUBUTTON, selectVerticesNegateObserver);
	g_interactionManager->registerObserver(CONTROLLER_TYPE::PRIMARY, Observer::CONSTANTLY_UPDATE, sphereVertexProspectObserver);

	//Get Rumble, Trigger event
	//dynamic_cast<ViveController*>(g_primaryController)->getVRSystem()->TriggerHapticPulse(4, 0, 2500);
}


