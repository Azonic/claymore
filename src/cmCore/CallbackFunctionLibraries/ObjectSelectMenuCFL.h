// ClayMore - Immersive Mesh Modelling --- Copyright (c) 2014-2017 Philipp Ladwig, Jannik Fiedler, Jan Beutgen
#pragma once

//#include "osg/ComputeBoundsVisitor"


void OSMCF_joinSelectedObjects()
{
	//Go through all geometries...
	for (unsigned int i = 0; i < g_geometryManager->getNumChildren(); i++)
	{
		//--and if a geom is selected go...
		if (dynamic_cast<GeometryData*>(g_geometryManager->getChild(i))->getIsSelected())
		{
			//...and make sure, that I don't join the active geom into itself ->will lead to crash, of course...
			if (!g_geometryManager->getNameOfActiveGeometry().compare(g_geometryManager->getChild(i)->getName()) == 0)
			{
				//...well, get one of the selected geometries and join it to the active ("active" is equal to the last selected object)...
				g_geometryManager->joinGeometries(g_geometryManager->
					getGeometryDataByName(g_geometryManager->getChild(i)->getName())->getOpenMeshGeo());
				//...and mark it as deleted (it's akain to OpenMesh deleting of mesh items; 
				//it's important because it's not possible to change the for() conditions while beeing in the loop...
				g_geometryManager->deleteGeometry(g_geometryManager->getChild(i)->getName());
			}
		}
	}
	//..finally delete the marked objects...
	g_geometryManager->garbageCollection();

	g_geometryManager->getActiveGeometryData()->updateBoundingBox();

	activateObserversForMenus();
	registerTranslateSelectedObjectObserver();

	g_geometryManager->updateWireframeStatesForAllGeometries();
}

void OSMCF_deleteSelectedObjects()
{

	for (unsigned int i = 0; i < g_geometryManager->getNumChildren(); i++)
	{
		if (dynamic_cast<GeometryData*>(g_geometryManager->getChild(i))->getIsSelected())
		{
			g_geometryManager->deleteGeometry(g_geometryManager->getChild(i)->getName());
		}
	}
	//Important: Call garbageCollection for "real" deleting the object, otherwise they are only marked as deleted (similar to OpenMesh Garbage Collection)
	g_geometryManager->garbageCollection();

	

	if(g_geometryManager->getNumChildren() == 0)
	{
		std::cout << "heul doch" << std::endl;
		g_geometryManager->setActiveGeometry(g_stringNothingInSceneAssertion);
		showAddMeshOBJModeMenu();
	}
	else
	{
		g_geometryManager->setActiveGeometry(g_stringNoSelectionAssertion);
		activateObserversForMenus();
	}

	g_geometryManager->updateWireframeStatesForAllGeometries();
}

//TODO3: Currently no multi selection is available
void OSMCF_duplicateSelectedObjects()
{
	//There is a experimental deep copy function, but it stills crash...
	//g_geometryManager->deepCopySelectedObjects();

	// Maybe there is also a more classical way by coping the vertex stuff in duoplicateProcessor and separate it afterwards
	for (unsigned int i = 0; i < g_geometryManager->getNumChildren(); i++)
	{
		if (dynamic_cast<GeometryData*>(g_geometryManager->getChild(i))->getIsSelected())
		{
			g_geometryManager->addGeometry(osg::Vec3f(0.1f, 0.1f, 0.1f));

			GeometryData* dataToCopy = dynamic_cast<GeometryData*>(g_geometryManager->getChild(i));
			g_geometryManager->getActiveGeometryData()->getOpenMeshGeo()->setVertexArray(
				dataToCopy->getOpenMeshGeo()->getVertexArray());
			g_geometryManager->getActiveGeometryData()->getOpenMeshGeo()->addPrimitiveSet(
				dataToCopy->getOpenMeshGeo()->getPrimitiveSet(0));

			osg::ref_ptr<osg::Vec4Array> colorArray = new osg::Vec4Array();
			for (unsigned int i = 0; i < dataToCopy->getOpenMeshGeo()->getVertexArray()->getNumElements(); i++)
			{
				colorArray->push_back(osg::Vec4(0.0f, 0.0f, 0.0f, 1.0f));
			}
			g_geometryManager->getActiveGeometryData()->getOpenMeshGeo()->setColorArray(colorArray);
			g_geometryManager->getActiveGeometryData()->getOpenMeshGeo()->setColorBinding(osg::Geometry::BIND_PER_VERTEX);

			//EXP: Call initializeAndDisplay() not before you've assign a VertexArray and a PrimitiveSet.
			g_geometryManager->getActiveGeometryData()->initializeAndDisplay();
			//m_geometryManager->readFileFromDisk(filename);
			g_geometryManager->getActiveGeometryData()->getGeometryVisualization()->selectTechnique(FLAT_SHADING);
		}
	}
	activateObserversForMenus();
	registerTranslateSelectedObjectObserver();
	g_geometryManager->getActiveGeometryData()->updateBoundingBox();
	g_geometryManager->updateWireframeStatesForAllGeometries();
}