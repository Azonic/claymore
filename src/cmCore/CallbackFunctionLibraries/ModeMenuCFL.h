// ClayMore - Immersive Mesh Modelling --- Copyright (c) 2014-2017 Philipp Ladwig, Jannik Fiedler, Jan Beutgen
#pragma once

void activateMenuStructureForObjectMode()
{
	g_miscDataStorage->setGlobalApplicationMode(OBJECT_MODE);

	//TODO: Possible to deactivate the triggerCallback in Menu, as well
	//Asks if is something selected
	if (g_geometryManager->getNameOfActiveGeometry().compare(g_stringNoSelectionAssertion) != 0 && g_geometryManager->getNameOfActiveGeometry().compare(g_stringNothingInSceneAssertion) != 0)
	{
		g_geometryManager->getActiveGeometryData()->setIsSelected(true);
		g_menuManager->hideAllMenus();

		TouchMenuObserver *primTouchMenuObserver = new TouchMenuObserver(TOUCH_OBJMODE_SELECTION, g_menuManager, g_miscDataStorage);
		g_primaryLastMenu = TOUCH_OBJMODE_SELECTION;

		TouchMenuObserver *secTouchMenuObserver = new TouchMenuObserver(TOUCH_OBJMODE_MAIN, g_menuManager, g_miscDataStorage);
		g_secondaryLastMenu = TOUCH_OBJMODE_MAIN;

		g_menuManager->hideAllMenus();
		g_menuManager->showMenuWithUniqueName(TOUCH_OBJMODE_SELECTION);
		g_menuManager->showMenuWithUniqueName(TOUCH_OBJMODE_MAIN);

		ObjectIntersectionObserver* objectIntersectionObserver = new ObjectIntersectionObserver();
		objectIntersectionObserver->setGeometryManager(g_geometryManager);

		unregisterObservers();
		registerTranslateSelectedObjectObserver();
		g_interactionManager->registerObserver(CONTROLLER_TYPE::PRIMARY, Observer::PRIMARY_JOYSTICK_MENU, primTouchMenuObserver);
		g_interactionManager->registerObserver(CONTROLLER_TYPE::PRIMARY, Observer::CONSTANTLY_UPDATE, objectIntersectionObserver);
		g_interactionManager->registerObserver(CONTROLLER_TYPE::SECONDARY, Observer::SECONDARY_JOYSTICK_MENU, secTouchMenuObserver);

		//Set back shaders to objectMode typical apperience for selected and active
		for (unsigned int i = 0; i < g_geometryManager->getNumChildren(); ++i)
		{
			if (dynamic_cast<GeometryData*>(g_geometryManager->getChild(i))->getIsSelected())
			{
				dynamic_cast<GeometryData*>(g_geometryManager->getChild(i))->getGeometryVisualization()->selectTechnique(INDICATED_AS_SELECTED_SHADING);
			}
			else
			{
				dynamic_cast<GeometryData*>(g_geometryManager->getChild(i))->getGeometryVisualization()->selectTechnique(FLAT_SHADING);
			}
			dynamic_cast<GeometryData*>(g_geometryManager->getChild(i))->detachFilthyLinesGeode();
			dynamic_cast<GeometryData*>(g_geometryManager->getChild(i))->detachFilthyPointsGeode();
		}
		g_geometryManager->getActiveGeometryData()->getGeometryVisualization()->selectTechnique(INDICATED_AS_SELECTED_AND_ACTIVE_SHADING);

		g_geometryManager->getActiveGeometryData()->updateBoundingBox();
	}

	if (g_geometryManager->getNameOfActiveGeometry().compare(g_stringNothingInSceneAssertion) == 0)
	{
		showAddMeshOBJModeMenu();
	}
	//else
	//{
	//	g_geometryManager->updateWireframeStatesForAllGeometries();
	//}
}

void activateMenuStructureForEditMode()
{
	g_miscDataStorage->setGlobalApplicationMode(EDIT_MODE);

	//g_geometryManager->updateBoundingBoxes();
	//TODO: Possible to deactivate the triggerCallback in Menu, as well
	//Asks if is something selected
	if (g_geometryManager->getNameOfActiveGeometry().compare(g_stringNoSelectionAssertion) != 0 && g_geometryManager->getNameOfActiveGeometry().compare(g_stringNothingInSceneAssertion) != 0)
	{
		g_menuManager->hideAllMenus();
		unregisterObservers();

		TouchMenuObserver *secTouchMenuObserver = new TouchMenuObserver(TOUCH_EDIMODE_MAIN, g_menuManager, g_miscDataStorage);
		g_secondaryLastMenu = TOUCH_EDIMODE_MAIN;

		SelectionSphereTouchMenuObserver *primTouchMenuObserver = new SelectionSphereTouchMenuObserver(TOUCH_EDIMODE_SELECTION);
		primTouchMenuObserver->setMenuManager(g_menuManager);
		primTouchMenuObserver->setMiscDataStorage(g_miscDataStorage);
		g_primaryLastMenu = TOUCH_EDIMODE_SELECTION;

		g_menuManager->showMenuWithUniqueName(TOUCH_EDIMODE_MAIN);
		g_menuManager->showMenuWithUniqueName(TOUCH_EDIMODE_SELECTION);

		registerSelectConfirmVerticesObserver();
		registerTranslateSelectedObjectObserver();

		g_interactionManager->registerObserver(CONTROLLER_TYPE::PRIMARY, Observer::PRIMARY_JOYSTICK_MENU, primTouchMenuObserver);
		g_interactionManager->registerObserver(CONTROLLER_TYPE::SECONDARY, Observer::SECONDARY_JOYSTICK_MENU, secTouchMenuObserver);

		g_geometryManager->updateWireframeStatesForAllGeometries();
	}
}

void activateSculptMode()
{
	if (!(g_geometryManager->getNameOfActiveGeometry().compare(g_stringNoSelectionAssertion) == 0))
	{
		unregisterObservers();
		g_menuManager->hideAllMenus();
		//TODO: Activating sculpting is very bugy!
		//registerSculptAddObserver();
	}
}

// ### ScanMode ###
void activateScanMode()
{
	g_miscDataStorage->setGlobalApplicationMode(SCAN_MODE);
	//g_geometryManager->updateBoundingBoxes();
	//TODO: Possible to deactivate the triggerCallback in Menu, as well
	//Asks if is something selected
	
	g_menuManager->hideAllMenus();
	unregisterObservers();

	TiltMenuObserver* tiltMenuObserver = new TiltMenuObserver(TILT_AND_SHIFT_SCAN_MODE, g_isEasyModeForTiltMenuActivated);
	g_interactionManager->registerObserver(CONTROLLER_TYPE::SECONDARY, Observer::SQUEEZE, tiltMenuObserver);
	tiltMenuObserver->setGeometryManager(g_geometryManager);
	tiltMenuObserver->setMenuManager(g_menuManager);
	tiltMenuObserver->setMiscDataStorage(g_miscDataStorage);
	tiltMenuObserver->setCustomBehaviourOnNotify([](TiltMenu* tiltMenu, MiscDataStorage* miscDataStorage) -> void {
		//Highlight the approriate text, depending which mode is currently active
		static_cast<TiltMenuButton*>(tiltMenu->getMenuButton(0))->highlightTextColor(miscDataStorage->getScanContext() == ScanContext::POINT_CLOUD_MODE); //POINT CLOUD MODE
		static_cast<TiltMenuButton*>(tiltMenu->getMenuButton(10))->highlightTextColor(miscDataStorage->getScanContext() == ScanContext::OCTREE_MODE); //OCTREE MODE
		static_cast<TiltMenuButton*>(tiltMenu->getMenuButton(1))->highlightTextColor(miscDataStorage->getScanContext() == ScanContext::MARCHING_CUBES_MODE); //MARCHING CUBES MODE
	});

	/* ScanMode Functions:
	-PointCloud
		-Scanning	: TRIGGER
		-Undo		: TOUCH LEFT
		-Clear		: FENCE MENU TOUCH UP
		-Show/Hide	: TOUCH DOWN
	-Octree
		-Draw		: TOUCH RIGHT
		-Clear		: FENCE MENU TOUCH UP
		-DDA-Demo	: TRIGGER
		-Show/Hide	: TOUCH DOWN
	-Marching Cubes
		-Execute	: TOUCH RIGHT
		-Clear		: FENCE MENU TOUCH UP
		-Show/Hide	: TOUCH DOWN
	*/
	switch (g_miscDataStorage->getScanContext()) {
		case ScanContext::OCTREE_MODE: // Draw Octree related menus
		{
			g_menuManager->showMenuWithUniqueName(TOUCH_SCAN_OCTREE);
			TouchMenuObserver* secTouchMenuObserver = new TouchMenuObserver(TOUCH_SCAN_OCTREE, g_menuManager, g_miscDataStorage);
			g_interactionManager->registerObserver(CONTROLLER_TYPE::SECONDARY, Observer::SECONDARY_JOYSTICK_MENU, secTouchMenuObserver);
			g_primaryLastMenu = g_secondaryLastMenu = TOUCH_SCAN_OCTREE;
			registerDDATraverseObserver();
			break;
		}
		case ScanContext::MARCHING_CUBES_MODE: // Draw Marching Cubes related menus
		{
			g_menuManager->showMenuWithUniqueName(TOUCH_SCAN_POLYGONIZE);
			TouchMenuObserver* secTouchMenuObserver = new TouchMenuObserver(TOUCH_SCAN_POLYGONIZE, g_menuManager, g_miscDataStorage);
			g_interactionManager->registerObserver(CONTROLLER_TYPE::SECONDARY, Observer::SECONDARY_JOYSTICK_MENU, secTouchMenuObserver);
			g_primaryLastMenu = g_secondaryLastMenu = TOUCH_SCAN_POLYGONIZE;
			break;
		}
		default: // POINT_CLOUD_MODE: Draw PointCloud related menus
		{
			g_menuManager->showMenuWithUniqueName(TOUCH_SCAN_POINT_CLOUD);
			TouchMenuObserver* secTouchMenuObserver = new TouchMenuObserver(TOUCH_SCAN_POINT_CLOUD, g_menuManager, g_miscDataStorage);
			g_interactionManager->registerObserver(CONTROLLER_TYPE::SECONDARY, Observer::SECONDARY_JOYSTICK_MENU, secTouchMenuObserver);
			g_primaryLastMenu = g_secondaryLastMenu = TOUCH_SCAN_POINT_CLOUD;
			registerScanPointCloudObserver();
			break;
		}
	}
	//g_geometryManager->updateWireframeStatesForAllGeometries();
}

void activateScanContextPointCloud()
{
	g_miscDataStorage->setScanContext(ScanContext::POINT_CLOUD_MODE);
	activateScanMode();
}

void activateScanContextOctree()
{
	g_miscDataStorage->setScanContext(ScanContext::OCTREE_MODE);
	activateScanMode();
}

void activateScanContextPolygonize()
{
	g_miscDataStorage->setScanContext(ScanContext::MARCHING_CUBES_MODE);
	activateScanMode();
}

// ######


void activatePaintingMode()
{
	if (!(g_geometryManager->getNameOfActiveGeometry().compare(g_stringNoSelectionAssertion) == 0))
	{

	}
	std::cout << "Still empty callback" << std::endl;
}

void activateSkinningRiggingMode()
{
	std::cout << "Still empty callback in activateSkinningRiggingMode" << std::endl;
	if (!(g_geometryManager->getNameOfActiveGeometry().compare(g_stringNoSelectionAssertion) == 0))
	{

	}
}