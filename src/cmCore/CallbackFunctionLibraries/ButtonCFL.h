//ClayMore - Immersive Mesh Modelling --- Copyright (c) 2014-2017 Philipp Ladwig, Jannik Fiedler, Jan Beutgen
#pragma once

//TODO:Split up this Library in EditModeCFL and ObjectModeCFL

//TODO: Remove ctime later, its only for Saving the OBJ with a unique time stamp
#include <ctime>

void activateObserversForMenus();

// General
void emptyCallback()
{
	std::cout << "Empty Callback triggered" << std::endl;
	//std::cout << "Current RenderTechnique: " << g_geometryManager->getActiveGeometryData()->getGeometryVisualization()->getSelectedTechnique() << std::endl;
	g_geometryManager->updateWireframeStatesForAllGeometries();
}

//void menuBack()
//{
//	g_menuManager->hideMenuWithUniqueName(PIE_EDIT);
//	g_menuManager->hideMenuWithUniqueName(PIE_OBJECT);
//}
//
//void menuBack2()
//{
//	g_menuManager->getMenuWithUniqueName(PIE_EDIT)->hideAllSubMenus();
//	g_menuManager->getMenuWithUniqueName(PIE_OBJECT)->hideAllSubMenus();
//}

void unregisterObservers()
{

	g_interactionManager->unregisterObserver(CONTROLLER_TYPE::PRIMARY, Observer::TRIGGER);
	//g_interactionManager->unregisterObserver(CONTROLLER_TYPE::PRIMARY, Observer::MENUBUTTON); //Last Tool observer
	g_interactionManager->unregisterObserver(CONTROLLER_TYPE::PRIMARY, Observer::CONSTANTLY_UPDATE);
	//
	//Dont unregister, cause of scaling
	//g_interactionManager->unregisterObserver(CONTROLLER_TYPE::PRIMARY, Observer::SQUEEZE);
	//g_interactionManager->unregisterObserver(CONTROLLER_TYPE::PRIMARY, Observer::MODE_MENU);
	g_interactionManager->unregisterObserver(CONTROLLER_TYPE::PRIMARY, Observer::PRIMARY_JOYSTICK_MENU);
	g_interactionManager->unregisterObserver(CONTROLLER_TYPE::PRIMARY, Observer::SECONDARY_JOYSTICK_MENU);
	//

	g_interactionManager->unregisterObserver(CONTROLLER_TYPE::SECONDARY, Observer::TRIGGER);
	g_interactionManager->unregisterObserver(CONTROLLER_TYPE::SECONDARY, Observer::MENUBUTTON);
	g_interactionManager->unregisterObserver(CONTROLLER_TYPE::SECONDARY, Observer::CONSTANTLY_UPDATE);
	//

	//Should be always registered for Speech Recognition
	//g_interactionManager->unregisterObserver(CONTROLLER_TYPE::SECONDARY, Observer::SQUEEZE);
	//Should be always registered for MainMenu (Object-, Edit-, etc modes)
	//g_interactionManager->unregisterObserver(CONTROLLER_TYPE::SECONDARY, Observer::MODE_MENU);
	g_interactionManager->unregisterObserver(CONTROLLER_TYPE::SECONDARY, Observer::PRIMARY_JOYSTICK_MENU);
	g_interactionManager->unregisterObserver(CONTROLLER_TYPE::SECONDARY, Observer::SECONDARY_JOYSTICK_MENU);
	//
}

//Has no unregister observers
void registerTranslateSelectedObjectObserver()
{
	//std::cout << "registerTranslateSelectedObjectObserver ++++-- +++" << std::endl;
	TranslateAndRotateSelectedObserver* translateAndRotateSelectedObserver = new TranslateAndRotateSelectedObserver();
	translateAndRotateSelectedObserver->setGeometryManager(g_geometryManager);
	translateAndRotateSelectedObserver->setMenuManager(g_menuManager);
	translateAndRotateSelectedObserver->setInteractionManager(g_interactionManager);

	//Dont know exactly why, but I have to set m_waitTriggerStateHasChanged = true in CTOR, otherwise the object will "glitch" around after fence menu closing...
	//...and the following line doesn't effect anything.... but actually it should effect the bool, because InteractionManagement->detectInteractionsAndNotify()...
	//.. should be called only next frame...
	translateAndRotateSelectedObserver->waitAndDoNothingUntilTriggerPressedAgain(true);

	g_interactionManager->registerObserver(CONTROLLER_TYPE::SECONDARY, Observer::SQUEEZE, translateAndRotateSelectedObserver);
}

// Vertices
void registerSelectConfirmVerticesObserver()
{
	SphereVertexProspectObserver* sphereVertexProspectObserver = new SphereVertexProspectObserver();
	sphereVertexProspectObserver->setGeometryManager(g_geometryManager);

	SelectVerticesObserver* selectVerticesObserver = new SelectVerticesObserver();
	selectVerticesObserver->setGeometryManager(g_geometryManager);
	selectVerticesObserver->waitAndDoNothingUntilTriggerPressedAgain(true);

	GrabVerticesObserver* grabVerticesObserver = new GrabVerticesObserver();
	grabVerticesObserver->setGeometryManager(g_geometryManager);
	grabVerticesObserver->setMiscDataStorage(g_miscDataStorage);
	grabVerticesObserver->setMenuManager(g_menuManager);
	grabVerticesObserver->setInteractionManager(g_interactionManager);
	grabVerticesObserver->setHistoryManager(g_historyManager);
	g_geometryManager->resetConstraint();


	//Register Secondary Trans Observer again
	//TranslateSelectedObjectObserver* translateSelectedObjectObserver = new TranslateSelectedObjectObserver();
	//translateSelectedObjectObserver->setGeometryManager(g_geometryManager);
	//translateSelectedObjectObserver->setMenuManager(g_menuManager);
	//translateSelectedObjectObserver->setInteractionManager(g_interactionManager);

	g_interactionManager->registerObserver(CONTROLLER_TYPE::PRIMARY, Observer::TRIGGER, selectVerticesObserver);
	g_interactionManager->registerObserver(CONTROLLER_TYPE::PRIMARY, Observer::CONSTANTLY_UPDATE, sphereVertexProspectObserver);
	//g_interactionManager->registerObserver(CONTROLLER_TYPE::SECONDARY, Observer::TRIGGER, translateSelectedObjectObserver);


	g_menuManager->hideAllMenus();
	g_menuManager->showMenuWithUniqueName(g_primaryLastMenu);
	g_menuManager->showMenuWithUniqueName(g_secondaryLastMenu);

	SelectionSphereTouchMenuObserver *primTouchMenuObserver = new SelectionSphereTouchMenuObserver(g_primaryLastMenu);
	primTouchMenuObserver->setMenuManager(g_menuManager);
	primTouchMenuObserver->setMiscDataStorage(g_miscDataStorage);

	TouchMenuObserver *secTouchMenuObserver = new TouchMenuObserver(g_secondaryLastMenu, g_menuManager, g_miscDataStorage);

	g_interactionManager->registerObserver(CONTROLLER_TYPE::PRIMARY, Observer::PRIMARY_JOYSTICK_MENU, primTouchMenuObserver);
	g_interactionManager->registerObserver(CONTROLLER_TYPE::SECONDARY, Observer::SECONDARY_JOYSTICK_MENU, secTouchMenuObserver);

	SphereControllerTool* sphereControllerTool = static_cast<SphereControllerTool*>(g_interactionManager->getPrimaryController()->getControllerTool(ControllerTool::SPHERE));
	sphereControllerTool->setSphereColor(osg::Vec4(1.0f, 1.0f, 0.0f, 0.15f));

	g_miscDataStorage->activateEDISelectionConfirmTool();
}

void registerSelectNegateVerticesObserver()
{
	SphereVertexProspectObserver* sphereVertexProspectObserver = new SphereVertexProspectObserver();
	sphereVertexProspectObserver->setGeometryManager(g_geometryManager);

	SelectVerticesNegateObserver* selectVerticesNegateObserver = new SelectVerticesNegateObserver();
	selectVerticesNegateObserver->setGeometryManager(g_geometryManager);

	GrabVerticesObserver* grabVerticesObserver = new GrabVerticesObserver();
	grabVerticesObserver->setGeometryManager(g_geometryManager);
	grabVerticesObserver->setMiscDataStorage(g_miscDataStorage);
	grabVerticesObserver->setMenuManager(g_menuManager);
	grabVerticesObserver->setInteractionManager(g_interactionManager);
	grabVerticesObserver->setHistoryManager(g_historyManager);
	g_geometryManager->resetConstraint();

	//Register Secondary Trans Observer again
	//TranslateSelectedObjectObserver* translateSelectedObjectObserver = new TranslateSelectedObjectObserver();
	//translateSelectedObjectObserver->setGeometryManager(g_geometryManager);
	//translateSelectedObjectObserver->setMenuManager(g_menuManager);
	//translateSelectedObjectObserver->setInteractionManager(g_interactionManager);

	unregisterObservers();
	g_interactionManager->registerObserver(CONTROLLER_TYPE::PRIMARY, Observer::TRIGGER, selectVerticesNegateObserver);
	g_interactionManager->registerObserver(CONTROLLER_TYPE::PRIMARY, Observer::CONSTANTLY_UPDATE, sphereVertexProspectObserver);
	//g_interactionManager->registerObserver(CONTROLLER_TYPE::SECONDARY, Observer::TRIGGER, translateSelectedObjectObserver);

	g_menuManager->hideAllMenus();
	g_menuManager->showMenuWithUniqueName(g_primaryLastMenu);
	g_menuManager->showMenuWithUniqueName(g_secondaryLastMenu);

	SelectionSphereTouchMenuObserver *primTouchMenuObserver = new SelectionSphereTouchMenuObserver(g_primaryLastMenu);
	primTouchMenuObserver->setMenuManager(g_menuManager);
	primTouchMenuObserver->setMiscDataStorage(g_miscDataStorage);

	TouchMenuObserver *secTouchMenuObserver = new TouchMenuObserver(g_secondaryLastMenu, g_menuManager, g_miscDataStorage);

	g_interactionManager->registerObserver(CONTROLLER_TYPE::PRIMARY, Observer::PRIMARY_JOYSTICK_MENU, primTouchMenuObserver);
	g_interactionManager->registerObserver(CONTROLLER_TYPE::SECONDARY, Observer::SECONDARY_JOYSTICK_MENU, secTouchMenuObserver);

	SphereControllerTool* sphereControllerTool = static_cast<SphereControllerTool*>(g_interactionManager->getPrimaryController()->getControllerTool(ControllerTool::SPHERE));
	sphereControllerTool->setSphereColor(osg::Vec4(0.8f, 0.0f, 0.0f, 0.15f));

	g_miscDataStorage->activateEDISelectionNegateTool();
}


void registerAddEdgeLineObserver()
{
	g_primaryMenuBeforLastMenu = g_primaryLastMenu;
	g_primaryLastMenu = TOUCH_EDIMODE_ADD_MESH_ELEMENTS;

	AddEdgeLineObserver* addEdgeLineObserver = new AddEdgeLineObserver();
	addEdgeLineObserver->setGeometryManager(g_geometryManager);

	//TouchMenuObserver *touchMenuObserver = new TouchMenuObserver(TOUCH_OBJMODE_MAIN);
	//touchMenuObserver->setMenuManager(g_menuManager);

	g_menuManager->hideAllMenus();
	//g_menuManager->showMenuWithUniqueName(TOUCH_OBJMODE_MAIN);

	unregisterObservers();
	g_interactionManager->registerObserver(CONTROLLER_TYPE::PRIMARY, Observer::TRIGGER, addEdgeLineObserver);
	//g_interactionManager->registerObserver(Controller::SECONDARY, Observer::SECONDARY_JOYSTICK_MENU, touchMenuObserver);

	registerTranslateSelectedObjectObserver();
	activateObserversForMenus();
}

void registerScanPointCloudObserver()
{
	ScanPointCloudObserver* scanPointCloudObserver = new ScanPointCloudObserver();
	scanPointCloudObserver->setPointCloudManager(g_pointCloudManager);
	scanPointCloudObserver->waitAndDoNothingUntilTriggerPressedAgain(true);
	g_interactionManager->registerObserver(CONTROLLER_TYPE::SECONDARY, Observer::TRIGGER, scanPointCloudObserver);
}

void registerDDATraverseObserver()
{
	DDATraverseObserver* ddaTraverseObserver = new DDATraverseObserver();
	ddaTraverseObserver->setPointCloudManager(g_pointCloudManager);
	ddaTraverseObserver->setControllerTarget(g_interactionManager->getGenericTracker());
	ddaTraverseObserver->waitAndDoNothingUntilTriggerPressedAgain(true);
	g_interactionManager->registerObserver(CONTROLLER_TYPE::SECONDARY, Observer::TRIGGER, ddaTraverseObserver);
}

//"Defensive" function -> has no unregisterObserver(), so its only "additive"
void activateObserversForMenus()
{
	g_geometryManager->sphereSelectCleanUpVerticesColor();
	if (g_primaryLastMenu == TOUCH_OBJMODE_SELECTION)
	{
		g_menuManager->hideAllMenus();
		g_menuManager->showMenuWithUniqueName(g_primaryLastMenu);
		g_menuManager->showMenuWithUniqueName(g_secondaryLastMenu);

		TouchMenuObserver *primTouchMenuObserver = new TouchMenuObserver(g_primaryLastMenu, g_menuManager, g_miscDataStorage);

		ObjectIntersectionObserver* objectIntersectionObserver = new ObjectIntersectionObserver();
		objectIntersectionObserver->setGeometryManager(g_geometryManager);

		TouchMenuObserver *secTouchMenuObserver = new TouchMenuObserver(g_secondaryLastMenu, g_menuManager, g_miscDataStorage);

		g_interactionManager->registerObserver(CONTROLLER_TYPE::PRIMARY, Observer::PRIMARY_JOYSTICK_MENU, primTouchMenuObserver);
		g_interactionManager->registerObserver(CONTROLLER_TYPE::PRIMARY, Observer::CONSTANTLY_UPDATE, objectIntersectionObserver);
		g_interactionManager->registerObserver(CONTROLLER_TYPE::SECONDARY, Observer::SECONDARY_JOYSTICK_MENU, secTouchMenuObserver);
	}
	else if (g_primaryLastMenu == TOUCH_EDIMODE_SELECTION)
	{
		g_menuManager->hideAllMenus();
		g_menuManager->showMenuWithUniqueName(g_primaryLastMenu);
		g_menuManager->showMenuWithUniqueName(g_secondaryLastMenu);

		SelectionSphereTouchMenuObserver *primTouchMenuObserver = new SelectionSphereTouchMenuObserver(g_primaryLastMenu);
		primTouchMenuObserver->setMenuManager(g_menuManager);
		primTouchMenuObserver->setMiscDataStorage(g_miscDataStorage);

		registerSelectConfirmVerticesObserver();

		TouchMenuObserver *secTouchMenuObserver = new TouchMenuObserver(g_secondaryLastMenu, g_menuManager, g_miscDataStorage);

		g_interactionManager->registerObserver(CONTROLLER_TYPE::PRIMARY, Observer::PRIMARY_JOYSTICK_MENU, primTouchMenuObserver);
		g_interactionManager->registerObserver(CONTROLLER_TYPE::SECONDARY, Observer::SECONDARY_JOYSTICK_MENU, secTouchMenuObserver);
	}
	else if (g_primaryLastMenu == TOUCH_OBJMODE_TRANS_ROT_SCALE)
	{
		g_menuManager->hideAllMenus();
		g_menuManager->showMenuWithUniqueName(g_primaryLastMenu);
		g_menuManager->showMenuWithUniqueName(g_secondaryLastMenu);

		TouchMenuObserver *primTouchMenuObserver = new TouchMenuObserver(g_primaryLastMenu, g_menuManager, g_miscDataStorage);

		TouchMenuObserver *secTouchMenuObserver = new TouchMenuObserver(g_secondaryLastMenu, g_menuManager, g_miscDataStorage);

		g_interactionManager->registerObserver(CONTROLLER_TYPE::PRIMARY, Observer::PRIMARY_JOYSTICK_MENU, primTouchMenuObserver);
		g_interactionManager->registerObserver(CONTROLLER_TYPE::SECONDARY, Observer::SECONDARY_JOYSTICK_MENU, secTouchMenuObserver);
	}
	else if (g_primaryLastMenu == TOUCH_EDIMODE_TRANS_ROT_SCALE)
	{
		g_menuManager->hideAllMenus();
		g_menuManager->showMenuWithUniqueName(g_primaryLastMenu);
		g_menuManager->showMenuWithUniqueName(g_secondaryLastMenu);

		TouchMenuObserver *primTouchMenuObserver = new TouchMenuObserver(g_primaryLastMenu, g_menuManager, g_miscDataStorage);

		TouchMenuObserver *secTouchMenuObserver = new TouchMenuObserver(g_secondaryLastMenu, g_menuManager, g_miscDataStorage);

		g_interactionManager->registerObserver(CONTROLLER_TYPE::PRIMARY, Observer::PRIMARY_JOYSTICK_MENU, primTouchMenuObserver);
		g_interactionManager->registerObserver(CONTROLLER_TYPE::SECONDARY, Observer::SECONDARY_JOYSTICK_MENU, secTouchMenuObserver);


	}
	else if (g_primaryLastMenu == TOUCH_EDIMODE_PROBEDIT)
	{
		g_menuManager->hideAllMenus();
		g_menuManager->showMenuWithUniqueName(g_primaryLastMenu);
		g_menuManager->showMenuWithUniqueName(g_secondaryLastMenu);

		//TODO: I rape here the name of EDIModeSelectionTouchMenuObserver for PropEdit stuff, mind name conventions and find a PropEditObserver name!
		SelectionSphereTouchMenuObserver *primTouchMenuObserver = new SelectionSphereTouchMenuObserver(g_primaryLastMenu);
		primTouchMenuObserver->setMenuManager(g_menuManager);
		primTouchMenuObserver->setMiscDataStorage(g_miscDataStorage);

		registerSelectConfirmVerticesObserver();

		TouchMenuObserver *secTouchMenuObserver = new TouchMenuObserver(g_secondaryLastMenu, g_menuManager, g_miscDataStorage);

		g_interactionManager->registerObserver(CONTROLLER_TYPE::PRIMARY, Observer::PRIMARY_JOYSTICK_MENU, primTouchMenuObserver);
		g_interactionManager->registerObserver(CONTROLLER_TYPE::SECONDARY, Observer::SECONDARY_JOYSTICK_MENU, secTouchMenuObserver);
	}
	else if (g_primaryLastMenu == TOUCH_EDIMODE_ADD_MESH_ELEMENTS)
	{
		g_menuManager->hideAllMenus();
		g_menuManager->showMenuWithUniqueName(g_primaryLastMenu);
		g_menuManager->showMenuWithUniqueName(g_secondaryLastMenu);

		SelectionSphereTouchMenuObserver *primTouchMenuObserver = new SelectionSphereTouchMenuObserver(g_primaryLastMenu);
		primTouchMenuObserver->setMenuManager(g_menuManager);
		primTouchMenuObserver->setMiscDataStorage(g_miscDataStorage);

		TouchMenuObserver *secTouchMenuObserver = new TouchMenuObserver(g_secondaryLastMenu, g_menuManager, g_miscDataStorage);

		g_interactionManager->registerObserver(CONTROLLER_TYPE::PRIMARY, Observer::PRIMARY_JOYSTICK_MENU, primTouchMenuObserver);
		g_interactionManager->registerObserver(CONTROLLER_TYPE::SECONDARY, Observer::SECONDARY_JOYSTICK_MENU, secTouchMenuObserver);
	}
	//TODO: TOUCH_SET_SPECTATOR_CAM must be impkemented
	else if (g_primaryLastMenu == TOUCH_SET_SPECTATOR_CAM)
	{
		g_menuManager->hideAllMenus();
		//g_menuManager->showMenuWithUniqueName(g_primaryLastMenu);
		g_menuManager->showMenuWithUniqueName(g_secondaryLastMenu);

		//TouchMenuObserver *primTouchMenuObserver = new TouchMenuObserver(g_primaryLastMenu);
		//primTouchMenuObserver->setMenuManager(g_menuManager);

		TouchMenuObserver *secTouchMenuObserver = new TouchMenuObserver(g_secondaryLastMenu, g_menuManager, g_miscDataStorage);

		//g_interactionManager->registerObserver(Controller::PRIMARY, Observer::PRIMARY_JOYSTICK_MENU, primTouchMenuObserver);
		g_interactionManager->registerObserver(CONTROLLER_TYPE::SECONDARY, Observer::SECONDARY_JOYSTICK_MENU, secTouchMenuObserver);
	}
	else if (g_primaryLastMenu == TOUCH_SCAN_POINT_CLOUD || g_primaryLastMenu == TOUCH_SCAN_OCTREE || g_primaryLastMenu == TOUCH_SCAN_POLYGONIZE) {
		g_menuManager->hideAllMenus();
		g_menuManager->showMenuWithUniqueName(g_primaryLastMenu);
		g_menuManager->showMenuWithUniqueName(g_secondaryLastMenu);
		
		TouchMenuObserver* secTouchMenuObserver = new TouchMenuObserver(g_secondaryLastMenu, g_menuManager, g_miscDataStorage);
		g_interactionManager->registerObserver(CONTROLLER_TYPE::SECONDARY, Observer::SECONDARY_JOYSTICK_MENU, secTouchMenuObserver);

		switch (g_miscDataStorage->getScanContext()) {
		case ScanContext::POINT_CLOUD_MODE:
			registerScanPointCloudObserver();
			break;
		case ScanContext::OCTREE_MODE:
			registerDDATraverseObserver();
			break;
		case ScanContext::MARCHING_CUBES_MODE:
			break;
		}
	}
}

void registerTranslateVerticesObserver()
{
	//TODO: Make a function for compute bounding box in geometryManager and think about what is best place for updating the bb again and again?
	//Updating Bounding Box
	//g_geometryManager->getActiveGeometryData()->getOpenMeshGeo()->computeBound();
	//g_geometryManager->getActiveGeometryData()->getOpenMeshGeo()->dirtyBound();

	GrabVerticesObserver* grabVerticesObserver = new GrabVerticesObserver();
	grabVerticesObserver->setGeometryManager(g_geometryManager);
	grabVerticesObserver->setMenuManager(g_menuManager);
	grabVerticesObserver->setInteractionManager(g_interactionManager);
	grabVerticesObserver->setMiscDataStorage(g_miscDataStorage);
	grabVerticesObserver->setHistoryManager(g_historyManager);
	grabVerticesObserver->waitAndDoNothingUntilTriggerPressedAgain(true);

	unregisterObservers();
	registerTranslateSelectedObjectObserver();
	g_interactionManager->registerObserver(CONTROLLER_TYPE::PRIMARY, Observer::TRIGGER, grabVerticesObserver);
	activateObserversForMenus();

	g_geometryManager->resetConstraint();

	g_miscDataStorage->activateEDITransTool();
	//std::cout << "In registerTranslateVerticesObserver " << std::endl;
}

void registerRotateVerticesObserver()
{
	RotateVerticesObserver* rotateVerticesObserver = new RotateVerticesObserver();
	rotateVerticesObserver->setGeometryManager(g_geometryManager);
	rotateVerticesObserver->setMenuManager(g_menuManager);
	rotateVerticesObserver->setInteractionManager(g_interactionManager);
	rotateVerticesObserver->setMiscDataStorage(g_miscDataStorage);
	rotateVerticesObserver->setHistoryManager(g_historyManager);
	rotateVerticesObserver->waitAndDoNothingUntilTriggerPressedAgain(true);

	unregisterObservers();
	g_interactionManager->registerObserver(CONTROLLER_TYPE::PRIMARY, Observer::TRIGGER, rotateVerticesObserver);

	activateObserversForMenus();
	g_geometryManager->resetConstraint();
	g_miscDataStorage->activateEDIRotateTool();
}

void registerScaleVerticesObserver()
{
	ScaleVerticesObserver* scaleVerticesObserver = new ScaleVerticesObserver();
	scaleVerticesObserver->setGeometryManager(g_geometryManager);
	scaleVerticesObserver->setMenuManager(g_menuManager);
	scaleVerticesObserver->setInteractionManager(g_interactionManager);
	scaleVerticesObserver->setMiscDataStorage(g_miscDataStorage);
	scaleVerticesObserver->setHistoryManager(g_historyManager);
	scaleVerticesObserver->waitAndDoNothingUntilTriggerPressedAgain(true);

	unregisterObservers();
	g_interactionManager->registerObserver(CONTROLLER_TYPE::PRIMARY, Observer::TRIGGER, scaleVerticesObserver);
	
	activateObserversForMenus();
	g_geometryManager->resetConstraint();
	g_miscDataStorage->activateEDIScaleTool();
}

void registerPropEditObserver()
{
	//TODO: Finish the work for attaching the spherecontroller tool and entire probEdit interaction
	SphereVertexProspectObserver* sphereVertexProspectObserver = new SphereVertexProspectObserver();
	sphereVertexProspectObserver->setGeometryManager(g_geometryManager);

	GrabPropEditObserver* grabPropEditObserver = new GrabPropEditObserver();
	grabPropEditObserver->setGeometryManager(g_geometryManager);

	unregisterObservers();
	g_interactionManager->registerObserver(CONTROLLER_TYPE::PRIMARY, Observer::TRIGGER, grabPropEditObserver);
	g_interactionManager->registerObserver(CONTROLLER_TYPE::PRIMARY, Observer::CONSTANTLY_UPDATE, sphereVertexProspectObserver);

	g_menuManager->hideAllMenus();
	g_geometryManager->updateWireframeStatesForAllGeometries();
}

void registerTranslateAndRotateSelectedObjectObserver()
{
	//TranslateAndRotateSelectedObserver* translateAndRotateSelectedObserver = new TranslateAndRotateSelectedObserver();
	//translateAndRotateSelectedObserver->setGeometryManager(g_geometryManager);

	//g_interactionManager->registerObserver(Controller::PRIMARY, Observer::TRIGGER, translateAndRotateSelectedObserver);
}

void registerScaleSelectedObjectObserver()
{
	ScaleSelectedObjectObserver* scaleSelectedObserver = new ScaleSelectedObjectObserver();
	scaleSelectedObserver->setGeometryManager(g_geometryManager);

	g_interactionManager->registerObserver(CONTROLLER_TYPE::PRIMARY, Observer::TRIGGER, scaleSelectedObserver);
}

void registerScaleSelectedObjectObserverWithTwoSqueeze()
{
	ScaleSelectedObjectSqueezeObserver* squeezeScaleObserverPrim = new ScaleSelectedObjectSqueezeObserver(true);
	squeezeScaleObserverPrim->setGeometryManager(g_geometryManager);
	squeezeScaleObserverPrim->setHistoryManager(g_historyManager);

	//ScaleSelectedObjectSqueezeObserver* squeezeScaleObserverPrimSec = new ScaleSelectedObjectSqueezeObserver(false);
	//squeezeScaleObserverPrimSec->setGeometryManager(g_geometryManager);
	//squeezeScaleObserverPrimSec->setMiscDataStorage(g_miscDataStorage);
	//squeezeScaleObserverPrimSec->setHistoryManager(g_historyManager);

	//g_interactionManager->registerObserver(Controller::SECONDARY, Observer::SQUEEZE, squeezeScaleObserverPrimSec);
	g_interactionManager->registerObserver(CONTROLLER_TYPE::PRIMARY, Observer::SQUEEZE, squeezeScaleObserverPrim);
}

// Sculpting
void registerSculptAddObserver()
{
	SphereVertexProspectObserver* sphereVertexProspectObserver = new SphereVertexProspectObserver();
	sphereVertexProspectObserver->setGeometryManager(g_geometryManager);

	SculptBrushAddObserver* sculptBrushAddObserver = new SculptBrushAddObserver();
	sculptBrushAddObserver->setGeometryManager(g_geometryManager);

	//Register Secondary Trans/Rot Observer again
	//TranslateAndRotateSelectedObserver* translateAndRotateSelectedObserver = new TranslateAndRotateSelectedObserver();
	//translateAndRotateSelectedObserver->setGeometryManager(g_geometryManager);

	//Menu Observer
	////////SecPieEditModeMenuObserver* mainMenuInteractionObserver = new SecPieEditModeMenuObserver();
	////////mainMenuInteractionObserver->setMenuManager(g_menuManager);
	////////PieMenuObserver* secPieEditModeMenuObserver = new PieMenuObserver(PIE_EDIT);
	////////secPieEditModeMenuObserver->setMenuManager(g_menuManager);

	unregisterObservers();
	g_interactionManager->registerObserver(CONTROLLER_TYPE::PRIMARY, Observer::TRIGGER, sculptBrushAddObserver);
	g_interactionManager->registerObserver(CONTROLLER_TYPE::PRIMARY, Observer::CONSTANTLY_UPDATE, sphereVertexProspectObserver);
	//g_interactionManager->registerObserver(Controller::SECONDARY, Observer::TRIGGER, translateAndRotateSelectedObserver);
	//////g_interactionManager->registerObserver(Controller::SECONDARY, Observer::SECONDARY_JOYSTICK_MENU, secPieEditModeMenuObserver);

	//g_menuManager->hideMenuWithUniqueName(PIE_EDIT);
}

void registerSculptSubtractObserver()
{
	SphereVertexProspectObserver* sphereVertexProspectObserver = new SphereVertexProspectObserver();
	sphereVertexProspectObserver->setGeometryManager(g_geometryManager);

	SculptBrushSubtractObserver* sculptBrushSubtractObserver = new SculptBrushSubtractObserver();
	sculptBrushSubtractObserver->setGeometryManager(g_geometryManager);

	//Register Secondary Trans/Rot Observer again
	//TranslateAndRotateSelectedObserver* translateAndRotateSelectedObserver = new TranslateAndRotateSelectedObserver();
	//translateAndRotateSelectedObserver->setGeometryManager(g_geometryManager);

	//Menu Observer
	//SecPieEditModeMenuObserver* mainMenuInteractionObserver = new SecPieEditModeMenuObserver();
	//mainMenuInteractionObserver->setMenuManager(g_menuManager);
	//////PieMenuObserver* secPieEditModeMenuObserver = new PieMenuObserver(PIE_EDIT);
	//////secPieEditModeMenuObserver->setMenuManager(g_menuManager);

	unregisterObservers();
	g_interactionManager->registerObserver(CONTROLLER_TYPE::PRIMARY, Observer::TRIGGER, sculptBrushSubtractObserver);
	g_interactionManager->registerObserver(CONTROLLER_TYPE::PRIMARY, Observer::CONSTANTLY_UPDATE, sphereVertexProspectObserver);
	//g_interactionManager->registerObserver(Controller::SECONDARY, Observer::TRIGGER, translateAndRotateSelectedObserver);
	//g_interactionManager->registerObserver(Controller::SECONDARY, Observer::SECONDARY_JOYSTICK_MENU, secPieEditModeMenuObserver);

	//////g_menuManager->hideMenuWithUniqueName(PIE_EDIT);
}

//Opens a menu and ask weather vertices or faces will be deleted
void deleteSelectedMeshElement()
{
	FenceMenuObserver* primFenceOBJModeMenuObserver = new FenceMenuObserver(FENCE_DELETE_MESH_ELEMENT_QUESTION_COMMAND);
	primFenceOBJModeMenuObserver->setMenuManager(g_menuManager);
	primFenceOBJModeMenuObserver->setMainViewer(g_mainViewer);
	primFenceOBJModeMenuObserver->triggerOpeningProcedureOnceFromExtern(g_interactionManager->getPrimaryController(), g_interactionManager->getSecondaryController());

	g_interactionManager->registerObserver(CONTROLLER_TYPE::PRIMARY, Observer::PRIMARY_JOYSTICK_MENU, primFenceOBJModeMenuObserver);
	g_menuManager->showMenuWithUniqueName(FENCE_DELETE_MESH_ELEMENT_QUESTION_COMMAND);
}

void deleteSelectedVertices()
{
	if (g_geometryManager->getActiveGeometryData() != NULL)
		g_geometryManager->deleteSelectedMeshElements(true, false, false);

	unregisterObservers();
	g_primaryMenuBeforLastMenu = g_primaryLastMenu;
	g_primaryLastMenu = TOUCH_EDIMODE_SELECTION;
	registerSelectConfirmVerticesObserver();
	activateObserversForMenus();

	g_historyManager->addAction(HistoryAction(ActionType::EDI_DELETE_VERTEX));

	g_geometryManager->updateWireframeStatesForAllGeometries();
}

void deleteSelectedFaces()
{
	if (g_geometryManager->getActiveGeometryData() != NULL)
		g_geometryManager->deleteSelectedMeshElements(false, false, true);

	unregisterObservers();
	g_primaryMenuBeforLastMenu = g_primaryLastMenu;
	g_primaryLastMenu = TOUCH_EDIMODE_SELECTION;
	registerSelectConfirmVerticesObserver();
	activateObserversForMenus();

	g_historyManager->addAction(HistoryAction(ActionType::EDI_DELETE_FACE));

	g_geometryManager->updateWireframeStatesForAllGeometries();
}

void saveCurrentGeometryAsOBJ()
{
	if (g_geometryManager->getActiveGeometryData() != NULL)
	{
		time_t t = time(0);   // get time now
		struct tm * now = localtime(&t);
		std::stringstream finalFileSavePath;
		finalFileSavePath <<
			"ClayMore-SaveFile-"
			<< now->tm_year + 1900 << "-"
			<< now->tm_mon + 1 << "-"
			<< now->tm_mday << "---"
			<< now->tm_hour << "-"
			<< now->tm_min << "-"
			<< now->tm_sec << ".obj";
		g_geometryManager->writeFileToDisk(finalFileSavePath.str());
	}
	g_geometryManager->updateWireframeStatesForAllGeometries();
}

void saveCurrentGeometryAsSTL()
{
	//ToDo4: Only .stl is different to the function above -> reduce code
	if (g_geometryManager->getActiveGeometryData() != NULL)
	{
		time_t t = time(0);   // get time now
		struct tm * now = localtime(&t);
		std::stringstream finalFileSavePath;
		finalFileSavePath <<
			"ClayMore-SaveFile-"
			<< now->tm_year + 1900 << "-"
			<< now->tm_mon + 1 << "-"
			<< now->tm_mday << "---"
			<< now->tm_hour << "-"
			<< now->tm_min << "-"
			<< now->tm_sec << ".stl";
		g_geometryManager->writeFileToDisk(finalFileSavePath.str());
	}
	g_geometryManager->updateWireframeStatesForAllGeometries();
}

void registerExtrudeVertices()
{
	if (g_geometryManager->getActiveGeometryData() != NULL)
		g_geometryManager->extrude();

	g_primaryMenuBeforLastMenu = g_primaryLastMenu;
	g_primaryLastMenu = TOUCH_EDIMODE_TRANS_ROT_SCALE;
	registerTranslateVerticesObserver();

	g_historyManager->addAction(HistoryAction(ActionType::EDI_EXTRUDE));

	g_geometryManager->updateWireframeStatesForAllGeometries();
}

void subdivide()
{
	if (g_geometryManager->getActiveGeometryData() != NULL)
		g_geometryManager->subdivide();

	g_primaryMenuBeforLastMenu = g_primaryLastMenu;
	g_primaryLastMenu = TOUCH_OBJMODE_TRANS_ROT_SCALE;
	registerTranslateVerticesObserver();

	g_geometryManager->updateWireframeStatesForAllGeometries();
}

//Eigentlich versteh ich garnicht wieso das FenceMenu zu geht, wenn der nur in diese d�nne Funktion geht.....

void toggleWireframe()
{
	if (g_geometryManager->getActiveGeometryData() != NULL)
	{
		g_geometryManager->toggleWireframeOfActiveGeometry();
	}
	g_geometryManager->updateWireframeStatesForAllGeometries();

	unregisterObservers();
	activateObserversForMenus();
	registerTranslateSelectedObjectObserver();
}

void loadLowResMonkeyModel()
{
	g_dataManager->loadData("data/monkey.obj");
	g_geometryManager->deselectAllObjects();
	g_geometryManager->updateShaderAssignments();
	registerTranslateSelectedObjectObserver();
	activateObserversForMenus();
	g_geometryManager->updateWireframeStatesForAllGeometries();
}

void loadHighResMonkeyModel()
{
	g_dataManager->loadData("data/monkey-31000verts.obj");
	g_geometryManager->deselectAllObjects();
	g_geometryManager->updateShaderAssignments();
	registerTranslateSelectedObjectObserver();
	activateObserversForMenus();
	g_geometryManager->updateWireframeStatesForAllGeometries();
}

void loadVeryHighResMonkeyModel()
{
	g_dataManager->loadData("data/monkey-126000verts.obj");
	g_geometryManager->deselectAllObjects();
	g_geometryManager->updateShaderAssignments();
	registerTranslateSelectedObjectObserver();
	activateObserversForMenus();
	g_geometryManager->updateWireframeStatesForAllGeometries();
}

void loadF1Front()
{
	g_dataManager->loadData("data/f1_front.obj");
	g_geometryManager->deselectAllObjects();
	g_geometryManager->updateShaderAssignments();
	registerTranslateSelectedObjectObserver();
	activateObserversForMenus();
	g_geometryManager->updateWireframeStatesForAllGeometries();
}

void loadR8LowPoly()
{
	g_dataManager->loadData("data/r8_LowPoly.obj");
	g_geometryManager->deselectAllObjects();
	g_geometryManager->updateShaderAssignments();
	registerTranslateSelectedObjectObserver();
	activateObserversForMenus();
	g_geometryManager->updateWireframeStatesForAllGeometries();
}

void loadR8HighPoly()
{
	g_dataManager->loadData("data/r8_HighPoly.obj");
	g_geometryManager->deselectAllObjects();
	g_geometryManager->updateShaderAssignments();
	registerTranslateSelectedObjectObserver();
	activateObserversForMenus();
	g_geometryManager->updateWireframeStatesForAllGeometries();
}

//void loadCube()
//{
//	g_dataManager->loadData("data/default_models/cube.obj");
//	g_geometryManager->deselectAllObjects();
//	g_geometryManager->updateShaderAssignments();
//	registerTranslateSelectedObjectObserver();
//	activateObserversForMenus();
//	g_geometryManager->updateWireframeStatesForAllGeometries();
//}

void loadCube()
{
	if (g_miscDataStorage->getGlobalApplicationMode() == ApplicationMode::OBJECT_MODE)
	{
		g_dataManager->loadData("data/default_models/cube.obj");
		g_geometryManager->deselectAllObjects();
		//g_geometryManager->updateShaderAssignments();
		registerTranslateSelectedObjectObserver();
	}
	else if (g_miscDataStorage->getGlobalApplicationMode() == ApplicationMode::EDIT_MODE)
	{
		//Save current geometry name
		std::string activeGeometry = g_geometryManager->getActiveGeometryData()->getName();
		//Deselect all verticies, because the new added geometry will be selected
		g_geometryManager->deselectAllVerts();
		g_geometryManager->getActiveGeometryData()->getOpenMeshGeo()->translateSelColorToOMStatusBit();
		g_dataManager->loadData("data/default_models/cube.obj");
		//Save primitive geometry name which will be joined now
		std::string primitiveToJoin = g_geometryManager->getActiveGeometryData()->getName();
		//Set the actual active geometry back (because loadData() will set the primitive as active)
		g_geometryManager->setActiveGeometry(activeGeometry);
		//Join them
		g_geometryManager->joinGeometries(g_geometryManager->getGeometryDataByName(primitiveToJoin)->getOpenMeshGeo());
		//Delete the primitive again
		g_geometryManager->deleteGeometry(primitiveToJoin);
		g_geometryManager->garbageCollection();
		//And do some other stuff
		g_geometryManager->getActiveGeometryData()->updateBoundingBox();
		g_geometryManager->getActiveGeometryData()->getOpenMeshGeo()->translateSelOMStatusBitToColor();
		g_geometryManager->getActiveGeometryData()->getOpenMeshGeo()->getColorArray()->dirty();

		g_primaryMenuBeforLastMenu = g_primaryLastMenu;
		g_primaryLastMenu = TOUCH_EDIMODE_TRANS_ROT_SCALE;
		registerTranslateVerticesObserver();
		g_historyManager->addAction(HistoryAction(ActionType::EDI_ADD_PRIMITIVE));
	}
	activateObserversForMenus();
	g_geometryManager->updateWireframeStatesForAllGeometries();
}

void loadSphere()
{
	if (g_miscDataStorage->getGlobalApplicationMode() == ApplicationMode::OBJECT_MODE)
	{
		g_dataManager->loadData("data/default_models/sphere.obj");
		g_geometryManager->deselectAllObjects();
		//g_geometryManager->updateShaderAssignments();
		registerTranslateSelectedObjectObserver();
	}
	else if (g_miscDataStorage->getGlobalApplicationMode() == ApplicationMode::EDIT_MODE)
	{
		//Save current geometry name
		std::string activeGeometry = g_geometryManager->getActiveGeometryData()->getName();
		//Deselect all verticies, because the new added geometry will be selected
		g_geometryManager->deselectAllVerts();
		g_geometryManager->getActiveGeometryData()->getOpenMeshGeo()->translateSelColorToOMStatusBit();
		g_dataManager->loadData("data/default_models/sphere.obj");
		//Save primitive geometry name which will be joined now
		std::string primitiveToJoin = g_geometryManager->getActiveGeometryData()->getName();
		//Set the actual active geometry back (because loadData() will set the primitive as active)
		g_geometryManager->setActiveGeometry(activeGeometry);
		//Join them
		g_geometryManager->joinGeometries(g_geometryManager->getGeometryDataByName(primitiveToJoin)->getOpenMeshGeo());
		//Delete the primitive again
		g_geometryManager->deleteGeometry(primitiveToJoin);
		g_geometryManager->garbageCollection();
		//And do some other stuff
		g_geometryManager->getActiveGeometryData()->updateBoundingBox();
		g_geometryManager->getActiveGeometryData()->getOpenMeshGeo()->translateSelOMStatusBitToColor();
		g_geometryManager->getActiveGeometryData()->getOpenMeshGeo()->getColorArray()->dirty();

		g_primaryMenuBeforLastMenu = g_primaryLastMenu;
		g_primaryLastMenu = TOUCH_EDIMODE_TRANS_ROT_SCALE;
		registerTranslateVerticesObserver();
		g_historyManager->addAction(HistoryAction(ActionType::EDI_ADD_PRIMITIVE));
	}
	activateObserversForMenus();
	g_geometryManager->updateWireframeStatesForAllGeometries();
}

void loadCylinder()
{
	if (g_miscDataStorage->getGlobalApplicationMode() == ApplicationMode::OBJECT_MODE)
	{
		g_dataManager->loadData("data/default_models/cylinder.obj");
		g_geometryManager->deselectAllObjects();
		//g_geometryManager->updateShaderAssignments();
		registerTranslateSelectedObjectObserver();
	}
	else if (g_miscDataStorage->getGlobalApplicationMode() == ApplicationMode::EDIT_MODE)
	{
		//Save current geometry name
		std::string activeGeometry = g_geometryManager->getActiveGeometryData()->getName();
		//Deselect all verticies, because the new added geometry will be selected
		g_geometryManager->deselectAllVerts();
		g_geometryManager->getActiveGeometryData()->getOpenMeshGeo()->translateSelColorToOMStatusBit();
		g_dataManager->loadData("data/default_models/cylinder.obj");
		//Save primitive geometry name which will be joined now
		std::string primitiveToJoin = g_geometryManager->getActiveGeometryData()->getName();
		//Set the actual active geometry back (because loadData() will set the primitive as active)
		g_geometryManager->setActiveGeometry(activeGeometry);
		//Join them
		g_geometryManager->joinGeometries(g_geometryManager->getGeometryDataByName(primitiveToJoin)->getOpenMeshGeo());
		//Delete the primitive again
		g_geometryManager->deleteGeometry(primitiveToJoin);
		g_geometryManager->garbageCollection();
		//And do some other stuff
		g_geometryManager->getActiveGeometryData()->updateBoundingBox();
		g_geometryManager->getActiveGeometryData()->getOpenMeshGeo()->translateSelOMStatusBitToColor();
		g_geometryManager->getActiveGeometryData()->getOpenMeshGeo()->getColorArray()->dirty();

		g_primaryMenuBeforLastMenu = g_primaryLastMenu;
		g_primaryLastMenu = TOUCH_EDIMODE_TRANS_ROT_SCALE;
		registerTranslateVerticesObserver();
		g_historyManager->addAction(HistoryAction(ActionType::EDI_ADD_PRIMITIVE));
	}
	activateObserversForMenus();
	g_geometryManager->updateWireframeStatesForAllGeometries();

}

void loadCone()
{

	if (g_miscDataStorage->getGlobalApplicationMode() == ApplicationMode::OBJECT_MODE)
	{
		g_dataManager->loadData("data/default_models/cone.obj");
		g_geometryManager->deselectAllObjects();
		//g_geometryManager->updateShaderAssignments();
		registerTranslateSelectedObjectObserver();
	}
	else if (g_miscDataStorage->getGlobalApplicationMode() == ApplicationMode::EDIT_MODE)
	{
		//Save current geometry name
		std::string activeGeometry = g_geometryManager->getActiveGeometryData()->getName();
		//Deselect all verticies, because the new added geometry will be selected
		g_geometryManager->deselectAllVerts();
		g_geometryManager->getActiveGeometryData()->getOpenMeshGeo()->translateSelColorToOMStatusBit();
		g_dataManager->loadData("data/default_models/cone.obj");
		//Save primitive geometry name which will be joined now
		std::string primitiveToJoin = g_geometryManager->getActiveGeometryData()->getName();
		//Set the actual active geometry back (because loadData() will set the primitive as active)
		g_geometryManager->setActiveGeometry(activeGeometry);
		//Join them
		g_geometryManager->joinGeometries(g_geometryManager->getGeometryDataByName(primitiveToJoin)->getOpenMeshGeo());
		//Delete the primitive again
		g_geometryManager->deleteGeometry(primitiveToJoin);
		g_geometryManager->garbageCollection();
		//And do some other stuff
		g_geometryManager->getActiveGeometryData()->updateBoundingBox();
		g_geometryManager->getActiveGeometryData()->getOpenMeshGeo()->translateSelOMStatusBitToColor();
		g_geometryManager->getActiveGeometryData()->getOpenMeshGeo()->getColorArray()->dirty();

		g_primaryMenuBeforLastMenu = g_primaryLastMenu;
		g_primaryLastMenu = TOUCH_EDIMODE_TRANS_ROT_SCALE;
		registerTranslateVerticesObserver();

		g_historyManager->addAction(HistoryAction(ActionType::EDI_ADD_PRIMITIVE));
	}
	activateObserversForMenus();
	g_geometryManager->updateWireframeStatesForAllGeometries();

	////Save current geometry name
	//std::string activeGeometry = g_geometryManager->getActiveGeometryData()->getName();
	//g_dataManager->loadData("data/default_models/cone.obj");
	////Save primitive geometry name which will be joined now
	//std::string primitiveToJoin = g_geometryManager->getActiveGeometryData()->getName();
	////Set the actual active geometry back (because loadData() will set the primitive as active)
	//g_geometryManager->setActiveGeometry(activeGeometry);
	////Join them
	//g_geometryManager->joinGeometries(g_geometryManager->getGeometryDataByName(primitiveToJoin)->getOpenMeshGeo());
	////Delete the primitive again
	//g_geometryManager->deleteGeometry(primitiveToJoin);
	//g_geometryManager->garbageCollection();
	////And do some other stuff
	//g_geometryManager->getActiveGeometryData()->updateBoundingBox();
	//activateObserversForMenus();
	//g_geometryManager->updateWireframeStatesForAllGeometries();
}



//void loadSphere()
//{
//	g_dataManager->loadData("data/default_models/sphere.obj");
//	g_geometryManager->deselectAllObjects();
//	g_geometryManager->updateShaderAssignments();
//	registerTranslateSelectedObjectObserver();
//	activateObserversForMenus();
//	g_geometryManager->updateWireframeStatesForAllGeometries();
//}
//
//void loadCylinder()
//{
//	g_dataManager->loadData("data/default_models/cylinder.obj");
//	g_geometryManager->deselectAllObjects();
//	g_geometryManager->updateShaderAssignments();
//	registerTranslateSelectedObjectObserver();
//	activateObserversForMenus();
//	g_geometryManager->updateWireframeStatesForAllGeometries();
//}
//
//void loadCone()
//{
//	g_dataManager->loadData("data/default_models/cone.obj");
//	g_geometryManager->deselectAllObjects();
//	g_geometryManager->updateShaderAssignments();
//	registerTranslateSelectedObjectObserver();
//	activateObserversForMenus();
//	g_geometryManager->updateWireframeStatesForAllGeometries();
//}




void duplicateSelectedMeshArea()
{
	g_geometryManager->duplicateSelectedMeshArea();
	g_primaryMenuBeforLastMenu = g_primaryLastMenu;
	g_primaryLastMenu = TOUCH_EDIMODE_TRANS_ROT_SCALE;
	registerTranslateVerticesObserver();
	//activateObserversForMenus();

	g_historyManager->addAction(HistoryAction(ActionType::EDI_DUPLICATE));

	g_geometryManager->updateWireframeStatesForAllGeometries();
}


void toggleHelp()
{
	if (g_miscDataStorage->getIsHelpTextActivated())
	{
		g_miscDataStorage->setIsHelpTextActivated(false);
	}
	else
	{
		g_miscDataStorage->setIsHelpTextActivated(true);
	}
	
	registerTranslateVerticesObserver();
	activateObserversForMenus();
	g_geometryManager->updateWireframeStatesForAllGeometries();
}









//###################################### NEW MENU STRUCTURE
void registerRulerObserver()
{
	RulerObserver* rulerObserver = new RulerObserver();
	g_menuManager->hideAllMenus();
	TouchMenuObserver *touchMenuObserver;
	if (g_miscDataStorage->getGlobalApplicationMode() == OBJECT_MODE)
	{
		touchMenuObserver = new TouchMenuObserver(TOUCH_OBJMODE_MAIN, g_menuManager, g_miscDataStorage);
		g_menuManager->showMenuWithUniqueName(TOUCH_OBJMODE_MAIN);
	}
	if (g_miscDataStorage->getGlobalApplicationMode() == EDIT_MODE)
	{
		touchMenuObserver = new TouchMenuObserver(TOUCH_EDIMODE_MAIN, g_menuManager, g_miscDataStorage);
		g_menuManager->showMenuWithUniqueName(TOUCH_EDIMODE_MAIN);
	}

	unregisterObservers();
	registerTranslateSelectedObjectObserver();
	//rulerObserver must be called constantly, cause update(bool) must be called constantly, otherwise the line is attached to the controller movements
	g_interactionManager->registerObserver(CONTROLLER_TYPE::PRIMARY, Observer::CONSTANTLY_UPDATE, rulerObserver);
	g_interactionManager->registerObserver(CONTROLLER_TYPE::SECONDARY, Observer::SECONDARY_JOYSTICK_MENU, touchMenuObserver);

	g_geometryManager->updateWireframeStatesForAllGeometries();
}

//######################################################################
//######################################################################
//######################################################################
//######################################################################
//################Umtersuchen, kann man das nicht mit getMenuObserver dings machen?`#####################################
//######################################################################
//######################################################################
//######################################################################
//######################################################################
void showOBJModeToolsMenu()
{
	unregisterObservers();
	g_menuManager->hideAllMenus();
	DartMenuObserver* secDartObjectModeMenuObserver = new DartMenuObserver(DART_OBJMODE_TOOLS);
	secDartObjectModeMenuObserver->setMenuManager(g_menuManager);
	secDartObjectModeMenuObserver->setGeometryManager(g_geometryManager);
	secDartObjectModeMenuObserver->triggerOpeningProcedureOnceFromExtern(g_interactionManager->getSecondaryController(), g_interactionManager->getPrimaryController());
	g_interactionManager->registerObserver(CONTROLLER_TYPE::SECONDARY, Observer::SECONDARY_JOYSTICK_MENU, secDartObjectModeMenuObserver);
	g_menuManager->showMenuWithUniqueName(DART_OBJMODE_TOOLS);

	g_geometryManager->activateWireframeForAllGeometries();
}

void showAddMeshOBJModeMenu()
{
	unregisterObservers();
	g_menuManager->hideAllMenus();

	FenceMenuObserver* secFenceOBJModeMenuObserver = new FenceMenuObserver(FENCE_OBJMODE_ADD_MESH);
	secFenceOBJModeMenuObserver->setMenuManager(g_menuManager);
	secFenceOBJModeMenuObserver->setMainViewer(g_mainViewer);
	secFenceOBJModeMenuObserver->triggerOpeningProcedureOnceFromExtern(g_interactionManager->getSecondaryController(), g_interactionManager->getPrimaryController());

	g_interactionManager->registerObserver(CONTROLLER_TYPE::SECONDARY, Observer::SECONDARY_JOYSTICK_MENU, secFenceOBJModeMenuObserver);
	g_menuManager->showMenuWithUniqueName(FENCE_OBJMODE_ADD_MESH);

	g_geometryManager->activateWireframeForAllGeometries();
	//std::cout << "heul doch2" << std::endl;
}

void showAddPrimitiveEDIModeMenu()
{
	unregisterObservers();
	g_menuManager->hideAllMenus();

	FenceMenuObserver* secFenceOBJModeMenuObserver = new FenceMenuObserver(FENCE_EDIMODE_ADD_PRIMITIVE);
	secFenceOBJModeMenuObserver->setMenuManager(g_menuManager);
	secFenceOBJModeMenuObserver->setMainViewer(g_mainViewer);
	secFenceOBJModeMenuObserver->triggerOpeningProcedureOnceFromExtern(g_interactionManager->getSecondaryController(), g_interactionManager->getPrimaryController());

	g_interactionManager->registerObserver(CONTROLLER_TYPE::SECONDARY, Observer::SECONDARY_JOYSTICK_MENU, secFenceOBJModeMenuObserver);
	g_menuManager->showMenuWithUniqueName(FENCE_EDIMODE_ADD_PRIMITIVE);

	g_geometryManager->activateWireframeForAllGeometries();
}

void showEDIModeToolsMenu()
{
	unregisterObservers();
	g_menuManager->hideAllMenus();
	DartMenuObserver* secDartObjectModeMenuObserver = new DartMenuObserver(DART_EDIMODE_TOOLS);
	secDartObjectModeMenuObserver->setMenuManager(g_menuManager);
	secDartObjectModeMenuObserver->setGeometryManager(g_geometryManager);
	secDartObjectModeMenuObserver->triggerOpeningProcedureOnceFromExtern(g_interactionManager->getSecondaryController(), g_interactionManager->getPrimaryController());
	g_interactionManager->registerObserver(CONTROLLER_TYPE::SECONDARY, Observer::SECONDARY_JOYSTICK_MENU, secDartObjectModeMenuObserver);
	g_menuManager->showMenuWithUniqueName(DART_EDIMODE_TOOLS);

	g_geometryManager->activateWireframeForAllGeometries();
}

void toggleConstraintX()
{
	//std::cout << "constrain X Axis" << std::endl;
	//g_geometryManager->setConstraint(Constraints::X_AXIS);
	g_geometryManager->toggleConstraint(Constraints::X_AXIS);
}

void toggleConstraintY()
{
	//std::cout << "constrain Y Axis" << std::endl;
	//g_geometryManager->setConstraint(Constraints::Y_AXIS);
	g_geometryManager->toggleConstraint(Constraints::Y_AXIS);
}

void toggleConstraintZ()
{
	//std::cout << "constrain Z Axis" << std::endl;
	//g_geometryManager->setConstraint(Constraints::Z_AXIS);
	g_geometryManager->toggleConstraint(Constraints::Z_AXIS);
}

void toggleConstraintSnapping()
{
	//std::cout << "constrain triggerConstraintGrid" << std::endl;
	//g_geometryManager->setConstraint(Constraints::GRID);
	//g_geometryManager->toggleConstraint(Constraints::GRID);
	g_geometryManager->toggleSnapping();
}












void showGeneralCommandFenceMenu()
{
	unregisterObservers();

	activateObserversForMenus();

	g_menuManager->hideAllMenus();
	FenceMenuObserver* fenceMenuObserver = new FenceMenuObserver(FENCE_GENERAL_COMMAND);
	fenceMenuObserver->setMenuManager(g_menuManager);
	fenceMenuObserver->setMainViewer(g_mainViewer);
	g_interactionManager->registerObserver(CONTROLLER_TYPE::SECONDARY, Observer::SECONDARY_JOYSTICK_MENU, fenceMenuObserver);

	fenceMenuObserver->triggerOpeningProcedureOnceFromExtern(g_interactionManager->getSecondaryController(), g_interactionManager->getPrimaryController());

	g_geometryManager->activateWireframeForAllGeometries();
}

void showObjectModeDetailCommandFenceMenu()
{
	unregisterObservers();

	g_menuManager->hideAllMenus();

	g_menuManager->showMenuWithUniqueName(g_secondaryLastMenu);
	g_menuManager->showMenuWithUniqueName(TOUCH_OBJMODE_MAIN);

	FenceMenuObserver* fenceMenuObserver = new FenceMenuObserver(FENCE_OBJMODE_DETAIL_COMMAND);
	fenceMenuObserver->setMenuManager(g_menuManager);
	fenceMenuObserver->setMainViewer(g_mainViewer);

	TouchMenuObserver *secTouchMenuObserver = new TouchMenuObserver(TOUCH_OBJMODE_MAIN, g_menuManager, g_miscDataStorage);

	g_interactionManager->registerObserver(CONTROLLER_TYPE::PRIMARY, Observer::PRIMARY_JOYSTICK_MENU, fenceMenuObserver);
	g_interactionManager->registerObserver(CONTROLLER_TYPE::SECONDARY, Observer::SECONDARY_JOYSTICK_MENU, secTouchMenuObserver);

	//Triggering the menu for oponing at the controller position
	fenceMenuObserver->triggerOpeningProcedureOnceFromExtern(g_interactionManager->getPrimaryController(), g_interactionManager->getSecondaryController());

	g_geometryManager->activateWireframeForAllGeometries();
}

void showEDIModeDetailCommandFenceMenu()
{
	unregisterObservers();

	g_menuManager->hideAllMenus();

	g_menuManager->showMenuWithUniqueName(g_secondaryLastMenu);
	g_menuManager->showMenuWithUniqueName(TOUCH_EDIMODE_MAIN);

	FenceMenuObserver* fenceMenuObserver = new FenceMenuObserver(FENCE_EDIMODE_DETAIL_COMMAND);
	fenceMenuObserver->setMenuManager(g_menuManager);
	fenceMenuObserver->setMainViewer(g_mainViewer);

	TouchMenuObserver *secTouchMenuObserver = new TouchMenuObserver(TOUCH_EDIMODE_MAIN, g_menuManager, g_miscDataStorage);

	g_interactionManager->registerObserver(CONTROLLER_TYPE::PRIMARY, Observer::PRIMARY_JOYSTICK_MENU, fenceMenuObserver);
	g_interactionManager->registerObserver(CONTROLLER_TYPE::SECONDARY, Observer::SECONDARY_JOYSTICK_MENU, secTouchMenuObserver);

	//Triggering the menu for oponing at the controller position
	fenceMenuObserver->triggerOpeningProcedureOnceFromExtern(g_interactionManager->getPrimaryController(), g_interactionManager->getSecondaryController());

	g_geometryManager->activateWireframeForAllGeometries();
}




void activateTransRotScaleToolOBJMode()
{
	unregisterObservers();
	g_primaryMenuBeforLastMenu = g_primaryLastMenu;
	g_primaryLastMenu = TOUCH_OBJMODE_TRANS_ROT_SCALE;
	activateObserversForMenus();
	g_geometryManager->updateWireframeStatesForAllGeometries();
}

void activateTransRotScaleToolEDIMode()
{
	unregisterObservers();
	g_primaryMenuBeforLastMenu = g_primaryLastMenu;
	g_primaryLastMenu = TOUCH_EDIMODE_TRANS_ROT_SCALE;
	registerTranslateVerticesObserver();
	activateObserversForMenus();
	g_geometryManager->updateWireframeStatesForAllGeometries();
}










void activateOBJModeSelectionTool()
{
	unregisterObservers();
	g_primaryMenuBeforLastMenu = g_primaryLastMenu;
	g_primaryLastMenu = TOUCH_OBJMODE_SELECTION;
	activateObserversForMenus();
	g_geometryManager->updateWireframeStatesForAllGeometries();
}

void activateEDIModeSelectionTool()
{
	unregisterObservers();
	g_primaryMenuBeforLastMenu = g_primaryLastMenu;
	g_primaryLastMenu = TOUCH_EDIMODE_SELECTION;
	activateObserversForMenus();
	g_geometryManager->updateWireframeStatesForAllGeometries();
}





void activateScaleSelectedObjectObserver()
{

}

//void activateRotateSelectedObjectObserver()
//{
//	unregisterObservers();
//	activateObserversForMenus();
//}






void OBJModeIntersectionSingleSelection()
{
	bool isIntersectionFound;
	osg::Vec3f dir = osg::Vec3f(0.0, 0.0, 50.0) - g_interactionManager->getPrimaryController()->getMatrixTransform()->getMatrix().getTrans();
	dir = g_interactionManager->getPrimaryController()->getMatrixTransform()->getMatrix().getRotate() * dir;
	osg::Vec3f lineEndPosition = dir + g_interactionManager->getPrimaryController()->getMatrixTransform()->getMatrix().getTrans();

	std::string nameOfIntersectedObject = g_geometryManager->checkIntersectionWithGeometry(
		g_interactionManager->getPrimaryController()->getMatrixTransform()->getMatrix().getTrans(),
		g_interactionManager->getPrimaryController()->getMatrixTransform()->getMatrix().getTrans() + g_interactionManager->getPrimaryController()->getMatrixTransform()->getMatrix().getRotate() * osg::Vec3f(0.0, 0.0, -50.0),
		&isIntersectionFound);

	if (isIntersectionFound)
	{
		g_geometryManager->setActiveGeometry(nameOfIntersectedObject);
		g_geometryManager->getGeometryDataByName(nameOfIntersectedObject)->setIsSelected(true);

		//A list of all geometries will be iterated through
		for (unsigned int i = 0; i < g_geometryManager->getNumChildren(); i++)
		{
			dynamic_cast<GeometryData*>(g_geometryManager->getChild(i))->setIsSelected(false);
			dynamic_cast<GeometryData*>(g_geometryManager->getChild(i))->getGeometryVisualization()->selectTechnique(FLAT_SHADING);
		}
		g_geometryManager->getGeometryDataByName(nameOfIntersectedObject)->setIsSelected(true);
		g_geometryManager->getGeometryDataByName(nameOfIntersectedObject)->getGeometryVisualization()->selectTechnique(INDICATED_AS_SELECTED_SHADING);
		//}
		g_geometryManager->getGeometryDataByName(nameOfIntersectedObject)->getGeometryVisualization()->selectTechnique(INDICATED_AS_SELECTED_AND_ACTIVE_SHADING);
	}
	registerTranslateSelectedObjectObserver();
}

void OBJModeIntersectionMultipleSelection()
{
	bool isIntersectionFound;
	osg::Vec3f dir = osg::Vec3f(0.0, 0.0, 50.0) - g_interactionManager->getPrimaryController()->getMatrixTransform()->getMatrix().getTrans();
	dir = g_interactionManager->getPrimaryController()->getMatrixTransform()->getMatrix().getRotate() * dir;
	osg::Vec3f lineEndPosition = dir + g_interactionManager->getPrimaryController()->getMatrixTransform()->getMatrix().getTrans();

	std::string nameOfIntersectedObject = g_geometryManager->checkIntersectionWithGeometry(
		g_interactionManager->getPrimaryController()->getMatrixTransform()->getMatrix().getTrans(),
		g_interactionManager->getPrimaryController()->getMatrixTransform()->getMatrix().getTrans() + g_interactionManager->getPrimaryController()->getMatrixTransform()->getMatrix().getRotate() * osg::Vec3f(0.0, 0.0, -50.0),
		&isIntersectionFound);

	if (isIntersectionFound)
	{
		g_geometryManager->setActiveGeometry(nameOfIntersectedObject);
		g_geometryManager->getGeometryDataByName(nameOfIntersectedObject)->setIsSelected(true);

		//A list of all geometries will be iterated through
		for (unsigned int i = 0; i < g_geometryManager->getNumChildren(); i++)
		{
			if (dynamic_cast<GeometryData*>(g_geometryManager->getChild(i))->getIsSelected())
			{
				dynamic_cast<GeometryData*>(g_geometryManager->getChild(i))->getGeometryVisualization()->selectTechnique(INDICATED_AS_SELECTED_SHADING);
			}
			else
			{
				dynamic_cast<GeometryData*>(g_geometryManager->getChild(i))->getGeometryVisualization()->selectTechnique(FLAT_SHADING);
			}
		}
		g_geometryManager->getGeometryDataByName(nameOfIntersectedObject)->getGeometryVisualization()->selectTechnique(INDICATED_AS_SELECTED_AND_ACTIVE_SHADING);
	}
	registerTranslateSelectedObjectObserver();
}


//Not clear, if there is any use for it anymore?
//void registerAddMeshElementObserver()
//{
//	AddMeshElementObserver* addMeshElementObserver = new AddMeshElementObserver();
//	addMeshElementObserver->setGeometryManager(g_geometryManager);
//
//	//TouchMenuObserver *touchMenuObserver = new TouchMenuObserver(TOUCH_OBJMODE_MAIN);
//	//touchMenuObserver->setMenuManager(g_menuManager);
//
//	g_menuManager->hideAllMenus();
//	//g_menuManager->showMenuWithUniqueName(TOUCH_OBJMODE_MAIN);
//
//	unregisterObservers();
//	//registerTranslateSelectedObjectObserver();
//	g_interactionManager->registerObserver(Controller::PRIMARY, Observer::TRIGGER, addMeshElementObserver);
//	//g_interactionManager->registerObserver(Controller::SECONDARY, Observer::SECONDARY_JOYSTICK_MENU, touchMenuObserver);
//
//}

void registerAddVertsObserver()
{
	g_primaryMenuBeforLastMenu = g_primaryLastMenu;
	g_primaryLastMenu = TOUCH_EDIMODE_ADD_MESH_ELEMENTS;

	AddVertsObserver* addVertsObserver = new AddVertsObserver();
	addVertsObserver->setGeometryManager(g_geometryManager);
	addVertsObserver->setHistoryManager(g_historyManager);

	//TouchMenuObserver *touchMenuObserver = new TouchMenuObserver(TOUCH_OBJMODE_MAIN);
	//touchMenuObserver->setMenuManager(g_menuManager);

	g_menuManager->hideAllMenus();
	//g_menuManager->showMenuWithUniqueName(TOUCH_OBJMODE_MAIN);

	unregisterObservers();
	g_interactionManager->registerObserver(CONTROLLER_TYPE::PRIMARY, Observer::TRIGGER, addVertsObserver);
	//g_interactionManager->registerObserver(Controller::SECONDARY, Observer::SECONDARY_JOYSTICK_MENU, touchMenuObserver);

	g_miscDataStorage->activateAddVertsTool();
	g_menuManager->getMenuWithUniqueName(TOUCH_EDIMODE_ADD_MESH_ELEMENTS)->getMenuButton(0)->detachFromSceneGraph();

	registerTranslateSelectedObjectObserver();
	activateObserversForMenus();
}

void activateAddMeshElementTool()
{
	unregisterObservers();
	g_primaryMenuBeforLastMenu = g_primaryLastMenu;
	g_primaryLastMenu = TOUCH_EDIMODE_ADD_MESH_ELEMENTS;
	//registerAddMeshElementObserver();
	registerAddVertsObserver();

	registerTranslateSelectedObjectObserver();
	activateObserversForMenus();
	g_geometryManager->updateWireframeStatesForAllGeometries();
}

void registerConnectMeshElementObserver()
{
	g_primaryMenuBeforLastMenu = g_primaryLastMenu;
	g_primaryLastMenu = TOUCH_EDIMODE_ADD_MESH_ELEMENTS;

	ConnectMeshElementsObserver* connectMeshElementsObserver = new ConnectMeshElementsObserver();
	connectMeshElementsObserver->setGeometryManager(g_geometryManager);
	connectMeshElementsObserver->setHistoryManager(g_historyManager);

	SphereControllerTool* sphereControllerTool = static_cast<SphereControllerTool*>(g_interactionManager->getPrimaryController()->getControllerTool(ControllerTool::SPHERE));
	sphereControllerTool->setSphereColor(osg::Vec4(0.9f, 0.9f, 0.9f, 0.15f));

	g_menuManager->hideAllMenus();

	unregisterObservers();
	g_interactionManager->registerObserver(CONTROLLER_TYPE::PRIMARY, Observer::TRIGGER, connectMeshElementsObserver);

	g_miscDataStorage->activateConnectMeshElementTool();

	g_menuManager->getMenuWithUniqueName(TOUCH_EDIMODE_ADD_MESH_ELEMENTS)->getMenuButton(0)->attachToSceneGraph(g_menuManager->getMenuWithUniqueName(TOUCH_EDIMODE_ADD_MESH_ELEMENTS)->getMenuTransform());

	registerTranslateSelectedObjectObserver();
	activateObserversForMenus();
}


void closeFenceMenu()
{
	//Register last observers for menus
	if (g_primaryLastMenu == TOUCH_EDIMODE_TRANS_ROT_SCALE)
	{
		if (*g_miscDataStorage->getAddressOfIsEDITranslateToolActive())
		{
			registerTranslateVerticesObserver();
		}
		else if (*g_miscDataStorage->getAddressOfIsEDIScaleToolActive())
		{
			registerScaleVerticesObserver();
		}
		else if (*g_miscDataStorage->getAddressOfIsEDIRotateToolActive())
		{
			registerRotateVerticesObserver();
		}
	}

	else if (g_primaryLastMenu == TOUCH_EDIMODE_ADD_MESH_ELEMENTS)
	{

		if (*g_miscDataStorage->getAddressOfIsEDIAddVertsToolActive())
		{
			registerAddVertsObserver();
		}
		else if (*g_miscDataStorage->getAddressOfIsEDIConnectMeshElementToolActive())
		{
			registerConnectMeshElementObserver();
		}
	}

	else if (g_primaryLastMenu == TOUCH_EDIMODE_SELECTION)
	{
		if (*g_miscDataStorage->getAddressOfIsEDISelectionConfirmToolActive())
		{
			registerSelectConfirmVerticesObserver();
		}
		else if (*g_miscDataStorage->getAddressOfIsEDISelectionNegateToolActive())
		{
			registerSelectNegateVerticesObserver();
		}
	}

	registerTranslateSelectedObjectObserver();
	activateObserversForMenus();
	g_geometryManager->updateWireframeStatesForAllGeometries();
}


void toggleSelectOrDeselectAllVertices()
{
	if (g_geometryManager->getActiveGeometryData() != NULL)
	{
		g_geometryManager->toggleSelectOrDeselectAll();
		if (!g_geometryManager->isAnyVertexSelected())
		{
			if (!(g_primaryLastMenu == TOUCH_EDIMODE_ADD_MESH_ELEMENTS))
			{
				g_primaryMenuBeforLastMenu = g_primaryLastMenu;
				g_primaryLastMenu = TOUCH_EDIMODE_SELECTION;
			}
			registerSelectConfirmVerticesObserver();
		}
		else
		{
			g_primaryMenuBeforLastMenu = g_primaryLastMenu;
			g_primaryLastMenu = TOUCH_EDIMODE_TRANS_ROT_SCALE;
			if (*g_miscDataStorage->getAddressOfIsEDITranslateToolActive())
			{
				registerTranslateVerticesObserver();
			}
			if (*g_miscDataStorage->getAddressOfIsEDIScaleToolActive())
			{
				registerScaleVerticesObserver();
			}
			if (*g_miscDataStorage->getAddressOfIsEDIRotateToolActive())
			{
				registerRotateVerticesObserver();
			}
		}
	}

	if (g_primaryLastMenu == TOUCH_EDIMODE_ADD_MESH_ELEMENTS)
	{
		registerConnectMeshElementObserver();
	}

	activateObserversForMenus();
	registerTranslateSelectedObjectObserver();
	g_geometryManager->updateWireframeStatesForAllGeometries();
}


void undo()
{
	g_historyManager->undo();
	//g_geometryManager->cancelAnyTransformation();
}



void registerSpectatorCamObserver()
{
	SpectatorCamObserver* spectatorCamObserver = new SpectatorCamObserver();
	spectatorCamObserver->setSpectatorViewer(g_spectatorViewer);
	g_interactionManager->registerObserver(CONTROLLER_TYPE::PRIMARY, Observer::TRIGGER, spectatorCamObserver);

	g_primaryMenuBeforLastMenu = g_primaryLastMenu;
	g_primaryLastMenu = TOUCH_SET_SPECTATOR_CAM;
	activateObserversForMenus();

	g_geometryManager->updateWireframeStatesForAllGeometries();
}

void exitClayMore()
{
	g_mainViewer->setDone(true);

	g_primaryMenuBeforLastMenu = g_primaryLastMenu;
	activateObserversForMenus();

	g_geometryManager->updateWireframeStatesForAllGeometries();
}

// ### Scan Mode ###
void openFenceMenuForScanGeneralCommands()
{
	unregisterObservers();
	//g_interactionManager->unregisterObserver(CONTROLLER_TYPE::SECONDARY, Observer::TRIGGER);
	FenceMenuObserver* secFenceScanModeMenuObserver = new FenceMenuObserver(FENCE_SCAN_GENERAL_COMMAND);
	secFenceScanModeMenuObserver->setMenuManager(g_menuManager);
	secFenceScanModeMenuObserver->setMainViewer(g_mainViewer);
	//primFenceOBJModeMenuObserver->triggerOpeningProcedureOnceFromExtern(g_interactionManager->getPrimaryController(), g_interactionManager->getSecondaryController());

	g_interactionManager->registerObserver(CONTROLLER_TYPE::SECONDARY, Observer::SECONDARY_JOYSTICK_MENU, secFenceScanModeMenuObserver);
	g_menuManager->showMenuWithUniqueName(FENCE_SCAN_GENERAL_COMMAND);
	//Triggering the menu for oponing at the controller position
	secFenceScanModeMenuObserver->triggerOpeningProcedureOnceFromExtern(g_interactionManager->getSecondaryController(), g_interactionManager->getSecondaryController());
}

void openDartMenuForScanPointCloudContext()
{
	unregisterObservers();
	g_menuManager->hideAllMenus();
	DartMenuObserver* secDartScanModeMenuObserver = new DartMenuObserver(DART_SCAN_POINT_CLOUD_TOOLS);
	secDartScanModeMenuObserver->setMenuManager(g_menuManager);
	secDartScanModeMenuObserver->setGeometryManager(g_geometryManager);
	secDartScanModeMenuObserver->triggerOpeningProcedureOnceFromExtern(g_interactionManager->getSecondaryController(), g_interactionManager->getPrimaryController());
	g_interactionManager->registerObserver(CONTROLLER_TYPE::SECONDARY, Observer::SECONDARY_JOYSTICK_MENU, secDartScanModeMenuObserver);
	g_menuManager->showMenuWithUniqueName(DART_SCAN_POINT_CLOUD_TOOLS);

	g_geometryManager->activateWireframeForAllGeometries();

	//FenceMenuObserver* secDartScanModeMenuObserver = new FenceMenuObserver(DART_SCAN_POINT_CLOUD_TOOLS);
	//secDartScanModeMenuObserver->setMenuManager(g_menuManager);
	//secDartScanModeMenuObserver->setMainViewer(g_mainViewer);
	////primFenceOBJModeMenuObserver->triggerOpeningProcedureOnceFromExtern(g_interactionManager->getPrimaryController(), g_interactionManager->getSecondaryController());

	//g_interactionManager->registerObserver(CONTROLLER_TYPE::SECONDARY, Observer::SECONDARY_JOYSTICK_MENU, secDartScanModeMenuObserver);
	//g_menuManager->showMenuWithUniqueName(DART_SCAN_POINT_CLOUD_TOOLS);
	////Triggering the menu for oponing at the controller position
	//secDartScanModeMenuObserver->triggerOpeningProcedureOnceFromExtern(g_interactionManager->getSecondaryController(), g_interactionManager->getSecondaryController());
}

void openDartMenuForScanOctreeContext()
{
	unregisterObservers();
	g_menuManager->hideAllMenus();
	DartMenuObserver* secDartScanModeMenuObserver = new DartMenuObserver(DART_SCAN_OCTREE_TOOLS);
	secDartScanModeMenuObserver->setMenuManager(g_menuManager);
	secDartScanModeMenuObserver->setGeometryManager(g_geometryManager);
	secDartScanModeMenuObserver->triggerOpeningProcedureOnceFromExtern(g_interactionManager->getSecondaryController(), g_interactionManager->getPrimaryController());
	g_interactionManager->registerObserver(CONTROLLER_TYPE::SECONDARY, Observer::SECONDARY_JOYSTICK_MENU, secDartScanModeMenuObserver);
	g_menuManager->showMenuWithUniqueName(DART_SCAN_OCTREE_TOOLS);

	g_geometryManager->activateWireframeForAllGeometries();

	//FenceMenuObserver* secFenceScanModeMenuObserver = new FenceMenuObserver(DART_SCAN_OCTREE_TOOLS);
	//secFenceScanModeMenuObserver->setMenuManager(g_menuManager);
	//secFenceScanModeMenuObserver->setMainViewer(g_mainViewer);
	////primFenceOBJModeMenuObserver->triggerOpeningProcedureOnceFromExtern(g_interactionManager->getPrimaryController(), g_interactionManager->getSecondaryController());

	//g_interactionManager->registerObserver(CONTROLLER_TYPE::SECONDARY, Observer::SECONDARY_JOYSTICK_MENU, secFenceScanModeMenuObserver);
	//g_menuManager->showMenuWithUniqueName(DART_SCAN_OCTREE_TOOLS);
	////Triggering the menu for oponing at the controller position
	//secFenceScanModeMenuObserver->triggerOpeningProcedureOnceFromExtern(g_interactionManager->getSecondaryController(), g_interactionManager->getSecondaryController());
}

void openDartMenuForScanPolygonizeContext()
{
	unregisterObservers();
	g_menuManager->hideAllMenus();
	DartMenuObserver* secDartScanModeMenuObserver = new DartMenuObserver(DART_SCAN_POLYGONIZE_TOOLS);
	secDartScanModeMenuObserver->setMenuManager(g_menuManager);
	secDartScanModeMenuObserver->setGeometryManager(g_geometryManager);
	secDartScanModeMenuObserver->triggerOpeningProcedureOnceFromExtern(g_interactionManager->getSecondaryController(), g_interactionManager->getPrimaryController());
	g_interactionManager->registerObserver(CONTROLLER_TYPE::SECONDARY, Observer::SECONDARY_JOYSTICK_MENU, secDartScanModeMenuObserver);
	g_menuManager->showMenuWithUniqueName(DART_SCAN_POLYGONIZE_TOOLS);

	g_geometryManager->activateWireframeForAllGeometries();

	//FenceMenuObserver* secFenceScanModeMenuObserver = new FenceMenuObserver(DART_SCAN_MARCHING_CUBES_TOOLS);
	//secFenceScanModeMenuObserver->setMenuManager(g_menuManager);
	//secFenceScanModeMenuObserver->setMainViewer(g_mainViewer);
	////primFenceOBJModeMenuObserver->triggerOpeningProcedureOnceFromExtern(g_interactionManager->getPrimaryController(), g_interactionManager->getSecondaryController());

	//g_interactionManager->registerObserver(CONTROLLER_TYPE::SECONDARY, Observer::SECONDARY_JOYSTICK_MENU, secFenceScanModeMenuObserver);
	//g_menuManager->showMenuWithUniqueName(DART_SCAN_MARCHING_CUBES_TOOLS);
	////Triggering the menu for oponing at the controller position
	//secFenceScanModeMenuObserver->triggerOpeningProcedureOnceFromExtern(g_interactionManager->getSecondaryController(), g_interactionManager->getSecondaryController());
}


/*
	+ Draw Octree
	+ Polygonize
	+ Scan
	+ ClearOctree
	+ Undo PCL
	+ Clear PCL
	+ DDA

 	Draw TSDF (maybe combine with octree drawing)
	[Draw MC normals ?]

	+ Show/hide octree	
	+ Show/hide tsdf
	+ Show/hide dda
	+ Show/hide MC
	+ Show/hide help
*/

void pointCloudUndo() {
	g_pointCloudManager->revertSnapshot(); //TODO: Implement Octree support for removing the rspoints of the last snapshot
	activateObserversForMenus();
}

void calibrateCameras() {
	g_pointCloudManager->toggleCameraPoseCalibration();
	activateObserversForMenus();
}

void clearPointCloud() {
	g_pointCloudManager->clearPointCloud();
	activateObserversForMenus();
}

void drawOctreeVolume() {
	g_pointCloudManager->setOctreeDisplay(true);
	g_pointCloudManager->drawOctreeVolume();
	activateObserversForMenus();
}

void drawTSDF() {
	g_pointCloudManager->setTSDFDisplay(true);
	g_pointCloudManager->drawTSDF();
	activateObserversForMenus();
}

void clearOctreeVolumeDisplay() {
	g_pointCloudManager->clearOctreeVolumeDisplay();
	activateObserversForMenus();
}

void clearTSDFDisplay() {
	g_pointCloudManager->clearTSDFDisplay();
	activateObserversForMenus();
}

void clearDDADisplay() {
	g_pointCloudManager->clearDDADisplay();
	activateObserversForMenus();
}

void polygonizePointCloudMarchingCubes() {
	g_pointCloudManager->setPolygonizeDisplay(true);
	g_pointCloudManager->clearPolygonize();
	g_pointCloudManager->polygonizePointCloud();
	activateObserversForMenus();
}

void clearPolygonize() {
	g_pointCloudManager->clearPolygonize();
	activateObserversForMenus();
}

void generateTextureForPolygonize() {
	g_pointCloudManager->setPolygonizeDisplay(true);
	g_pointCloudManager->exportPointCloudTexture();
	activateObserversForMenus();
}

void toggleHelpForScanContext()
{
	g_miscDataStorage->setIsHelpTextActivated(!g_miscDataStorage->getIsHelpTextActivated());
	activateObserversForMenus();
}

void togglePointCloudDisplay() {
	g_pointCloudManager->togglePointCloudDisplay();
	activateObserversForMenus();
}

void toggleOctreeDisplay() {
	g_pointCloudManager->toggleOctreeDisplay();
	activateObserversForMenus();
}

void toggleTSDFDisplay() {
	g_pointCloudManager->toggleTSDFDisplay();
	activateObserversForMenus();
}

void toggleDDADisplay() {
	g_pointCloudManager->toggleDDADisplay();
	activateObserversForMenus();
}

void togglePolygonizeDisplay() {
	g_pointCloudManager->toggleMarchingCubesDisplay();
	activateObserversForMenus();
}

void damianDemo()
{
	std::cout << "Moin Moin!" << std::endl;
	activateObserversForMenus();
}


//void hideAllMenusCallback()
//{
//	std::cout << "hideAllMenusCallback" << std::endl;
//	g_menuManager->hideAllMenus();
//}

//void showCurrentMenu()
//{
//	std::cout << "showCurrentMenu" << std::endl;
//	MenuName test = static_cast<MenuObserver*>(g_interactionManager->getRegisteredObserverFromSecondaryController(Observer::SECONDARY_JOYSTICK_MENU))->getMenuName();
//	std::cout << "MenuName ist " << test << std::endl;
//	g_menuManager->showMenuWithUniqueName(static_cast<MenuObserver*>(g_interactionManager->getRegisteredObserverFromSecondaryController(Observer::SECONDARY_JOYSTICK_MENU))->getMenuName());
//}