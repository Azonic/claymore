// ClayMore - Immersive Mesh Modelling --- Copyright (c) 2014-2017 Philipp Ladwig, Jannik Fiedler, Jan Beutgen
#pragma once

#include "osg/ComputeBoundsVisitor"


void CTRMCF_translationAndRotation()
{
	std::cout << "CTRMCF_translationAndRotation" << std::endl;

}

void CTRMCF_onlyRotation()
{
	std::cout << "CTRMCF_onlyRotation" << std::endl;
}