// ClayMore - Immersive Mesh Modelling --- Copyright (c) 2014-2017 Philipp Ladwig, Jannik Fiedler, Jan Beutgen
#pragma once

#include <string>
#include <osg/Group>
#include <osg/MatrixTransform>

class Floor
{

public:
	Floor(std::string sgName);
	~Floor();
	
	void createFloor(float yPos);
	//inline void attachToSceneGraph(osg::Group* parentGroupIn) { parentGroupIn->addChild(m_matrixTransform); }
	inline osg::ref_ptr<osg::MatrixTransform> getMatrixTransform() { return m_matrixTransform; }

private:
	std::string m_sgName;
	float m_yPos;
	osg::ref_ptr<osg::MatrixTransform> m_matrixTransform;

};