// ClayMore - Immersive Mesh Modelling --- Copyright (c) 2014-2017 Philipp Ladwig, Jannik Fiedler, Jan Beutgen
#pragma once
#include <string>

// -- CONFIG
static const char * CONFIG_FILE_PATH = "./data/config.xml";
static const char * POSE_ESTIMATION_FILE_PATH = "./PoseEstimation/CameraParameters.xml";


const std::string g_stringNoSelectionAssertion = "123>-NO!SELECTION-<321";
const std::string g_stringNothingInSceneAssertion = "123>-NOTHING!IN!SCENE-<321";

//Network Management defines:
//The following defines are set in order to observe the maximal packet size of ethernet of usually (1500bytes - some heading) ~1488bytes... 
// and 300*sizeof(float) is 1200 bytes... soo, we are always far under the minimum
const unsigned int VERTEX_ARRAY_ELEMENTS_SEND_IN_ONE_PACKET = 300; //One element is either x, y or z; means an element is a single float and not a vertex
const unsigned int COLOR_ARRAY_ELEMENTS_SEND_IN_ONE_PACKET = 300; //One element is either x, y, z, w; means an element is a single float and not a vertex
const unsigned int INDEX_ARRAY_ELEMENTS_SEND_IN_ONE_PACKET = 300; //One element is either x, y or z; means an element is a single float and not a vertex
const unsigned int NORMAL_ARRAY_ELEMENTS_SEND_IN_ONE_PACKET = 300; //One element is either x, y or z; means an element is a single float and not a vertex
