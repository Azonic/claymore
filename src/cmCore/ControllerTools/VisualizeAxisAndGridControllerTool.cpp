// ClayMore - Immersive Mesh Modelling --- Copyright (c) 2014-2017 Philipp Ladwig, Jannik Fiedler, Jan Beutgen
#include "VisualizeAxisAndGridControllerTool.h"
#include "cmGeometryManagement/GeometryManager.h"
#include "../../cmUtil/MiscDataStorage.h"

#include <osg/Geode>
#include <osg/Geometry>
#include <osg/PolygonMode>
//#include <osg/io_utils> //for "c-outing" osg::matrices

VisualizeAxisAndGridControllerTool::VisualizeAxisAndGridControllerTool(
	ConfigReader* configReaderIn,
	osg::ref_ptr<GeometryManager> geometryManagerIn,
	MiscDataStorage* miscDataStorageIn)

	: m_configReader(configReaderIn),
	m_geometryManager(geometryManagerIn),
	m_miscDataStorage(miscDataStorageIn)
{
	m_lineLength = atof(m_configReader->getStringFromStartupConfig("line_length_for_snapping_axis").c_str());
	m_lineLengthCross = atof(m_configReader->getStringFromStartupConfig("line_length_for_snapping_cross").c_str());

	m_lineVertices = new osg::Vec3Array;
	m_gridVertices = new osg::Vec3Array;
	m_lineIndicies = new osg::DrawElementsUInt(osg::PrimitiveSet::LINES, 0);
	m_gridIndicies = new osg::DrawElementsUInt(osg::PrimitiveSet::LINES, 0);
	m_lineColorArray = new osg::Vec4Array;
	m_gridColorArray = new osg::Vec4Array;

	m_drawableForAxisVisualization = new osg::Geometry();
	m_drawableForGridVisualization = new osg::Geometry();
	m_visulizeAxisPosAtt = new osg::PositionAttitudeTransform();
	m_geodeForAxisVisualization = new osg::Geode();
	m_geodeForAxisVisualization->setCullingActive(false);
	m_geodeForGridVisualization = new osg::Geode();
	m_geodeForGridVisualization->setCullingActive(false);
	m_startPoint = new osg::Vec3f(0, 0, 0);
	m_matrixTransform->addChild(m_visulizeAxisPosAtt);
	m_visulizeAxisPosAtt->addChild(m_geodeForAxisVisualization);
	m_visulizeAxisPosAtt->addChild(m_geodeForGridVisualization);

	//EXP: Prepare Stuff for visualize AxisSnapping
	osg::ref_ptr<osg::PolygonMode> polymode = new osg::PolygonMode;
	polymode->setMode(osg::PolygonMode::FRONT_AND_BACK, osg::PolygonMode::LINE);
	m_geodeForAxisVisualization->getOrCreateStateSet()->setAttributeAndModes(polymode.get(), osg::StateAttribute::OVERRIDE | osg::StateAttribute::ON);
	m_geodeForGridVisualization->getOrCreateStateSet()->setAttributeAndModes(polymode.get(), osg::StateAttribute::OVERRIDE | osg::StateAttribute::ON);
	osg::LineWidth* lineWidth = new osg::LineWidth();
	lineWidth->setWidth(atof(m_configReader->getStringFromStartupConfig("line_width_for_snapping_axis").c_str()));
	m_geodeForAxisVisualization->getOrCreateStateSet()->setAttributeAndModes(lineWidth, osg::StateAttribute::OVERRIDE | osg::StateAttribute::ON);
	m_geodeForGridVisualization->getOrCreateStateSet()->setAttributeAndModes(lineWidth, osg::StateAttribute::OVERRIDE | osg::StateAttribute::ON);
	m_geodeForAxisVisualization->getOrCreateStateSet()->setMode(GL_LIGHTING, osg::StateAttribute::OFF);
	m_geodeForGridVisualization->getOrCreateStateSet()->setMode(GL_LIGHTING, osg::StateAttribute::OFF);
	m_geodeForAxisVisualization->getOrCreateStateSet()->setTextureMode(0, GL_TEXTURE_2D, osg::StateAttribute::OVERRIDE | osg::StateAttribute::OFF);
	m_geodeForGridVisualization->getOrCreateStateSet()->setTextureMode(0, GL_TEXTURE_2D, osg::StateAttribute::OVERRIDE | osg::StateAttribute::OFF);

	//1. EXTREME PERFORMANCE BOOST: Use VBOs!
	m_drawableForAxisVisualization->setUseDisplayList(false);
	m_drawableForGridVisualization->setUseDisplayList(false);
	m_drawableForAxisVisualization->setUseVertexBufferObjects(true);
	m_drawableForGridVisualization->setUseVertexBufferObjects(true);
	m_drawableForAxisVisualization->setDataVariance(osg::Object::DYNAMIC);
	m_drawableForGridVisualization->setDataVariance(osg::Object::DYNAMIC);

	//2. EXTREME PERFORMANCE BOOST: Don't do this every frame. This is neseccary every frame if using displayList, but not for using VBO
	m_drawableForAxisVisualization->setVertexArray(m_lineVertices);
	m_drawableForGridVisualization->setVertexArray(m_gridVertices);
	m_drawableForAxisVisualization->addPrimitiveSet(m_lineIndicies);
	m_drawableForGridVisualization->addPrimitiveSet(m_gridIndicies);
	m_drawableForAxisVisualization->setColorArray(m_lineColorArray);
	m_drawableForGridVisualization->setColorArray(m_gridColorArray);

	m_drawableForAxisVisualization->setColorBinding(osg::Geometry::BIND_PER_VERTEX);
	m_drawableForGridVisualization->setColorBinding(osg::Geometry::BIND_PER_VERTEX);
}

VisualizeAxisAndGridControllerTool::~VisualizeAxisAndGridControllerTool()
{
	delete m_startPoint;
}

void VisualizeAxisAndGridControllerTool::update()
{

}

//EXP: baryCentrumAtStartIn will only be used for constraint::GRID
void VisualizeAxisAndGridControllerTool::update(bool isSnappingActive, Constraints constraintIn, bool isLocalCoordSysSelectedIn, osg::Vec3f baryCentrumAtStartIn, osg::Vec3f deltaPositionIn)
{
	//EXP: If snapping is activated, draw a grid
	if (isSnappingActive)
	{
		if (*m_miscDataStorage->getAddressOfIsEDITranslateToolActive())
		{
			if (m_geodeForGridVisualization->getNumDrawables() == 0)
			{
				m_geodeForGridVisualization->addDrawable(m_drawableForGridVisualization);
				//EXP: PrimitiveSet has to be added every time again, if m_geodeForGridVisualization->removeDrawables() was called
				m_drawableForGridVisualization->addPrimitiveSet(m_gridIndicies);
			}
			else
			{
				m_geodeForGridVisualization->setDrawable(0, m_drawableForGridVisualization);
			}

			m_gridVertices->clear();
			m_gridIndicies->clear();
			m_gridColorArray->clear();

			osg::Vec4 color = osg::Vec4(0.7f, 0.7f, 0.7f, 0.5f);

			//Controller Tools are attached to the controller node, so have to computeLocalToWorld for "correcting" position of grid
			osg::Matrix local2WorldMatrixInverseController = osg::Matrix::inverse(osg::computeLocalToWorld(m_matrixTransform->getParentalNodePaths()[0]));
			osg::Matrix local2WorldMatrixGeom = osg::computeLocalToWorld(m_geometryManager->getActiveGeometryData()->getParentalNodePaths()[0]);

			//TODO: Put in geomeryManager or miscData
			//EXP: scales the distance between each cross -> 0.1 is distance of 10cm -> 0.01 is 1cm
			float scale = 0.1;

			//EXP: The number of crosses can be determined here, minimum is always 3 and the cube of crosses will be extended if the controller is moved farther awy from start point
			int numCrossesX = 3 + abs(floorf(deltaPositionIn.x()*10.0 + 0.5f)) * 2;
			int numCrossesY = 3 + abs(floorf(deltaPositionIn.y()*10.0 + 0.5f)) * 2;
			int numCrossesZ = 3 + abs(floorf(deltaPositionIn.z()*10.0 + 0.5f)) * 2;

			//std::cout << "Tutty " << abs(floorf(deltaPositionIn.x()*10.0 + 0.5f)) * 2 << std::endl;

			float xStart;
			float xEnd;
			float yStart;
			float yEnd;
			float zStart;
			float zEnd;

			if (constraintIn == Constraints::NO_CONSTRAIN)
			{
				xStart = -floorf((numCrossesX / 2));
				xEnd = floorf((numCrossesX / 2));

				yStart = -floorf((numCrossesY / 2));
				yEnd = floorf((numCrossesY / 2));

				zStart = -floorf((numCrossesZ / 2));
				zEnd = floorf((numCrossesZ / 2));
			}
			else if (constraintIn == Constraints::X_AXIS)
			{
				xStart = -floorf((numCrossesX / 2));
				xEnd = floorf((numCrossesX / 2));

				yStart = -1.0f;
				yEnd = 1.0f;

				zStart = -1.0f;
				zEnd = 1.0f;
			}
			else if (constraintIn == Constraints::Y_AXIS)
			{
				xStart = -1.0f;
				xEnd = 1.0f;

				yStart = -floorf((numCrossesY / 2));
				yEnd = floorf((numCrossesY / 2));

				zStart = -1.0f;
				zEnd = 1.0f;
			}
			else if (constraintIn == Constraints::Z_AXIS)
			{
				xStart = -1.0f;
				xEnd = 1.0f;

				yStart = -1.0f;
				yEnd = 1.0f;

				zStart = -floorf((numCrossesZ / 2));
				zEnd = floorf((numCrossesZ / 2));
			}


			for (float x = xStart; x <= xEnd; ++x)
			{
				//std::cout << "x: " << x << std::endl;
				for (float y = yStart; y <= yEnd; ++y)
				{
					//std::cout << "y: " << y << std::endl;
					for (float z = zStart; z <= zEnd; ++z)
					{
						//std::cout << "z: " << z << std::endl;
						//EXP: The matrix osg::Matrix::inverse(local2WorldMatrixController neutralises the controller transform, since all controller tools are attached to the controller
						m_gridVertices->push_back(((osg::Vec3f(x, y, z + m_lineLengthCross) * scale) + baryCentrumAtStartIn * local2WorldMatrixGeom) * local2WorldMatrixInverseController);
						m_gridVertices->push_back(((osg::Vec3f(x, y, z - m_lineLengthCross) * scale) + baryCentrumAtStartIn * local2WorldMatrixGeom) * local2WorldMatrixInverseController);
						//color = osg::Vec4(0.0f, 0.0f, 1.0f, 0.5f);
						m_gridColorArray->push_back(color);
						m_gridColorArray->push_back(color);

						m_gridVertices->push_back(((osg::Vec3f(x, y + m_lineLengthCross, z) * scale) + baryCentrumAtStartIn * local2WorldMatrixGeom) * local2WorldMatrixInverseController);
						m_gridVertices->push_back(((osg::Vec3f(x, y - m_lineLengthCross, z) * scale) + baryCentrumAtStartIn * local2WorldMatrixGeom) *local2WorldMatrixInverseController);
						//color = osg::Vec4(0.0f, 1.0f, 0.0f, 0.5f);
						m_gridColorArray->push_back(color);
						m_gridColorArray->push_back(color);

						m_gridVertices->push_back(((osg::Vec3f(x + m_lineLengthCross, y, z) * scale) + baryCentrumAtStartIn * local2WorldMatrixGeom) * local2WorldMatrixInverseController);
						m_gridVertices->push_back(((osg::Vec3f(x - m_lineLengthCross, y, z)  * scale) + baryCentrumAtStartIn * local2WorldMatrixGeom) * local2WorldMatrixInverseController);
						//color = osg::Vec4(1.0f, 0.0f, 0.0f, 0.5f);
						m_gridColorArray->push_back(color);
						m_gridColorArray->push_back(color);
					}
				}
			}
			m_gridVertices->dirty();
			m_gridColorArray->dirty();

			//Create indicies
			for (unsigned int i = 0; i < m_gridVertices->getNumElements(); ++i)
			{
				m_gridIndicies->push_back(i);
			}
			m_drawableForGridVisualization->getPrimitiveSet(0)->dirty();
		}

	}


	//EXP: If no axis selected (m_snapToAxis = 0), no axis must be visualized
	if (constraintIn == Constraints::NO_CONSTRAIN)
	{
		m_geodeForAxisVisualization->removeDrawables(0, m_geodeForAxisVisualization->getNumDrawables());
		if (!isSnappingActive)
		{
			m_geodeForGridVisualization->removeDrawables(0, m_geodeForGridVisualization->getNumDrawables());
		}
	}
	//EXP: If an axis is selected, it must be visualized, but first clean up and remove all drawables
	else
	{
		if (!isSnappingActive)
		{
			m_geodeForGridVisualization->removeDrawables(0, m_geodeForGridVisualization->getNumDrawables());
		}
		if (m_geodeForAxisVisualization->getNumDrawables() == 0)
		{
			m_geodeForAxisVisualization->addDrawable(m_drawableForAxisVisualization);
			m_drawableForAxisVisualization->addPrimitiveSet(m_lineIndicies);
		}
		else
		{
			m_geodeForAxisVisualization->setDrawable(0, m_drawableForAxisVisualization);

		}

		m_lineVertices->clear();
		m_lineIndicies->clear();
		m_lineColorArray->clear();

		osg::Vec3f firstVertex;
		osg::Vec3f secondVertex;
		osg::Vec4 color;
		if (constraintIn == Constraints::X_AXIS)
		{
			firstVertex.set(osg::Vec3f(-m_lineLength, 0, 0));
			secondVertex.set(osg::Vec3f(m_lineLength, 0, 0));
			color = osg::Vec4(1.0f, 0.0f, 0.0f, 1.0f);
		}
		if (constraintIn == Constraints::Y_AXIS)
		{
			firstVertex.set(osg::Vec3f(0, -m_lineLength, 0));
			secondVertex.set(osg::Vec3f(0, m_lineLength, 0));
			color = osg::Vec4(0.0f, 1.0f, 0.0f, 1.0f);
		}
		if (constraintIn == Constraints::Z_AXIS)
		{
			firstVertex.set(osg::Vec3f(0, 0, -m_lineLength));
			secondVertex.set(osg::Vec3f(0, 0, m_lineLength));
			color = osg::Vec4(0.0f, 0.0f, 1.0f, 1.0f);
		}

		//EXP: Though this controller tool is attached to the controllers node, we have to calculate the transformation back to global, 
		//	in respect to the transformation of the geometry node and the barycentrum of the selected verticies
		//TODO: Too heavy-> every time when visualizing an axis  the barycentrum will be calculated again and again-> Better is, when BC is calculated at the first time and stored. Management will be done by GeoManager
		osg::Vec3f barycentrum = m_geometryManager->getBarycentrumOfSELECTEDVertsForActiveGeomData();
		osg::Matrix local2WorldMatrixController = osg::computeLocalToWorld(m_matrixTransform->getParentalNodePaths()[0]);
		osg::Matrix local2WorldMatrixGeom = osg::computeLocalToWorld(m_geometryManager->getActiveGeometryData()->getParentalNodePaths()[0]);

		//m_lineVertices->clear();
		m_lineVertices->push_back(((firstVertex + (barycentrum *  local2WorldMatrixGeom)) * osg::Matrix::inverse(local2WorldMatrixController)));
		m_lineVertices->push_back(((secondVertex + (barycentrum *  local2WorldMatrixGeom))* osg::Matrix::inverse(local2WorldMatrixController)));

		m_lineIndicies->push_back(1);
		m_lineIndicies->push_back(0);

		m_lineColorArray->push_back(color);
		m_lineColorArray->push_back(color);

		m_lineVertices->dirty();
		m_lineColorArray->dirty();

		osg::Quat rotation = m_geometryManager->getActiveGeometryData()->getAttitude();
		if (isLocalCoordSysSelectedIn)
		{
			m_visulizeAxisPosAtt->setAttitude(rotation);
		}
		else
		{
			m_visulizeAxisPosAtt->setAttitude(osg::Quat(0.0f, 0.0f, 0.0f, 1.0f));
		}
	}

	//TODO: Seems to has no effect, sometimes the grid disappears...
	m_geodeForAxisVisualization->computeBound();
	m_geodeForAxisVisualization->dirtyBound();
	m_geodeForGridVisualization->computeBound();
	m_geodeForGridVisualization->dirtyBound();

}

void VisualizeAxisAndGridControllerTool::removeLines()
{
	//m_drawableForAxisVisualization->removePrimitiveSet(0, m_drawableForAxisVisualization->getNumPrimitiveSets());
	m_geodeForAxisVisualization->removeDrawables(0, m_geodeForAxisVisualization->getNumDrawables());
	m_geodeForGridVisualization->removeDrawables(0, m_geodeForGridVisualization->getNumDrawables());
}