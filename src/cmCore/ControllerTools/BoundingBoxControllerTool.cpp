// ClayMore - Immersive Mesh Modelling --- Copyright (c) 2014-2017 Philipp Ladwig, Jannik Fiedler, Jan Beutgen
#include "BoundingBoxControllerTool.h"
#include "cmGeometryManagement\GeometryManager.h"

#include <osg/PolygonMode>

//#include <osg/io_utils>

BoundingBoxControllerTool::BoundingBoxControllerTool()
{
	m_isShowActivated = false;

	m_geode = new osg::Geode();
	m_geode->getOrCreateStateSet();
	osg::ref_ptr<osg::StateSet> stateSet = new osg::StateSet();
	m_geode->setStateSet(stateSet);

	m_boxDrawable = new osg::ShapeDrawable(new osg::Box);

	m_BBmatrixTransform = new osg::MatrixTransform;

	m_geode->getOrCreateStateSet()->setAttributeAndModes(new osg::PolygonMode(osg::PolygonMode::FRONT_AND_BACK, osg::PolygonMode::LINE));
	m_geode->getOrCreateStateSet()->setMode(GL_LIGHTING, osg::StateAttribute::OFF);

	m_geode->addDrawable(m_boxDrawable);
	m_BBmatrixTransform->addChild(m_geode);
	m_matrixTransform->addChild(m_BBmatrixTransform);
}

BoundingBoxControllerTool::~BoundingBoxControllerTool()
{

}

void BoundingBoxControllerTool::update()
{
	if (m_isShowActivated)
	{
		osg::Matrix local2WorldMatrixInverseController = osg::Matrix::inverse(osg::computeLocalToWorld(m_matrixTransform->getParentalNodePaths()[0]));
		osg::Matrix local2WorldMatrixGeom = osg::computeLocalToWorld(m_geometryManager->getActiveGeometryData()->getParentalNodePaths()[0]);

		m_geometryManager->getActiveGeometryData()->updateBoundingBox();
		//std::cout << "m_geometryManager->getActiveGeometryData()->getBound().radius" << m_geometryManager->getActiveGeometryData()->getBound().radius() << std::endl;

		osg::Matrix scaleMatrix;
		osg::Matrix transMatrix;
		float bbRadius = m_geometryManager->getActiveGeometryData()->getBound().radius() * 1.35f;
		scaleMatrix.makeScale(osg::Vec3f(bbRadius, bbRadius, bbRadius));

		osg::Vec3f transVec = m_geometryManager->getActiveGeometryData()->getBound().center();
		transMatrix.makeTranslate(transVec);
		//std::cout << transMatrix << std::endl;

		//scaleMatrix = (local2WorldMatrixGeom * scaleMatrix * transMatrix) * local2WorldMatrixInverseController;

		scaleMatrix = (scaleMatrix * transMatrix) * local2WorldMatrixInverseController;
		m_BBmatrixTransform->setMatrix(scaleMatrix);

		if (m_geode->getNumDrawables() == 0)
		{
			m_geode->addDrawable(m_boxDrawable);
		}
	}
	else
	{
		if (m_geode->getNumDrawables() != 0)
		{
			m_geode->removeDrawables(0, m_geode->getNumDrawables());
		}
	}
}