// ClayMore - Immersive Mesh Modelling --- Copyright (c) 2014-2017 Philipp Ladwig, Jannik Fiedler, Jan Beutgen
#pragma once

#include "cmInteractionManagement\Observer.h"
#include <osg/ShapeDrawable>
#include <osg/Geode>
#include <osg/PositionAttitudeTransform>

class ThreeDIconControllerTool : public ControllerTool
{
public:
	ThreeDIconControllerTool();
	~ThreeDIconControllerTool();

	void update();

	void inline setIsShowMoveIconActivated(bool valueIn) { m_isShowMoveIconActivated = valueIn; }
	void inline setIsShowRotateIconActivated(bool valueIn) { m_isShowRotateIconActivated = valueIn; }
	void inline setIsShowScaleIconActivated(bool valueIn) { m_isShowScaleIconActivated = valueIn; }

private:

	bool m_isShowMoveIconActivated;
	bool m_isShowRotateIconActivated;
	bool m_isShowScaleIconActivated;

	osg::ref_ptr<osg::MatrixTransform> m_IconMatrixTransform;

	osg::ref_ptr<osg::Node> m_moveIcon;
	osg::ref_ptr<osg::Node> m_rotateIcon;
	osg::ref_ptr<osg::Node> m_scaleIcon;
};