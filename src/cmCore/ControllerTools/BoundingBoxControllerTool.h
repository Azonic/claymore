// ClayMore - Immersive Mesh Modelling --- Copyright (c) 2014-2017 Philipp Ladwig, Jannik Fiedler, Jan Beutgen
#pragma once

#include "cmInteractionManagement\Observer.h"
#include <osg/ShapeDrawable>
#include <osg/Geode>
#include <osg/PositionAttitudeTransform>

class GeometryManager;

class BoundingBoxControllerTool : public ControllerTool
{
public:
	BoundingBoxControllerTool();
	~BoundingBoxControllerTool();

	void update();

	inline void setGeometryManager(osg::ref_ptr<GeometryManager> geometryManager) { m_geometryManager = geometryManager; };

	void inline show() { m_isShowActivated = true; }
	void inline hide() { m_isShowActivated = false; }

private:
	osg::ref_ptr<GeometryManager> m_geometryManager;

	bool m_isShowActivated;
	osg::ref_ptr<osg::Geode> m_geode;
	osg::ref_ptr<osg::ShapeDrawable> m_boxDrawable;
	osg::ref_ptr<osg::MatrixTransform> m_BBmatrixTransform;
};