// ClayMore - Immersive Mesh Modelling --- Copyright (c) 2014-2017 Philipp Ladwig, Jannik Fiedler, Jan Beutgen
#pragma once

#include "cmInteractionManagement\Observer.h"
#include <osg/ShapeDrawable>
#include <osg/Geode>
#include <osg/PositionAttitudeTransform>

class DashedLineControllerTool : public ControllerTool
{
public:
	DashedLineControllerTool();
	~DashedLineControllerTool();

	void update();

	void inline setStartPoint(osg::Vec3f startIn) { m_startPoint = startIn; }
	void inline setEndPoint(osg::Vec3f endIn) { m_endPoint = endIn; }

	void inline showLine() { m_showLine = true; }
	void inline hideLine() { m_showLine = false; }

private:
	bool m_showLine;
	osg::Vec3f m_startPoint, m_endPoint;
	osg::ref_ptr<osg::Geode> m_geode;
	osg::ref_ptr<osg::Geometry> m_geometry;
	osg::ref_ptr<osg::TexGenNode> m_texgenNode;

	osg::ref_ptr<osg::Vec3Array> m_rulerLineVertices;
	osg::ref_ptr<osg::DrawElementsUInt> m_indicies;
	osg::ref_ptr<osg::Vec4Array> m_colorArrayRulerLine;
};