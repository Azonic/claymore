// ClayMore - Immersive Mesh Modelling --- Copyright (c) 2014-2017 Philipp Ladwig, Jannik Fiedler, Jan Beutgen
#pragma once

#include "cmInteractionManagement\Observer.h"

class Geode;
class Geometry;
class GeometryManager;
class MiscDataStorage;

class RulerControllerTool : public ControllerTool
{
public:
	RulerControllerTool(ConfigReader* configReaderIn, osg::ref_ptr<GeometryManager> geometryManagerIn, MiscDataStorage* miscDataStorageIn);
	~RulerControllerTool();

	void update();
	//Funny, just giving a parameter without name, just only the type is feasable for the compiler
	void update(bool);

	//Pass here the length of the scale vector (netween controller and barycentrum)
	inline void setStartScale(float startScaleIn) { m_startScale = startScaleIn; }
	inline void setCurrentScale(float currentScaleIn) { m_currentScale = currentScaleIn; }

	inline void setStartAndEndPoint(osg::Vec3f startPoint, osg::Vec3f endPoint) { m_startPoint = startPoint; m_endPoint = endPoint; }
	inline void setStartAndEndRotation(osg::Quat startRotationIn, osg::Quat endRotationIn) { m_startRotation = startRotationIn; m_endRotation = endRotationIn; }
	//inline void setStartAndEndScale( startScaleIn, endScaleIn) { m_startRotation = startScaleIn; m_endPointIn = endScaleIn; }

	inline void setPathMeasurement(bool setPathMeasurement) { m_isPathMeasurementEnabled = setPathMeasurement;  }

	inline void setShowRulerLine(bool showLineIn) { m_showRulerLine = showLineIn; }
	inline bool getShowRulerLine() { return m_showRulerLine; }

	void hide();

private:

	ConfigReader* m_configReader;
	osg::ref_ptr<GeometryManager> m_geometryManager;
	MiscDataStorage* m_miscDataStorage;

	osg::Vec3f m_startPoint;
	osg::Vec3f m_endPoint;

	float m_startScale;
	float m_currentScale;

	osg::Quat m_startRotation;
	osg::Quat m_endRotation;

	osg::ref_ptr<osg::Geode> m_geodeRulerLine;
	osg::ref_ptr<osg::Geode> m_geodeControllerPointer;
	osg::ref_ptr<osg::Geometry> m_drawableRulerLine;
	osg::ref_ptr<osg::Geometry> m_drawableControllerPointer;
	osg::ref_ptr<osg::Vec3Array> m_rulerLineVertices;
	osg::ref_ptr<osg::DrawElementsUInt> m_indicies;
	
	osg::Matrix m_local2WorldMatrix;

	osg::ref_ptr<osgText::Text> m_text;
	osg::ref_ptr<osg::MatrixTransform> m_textTransform;

	bool m_isPathMeasurementEnabled, m_showRulerLine;

	//WIP: Path Measurement: Uncooment follwoing line!!!!!!!!!!!!!!
	//osg::ref_ptr<osg::Vec4Array> m_colorArrayRulerLine;
	//osg::ref_ptr<osg::Vec3Array> m_rulerLineVertices;
	//osg::ref_ptr<osg::DrawElementsUInt> m_rulerLine;
};