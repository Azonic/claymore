// ClayMore - Immersive Mesh Modelling --- Copyright (c) 2014-2017 Philipp Ladwig, Jannik Fiedler, Jan Beutgen
#include "RulerControllerTool.h"
#include <osg/Geode>
#include <osg/Geometry>
#include <osg/PolygonMode>
#include <osg/LineWidth>

#include "cmGeometryManagement/GeometryManager.h"
#include "../../cmUtil/MiscDataStorage.h"
#include "../../cmUtil/Quat2EulerAndBackConverter.h"

#include <sstream>


//TODO: In main loop, the controller tools position is wrong, if I write this:
//g_mainViewer->frame();
//g_secondaryController->updateMatrixTransform();
//g_primaryController->updateMatrixTransform();

//If I change it, the Menus and other controller tools have this problem. Something with getParentalNodePath()?
//74,5

RulerControllerTool::RulerControllerTool(ConfigReader* configReaderIn, osg::ref_ptr<GeometryManager> geometryManagerIn, MiscDataStorage* miscDataStorageIn)
	: m_configReader(configReaderIn),
	m_geometryManager(geometryManagerIn),
	m_miscDataStorage(miscDataStorageIn)
{
	//TODO: Use VBOs and effiencent calls in update()
	m_isPathMeasurementEnabled = false;
	m_showRulerLine = true;

	m_drawableRulerLine = new osg::Geometry();
	m_geodeRulerLine = new osg::Geode();
	m_geodeRulerLine->addDrawable(m_drawableRulerLine);
	m_matrixTransform->addChild(m_geodeRulerLine);

	osg::ref_ptr<osg::PolygonMode> polymode = new osg::PolygonMode;
	polymode->setMode(osg::PolygonMode::FRONT_AND_BACK, osg::PolygonMode::LINE);
	m_geodeRulerLine->getOrCreateStateSet()->setAttributeAndModes(polymode.get(), osg::StateAttribute::OVERRIDE | osg::StateAttribute::ON);
	osg::LineWidth* lineWidth = new osg::LineWidth();
	lineWidth->setWidth(atof(m_configReader->getStringFromStartupConfig("line_width_for_ruler_tool").c_str()));
	m_geodeRulerLine->getOrCreateStateSet()->setAttributeAndModes(lineWidth, osg::StateAttribute::OVERRIDE | osg::StateAttribute::ON);
	m_geodeRulerLine->getOrCreateStateSet()->setMode(GL_LIGHTING, osg::StateAttribute::OFF);
	m_geodeRulerLine->getOrCreateStateSet()->setTextureMode(0, GL_TEXTURE_2D, osg::StateAttribute::OVERRIDE | osg::StateAttribute::OFF);
	m_drawableRulerLine->setUseDisplayList(false);
	m_drawableRulerLine->setUseVertexBufferObjects(true);
	m_drawableRulerLine->setDataVariance(osg::Object::DYNAMIC);
	m_geodeRulerLine->addDrawable(m_drawableRulerLine);

	//Text stuff
	osg::ref_ptr<osgText::Font> debugFont = osgText::readFontFile("fonts/arial.ttf");
	m_text = new osgText::Text;

	m_text->setFont(debugFont.get());
	m_text->setCharacterSize(0.025);
	m_text->setAxisAlignment(osgText::TextBase::YZ_PLANE);

	m_text->getOrCreateStateSet()->setMode(GL_CULL_FACE, osg::StateAttribute::ON);
	m_text->getOrCreateStateSet()->setMode(GL_LIGHTING, osg::StateAttribute::OVERRIDE | osg::StateAttribute::OFF);
	m_text->getOrCreateStateSet()->setRenderBinDetails(5, "DepthSortedBin");

	m_textTransform = new osg::MatrixTransform();

	osg::Matrix rotateDebugText1 = osg::Matrix::rotate(osg::DegreesToRadians(90.0), 0.0, 0.0, -1.0);
	osg::Matrix rotateDebugText2 = osg::Matrix::rotate(osg::DegreesToRadians(180.0), 0.0, 1.0, 0.0);
	osg::Matrix positionDebugText = osg::Matrix::translate(osg::Vec3(0.03, -0.04, 0.0));
	osg::Matrix finalTransform = rotateDebugText2 * rotateDebugText1 * positionDebugText;
	m_textTransform->setMatrix(finalTransform);
	m_textTransform->addChild(m_text);
	m_matrixTransform->addChild(m_textTransform);

	//Controller Pointer
	m_drawableControllerPointer = new osg::Geometry();
	m_geodeControllerPointer = new osg::Geode();
	m_geodeControllerPointer->addDrawable(m_drawableControllerPointer);
	m_matrixTransform->addChild(m_geodeControllerPointer);

	osg::LineWidth* lineWidthPointer = new osg::LineWidth();
	lineWidthPointer->setWidth(7.0);
	m_geodeControllerPointer->getOrCreateStateSet()->setAttributeAndModes(lineWidthPointer, osg::StateAttribute::OVERRIDE | osg::StateAttribute::ON);

	osg::ref_ptr<osg::Vec4Array> colorArrayControllerPointer = new osg::Vec4Array;
	osg::Vec4 colorGreen = osg::Vec4(0.0f, 1.0f, 0.0f, 1.0f);;
	colorArrayControllerPointer->push_back(colorGreen);
	colorArrayControllerPointer->push_back(colorGreen);

	osg::ref_ptr<osg::Vec3Array> controllerPointerVertices = new osg::Vec3Array;
	controllerPointerVertices->push_back(osg::Vec3f(0.0, 0.0, 0.0));
	controllerPointerVertices->push_back(osg::Vec3f(0.0, 0.0, 0.05));
	m_drawableControllerPointer->setVertexArray(controllerPointerVertices);

	osg::ref_ptr<osg::DrawElementsUInt> contrPointerLine = new osg::DrawElementsUInt(osg::PrimitiveSet::LINES, 0);
	contrPointerLine->push_back(1);
	contrPointerLine->push_back(0);
	m_drawableControllerPointer->addPrimitiveSet(contrPointerLine);

	m_drawableControllerPointer->setColorArray(colorArrayControllerPointer);
	m_drawableControllerPointer->setColorBinding(osg::Geometry::BIND_PER_VERTEX);

	osg::ref_ptr<osg::Vec4Array> colorArrayRulerLine = new osg::Vec4Array;
	osg::Vec4 colorRed = osg::Vec4(1.0f, 0.0f, 0.0f, 1.0f);;
	colorArrayRulerLine->push_back(colorRed);
	colorArrayRulerLine->push_back(colorRed);
	m_drawableRulerLine->setColorArray(colorArrayRulerLine);
	m_drawableRulerLine->setColorBinding(osg::Geometry::BIND_PER_VERTEX);

	m_rulerLineVertices = new osg::Vec3Array;

	m_indicies = new osg::DrawElementsUInt(osg::PrimitiveSet::LINES, 0);

	//WIP: Path Measurement: Uncooment follwoing line!!!!!!!!!!!!!!
	//m_rulerLine = new osg::DrawElementsUInt(osg::PrimitiveSet::LINES, 0);
	//m_rulerLineVertices = new osg::Vec3Array;
	//m_colorArrayRulerLine = new osg::Vec4Array;
}

RulerControllerTool::~RulerControllerTool()
{

}

//Remove empty update function in all ControllerTools
void RulerControllerTool::update()
{

}

void RulerControllerTool::update(bool)
{
	//WIP: Path Measurement: Uncooment follwoing line!!!!!!!!!!!!!!
	m_isPathMeasurementEnabled = false;

	if (m_isPathMeasurementEnabled)
	{
		//WIP: Path Measurement: Uncooment follwoing line!!!!!!!!!!!!!!
		//std::cout << "DRIN " << std::endl;
		////m_geodeRulerLine->removeDrawables(0, m_geodeRulerLine->getNumDrawables());

		////RulerLine
		////m_drawableRulerLine = new osg::Geometry();

		////m_geodeRulerLine->addDrawable(m_drawableRulerLine);

		//osg::Vec4 colorRed = osg::Vec4(1.0f, 0.0f, 0.0f, 1.0f);;
		//m_colorArrayRulerLine->push_back(colorRed);
		//m_colorArrayRulerLine->push_back(colorRed);

		////Here its important getting the LocalToWorld matrix from the controller movement...
		//m_geomMatrix = osg::computeLocalToWorld(m_matrixTransform->getParentalNodePaths()[0]);
		////and take the inverse with the start and end point to retain global points
		//m_rulerLineVertices->push_back(m_startPoint * osg::Matrix::inverse(m_geomMatrix));
		//m_rulerLineVertices->push_back(m_endPoint * osg::Matrix::inverse(m_geomMatrix));
		//m_drawableRulerLine->setVertexArray(m_rulerLineVertices);

		//for(unsigned int i = m_rulerLineVertices->size(); i > 0 ; --i)
		//{
		//	m_rulerLine->push_back(i-1);
		//}
		//m_drawableRulerLine->addPrimitiveSet(m_rulerLine);

		//m_drawableRulerLine->setColorArray(m_colorArrayRulerLine);
		//m_drawableRulerLine->setColorBinding(osg::Geometry::BIND_PER_VERTEX);

		////Text Stuff
		//osg::Vec3f lengthVector = m_endPoint - m_startPoint;
		//float length = lengthVector.length();
		//m_text->setText(std::to_string(length) + "m");
	}
	else
	{
		if (m_showRulerLine)
		{
			//Showing the small tip in front of the controller:
			if (m_geodeControllerPointer->getNumDrawables() == 0)
			{
				m_geodeControllerPointer->addDrawable(m_drawableControllerPointer);
			}

			m_geodeRulerLine->removeDrawables(0, m_geodeRulerLine->getNumDrawables());
			m_geodeRulerLine->addDrawable(m_drawableRulerLine);

			//Here its important getting the LocalToWorld matrix from the controller movement...
			m_local2WorldMatrix = osg::computeLocalToWorld(m_matrixTransform->getParentalNodePaths()[0]);
			//...and take the inverse with the start and end point to retain global points
			m_rulerLineVertices->clear();
			m_rulerLineVertices->push_back(m_startPoint * osg::Matrix::inverse(m_local2WorldMatrix));
			m_rulerLineVertices->push_back(m_endPoint * osg::Matrix::inverse(m_local2WorldMatrix));
			m_rulerLineVertices->dirty();
			m_drawableRulerLine->setVertexArray(m_rulerLineVertices);

			m_indicies->clear();
			m_indicies->push_back(1);
			m_indicies->push_back(0);
			m_indicies->dirty();
			m_drawableRulerLine->addPrimitiveSet(m_indicies);
		}
		else
		{
			//Remove the small tip in front of the controller:
			m_geodeControllerPointer->removeChildren(0, m_geodeControllerPointer->getNumChildren());
		}
		//Text Stuff
		if (m_textTransform->getNumChildren() == 0)
		{
			m_textTransform->addChild(m_text);
		}
		osg::Vec3f lengthVector = m_endPoint - m_startPoint;

		float length = lengthVector.length();

		//Displays grabVverticiesObserver interaction
		if (*m_miscDataStorage->getAddressOfIsEDITranslateToolActive())
		{
			std::string x = std::to_string((double)floor((lengthVector.x() * 1000.0) + 0.5) / 1000.0);
			x.erase(x.find_last_not_of('0') + 1, std::string::npos);
			if (x.compare("0.") == 0)
			{
				x = "0.000";
			}

			std::string y = std::to_string((double)floor((lengthVector.y() * 1000.0) + 0.5) / 1000.0);
			y.erase(y.find_last_not_of('0') + 1, std::string::npos);
			if (y.compare("0.") == 0)
			{
				y = "0.000";
			}

			std::string z = std::to_string((double)floor((lengthVector.z() * 1000.0) + 0.5) / 1000.0);
			z.erase(z.find_last_not_of('0') + 1, std::string::npos);
			if (z.compare("0.") == 0)
			{
				z = "0.000";
			}

			std::string lengthString = std::to_string((double)floor((length * 1000.0) + 0.5) / 1000.0);
			lengthString.erase(lengthString.find_last_not_of('0') + 1, std::string::npos);
			if (lengthString.compare("0.") == 0)
			{
				lengthString = "0.000";
			}

			std::string text = "X:         "; text.append(x); text.append(" m\n");
			text.append("Y:         ");  text.append(y); text.append(" m\n");
			text.append("Z:         "); text.append(z); text.append(" m\n");
			text.append("Length: ");  text.append(lengthString); text.append(" m\n"); 
			m_text->setText(text);
		}
		//Displays scaleVerticesObserver interaction
		else if (*m_miscDataStorage->getAddressOfIsEDIScaleToolActive())
		{
			std::string text;
			if (m_geometryManager->getConstraint() == X_AXIS)
			{
				text.append("X:    " + std::to_string((m_currentScale / m_startScale) * 100.0 - 100.0) + "%\n");
				text.append("Y:    " + std::to_string(0.0) + "%\n");
				text.append("Z:    " + std::to_string(0.0) + "%\n");
			}
			else if (m_geometryManager->getConstraint() == Y_AXIS)
			{
				text.append("X:    " + std::to_string(0.0) + "%\n");
				text.append("Y:    " + std::to_string((m_currentScale / m_startScale) * 100.0 - 100.0) + "%\n");
				text.append("Z:    " + std::to_string(0.0) + "%\n");
			}
			else if (m_geometryManager->getConstraint() == Z_AXIS)
			{
				text.append("X:    " + std::to_string(0.0) + "%\n");
				text.append("Y:    " + std::to_string(0.0) + "%\n");
				text.append("Z:    " + std::to_string((m_currentScale / m_startScale) * 100.0 - 100.0) + "%\n");
			}
			else if (m_geometryManager->getConstraint() == NO_CONSTRAIN)
			{
				text.append("X:    " + std::to_string((m_currentScale / m_startScale) * 100.0 - 100.0) + "%\n");
				text.append("Y:    " + std::to_string((m_currentScale / m_startScale) * 100.0 - 100.0) + "%\n");
				text.append("Z:    " + std::to_string((m_currentScale / m_startScale) * 100.0 - 100.0) + "%\n");
			}
			m_text->setText(text);
		}
		//DisplaysrotateVerticesObserver interaction
		else if (*m_miscDataStorage->getAddressOfIsEDIRotateToolActive())
		{
			//TODO: This is the code from GrabRotateScaleProcessor.cpp. Code reduction necessary. At least exchange the address/val of snappingValue
			double snappingValue = M_PI_2 / 6.0; //15 degrees
			if (m_geometryManager->getConstraint() == X_AXIS)
			{
				//Set the y and z component to zero and pass only the values of...
				m_endRotation.y() = 0.0;
				m_endRotation.z() = 0.0;

				//x and w, but normalize the Quat again with the sqrt()
				double mag = sqrt(m_endRotation.w() * m_endRotation.w() + m_endRotation.x() * m_endRotation.x());
				m_endRotation.x() /= mag;
				m_endRotation.w() /= mag;

				//If snapping is active, snap to every "snappingValue" in [rad]. Therefore convert in rad, snap in rad an convert it back to between +1.0 and -1.0
				if (m_geometryManager->isSnappingActive())
				{
					double angX = 2 * acos(m_endRotation.x());
					double snappedAngX = (floorf((angX / snappingValue) + 0.5) * snappingValue);
					m_endRotation.x() = cos(snappedAngX / 2.0);
				}
			}
			else if (m_geometryManager->getConstraint() == Y_AXIS)
			{
				m_endRotation.x() = 0.0;
				m_endRotation.z() = 0.0;

				double mag = sqrt(m_endRotation.w() * m_endRotation.w() + m_endRotation.y() * m_endRotation.y());
				m_endRotation.y() /= mag;
				m_endRotation.w() /= mag;

				if (m_geometryManager->isSnappingActive())
				{
					double angY = 2 * acos(m_endRotation.y());
					double snappedAngY = (floorf((angY / snappingValue) + 0.5) * snappingValue);
					m_endRotation.y() = cos(snappedAngY / 2.0);
				}

			}
			else if (m_geometryManager->getConstraint() == Z_AXIS)
			{
				m_endRotation.x() = 0.0;
				m_endRotation.y() = 0.0;

				double mag = sqrt(m_endRotation.w() * m_endRotation.w() + m_endRotation.z() * m_endRotation.z());
				m_endRotation.z() /= mag;
				m_endRotation.w() /= mag;

				if (m_geometryManager->isSnappingActive())
				{
					double angZ = 2 * acos(m_endRotation.z());
					double snappedAngZ = (floorf((angZ / snappingValue) + 0.5) * snappingValue);
					m_endRotation.z() = cos(snappedAngZ / 2.0);
				}
			}
			else if (m_geometryManager->getConstraint() == NO_CONSTRAIN)
			{
				if (m_geometryManager->isSnappingActive())
				{
					double angX = 2 * acos(m_endRotation.x());
					double snappedAngX = (floorf((angX / snappingValue) + 0.5) * snappingValue);
					m_endRotation.x() = cos(snappedAngX / 2.0);

					double angY = 2 * acos(m_endRotation.y());
					double snappedAngY = (floorf((angY / snappingValue) + 0.5) * snappingValue);
					m_endRotation.y() = cos(snappedAngY / 2.0);

					double angZ = 2 * acos(m_endRotation.z());
					double snappedAngZ = (floorf((angZ / snappingValue) + 0.5) * snappingValue);
					m_endRotation.z() = cos(snappedAngZ / 2.0);
				}
			}

			double angW = 2 * acos(m_endRotation.w());
			double snappedAngW = (floorf((angW / snappingValue) + 0.5) * snappingValue);
			m_endRotation.w() = cos(snappedAngW / 2.0);

			double yaw, roll, pitch;
			Quat2EulerAndBackConverter::getEulerFromQuat(m_endRotation, yaw, roll, pitch);
			std::string text = "X:    " + std::to_string((int)floorf((pitch / M_PI) * 180.0)) + "�\n";
			text.append("Y:    " + std::to_string((int)floorf((yaw / M_PI) * 180.0)) + "�\n");
			text.append("Z:    " + std::to_string((int)floorf((roll / M_PI) * 180.0)) + "�\n");

			m_text->setText(text);
		}


	}
}

void RulerControllerTool::hide()
{
	m_textTransform->removeChildren(0, m_textTransform->getNumChildren());
	m_geodeRulerLine->removeDrawables(0, m_geodeRulerLine->getNumDrawables());
	m_geodeControllerPointer->removeChildren(0, m_geodeControllerPointer->getNumChildren());
}