// ClayMore - Immersive Mesh Modelling --- Copyright (c) 2014-2017 Philipp Ladwig, Jannik Fiedler, Jan Beutgen
#pragma once

#include "cmInteractionManagement\Observer.h"
#include <osg/ShapeDrawable>
#include <osg/Geode>
#include <osg/PositionAttitudeTransform>

class SphereControllerTool : public ControllerTool
{
public:
	SphereControllerTool(float sphereSize);
	~SphereControllerTool();

	void update();

	double getSphereScaleValue() { return m_sphereScaleMatrix->getScale().x(); }
	void setSphereScaleValue(double sphereScaleValue) { m_sphereScaleMatrix->setScale(osg::Vec3d(sphereScaleValue, sphereScaleValue, sphereScaleValue)); }
	inline void setSphereColor(osg::Vec4f sphereColorIn) { m_sphereColor = sphereColorIn;  }
	inline osg::Vec4f getSphereColor() { return m_sphereColor; }
	inline void isShownInWorldCoords(bool isShownInWorldCoordsIn) { m_isShownInWorldCoordsIn = isShownInWorldCoordsIn; }

private:
	osg::ref_ptr<osg::ShapeDrawable> m_selectionSphere;
	osg::ref_ptr<osg::Geode> m_geode;
	osg::ref_ptr<osg::PositionAttitudeTransform> m_sphereScaleMatrix;

	//TODO: Finish PropEdit Sphere attaching to the Barycentrum of selected Vertices
	osg::ref_ptr < osg::MatrixTransform> m_renderInWorldCoordsOffset;

	osg::Vec4f m_sphereColor;
	bool m_isShownInWorldCoordsIn;
};