// ClayMore - Immersive Mesh Modelling --- Copyright (c) 2014-2017 Philipp Ladwig, Jannik Fiedler, Jan Beutgen
#pragma once

#include "cmInteractionManagement\Observer.h"
#include "cmGeometryManagement\Constraints.h"

class GeometryManager;
//class MiscDataStorage;
class Geode;
class Geometry;

class VisualizeBarycentrumControllerTool : public ControllerTool
{
public:
	VisualizeBarycentrumControllerTool(ConfigReader* configReaderIn);
	~VisualizeBarycentrumControllerTool();

	inline void setGeometryManager(osg::ref_ptr<GeometryManager> geometryManager) { m_geometryManager = geometryManager; };
	//inline void setMiscDataStorage(MiscDataStorage* miscDataStorageIn) { m_miscDataStorage = miscDataStorageIn; };

	//TODO: @JANNIK: It seems, that update() is always and everytime called, right? Maybe better if it's only callable in a manual way. This would avoid the function updateCross()
	void update();
	//Updates the position and drawing of the cross. Barycentrum will be obtained by selected verticies
	void updateCross();
	//Same as update(). Updates the position and drawing of the cross. But Barycentrum is passed through here. This is for example, if no vertex is selected and
	//	the barycentrum have to be calculated over all vertices in a preceding step (because is also used by e.g. dashedLineController Tool)
	void updateCross(osg::Vec3d bayrcentrumIn);

	void removeLines();


private:
	osg::ref_ptr<GeometryManager> m_geometryManager;
	//MiscDataStorage* m_miscDataStorage;

	ConfigReader* m_configReader;
	float m_lineLength, m_lineLengthCross;

	//TODO4: Has every ControllerTool a Geode? Inherit it from ControllerTool, maybe better.
	osg::ref_ptr<osg::Geode> m_geode;
	osg::ref_ptr<osg::Geometry> m_drawable;
	osg::ref_ptr<osg::Vec3Array> m_lineVertices;
	osg::ref_ptr<osg::DrawElementsUInt> m_indicies;
	osg::ref_ptr<osg::Vec4Array> m_colorArray;

	osg::ref_ptr<osg::PositionAttitudeTransform> m_posAtt;

};