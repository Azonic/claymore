// ClayMore - Immersive Mesh Modelling --- Copyright (c) 2014-2017 Philipp Ladwig, Jannik Fiedler, Jan Beutgen
#include "PickRayControllerTool.h"
#include <osg/CullFace>
#include <osg/PolygonMode>
#include <osgText/Text>
#include <osgText/Font>
#include <iostream>

PickRayControllerTool::PickRayControllerTool()
{
	m_matrix = new osg::PositionAttitudeTransform();

	m_pickRay = new osg::ShapeDrawable(new osg::Cylinder(osg::Vec3f(0.0, 0.0, -25.05), 0.0025, 50.0));
	m_geode = new osg::Geode();
	m_geode->getOrCreateStateSet();
	osg::ref_ptr<osg::StateSet> stateSet = new osg::StateSet();
	m_geode->setStateSet(stateSet);

	osg::ref_ptr<osg::CullFace> cullFace = new osg::CullFace();
	cullFace->setMode(osg::CullFace::BACK);
	stateSet->setAttributeAndModes(cullFace, osg::StateAttribute::OFF);

	m_pickRay->setColor(osg::Vec4(1.0f, 0.0f, 0.0f, 1.0f));

	m_geode->addChild(m_pickRay);
	m_matrixTransform->addChild(m_matrix);
	m_matrix->addChild(m_geode);
}

PickRayControllerTool::~PickRayControllerTool()
{

}

void PickRayControllerTool::update()
{

}

void PickRayControllerTool::update(bool isIntersectionFound)
{
	//m_matrixTransform->setMatrix(registeredControllerIn->getMatrixTransform()->getMatrix());
	if (isIntersectionFound)
	{
		m_pickRay->setColor(osg::Vec4(0.0f, 1.0f, 0.0f, 1.0f));
	}
	else
	{
		m_pickRay->setColor(osg::Vec4(1.0f, 0.0f, 0.0f, 1.0f));
	}
}