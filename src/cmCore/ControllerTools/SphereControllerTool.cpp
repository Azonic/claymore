// ClayMore - Immersive Mesh Modelling --- Copyright (c) 2014-2017 Philipp Ladwig, Jannik Fiedler, Jan Beutgen
#include "SphereControllerTool.h"
#include <osg/CullFace>
#include <osg/PolygonMode>
#include <osgText/Text>
#include <osgText/Font>

SphereControllerTool::SphereControllerTool(float sphereSize)
{
	m_sphereScaleMatrix = new osg::PositionAttitudeTransform();

	//TODO: Finish PropEdit Sphere attaching to the Barycentrum of selected Vertices
	m_renderInWorldCoordsOffset = new osg::MatrixTransform();

	m_selectionSphere = new osg::ShapeDrawable(new osg::Sphere(osg::Vec3d(), sphereSize));
	//0.075f
	m_geode = new osg::Geode();
	m_geode->getOrCreateStateSet();
	osg::ref_ptr<osg::StateSet> stateSet = new osg::StateSet();
	m_geode->setStateSet(stateSet);

	//Disable lighting, enable transparency
	stateSet->setMode(GL_LIGHTING, osg::StateAttribute::OVERRIDE | osg::StateAttribute::OFF);
	stateSet->setMode(GL_BLEND, osg::StateAttribute::ON);
	stateSet->setRenderingHint(osg::StateSet::TRANSPARENT_BIN);
	stateSet->setRenderBinDetails(17, "DepthSortedBin");
	stateSet->setAttribute(new osg::PolygonMode(osg::PolygonMode::FRONT, osg::PolygonMode::LINE));


	osg::ref_ptr<osg::CullFace> cullFace = new osg::CullFace();
	cullFace->setMode(osg::CullFace::BACK);
	stateSet->setAttributeAndModes(cullFace, osg::StateAttribute::OFF);

	m_sphereColor = osg::Vec4(0.0f, 1.0f, 0.2f, 0.15f);
	m_selectionSphere->setColor(m_sphereColor);

	m_geode->addChild(m_selectionSphere);
	m_matrixTransform->addChild(m_sphereScaleMatrix);
	m_sphereScaleMatrix->addChild(m_renderInWorldCoordsOffset);
	m_renderInWorldCoordsOffset->addChild(m_geode);

	//Temp Help Text
	//osg::ref_ptr<osgText::Font> font = osgText::readFontFile("fonts/arial.ttf");
	//osg::ref_ptr<osgText::Text> hintText = new osgText::Text;

	//hintText->setFont(font.get());
	//hintText->setCharacterSize(1.0);
	//hintText->setAxisAlignment(osgText::TextBase::YZ_PLANE);
	//hintText->setText("Help Text");
	//hintText->getOrCreateStateSet()->setMode(GL_LIGHTING, osg::StateAttribute::OVERRIDE | osg::StateAttribute::OFF);
	//hintText->getOrCreateStateSet()->setRenderBinDetails(16, "DepthSortedBin");

	//osg::ref_ptr<osg::MatrixTransform> textTransform = new osg::MatrixTransform();
	//osg::Matrix rotate1 = osg::Matrix::rotate(osg::DegreesToRadians(90.0), 0.0, 0.0, -1.0);
	//osg::Matrix rotate2 = osg::Matrix::rotate(osg::DegreesToRadians(180.0), 0.0, 1.0, 0.0);
	//osg::Matrix position = osg::Matrix::translate(osg::Vec3(0.1, 0.0, 0.0));
	//osg::Matrix finalTransform = rotate2 * rotate1 * position;
	//textTransform->setMatrix(finalTransform);
	//textTransform->addChild(hintText);
	//m_matrixTransform->addChild(textTransform);
}

SphereControllerTool::~SphereControllerTool()
{

}

void SphereControllerTool::update()
{
	m_selectionSphere->setColor(m_sphereColor);
	//m_matrixTransform->setMatrix(registeredControllerIn->getMatrixTransform()->getMatrix());
}

//void SphereControllerTool::increaseSphereScaleValue()
//{
//	double sphereScaleValue = getSphereScaleValue();
//
//	setSphereScaleValue(sphereScaleValue * 1.015);
//}
//
//void SphereControllerTool::decreaseSphereScaleValue()
//{
//	double sphereScaleValue = getSphereScaleValue();
//
//	setSphereScaleValue(sphereScaleValue * 0.985);
//}