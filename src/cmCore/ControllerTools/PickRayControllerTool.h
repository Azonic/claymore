// ClayMore - Immersive Mesh Modelling --- Copyright (c) 2014-2017 Philipp Ladwig, Jannik Fiedler, Jan Beutgen
#pragma once

#include "cmInteractionManagement\Observer.h"
#include <osg/ShapeDrawable>
#include <osg/Geode>
#include <osg/PositionAttitudeTransform>

class PickRayControllerTool : public ControllerTool
{
public:
	PickRayControllerTool();
	~PickRayControllerTool();

	void update();
	void update(bool isIntersectionFound);

private:
	osg::ref_ptr<osg::ShapeDrawable> m_pickRay;
	osg::ref_ptr<osg::Geode> m_geode;
	osg::ref_ptr<osg::PositionAttitudeTransform> m_matrix;
};