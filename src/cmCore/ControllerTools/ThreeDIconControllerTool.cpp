// ClayMore - Immersive Mesh Modelling --- Copyright (c) 2014-2017 Philipp Ladwig, Jannik Fiedler, Jan Beutgen
#include "ThreeDIconControllerTool.h"

#include <osg/PolygonMode>

#include <osgDB/ReadFile>

//#include <osg/io_utils>

ThreeDIconControllerTool::ThreeDIconControllerTool()
{
	osg::ref_ptr<osg::StateSet> stateSet = new osg::StateSet();
	stateSet->setMode(GL_LIGHTING, osg::StateAttribute::OFF);

	m_IconMatrixTransform = new osg::MatrixTransform;
	m_IconMatrixTransform->setStateSet(stateSet);


	osg::Matrix matrixScale;
	matrixScale.makeScale(0.09, 0.09, 0.09);

	osg::Matrix matrixRotate;
	matrixRotate.makeRotate(M_PI_4 + -0.18, 1.0, 0.0, 0.0);

	osg::Matrix matrixTranslation;
	matrixTranslation.makeTranslate(0.0, -0.04, -0.02);

	m_IconMatrixTransform->setMatrix(matrixScale * matrixTranslation * matrixRotate);
	m_matrixTransform->addChild(m_IconMatrixTransform);

	m_moveIcon = osgDB::readNodeFile("data/controller_models/threeDIconMove.obj");
	m_rotateIcon = osgDB::readNodeFile("data/controller_models/threeDIconRotate.obj");
	m_scaleIcon = osgDB::readNodeFile("data/controller_models/threeDIconScale.obj");

	m_isShowMoveIconActivated = false;
	m_isShowRotateIconActivated = false;
	m_isShowScaleIconActivated = false;
}

ThreeDIconControllerTool::~ThreeDIconControllerTool()
{

}

void ThreeDIconControllerTool::update()
{
	if (m_isShowMoveIconActivated)
	{
		if(!(m_IconMatrixTransform->containsNode(m_moveIcon)))
			m_IconMatrixTransform->addChild(m_moveIcon);
	}
	else
	{
		m_IconMatrixTransform->removeChild(m_moveIcon);
	}


	if (m_isShowRotateIconActivated)
	{
		if (!(m_IconMatrixTransform->containsNode(m_rotateIcon)))
		m_IconMatrixTransform->addChild(m_rotateIcon);
	}
	else
	{
		m_IconMatrixTransform->removeChild(m_rotateIcon);
	}

	if (m_isShowScaleIconActivated)
	{
		if (!(m_IconMatrixTransform->containsNode(m_scaleIcon)))
		m_IconMatrixTransform->addChild(m_scaleIcon);
	}
	else
	{
		m_IconMatrixTransform->removeChild(m_scaleIcon);
	}

	//if (!(m_isShowMoveIconActivated || m_isShowRotateIconActivated || m_isShowScaleIconActivated))
	//{
	//	if (m_IconMatrixTransform->getNumChildren() != 0)
	//	{
	//		m_IconMatrixTransform->removeChildren(0, m_IconMatrixTransform->getNumChildren());
	//	}
	//}
}