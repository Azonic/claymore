// ClayMore - Immersive Mesh Modelling --- Copyright (c) 2014-2017 Philipp Ladwig, Jannik Fiedler, Jan Beutgen
#include "DashedLineControllerTool.h"
//#include <osg/CullFace>
//#include <osg/PolygonMode>
//#include <osgText/Text>
//#include <osgText/Font>
#include <osg/LineWidth>
#include <osg/Texture1D>
#include <osg/TexGenNode>

DashedLineControllerTool::DashedLineControllerTool()
{
	m_showLine = false;

	m_texgenNode = new osg::TexGenNode;
	m_texgenNode->setReferenceFrame(osg::TexGenNode::ABSOLUTE_RF);
	m_texgenNode->getTexGen()->setMode(osg::TexGen::OBJECT_LINEAR);

	m_geode = new osg::Geode();
	m_geode->getOrCreateStateSet();
	osg::ref_ptr<osg::StateSet> stateSet = new osg::StateSet();
	m_geode->setStateSet(stateSet);

	m_geometry = new osg::Geometry();
	m_geode->addChild(m_geometry);
	m_texgenNode->addChild(m_geode);
	m_matrixTransform->addChild(m_texgenNode);

	//Disable lighting, enable transparency
	stateSet->setMode(GL_LIGHTING, osg::StateAttribute::OVERRIDE | osg::StateAttribute::OFF);
	stateSet->setMode(GL_BLEND, osg::StateAttribute::ON);
	stateSet->setRenderingHint(osg::StateSet::TRANSPARENT_BIN);
	stateSet->setRenderBinDetails(17, "DepthSortedBin");
	osg::LineWidth* lineWidth = new osg::LineWidth();
	lineWidth->setWidth(20.0f);
	stateSet->setAttributeAndModes(lineWidth, osg::StateAttribute::OVERRIDE | osg::StateAttribute::ON);

	m_startPoint = osg::Vec3f(0.0, 0.0, 0.0);
	m_endPoint = osg::Vec3f(0.0, 0.0, 0.0);

	m_rulerLineVertices = new osg::Vec3Array();
	
	m_indicies = new osg::DrawElementsUInt(osg::PrimitiveSet::LINES, 0);

	m_colorArrayRulerLine = new osg::Vec4Array();
	osg::Vec4 colorRed = osg::Vec4(0.0f, 0.0f, 0.0f, 1.0f);;
	m_colorArrayRulerLine->push_back(colorRed);
	m_colorArrayRulerLine->push_back(colorRed);


	//Create Dashed Lines
	osg::Image* image = new osg::Image;
	//Resolution of the 1d texture
	int numPixels = 1024;
	//Length of the dashes in pixel
	unsigned int dashLength = 10;

	// allocate the image data, numPixels x 1 x 1 with 4 rgba floats - equivalent to a Vec4!
	image->allocateImage(numPixels, 1, 1, GL_RGBA, GL_FLOAT);
	image->setInternalTextureFormat(GL_RGBA);

	osg::Vec4* dataPtr = (osg::Vec4*)image->data();

	//2 different colors
	osg::Vec4f visibleDash = osg::Vec4f(0.0, 0.0, 0.0, 1.0);
	osg::Vec4f invisibleDash = osg::Vec4f(0.0, 0.0, 0.0, 0.0);

	for (size_t i = 0; i < numPixels; ++i)
	{
		int flipAlpha = floorf(i / dashLength);
		//depending if odd or even, set the color for every "dashLength"
		if ((flipAlpha % 2) != 0)
		{
			*dataPtr++ = invisibleDash;
		}
		else
		{
			*dataPtr++ = visibleDash;
		}
	}

	osg::Texture1D* texture = new osg::Texture1D;
	texture->setWrap(osg::Texture1D::WRAP_S, osg::Texture1D::MIRROR);
	texture->setFilter(osg::Texture1D::MIN_FILTER, osg::Texture1D::LINEAR);
	texture->setImage(image);

	stateSet->setTextureAttribute(0, texture, osg::StateAttribute::OVERRIDE);
	stateSet->setTextureMode(0, GL_TEXTURE_1D, osg::StateAttribute::ON | osg::StateAttribute::OVERRIDE);
	stateSet->setTextureMode(0, GL_TEXTURE_2D, osg::StateAttribute::OFF | osg::StateAttribute::OVERRIDE);
	stateSet->setTextureMode(0, GL_TEXTURE_3D, osg::StateAttribute::OFF | osg::StateAttribute::OVERRIDE);

	stateSet->setTextureMode(0, GL_TEXTURE_GEN_S, osg::StateAttribute::ON | osg::StateAttribute::OVERRIDE);

	//Despite it's a 1d texture one have to use 2d coords...is there a 1d array?
	osg::Vec2Array* texcoords = new osg::Vec2Array(2);
	(*texcoords)[0].set(0.0f, 0.0f);
	(*texcoords)[1].set(1.0f, 0.0f);
	m_geometry->setTexCoordArray(0, texcoords);

	//Create dashes with shader
	//osg::Program* flatWireProgram = new osg::Program;
	//osg::Shader* flatWireVertex = new osg::Shader(osg::Shader::VERTEX);
	//osg::Shader* flatWireFragment = new osg::Shader(osg::Shader::FRAGMENT);
	//flatWireProgram->addShader(flatWireFragment);
	//flatWireProgram->addShader(flatWireVertex);
	//flatWireVertex->loadShaderSourceFromFile("./data/shader/DASHED_LINE.vert");
	//flatWireFragment->loadShaderSourceFromFile("./data/shader/DASHED_LINE.frag");

	////flatWireProgram->setParameter(GL_GEOMETRY_VERTICES_OUT_EXT, 3);
	////flatWireProgram->setParameter(GL_GEOMETRY_INPUT_TYPE_EXT, GL_TRIANGLES);
	////flatWireProgram->setParameter(GL_GEOMETRY_OUTPUT_TYPE_EXT, GL_TRIANGLES);

	////ss->setMode(GL_LIGHTING, osg::StateAttribute::OFF);
	////ss->setMode(GL_BLEND, osg::StateAttribute::ON);
	////ss->setRenderingHint(osg::StateSet::TRANSPARENT_BIN);
	////ss->setRenderBinDetails(18, "DepthSortedBin");

	//stateSet->setAttributeAndModes(flatWireProgram, osg::StateAttribute::ON);
}

DashedLineControllerTool::~DashedLineControllerTool()
{

}

void DashedLineControllerTool::update()
{
	if(m_showLine)
	{
	//Here its important getting the LocalToWorld matrix from the controller movement...
	osg::Matrix local2WorldMatrix = osg::computeLocalToWorld(m_matrixTransform->getParentalNodePaths()[0]);
	//...and take the inverse with the start and end point to retain global points
	m_rulerLineVertices->clear();
	m_rulerLineVertices->push_back(m_startPoint * osg::Matrix::inverse(local2WorldMatrix));
	m_rulerLineVertices->push_back(m_endPoint * osg::Matrix::inverse(local2WorldMatrix));
	m_rulerLineVertices->dirty();
	m_geometry->setVertexArray(m_rulerLineVertices);

	m_indicies->clear();
	m_indicies->push_back(1);
	m_indicies->push_back(0);
	m_indicies->dirty();
	m_geometry->addPrimitiveSet(m_indicies);

	m_geometry->setColorArray(m_colorArrayRulerLine);
	m_geometry->setColorBinding(osg::Geometry::BIND_PER_VERTEX);
	}
	else
	{
		m_rulerLineVertices->clear();
		m_rulerLineVertices->push_back(osg::Vec3f(0.0, 0.0, 0.0));
		m_rulerLineVertices->push_back(osg::Vec3f(0.0, 0.0, 0.0));
		m_rulerLineVertices->dirty();
		m_geometry->setVertexArray(m_rulerLineVertices);
	}
}