// ClayMore - Immersive Mesh Modelling --- Copyright (c) 2014-2017 Philipp Ladwig, Jannik Fiedler, Jan Beutgen
#pragma once

#include "cmInteractionManagement\Observer.h"
#include "cmGeometryManagement\Constraints.h"

class GeometryManager;
class MiscDataStorage;
//class Geode;
//class Geometry;

class VisualizeAxisAndGridControllerTool : public ControllerTool
{
public:
	VisualizeAxisAndGridControllerTool(ConfigReader* configReaderIn, osg::ref_ptr<GeometryManager> geometryManagerIn, MiscDataStorage* miscDataStorageIn);
	~VisualizeAxisAndGridControllerTool();

	void update();
	void update(bool isSnappingActive, Constraints constrainIn, bool isLocalCoordSysSelectedIn, osg::Vec3f baryCentrumAtStartIn, osg::Vec3f deltaPositionIn);
	void removeLines();

private:
	osg::ref_ptr<GeometryManager> m_geometryManager;
	MiscDataStorage* m_miscDataStorage;

	ConfigReader* m_configReader;
	float m_lineLength, m_lineLengthCross;

	//TODO4: Has every ControllerTool a Geode? Inherit it from ControllerTool, maybe better.
	osg::ref_ptr<osg::Geode> m_geodeForAxisVisualization;
	osg::ref_ptr<osg::Geode> m_geodeForGridVisualization;
	osg::ref_ptr<osg::Geometry> m_drawableForAxisVisualization;
	osg::ref_ptr<osg::Geometry> m_drawableForGridVisualization;
	osg::ref_ptr<osg::Vec3Array> m_lineVertices;
	osg::ref_ptr<osg::Vec3Array> m_gridVertices;
	osg::ref_ptr<osg::DrawElementsUInt> m_lineIndicies;
	osg::ref_ptr<osg::DrawElementsUInt> m_gridIndicies;
	osg::ref_ptr<osg::Vec4Array> m_lineColorArray;
	osg::ref_ptr<osg::Vec4Array> m_gridColorArray;

	osg::ref_ptr<osg::PositionAttitudeTransform> m_visulizeAxisPosAtt;

	//OBSCURE: Compiler said with: osg::ref_ptr<osg::Vec3f> m_startPoint; that ref and unref no function of osg::Vec3f are. Why???
	//If only a pointer, it works...added a delete command to destructor now
	osg::Vec3f* m_startPoint;
};