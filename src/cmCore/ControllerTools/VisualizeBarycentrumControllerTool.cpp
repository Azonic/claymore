// ClayMore - Immersive Mesh Modelling --- Copyright (c) 2014-2017 Philipp Ladwig, Jannik Fiedler, Jan Beutgen
#include "VisualizeBarycentrumControllerTool.h"
#include "cmGeometryManagement\GeometryManager.h"
//#include "../MiscDataStorage.h"

#include <osg/Geode>
#include <osg/Geometry>
#include <osg/PolygonMode>
//#include <osg/io_utils> //for "c-outing" osg::matrices

VisualizeBarycentrumControllerTool::VisualizeBarycentrumControllerTool(ConfigReader* configReaderIn) : m_configReader(configReaderIn)
{
	//TODO / PERFORMANCE: change all geometries to VBOs -> like the geometryData geometries -> currently displayLists are used
	m_lineLengthCross = atof(m_configReader->getStringFromStartupConfig("line_length_for_barycentrum_cross").c_str());

	m_lineVertices = new osg::Vec3Array;
	m_indicies = new osg::DrawElementsUInt(osg::PrimitiveSet::LINES, 0);
	m_colorArray = new osg::Vec4Array;

	m_drawable = new osg::Geometry();
	m_posAtt = new osg::PositionAttitudeTransform();
	m_geode = new osg::Geode();
	m_matrixTransform->addChild(m_posAtt);
	m_posAtt->addChild(m_geode);

	//EXP: Prepare Stuff for visualize AxisSnapping
	osg::ref_ptr<osg::PolygonMode> polymode = new osg::PolygonMode;
	polymode->setMode(osg::PolygonMode::FRONT_AND_BACK, osg::PolygonMode::LINE);
	m_geode->getOrCreateStateSet()->setAttributeAndModes(polymode.get(), osg::StateAttribute::OVERRIDE | osg::StateAttribute::ON);
	osg::LineWidth* lineWidth = new osg::LineWidth();
	lineWidth->setWidth(atof(m_configReader->getStringFromStartupConfig("line_width_for_snapping_axis").c_str()));
	m_geode->getOrCreateStateSet()->setAttributeAndModes(lineWidth, osg::StateAttribute::OVERRIDE | osg::StateAttribute::ON);
	m_geode->getOrCreateStateSet()->setMode(GL_LIGHTING, osg::StateAttribute::OFF);
	m_geode->getOrCreateStateSet()->setTextureMode(0, GL_TEXTURE_2D, osg::StateAttribute::OVERRIDE | osg::StateAttribute::OFF);
	m_drawable->setUseDisplayList(false);
	m_drawable->setUseVertexBufferObjects(true);
	m_drawable->setDataVariance(osg::Object::DYNAMIC);
	m_geode->addDrawable(m_drawable);
}

VisualizeBarycentrumControllerTool::~VisualizeBarycentrumControllerTool()
{

}

//void VisualizeBarycentrumControllerTool::update()
//{
//
//}
void VisualizeBarycentrumControllerTool::update()
{
}

//Creating crosses


void VisualizeBarycentrumControllerTool::updateCross()
{
	m_geode->removeDrawables(0, m_geode->getNumDrawables());
	m_geode->addDrawable(m_drawable);
	m_lineVertices->clear();
	m_indicies->clear();
	m_colorArray->clear();

	osg::Vec4 color;

	osg::Vec3f barycentrum = m_geometryManager->getBarycentrumOfSELECTEDVertsForActiveGeomData();

	//Controller Tools are attached to the controller node, so have to computeLocalToWorld for "correcting" position of grid
	osg::Matrix local2WorldMatrixController = osg::computeLocalToWorld(m_matrixTransform->getParentalNodePaths()[0]);
	osg::Matrix local2WorldMatrixGeom = osg::computeLocalToWorld(m_geometryManager->getActiveGeometryData()->getParentalNodePaths()[0]);

	//TODO: Useful anywhere?
	float scale = 1.0;

	m_lineVertices->push_back(((osg::Vec3f(0.0f, 0.0f, m_lineLengthCross) * scale) + barycentrum * local2WorldMatrixGeom) * osg::Matrix::inverse(local2WorldMatrixController));
	m_lineVertices->push_back(((osg::Vec3f(0.0f, 0.0f, -m_lineLengthCross) * scale) + barycentrum * local2WorldMatrixGeom) * osg::Matrix::inverse(local2WorldMatrixController));
	color = osg::Vec4(0.0f, 0.0f, 1.0f, 0.5f);
	m_colorArray->push_back(color);
	m_colorArray->push_back(color);

	m_lineVertices->push_back(((osg::Vec3f(0.0f, m_lineLengthCross, 0.0f) * scale) + barycentrum * local2WorldMatrixGeom) * osg::Matrix::inverse(local2WorldMatrixController));
	m_lineVertices->push_back(((osg::Vec3f(0.0f, -m_lineLengthCross, 0.0f) * scale) + barycentrum * local2WorldMatrixGeom) * osg::Matrix::inverse(local2WorldMatrixController));
	color = osg::Vec4(0.0f, 1.0f, 0.0f, 0.5f);
	m_colorArray->push_back(color);
	m_colorArray->push_back(color);

	m_lineVertices->push_back(((osg::Vec3f(m_lineLengthCross, 0.0f, 0.0f) * scale) + barycentrum * local2WorldMatrixGeom) * osg::Matrix::inverse(local2WorldMatrixController));
	m_lineVertices->push_back(((osg::Vec3f(-m_lineLengthCross, 0.0f, 0.0f)  * scale) + barycentrum * local2WorldMatrixGeom) * osg::Matrix::inverse(local2WorldMatrixController));
	color = osg::Vec4(1.0f, 0.0f, 0.0f, 0.5f);
	m_colorArray->push_back(color);
	m_colorArray->push_back(color);

	m_lineVertices->dirty();
	m_drawable->setVertexArray(m_lineVertices);
	m_colorArray->dirty();

	//Create indicies
	for (unsigned int i = 0; i < m_lineVertices->getNumElements(); ++i)
	{
		m_indicies->push_back(i);
	}
	m_drawable->addPrimitiveSet(m_indicies);

	m_drawable->setColorArray(m_colorArray);
	m_drawable->setColorBinding(osg::Geometry::BIND_PER_VERTEX);
}

void VisualizeBarycentrumControllerTool::updateCross(osg::Vec3d bayrcentrumIn)
{
	m_geode->removeDrawables(0, m_geode->getNumDrawables());
	m_geode->addDrawable(m_drawable);
	m_lineVertices->clear();
	m_indicies->clear();
	m_colorArray->clear();

	osg::Vec4 color;

	//Controller Tools are attached to the controller node, so have to computeLocalToWorld for "correcting" position of grid
	osg::Matrix local2WorldMatrixController = osg::computeLocalToWorld(m_matrixTransform->getParentalNodePaths()[0]);
	osg::Matrix local2WorldMatrixGeom = osg::computeLocalToWorld(m_geometryManager->getActiveGeometryData()->getParentalNodePaths()[0]);

	//TODO: Useful anywhere?
	float scale = 1.0;

	m_lineVertices->push_back(((osg::Vec3f(0.0f, 0.0f, m_lineLengthCross) * scale) + bayrcentrumIn * local2WorldMatrixGeom) * osg::Matrix::inverse(local2WorldMatrixController));
	m_lineVertices->push_back(((osg::Vec3f(0.0f, 0.0f, -m_lineLengthCross) * scale) + bayrcentrumIn * local2WorldMatrixGeom) * osg::Matrix::inverse(local2WorldMatrixController));
	color = osg::Vec4(0.0f, 0.0f, 1.0f, 0.5f);
	m_colorArray->push_back(color);
	m_colorArray->push_back(color);

	m_lineVertices->push_back(((osg::Vec3f(0.0f, m_lineLengthCross, 0.0f) * scale) + bayrcentrumIn * local2WorldMatrixGeom) * osg::Matrix::inverse(local2WorldMatrixController));
	m_lineVertices->push_back(((osg::Vec3f(0.0f, -m_lineLengthCross, 0.0f) * scale) + bayrcentrumIn * local2WorldMatrixGeom) * osg::Matrix::inverse(local2WorldMatrixController));
	color = osg::Vec4(0.0f, 1.0f, 0.0f, 0.5f);
	m_colorArray->push_back(color);
	m_colorArray->push_back(color);

	m_lineVertices->push_back(((osg::Vec3f(m_lineLengthCross, 0.0f, 0.0f) * scale) + bayrcentrumIn * local2WorldMatrixGeom) * osg::Matrix::inverse(local2WorldMatrixController));
	m_lineVertices->push_back(((osg::Vec3f(-m_lineLengthCross, 0.0f, 0.0f)  * scale) + bayrcentrumIn * local2WorldMatrixGeom) * osg::Matrix::inverse(local2WorldMatrixController));
	color = osg::Vec4(1.0f, 0.0f, 0.0f, 0.5f);
	m_colorArray->push_back(color);
	m_colorArray->push_back(color);

	m_lineVertices->dirty();
	m_drawable->setVertexArray(m_lineVertices);
	m_colorArray->dirty();

	//Create indicies
	for (unsigned int i = 0; i < m_lineVertices->getNumElements(); ++i)
	{
		m_indicies->push_back(i);
	}
	m_drawable->addPrimitiveSet(m_indicies);

	m_drawable->setColorArray(m_colorArray);
	m_drawable->setColorBinding(osg::Geometry::BIND_PER_VERTEX);
}

void VisualizeBarycentrumControllerTool::removeLines()
{
	m_drawable->removePrimitiveSet(0, m_drawable->getNumPrimitiveSets());
}