// ClayMore - Immersive Mesh Modelling --- Copyright (c) 2014-2017 Philipp Ladwig, Jannik Fiedler, Jan Beutgen
#pragma once

//#include "imNetworkManagement\PacketSender.h"
#include "..\cmGeometryManagement\GeometryData.h"

class NetworkManager;
class NetworkManagerRakNet;
class NetworkManagerOSC;

class GeometryPacketSender
{
	public:
		GeometryPacketSender(NetworkManagerOSC* networkManagerOSCIn);
		~GeometryPacketSender();

		//inline void setActiveGeometry(GeometryData* meshIn) { m_currentGeometryData = meshIn; }
		//void sendUpdate();
		//void sendGeometry(GeometryData* geometryDataIn);
		//void sendGeometryRakNet(GeometryData* geometryDataIn);
		void sendGeometryOSC(GeometryData* geometryDataIn);

	private:
		NetworkManager* m_networkManager;
		NetworkManagerRakNet* m_networkManagerRakNet;
		NetworkManagerOSC* m_networkManagerOSC;
		
		//GeometryData* m_currentGeometryData;
};