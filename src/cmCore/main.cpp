// ClayMore - Immersive Mesh Modelling --- Copyright (c) 2014-2017 Philipp Ladwig, Jannik Fiedler, Jan Beutgen

//############################################################################################
//Terms used in this source:
//FIXME: means, that something can be written in better code and should changed in future
//TODO[p]: means, that there are more to do but it is not compelling 
//		[p] is optional and classify the priority-> 1 is the hightes; 5 is the lowest
//EXP: stands for Explanation. It explains code
//OBSCURE: stands for obscure behavior of the program and the reason is not known
//REPO: Here is some old code in the repository, which can be helpful for the future but clutters the code
//PERFORMANCE: Check here the performance. How much time does it take and is it possible doing it better?
//############################################################################################

// -- INCLUDES
//Standard
#include <iostream>
//#include <iomanip> // Just for std::setprecision(15) in an std::cout environment

//For printing OSG matrices and vectors
#include <osg/io_utils>

//OpenSceneGraph (OSG)
#include <osgViewer/CompositeViewer>

//ClayMore defines - defines which must be constant during run time and which are not suitable for config.xml
#include "defines.h"

//imUtil
#include "../cmUtil/ConfigReader.h"
#include "../cmUtil/LanguageStringsReader.h"
//Helper class for store  several information while run time (mainly user interaction data...
//...and communicating between GeometryManagement and InteractionManagenment without include the libs from each other (for rumble if a select is selected)
#include "../cmUtil/MiscDataStorage.h"
#include "../cmUtil/ClayMoreVideoPlayer.h"
#include "../cmUtil/DebugAndStatisticsPlugin.h"


//imInteractionManagement
#include "../cmInteractionManagement/InteractionManager.h"
#include "../cmInteractionManagement/ViveController.h"
#include "../cmInteractionManagement/OpenVRConnection.h"
#include "../cmInteractionManagement/KeyboardMouseConnection.h"
#include "../cmInteractionManagement/WASDCameraManipulator.h"

//imGeometryManagement
#include "../cmGeometryManagement/GeometryManager.h"

//imDataManagement
#include "../cmDataManagement/DataManager.h"

//imMenuManager
#include "../cmMenuManagement/MenuManager.h"

//imCore includes
// ### OBSERVERS: ###
//Menu Observers
#include "cmMenuManagement\MenuObservers\PieMenuObserver.h"
#include "cmMenuManagement\MenuObservers\DartMenuObserver.h"
#include "cmMenuManagement\MenuObservers\TiltMenuObserver.h"
#include "cmMenuManagement\MenuObservers\FenceMenuObserver.h"
#include "cmMenuManagement\MenuObservers\TouchMenuObserver.h"
#include "cmMenuManagement\MenuObservers\SelectionSphereTouchMenuObserver.h"

//All other observer
//#include "Observers\TranslateSelectedObjectObserver.h" // currently dead
#include "Observers\TranslateAndRotateSelectedObserver.h"
#include "Observers\SculptBrushAddObserver.h"
#include "Observers\SculptBrushSubtractObserver.h"
#include "Observers\ScaleVerticesObserver.h"
#include "Observers\SelectVerticesObserver.h"
#include "Observers\SelectVerticesNegateObserver.h"
#include "Observers\SphereVertexProspectObserver.h"
#include "Observers\GrabVerticesObserver.h"
#include "Observers\GrabPropEditObserver.h"
#include "Observers\DebugObserver.h"
#include "Observers\RotateVerticesObserver.h"
#include "Observers\ObjectIntersectionObserver.h"
#include "Observers\SpeechSqueezeObserver.h"
#include "Observers\RulerObserver.h"
#include "Observers\ScaleSelectedObjectObserver.h"
#include "Observers\AddMeshElementObserver.h"
#include "Observers\AddEdgeLineObserver.h"
#include "Observers\AddVertsObserver.h"
#include "Observers\ConnectMeshElementsObserver.h"
#include "Observers\SpectatorCamObserver.h"
#include "Observers\ScaleSelectedObjectSqueezeObserver.h"
#include "Observers\ScanPointCloudObserver.h"
#include "Observers\DDATraverseObserver.h"

//ControllerTools
#include "ControllerTools\SphereControllerTool.h"
#include "ControllerTools\PickRayControllerTool.h"
#include "ControllerTools\VisualizeAxisAndGridControllerTool.h"
#include "ControllerTools\VisualizeBarycentrumControllerTool.h"
#include "ControllerTools\RulerControllerTool.h"
#include "ControllerTools\DashedLineControllerTool.h"
#include "ControllerTools\BoundingBoxControllerTool.h"
#include "ControllerTools\ThreeDIconControllerTool.h"

#include "Grid.h"
#include "SkyBox.h"
#include "Floor.h"

//imNetworkManagementOSC
#include "cmNetworkManagementOSC/NetworkManagerOSC.h"
NetworkManagerOSC* g_networkManagerOSC = nullptr;
//General Network
#include "GeometryPacketSender.h"
bool g_isMagicLeapSpectatorActive = false;

//For Undo/Redo
#include "HistoryManager.h"	

//imPointCloudManagement (RealSense, PointCloud Library)
#include "cmPointCloudManagement\PointCloudManager.h"

//AutoRigger includes
#include "cmAutoRigger/model.h"

#include "cmUtil/PoseEstimationXMLReader.h"
/*#include <opencv2/highgui/highgui_c.h>
#include <opencv2/opencv.hpp> */  // Include OpenCV API

//CAMERA CALIB
//#include <opencv2/calib3d/calib3d.hpp>
//#include <opencv2/highgui/highgui.hpp>
//#include <opencv2/imgproc/imgproc.hpp>

#ifdef TORCH_SUPPORTED_BY_CMAKE
#include <cmTensorManagement/TensorManager.h>
TensorManager* g_tensorManager = nullptr;
bool g_isAnimationIteratorIEEEAIVRforCameraActive = true;
float g_animationIteratorIEEEAIVRforCamera = 0.0;
#endif

//## GANCapture START
int g_GANCaptureCounter = 0;
bool g_toogleGANCaptureRecord = false;
double g_twSumForGANCapture = 0;

//## START TOBI EYETRACKING TEST ## 
//#include "tobii_research_eyetracker.h"
//#include "tobii_research_streams.h"
//
//void gaze_data_callback(TobiiResearchGazeData* gaze_data, void* user_data) {
//	memcpy(user_data, gaze_data, sizeof(*gaze_data));
//}

#include <tobii.h>
#include <tobii_streams.h>
#include <assert.h>
//#include <string.h>
//#include <fstream>

std::string g_dirSavePath;
std::ofstream outputFileGazeData("data\\GAN\\GazeData.txt", std::ios_base::app | std::ios_base::in);
tobii_gaze_point_t const* g_GazePoint;
tobii_device_t* tobiiDevice = NULL;
tobii_api_t* g_tobiiAPI = NULL;

void writeGazeDataToFile(std::string dirSavePath)
{
	_sleep(90);
	tobii_error_t result = tobii_api_create(&g_tobiiAPI, NULL, NULL);
	result = tobii_wait_for_callbacks(1, &tobiiDevice);
	assert(result == TOBII_ERROR_NO_ERROR || result == TOBII_ERROR_TIMED_OUT);

	result = tobii_device_process_callbacks(tobiiDevice);
	result = tobii_device_process_callbacks(tobiiDevice);
	result = tobii_device_process_callbacks(tobiiDevice);
	assert(result == TOBII_ERROR_NO_ERROR);

	outputFileGazeData.precision(4);
	if (g_GazePoint)
	{
		if (g_GazePoint->validity == TOBII_VALIDITY_VALID)
		{
			//Alternative would be the g_dirSavePath
			outputFileGazeData << "Gaze point:" << g_GazePoint->position_xy[0] << "  " << g_GazePoint->position_xy[1] << " at " << dirSavePath << std::endl;
			std::cout << "Gaze point:" << g_GazePoint->position_xy[0] << "," << g_GazePoint->position_xy[1] << "," << g_dirSavePath << std::endl;

		} else
		{
			outputFileGazeData << "Gaze point:" << "0.0" << "  " << "0.0" << " at " << dirSavePath << std::endl;
			std::cout << "Gaze point ERROR!!!!!!!!!!!!!!!!!!! " << std::endl;
		}
	} else
	{
		outputFileGazeData << "Gaze point:" << "0.0" << "  " << "0.0" << " at " << dirSavePath << std::endl;
		std::cout << "Gaze point ERROR!!!!!!!!!!!!!!!!!!!" << std::endl;
	}
}

void gaze_point_callback(tobii_gaze_point_t const* gaze_point, void* /* user_data */)
{

	// Check that the data is valid before using it
	if (gaze_point->validity == TOBII_VALIDITY_VALID) {
		g_GazePoint = gaze_point;
		//std::cout << "Gaze point:" << gaze_point->position_xy[0] << "," << gaze_point->position_xy[1] << "," << g_dirSavePath << std::endl;
	}
}

void url_receiver(char const* url, void* user_data)
{
	char* buffer = (char*)user_data;
	if (*buffer != '\0') return; // only keep first value

	if (strlen(url) < 256)
		strcpy(buffer, url);
}


//## END TOBI EYETRACKING TEST ## 




#include <opencv2/opencv.hpp> 
#include <cmUtil\TimeWriter.h>

cv::VideoCapture g_openCV_VideoCapture;
void captureOpenCVFrame(std::string dirSavePath) {
	TimeWriter tw;
	tw.startMetering("g_openCV_VideoCapture", true, false);
	//Save OpenCV Cam
	_sleep(83);
	std::cout << "isOpened: " << g_openCV_VideoCapture.isOpened() << std::endl;
	cv::Mat frame;
	g_openCV_VideoCapture >> frame;
	//cv::Mat frame;
	//cap.read(frame);
	std::string savePath;
	savePath.append(dirSavePath);
	savePath.append("OpenCV.jpg");
	cv::imwrite(savePath, frame);
	//cv::imshow("Display window", frame);
	tw.endMetering();
}


//Vive Eye and Lip Tracking
#include "SRanipal.h"
#include "SRanipal_Eye.h"
#include "SRanipal_Lip.h"
#include "SRanipal_Enums.h"


// -- GLOBALS
osg::PositionAttitudeTransform* g_MenuTransformMatrix = nullptr;


//SpeechCallbackHandler* g_speechCallbackHandler = nullptr;
GeometryPacketSender* g_geometryPacketSender = nullptr;

ConfigReader* g_configReader = nullptr;
LanguageStringsReader* g_languageStringsReader = nullptr;

osgViewer::Viewer* g_mainViewer = nullptr;
osg::ref_ptr<osg::Group> g_sceneRoot = nullptr;

MenuManager* g_menuManager = nullptr;
GeometryManager* g_geometryManager = nullptr;
DataManager* g_dataManager = nullptr;

//INTERACTION MANAGEMENT
InteractionManager* g_interactionManager = nullptr;
bool g_useKeyboardMouse = false;
bool g_useVive = false;
bool g_useLeapMotion = false;
bool g_useColorAndDepthShooter = false;
bool g_useAzureTelepresence;
double g_lastKeyPressedTimeStamp;

//GAN-Stuff
bool g_useSeventyFacialLandMarksOverOSCListener = false;
bool g_useSRanipalEyeTracking = false;


//MENU MANAGEMENT
bool g_isEasyModeForTiltMenuActivated = true;
MenuName g_secondaryLastMenu;
MenuName g_primaryLastMenu;
MenuName g_primaryMenuBeforLastMenu = MenuName::TOUCH_EDIMODE_SELECTION;
bool isActivatedChangeToOtherTool;
osg::Timer_t g_tickForDeltaTime;
double g_deltaTime;

//Class mainly for communication between Observers and Controller Tools - TODO: Maybe move all members to geometryManager?
MiscDataStorage* g_miscDataStorage = nullptr;

HistoryManager* g_historyManager = nullptr;

ClayMoreVideoPlayer* g_clayMoreVideoPlayer = nullptr;

//SPECTATOR WINDOW IS ACTIVE
//for better performance in main loop, instead of always reading configReader
bool g_isSpectatorWindowActive = false;
//must be read in CFLs
osg::ref_ptr<osgViewer::Viewer> g_spectatorViewer = nullptr;

//POINT CLOUD MANAGEMENT (RealSense, PCL)
PointCloudManager* g_pointCloudManager = nullptr;
bool g_useRealSenseDevice = false;
// ### START - Old Scan Pipeline ###
// bool g_useMultipleRealSenseDevices = false;
// ### END - Old Scan Pipeline ###
bool g_useAzureKinectDevice = false;
bool g_useARCoreTracking = false;


//PLUGINS
DebugAndStatisticsPlugin* g_debugAndStatisticsPlugin = nullptr;

//All -CallbackFunctionLibrary (CFL)- requires the global variables above
#include "CallbackFunctionLibraries\ButtonCFL.h"
#include "CallbackFunctionLibraries\SpeechCFL.h"
#include "CallbackFunctionLibraries\VertSelectMenuCFL.h"
#include "CallbackFunctionLibraries\ObjectSelectMenuCFL.h"
#include "CallbackFunctionLibraries\ModeMenuCFL.h"
#include "CallbackFunctionLibraries\ContextTransRotMenuCFL.h"


#include "initialisations.h"
#include "menuInitialisations.h"

// ########################################################################################################   
// ########################################################################################################   
// ###############################################   MAIN   ###############################################   
// ########################################################################################################   
// ########################################################################################################   

int main(int argc, char** argv)
{
	osg::ArgumentParser arguments(&argc, argv);

	if (ConfigReader::DEBUG_LEVEL >= NO_OUTPUT)
	{
		std::cout << std::endl << " ------------------------------------------------------------- " << std::endl;
		std::cout << std::endl << " Welcome to ClayMoreVR. Starting up with no debugging output." << std::endl;
		std::cout << std::endl << " ------------------------------------------------------------- " << std::endl << std::endl;
	} else if (ConfigReader::DEBUG_LEVEL >= NOT_IN_LOOPS)
	{
		std::cout << std::endl << " ---------------------------------------------------------------------------------------------- " << std::endl;
		std::cout << std::endl << " Welcome to ClayMoreVR. Starting up with debugging output <<Verbose, except loops>> enabled..." << std::endl;
		std::cout << std::endl << " ---------------------------------------------------------------------------------------------- " << std::endl << std::endl;
	} else if (ConfigReader::DEBUG_LEVEL >= VERBOSE)
	{
		std::cout << std::endl << " ------------------------------------------------------------------------------- " << std::endl;
		std::cout << std::endl << " Welcome to ClayMoreVR. Starting up with debugging output <<Verbose>> enabled..." << std::endl;
		std::cout << std::endl << " ------------------------------------------------------------------------------- " << std::endl << std::endl;
	}

	if (!parseConfigAndStringsXML())
	{
		std::cout << "Error in parseConfigAndStringsXML()" << std::endl;
		return 0; //Close ClayMore
	}

	if (!createFastAccessParametersFromConfig())
	{
		std::cout << "Error in createFastAccessParametersFromConfig()" << std::endl;
		return 0; //Close ClayMore
	}

	if (!createAndSetUpGeometryManagement())
	{
		std::cout << "Error in createAndSetUpGeometryManagement()" << std::endl;
		return 0; //Close ClayMore
	}

	if (!createAndSetUpDataManagement())
	{
		std::cout << "Error in createAndSetUpDataManagement()" << std::endl;
		return 0; //Close ClayMore
	}

	if (!createAndSetUpScene())
	{
		std::cout << "Error in createAndSetUpScene()" << std::endl;
		return 0; //Close ClayMore
	}

	if (!createAndSetUpInteractionManager(arguments))
	{
		std::cout << "Error in createAndSetUpInteractionManager()" << std::endl;
		return 0; //Close ClayMore
	}

	if (!createAndSetUpMenuManagement())
	{
		std::cout << " # ERROR in main() - Could not initialize MenuManager!" << std::endl;
		system("pause");
		return 0; //Close ClayMore
	}

	if (!createAndSetUpViewersAndWindows())
	{
		std::cout << "Error in createAndSetUpViewersAndWindows()" << std::endl;
		return 0; //Close ClayMore
	}

	if (!createAndSetUpNetworkOSC())
	{
		std::cout << "Error in createAndSetUpNetworkOSC()" << std::endl;
		return 0; //Close ClayMore
	}

	if (!setUpHistoryManager())
	{
		std::cout << "Error in createAndSetUpHistoryManager()" << std::endl;
		return 0; //Close ClayMore
	}

	if (!createAndSetUpMagicLeapCommunication())
	{
		std::cout << "Error in createAndSetUpHoloLensCommunication()" << std::endl;
		return 0; //Close ClayMore
	}

	if (!createAndSetUpPointCloudManagement())
	{
		std::cout << "Error in createAndSetUpPointCloudManagement()" << std::endl;
		return 0; //Close ClayMore
	}

	if (!createAndSetUpTensorManagement())
	{
		std::cout << "Error in createAndSetUpTensorManagement()" << std::endl;
		return 0; //Close ClayMore
	}

	//######################################################################################################
	//Load PlugIns
	//TODO: Create a PlugIn Manager
	if (!createAndSetUpDebugAndStatisticsPlugin())
	{
		std::cout << "Error in createAndSetUpDebugAndStatisticsPlugin()" << std::endl;
		return 0; //Close ClayMore
	}

	//######################################################################################################
	//EXPERIMENTAL RAW PLUGIN FOR ARCORE TRACKING
	////////////DEBUG STUFF ARCORE
	osg::ref_ptr<osg::MatrixTransform> g_ARCoreModellMatrixTransform = new osg::MatrixTransform();
	if (g_useARCoreTracking)
	{
		osg::ref_ptr<osg::Node> g_ARCoreModell = osgDB::readNodeFile("./data/controller_models/android.obj");
		g_ARCoreModell->getOrCreateStateSet();
		g_ARCoreModellMatrixTransform->addChild(g_ARCoreModell);
		g_ARCoreModell->setName("ArCoreAndroidModel");
		g_sceneRoot->addChild(g_ARCoreModellMatrixTransform);
	}
	////////////DEBUG STUFF ARCORE END

	//TODO: Actually some config.xml stuff here:
	short refreshTimeRegulatorForFPSTextBehindController = 0;
	short refreshCountFramesForFPSTextBehindController = 20; // every x frame update

	short refreshTimeRegulatorForHoloLensSpectator = 0;
	short refreshCountFramesForHoloLensSpectator = 15; // every x frame update

	short refreshTimeRegulatorForMagicLeapSpectator = 0;
	short refreshCountFramesForMagicLeapSpectator = 20; // every x frame update

	short refreshTimeRegulatorForSpectatorView = 0; //Spectator view is a separate window on the local machine
	short refreshCountFramesForSpectatorView = 3; // every x frame update

	short refreshTimeRegulatorForBodyScanning = 0;
	short refreshCountFramesForBodyScanning = 60;
	bool bodyScanningTriggerHKey = false;

	short refreshTimeRegulatorForAutomaticDebugScanLoop = 1; //Spectator view is a separate window on the local machine
	short refreshCountFramesForAutomaticDebugScanLoop = 100; // every x frame update


	g_deltaTime = 1.0;
	g_tickForDeltaTime = osg::Timer::instance()->tick();


	if (ConfigReader::DEBUG_LEVEL >= NOT_IN_LOOPS)
		std::cout << std::endl << "All done! Starting main loop." << std::endl;

	//Sets the circularBuffer for synchonisation of depth camera and tracker
	if (g_useVive && g_useRealSenseDevice)
	{
		g_pointCloudManager->setTimedCircularTrackerMatrixBuffer(
			g_interactionManager->getTimedCircularTrackerMatrixBufferFromOpenVR(),
			g_interactionManager->getTimedCircularTrackerMatrixBuffer_Lock_FromOpenVR());
	}

	bool g_activateDshowCapture = g_configReader->getBoolFromStartupConfig("activate_dshow_capture_with_claymore_video_player");
	if (g_activateDshowCapture)
	{
		g_clayMoreVideoPlayer = new ClayMoreVideoPlayer("g_clayMoreVideoPlayer");
		g_clayMoreVideoPlayer->init("dshow://"); //TODO: Still hardcoded...
		g_clayMoreVideoPlayer->setPosition(g_configReader->getVec3fFromStartupConfig("video_plane_position"));
		g_clayMoreVideoPlayer->setRotationAroundYAxis(g_configReader->getFloatFromStartupConfig("video_plane_rotation_Y_in_degree"));
		g_clayMoreVideoPlayer->attachToSceneGraph(g_sceneRoot);
		g_clayMoreVideoPlayer->play();
	}

	// ## START TOBI EYETRACKING ###
	tobii_error_t result;
	if (g_configReader->getBoolFromStartupConfig("use_tobii_4c_tracker"))
	{
		result = tobii_api_create(&g_tobiiAPI, NULL, NULL);
		assert(result == TOBII_ERROR_NO_ERROR);

		// Enumerate devices to find connected eye trackers, keep the first
		char url[256] = { 0 };
		result = tobii_enumerate_local_device_urls(g_tobiiAPI, url_receiver, url);
		assert(result == TOBII_ERROR_NO_ERROR);
		if (*url == '\0')
		{
			std::cout << "Tobii Eyetracking: No device found" << std::endl;
		}

		// Connect to the first tracker found
		//result = tobii_device_create(g_tobiiAPI, url, TOBII_FIELD_OF_USE_INTERACTIVE, &tobiiDevice);
		assert(result == TOBII_ERROR_NO_ERROR);

		// Subscribe to gaze data
		result = tobii_gaze_point_subscribe(tobiiDevice, gaze_point_callback, 0);
		assert(result == TOBII_ERROR_NO_ERROR);

		//// This sample will collect 1000 gaze points
		//for (int i = 0; i < 1000; i++)
		//{
		//	// Optionally block this thread until data is available. Especially useful if running in a separate thread.
		//	result = tobii_wait_for_callbacks(1, &tobiiDevice);
		//	assert(result == TOBII_ERROR_NO_ERROR || result == TOBII_ERROR_TIMED_OUT);

		//	// Process callbacks on this thread if data is available
		//	result = tobii_device_process_callbacks(tobiiDevice);
		//	assert(result == TOBII_ERROR_NO_ERROR);
		//}

		// ## END TOBI EYETRACKING TEST ## 
	}

	// ## START ViveSR SRanipal SDK Eye Tracking
	// NOTE: When starting Sranipal for the first time on a new system (with first time used Eye (and Lip Tracking)), the prompts
	// in the HMD must be confirmed and eyetracking must be calibrated before running the following code. Otherwiese,
	// execution stops here without any error...
	ViveSR::anipal::Eye::EyeData_v2 eye_data;
	if (g_useSRanipalEyeTracking)
	{
		std::cout << "Init SRanipal SDK..." << std::endl;
		int error, handle = NULL;
		error = ViveSR::anipal::Initial(ViveSR::anipal::Eye::ANIPAL_TYPE_EYE_V2, NULL);
		if (error == ViveSR::Error::WORK)
		{
			std::cout << "SRanipal: Successfully initialize Eye engine." << std::endl;
		} else if (error == ViveSR::Error::RUNTIME_NOT_FOUND) {
			std::cout << "SRanipal Error: Please follows SRanipal SDK guide to install SR_Runtime first" << std::endl;
		} else {
			std::cout << "SRanipal Error: Fail to initialize Eye engine. please refer the code %d of ViveSR::Error." << error << std::endl;
		}
	}
	// ## END ViveSR SRanipal SDK Eye Tracking 




	//std::cout << std::endl << "AutoRigger Start" << std::endl;
	//Model* model = new Model();
	//model->modelProcessor();
	//std::cout << std::endl << "AutoRigger End" << std::endl;





	//cv::VideoCapture cap(0, cv::CAP_FFMPEG);
	//cap.set(cv::CAP_FFMPEG, 1);
	//cap.set(cv::CAP_PROP_FPS, 30);
	//cap.set(cv::CAP_PROP_FRAME_WIDTH, 720);
	//cap.set(cv::CAP_PROP_FRAME_HEIGHT, 576);
	//cap.set(cv::CAP_PROP_MODE, 2);
	//std::cout << cv::getBuildInformation() << std::endl;

	//cap.set(cv::CAP_PROP_CONVERT_RGB, 0);

	//cap.set(cv::CAP_PROP_FPS, 30);
	g_openCV_VideoCapture.set(cv::CAP_PROP_FRAME_WIDTH, 1280);
	g_openCV_VideoCapture.set(cv::CAP_PROP_FRAME_HEIGHT, 720);
	//cap.set(cv::CAP_PROP_MODE, 1);
	//cap.set(cv::CAP_PROP_SETTINGS, 1);
	//int apiBackend = cv::CAP_DSHOW;
	//if (!cap.open(0+ apiBackend));
	if (!g_openCV_VideoCapture.open(2))
	{
		std::cout << "Klappt net" << std::endl;
	}

	//######################################################################################################
	//######################################################################################################
	//###########################################   Main loop   ############################################
	//######################################################################################################
	//######################################################################################################
	while (!g_mainViewer->done() && !g_spectatorViewer->done())
	{
		// ## START ViveSR SRanipal SDK Eye Tracking 
		if (g_useSRanipalEyeTracking)
		{
			int result = ViveSR::anipal::Eye::GetEyeData_v2(&eye_data);
			if (result == ViveSR::Error::WORK) {
				float* gazeLeft = eye_data.verbose_data.left.gaze_direction_normalized.elem_;
				//printf("[Eye Left] Gaze: %.2f %.2f %.2f\n", gazeLeft[0], gazeLeft[1], gazeLeft[2]);
				float* gazeRight = eye_data.verbose_data.right.gaze_direction_normalized.elem_;
				//printf("[Eye Right] Gaze: %.2f %.2f %.2f\n", gazeRight[0], gazeRight[1], gazeRight[2]);

				float openessLeft = eye_data.expression_data.left.eye_wide;
				//printf("[Eye Left] Openess: %.2f\n", openessLeft);
				float openessRight = eye_data.expression_data.right.eye_wide;
				//printf("[Eye Right] Openess: %.2f\n", openessRight);

				//float frownLeft = eye_data.expression_data.left.eye_frown;
				////printf("[Eye Left] Frown: %.2f\n", frownLeft);
				//float frownRight = eye_data.expression_data.right.eye_frown;
				////printf("[Eye Right] Frown: %.2f\n", frownRight);

				//float squeezeLeft = eye_data.expression_data.left.eye_squeeze;
				////printf("[Eye Left] Squeeze: %.2f\n", squeezeLeft);
				//float squeezeRight = eye_data.expression_data.right.eye_squeeze;
				////printf("[Eye Right] Squeeze: %.2f\n", squeezeRight);

				g_networkManagerOSC->sendEyeTrackingData(
					gazeLeft[0], //0
					gazeLeft[1],
					openessLeft,
					gazeRight[0],
					gazeRight[1],
					openessRight);
			}
		}
		// ## END ViveSR SRanipal SDK Eye Tracking 

		//TODO5: InteractionManager update in own thread?
		g_interactionManager->update();
		g_menuManager->updateMenuAnimations(g_deltaTime);

		//Using this way for updating because of shadows
		g_interactionManager->getPrimaryController()->updateMatrixTransform();
		g_interactionManager->getSecondaryController()->updateMatrixTransform();
		g_interactionManager->getGenericTracker()->updateMatrixTransform();

		//TODO1: if g_spectatorViewer is enabled and selecting some points and add a simple cube, it crashes in frame... only in Release...debug is fine....
		//deltaTimeRealSense = osg::Timer::instance()->delta_s(g_tickDeltaTime, osg::Timer::instance()->tick());
		g_deltaTime = osg::Timer::instance()->delta_m(g_tickForDeltaTime, osg::Timer::instance()->tick()); //deltaTime in millisec
		g_deltaTime = g_deltaTime > 1.0 ? g_deltaTime < 20.0 ? g_deltaTime : 20.0 : 1.0;
		g_tickForDeltaTime = osg::Timer::instance()->tick();

		//START: Text behind secondary controller ####
		if (refreshTimeRegulatorForFPSTextBehindController % refreshCountFramesForFPSTextBehindController == 0) // Update every refreshCountFramesForFPSTextBehindController frames
		{
			g_debugAndStatisticsPlugin->setFrameCounterText(std::to_string((int)(1000.0 / g_deltaTime)) + " fps");
			std::cout << (1000.0 / g_deltaTime) << " fps - DeltaTime main loop is: " << g_deltaTime << std::endl;
			//speechCommandText->setText("Last speech: " + g_speechCallbackHandler->getLastCommand());
			//speechCommandText->setText("Last speech: Auskommentiert!");
			refreshTimeRegulatorForFPSTextBehindController = 0;
		}
		refreshTimeRegulatorForFPSTextBehindController++;
		//END: Text behind secondary controller ####

		g_mainViewer->frame(); //Render Main Viewer frame
		//######################################################################################################
		//START: Point Cloud Stuff ####
		//TODO3: Ugly hacking -> Interaction is managed here without CallbackFuntionLibraries and InteractionManagement, 
		//but it is only active when realSense && Vive is activated in config
		if (g_isSpectatorWindowActive)
		{
			//######################################################################################################
			//Spectator viewer loop part start
			//Enters this function every frame depending on refreshCountFramesForSpectatorView
			//	This is necessary, since the Spectator viewer can only run at 60fps and delays the entire application until his next "allowed" render time
			//	which drops the framerate of the 90fps HMD Renderer. Render the spectator every 2 frame is ok (Variable: refreshCountFramesForSpectatorView)
			if (refreshTimeRegulatorForSpectatorView % refreshCountFramesForSpectatorView == 0)  // Update every refreshCountFramesForSpectatorView frames
			{
				//Passes the information to the pointCloudManager that a frame is now rendered and the scene graph should be not changed unitil the frames is rendered
				g_pointCloudManager->lockSceneGraph();
				//std::this_thread::sleep_for(std::chrono::milliseconds(5));				//TensorManagement
#ifdef TORCH_SUPPORTED_BY_CMAKE
				//std::cout << "### ############### FacialLandmarks auslesen aus main " << std::endl;
				g_tensorManager->updateSeventyFacialLandmarks(g_networkManagerOSC->getSeventyFacialLandmarks());


				//osg::Matrixd viewMatrix;
				//std::vector<osg::Camera*> cameraList;
				//g_mainViewer->getCameras(cameraList);
				//if (cameraList.at(0))
				//{
				//	//In viewMatrix.getTrans() and viewMatrix.getRotate() is stored the position of the HMD
				//	viewMatrix = cameraList.at(0)->getViewMatrix();
				//}
				//g_tensorManager->updateHMDPose(viewMatrix);


				//g_interactionManager->get
				//std::cout << "Pos: ---" << g_interactionManager->getOpenVRViewer()->getHMDDevice()->getPosition() << std::endl;
				//std::cout << "Rot: " << g_interactionManager->getOpenVRViewer()->getHMDDevice()->getRotation() << std::endl;
				//std::cout << "### ############### FacialLandmarks ende aus main" << std::endl;
				BUTTON_STATE keyboardButtonStateKeyJ = g_interactionManager->getKeyboardMouseConnection()->getButtonState(osgGA::GUIEventAdapter::KEY_J); // Save generated images
				if (keyboardButtonStateKeyJ ==
					PUSH && (std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::system_clock::now().time_since_epoch()).count()
						- g_lastKeyPressedTimeStamp) > 300.0)
				{
					std::stringstream dirSavePath;
					time_t t = time(0);   // get time now
					struct tm* now = localtime(&t);
					dirSavePath <<
						"data\\GAN\\IEEE-AIVR-EvalIamge"
						<< now->tm_year + 1900 << "-"
						<< now->tm_mon + 1 << "-"
						<< now->tm_mday << "---"
						<< now->tm_hour << "-"
						<< now->tm_min << "-"
						<< now->tm_sec << "-"
						<< ".png";
					g_tensorManager->saveGeneratedRGBDImageToDisk(dirSavePath.str());
					g_lastKeyPressedTimeStamp = std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::system_clock::now().time_since_epoch()).count();
				}
				if (g_isAnimationIteratorIEEEAIVRforCameraActive)
				{
					g_animationIteratorIEEEAIVRforCamera += 0.049;
					dynamic_cast<WASDCameraManipulator*>(g_spectatorViewer->getCameraManipulator())->setVerticalAxisFixed(false);
					dynamic_cast<WASDCameraManipulator*>(g_spectatorViewer->getCameraManipulator())->setTransformation(
						osg::Vec3d(cos(g_animationIteratorIEEEAIVRforCamera) * 0.38 - 0.30, 1.40, -0.95), osg::Vec3d(-0.30, 1.3, 2.0), osg::Vec3d(0.0, 1.0, 0.0));
				}
				//-1.0+cos(g_animationIteratorIEEEAIVRforCamera)*0.15 + 0.65


#endif
				g_spectatorViewer->frame(); //Render SpectatorViewer frame
				//std::this_thread::sleep_for(std::chrono::milliseconds(5));
				g_pointCloudManager->unlockSceneGraph();

				//Function for Alex Pech's GAN thesis
				if (g_useColorAndDepthShooter)
				{
					BUTTON_STATE keyboardButtonStateKeyK = g_interactionManager->getKeyboardMouseConnection()->getButtonState(osgGA::GUIEventAdapter::KEY_K); //Start Kinect GANCapture
					BUTTON_STATE keyboardButtonStateKeyC = g_interactionManager->getKeyboardMouseConnection()->getButtonState(osgGA::GUIEventAdapter::KEY_C); // Cancel Capture
					if (keyboardButtonStateKeyC ==
						PUSH && (std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::system_clock::now().time_since_epoch()).count()
							- g_lastKeyPressedTimeStamp) > 300.0)
					{
						g_toogleGANCaptureRecord = false;
						std::cout << "### ### Cancel GANCapture ### ###" << std::endl;
						g_lastKeyPressedTimeStamp = std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::system_clock::now().time_since_epoch()).count();
					}

					if (keyboardButtonStateKeyK ==
						PUSH && (std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::system_clock::now().time_since_epoch()).count()
							- g_lastKeyPressedTimeStamp) > 3.0)
					{
						g_toogleGANCaptureRecord = true;
						std::cout << "### ### Start GANCapture ### ###" << std::endl;
						g_lastKeyPressedTimeStamp = std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::system_clock::now().time_since_epoch()).count();
					}




					if (g_toogleGANCaptureRecord)
					{
						TimeWriter tw;
						tw.startMetering("Saving images for GAN", true, false);

						g_GANCaptureCounter++;
						std::cout << "Start saving" << std::endl;
						//Write captures as .png to disk
						time_t t = time(0);   // get time now
						struct tm* now = localtime(&t);
						std::stringstream dirSavePath;
						dirSavePath <<
							"data\\GAN\\test\\color_and_depth_shoot_nr_"
							<< std::to_string(g_GANCaptureCounter) << "-"
							<< "from_"
							<< now->tm_year + 1900 << "-"
							<< now->tm_mon + 1 << "-"
							<< now->tm_mday << "---"
							<< now->tm_hour << "-"
							<< now->tm_min << "-"
							<< now->tm_sec;

						g_pointCloudManager->makeAndSaveAFewDepthImagesWithKinect(dirSavePath.str());

						bool activateMultiThread = true;
						//if (activateMultiThread)
						//{
						//	std::thread* threadSaveKinect;
						//	std::thread* threadSaveTobii;
						//	//std::thread* threadSaveDShow;
						//	//std::thread* threadSaveOpenCV;

						//	//Save Kinect RGB and D images
						//	//g_pointCloudManager->makeAndSaveAFewDepthImagesWithKinect(dirSavePath.str());
						//	threadSaveKinect = new std::thread(&PointCloudManager::makeAndSaveAFewDepthImagesWithKinect, g_pointCloudManager, dirSavePath.str());

						//	//Save Tobii Gaze Data
						//	//writeGazeDataToFile(dirSavePath.str());
						//	//threadSaveTobii = new std::thread(&writeGazeDataToFile, dirSavePath.str());

						//	//Save DShow (AV2GO-Cam) image
						//	//g_clayMoreVideoPlayer->saveCurrentImage(dirSavePath.str());
						//	//if (g_activateDshowCapture)
						//	//{
						//	//	threadSaveDShow = new std::thread(&ClayMoreVideoPlayer::saveCurrentImage, g_clayMoreVideoPlayer, dirSavePath.str());
						//	//}

						//	//Save OpenCV-Image
						//	//captureOpenCVFrame(g_dirSavePath);
						//	//threadSaveOpenCV = new std::thread(&captureOpenCVFrame, dirSavePath.str());

						//	threadSaveKinect->join();
						//	threadSaveTobii->join();
						//	//if (g_activateDshowCapture)
						//	//{
						//	//	threadSaveDShow->join();
						//	//}
						//	//threadSaveOpenCV->join();
						//}
						//else
						//{
						//	//Save Kinect RGB and D images
						//	g_pointCloudManager->makeAndSaveAFewDepthImagesWithKinect(dirSavePath.str());

						//	//Save Tobii Gaze Data
						//	//result = tobii_device_process_callbacks(tobiiDevice);
						//	writeGazeDataToFile(dirSavePath.str());


						//	//Save DShow (AV2GO-Cam) image
						//	if (g_activateDshowCapture)
						//	{
						//		g_clayMoreVideoPlayer->saveCurrentImage(dirSavePath.str());
						//	}

						//	//Save OpenCV-Image
						//	captureOpenCVFrame(g_dirSavePath);
						//}

						g_twSumForGANCapture += tw.endMetering();
						std::cout << "GANCapture Counter: " << g_GANCaptureCounter << std::endl;
						std::cout << "GANCapture AverageTime: " << (double)g_twSumForGANCapture / g_GANCaptureCounter << std::endl;

					}
					//Paint the RGB data onto a quad
					g_pointCloudManager->paintKinectImageOnQuad();
				}




				BUTTON_STATE keyboardButtonStateKeyR = g_interactionManager->getKeyboardMouseConnection()->getButtonState(osgGA::GUIEventAdapter::KEY_R);
				if (keyboardButtonStateKeyR == PUSH && (std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::system_clock::now().time_since_epoch()).count() - g_lastKeyPressedTimeStamp) > 500.0)
				{

					std::cout << "smooth default mesh" << std::endl;
					//TODO: smooth default mesh

					typedef OpenMesh::OSG_TriMesh_BindableArrayKernelT MyMesh;
					typedef OpenMesh::Smoother::JacobiLaplaceSmootherT<MyMesh> Smoother;


					//OpenMesh::OSG_TriMesh_BindableArrayKernelT* inputMesh = g_geometryManager->getActiveGeometryData()->getOpenMeshGeo()->getMesh();

					Smoother smoother(*g_geometryManager->getActiveGeometryData()->getOpenMeshGeo()->getMesh());
					smoother.initialize(OpenMesh::Smoother::SmootherT<MyMesh>::Component::Tangential_and_Normal, OpenMesh::Smoother::SmootherT<MyMesh>::Continuity::C0);
					smoother.smooth(3);

				}




				//Load Point Cloud from HDD
				BUTTON_STATE keyboardButtonStateKeyL = g_interactionManager->getKeyboardMouseConnection()->getButtonState(osgGA::GUIEventAdapter::KEY_L);
				if (keyboardButtonStateKeyL == PUSH && (std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::system_clock::now().time_since_epoch()).count() - g_lastKeyPressedTimeStamp) > 500.0)
				{
					//g_geometryManager->deselectAllObjects();
					g_geometryManager->toggleSelectOrDeselectAll();
					//g_geometryManager->toggleSelectOrDeselectAll();
					g_geometryManager->deleteSelectedMeshElements(false, false, true);
					//g_geometryManager->garbageCollection();
					std::cout << "Load PointCloud now" << std::endl;
					g_pointCloudManager->loadPointCloud(g_geometryManager->getActiveGeometryData()->getOpenMeshGeo(), "vinci.pcd");
					g_geometryManager->getActiveGeometryData()->getOpenMeshGeo()->getVertexArray()->dirty();
					g_geometryManager->updateFilthyGeometries();
					g_geometryManager->getActiveGeometryData()->updateBoundingBox();
					g_geometryManager->getActiveGeometryData()->getOpenMeshGeo()->translateSelOMStatusBitToColor();
					g_geometryManager->getActiveGeometryData()->getOpenMeshGeo()->getColorArray()->dirty();
					std::cout << "Load PointCloud Ende" << std::endl;
					std::cout << "Vertex Array Size: " << g_geometryManager->getActiveGeometryData()->getOpenMeshGeo()->getVertexArray()->getElementSize() << std::endl;
					//std::cout << "Vertex Array Size: " << g_geometryManager->getActiveGeometryData()->getOpenMeshGeo()->getVertexArray()->size() << std::endl;
					g_lastKeyPressedTimeStamp = std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::system_clock::now().time_since_epoch()).count();
				}
				//Save current Point Cloud to a .pcd file
				BUTTON_STATE keyboardButtonStateKeyP = g_interactionManager->getKeyboardMouseConnection()->getButtonState(osgGA::GUIEventAdapter::KEY_P);
				if (keyboardButtonStateKeyP == PUSH && (std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::system_clock::now().time_since_epoch()).count() - g_lastKeyPressedTimeStamp) > 500.0)
				{
					std::cout << "Save PointCloud now" << std::endl;
					g_pointCloudManager->savePointCloud("testsave.pcd", g_geometryManager->getActiveGeometryData()->getOpenMeshGeo());
					g_lastKeyPressedTimeStamp = std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::system_clock::now().time_since_epoch()).count();
				}


				BUTTON_STATE keyboardButtonStateKeyI = g_interactionManager->getKeyboardMouseConnection()->getButtonState(osgGA::GUIEventAdapter::KEY_I);
				if (keyboardButtonStateKeyI == PUSH)
				{
					g_pointCloudManager->runICP();
					g_lastKeyPressedTimeStamp = std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::system_clock::now().time_since_epoch()).count();
				}


				if (g_useAzureTelepresence)
				{
					BUTTON_STATE keyboardButtonStateKeyH = g_interactionManager->getKeyboardMouseConnection()->getButtonState(osgGA::GUIEventAdapter::KEY_T);

					if (keyboardButtonStateKeyH == PUSH && (std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::system_clock::now().time_since_epoch()).count() - g_lastKeyPressedTimeStamp) > 500.0)
					{
						std::cout << "T Key pressed -> Telepresence" << std::endl;
						g_pointCloudManager->updateAndRenderPointCloud();
						g_lastKeyPressedTimeStamp = std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::system_clock::now().time_since_epoch()).count();
					}


				}

				if (g_useRealSenseDevice)
				{
					if (g_useARCoreTracking)
					{
						g_ARCoreModellMatrixTransform->setMatrix(g_networkManagerOSC->getARCoreViewMatrix()->getMatrix());
					}
					BUTTON_STATE keyboardButtonStateKeyH = g_interactionManager->getKeyboardMouseConnection()->getButtonState(osgGA::GUIEventAdapter::KEY_H);
					if (keyboardButtonStateKeyH == PUSH || refreshTimeRegulatorForAutomaticDebugScanLoop % refreshCountFramesForAutomaticDebugScanLoop == 0)
					{
						/* ### START - Old Scan Pipeline ###
						if (g_useMultipleRealSenseDevices && !bodyScanningTriggerHKey)
						{
							//TODO: Ultra MemoryHoles here!
							PoseEstimationXMLReader* poseEstimationXMLReader = new PoseEstimationXMLReader();
							poseEstimationXMLReader->readConfigFile(POSE_ESTIMATION_FILE_PATH);
							std::vector<PoseEstimationXMLReader::CameraSerialPosRot>* cameraSerialPosRotStruct = poseEstimationXMLReader->getCameraSerialPosRotStruct();

							std::vector<osg::MatrixTransform*> realSensePositions;

							for (short i = 0; i < cameraSerialPosRotStruct->size(); ++i)
							{
								osg::Matrix rotation;
								rotation.makeRotate(cameraSerialPosRotStruct->at(i).rotation);

								osg::Matrix position;
								position.makeTranslate(cameraSerialPosRotStruct->at(i).position);

								osg::MatrixTransform* matrixTranform = new osg::MatrixTransform();
								matrixTranform->setMatrix(osg::Matrix::inverse(rotation * position));
								realSensePositions.push_back(matrixTranform);
							}

							//std::cout << "x0: " << cameraSerialPosRotStruct->at(0).position.x() << std::endl;
							//std::cout << "y0: " << cameraSerialPosRotStruct->at(0).position.y() << std::endl;
							//std::cout << "z0: " << cameraSerialPosRotStruct->at(0).position.z() << std::endl;
							//std::cout << "q1 von 0: " << cameraSerialPosRotStruct->at(0).rotation.x() << std::endl;
							//std::cout << "q2 von 0: " << cameraSerialPosRotStruct->at(0).rotation.y() << std::endl;
							//std::cout << "q3 von 0: " << cameraSerialPosRotStruct->at(0).rotation.z() << std::endl;
							//std::cout << "q4 von 0: " << cameraSerialPosRotStruct->at(0).rotation.w() << std::endl;

							//std::cout << "x1: " << cameraSerialPosRotStruct->at(1).position.x() << std::endl;
							//std::cout << "y1: " << cameraSerialPosRotStruct->at(1).position.y() << std::endl;
							//std::cout << "z1: " << cameraSerialPosRotStruct->at(1).position.z() << std::endl;
							//std::cout << "q1 von 1: " << (double)cameraSerialPosRotStruct->at(1).rotation.x() << std::endl;
							//std::cout << "q2 von 1: " << cameraSerialPosRotStruct->at(1).rotation.y() << std::endl;
							//std::cout << "q3 von 1: " << cameraSerialPosRotStruct->at(1).rotation.z() << std::endl;
							//std::cout << "q4 von 1: " <<  cameraSerialPosRotStruct->at(1).rotation.w() << std::endl;

							// ### START - Old Scan Pipeline ###
							//g_pointCloudManager->updateAndRenderPointCloud(realSensePositions);
							// ### END - Old Scan Pipeline ###
							bodyScanningTriggerHKey = true;
							refreshTimeRegulatorForBodyScanning = 1;
							std::cout << "Nur einmal drin?" << std::endl;
							std::cout << "in main:: g_pointCloudManager->updateAndRenderPointCloud " << std::endl;

							//TODO: Delete poseEstimationXMLReader and other stuff
						}
						else if (!g_useMultipleRealSenseDevices && g_useARCoreTracking)
						// ### END - Old Scan Pipeline ### */
						if (g_useARCoreTracking)
						{
							std::cout << "PointCloud update with ARCore and RealSense" << std::endl;
							//ToDo3: Adapt ArCoreTracking with circularBuffer similar to ViveGenericTracker solution
							//g_pointCloudManager->updateAndRenderPointCloud(g_networkManagerOSC->getARCoreViewMatrix()->getMatrix());
						} else
						{
							refreshTimeRegulatorForAutomaticDebugScanLoop = 0;
							//g_pointCloudManager->updateAndRenderPointCloud(g_interactionManager->getGenericTracker()->getMatrixTransform());
							//g_pointCloudManager->updateAndRenderPointCloud();
							g_pointCloudManager->attemptToScan();
						}
						std::cout << "Not ready for scanning - try again later" << std::endl;
						refreshTimeRegulatorForAutomaticDebugScanLoop++;
					}
					if (bodyScanningTriggerHKey && refreshTimeRegulatorForBodyScanning % refreshCountFramesForBodyScanning == 0)  // Update every refreshCountFramesForSpectatorView frames
					{
						if (refreshTimeRegulatorForBodyScanning % refreshCountFramesForBodyScanning == 0)
						{
							std::cout << "Ready to scan again" << std::endl;
							bodyScanningTriggerHKey = false;
						}
					}
					refreshTimeRegulatorForBodyScanning++;

					/*BUTTON_STATE keyboardButtonStateKeyO = g_interactionManager->getKeyboardMouseConnection()->getButtonState(osgGA::GUIEventAdapter::KEY_O);
					BUTTON_STATE triggerButtonState = g_interactionManager->getPrimaryController()->getButtonState(MENU);
					if (triggerButtonState == PUSH || triggerButtonState == HOLD) {
						g_pointCloudManager->debugDrawClear();
						g_pointCloudManager->debugDDA(g_interactionManager->getPrimaryController()->getPosition(), g_interactionManager->getSecondaryController()->getPosition());
						g_pointCloudManager->debugDrawOctreeSelection();
					}
					BUTTON_STATE triggerButtonStateSqueeze = g_interactionManager->getPrimaryController()->getButtonState(SQUEEZE);
					if (keyboardButtonStateKeyO == PUSH || triggerButtonStateSqueeze == HOLD) {
						g_pointCloudManager->debugDrawClear();
						g_pointCloudManager->debugDrawOctree();
						//g_pointCloudManager->debugDDA(g_interactionManager->getPrimaryController()->getPosition(), g_interactionManager->getSecondaryController()->getPosition());
						//g_pointCloudManager->debugDrawOctreeSelection();
					}
					BUTTON_STATE keyboardButtonStateKeyC = g_interactionManager->getKeyboardMouseConnection()->getButtonState(osgGA::GUIEventAdapter::KEY_C);
					if (keyboardButtonStateKeyC == PUSH)
					{
						g_pointCloudManager->clearPointCloud();
						std::cout << "PointCloud cleared." << std::endl;
					}
					BUTTON_STATE keyboardButtonStateKeyI = g_interactionManager->getKeyboardMouseConnection()->getButtonState(osgGA::GUIEventAdapter::KEY_I);
					if (keyboardButtonStateKeyI == PUSH)
					{
						g_pointCloudManager->savePointCloud("realSensePC.pcd"); //Works only with "/", not "\"
					}
					BUTTON_STATE keyboardButtonStateKeyM = g_interactionManager->getKeyboardMouseConnection()->getButtonState(osgGA::GUIEventAdapter::KEY_M);
					if (keyboardButtonStateKeyM == PUSH)
					{
						g_pointCloudManager->polygonizePointCloud();
					}*/
				}

				refreshTimeRegulatorForSpectatorView = 0;
			}
			refreshTimeRegulatorForSpectatorView++;
			//SPECTATOR VIEWER LOOP PART END
		}// END: g_isSpectatorWindowActive

		if (g_useRealSenseDevice)
		{
			if (g_useVive)
			{
				//Create Snapshot with primary Vive Trigger for just a single RS
				/*BUTTON_STATE triggerButtonState = g_interactionManager->getPrimaryController()->getButtonState(REAR_TRIGGER);
				if (triggerButtonState == PUSH)
				{
					if (g_useMultipleRealSenseDevices)
					{

					}
					else {
						g_pointCloudManager->updateAndRenderPointCloud();
					}
				}
				else if (triggerButtonState == HOLD)
				{
					if (g_useMultipleRealSenseDevices)
					{
					}
					else {
						g_pointCloudManager->updateAndRenderPointCloud();
						//std::cout << "### updateAndRenderPointCloud ###" << std::endl;
					}
				}*/

				//Delete Point cloud with secondary Vive Squeeze
				/*BUTTON_STATE squeeze2ndButtonState = g_interactionManager->getSecondaryController()->getButtonState(SQUEEZE);
				if (squeeze2ndButtonState == PUSH)
				{
					g_pointCloudManager->clearPointCloud();
					std::cout << "PointCloud cleared." << std::endl;
				}

				//Delete last snapshot from point cloud with secondary Vive joystick
				BUTTON_STATE joystick2ndButtonState = g_interactionManager->getSecondaryController()->getButtonState(JOYSTICK);
				float joystick2ndX = g_interactionManager->getSecondaryController()->getJoystickX();
				float joystick2ndY = g_interactionManager->getSecondaryController()->getJoystickY();
				if (joystick2ndButtonState == PUSH && joystick2ndX <= -0.15f && abs(joystick2ndY) < abs(joystick2ndX))
				{
					std::cout << (g_pointCloudManager->revertSnapshot() ? "Latest Snapshot was removed." : "There are no more steps to undo.") << std::endl;
				}

				BUTTON_STATE joystick1ndButtonState = g_interactionManager->getPrimaryController()->getButtonState(JOYSTICK);
				float joystick1ndX = g_interactionManager->getPrimaryController()->getJoystickX();
				float joystick1ndY = g_interactionManager->getPrimaryController()->getJoystickY();
				if (joystick1ndButtonState == PUSH && joystick1ndX <= -0.15f && abs(joystick1ndY) < abs(joystick1ndX)) {
					//Generate a mesh out of the PC
					g_pointCloudManager->polygonizePointCloud();
				}*/
				//g_pointCloudManager->debugDrawClear();
				//g_pointCloudManager->debugDraw(g_interactionManager->getGenericTracker()->getMatrixTransform());
			}
			//g_pointCloudManager->renderVideoTexture();
			//std::cout << "### VisualizePointCloud ###" << std::endl;

			if (g_pointCloudManager->realtimePointCloud()) g_pointCloudManager->attemptToScan();
			g_pointCloudManager->visualizePointCloud(); //The method itself decides whether it is safe to visualize or not.
			//g_pointCloudManager->updateAndRenderPointCloud(g_interactionManager->getGenericTracker()->getMatrixTransform());
			g_pointCloudManager->updateProcessingThread();

			BUTTON_STATE keyboardButtonStateKeyX = g_interactionManager->getKeyboardMouseConnection()->getButtonState(osgGA::GUIEventAdapter::KEY_X);
			if (keyboardButtonStateKeyX == PUSH && (std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::system_clock::now().time_since_epoch()).count() - g_lastKeyPressedTimeStamp) > 150.0)
			{
				//g_pointCloudManager->findAndInitializeScanDevices();
				g_pointCloudManager->toggleCameraPoseCalibration();
				//std::cout << "findAndInitializeScanDevices() finished" << std::endl;
				g_lastKeyPressedTimeStamp = std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::system_clock::now().time_since_epoch()).count();
			}
			g_pointCloudManager->finalizeCameraPoseCalibration();
		}

		BUTTON_STATE keyboardButtonStateKeyY = g_interactionManager->getKeyboardMouseConnection()->getButtonState(osgGA::GUIEventAdapter::KEY_Y);
		if (keyboardButtonStateKeyY == PUSH && (std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::system_clock::now().time_since_epoch()).count() - g_lastKeyPressedTimeStamp) > 150.0)
		{
			g_pointCloudManager->exportPointCloud();
			g_lastKeyPressedTimeStamp = std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::system_clock::now().time_since_epoch()).count();
		}

		BUTTON_STATE keyboardButtonStateKeyM = g_interactionManager->getKeyboardMouseConnection()->getButtonState(osgGA::GUIEventAdapter::KEY_M);
		if (keyboardButtonStateKeyM == PUSH && (std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::system_clock::now().time_since_epoch()).count() - g_lastKeyPressedTimeStamp) > 150.0)
		{
			g_pointCloudManager->exportPointCloudMesh();
			g_lastKeyPressedTimeStamp = std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::system_clock::now().time_since_epoch()).count();
		}
		//BUTTON_STATE keyboardButtonStateKeyT = g_interactionManager->getKeyboardMouseConnection()->getButtonState(osgGA::GUIEventAdapter::KEY_T);
		//if (keyboardButtonStateKeyT == PUSH && (std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::system_clock::now().time_since_epoch()).count() - g_lastKeyPressedTimeStamp) > 150.0)
		//{
		//	g_pointCloudManager->exportPointCloudTexture();
		//	g_lastKeyPressedTimeStamp = std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::system_clock::now().time_since_epoch()).count();
		//}
		BUTTON_STATE keyboardButtonStateKeyR = g_interactionManager->getKeyboardMouseConnection()->getButtonState(osgGA::GUIEventAdapter::KEY_R);
		if (keyboardButtonStateKeyR == PUSH && (std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::system_clock::now().time_since_epoch()).count() - g_lastKeyPressedTimeStamp) > 150.0)
		{
			g_pointCloudManager->toggleRecording();
			g_lastKeyPressedTimeStamp = std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::system_clock::now().time_since_epoch()).count();
		}
		BUTTON_STATE keyboardButtonStateKeyU = g_interactionManager->getKeyboardMouseConnection()->getButtonState(osgGA::GUIEventAdapter::KEY_U);
		if (keyboardButtonStateKeyU == PUSH && (std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::system_clock::now().time_since_epoch()).count() - g_lastKeyPressedTimeStamp) > 150.0)
		{
			g_pointCloudManager->startMultiCamOptimization();
			g_lastKeyPressedTimeStamp = std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::system_clock::now().time_since_epoch()).count();
		}
		BUTTON_STATE keyboardButtonStateKeyG = g_interactionManager->getKeyboardMouseConnection()->getButtonState(osgGA::GUIEventAdapter::KEY_G);
		if (keyboardButtonStateKeyG == PUSH && (std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::system_clock::now().time_since_epoch()).count() - g_lastKeyPressedTimeStamp) > 150.0)
		{
			g_pointCloudManager->toggleRealtimePointCloud();
			g_lastKeyPressedTimeStamp = std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::system_clock::now().time_since_epoch()).count();
		}

		//END: Point Cloud Stuff ####

		if (g_isMagicLeapSpectatorActive)
		{
			if (refreshTimeRegulatorForMagicLeapSpectator % refreshCountFramesForMagicLeapSpectator == 0) // Update every refreshCountFramesForHoloLensSpectator frames
			{
				//std::cout << "Active Geometry: " << g_geometryManager->getNameOfActiveGeometry() << std::endl;
				//osg::Vec3Array *vertexArray = dynamic_cast<osg::Vec3Array*>(g_geometryManager->getActiveGeometryData()->getOpenMeshGeo()->getVertexArray());
				//std::cout << "Vertex Array: " << vertexArray << std::endl;

				g_geometryPacketSender->sendGeometryOSC(g_geometryManager->getActiveGeometryData());


				refreshTimeRegulatorForMagicLeapSpectator = 0;
			}
			++refreshTimeRegulatorForMagicLeapSpectator;
		}
		g_pointCloudManager->drawDebugDisplay(g_interactionManager->getGenericTracker()->getMatrixTransform()->getMatrix());
		// ## START TOBI EYETRACKING TEST ## 
		// ## END TOBI EYETRACKING TEST ## 
	}//END OF MAIN LOOP

	if (ConfigReader::DEBUG_LEVEL >= NOT_IN_LOOPS)
	{
		std::cout << std::endl << "Main loop finished, shutting down... " << std::endl;
	}

	/////////
	//shut thread down (join)
	/////////
	g_pointCloudManager->joinTelepresenceThread();
	std::this_thread::sleep_for(std::chrono::milliseconds(100));


	// Need to do this here to make it happen before destruction of the OSG Viewer, which destroys the OpenGL context.
	g_interactionManager->shutdown();
	if (g_useColorAndDepthShooter)
	{
		g_pointCloudManager->shutDownAzureKinect();
	}
	delete g_pointCloudManager;

	//CleanUp of Tobi Device
	result = tobii_gaze_point_unsubscribe(tobiiDevice);
	assert(result == TOBII_ERROR_NO_ERROR);
	result = tobii_device_destroy(tobiiDevice);
	assert(result == TOBII_ERROR_NO_ERROR);
	result = tobii_api_destroy(g_tobiiAPI);
	assert(result == TOBII_ERROR_NO_ERROR);
	outputFileGazeData.close();

	g_openCV_VideoCapture.release();

	std::exit(0); //Must be called, otherwise console will be kept open

	return 0;
}


