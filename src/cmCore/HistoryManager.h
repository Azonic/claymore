// ClayMore - Immersive Mesh Modelling --- Copyright (c) 2014-2017 Philipp Ladwig, Jannik Fiedler, Jan Beutgen
#pragma once

#include <osg/ref_ptr>
#include <vector>


enum ActionType {
	//EDI Mode Actions
	EDI_GRAB,
	EDI_SCALE,
	EDI_ROTATE,
	EDI_DUPLICATE,
	EDI_DELETE_VERTEX,
	EDI_DELETE_FACE,
	EDI_EXTRUDE,
	EDI_ADD_VERTEX,
	EDI_ADD_EDGE,
	EDI_ADD_FACE,
	EDI_ADD_PRIMITIVE,

	//OBJ Mode Actions
	OBJ_GRAB,
	OBJ_SCALE,
	OBJ_ROTATE,
	OBJ_DUPLICATE,
	OBJ_DELETE,
};

class HistoryAction
{

public:
	HistoryAction(ActionType actionTypeIn);
	~HistoryAction();

	inline ActionType getActionType() { return m_actionType; }


private:
	ActionType m_actionType;

};

//TODO: Currently it's possible to go back 3 steps. 
//But there are still an issue with going back 3 times, doing 1 step forward (not redo button) an than undoing it again is not correct.
//But it runs without exceptions and works actually well, so I will use it for the final userTest
class GeometryManager;
//Manages global Undo and Redo operations
class HistoryManager
{

public:
	HistoryManager();
	~HistoryManager();

	inline void setGeometryManager(osg::ref_ptr<GeometryManager> geometryManager) { m_geometryManager = geometryManager; };

	void addAction(HistoryAction historyActionIn);
	void undo();
	void redo();

	

private:
	std::vector<HistoryAction> m_historyOfActions;
	osg::ref_ptr<GeometryManager> m_geometryManager;
};

