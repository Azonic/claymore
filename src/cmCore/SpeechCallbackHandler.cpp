// ClayMore - Immersive Mesh Modelling --- Copyright (c) 2014-2017 Philipp Ladwig, Jannik Fiedler, Jan Beutgen
#include "SpeechCallbackHandler.h"

//#include "cmNetworkManagementQt\NetworkManager.h"

#include <iostream>

#include <utility>

SpeechCallbackHandler::SpeechCallbackHandler(NetworkManager* networkManager) : m_networkManager(networkManager)
{
	m_startTickListen = osg::Timer::instance()->tick();
	m_keepAwakeListen = osg::Timer::instance()->tick();
	m_lastSpeechCommand = "No command received yet";
}

SpeechCallbackHandler::~SpeechCallbackHandler()
{

}

void SpeechCallbackHandler::update()
{
	//TODO5: Implement it with NetworkManagementOSC

	////Wait 0.75 sec after first press of squeeze and than (enable) listen (avoids wrong regonitions from talking before "squeezing")
	//if (osg::Timer::instance()->delta_m(m_startTickListen, osg::Timer::instance()->tick()) > 750)
	//{
	//	//If the last keepAwakeListen time is more than 1,5sec ago, disable listening
	//	if (osg::Timer::instance()->delta_m(m_keepAwakeListen, osg::Timer::instance()->tick()) < 1500)
	//	{
	//		m_networkManager->enableSpeechListening();
	//		if (m_networkManager->getSpeechCommands()->size() > 0)
	//		{
	//			for (std::vector<std::string>::iterator it = m_networkManager->getSpeechCommands()->begin(); it != m_networkManager->getSpeechCommands()->end(); ++it)
	//			{
	//				if (it->compare("OBJECT_SELECT") == 0)
	//				{
	//					//std::cout << "IN OBJECT_SELECT" << std::endl;
	//					m_networkManager->getSpeechCommands()->erase(it);
	//					m_speechCallbacks["OBJECT_SELECT"]();
	//					m_lastSpeechCommand = "OBJECT_SELECT";
	//					return;
	//				}
	//				else if (it->compare("TOGGLE_WIRE") == 0)
	//				{
	//					m_networkManager->getSpeechCommands()->erase(it);
	//					m_speechCallbacks["TOGGLE_WIRE"]();
	//					m_lastSpeechCommand = "TOGGLE_WIRE";
	//					return;
	//				}
	//				else if (it->compare("TOGGLE_VERTEX") == 0)
	//				{
	//					m_networkManager->getSpeechCommands()->erase(it);
	//					m_speechCallbacks["TOGGLE_VERTEX"]();
	//					m_lastSpeechCommand = "TOGGLE_VERTEX";
	//					return;
	//				}
	//				else if (it->compare("SCALE_VERTEX") == 0)
	//				{
	//					m_networkManager->getSpeechCommands()->erase(it);
	//					m_speechCallbacks["SCALE_VERTEX"]();
	//					m_lastSpeechCommand = "SCALE_VERTEX";
	//					return;
	//				}
	//				else if (it->compare("SELECT_POINTS") == 0)
	//				{
	//					m_networkManager->getSpeechCommands()->erase(it);
	//					m_speechCallbacks["SELECT_POINTS"]();
	//					m_lastSpeechCommand = "SELECT_POINTS";
	//					return;
	//				}
	//				else if (it->compare("DELETE_THIS") == 0)
	//				{
	//					m_networkManager->getSpeechCommands()->erase(it);
	//					m_speechCallbacks["DELETE_THIS"]();
	//					m_lastSpeechCommand = "DELETE_THIS";
	//					return;
	//				}
	//				else if (it->compare("ADD_CUBE") == 0)
	//				{
	//					m_networkManager->getSpeechCommands()->erase(it);
	//					m_speechCallbacks["ADD_CUBE"]();
	//					m_lastSpeechCommand = "ADD_CUBE";
	//					return;
	//				}
	//				else if (it->compare("ADD_SPHERE") == 0)
	//				{
	//					m_networkManager->getSpeechCommands()->erase(it);
	//					m_speechCallbacks["ADD_SPHERE"]();
	//					m_lastSpeechCommand = "ADD_SPHERE";
	//					return;
	//				}
	//				else if (it->compare("ADD_CYLINDER") == 0)
	//				{
	//					m_networkManager->getSpeechCommands()->erase(it);
	//					m_speechCallbacks["ADD_CYLINDER"]();
	//					m_lastSpeechCommand = "ADD_CYLINDER";
	//					return;
	//				}
	//				else if (it->compare("ADD_CONE") == 0)
	//				{
	//					m_networkManager->getSpeechCommands()->erase(it);
	//					m_speechCallbacks["ADD_CONE"]();
	//					m_lastSpeechCommand = "ADD_CONE";
	//					return;
	//				}
	//				else if (it->compare("NO_RECOG") == 0)
	//				{
	//					m_networkManager->getSpeechCommands()->erase(it);
	//					m_lastSpeechCommand = "NO_RECOG";
	//					return;
	//				}
	//			}
	//		}
	//	}
	//	else
	//	{
	//		m_networkManager->disableSpeechListening();
	//	}
	//}
}

void SpeechCallbackHandler::setSpeechCallback(std::string speechCommand, void(*callback)())
{
	m_speechCallbacks.insert(std::pair<std::string, void(*)()>(speechCommand, callback));
}

void SpeechCallbackHandler::startListen()
{
	m_startTickListen = osg::Timer::instance()->tick();
}

void SpeechCallbackHandler::lastKeepAwakeListing()
{
	m_keepAwakeListen = osg::Timer::instance()->tick();
}