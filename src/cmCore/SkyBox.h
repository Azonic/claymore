// ClayMore - Immersive Mesh Modelling --- Copyright (c) 2014-2017 Philipp Ladwig, Jannik Fiedler, Jan Beutgen
#pragma once

#include <osg/TexMat>
#include <osg/Depth>
#include <osg/TextureCubeMap>
#include <osgDB/ReadFile>
#include <osgUtil/CullVisitor>
#include <osg/TexEnv>
#include <osg/TexGen>
#include <osg/ShapeDrawable>
#include <osg/MatrixTransform>

struct TexMatCallback : public osg::NodeCallback
{
public:

	TexMatCallback(osg::TexMat& tm) :
		_texMat(tm)
	{
	}

	virtual void operator()(osg::Node* node, osg::NodeVisitor* nv)
	{
		osgUtil::CullVisitor* cv = dynamic_cast<osgUtil::CullVisitor*>(nv);
		if (cv)
		{
			const osg::Matrix& MV = *(cv->getModelViewMatrix());
			const osg::Matrix R = osg::Matrix::rotate(M_PI, 0.0, 0.0, 1.0);

			osg::Quat q = MV.getRotate();
			const osg::Matrix C = osg::Matrix::rotate(q.inverse());

			_texMat.setMatrix(C*R);
		}

		traverse(node, nv);
	}

	osg::TexMat& _texMat;
};

class Skybox
{

public:
	Skybox(std::string & name);
	~Skybox();

	inline void attachToSceneGraph(osg::Group* parentGroupIn) {	parentGroupIn->addChild(m_clearNode); }

private:

	osg::TextureCubeMap* readCubeMap();
	void createSkyBox();

	std::string m_name;
	osg::ref_ptr<osg::ClearNode> m_clearNode;

};