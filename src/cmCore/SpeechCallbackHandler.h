// ClayMore - Immersive Mesh Modelling --- Copyright (c) 2014-2017 Philipp Ladwig, Jannik Fiedler, Jan Beutgen
#pragma once

#include <string>
#include <map>
#include <osg\Timer>

class NetworkManager;

class SpeechCallbackHandler
{

public:
	// #### CONSTRUCTOR & DESTRUCTOR ###############
	SpeechCallbackHandler(NetworkManager* networkManager);
	~SpeechCallbackHandler();

	// #### MEMBER FUNCTIONS ###############
public:
	void update();
	void startListen();
	void lastKeepAwakeListing();
	void setSpeechCallback(std::string speechCommand, void(*callback)());
	//void setNetworkMangement(NetworkManager* networkManager);
	inline std::string getLastCommand() { return m_lastSpeechCommand; }

	// #### MEMBER VARIABLES ###############
private:
	NetworkManager* m_networkManager;
	std::map<std::string, void(*)()> m_speechCallbacks;
	osg::Timer_t m_startTickListen;
	osg::Timer_t m_keepAwakeListen;
	std::string	m_lastSpeechCommand;
};


