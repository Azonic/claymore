// ClayMore - Immersive Mesh Modelling --- Copyright (c) 2014-2017 Philipp Ladwig, Jannik Fiedler, Jan Beutgen
#pragma once

//First initialze menus and afterwards access them with observers
bool createAndSetUpMenuManagement()
{
	if (ConfigReader::DEBUG_LEVEL >= NOT_IN_LOOPS)
		std::cout << std::endl << "Adding MenuManager" << std::endl;
	g_menuManager = new MenuManager();

	//Parsing menu colors and some variables from the config.xml - just for easier reading, cuse of shorter lines
	osg::Vec4 defaultMenuColor,
		highlightMenuColor,
		defaultTouchMenuColor,
		highlightTouchMenuColor,
		editModeContextDefaultFenceMenuColor,
		editModeContextHighlightFenceMenuColor,
		menuPointerColor;
	defaultMenuColor = g_configReader->getVec4fFromStartupConfig("default_fence_menu_color");
	highlightMenuColor = g_configReader->getVec4fFromStartupConfig("highlight_fence_menu_color");
	defaultTouchMenuColor = g_configReader->getVec4fFromStartupConfig("default_touch_menu_color");
	highlightTouchMenuColor = g_configReader->getVec4fFromStartupConfig("highlight_touch_menu_color");
	editModeContextDefaultFenceMenuColor = g_configReader->getVec4fFromStartupConfig("edit_mode_context_default_fence_menu_color");
	editModeContextHighlightFenceMenuColor = g_configReader->getVec4fFromStartupConfig("edit_mode_context_highlight_fence_menu_color");
	menuPointerColor = g_configReader->getVec4fFromStartupConfig("menu_pointer_color");
	float touchMenuFloatingHeight = g_configReader->getFloatFromStartupConfig("touch_menu_floating_height");
	float dartMenuTextSize = g_configReader->getFloatFromStartupConfig("dart_menu_text_size");
	float pieMenuFloatingHeight = g_configReader->getFloatFromStartupConfig("pie_menu_floating_height");

	//##############################################################################
	//##############################################################################
	//##############################################################################
	//##############################################################################
	//############################ MENU STRUCTURE ##################################
	//##############################################################################
	//##############################################################################
	//##############################################################################
	//##############################################################################
	//##############################################################################

	if (g_configReader->getStringFromStartupConfig("type_of_menu_structure").compare("DEFAULT") == 0)
	{
		{
			TiltMenu* tiltModeMenu = new TiltMenu(g_configReader, TILT_AND_SHIFT_MODE, defaultMenuColor, highlightMenuColor, menuPointerColor, CONTROLLER_TYPE::SECONDARY);
			tiltModeMenu->getMenuTransform()->setName("PosAttTra-TILT_AND_SHIFT_MODE-Menu");
			g_menuManager->addMenuForUniqueName(tiltModeMenu);

			BUTTON_ID buttonTiltShiftModeID1 = tiltModeMenu->createButton(g_languageStringsReader->getString("object_mode"));
			tiltModeMenu->getMenuButton(buttonTiltShiftModeID1)->triggerCallback = activateMenuStructureForObjectMode;

			BUTTON_ID buttonTiltShiftModeID2 = tiltModeMenu->createButton(g_languageStringsReader->getString("edit_mode"));
			tiltModeMenu->getMenuButton(buttonTiltShiftModeID2)->triggerCallback = activateMenuStructureForEditMode;

			BUTTON_ID buttonTiltShiftModeID3 = tiltModeMenu->createButton(g_languageStringsReader->getString("scan_mode"));
			//static_cast<TiltMenuButton*>(tiltModeMenu->getMenuButton(buttonTiltShiftModeID3))->markAsActive(true);
			tiltModeMenu->getMenuButton(buttonTiltShiftModeID3)->triggerCallback = activateScanMode;

			BUTTON_ID buttonTiltShiftModeID4 = tiltModeMenu->createButton(g_languageStringsReader->getString("sculpt_mode"));
			static_cast<TiltMenuButton*>(tiltModeMenu->getMenuButton(buttonTiltShiftModeID4))->markAsActive(false);
			tiltModeMenu->getMenuButton(buttonTiltShiftModeID4)->triggerCallback = activateSculptMode;

			BUTTON_ID buttonTiltShiftModeID5 = tiltModeMenu->createButton("");
			static_cast<TiltMenuButton*>(tiltModeMenu->getMenuButton(buttonTiltShiftModeID5))->markAsSpacerButton(true);
			tiltModeMenu->getMenuButton(buttonTiltShiftModeID5)->triggerCallback = emptyCallback;

			BUTTON_ID buttonTiltShiftModeID6 = tiltModeMenu->createButton("");
			static_cast<TiltMenuButton*>(tiltModeMenu->getMenuButton(buttonTiltShiftModeID6))->markAsSpacerButton(true);
			tiltModeMenu->getMenuButton(buttonTiltShiftModeID6)->triggerCallback = emptyCallback;

			BUTTON_ID buttonTiltShiftModeID7 = tiltModeMenu->createButton("");
			static_cast<TiltMenuButton*>(tiltModeMenu->getMenuButton(buttonTiltShiftModeID7))->markAsSpacerButton(true);
			tiltModeMenu->getMenuButton(buttonTiltShiftModeID7)->triggerCallback = emptyCallback;

			BUTTON_ID buttonTiltShiftModeID8 = tiltModeMenu->createButton("");
			static_cast<TiltMenuButton*>(tiltModeMenu->getMenuButton(buttonTiltShiftModeID8))->markAsSpacerButton(true);
			tiltModeMenu->getMenuButton(buttonTiltShiftModeID8)->triggerCallback = emptyCallback;

			BUTTON_ID buttonTiltShiftModeID9 = tiltModeMenu->createButton("");
			static_cast<TiltMenuButton*>(tiltModeMenu->getMenuButton(buttonTiltShiftModeID9))->markAsSpacerButton(true);
			tiltModeMenu->getMenuButton(buttonTiltShiftModeID9)->triggerCallback = emptyCallback;

			BUTTON_ID buttonTiltShiftModeID10 = tiltModeMenu->createButton(g_languageStringsReader->getString("paint_mode"));
			static_cast<TiltMenuButton*>(tiltModeMenu->getMenuButton(buttonTiltShiftModeID10))->markAsActive(false);
			tiltModeMenu->getMenuButton(buttonTiltShiftModeID10)->triggerCallback = activatePaintingMode;

			BUTTON_ID buttonTiltShiftModeID11 = tiltModeMenu->createButton(g_languageStringsReader->getString("rigging_mode"));
			static_cast<TiltMenuButton*>(tiltModeMenu->getMenuButton(buttonTiltShiftModeID11))->markAsActive(false);
			tiltModeMenu->getMenuButton(buttonTiltShiftModeID11)->triggerCallback = activateSkinningRiggingMode;

			tiltModeMenu->setFramesBeforeClose(50000);
			tiltModeMenu->setStartScaleValue(0.000f);
			tiltModeMenu->setTargetScaleValue(0.20f);
			tiltModeMenu->initializeDrawables();
			tiltModeMenu->attachToSceneGraph(g_sceneRoot);
		}

		{
			TouchMenu *touchObjectModeMainMenu = new TouchMenu(TOUCH_OBJMODE_MAIN, touchMenuFloatingHeight, defaultTouchMenuColor, highlightTouchMenuColor, CONTROLLER_TYPE::SECONDARY);
			touchObjectModeMainMenu->getMenuTransform()->setName("PosAttTra-TOUCH_OBJMODE_MAIN-Menu");
			g_menuManager->addMenuForUniqueName(touchObjectModeMainMenu);


			BUTTON_ID touchButtonID1 = touchObjectModeMainMenu->createButton("");
			static_cast<TouchMenuButton*>(touchObjectModeMainMenu->getMenuButton(touchButtonID1))->setButtonUseTexture("./data/menu_textures/toolMenuIcon.png");
			static_cast<TouchMenuButton*>(touchObjectModeMainMenu->getMenuButton(touchButtonID1))->setButtonUseTextureHover("./data/menu_textures/toolMenuIconHover.png");
			static_cast<TouchMenuButton*>(touchObjectModeMainMenu->getMenuButton(touchButtonID1))->setButtonUseTextureActive("./data/menu_textures/toolMenuIconActive.png");
			touchObjectModeMainMenu->getMenuButton(touchButtonID1)->triggerCallback = showOBJModeToolsMenu;

			BUTTON_ID touchButtonID2 = touchObjectModeMainMenu->createButton("");
			static_cast<TouchMenuButton*>(touchObjectModeMainMenu->getMenuButton(touchButtonID2))->setButtonUseTexture("./data/menu_textures/redo.png");
			static_cast<TouchMenuButton*>(touchObjectModeMainMenu->getMenuButton(touchButtonID2))->setButtonUseTextureHover("./data/menu_textures/redoHover.png");
			static_cast<TouchMenuButton*>(touchObjectModeMainMenu->getMenuButton(touchButtonID2))->setButtonUseTextureActive("./data/menu_textures/redoActive.png");
			touchObjectModeMainMenu->getMenuButton(touchButtonID2)->triggerCallback = emptyCallback;

			BUTTON_ID touchButtonID3 = touchObjectModeMainMenu->createButton("");
			static_cast<TouchMenuButton*>(touchObjectModeMainMenu->getMenuButton(touchButtonID3))->setButtonUseTexture("./data/menu_textures/fenceMenuIcon.png");
			static_cast<TouchMenuButton*>(touchObjectModeMainMenu->getMenuButton(touchButtonID3))->setButtonUseTextureHover("./data/menu_textures/fenceMenuIconHover.png");
			static_cast<TouchMenuButton*>(touchObjectModeMainMenu->getMenuButton(touchButtonID3))->setButtonUseTextureActive("./data/menu_textures/fenceMenuIconActive.png");
			touchObjectModeMainMenu->getMenuButton(touchButtonID3)->triggerCallback = showGeneralCommandFenceMenu;

			BUTTON_ID touchButtonID4 = touchObjectModeMainMenu->createButton("");
			static_cast<TouchMenuButton*>(touchObjectModeMainMenu->getMenuButton(touchButtonID4))->setButtonUseTexture("./data/menu_textures/undo.png");
			static_cast<TouchMenuButton*>(touchObjectModeMainMenu->getMenuButton(touchButtonID4))->setButtonUseTextureHover("./data/menu_textures/undoHover.png");
			static_cast<TouchMenuButton*>(touchObjectModeMainMenu->getMenuButton(touchButtonID4))->setButtonUseTextureActive("./data/menu_textures/undoActive.png");
			touchObjectModeMainMenu->getMenuButton(touchButtonID4)->triggerCallback = emptyCallback;

			//Help Text
			touchObjectModeMainMenu->setHelpText(g_languageStringsReader->getString("upper_redo"),
				g_languageStringsReader->getString("upper_undo"),
				g_languageStringsReader->getString("upper_object_mode_tools"),
				g_languageStringsReader->getString("upper_object_mode_settings"));

			touchObjectModeMainMenu->initializeHelpText();

			touchObjectModeMainMenu->setFramesBeforeClose(50000);
			touchObjectModeMainMenu->setStartScaleValue(0.0f);
			touchObjectModeMainMenu->setTargetScaleValue(0.026f);
			touchObjectModeMainMenu->initializeDrawables();
			touchObjectModeMainMenu->attachToSceneGraph(g_interactionManager->getSecondaryController()->getMatrixTransformNotShadowed());
		}
		
		{
			TouchMenu *touchEdiModeMainMenu = new TouchMenu(TOUCH_EDIMODE_MAIN, touchMenuFloatingHeight, defaultTouchMenuColor, highlightTouchMenuColor, CONTROLLER_TYPE::SECONDARY);
			touchEdiModeMainMenu->getMenuTransform()->setName("PosAttTra-TOUCH_EDIMODE_MAIN-Menu");
			g_menuManager->addMenuForUniqueName(touchEdiModeMainMenu);

			BUTTON_ID touchButtonID1 = touchEdiModeMainMenu->createButton("");
			static_cast<TouchMenuButton*>(touchEdiModeMainMenu->getMenuButton(touchButtonID1))->setButtonUseTexture("./data/menu_textures/toolMenuIcon.png");
			static_cast<TouchMenuButton*>(touchEdiModeMainMenu->getMenuButton(touchButtonID1))->setButtonUseTextureHover("./data/menu_textures/toolMenuIconHover.png");
			static_cast<TouchMenuButton*>(touchEdiModeMainMenu->getMenuButton(touchButtonID1))->setButtonUseTextureActive("./data/menu_textures/toolMenuIconActive.png");
			touchEdiModeMainMenu->getMenuButton(touchButtonID1)->triggerCallback = showEDIModeToolsMenu;

			BUTTON_ID touchButtonID2 = touchEdiModeMainMenu->createButton("");
			static_cast<TouchMenuButton*>(touchEdiModeMainMenu->getMenuButton(touchButtonID2))->setButtonUseTexture("./data/menu_textures/redo.png");
			static_cast<TouchMenuButton*>(touchEdiModeMainMenu->getMenuButton(touchButtonID2))->setButtonUseTextureHover("./data/menu_textures/redoHover.png");
			static_cast<TouchMenuButton*>(touchEdiModeMainMenu->getMenuButton(touchButtonID2))->setButtonUseTextureActive("./data/menu_textures/redoActive.png");
			touchEdiModeMainMenu->getMenuButton(touchButtonID2)->triggerCallback = emptyCallback;

			BUTTON_ID touchButtonID3 = touchEdiModeMainMenu->createButton("");
			static_cast<TouchMenuButton*>(touchEdiModeMainMenu->getMenuButton(touchButtonID3))->setButtonUseTexture("./data/menu_textures/fenceMenuIcon.png");
			static_cast<TouchMenuButton*>(touchEdiModeMainMenu->getMenuButton(touchButtonID3))->setButtonUseTextureHover("./data/menu_textures/fenceMenuIconHover.png");
			static_cast<TouchMenuButton*>(touchEdiModeMainMenu->getMenuButton(touchButtonID3))->setButtonUseTextureActive("./data/menu_textures/fenceMenuIconActive.png");
			touchEdiModeMainMenu->getMenuButton(touchButtonID3)->triggerCallback = showGeneralCommandFenceMenu;

			BUTTON_ID touchButtonID4 = touchEdiModeMainMenu->createButton("");
			static_cast<TouchMenuButton*>(touchEdiModeMainMenu->getMenuButton(touchButtonID4))->setButtonUseTexture("./data/menu_textures/undo.png");
			static_cast<TouchMenuButton*>(touchEdiModeMainMenu->getMenuButton(touchButtonID4))->setButtonUseTextureHover("./data/menu_textures/undoHover.png");
			static_cast<TouchMenuButton*>(touchEdiModeMainMenu->getMenuButton(touchButtonID4))->setButtonUseTextureActive("./data/menu_textures/undoActive.png");
			touchEdiModeMainMenu->getMenuButton(touchButtonID4)->triggerCallback = undo;

			touchEdiModeMainMenu->setHelpText(g_languageStringsReader->getString("upper_redo"),
				g_languageStringsReader->getString("upper_undo"),
				g_languageStringsReader->getString("upper_edit_mode_tools"),
				g_languageStringsReader->getString("upper_edit_mode_settings"));

			touchEdiModeMainMenu->initializeHelpText();

			touchEdiModeMainMenu->setFramesBeforeClose(50000);
			touchEdiModeMainMenu->setStartScaleValue(0.0f);
			touchEdiModeMainMenu->setTargetScaleValue(0.026f);
			touchEdiModeMainMenu->initializeDrawables();
			touchEdiModeMainMenu->attachToSceneGraph(g_interactionManager->getSecondaryController()->getMatrixTransformNotShadowed());
		}

		{
			TouchMenu *touchOBJModeTransRotScaleMenu = new TouchMenu(TOUCH_OBJMODE_TRANS_ROT_SCALE, touchMenuFloatingHeight, defaultTouchMenuColor, highlightTouchMenuColor, CONTROLLER_TYPE::PRIMARY);
			touchOBJModeTransRotScaleMenu->getMenuTransform()->setName("PosAttTra-TOUCH_OBJMODE_TRANS_ROT_SCALE-Menu");
			g_menuManager->addMenuForUniqueName(touchOBJModeTransRotScaleMenu);

			BUTTON_ID touchButtonID1 = touchOBJModeTransRotScaleMenu->createButton("");
			static_cast<TouchMenuButton*>(touchOBJModeTransRotScaleMenu->getMenuButton(touchButtonID1))->setButtonUseTexture("./data/menu_textures/translate_icon.png");
			static_cast<TouchMenuButton*>(touchOBJModeTransRotScaleMenu->getMenuButton(touchButtonID1))->setButtonUseTextureHover("./data/menu_textures/translate_iconHover.png");
			static_cast<TouchMenuButton*>(touchOBJModeTransRotScaleMenu->getMenuButton(touchButtonID1))->setButtonUseTextureActive("./data/menu_textures/translate_iconActive.png");
			touchOBJModeTransRotScaleMenu->getMenuButton(touchButtonID1)->triggerCallback = registerTranslateSelectedObjectObserver;

			BUTTON_ID touchButtonID2 = touchOBJModeTransRotScaleMenu->createButton("");
			static_cast<TouchMenuButton*>(touchOBJModeTransRotScaleMenu->getMenuButton(touchButtonID2))->setButtonUseTexture("./data/menu_textures/scale_icon.png");
			static_cast<TouchMenuButton*>(touchOBJModeTransRotScaleMenu->getMenuButton(touchButtonID2))->setButtonUseTextureHover("./data/menu_textures/scale_iconHover.png");
			static_cast<TouchMenuButton*>(touchOBJModeTransRotScaleMenu->getMenuButton(touchButtonID2))->setButtonUseTextureActive("./data/menu_textures/scale_iconActive.png");
			touchOBJModeTransRotScaleMenu->getMenuButton(touchButtonID2)->triggerCallback = registerScaleSelectedObjectObserver;

			BUTTON_ID touchButtonID3 = touchOBJModeTransRotScaleMenu->createButton("");
			static_cast<TouchMenuButton*>(touchOBJModeTransRotScaleMenu->getMenuButton(touchButtonID3))->setButtonUseTexture("./data/menu_textures/fenceMenuIcon.png");
			static_cast<TouchMenuButton*>(touchOBJModeTransRotScaleMenu->getMenuButton(touchButtonID3))->setButtonUseTextureHover("./data/menu_textures/fenceMenuIconHover.png");
			static_cast<TouchMenuButton*>(touchOBJModeTransRotScaleMenu->getMenuButton(touchButtonID3))->setButtonUseTextureActive("./data/menu_textures/fenceMenuIconActive.png");
			touchOBJModeTransRotScaleMenu->getMenuButton(touchButtonID3)->triggerCallback = showObjectModeDetailCommandFenceMenu;

			BUTTON_ID touchButtonID4 = touchOBJModeTransRotScaleMenu->createButton("");
			static_cast<TouchMenuButton*>(touchOBJModeTransRotScaleMenu->getMenuButton(touchButtonID4))->setButtonUseTexture("./data/menu_textures/rotate_icon.png");
			static_cast<TouchMenuButton*>(touchOBJModeTransRotScaleMenu->getMenuButton(touchButtonID4))->setButtonUseTextureHover("./data/menu_textures/rotate_iconHover.png");
			static_cast<TouchMenuButton*>(touchOBJModeTransRotScaleMenu->getMenuButton(touchButtonID4))->setButtonUseTextureActive("./data/menu_textures/rotate_iconActive.png");

			touchOBJModeTransRotScaleMenu->setHelpText(g_languageStringsReader->getString("upper_scale"),
				g_languageStringsReader->getString("upper_rotate"),
				g_languageStringsReader->getString("upper_move"),
				g_languageStringsReader->getString("upper_more"));

			touchOBJModeTransRotScaleMenu->initializeHelpText();

			touchOBJModeTransRotScaleMenu->setFramesBeforeClose(50000);
			touchOBJModeTransRotScaleMenu->setStartScaleValue(0.0f);
			touchOBJModeTransRotScaleMenu->setTargetScaleValue(0.022f);
			touchOBJModeTransRotScaleMenu->initializeDrawables();
			touchOBJModeTransRotScaleMenu->attachToSceneGraph(g_interactionManager->getPrimaryController()->getMatrixTransformNotShadowed());
		}

		{
			TouchMenu *touchEditModeTransRotScaleMenu = new TouchMenu(TOUCH_EDIMODE_TRANS_ROT_SCALE, touchMenuFloatingHeight, defaultTouchMenuColor, highlightTouchMenuColor, CONTROLLER_TYPE::PRIMARY);
			touchEditModeTransRotScaleMenu->getMenuTransform()->setName("PosAttTra-TOUCH_EDIMODE_TRANS_ROT_SCALE-Menu");
			g_menuManager->addMenuForUniqueName(touchEditModeTransRotScaleMenu);

			BUTTON_ID touchButtonID1 = touchEditModeTransRotScaleMenu->createButton("");
			static_cast<TouchMenuButton*>(touchEditModeTransRotScaleMenu->getMenuButton(touchButtonID1))->setButtonUseTexture("./data/menu_textures/translate_icon.png");
			static_cast<TouchMenuButton*>(touchEditModeTransRotScaleMenu->getMenuButton(touchButtonID1))->setButtonUseTextureHover("./data/menu_textures/translate_iconHover.png");
			static_cast<TouchMenuButton*>(touchEditModeTransRotScaleMenu->getMenuButton(touchButtonID1))->setButtonUseTextureActive("./data/menu_textures/translate_iconActive.png");
			static_cast<TouchMenuButton*>(touchEditModeTransRotScaleMenu->getMenuButton(touchButtonID1))->setHighlightActiveFromExternalBool(g_miscDataStorage->getAddressOfIsEDITranslateToolActive());
			touchEditModeTransRotScaleMenu->getMenuButton(touchButtonID1)->triggerCallback = registerTranslateVerticesObserver;

			BUTTON_ID touchButtonID2 = touchEditModeTransRotScaleMenu->createButton("");
			static_cast<TouchMenuButton*>(touchEditModeTransRotScaleMenu->getMenuButton(touchButtonID2))->setButtonUseTexture("./data/menu_textures/scale_icon.png");
			static_cast<TouchMenuButton*>(touchEditModeTransRotScaleMenu->getMenuButton(touchButtonID2))->setButtonUseTextureHover("./data/menu_textures/scale_iconHover.png");
			static_cast<TouchMenuButton*>(touchEditModeTransRotScaleMenu->getMenuButton(touchButtonID2))->setButtonUseTextureActive("./data/menu_textures/scale_iconActive.png");
			static_cast<TouchMenuButton*>(touchEditModeTransRotScaleMenu->getMenuButton(touchButtonID2))->setHighlightActiveFromExternalBool(g_miscDataStorage->getAddressOfIsEDIScaleToolActive());
			touchEditModeTransRotScaleMenu->getMenuButton(touchButtonID2)->triggerCallback = registerScaleVerticesObserver;

			BUTTON_ID touchButtonID3 = touchEditModeTransRotScaleMenu->createButton("");
			static_cast<TouchMenuButton*>(touchEditModeTransRotScaleMenu->getMenuButton(touchButtonID3))->setButtonUseTexture("./data/menu_textures/fenceMenuIcon.png");
			static_cast<TouchMenuButton*>(touchEditModeTransRotScaleMenu->getMenuButton(touchButtonID3))->setButtonUseTextureHover("./data/menu_textures/fenceMenuIconHover.png");
			static_cast<TouchMenuButton*>(touchEditModeTransRotScaleMenu->getMenuButton(touchButtonID3))->setButtonUseTextureActive("./data/menu_textures/fenceMenuIconActive.png");
			touchEditModeTransRotScaleMenu->getMenuButton(touchButtonID3)->triggerCallback = showEDIModeDetailCommandFenceMenu;

			BUTTON_ID touchButtonID4 = touchEditModeTransRotScaleMenu->createButton("");
			static_cast<TouchMenuButton*>(touchEditModeTransRotScaleMenu->getMenuButton(touchButtonID4))->setButtonUseTexture("./data/menu_textures/rotate_icon.png");
			static_cast<TouchMenuButton*>(touchEditModeTransRotScaleMenu->getMenuButton(touchButtonID4))->setButtonUseTextureHover("./data/menu_textures/rotate_iconHover.png");
			static_cast<TouchMenuButton*>(touchEditModeTransRotScaleMenu->getMenuButton(touchButtonID4))->setButtonUseTextureActive("./data/menu_textures/rotate_iconActive.png");
			static_cast<TouchMenuButton*>(touchEditModeTransRotScaleMenu->getMenuButton(touchButtonID4))->setHighlightActiveFromExternalBool(g_miscDataStorage->getAddressOfIsEDIRotateToolActive());
			touchEditModeTransRotScaleMenu->getMenuButton(touchButtonID4)->triggerCallback = registerRotateVerticesObserver;

			touchEditModeTransRotScaleMenu->setHelpText(g_languageStringsReader->getString("upper_scale"),
				g_languageStringsReader->getString("upper_rotate"),
				g_languageStringsReader->getString("upper_move"),
				g_languageStringsReader->getString("upper_more"));

			touchEditModeTransRotScaleMenu->initializeHelpText();

			touchEditModeTransRotScaleMenu->setFramesBeforeClose(50000);
			touchEditModeTransRotScaleMenu->setStartScaleValue(0.0f);
			touchEditModeTransRotScaleMenu->setTargetScaleValue(0.022f);
			touchEditModeTransRotScaleMenu->initializeDrawables();
			touchEditModeTransRotScaleMenu->attachToSceneGraph(g_interactionManager->getPrimaryController()->getMatrixTransformNotShadowed());
		}

		{
			TouchMenu *touchOBJSelectionMenu = new TouchMenu(TOUCH_OBJMODE_SELECTION, touchMenuFloatingHeight, defaultTouchMenuColor, highlightTouchMenuColor, CONTROLLER_TYPE::PRIMARY);
			touchOBJSelectionMenu->getMenuTransform()->setName("PosAttTra-TOUCH_OBJMODE_SELECTION-Menu");
			g_menuManager->addMenuForUniqueName(touchOBJSelectionMenu);

			BUTTON_ID touchButtonID1 = touchOBJSelectionMenu->createButton("1");
			static_cast<TouchMenuButton*>(touchOBJSelectionMenu->getMenuButton(touchButtonID1))->setButtonUseTexture("./data/menu_textures/confirmMultipleSelection.png");
			static_cast<TouchMenuButton*>(touchOBJSelectionMenu->getMenuButton(touchButtonID1))->setButtonUseTextureHover("./data/menu_textures/confirmMultipleSelectionHover.png");
			static_cast<TouchMenuButton*>(touchOBJSelectionMenu->getMenuButton(touchButtonID1))->setButtonUseTextureActive("./data/menu_textures/confirmMultipleSelectionActive.png");
			touchOBJSelectionMenu->getMenuButton(touchButtonID1)->triggerCallback = OBJModeIntersectionMultipleSelection;

			BUTTON_ID touchButtonID2 = touchOBJSelectionMenu->createButton("2");
			static_cast<TouchMenuButton*>(touchOBJSelectionMenu->getMenuButton(touchButtonID2))->setButtonUseTexture("./data/menu_textures/negateSelection.png");
			static_cast<TouchMenuButton*>(touchOBJSelectionMenu->getMenuButton(touchButtonID2))->setButtonUseTextureHover("./data/menu_textures/negateSelectionHover.png");
			static_cast<TouchMenuButton*>(touchOBJSelectionMenu->getMenuButton(touchButtonID2))->setButtonUseTextureActive("./data/menu_textures/negateSelectionActive.png");
			touchOBJSelectionMenu->getMenuButton(touchButtonID2)->triggerCallback = emptyCallback;

			BUTTON_ID touchButtonID3 = touchOBJSelectionMenu->createButton("3");
			static_cast<TouchMenuButton*>(touchOBJSelectionMenu->getMenuButton(touchButtonID3))->setButtonUseTexture("./data/menu_textures/fenceMenuIcon.png");
			static_cast<TouchMenuButton*>(touchOBJSelectionMenu->getMenuButton(touchButtonID3))->setButtonUseTextureHover("./data/menu_textures/fenceMenuIconHover.png");
			static_cast<TouchMenuButton*>(touchOBJSelectionMenu->getMenuButton(touchButtonID3))->setButtonUseTextureActive("./data/menu_textures/fenceMenuIconActive.png");
			touchOBJSelectionMenu->getMenuButton(touchButtonID3)->triggerCallback = showObjectModeDetailCommandFenceMenu;

			BUTTON_ID touchButtonID4 = touchOBJSelectionMenu->createButton("4");
			static_cast<TouchMenuButton*>(touchOBJSelectionMenu->getMenuButton(touchButtonID4))->setButtonUseTexture("./data/menu_textures/confirmSelection.png");
			static_cast<TouchMenuButton*>(touchOBJSelectionMenu->getMenuButton(touchButtonID4))->setButtonUseTextureHover("./data/menu_textures/confirmSelectionHover.png");
			static_cast<TouchMenuButton*>(touchOBJSelectionMenu->getMenuButton(touchButtonID4))->setButtonUseTextureActive("./data/menu_textures/confirmSelectionActive.png");
			touchOBJSelectionMenu->getMenuButton(touchButtonID4)->triggerCallback = OBJModeIntersectionSingleSelection;

			touchOBJSelectionMenu->setHelpText(g_languageStringsReader->getString("upper_deselect"),
				g_languageStringsReader->getString("upper_select"),
				g_languageStringsReader->getString("upper_multiple_select"),
				g_languageStringsReader->getString("upper_more"));

			touchOBJSelectionMenu->initializeHelpText();

			touchOBJSelectionMenu->setFramesBeforeClose(50000);
			touchOBJSelectionMenu->setStartScaleValue(0.0f);
			touchOBJSelectionMenu->setTargetScaleValue(0.022f);
			touchOBJSelectionMenu->initializeDrawables();
			touchOBJSelectionMenu->attachToSceneGraph(g_interactionManager->getPrimaryController()->getMatrixTransformNotShadowed());
		}

		{
			TouchMenu *touchMenu = new TouchMenu(TOUCH_EDIMODE_SELECTION, touchMenuFloatingHeight, defaultTouchMenuColor, highlightTouchMenuColor, CONTROLLER_TYPE::PRIMARY);
			touchMenu->getMenuTransform()->setName("PosAttTra-TOUCH_EDIMODE_SELECTION-Menu");
			g_menuManager->addMenuForUniqueName(touchMenu);

			BUTTON_ID touchButtonID1 = touchMenu->createButton("1");
			static_cast<TouchMenuButton*>(touchMenu->getMenuButton(touchButtonID1))->setButtonUseTexture("./data/menu_textures/changeSelectionSphereSize.png");
			static_cast<TouchMenuButton*>(touchMenu->getMenuButton(touchButtonID1))->setButtonUseTextureHover("./data/menu_textures/changeSelectionSphereSizeHover.png");
			static_cast<TouchMenuButton*>(touchMenu->getMenuButton(touchButtonID1))->setButtonUseTextureActive("./data/menu_textures/changeSelectionSphereSizeActive.png");
			static_cast<TouchMenuButton*>(touchMenu->getMenuButton(touchButtonID1))->allowConstantTriggering();
			touchMenu->getMenuButton(touchButtonID1)->triggerCallback = emptyCallback;

			BUTTON_ID touchButtonID2 = touchMenu->createButton("2");
			static_cast<TouchMenuButton*>(touchMenu->getMenuButton(touchButtonID2))->setButtonUseTexture("./data/menu_textures/negateSelection.png");
			static_cast<TouchMenuButton*>(touchMenu->getMenuButton(touchButtonID2))->setButtonUseTextureHover("./data/menu_textures/negateSelectionHover.png");
			static_cast<TouchMenuButton*>(touchMenu->getMenuButton(touchButtonID2))->setButtonUseTextureActive("./data/menu_textures/negateSelectionActive.png");
			static_cast<TouchMenuButton*>(touchMenu->getMenuButton(touchButtonID2))->setHighlightActiveFromExternalBool(g_miscDataStorage->getAddressOfIsEDISelectionNegateToolActive());
			touchMenu->getMenuButton(touchButtonID2)->allowConstantTriggering();
			touchMenu->getMenuButton(touchButtonID2)->triggerCallback = vertSelectMenuDeselectVertices;

			BUTTON_ID touchButtonID3 = touchMenu->createButton("3");
			static_cast<TouchMenuButton*>(touchMenu->getMenuButton(touchButtonID3))->setButtonUseTexture("./data/menu_textures/fenceMenuIcon.png");
			static_cast<TouchMenuButton*>(touchMenu->getMenuButton(touchButtonID3))->setButtonUseTextureHover("./data/menu_textures/fenceMenuIconHover.png");
			static_cast<TouchMenuButton*>(touchMenu->getMenuButton(touchButtonID3))->setButtonUseTextureActive("./data/menu_textures/fenceMenuIconActive.png");
			touchMenu->getMenuButton(touchButtonID3)->triggerCallback = showEDIModeDetailCommandFenceMenu;

			BUTTON_ID touchButtonID4 = touchMenu->createButton("4");
			static_cast<TouchMenuButton*>(touchMenu->getMenuButton(touchButtonID4))->setButtonUseTexture("./data/menu_textures/confirmSelection.png");
			static_cast<TouchMenuButton*>(touchMenu->getMenuButton(touchButtonID4))->setButtonUseTextureHover("./data/menu_textures/confirmSelectionHover.png");
			static_cast<TouchMenuButton*>(touchMenu->getMenuButton(touchButtonID4))->setButtonUseTextureActive("./data/menu_textures/confirmSelectionActive.png");
			static_cast<TouchMenuButton*>(touchMenu->getMenuButton(touchButtonID4))->setHighlightActiveFromExternalBool(g_miscDataStorage->getAddressOfIsEDISelectionConfirmToolActive());
			touchMenu->getMenuButton(touchButtonID4)->allowConstantTriggering();
			touchMenu->getMenuButton(touchButtonID4)->triggerCallback = vertSelectMenuSelectVertices;

			touchMenu->setHelpText(g_languageStringsReader->getString("upper_deselect"),
				g_languageStringsReader->getString("upper_select"),
				g_languageStringsReader->getString("upper_change_sphere_size"),
				g_languageStringsReader->getString("upper_more"));

			touchMenu->initializeHelpText();

			touchMenu->setFramesBeforeClose(50000);
			touchMenu->setStartScaleValue(0.0f);
			touchMenu->setTargetScaleValue(0.022f);
			touchMenu->initializeDrawables();
			touchMenu->attachToSceneGraph(g_interactionManager->getPrimaryController()->getMatrixTransformNotShadowed());
		}

		{
			DartMenu *secDartOBJModeTools = new DartMenu(g_configReader, DART_OBJMODE_TOOLS, defaultMenuColor, highlightMenuColor, menuPointerColor, CONTROLLER_TYPE::SECONDARY, dartMenuTextSize);
			secDartOBJModeTools->getMenuTransform()->setName("PosAttTra-DART_OBJMODE_TOOLS-Menu");
			g_menuManager->addMenuForUniqueName(secDartOBJModeTools);

			BUTTON_ID buttonID1 = secDartOBJModeTools->createButton(g_languageStringsReader->getString("transform_tool"));
			static_cast<PieMenuButton*>(secDartOBJModeTools->getMenuButton(buttonID1))->markAsActive(false);
			secDartOBJModeTools->getMenuButton(buttonID1)->triggerCallback = activateTransRotScaleToolOBJMode;

			BUTTON_ID buttonID2 = secDartOBJModeTools->createButton(g_languageStringsReader->getString("selection_tool"));
			secDartOBJModeTools->getMenuButton(buttonID2)->triggerCallback = activateOBJModeSelectionTool;

			BUTTON_ID buttonID3 = secDartOBJModeTools->createButton("");
			static_cast<PieMenuButton*>(secDartOBJModeTools->getMenuButton(buttonID3))->markAsActive(false);

			BUTTON_ID buttonID4 = secDartOBJModeTools->createButton(g_languageStringsReader->getString("close"));
			secDartOBJModeTools->getMenuButton(buttonID4)->triggerCallback = activateMenuStructureForObjectMode;

			BUTTON_ID buttonID5 = secDartOBJModeTools->createButton(g_languageStringsReader->getString("save_mesh"));
			secDartOBJModeTools->getMenuButton(buttonID5)->triggerCallback = saveCurrentGeometryAsOBJ;

			BUTTON_ID buttonID6 = secDartOBJModeTools->createButton(g_languageStringsReader->getString("load_mesh"));
			secDartOBJModeTools->getMenuButton(buttonID6)->triggerCallback = showAddMeshOBJModeMenu;

			secDartOBJModeTools->setFramesBeforeClose(50000);
			secDartOBJModeTools->setStartScaleValue(0.0f);
			secDartOBJModeTools->setTargetScaleValue(0.5f);
			secDartOBJModeTools->initializeDrawables();
			secDartOBJModeTools->attachToSceneGraph(g_sceneRoot);
		}

		{
			DartMenu *secDartOBJModeTools = new DartMenu(g_configReader, DART_EDIMODE_TOOLS, defaultMenuColor, highlightMenuColor, menuPointerColor, CONTROLLER_TYPE::SECONDARY, dartMenuTextSize);
			secDartOBJModeTools->getMenuTransform()->setName("PosAttTra-DART_EDIMODE_TOOLS-Menu");
			g_menuManager->addMenuForUniqueName(secDartOBJModeTools);

			BUTTON_ID buttonID1 = secDartOBJModeTools->createButton(g_languageStringsReader->getString("transform_tool"));
			secDartOBJModeTools->getMenuButton(buttonID1)->triggerCallback = activateTransRotScaleToolEDIMode;

			BUTTON_ID buttonID2 = secDartOBJModeTools->createButton(g_languageStringsReader->getString("selection_tool"));
			secDartOBJModeTools->getMenuButton(buttonID2)->triggerCallback = activateEDIModeSelectionTool;

			BUTTON_ID buttonID3 = secDartOBJModeTools->createButton("");
			static_cast<PieMenuButton*>(secDartOBJModeTools->getMenuButton(buttonID3))->markAsActive(false);

			BUTTON_ID buttonID4 = secDartOBJModeTools->createButton(g_languageStringsReader->getString("close"));
			secDartOBJModeTools->getMenuButton(buttonID4)->triggerCallback = activateMenuStructureForEditMode;

			BUTTON_ID buttonID5 = secDartOBJModeTools->createButton(g_languageStringsReader->getString("prop_edit_tool"));
			static_cast<PieMenuButton*>(secDartOBJModeTools->getMenuButton(buttonID5))->markAsActive(false);
			secDartOBJModeTools->getMenuButton(buttonID5)->triggerCallback = registerPropEditObserver;

			BUTTON_ID buttonID6 = secDartOBJModeTools->createButton(g_languageStringsReader->getString("add_mesh_element"));
			secDartOBJModeTools->getMenuButton(buttonID6)->triggerCallback = activateAddMeshElementTool;

			secDartOBJModeTools->setFramesBeforeClose(50000);
			secDartOBJModeTools->setStartScaleValue(0.0f);
			secDartOBJModeTools->setTargetScaleValue(0.5f);
			secDartOBJModeTools->initializeDrawables();
			secDartOBJModeTools->attachToSceneGraph(g_sceneRoot);
		}

		{
			FenceMenu *menu = new FenceMenu(g_configReader, FENCE_OBJMODE_ADD_MESH, defaultMenuColor, highlightMenuColor, menuPointerColor, CONTROLLER_TYPE::SECONDARY);
			menu->getMenuTransform()->setName("PosAttTra-FENCE_OBJMODE_ADD_MESH-Menu");
			g_menuManager->addMenuForUniqueName(menu);

			BUTTON_ID buttonID5 = menu->createButton(g_languageStringsReader->getString("low_res_monkey"));
			menu->getMenuButton(buttonID5)->triggerCallback = loadLowResMonkeyModel;

			BUTTON_ID buttonID7 = menu->createButton(g_languageStringsReader->getString("high_res_monkey"));
			menu->getMenuButton(buttonID7)->triggerCallback = loadHighResMonkeyModel;

			BUTTON_ID buttonID8 = menu->createButton(g_languageStringsReader->getString("very_high_res_monkey"));
			menu->getMenuButton(buttonID8)->triggerCallback = loadVeryHighResMonkeyModel;

			BUTTON_ID buttonID9 = menu->createButton(g_languageStringsReader->getString("low_res_audi_r8"));
			menu->getMenuButton(buttonID9)->triggerCallback = loadR8LowPoly;

			BUTTON_ID buttonID10 = menu->createButton(g_languageStringsReader->getString("high_res_audi_r8"));
			menu->getMenuButton(buttonID10)->triggerCallback = loadR8HighPoly;

			BUTTON_ID buttonID11 = menu->createButton(g_languageStringsReader->getString("f1_chassis_front"));
			menu->getMenuButton(buttonID11)->triggerCallback = loadF1Front;

			BUTTON_ID buttonID6 = menu->createButton(g_languageStringsReader->getString("close"));
			menu->getMenuButton(buttonID6)->triggerCallback = activateObserversForMenus;
			menu->closeCallback = closeFenceMenu;

			menu->setStartScaleValue(0.0f);
			menu->setTargetScaleValue(0.2f);
			menu->initializeDrawables();
			menu->attachToSceneGraph(g_sceneRoot);
		}

		{
			FenceMenu *menu = new FenceMenu(g_configReader, FENCE_EDIMODE_ADD_PRIMITIVE, defaultMenuColor, highlightMenuColor, menuPointerColor, CONTROLLER_TYPE::SECONDARY, 0.13, 0.15, 0.9);
			menu->getMenuTransform()->setName("PosAttTra-FENCE_EDIMODE_ADD_PRIMITIVE-Menu");
			g_menuManager->addMenuForUniqueName(menu);

			BUTTON_ID buttonID1 = menu->createButton(g_languageStringsReader->getString("cube"));
			menu->getMenuButton(buttonID1)->triggerCallback = loadCube;

			BUTTON_ID buttonID2 = menu->createButton(g_languageStringsReader->getString("cone"));
			menu->getMenuButton(buttonID2)->triggerCallback = loadCone;

			BUTTON_ID buttonID3 = menu->createButton(g_languageStringsReader->getString("sphere"));
			menu->getMenuButton(buttonID3)->triggerCallback = loadSphere;

			BUTTON_ID buttonID4 = menu->createButton(g_languageStringsReader->getString("cylinder"));
			menu->getMenuButton(buttonID4)->triggerCallback = loadCylinder;

			BUTTON_ID buttonID6 = menu->createButton(g_languageStringsReader->getString("close"));
			menu->getMenuButton(buttonID6)->triggerCallback = closeFenceMenu;
			menu->closeCallback = closeFenceMenu;

			menu->setStartScaleValue(0.0f);
			menu->setTargetScaleValue(0.2f);
			menu->initializeDrawables();
			menu->attachToSceneGraph(g_sceneRoot);
		}

		{
			FenceMenu* primFenceObjModeGeneralMenu = new FenceMenu(g_configReader, FENCE_GENERAL_COMMAND, defaultMenuColor, highlightMenuColor, menuPointerColor, CONTROLLER_TYPE::SECONDARY);
			primFenceObjModeGeneralMenu->getMenuTransform()->setName("PosAttTra-FENCE_GENERAL_COMMAND-Menu");
			g_menuManager->addMenuForUniqueName(primFenceObjModeGeneralMenu);

			BUTTON_ID buttonfenceID1 = primFenceObjModeGeneralMenu->createButton(g_languageStringsReader->getString("wireframe_on_off"));
			primFenceObjModeGeneralMenu->getMenuButton(buttonfenceID1)->triggerCallback = toggleWireframe;

			BUTTON_ID buttonfenceID11 = primFenceObjModeGeneralMenu->createButton(g_languageStringsReader->getString("show_help_text_on_off"));
			primFenceObjModeGeneralMenu->getMenuButton(buttonfenceID11)->triggerCallback = toggleHelp;

			BUTTON_ID buttonfenceID2 = primFenceObjModeGeneralMenu->createButton(g_languageStringsReader->getString("ruler"));
			primFenceObjModeGeneralMenu->getMenuButton(buttonfenceID2)->triggerCallback = registerRulerObserver;

			BUTTON_ID buttonfenceID21 = primFenceObjModeGeneralMenu->createButton(g_languageStringsReader->getString("add_primitive"));
			primFenceObjModeGeneralMenu->getMenuButton(buttonfenceID21)->triggerCallback = showAddPrimitiveEDIModeMenu;

			BUTTON_ID buttonfenceID3 = primFenceObjModeGeneralMenu->createButton(g_languageStringsReader->getString("manage_scene"));
			static_cast<FenceMenuButton*>(primFenceObjModeGeneralMenu->getMenuButton(buttonfenceID3))->markAsActive(false);
			primFenceObjModeGeneralMenu->getMenuButton(buttonfenceID3)->triggerCallback = emptyCallback;

			BUTTON_ID buttonfenceID4 = primFenceObjModeGeneralMenu->createButton(g_languageStringsReader->getString("statistics"));
			static_cast<FenceMenuButton*>(primFenceObjModeGeneralMenu->getMenuButton(buttonfenceID4))->markAsActive(false);
			primFenceObjModeGeneralMenu->getMenuButton(buttonfenceID4)->triggerCallback = emptyCallback;

			BUTTON_ID buttonfenceID5 = primFenceObjModeGeneralMenu->createButton(g_languageStringsReader->getString("set_spectator_cam"));
			static_cast<FenceMenuButton*>(primFenceObjModeGeneralMenu->getMenuButton(buttonfenceID5))->markAsActive(false);
			primFenceObjModeGeneralMenu->getMenuButton(buttonfenceID5)->triggerCallback = registerSpectatorCamObserver;

			BUTTON_ID buttonfenceID6 = primFenceObjModeGeneralMenu->createButton(g_languageStringsReader->getString("close"));
			primFenceObjModeGeneralMenu->getMenuButton(buttonfenceID6)->triggerCallback = closeFenceMenu;
			primFenceObjModeGeneralMenu->closeCallback = closeFenceMenu;

			primFenceObjModeGeneralMenu->setStartScaleValue(0.0f);
			primFenceObjModeGeneralMenu->setTargetScaleValue(0.2);
			primFenceObjModeGeneralMenu->initializeDrawables();
			primFenceObjModeGeneralMenu->attachToSceneGraph(g_sceneRoot);
		}

		{
			FenceMenu* primObjectFence = new FenceMenu(g_configReader, FENCE_OBJMODE_DETAIL_COMMAND, defaultMenuColor, highlightMenuColor, menuPointerColor, CONTROLLER_TYPE::PRIMARY, 0.13, 0.15, 0.9);
			primObjectFence->getMenuTransform()->setName("PosAttTra-FENCE_OBJMODE_DETAIL_COMMAND-Menu");
			g_menuManager->addMenuForUniqueName(primObjectFence);

			BUTTON_ID buttonPOAFID2 = primObjectFence->createButton(g_languageStringsReader->getString("delete"));
			primObjectFence->getMenuButton(buttonPOAFID2)->triggerCallback = OSMCF_deleteSelectedObjects;

			BUTTON_ID buttonPOAFID3 = primObjectFence->createButton(g_languageStringsReader->getString("duplicate"));
			static_cast<FenceMenuButton*>(primObjectFence->getMenuButton(buttonPOAFID3))->markAsActive(false);
			primObjectFence->getMenuButton(buttonPOAFID3)->triggerCallback = OSMCF_duplicateSelectedObjects;

			BUTTON_ID buttonfenceID4 = primObjectFence->createButton(g_languageStringsReader->getString("join"));
			primObjectFence->getMenuButton(buttonfenceID4)->triggerCallback = OSMCF_joinSelectedObjects;

			BUTTON_ID buttonfenceID5 = primObjectFence->createButton(g_languageStringsReader->getString("subdivide"));
			static_cast<FenceMenuButton*>(primObjectFence->getMenuButton(buttonfenceID5))->markAsActive(false);
			primObjectFence->getMenuButton(buttonfenceID5)->triggerCallback = subdivide;

			BUTTON_ID buttonfenceID6 = primObjectFence->createButton(g_languageStringsReader->getString("close"));
			primObjectFence->getMenuButton(buttonfenceID6)->triggerCallback = closeFenceMenu;
			primObjectFence->closeCallback = closeFenceMenu;

			primObjectFence->setStartScaleValue(0.0f);
			primObjectFence->setTargetScaleValue(0.2);
			primObjectFence->initializeDrawables();
			primObjectFence->attachToSceneGraph(g_sceneRoot);
		}

		{
			FenceMenu* primObjectFence = new FenceMenu(g_configReader, FENCE_EDIMODE_DETAIL_COMMAND,
				editModeContextDefaultFenceMenuColor,
				editModeContextHighlightFenceMenuColor,
				menuPointerColor,
				CONTROLLER_TYPE::PRIMARY, 0.13, 0.15, 0.9);
			primObjectFence->getMenuTransform()->setName("PosAttTra-FENCE_EDIMODE_DETAIL_COMMAND-Menu");
			g_menuManager->addMenuForUniqueName(primObjectFence);

			BUTTON_ID buttonPOAFID1 = primObjectFence->createButton(g_languageStringsReader->getString("extrude"));
			primObjectFence->getMenuButton(buttonPOAFID1)->triggerCallback = registerExtrudeVertices;

			BUTTON_ID buttonPOAFID2 = primObjectFence->createButton(g_languageStringsReader->getString("delete"));
			primObjectFence->getMenuButton(buttonPOAFID2)->triggerCallback = deleteSelectedMeshElement;

			BUTTON_ID buttonPOAFID3 = primObjectFence->createButton(g_languageStringsReader->getString("de_select_all"));
			primObjectFence->getMenuButton(buttonPOAFID3)->triggerCallback = toggleSelectOrDeselectAllVertices;

			BUTTON_ID buttonfenceID4 = primObjectFence->createButton(g_languageStringsReader->getString("duplicate"));
			primObjectFence->getMenuButton(buttonfenceID4)->triggerCallback = duplicateSelectedMeshArea;

			BUTTON_ID buttonfenceID5 = primObjectFence->createButton(g_languageStringsReader->getString("close"));
			primObjectFence->getMenuButton(buttonfenceID5)->triggerCallback = closeFenceMenu;
			primObjectFence->closeCallback = closeFenceMenu;

			primObjectFence->setStartScaleValue(0.0f);
			primObjectFence->setTargetScaleValue(0.2);
			primObjectFence->initializeDrawables();
			primObjectFence->attachToSceneGraph(g_sceneRoot);
		}

		
		{
			TouchMenu *touchMenu = new TouchMenu(TOUCH_CONSTRAINT, touchMenuFloatingHeight, defaultTouchMenuColor, highlightTouchMenuColor, CONTROLLER_TYPE::PRIMARY);
			touchMenu->getMenuTransform()->setName("PosAttTra-TOUCH_CONSTRAINT-Menu");
			g_menuManager->addMenuForUniqueName(touchMenu);

			BUTTON_ID touchButtonID1 = touchMenu->createButton("1");
			static_cast<TouchMenuButton*>(touchMenu->getMenuButton(touchButtonID1))->setButtonUseTexture("./data/menu_textures/constraint_snap.png");
			static_cast<TouchMenuButton*>(touchMenu->getMenuButton(touchButtonID1))->setButtonUseTextureHover("./data/menu_textures/constraint_snapHover.png");
			static_cast<TouchMenuButton*>(touchMenu->getMenuButton(touchButtonID1))->setButtonUseTextureActive("./data/menu_textures/constraint_snapActive.png");
			static_cast<TouchMenuButton*>(touchMenu->getMenuButton(touchButtonID1))->setHighlightActiveFromExternalBool(g_geometryManager->getBoolAdressOfSnapping());
			touchMenu->getMenuButton(touchButtonID1)->triggerCallback = toggleConstraintSnapping;

			BUTTON_ID touchButtonID2 = touchMenu->createButton("2");
			static_cast<TouchMenuButton*>(touchMenu->getMenuButton(touchButtonID2))->setButtonUseTexture("./data/menu_textures/constraint_z.png");
			static_cast<TouchMenuButton*>(touchMenu->getMenuButton(touchButtonID2))->setButtonUseTextureHover("./data/menu_textures/constraint_zHover.png");
			static_cast<TouchMenuButton*>(touchMenu->getMenuButton(touchButtonID2))->setButtonUseTextureActive("./data/menu_textures/constraint_zActive.png");
			static_cast<TouchMenuButton*>(touchMenu->getMenuButton(touchButtonID2))->setHighlightActiveFromExternalBool(g_geometryManager->getBoolAddressFromConstraintZ());
			touchMenu->getMenuButton(touchButtonID2)->triggerCallback = toggleConstraintZ;

			BUTTON_ID touchButtonID3 = touchMenu->createButton("3");
			static_cast<TouchMenuButton*>(touchMenu->getMenuButton(touchButtonID3))->setButtonUseTexture("./data/menu_textures/constraint_y.png");
			static_cast<TouchMenuButton*>(touchMenu->getMenuButton(touchButtonID3))->setButtonUseTextureHover("./data/menu_textures/constraint_yHover.png");
			static_cast<TouchMenuButton*>(touchMenu->getMenuButton(touchButtonID3))->setButtonUseTextureActive("./data/menu_textures/constraint_yActive.png");
			static_cast<TouchMenuButton*>(touchMenu->getMenuButton(touchButtonID3))->setHighlightActiveFromExternalBool(g_geometryManager->getBoolAddressFromConstraintY());
			touchMenu->getMenuButton(touchButtonID3)->triggerCallback = toggleConstraintY;

			BUTTON_ID touchButtonID4 = touchMenu->createButton("4");
			static_cast<TouchMenuButton*>(touchMenu->getMenuButton(touchButtonID4))->setButtonUseTexture("./data/menu_textures/constraint_x.png");
			static_cast<TouchMenuButton*>(touchMenu->getMenuButton(touchButtonID4))->setButtonUseTextureHover("./data/menu_textures/constraint_xHover.png");
			static_cast<TouchMenuButton*>(touchMenu->getMenuButton(touchButtonID4))->setButtonUseTextureActive("./data/menu_textures/constraint_xActive.png");
			static_cast<TouchMenuButton*>(touchMenu->getMenuButton(touchButtonID4))->setHighlightActiveFromExternalBool(g_geometryManager->getBoolAddressFromConstraintX());
			touchMenu->getMenuButton(touchButtonID4)->triggerCallback = toggleConstraintX;


			touchMenu->setHelpText(g_languageStringsReader->getString("upper_constraint_z"),
				g_languageStringsReader->getString("upper_constraint_x"),
				g_languageStringsReader->getString("upper_constraint_snap"),
				g_languageStringsReader->getString("upper_constraint_y"));

			touchMenu->initializeHelpText();

			touchMenu->setFramesBeforeClose(50000);
			touchMenu->setStartScaleValue(0.0f);
			touchMenu->setTargetScaleValue(0.022f);
			touchMenu->initializeDrawables();
			touchMenu->attachToSceneGraph(g_interactionManager->getPrimaryController()->getMatrixTransformNotShadowed());
		}

		{
			TouchMenu *touchMenu = new TouchMenu(TOUCH_EDIMODE_PROBEDIT, touchMenuFloatingHeight, defaultTouchMenuColor, highlightTouchMenuColor, CONTROLLER_TYPE::PRIMARY);
			touchMenu->getMenuTransform()->setName("PosAttTra-TOUCH_EDIMODE_PROBEDIT-Menu");
			g_menuManager->addMenuForUniqueName(touchMenu);

			BUTTON_ID touchButtonID1 = touchMenu->createButton("1");
			static_cast<TouchMenuButton*>(touchMenu->getMenuButton(touchButtonID1))->setButtonUseTexture("./data/menu_textures/changeSelectionSphereSize.png");
			static_cast<TouchMenuButton*>(touchMenu->getMenuButton(touchButtonID1))->setButtonUseTextureHover("./data/menu_textures/changeSelectionSphereSizeHover.png");
			static_cast<TouchMenuButton*>(touchMenu->getMenuButton(touchButtonID1))->setButtonUseTextureActive("./data/menu_textures/changeSelectionSphereSizeActive.png");
			static_cast<TouchMenuButton*>(touchMenu->getMenuButton(touchButtonID1))->allowConstantTriggering();
			touchMenu->getMenuButton(touchButtonID1)->triggerCallback = emptyCallback;

			BUTTON_ID touchButtonID2 = touchMenu->createButton("2");
			static_cast<TouchMenuButton*>(touchMenu->getMenuButton(touchButtonID2))->setButtonUseTexture("./data/menu_textures/negateSelection.png");
			static_cast<TouchMenuButton*>(touchMenu->getMenuButton(touchButtonID2))->setButtonUseTextureHover("./data/menu_textures/negateSelectionHover.png");
			static_cast<TouchMenuButton*>(touchMenu->getMenuButton(touchButtonID2))->setButtonUseTextureActive("./data/menu_textures/negateSelectionActive.png");
			touchMenu->getMenuButton(touchButtonID2)->allowConstantTriggering();
			touchMenu->getMenuButton(touchButtonID2)->triggerCallback = vertSelectMenuDeselectVertices;

			BUTTON_ID touchButtonID3 = touchMenu->createButton("3");
			static_cast<TouchMenuButton*>(touchMenu->getMenuButton(touchButtonID3))->setButtonUseTexture("./data/menu_textures/fenceMenuIcon.png");
			static_cast<TouchMenuButton*>(touchMenu->getMenuButton(touchButtonID3))->setButtonUseTextureHover("./data/menu_textures/fenceMenuIconHover.png");
			static_cast<TouchMenuButton*>(touchMenu->getMenuButton(touchButtonID3))->setButtonUseTextureActive("./data/menu_textures/fenceMenuIconActive.png");
			touchMenu->getMenuButton(touchButtonID3)->triggerCallback = showEDIModeDetailCommandFenceMenu;

			BUTTON_ID touchButtonID4 = touchMenu->createButton("4");
			static_cast<TouchMenuButton*>(touchMenu->getMenuButton(touchButtonID4))->setButtonUseTexture("./data/menu_textures/confirmSelection.png");
			static_cast<TouchMenuButton*>(touchMenu->getMenuButton(touchButtonID4))->setButtonUseTextureHover("./data/menu_textures/confirmSelectionHover.png");
			static_cast<TouchMenuButton*>(touchMenu->getMenuButton(touchButtonID4))->setButtonUseTextureActive("./data/menu_textures/confirmSelectionActive.png");
			touchMenu->getMenuButton(touchButtonID4)->allowConstantTriggering();
			touchMenu->getMenuButton(touchButtonID4)->triggerCallback = vertSelectMenuSelectVertices;

			touchMenu->setHelpText(g_languageStringsReader->getString("upper_deselect"),
				g_languageStringsReader->getString("upper_select"),
				g_languageStringsReader->getString("upper_change_sphere_size"),
				g_languageStringsReader->getString("upper_more"));

			touchMenu->initializeHelpText();

			touchMenu->setFramesBeforeClose(50000);
			touchMenu->setStartScaleValue(0.0f);
			touchMenu->setTargetScaleValue(0.022f);
			touchMenu->initializeDrawables();
			touchMenu->attachToSceneGraph(g_interactionManager->getPrimaryController()->getMatrixTransformNotShadowed());
		}

		{
			TouchMenu *touchMenu = new TouchMenu(TOUCH_EDIMODE_ADD_MESH_ELEMENTS, touchMenuFloatingHeight, defaultTouchMenuColor, highlightTouchMenuColor, CONTROLLER_TYPE::PRIMARY);
			touchMenu->getMenuTransform()->setName("PosAttTra-TOUCH_EDIMODE_ADD_MESH_ELEMENTS-Menu");
			g_menuManager->addMenuForUniqueName(touchMenu);

			//Old Menu Version with Adding an edge line
			//BUTTON_ID touchButtonID1 = touchMenu->createButton("1");
			//static_cast<TouchMenuButton*>(touchMenu->getMenuButton(touchButtonID1))->setButtonUseTexture("./data/menu_textures/addEdgeLineIcon.png");
			//static_cast<TouchMenuButton*>(touchMenu->getMenuButton(touchButtonID1))->setButtonUseTextureHover("./data/menu_textures/addEdgeLineIconHover.png");
			//static_cast<TouchMenuButton*>(touchMenu->getMenuButton(touchButtonID1))->setButtonUseTextureActive("./data/menu_textures/addEdgeLineIconActive.png");
			//touchMenu->getMenuButton(touchButtonID1)->triggerCallback = registerAddEdgeLineObserver;

			BUTTON_ID touchButtonID1 = touchMenu->createButton("1");
			static_cast<TouchMenuButton*>(touchMenu->getMenuButton(touchButtonID1))->setButtonUseTexture("./data/menu_textures/changeSelectionSphereSize.png");
			static_cast<TouchMenuButton*>(touchMenu->getMenuButton(touchButtonID1))->setButtonUseTextureHover("./data/menu_textures/changeSelectionSphereSizeHover.png");
			static_cast<TouchMenuButton*>(touchMenu->getMenuButton(touchButtonID1))->setButtonUseTextureActive("./data/menu_textures/changeSelectionSphereSizeActive.png");
			static_cast<TouchMenuButton*>(touchMenu->getMenuButton(touchButtonID1))->allowConstantTriggering();
			touchMenu->getMenuButton(touchButtonID1)->triggerCallback = emptyCallback;

			BUTTON_ID touchButtonID2 = touchMenu->createButton("2");
			static_cast<TouchMenuButton*>(touchMenu->getMenuButton(touchButtonID2))->setButtonUseTexture("./data/menu_textures/meshElementConnectorIcon.png");
			static_cast<TouchMenuButton*>(touchMenu->getMenuButton(touchButtonID2))->setButtonUseTextureHover("./data/menu_textures/meshElementConnectorIconHover.png");
			static_cast<TouchMenuButton*>(touchMenu->getMenuButton(touchButtonID2))->setButtonUseTextureActive("./data/menu_textures/meshElementConnectorIconActive.png");
			static_cast<TouchMenuButton*>(touchMenu->getMenuButton(touchButtonID2))->setHighlightActiveFromExternalBool(g_miscDataStorage->getAddressOfIsEDIConnectMeshElementToolActive());
			touchMenu->getMenuButton(touchButtonID2)->triggerCallback = registerConnectMeshElementObserver;

			BUTTON_ID touchButtonID3 = touchMenu->createButton("3");
			static_cast<TouchMenuButton*>(touchMenu->getMenuButton(touchButtonID3))->setButtonUseTexture("./data/menu_textures/fenceMenuIcon.png");
			static_cast<TouchMenuButton*>(touchMenu->getMenuButton(touchButtonID3))->setButtonUseTextureHover("./data/menu_textures/fenceMenuIconHover.png");
			static_cast<TouchMenuButton*>(touchMenu->getMenuButton(touchButtonID3))->setButtonUseTextureActive("./data/menu_textures/fenceMenuIconActive.png");
			touchMenu->getMenuButton(touchButtonID3)->triggerCallback = showEDIModeDetailCommandFenceMenu;

			BUTTON_ID touchButtonID4 = touchMenu->createButton("4");
			static_cast<TouchMenuButton*>(touchMenu->getMenuButton(touchButtonID4))->setButtonUseTexture("./data/menu_textures/addVertsIcon.png");
			static_cast<TouchMenuButton*>(touchMenu->getMenuButton(touchButtonID4))->setButtonUseTextureHover("./data/menu_textures/addVertsIconHover.png");
			static_cast<TouchMenuButton*>(touchMenu->getMenuButton(touchButtonID4))->setButtonUseTextureActive("./data/menu_textures/addVertsIconActive.png");
			static_cast<TouchMenuButton*>(touchMenu->getMenuButton(touchButtonID4))->setHighlightActiveFromExternalBool(g_miscDataStorage->getAddressOfIsEDIAddVertsToolActive());
			touchMenu->getMenuButton(touchButtonID4)->triggerCallback = registerAddVertsObserver;

			//TODO: Make the order more clear to the developer...
			touchMenu->setHelpText(g_languageStringsReader->getString("connect_3_points_to_a_face"),
				g_languageStringsReader->getString("add_isolated_points"),
				g_languageStringsReader->getString("upper_change_sphere_size"),
				g_languageStringsReader->getString("upper_more"));

			touchMenu->initializeHelpText();

			touchMenu->setFramesBeforeClose(50000);
			touchMenu->setStartScaleValue(0.0f);
			touchMenu->setTargetScaleValue(0.022f);
			touchMenu->initializeDrawables();
			touchMenu->attachToSceneGraph(g_interactionManager->getPrimaryController()->getMatrixTransformNotShadowed());
		}

		{
			FenceMenu* primDeleteQuestionFence = new FenceMenu(g_configReader,
				FENCE_DELETE_MESH_ELEMENT_QUESTION_COMMAND,
				defaultMenuColor,
				highlightMenuColor,
				menuPointerColor,
				CONTROLLER_TYPE::PRIMARY, 0.13, 0.15, 0.7);
			primDeleteQuestionFence->getMenuTransform()->setName("PosAttTra-FENCE_DELETE_MESH_ELEMENT_QUESTION_COMMAND-Menu");
			g_menuManager->addMenuForUniqueName(primDeleteQuestionFence);

			BUTTON_ID buttonPOAFID1 = primDeleteQuestionFence->createButton(g_languageStringsReader->getString("vertices"));
			primDeleteQuestionFence->getMenuButton(buttonPOAFID1)->triggerCallback = deleteSelectedVertices;

			BUTTON_ID buttonPOAFID2 = primDeleteQuestionFence->createButton(g_languageStringsReader->getString("faces"));
			primDeleteQuestionFence->getMenuButton(buttonPOAFID2)->triggerCallback = deleteSelectedFaces;

			BUTTON_ID buttonfenceID5 = primDeleteQuestionFence->createButton(g_languageStringsReader->getString("close"));
			primDeleteQuestionFence->getMenuButton(buttonfenceID5)->triggerCallback = closeFenceMenu;
			primDeleteQuestionFence->closeCallback = closeFenceMenu;

			primDeleteQuestionFence->setStartScaleValue(0.0f);
			primDeleteQuestionFence->setTargetScaleValue(0.2);
			primDeleteQuestionFence->initializeDrawables();
			primDeleteQuestionFence->attachToSceneGraph(g_sceneRoot);

			//######################################################################################################
			//Mode Menu Observer
			if (g_configReader->getStringFromStartupConfig("easy_mode_for_tilt_menu") == "true")
				g_isEasyModeForTiltMenuActivated = true;
			else
				g_isEasyModeForTiltMenuActivated = false;

			TiltMenuObserver* tiltMenuObserver = new TiltMenuObserver(TILT_AND_SHIFT_MODE, g_isEasyModeForTiltMenuActivated);
			g_interactionManager->registerObserver(CONTROLLER_TYPE::SECONDARY, Observer::MODE_MENU, tiltMenuObserver);
			tiltMenuObserver->setGeometryManager(g_geometryManager);
			tiltMenuObserver->setMenuManager(g_menuManager);
			tiltMenuObserver->setMiscDataStorage(g_miscDataStorage);
		}





		//3D SCANNING MENUS
		{
			TiltMenu* tiltScanModeMenu = new TiltMenu(g_configReader, TILT_AND_SHIFT_SCAN_MODE, defaultMenuColor, highlightMenuColor, menuPointerColor, CONTROLLER_TYPE::SECONDARY);
			tiltScanModeMenu->getMenuTransform()->setName("PosAttTra-TILT_AND_SHIFT_SCAN_MODE-Menu");
			g_menuManager->addMenuForUniqueName(tiltScanModeMenu);

			BUTTON_ID buttonTiltShiftModeID1 = tiltScanModeMenu->createButton(g_languageStringsReader->getString("point_cloud"));
			tiltScanModeMenu->getMenuButton(buttonTiltShiftModeID1)->triggerCallback = activateScanContextPointCloud;

			BUTTON_ID buttonTiltShiftModeID2 = tiltScanModeMenu->createButton(g_languageStringsReader->getString("polygonize"));
			tiltScanModeMenu->getMenuButton(buttonTiltShiftModeID2)->triggerCallback = activateScanContextPolygonize;

			BUTTON_ID buttonTiltShiftModeID3 = tiltScanModeMenu->createButton("");
			static_cast<TiltMenuButton*>(tiltScanModeMenu->getMenuButton(buttonTiltShiftModeID3))->markAsSpacerButton(true);
			tiltScanModeMenu->getMenuButton(buttonTiltShiftModeID3)->triggerCallback = emptyCallback;

			BUTTON_ID buttonTiltShiftModeID4 = tiltScanModeMenu->createButton("");
			static_cast<TiltMenuButton*>(tiltScanModeMenu->getMenuButton(buttonTiltShiftModeID4))->markAsSpacerButton(true);
			tiltScanModeMenu->getMenuButton(buttonTiltShiftModeID4)->triggerCallback = emptyCallback;

			BUTTON_ID buttonTiltShiftModeID5 = tiltScanModeMenu->createButton("");
			static_cast<TiltMenuButton*>(tiltScanModeMenu->getMenuButton(buttonTiltShiftModeID5))->markAsSpacerButton(true);
			tiltScanModeMenu->getMenuButton(buttonTiltShiftModeID5)->triggerCallback = emptyCallback;

			BUTTON_ID buttonTiltShiftModeID6 = tiltScanModeMenu->createButton("");
			static_cast<TiltMenuButton*>(tiltScanModeMenu->getMenuButton(buttonTiltShiftModeID6))->markAsSpacerButton(true);
			tiltScanModeMenu->getMenuButton(buttonTiltShiftModeID6)->triggerCallback = emptyCallback;

			BUTTON_ID buttonTiltShiftModeID7 = tiltScanModeMenu->createButton("");
			static_cast<TiltMenuButton*>(tiltScanModeMenu->getMenuButton(buttonTiltShiftModeID7))->markAsSpacerButton(true);
			tiltScanModeMenu->getMenuButton(buttonTiltShiftModeID7)->triggerCallback = emptyCallback;

			BUTTON_ID buttonTiltShiftModeID8 = tiltScanModeMenu->createButton("");
			static_cast<TiltMenuButton*>(tiltScanModeMenu->getMenuButton(buttonTiltShiftModeID8))->markAsSpacerButton(true);
			tiltScanModeMenu->getMenuButton(buttonTiltShiftModeID8)->triggerCallback = emptyCallback;

			BUTTON_ID buttonTiltShiftModeID9 = tiltScanModeMenu->createButton("");
			static_cast<TiltMenuButton*>(tiltScanModeMenu->getMenuButton(buttonTiltShiftModeID9))->markAsSpacerButton(true);
			tiltScanModeMenu->getMenuButton(buttonTiltShiftModeID9)->triggerCallback = emptyCallback;

			BUTTON_ID buttonTiltShiftModeID10 = tiltScanModeMenu->createButton("");
			static_cast<TiltMenuButton*>(tiltScanModeMenu->getMenuButton(buttonTiltShiftModeID10))->markAsSpacerButton(true);
			tiltScanModeMenu->getMenuButton(buttonTiltShiftModeID10)->triggerCallback = emptyCallback;

			BUTTON_ID buttonTiltShiftModeID11 = tiltScanModeMenu->createButton(g_languageStringsReader->getString("octree"));
			tiltScanModeMenu->getMenuButton(buttonTiltShiftModeID11)->triggerCallback = activateScanContextOctree;

			tiltScanModeMenu->setFramesBeforeClose(50000);
			tiltScanModeMenu->setStartScaleValue(0.000f);
			tiltScanModeMenu->setTargetScaleValue(0.20f);
			tiltScanModeMenu->initializeDrawables();
			tiltScanModeMenu->attachToSceneGraph(g_sceneRoot);
		}

		{
			FenceMenu* primFenceScanModeGeneralMenu = new FenceMenu(g_configReader, FENCE_SCAN_GENERAL_COMMAND, defaultMenuColor, highlightMenuColor, menuPointerColor, CONTROLLER_TYPE::SECONDARY);
			primFenceScanModeGeneralMenu->getMenuTransform()->setName("PosAttTra-FENCE_SCAN_GENERAL_COMMAND-Menu");
			primFenceScanModeGeneralMenu->closeCallback = activateObserversForMenus;
			g_menuManager->addMenuForUniqueName(primFenceScanModeGeneralMenu);
			
			BUTTON_ID buttonfenceID1 = primFenceScanModeGeneralMenu->createButton(g_languageStringsReader->getString("show_point_cloud_on_off"));
			primFenceScanModeGeneralMenu->getMenuButton(buttonfenceID1)->triggerCallback = togglePointCloudDisplay;

			BUTTON_ID buttonfenceID2 = primFenceScanModeGeneralMenu->createButton(g_languageStringsReader->getString("show_octree_on_off"));
			primFenceScanModeGeneralMenu->getMenuButton(buttonfenceID2)->triggerCallback = toggleOctreeDisplay;

			BUTTON_ID buttonfenceID3 = primFenceScanModeGeneralMenu->createButton(g_languageStringsReader->getString("show_dda_on_off"));
			primFenceScanModeGeneralMenu->getMenuButton(buttonfenceID3)->triggerCallback = toggleDDADisplay;

			BUTTON_ID buttonfenceID4 = primFenceScanModeGeneralMenu->createButton(g_languageStringsReader->getString("show_tsdf_on_off"));
			primFenceScanModeGeneralMenu->getMenuButton(buttonfenceID4)->triggerCallback = toggleTSDFDisplay;

			BUTTON_ID buttonfenceID5 = primFenceScanModeGeneralMenu->createButton(g_languageStringsReader->getString("show_polygonize_on_off"));
			primFenceScanModeGeneralMenu->getMenuButton(buttonfenceID5)->triggerCallback = togglePolygonizeDisplay;

			BUTTON_ID buttonfenceID6 = primFenceScanModeGeneralMenu->createButton(g_languageStringsReader->getString("show_help_text_on_off"));
			primFenceScanModeGeneralMenu->getMenuButton(buttonfenceID6)->triggerCallback = toggleHelpForScanContext;
			
			BUTTON_ID buttonfenceIDClose = primFenceScanModeGeneralMenu->createButton(g_languageStringsReader->getString("close"));
			primFenceScanModeGeneralMenu->getMenuButton(buttonfenceIDClose)->triggerCallback = activateObserversForMenus;

			primFenceScanModeGeneralMenu->setStartScaleValue(0.0f);
			primFenceScanModeGeneralMenu->setTargetScaleValue(0.2);
			primFenceScanModeGeneralMenu->initializeDrawables();
			primFenceScanModeGeneralMenu->attachToSceneGraph(g_sceneRoot);
		}

		//  Context: PointCloud
		{
			TouchMenu *touchMenu = new TouchMenu(TOUCH_SCAN_POINT_CLOUD, touchMenuFloatingHeight, defaultTouchMenuColor, highlightTouchMenuColor, CONTROLLER_TYPE::SECONDARY);
			touchMenu->getMenuTransform()->setName("PosAttTra-TOUCH_SCAN_POINT_CLOUD-Menu");
			g_menuManager->addMenuForUniqueName(touchMenu);

			BUTTON_ID touchButtonID1 = touchMenu->createButton("1");
			static_cast<TouchMenuButton*>(touchMenu->getMenuButton(touchButtonID1))->setButtonUseTexture("./data/menu_textures/toolMenuIcon.png");
			static_cast<TouchMenuButton*>(touchMenu->getMenuButton(touchButtonID1))->setButtonUseTextureHover("./data/menu_textures/toolMenuIconHover.png");
			static_cast<TouchMenuButton*>(touchMenu->getMenuButton(touchButtonID1))->setButtonUseTextureActive("./data/menu_textures/toolMenuIconActive.png");
			touchMenu->getMenuButton(touchButtonID1)->triggerCallback = openDartMenuForScanPointCloudContext;

			BUTTON_ID touchButtonID2 = touchMenu->createButton("2");
			static_cast<TouchMenuButton*>(touchMenu->getMenuButton(touchButtonID2))->setButtonUseTexture("./data/menu_textures/cubeIcon.png");
			static_cast<TouchMenuButton*>(touchMenu->getMenuButton(touchButtonID2))->setButtonUseTextureHover("./data/menu_textures/cubeIconHover.png");
			static_cast<TouchMenuButton*>(touchMenu->getMenuButton(touchButtonID2))->setButtonUseTextureActive("./data/menu_textures/cubeIconActive.png");
			touchMenu->getMenuButton(touchButtonID2)->triggerCallback = emptyCallback;

			BUTTON_ID touchButtonID3 = touchMenu->createButton("3");
			static_cast<TouchMenuButton*>(touchMenu->getMenuButton(touchButtonID3))->setButtonUseTexture("./data/menu_textures/fenceMenuIcon.png");
			static_cast<TouchMenuButton*>(touchMenu->getMenuButton(touchButtonID3))->setButtonUseTextureHover("./data/menu_textures/fenceMenuIconHover.png");
			static_cast<TouchMenuButton*>(touchMenu->getMenuButton(touchButtonID3))->setButtonUseTextureActive("./data/menu_textures/fenceMenuIconActive.png");
			touchMenu->getMenuButton(touchButtonID3)->triggerCallback = openFenceMenuForScanGeneralCommands;

			/*BUTTON_ID touchButtonID4 = touchMenu->createButton("4");
			static_cast<TouchMenuButton*>(touchMenu->getMenuButton(touchButtonID4))->setButtonUseTexture("./data/menu_textures/Undo.png");
			static_cast<TouchMenuButton*>(touchMenu->getMenuButton(touchButtonID4))->setButtonUseTextureHover("./data/menu_textures/UndoHover.png");
			static_cast<TouchMenuButton*>(touchMenu->getMenuButton(touchButtonID4))->setButtonUseTextureActive("./data/menu_textures/UndoActive.png");
			touchMenu->getMenuButton(touchButtonID4)->triggerCallback = pointCloudUndo;*/

			touchMenu->setHelpText(g_languageStringsReader->getString("upper_scan_mode_toggle_visibility"),
				g_languageStringsReader->getString("upper_undo"),
				g_languageStringsReader->getString("upper_tools"),
				g_languageStringsReader->getString("upper_settings"));
			touchMenu->initializeHelpText();

			touchMenu->setFramesBeforeClose(50000);
			touchMenu->setStartScaleValue(0.0f);
			touchMenu->setTargetScaleValue(0.022f);
			touchMenu->initializeDrawables();
			touchMenu->attachToSceneGraph(g_interactionManager->getSecondaryController()->getMatrixTransformNotShadowed());
		}

		{
			DartMenu* secDartScanTools = new DartMenu(g_configReader, DART_SCAN_POINT_CLOUD_TOOLS, defaultMenuColor, highlightMenuColor, menuPointerColor, CONTROLLER_TYPE::SECONDARY, dartMenuTextSize);
			secDartScanTools->getMenuTransform()->setName("PosAttTra-DART_SCAN_POINT_CLOUD_TOOLS-Menu");
			g_menuManager->addMenuForUniqueName(secDartScanTools);

			BUTTON_ID buttonID1 = secDartScanTools->createButton(g_languageStringsReader->getString("camera_calibration"));
			secDartScanTools->getMenuButton(buttonID1)->triggerCallback = calibrateCameras;

			BUTTON_ID buttonID2 = secDartScanTools->createButton(g_languageStringsReader->getString("clear_point_cloud"));
			secDartScanTools->getMenuButton(buttonID2)->triggerCallback = clearPointCloud;

			BUTTON_ID buttonIDclose = secDartScanTools->createButton(g_languageStringsReader->getString("close"));
			secDartScanTools->getMenuButton(buttonIDclose)->triggerCallback = activateObserversForMenus;

			secDartScanTools->setFramesBeforeClose(50000);
			secDartScanTools->setStartScaleValue(0.0f);
			secDartScanTools->setTargetScaleValue(0.5f);
			secDartScanTools->initializeDrawables();
			secDartScanTools->attachToSceneGraph(g_sceneRoot);
		}

		/*{
			FenceMenu* primObjectFence = new FenceMenu(g_configReader, FENCE_SCAN_POINT_CLOUD,
				editModeContextDefaultFenceMenuColor,
				editModeContextHighlightFenceMenuColor,
				menuPointerColor,
				CONTROLLER_TYPE::SECONDARY, 0.13, 0.15, 0.9);
			primObjectFence->getMenuTransform()->setName("PosAttTra-FENCE_SCAN_POINT_CLOUD-Menu");
			primObjectFence->closeCallback = activateObserversForMenus;
			g_menuManager->addMenuForUniqueName(primObjectFence);

			BUTTON_ID buttonPOAFID1 = primObjectFence->createButton("Test");
			primObjectFence->getMenuButton(buttonPOAFID1)->triggerCallback = damianDemo;

			BUTTON_ID buttonfenceID5 = primObjectFence->createButton(g_languageStringsReader->getString("close"));
			primObjectFence->getMenuButton(buttonfenceID5)->triggerCallback = activateObserversForMenus;
			
			primObjectFence->setStartScaleValue(0.0f);
			primObjectFence->setTargetScaleValue(0.2);
			primObjectFence->initializeDrawables();
			primObjectFence->attachToSceneGraph(g_sceneRoot);
		}*/

		//  Context: Octree
		{
			TouchMenu* touchMenu = new TouchMenu(TOUCH_SCAN_OCTREE, touchMenuFloatingHeight, defaultTouchMenuColor, highlightTouchMenuColor, CONTROLLER_TYPE::SECONDARY);
			touchMenu->getMenuTransform()->setName("PosAttTra-TOUCH_SCAN_OCTREE-Menu");
			g_menuManager->addMenuForUniqueName(touchMenu);

			BUTTON_ID touchButtonID1 = touchMenu->createButton("1");
			static_cast<TouchMenuButton*>(touchMenu->getMenuButton(touchButtonID1))->setButtonUseTexture("./data/menu_textures/toolMenuIcon.png");
			static_cast<TouchMenuButton*>(touchMenu->getMenuButton(touchButtonID1))->setButtonUseTextureHover("./data/menu_textures/toolMenuIconHover.png");
			static_cast<TouchMenuButton*>(touchMenu->getMenuButton(touchButtonID1))->setButtonUseTextureActive("./data/menu_textures/toolMenuIconActive.png");
			touchMenu->getMenuButton(touchButtonID1)->triggerCallback = openDartMenuForScanOctreeContext;

			BUTTON_ID touchButtonID2 = touchMenu->createButton("2");
			static_cast<TouchMenuButton*>(touchMenu->getMenuButton(touchButtonID2))->setButtonUseTexture("./data/menu_textures/cubeIcon.png");
			static_cast<TouchMenuButton*>(touchMenu->getMenuButton(touchButtonID2))->setButtonUseTextureHover("./data/menu_textures/cubeIconHover.png");
			static_cast<TouchMenuButton*>(touchMenu->getMenuButton(touchButtonID2))->setButtonUseTextureActive("./data/menu_textures/cubeIconActive.png");
			touchMenu->getMenuButton(touchButtonID2)->triggerCallback = emptyCallback;

			BUTTON_ID touchButtonID3 = touchMenu->createButton("3");
			static_cast<TouchMenuButton*>(touchMenu->getMenuButton(touchButtonID3))->setButtonUseTexture("./data/menu_textures/fenceMenuIcon.png");
			static_cast<TouchMenuButton*>(touchMenu->getMenuButton(touchButtonID3))->setButtonUseTextureHover("./data/menu_textures/fenceMenuIconHover.png");
			static_cast<TouchMenuButton*>(touchMenu->getMenuButton(touchButtonID3))->setButtonUseTextureActive("./data/menu_textures/fenceMenuIconActive.png");
			touchMenu->getMenuButton(touchButtonID3)->triggerCallback = openFenceMenuForScanGeneralCommands;

			/*BUTTON_ID touchButtonID4 = touchMenu->createButton("4");
			static_cast<TouchMenuButton*>(touchMenu->getMenuButton(touchButtonID4))->setButtonUseTexture("./data/menu_textures/Undo.png");
			static_cast<TouchMenuButton*>(touchMenu->getMenuButton(touchButtonID4))->setButtonUseTextureHover("./data/menu_textures/UndoHover.png");
			static_cast<TouchMenuButton*>(touchMenu->getMenuButton(touchButtonID4))->setButtonUseTextureActive("./data/menu_textures/UndoActive.png");
			touchMenu->getMenuButton(touchButtonID4)->triggerCallback = emptyCallback;
			touchMenu->getMenuButton(touchButtonID4)->markAsActive(false);*/

			touchMenu->setHelpText(g_languageStringsReader->getString("upper_scan_mode_toggle_visibility"),
				g_languageStringsReader->getString("upper_undo"),
				g_languageStringsReader->getString("upper_tools"),
				g_languageStringsReader->getString("upper_settings"));
			touchMenu->initializeHelpText();

			touchMenu->setFramesBeforeClose(50000);
			touchMenu->setStartScaleValue(0.0f);
			touchMenu->setTargetScaleValue(0.022f);
			touchMenu->initializeDrawables();
			touchMenu->attachToSceneGraph(g_interactionManager->getSecondaryController()->getMatrixTransformNotShadowed());
		}

		{
			DartMenu* secDartScanTools = new DartMenu(g_configReader, DART_SCAN_OCTREE_TOOLS, defaultMenuColor, highlightMenuColor, menuPointerColor, CONTROLLER_TYPE::SECONDARY, dartMenuTextSize);
			secDartScanTools->getMenuTransform()->setName("PosAttTra-DART_SCAN_OCTREE_TOOLS-Menu");
			g_menuManager->addMenuForUniqueName(secDartScanTools);

			BUTTON_ID buttonID1 = secDartScanTools->createButton(g_languageStringsReader->getString("draw_octree"));
			secDartScanTools->getMenuButton(buttonID1)->triggerCallback = drawOctreeVolume;

			BUTTON_ID buttonID2 = secDartScanTools->createButton(g_languageStringsReader->getString("draw_tsdf"));
			secDartScanTools->getMenuButton(buttonID2)->triggerCallback = drawTSDF;

			BUTTON_ID buttonID3 = secDartScanTools->createButton(g_languageStringsReader->getString("clear_octree_display"));
			secDartScanTools->getMenuButton(buttonID3)->triggerCallback = clearOctreeVolumeDisplay;

			BUTTON_ID buttonID4 = secDartScanTools->createButton(g_languageStringsReader->getString("clear_tsdf_display"));
			secDartScanTools->getMenuButton(buttonID4)->triggerCallback = clearTSDFDisplay;

			BUTTON_ID buttonID5 = secDartScanTools->createButton(g_languageStringsReader->getString("clear_dda_display"));
			secDartScanTools->getMenuButton(buttonID5)->triggerCallback = clearDDADisplay;

			BUTTON_ID buttonID6 = secDartScanTools->createButton(g_languageStringsReader->getString("close"));
			secDartScanTools->getMenuButton(buttonID6)->triggerCallback = activateObserversForMenus;

			secDartScanTools->setFramesBeforeClose(50000);
			secDartScanTools->setStartScaleValue(0.0f);
			secDartScanTools->setTargetScaleValue(0.5f);
			secDartScanTools->initializeDrawables();
			secDartScanTools->attachToSceneGraph(g_sceneRoot);
		}

		/*{
			FenceMenu* primObjectFence = new FenceMenu(g_configReader, FENCE_SCAN_OCTREE,
				editModeContextDefaultFenceMenuColor,
				editModeContextHighlightFenceMenuColor,
				menuPointerColor,
				CONTROLLER_TYPE::SECONDARY, 0.13, 0.15, 0.9);
			primObjectFence->getMenuTransform()->setName("PosAttTra-FENCE_SCAN_OCTREE-Menu");
			primObjectFence->closeCallback = activateObserversForMenus;
			g_menuManager->addMenuForUniqueName(primObjectFence);

			BUTTON_ID buttonfenceID1 = primObjectFence->createButton(g_languageStringsReader->getString("draw_octree"));
			primObjectFence->getMenuButton(buttonfenceID1)->triggerCallback = damianDemo;

			BUTTON_ID buttonfenceID5 = primObjectFence->createButton(g_languageStringsReader->getString("close"));
			primObjectFence->getMenuButton(buttonfenceID5)->triggerCallback = activateObserversForMenus;

			primObjectFence->setStartScaleValue(0.0f);
			primObjectFence->setTargetScaleValue(0.2);
			primObjectFence->initializeDrawables();
			primObjectFence->attachToSceneGraph(g_sceneRoot);
		}*/

		//  Context: Polygonize
		{
			TouchMenu* touchMenu = new TouchMenu(TOUCH_SCAN_POLYGONIZE, touchMenuFloatingHeight, defaultTouchMenuColor, highlightTouchMenuColor, CONTROLLER_TYPE::SECONDARY);
			touchMenu->getMenuTransform()->setName("PosAttTra-TOUCH_SCAN_POLYGONIZE-Menu");
			g_menuManager->addMenuForUniqueName(touchMenu);
			
			BUTTON_ID touchButtonID1 = touchMenu->createButton("1");
			static_cast<TouchMenuButton*>(touchMenu->getMenuButton(touchButtonID1))->setButtonUseTexture("./data/menu_textures/toolMenuIcon.png");
			static_cast<TouchMenuButton*>(touchMenu->getMenuButton(touchButtonID1))->setButtonUseTextureHover("./data/menu_textures/toolMenuIconHover.png");
			static_cast<TouchMenuButton*>(touchMenu->getMenuButton(touchButtonID1))->setButtonUseTextureActive("./data/menu_textures/toolMenuIconActive.png");
			touchMenu->getMenuButton(touchButtonID1)->triggerCallback = openDartMenuForScanPolygonizeContext;

			BUTTON_ID touchButtonID2 = touchMenu->createButton("2");
			static_cast<TouchMenuButton*>(touchMenu->getMenuButton(touchButtonID2))->setButtonUseTexture("./data/menu_textures/cubeIcon.png");
			static_cast<TouchMenuButton*>(touchMenu->getMenuButton(touchButtonID2))->setButtonUseTextureHover("./data/menu_textures/cubeIconHover.png");
			static_cast<TouchMenuButton*>(touchMenu->getMenuButton(touchButtonID2))->setButtonUseTextureActive("./data/menu_textures/cubeIconActive.png");
			touchMenu->getMenuButton(touchButtonID2)->triggerCallback = emptyCallback;

			BUTTON_ID touchButtonID3 = touchMenu->createButton("3");
			static_cast<TouchMenuButton*>(touchMenu->getMenuButton(touchButtonID3))->setButtonUseTexture("./data/menu_textures/fenceMenuIcon.png");
			static_cast<TouchMenuButton*>(touchMenu->getMenuButton(touchButtonID3))->setButtonUseTextureHover("./data/menu_textures/fenceMenuIconHover.png");
			static_cast<TouchMenuButton*>(touchMenu->getMenuButton(touchButtonID3))->setButtonUseTextureActive("./data/menu_textures/fenceMenuIconActive.png");
			touchMenu->getMenuButton(touchButtonID3)->triggerCallback = openFenceMenuForScanGeneralCommands;

			/*BUTTON_ID touchButtonID4 = touchMenu->createButton("4");
			static_cast<TouchMenuButton*>(touchMenu->getMenuButton(touchButtonID4))->setButtonUseTexture("./data/menu_textures/Undo.png");
			static_cast<TouchMenuButton*>(touchMenu->getMenuButton(touchButtonID4))->setButtonUseTextureHover("./data/menu_textures/UndoHover.png");
			static_cast<TouchMenuButton*>(touchMenu->getMenuButton(touchButtonID4))->setButtonUseTextureActive("./data/menu_textures/UndoActive.png");
			touchMenu->getMenuButton(touchButtonID4)->triggerCallback = emptyCallback;
			touchMenu->getMenuButton(touchButtonID4)->markAsActive(false);*/

			touchMenu->setHelpText(g_languageStringsReader->getString("upper_scan_mode_toggle_visibility"),
				g_languageStringsReader->getString("upper_undo"),
				g_languageStringsReader->getString("upper_tools"),
				g_languageStringsReader->getString("upper_settings"));
			touchMenu->initializeHelpText();

			touchMenu->setFramesBeforeClose(50000);
			touchMenu->setStartScaleValue(0.0f);
			touchMenu->setTargetScaleValue(0.022f);
			touchMenu->initializeDrawables();
			touchMenu->attachToSceneGraph(g_interactionManager->getSecondaryController()->getMatrixTransformNotShadowed());
		}

		{
			DartMenu* secDartScanTools = new DartMenu(g_configReader, DART_SCAN_POLYGONIZE_TOOLS, defaultMenuColor, highlightMenuColor, menuPointerColor, CONTROLLER_TYPE::SECONDARY, dartMenuTextSize);
			secDartScanTools->getMenuTransform()->setName("PosAttTra-DART_SCAN_POLYGONIZE_TOOLS-Menu");
			g_menuManager->addMenuForUniqueName(secDartScanTools);

			BUTTON_ID buttonID1 = secDartScanTools->createButton(g_languageStringsReader->getString("marching_cubes"));
			secDartScanTools->getMenuButton(buttonID1)->triggerCallback = polygonizePointCloudMarchingCubes;

			BUTTON_ID buttonID2 = secDartScanTools->createButton(g_languageStringsReader->getString("project_texture"));
			secDartScanTools->getMenuButton(buttonID2)->triggerCallback = generateTextureForPolygonize;

			BUTTON_ID buttonID3 = secDartScanTools->createButton(g_languageStringsReader->getString("clear_polygonize"));
			secDartScanTools->getMenuButton(buttonID3)->triggerCallback = clearPolygonize;

			BUTTON_ID buttonID4 = secDartScanTools->createButton(g_languageStringsReader->getString("close"));
			secDartScanTools->getMenuButton(buttonID4)->triggerCallback = activateObserversForMenus;

			secDartScanTools->setFramesBeforeClose(50000);
			secDartScanTools->setStartScaleValue(0.0f);
			secDartScanTools->setTargetScaleValue(0.5f);
			secDartScanTools->initializeDrawables();
			secDartScanTools->attachToSceneGraph(g_sceneRoot);
		}

		/*{
			FenceMenu* primObjectFence = new FenceMenu(g_configReader, FENCE_SCAN_MARCHING_CUBES,
				editModeContextDefaultFenceMenuColor,
				editModeContextHighlightFenceMenuColor,
				menuPointerColor,
				CONTROLLER_TYPE::SECONDARY, 0.13, 0.15, 0.9);
			primObjectFence->getMenuTransform()->setName("PosAttTra-FENCE_SCAN_MARCHING_CUBES-Menu");
			primObjectFence->closeCallback = activateObserversForMenus;
			g_menuManager->addMenuForUniqueName(primObjectFence);

			BUTTON_ID buttonfenceID1 = primObjectFence->createButton(g_languageStringsReader->getString("polygonize"));
			primObjectFence->getMenuButton(buttonfenceID1)->triggerCallback = damianDemo;

			BUTTON_ID buttonfenceID5 = primObjectFence->createButton(g_languageStringsReader->getString("close"));
			primObjectFence->getMenuButton(buttonfenceID5)->triggerCallback = activateObserversForMenus;

			primObjectFence->setStartScaleValue(0.0f);
			primObjectFence->setTargetScaleValue(0.2);
			primObjectFence->initializeDrawables();
			primObjectFence->attachToSceneGraph(g_sceneRoot);
		}*/

		//ToDo: Since I put these lines in "DEFAULT" but use "SCAN", I'm not able anymore to trigger a scan 
		if (g_configReader->getStringFromStartupConfig("global_application_mode_at_startup").compare("EDIT_MODE") == 0)
		{
			registerScaleSelectedObjectObserverWithTwoSqueeze();
			activateMenuStructureForEditMode();
		}
		else if (g_configReader->getStringFromStartupConfig("global_application_mode_at_startup").compare("SCAN_MODE") == 0)
		{
			activateScanContextPointCloud();
			registerScanPointCloudObserver();
		}
	}

	//####################################################################################################
	//####################################################################################################
	//########################################### SCAN ##################################################
	//####################################################################################################
	//####################################################################################################
	else if (g_configReader->getStringFromStartupConfig("type_of_menu_structure") == "SCAN")
	{
		//FenceMenu *menu = new FenceMenu(g_configReader, FENCE_OBJMODE_ADD_MESH, defaultMenuColor, highlightMenuColor, menuPointerColor, CONTROLLER_TYPE::SECONDARY);
		//menu->getMenuTransform()->setName("PosAttTra-FENCE_OBJMODE_ADD_MESH-Menu");
		//g_menuManager->addMenuForUniqueName(menu);

		//BUTTON_ID buttonID1 = menu->createButton(g_languageStringsReader->getString("Exit ClayMore"));
		//menu->getMenuButton(buttonID1)->triggerCallback = loadLowResMonkeyModel;
		//menu->closeCallback = closeFenceMenu;

		//menu->setStartScaleValue(0.0f);
		//menu->setTargetScaleValue(0.2f);
		//menu->initializeDrawables();
		//menu->attachToSceneGraph(g_sceneRoot);
	}





	//TODO:Currently abuse it for only selecting the selection observer
	//LastMenuObserver* lastMenuObserver = new LastMenuObserver(&isActivatedChangeToOtherTool, &g_primaryMenuBeforLastMenu);
	//g_interactionManager->registerObserver(CONTROLLER_TYPE::PRIMARY, Observer::MENUBUTTON, lastMenuObserver);


	return true;
}