//This file is just outsourced code from the main.cpp

//######################################################################################################
//Parsing  config.xml and strings.xml
bool parseConfigAndStringsXML()
{
	g_configReader = new ConfigReader();
	if (!g_configReader->readConfigFile(CONFIG_FILE_PATH))
	{
		system("pause");
		return false;
	}

	g_languageStringsReader = new LanguageStringsReader();
	if (!g_languageStringsReader->readLanguageStringsFile(g_configReader->getStringFromStartupConfig("strings_file_path").c_str(), g_configReader->getStringFromStartupConfig("language").c_str()))
	{
		system("pause");
		return false;
	}

	if (ConfigReader::DEBUG_LEVEL >= NOT_IN_LOOPS)
		std::cout << std::endl << "Config and Language Strings files loaded from " << CONFIG_FILE_PATH << " and " << "..." << std::endl;
	return true;
}

//######################################################################################################
//Parsing xml parameters in faster data types than reading again and again the configManager's map
//TODO: Better, if the configReader initializes all those fast access parameters
bool createFastAccessParametersFromConfig()
{
	//TODO3: Create own file/class for this
	//Magic Leap Spectator Fast Access Parameters
	g_isMagicLeapSpectatorActive = g_configReader->getBoolFromStartupConfig("activate_magic_leap_spectator");
	g_useAzureTelepresence = g_configReader->getBoolFromStartupConfig("use_azure_telepresence");

	//Interaction Management Fast Access Parameters
	if (g_configReader->getStringFromStartupConfig("input_device").compare("MOUSE") == 0 ||
		g_configReader->getStringFromStartupConfig("input_device").compare("VIVE") == 0) // TODO: Actually there must be something like this for interaction: MOUSE_AND_KEYBOARD_VIVE
	{
		g_useKeyboardMouse = true;
	}

	if (g_configReader->getStringFromStartupConfig("input_device").compare("VIVE") == 0)
	{
		g_useVive = true;
	}

	g_useLeapMotion = g_configReader->getBoolFromStartupConfig("use_leap_motion_device");

	//Point Cloud Fast Access Parameters
	g_useRealSenseDevice = g_configReader->getBoolFromStartupConfig("use_real_sense_device");
	g_useAzureKinectDevice = g_configReader->getBoolFromStartupConfig("use_azure_kinect_device");
	// ### START - Old Scan Pipeline ###
	// g_useMultipleRealSenseDevices = g_configReader->getBoolFromStartupConfig("use_multiple_real_sense_device");
	// ### END - Old Scan Pipeline ###
	g_useARCoreTracking = g_configReader->getBoolFromStartupConfig("enable_osc_arcore_tracking");

	//Misc Fast Access Parameters
	g_isSpectatorWindowActive = g_configReader->getBoolFromStartupConfig("activate_spectator_window");

	//Azure Kinect
	g_useColorAndDepthShooter = g_configReader->getBoolFromStartupConfig("use_color_and_depth_shooter");
	if (!g_configReader->getBoolFromStartupConfig("use_azure_kinect_device"))
	{
		g_useColorAndDepthShooter = false;
	}

	//FacialLandMarksOverOSCListener
	g_useSeventyFacialLandMarksOverOSCListener = g_configReader->getBoolFromStartupConfig("use_70_facial_landmarks_over_osc");

	//Use SRanipal Eye Tracking
	g_useSRanipalEyeTracking = g_configReader->getBoolFromStartupConfig("use_sranipal_eye_tracking");

	return true;
}

bool createAndSetUpGeometryManagement()
{
	//######################################################################################################
	//GeometryManagement
	if (ConfigReader::DEBUG_LEVEL >= NOT_IN_LOOPS)
		std::cout << std::endl << "First: Gonna need some GeometryManagement!" << std::endl;

	g_miscDataStorage = new MiscDataStorage();
	g_geometryManager = new GeometryManager(g_configReader, g_miscDataStorage);

	//HistoryManager
	g_historyManager = new HistoryManager();
	g_historyManager->setGeometryManager(g_geometryManager);

	return true;
}

bool createAndSetUpDataManagement()
{
	//######################################################################################################
	//DataManagement
	if (ConfigReader::DEBUG_LEVEL >= NOT_IN_LOOPS)
		std::cout << std::endl << "Next: DataManagement. I'll load the startup model as well (path: " << g_configReader->getStringFromStartupConfig("path_to_startup_model") << ")" << std::endl;

	g_dataManager = new DataManager(g_geometryManager);
	return true;
}


osg::ref_ptr<osg::MatrixTransform> createLightSource(unsigned int num, const osg::Vec3& trans, const osg::Vec4& color)
{
	osg::ref_ptr<osg::Light> light = new osg::Light;
	light->setLightNum(num);
	light->setDiffuse(color);
	light->setPosition(osg::Vec4(0.0f, 0.0f, 0.0f, 1.0f));
	osg::ref_ptr<osg::LightSource> lightSource = new osg::LightSource;
	lightSource->setLight(light);
	light->setDirection(osg::Vec3f(0.0, -1.0, 0.0));
	osg::ref_ptr<osg::MatrixTransform> sourceTrans = new osg::MatrixTransform;
	sourceTrans->setMatrix(osg::Matrix::translate(trans));
	sourceTrans->addChild(lightSource.get());
	return sourceTrans.release();
}


bool createAndSetUpScene()
{
	//######################################################################################################
	//Scene
	//Maybe not bad, if there is a SceneCreator -> The scene creator uses config.xml parameters as well, for creating costums startup scenes
	if (ConfigReader::DEBUG_LEVEL >= NOT_IN_LOOPS)
		std::cout << std::endl << "Creating scene..." << std::endl;

	g_sceneRoot = new osg::Group();
	g_sceneRoot->setName("g_sceneRoot");

#ifndef TORCH_SUPPORTED_BY_CMAKE
	Grid grid(g_configReader->getIntFromStartupConfig("line_count_for_floor_grid"),
		SHOW_X_Z,
		g_configReader->getIntFromStartupConfig("cell_size_for_floor_grid"),
		g_configReader->getIntFromStartupConfig("line_width_for_rgb_axes_floor_grid"),
		g_configReader->getIntFromStartupConfig("line_width_for_grey_axes_floor_grid"));
	grid.attachToSceneGraph(g_sceneRoot);
#endif
	//add a floor to the scene
	Floor* floor = new Floor("floor");
	floor->createFloor(-0.01);

	//Create a basic light - TODO: position should be controllable with VIVE
	osg::ref_ptr<osg::MatrixTransform> light0;
	osg::ref_ptr<osg::MatrixTransform> light1;
	osg::ref_ptr<osg::MatrixTransform> light2;

	light0 = createLightSource(0, osg::Vec3(0.0f, 20.0f, 0.0f), osg::Vec4(0.4f, 0.4f, 0.4f, 1.0f));
	light1 = createLightSource(1, osg::Vec3(10.0f, 6.0f, -8.0f), osg::Vec4(0.9f, 0.3f, 0.3f, 1.0f));
	light2 = createLightSource(2, osg::Vec3(1.0f, 2.0f, 10.0f), osg::Vec4(0.3f, 0.9f, 0.3f, 1.0f));

	g_sceneRoot->addChild(light0);
	g_sceneRoot->addChild(light1);
	g_sceneRoot->addChild(light2);

	g_sceneRoot->addChild(g_geometryManager);
	g_sceneRoot->addChild(floor->getMatrixTransform());

	//TODO: Catch something like "no_startup_model"
	g_dataManager->loadData(g_configReader->getStringFromStartupConfig("path_to_startup_model"));
	g_geometryManager->getActiveGeometryData()->getGeometryVisualization()->selectTechnique(INDICATED_AS_SELECTED_AND_ACTIVE_SHADING);

#ifndef TORCH_SUPPORTED_BY_CMAKE
	//Adding Skybox
	if (g_configReader->getStringFromStartupConfig("skybox").compare("") != 0 && g_configReader->getStringFromStartupConfig("skybox").compare("none") != 0)
	{
		Skybox* skybox = new Skybox(g_configReader->getStringFromStartupConfig("skybox"));
		skybox->attachToSceneGraph(g_sceneRoot);
	}
#endif
	return true;
}


//Initialize ControllerTools for both controllers separately to allow simultan usage on both controllers
bool initializeControllerTools()
{
	g_interactionManager->getPrimaryController()->addControllerToolForType(ControllerTool::SPHERE, new SphereControllerTool(0.075f));
	g_interactionManager->getSecondaryController()->addControllerToolForType(ControllerTool::SPHERE, new SphereControllerTool(0.075f));

	g_interactionManager->getPrimaryController()->addControllerToolForType(ControllerTool::PICK_RAY, new PickRayControllerTool());
	g_interactionManager->getSecondaryController()->addControllerToolForType(ControllerTool::PICK_RAY, new PickRayControllerTool());

	g_interactionManager->getPrimaryController()->addControllerToolForType(ControllerTool::RULER, new RulerControllerTool(g_configReader, g_geometryManager, g_miscDataStorage));
	g_interactionManager->getSecondaryController()->addControllerToolForType(ControllerTool::RULER, new RulerControllerTool(g_configReader, g_geometryManager, g_miscDataStorage));

	g_interactionManager->getPrimaryController()->addControllerToolForType(ControllerTool::VISUALIZE_AXIS_AND_GRID, new VisualizeAxisAndGridControllerTool(
		g_configReader, g_geometryManager, g_miscDataStorage));
	g_interactionManager->getSecondaryController()->addControllerToolForType(ControllerTool::VISUALIZE_AXIS_AND_GRID, new VisualizeAxisAndGridControllerTool(
		g_configReader, g_geometryManager, g_miscDataStorage));

	g_interactionManager->getPrimaryController()->addControllerToolForType(ControllerTool::VISUALIZE_BARYCENTRUM, new VisualizeBarycentrumControllerTool(g_configReader));
	g_interactionManager->getSecondaryController()->addControllerToolForType(ControllerTool::VISUALIZE_BARYCENTRUM, new VisualizeBarycentrumControllerTool(g_configReader));

	g_interactionManager->getPrimaryController()->addControllerToolForType(ControllerTool::DASHED_LINE, new DashedLineControllerTool());
	g_interactionManager->getSecondaryController()->addControllerToolForType(ControllerTool::DASHED_LINE, new DashedLineControllerTool());

	g_interactionManager->getPrimaryController()->addControllerToolForType(ControllerTool::BOUNDING_BOX, new BoundingBoxControllerTool());
	g_interactionManager->getSecondaryController()->addControllerToolForType(ControllerTool::BOUNDING_BOX, new BoundingBoxControllerTool());

	g_interactionManager->getPrimaryController()->addControllerToolForType(ControllerTool::THREE_D_ICON, new ThreeDIconControllerTool());
	g_interactionManager->getSecondaryController()->addControllerToolForType(ControllerTool::THREE_D_ICON, new ThreeDIconControllerTool());

	return true;
}


bool createAndSetUpInteractionManager(osg::ArgumentParser argumentsIn)
{
	//######################################################################################################
	//Interaction
	if (ConfigReader::DEBUG_LEVEL >= NOT_IN_LOOPS)
		std::cout << std::endl << "Set up InteractionManagement... " << std::endl;
	//Just for managing KeyBoardKeyPresses once (I guess there is some weird OSG behaviour)
	g_lastKeyPressedTimeStamp = std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::system_clock::now().time_since_epoch()).count();

	g_interactionManager = new InteractionManager(g_configReader, g_useKeyboardMouse, g_useVive, g_useLeapMotion, g_miscDataStorage);


	if (!g_interactionManager->initializeInteraction(&argumentsIn))
	{
		std::cout << " # ERROR in main() - Could not initialize interaction!" << std::endl;
		system("pause");
		return false;
	}

	g_sceneRoot->addChild(g_interactionManager->getOpenVRViewer());

	if (!initializeControllerTools())
	{
		std::cout << " # ERROR in main() - Could not initialize ControllerTools!" << std::endl;
		system("pause");
		return false;
	}
	if (g_useVive)
	{
		g_interactionManager->getPrimaryController()->addToScenegraph(g_sceneRoot, g_sceneRoot);
		g_interactionManager->getSecondaryController()->addToScenegraph(g_sceneRoot, g_sceneRoot);
		g_interactionManager->getGenericTracker()->addToScenegraph(g_sceneRoot, g_sceneRoot);
	}

	return true;
}


bool createAndSetUpViewersAndWindows()
{
	//######################################################################################################
	//Viewer settings
	if (ConfigReader::DEBUG_LEVEL >= NOT_IN_LOOPS)
		std::cout << std::endl << "Last: The Viewer settings... " << std::endl;

	//TODO: Move this to interaction Manager?
	g_mainViewer = g_interactionManager->getMainViewer();
	//NOTE: Important, must be SingleThreaded or ThreadedPerContext - otherwise no updating from vive position possible
	g_mainViewer->setThreadingModel(osgViewer::Viewer::SingleThreaded);
	g_mainViewer->getCamera()->setComputeNearFarMode(osg::CullSettings::DO_NOT_COMPUTE_NEAR_FAR);
	g_mainViewer->getCamera()->setClearColor(g_configReader->getVec4fFromStartupConfig("main_window_background_color"));
	g_mainViewer->setSceneData(g_sceneRoot);
	g_mainViewer->getCamera()->setAllowEventFocus(false);
	g_mainViewer->setRunMaxFrameRate(90.0);

	g_mainViewer->setUpViewInWindow(g_configReader->getIntFromStartupConfig("main_window_x_pos"),
		g_configReader->getIntFromStartupConfig("main_window_y_pos"),
		g_configReader->getIntFromStartupConfig("main_window_width"),
		g_configReader->getIntFromStartupConfig("main_window_height"));

	osg::ref_ptr< osg::GraphicsContext::Traits > mainViewerTraits = new osg::GraphicsContext::Traits((*g_mainViewer->getCamera()->getGraphicsContext()->getTraits()));
	mainViewerTraits->vsync = false;
	mainViewerTraits->screenNum = g_configReader->getIntFromStartupConfig("main_window_on_monitor"); //On which monitor should it placed when started //TODO: Add to config or at least to define.h
	mainViewerTraits->windowName = "ClayMore Main Window - Mouse Trackball Control - Statistics with Key S in MOUSE 'input devive' mode";
	mainViewerTraits->windowDecoration = true;
	osg::ref_ptr< osg::GraphicsContext > mainViewerGC = osg::GraphicsContext::createGraphicsContext(mainViewerTraits.get());
	//First I have to realize it, cause otherwise it wont be accepted by the graphics driver (cite by OSG Forum)
	mainViewerGC->realize();
	g_mainViewer->getCamera()->setGraphicsContext(mainViewerGC);

	//######################################################################################################
	//Spectator Window
	g_spectatorViewer = new osgViewer::Viewer;
	if (g_isSpectatorWindowActive)
	{
		g_spectatorViewer->setSceneData(g_sceneRoot);

		//TODO: setUpViewInWindow is deprecated. Use g_spectatorViewer->apply instead
		//osg::ref_ptr<osgViewer::ViewConfig> viewConfig;
		//viewConfig->
		//g_spectatorViewer->apply(viewConfig);

		g_spectatorViewer->setUpViewInWindow(g_configReader->getIntFromStartupConfig("spectator_window_x_pos"),
			g_configReader->getIntFromStartupConfig("spectator_window_y_pos"),
			g_configReader->getIntFromStartupConfig("spectator_window_width"),
			g_configReader->getIntFromStartupConfig("spectator_window_height"));
		
		//Set the WASD Camera Controller to the SpectatorViewer
		if ((g_configReader->getStringFromStartupConfig("activate_wasd_manipulator_at_startup").compare("true") == 0))
		{
			//g_spectatorViewer->addEventHandler(new UserTest2KeyboardListener(g_clayMoreVideoPlayer, g_miscDataStorage, &g_writeAutoSaveFileUserTest2, &g_readAutoSaveFileUserTest2));
			//osg::ref_ptr<osgGA::TrackballManipulator> trackballManipulator = new osgGA::TrackballManipulator();
			osg::ref_ptr<WASDCameraManipulator> wasdCameraManipulator = new WASDCameraManipulator();
			wasdCameraManipulator->setAutoComputeHomePosition(false);
			if (g_useColorAndDepthShooter)
			{
				wasdCameraManipulator->setHomePosition(osg::Vec3f(1.0, 2.0, 1.0), osg::Vec3f(1.0, 0.0, 1.0), osg::Vec3f(0.0, 0.0, -1.0), false);
			}
			else
			{
				//wasdCameraManipulator->setHomePosition(osg::Vec3f(0.0, 1.0, -1.1), osg::Vec3f(0.0, 0.0, 1.0), osg::Vec3f(0.0, -1.0, 0.0), false);
				wasdCameraManipulator->setHomePosition(osg::Vec3f(0.0, 0.0, 0.0), osg::Vec3f(0.0, 0.0, 1.0), osg::Vec3f(0.0, 1.0, 0.0), false);
			}
			g_spectatorViewer->setCameraManipulator(wasdCameraManipulator);		
		}

		//Set the KeyboardMouseConnection to the spectator window for grabbing keyboard and mouse events
		if (g_useKeyboardMouse) {
			g_interactionManager->setSpectatorView(g_spectatorViewer);
			g_interactionManager->activateKeyboardMouseConnection();
		}

		g_spectatorViewer->getCamera()->setAllowEventFocus(false);
		g_spectatorViewer->setThreadingModel(osgViewer::Viewer::SingleThreaded);
		g_spectatorViewer->setRunMaxFrameRate(90.0);
		g_spectatorViewer->getCamera()->setComputeNearFarMode(osg::CullSettings::DO_NOT_COMPUTE_NEAR_FAR);
		g_spectatorViewer->getCamera()->setProjectionMatrixAsPerspective(45.0, 
			(float)g_configReader->getIntFromStartupConfig("spectator_window_width") / (float)g_configReader->getIntFromStartupConfig("spectator_window_height"),
			0.001, 
			500.0);
		g_spectatorViewer->getCamera()->setClearColor(g_configReader->getVec4fFromStartupConfig("spectator_window_background_color"));

		osg::ref_ptr< osg::GraphicsContext::Traits > traits = new osg::GraphicsContext::Traits((*g_spectatorViewer->getCamera()->getGraphicsContext()->getTraits()));
		traits->vsync = false;
		traits->screenNum = g_configReader->getIntFromStartupConfig("spectator_window_on_monitor"); //On which monitor should it placed when started //TODO: Add to config or at least to define.h
		traits->windowName = "ClayMore Spectator - WASD Camera Control";
		traits->samples = 8;
		osg::ref_ptr< osg::GraphicsContext > gc = osg::GraphicsContext::createGraphicsContext(traits.get());
		//First I have to realize it, cause otherwise it wont be accepted by the graphics driver (cite by OSG Forum)
		gc->realize();
		g_spectatorViewer->getCamera()->setGraphicsContext(gc);

		g_spectatorViewer->realize();
		//g_compositeViewer->addView(g_spectatorViewer);

	}
	g_mainViewer->realize();
	return true;
}

bool createAndSetUpNetworkOSC()
{
	//TODO: Create something like this (it is a better threading structure):
	//		https://stackoverflow.com/questions/25937735/c-interrupting-udp-listener-compiled-using-oscpack-in-xcode

	g_networkManagerOSC = new NetworkManagerOSC(g_configReader);

	//Read IP adresses from config
	g_networkManagerOSC->setIpAddressForSRanipalEyeSender(g_configReader->getStringFromStartupConfig("sranipal_eye_tracking_sender_to_facial_landmarks_osc_client"));

	if (g_useARCoreTracking)
	{
		g_networkManagerOSC->activateARCoreListener();
		std::thread thread(&NetworkManagerOSC::runARCoreListener, g_networkManagerOSC);
		thread.detach();
	}

	if (g_configReader->getBoolFromStartupConfig("activate_magic_leap_spectator"))
	{
		// g_networkManagerOSC->activateMagicLeapListener(); //Funtion currently not implemented
		std::thread thread(&NetworkManagerOSC::runMagicLeapListener, g_networkManagerOSC);
		thread.detach();
	}

	if (g_configReader->getBoolFromStartupConfig("activate_remote_gui_functionality"))
	{
		g_networkManagerOSC->activateControlCommandListener();
		g_networkManagerOSC->registerCallbackForControlCommandListener(activateScanContextPointCloud, "activateScanContextPointCloud");
		g_networkManagerOSC->registerCallbackForControlCommandListener(activateScanContextPolygonize, "activateScanContextMarchingCubes");
		g_networkManagerOSC->registerCallbackForControlCommandListener(activateScanContextOctree, "activateScanContextOctree");
		g_networkManagerOSC->registerCallbackForControlCommandListener(togglePointCloudDisplay, "togglePointCloudDisplay");
		g_networkManagerOSC->registerCallbackForControlCommandListener(toggleOctreeDisplay, "toggleOctreeDisplay");
		g_networkManagerOSC->registerCallbackForControlCommandListener(toggleDDADisplay, "toggleDDADisplay");
		g_networkManagerOSC->registerCallbackForControlCommandListener(toggleTSDFDisplay, "toggleTSDFDisplay");
		g_networkManagerOSC->registerCallbackForControlCommandListener(togglePolygonizeDisplay, "toggleMarchingCubesDisplay");
		g_networkManagerOSC->registerCallbackForControlCommandListener(toggleHelpForScanContext, "toggleHelpForScanContext");
		g_networkManagerOSC->registerCallbackForControlCommandListener(openDartMenuForScanPointCloudContext, "openDartMenuForScanPointCloudContext");
		g_networkManagerOSC->registerCallbackForControlCommandListener(clearPointCloud, "clearPointCloud");
		g_networkManagerOSC->registerCallbackForControlCommandListener(activateObserversForMenus, "activateObserversForMenus");
		g_networkManagerOSC->registerCallbackForControlCommandListener(openDartMenuForScanOctreeContext, "openDartMenuForScanOctreeContext");
		//g_networkManagerOSC->registerCallbackForControlCommandListener(drawOctreeVolume, "drawOctreeVolume");
		g_networkManagerOSC->registerCallbackForControlCommandListener(drawTSDF, "drawTSDF");
		g_networkManagerOSC->registerCallbackForControlCommandListener(clearOctreeVolumeDisplay, "clearOctreeVolumeDisplay");
		g_networkManagerOSC->registerCallbackForControlCommandListener(clearTSDFDisplay, "clearTSDFDisplay");
		g_networkManagerOSC->registerCallbackForControlCommandListener(clearDDADisplay, "clearDDADisplay");
		g_networkManagerOSC->registerCallbackForControlCommandListener(openDartMenuForScanPolygonizeContext, "openDartMenuForScanMarchingCubesContext");
		g_networkManagerOSC->registerCallbackForControlCommandListener(openFenceMenuForScanGeneralCommands, "openFenceMenuForScanGeneralCommands");
		g_networkManagerOSC->registerCallbackForControlCommandListener(polygonizePointCloudMarchingCubes, "polygonizePointCloud");
		g_networkManagerOSC->registerCallbackForControlCommandListener(clearPolygonize, "clearMarchingCubes");

		std::thread thread(&NetworkManagerOSC::runControlCommandListener, g_networkManagerOSC);
		thread.detach();

		//TODO: Do we need a runStatusUpdateListener in a thread? Is it ever receiving messages?
		std::thread thread1(&NetworkManagerOSC::runStatusUpdateListener, g_networkManagerOSC);
		thread1.detach();

		//The following function must be called in the remoteGUI in order to listen to status updates and further debugging data
		//g_networkManagerOSC->activateStatusUpdateListener(); //Don't call this function in ClayMore. Only in ClayMore RemoteGUI
	}
	if (g_useSeventyFacialLandMarksOverOSCListener)
	{
		std::cout << "In Init...h beu activateSeventyFacialLandMarksOverOSCListener" << std::endl;
		g_networkManagerOSC->activateSeventyFacialLandMarksOverOSCListener();
		std::thread thread(&NetworkManagerOSC::runSeventyFacialLandMarksOverOSCListener, g_networkManagerOSC);
		thread.detach();
	}

	return true;
}


bool setUpHistoryManager()
{
	//######################################################################################################
	//TODO: Create a HistoryManager function for this
	//Save one mesh in the history, cause some tools work on this mesh
	g_geometryManager->rotateSelectedVerticesStart();
	return true;
}

bool createAndSetUpMagicLeapCommunication()
{
	if (g_isMagicLeapSpectatorActive)
	{
		g_geometryPacketSender = new GeometryPacketSender(g_networkManagerOSC);
	}

	return true;
}

bool createAndSetUpPointCloudManagement()
{
	if (ConfigReader::DEBUG_LEVEL >= NOT_IN_LOOPS)
		std::cout << "Creating PointCloudManager" << std::endl;
	osg::ref_ptr<osg::Group> pointCloudGroup = new osg::Group();
	pointCloudGroup->setName("PointCloudGroup");
	g_sceneRoot->addChild(pointCloudGroup);
	g_pointCloudManager = new PointCloudManager(g_configReader, pointCloudGroup);
	//std::thread (&PointCloudManager::updateAndRenderPointCloudInLoop, g_pointCloudManager).detach();
	//thread0.detach();

	if (g_useAzureKinectDevice)
	{
		g_pointCloudManager->startAzureKinect();
	}

	return true;
}

bool createAndSetUpTensorManagement()
{
#ifdef TORCH_SUPPORTED_BY_CMAKE
	if (ConfigReader::DEBUG_LEVEL >= NOT_IN_LOOPS)
		std::cout << "Creating TensorManager" << std::endl;
	osg::ref_ptr<osg::Group> tensorGroup = new osg::Group();
	tensorGroup->setName("TensorGroup");
	g_sceneRoot->addChild(tensorGroup);
	g_tensorManager = new TensorManager(g_configReader, tensorGroup);
#endif
	return true;
}


bool createAndSetUpDebugAndStatisticsPlugin()
{
	g_debugAndStatisticsPlugin = new DebugAndStatisticsPlugin();

	//Weld a FrameCounter to the back of the secondary Controller
	auto frameCounter = g_debugAndStatisticsPlugin->createFrameCounter(0.03, "fonts/arial.ttf");
	frameCounter->setName("FrameCounter");
	g_interactionManager->getSecondaryController()->getMatrixTransformNotShadowed()->addChild(frameCounter);


	//TODO: Commented Code from the main loop -> Reuse this for the DebugAndStatisticsPlugin
		////Trying to get some viewer stats, all the stuff what is in stats handler with hitting the S key....
		//g_mainViewer->addEventHandler(new osgViewer::StatsHandler);
		////osgViewer::StatsHandler* test = new osgViewer::StatsHandler;
		//osg::Stats* test = g_mainViewer->getViewerStats();
		//test->collectStats("Ehm");
		//std::map<std::string, double> testMap = test->getAttributeMap(g_mainViewer->getViewerStats()->getEarliestFrameNumber());
		//std::map<std::string, double>::iterator mapIt = testMap.begin();
		//while (mapIt != testMap.end())
		//{
		//	mapIt++;
		//	std::cout << "1: " << (*mapIt).first << std::endl;
		//	std::cout << "2: " << (*mapIt).second << std::endl;
		//}

	return true;
}
