// ClayMore - Immersive Mesh Modelling --- Copyright (c) 2014-2017 Philipp Ladwig, Jannik Fiedler, Jan Beutgen
#include "HistoryManager.h"
#include "cmGeometryManagement\GeometryManager.h"

HistoryAction::HistoryAction(ActionType actionTypeIn) : m_actionType(actionTypeIn)
{

}

HistoryAction::~HistoryAction()
{

}

HistoryManager::HistoryManager()
{
	//m_historyOfActions = new std::vector<HistoryAction>();
}

HistoryManager::~HistoryManager()
{

}

void HistoryManager::addAction(HistoryAction historyActionIn)
{
	m_historyOfActions.push_back(historyActionIn);

	if (m_historyOfActions.size() > 3)
	{
		m_historyOfActions.erase(m_historyOfActions.begin());
	}

	//m_geometryManager->storeMeshDataAsHistory();

	//DEBUG
	//std::cout << "DEBUG OUTPUT HISTORYMANAGER" << std::endl;
	//for (unsigned int i = 0; i < m_historyOfActions.size(); ++i)
	//{
	//	std::cout << "i: " << i << "  Type of Action:" << m_historyOfActions.at(i).getActionType() << std::endl;
	//}
}

void HistoryManager::undo()
{
	if (m_historyOfActions.size() > 0)
	{
		bool isForbiddenActionInHistoryActions = false;
		for (short i = 0; i < m_historyOfActions.size(); ++i)
		{
			if (m_historyOfActions.at(i).getActionType() == EDI_DELETE_VERTEX || m_historyOfActions.at(i).getActionType() == EDI_DELETE_FACE
				|| m_historyOfActions.at(i).getActionType() == EDI_DUPLICATE || m_historyOfActions.at(i).getActionType() == EDI_EXTRUDE 
				|| m_historyOfActions.at(i).getActionType() == EDI_ADD_PRIMITIVE || m_historyOfActions.at(i).getActionType() == EDI_ADD_VERTEX
				|| m_historyOfActions.at(i).getActionType() == EDI_ADD_EDGE || m_historyOfActions.at(i).getActionType() == EDI_ADD_FACE)
			{
				isForbiddenActionInHistoryActions = true;
			}
		}

		std::vector<HistoryAction>::iterator it = m_historyOfActions.end();
		//If m_historyOfActions contains "forbididden actions, such as delete, extrude or etc, the current historyManager cant handle this and
		//	the undo will not been envoked
			if ((!isForbiddenActionInHistoryActions) && ((*it).getActionType() == EDI_GRAB || (*it).getActionType() == EDI_SCALE || (*it).getActionType() == EDI_ROTATE))
			{
				//m_geometryManager->cancelAnyTransformation();
				m_geometryManager->undo();
			}
	}
}