// ClayMore - Immersive Mesh Modelling --- Copyright (c) 2014-2017 Philipp Ladwig, Jannik Fiedler, Jan Beutgen
#include "Skybox.h"

Skybox::Skybox(std::string & name) :
	m_name(name),
	m_clearNode(nullptr)
{
	createSkyBox();
}

Skybox::~Skybox()
{
}

void Skybox::createSkyBox()
{

	osg::StateSet* stateset = new osg::StateSet();

	osg::TexEnv* te = new osg::TexEnv;
	te->setMode(osg::TexEnv::REPLACE);
	stateset->setTextureAttributeAndModes(0, te, osg::StateAttribute::ON);

	osg::TexGen *tg = new osg::TexGen;
	tg->setMode(osg::TexGen::NORMAL_MAP);
	stateset->setTextureAttributeAndModes(0, tg, osg::StateAttribute::ON);

	osg::TexMat *tm = new osg::TexMat;
	stateset->setTextureAttribute(0, tm);

	osg::TextureCubeMap* skymap = readCubeMap();
	stateset->setTextureAttributeAndModes(0, skymap, osg::StateAttribute::ON);

	stateset->setMode(GL_LIGHTING, osg::StateAttribute::OFF);
	stateset->setMode(GL_CULL_FACE, osg::StateAttribute::OFF);

	// clear the depth to the far plane.
	osg::Depth* depth = new osg::Depth;
	depth->setFunction(osg::Depth::ALWAYS);
	depth->setRange(1.0, 1.0);
	stateset->setAttributeAndModes(depth, osg::StateAttribute::ON);

	stateset->setRenderBinDetails(-1, "RenderBin");

	osg::Drawable* drawable = new osg::ShapeDrawable(new osg::Sphere(osg::Vec3(0.0f, 0.0f, 0.0f), 1));

	osg::Geode* geode = new osg::Geode;
	geode->setCullingActive(false);
	geode->setStateSet(stateset);
	geode->addDrawable(drawable);


	osg::MatrixTransform* transform = new osg::MatrixTransform();
	transform->setMatrix(osg::Matrix::scale(200.0, 200.0, 200.0));
	transform->setCullingActive(false);
	transform->addChild(geode);

	m_clearNode = new osg::ClearNode;
	//  clearNode->setRequiresClear(false);
	m_clearNode->setCullCallback(new TexMatCallback(*tm));
	m_clearNode->addChild(transform);
}

osg::TextureCubeMap* Skybox::readCubeMap()
{
	osg::TextureCubeMap* cubemap = new osg::TextureCubeMap;


	osg::Image* imagePosX = osgDB::readImageFile("data/skyboxes/" + m_name + "/right.tga");
	osg::Image* imageNegX = osgDB::readImageFile("data/skyboxes/" + m_name + "/left.tga");
	osg::Image* imagePosY = osgDB::readImageFile("data/skyboxes/" + m_name + "/down.tga");
	osg::Image* imageNegY = osgDB::readImageFile("data/skyboxes/" + m_name + "/up.tga");
	osg::Image* imagePosZ = osgDB::readImageFile("data/skyboxes/" + m_name + "/back.tga");
	osg::Image* imageNegZ = osgDB::readImageFile("data/skyboxes/" + m_name + "/front.tga");

	if (imagePosX && imageNegX && imagePosY && imageNegY && imagePosZ && imageNegZ)
	{
		cubemap->setImage(osg::TextureCubeMap::POSITIVE_X, imagePosX);
		cubemap->setImage(osg::TextureCubeMap::NEGATIVE_X, imageNegX);
		cubemap->setImage(osg::TextureCubeMap::POSITIVE_Y, imagePosY);
		cubemap->setImage(osg::TextureCubeMap::NEGATIVE_Y, imageNegY);
		cubemap->setImage(osg::TextureCubeMap::POSITIVE_Z, imagePosZ);
		cubemap->setImage(osg::TextureCubeMap::NEGATIVE_Z, imageNegZ);

		cubemap->setWrap(osg::Texture::WRAP_S, osg::Texture::CLAMP_TO_EDGE);
		cubemap->setWrap(osg::Texture::WRAP_T, osg::Texture::CLAMP_TO_EDGE);
		cubemap->setWrap(osg::Texture::WRAP_R, osg::Texture::CLAMP_TO_EDGE);

		cubemap->setFilter(osg::Texture::MIN_FILTER, osg::Texture::LINEAR_MIPMAP_LINEAR);
		cubemap->setFilter(osg::Texture::MAG_FILTER, osg::Texture::LINEAR);
	}

	return cubemap;
}