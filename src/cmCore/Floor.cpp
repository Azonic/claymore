// ClayMore - Immersive Mesh Modelling --- Copyright (c) 2014-2017 Philipp Ladwig, Jannik Fiedler, Jan Beutgen
#include "Floor.h"

#include "osg/Geode"
#include "osg/Geometry"

Floor::Floor(std::string sgName) :
	m_sgName(sgName)
{
	m_matrixTransform = new osg::MatrixTransform();
}

Floor::~Floor()
{
}

void Floor::createFloor(float yPos)
{
	m_yPos = yPos;
	//add example floor for testing - simple plane
	osg::ref_ptr<osg::Geode> floorGeode = new osg::Geode();
	osg::ref_ptr<osg::Geometry> floorGeometry = new osg::Geometry();
	floorGeode->addDrawable(floorGeometry);

	//Use VBO, instead of DisplayLists
	floorGeometry->setUseDisplayList(false);
	floorGeometry->setUseVertexBufferObjects(true);
	floorGeometry->setDataVariance(osg::Object::STATIC);

	osg::ref_ptr<osg::Vec3Array> floorVertices = new osg::Vec3Array;
	floorVertices->push_back(osg::Vec3(-10.0, m_yPos, -10.0));
	floorVertices->push_back(osg::Vec3(10.0, m_yPos, -10.0));
	floorVertices->push_back(osg::Vec3(10.0, m_yPos, 10.0));
	floorVertices->push_back(osg::Vec3(-10.0, m_yPos, 10.0));

	floorGeometry->setVertexArray(floorVertices);

	osg::ref_ptr<osg::DrawElementsUInt> vertIndices = new osg::DrawElementsUInt(osg::PrimitiveSet::QUADS, 0);
	vertIndices->push_back(3);
	vertIndices->push_back(2);
	vertIndices->push_back(1);
	vertIndices->push_back(0);
	floorGeometry->addPrimitiveSet(vertIndices);


	osg::ref_ptr<osg::Vec4Array> colors = new osg::Vec4Array;
	colors->push_back(osg::Vec4(1.0f, 1.0f, 1.0f, 1.0f));

	floorGeometry->setColorArray(colors);
	floorGeometry->setColorBinding(osg::Geometry::BIND_OVERALL);
	floorGeometry->setCullingActive(false);

	osg::ref_ptr<osg::MatrixTransform> floorMatrixTransform = new osg::MatrixTransform();
	//floorMatrixTransform->setMatrix(osg::Matrix::scale(1000.0, 1000.0, 1000.0));
	floorMatrixTransform->addChild(floorGeode);
}