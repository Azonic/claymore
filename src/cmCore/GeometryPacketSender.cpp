// ClayMore - Immersive Mesh Modelling --- Copyright (c) 2014-2017 Philipp Ladwig, Jannik Fiedler, Jan Beutgen
#include "GeometryPacketSender.h"

//#include "..\cmNetworkManagementQt\VertexArrayPacket.h"
//#include "..\cmNetworkManagementQt\IndexArrayPacket.h"
//#include "..\cmNetworkManagementQt\ColorArrayPacket.h"
//#include "cmNetworkManagementQt\NetworkManager.h"
//#include "cmNetworkManagementRakNet\NetworkManagerRakNet.h"
#include "cmNetworkManagementOSC\NetworkManagerOSC.h"
#include "defines.h"

//#include "cmNetworkManagementRakNet\definesAndGlobals.h"

//RakNet
//#include "BitStream.h"
//#include "RakNetTypes.h"  // MessageID

#include <iostream>
GeometryPacketSender::GeometryPacketSender(NetworkManagerOSC* networkManagerOSCIn) :
	//m_networkManager(networkManagerIn),
	//m_networkManagerRakNet(networkManagerRakNetIn),
	m_networkManagerOSC(networkManagerOSCIn)
{

}

GeometryPacketSender::~GeometryPacketSender()
{

}

//void GeometryPacketSender::sendGeometry(GeometryData* geometryDataIn)
//{
	//TODO3: Memory Hole is still a problem, when a connected device disconnects - checkIfAnySocketsIsConnected() remains true and does not restore its value to false
//	if (m_networkManager->checkIfAnySocketsIsConnected())
//	{
//		// ##############################################################################################
//		// ######################################## VERTEX ARRAY ########################################
//		// ##############################################################################################
//		{
//			osg::Vec3Array *vertexArray = dynamic_cast<osg::Vec3Array*>(geometryDataIn->getOpenMeshGeo()->getVertexArray());
//			float arrayForPayload[VERTEX_ARRAY_ELEMENTS_SEND_IN_ONE_PACKET];
//			unsigned int numberOfAllVertices = geometryDataIn->getOpenMeshGeo()->getVertexArray()->getNumElements();
//
//			unsigned int numberOfCurrentPacket = 0;
//			unsigned int k = 0; // Helper variable for filling a float[] like a Vec3Array
//			for (unsigned int i = 0; i < numberOfAllVertices; ++i)
//			{
//				if ((i % (VERTEX_ARRAY_ELEMENTS_SEND_IN_ONE_PACKET / 3) == 0) && (i != 0)) //Send every n verticies (and do not send for the first loop through(when i == 0))
//				{
//					k = 0; //Reset, since arrayForPayload[] must always be written within his range (range of VERTEX_ARRAY_ELEMENTS_SEND_IN_ONE_PACKET)
//					VertexArrayPacket* vertexArrayPacket = new VertexArrayPacket(arrayForPayload, numberOfAllVertices, numberOfCurrentPacket); //TODO: Memory Hole
//					m_networkManager->sendPacketOnAllConnectedSockets(vertexArrayPacket); // packet send
//					++numberOfCurrentPacket;
//				}
//				arrayForPayload[3 * k] = vertexArray->at(i).x();
//				arrayForPayload[3 * k + 1] = vertexArray->at(i).y();
//				arrayForPayload[3 * k + 2] = vertexArray->at(i).z();
//				++k;
//			}
//
//			//If there is a "division rest" then send the last packet (the for loop only send a multiplier of VERTEX_ARRAY_ELEMENTS_SEND_IN_ONE_PACKET elements)
//			if (numberOfAllVertices % (VERTEX_ARRAY_ELEMENTS_SEND_IN_ONE_PACKET / 3) > 0)
//			{
//				unsigned int lastPacketWasSendOnArrayID = numberOfCurrentPacket * (VERTEX_ARRAY_ELEMENTS_SEND_IN_ONE_PACKET / 3);
//				unsigned int pendingArrayIDsForTransmitting = numberOfAllVertices % (VERTEX_ARRAY_ELEMENTS_SEND_IN_ONE_PACKET / 3);
//				k = 0;
//				for (unsigned int i = lastPacketWasSendOnArrayID; i < lastPacketWasSendOnArrayID + pendingArrayIDsForTransmitting; ++i)
//				{
//					arrayForPayload[3 * k] = vertexArray->at(i).x();
//					arrayForPayload[3 * k + 1] = vertexArray->at(i).y();
//					arrayForPayload[3 * k + 2] = vertexArray->at(i).z();
//					++k;
//				}
//				VertexArrayPacket* vertexArrayPacket = new VertexArrayPacket(arrayForPayload, numberOfAllVertices, numberOfCurrentPacket); //TODO: Memory Hole
//				m_networkManager->sendPacketOnAllConnectedSockets(vertexArrayPacket); // packet send
//			}
//		}
//
//		// ##############################################################################################
//		// ######################################## COLOR ARRAY ########################################
//		// ##############################################################################################
//		{
//			osg::Vec4Array *colorArray = dynamic_cast<osg::Vec4Array*>(geometryDataIn->getOpenMeshGeo()->getColorArray());
//			float arrayForPayload[COLOR_ARRAY_ELEMENTS_SEND_IN_ONE_PACKET];
//			unsigned int numberOfAllVertexColors = geometryDataIn->getOpenMeshGeo()->getColorArray()->getNumElements();
//
//			unsigned int numberOfCurrentPacket = 0;
//			unsigned int k = 0; // Helper variable for filling a float[] like a Vec3Array
//			for (unsigned int i = 0; i < numberOfAllVertexColors; ++i)
//			{
//				if ((i % (COLOR_ARRAY_ELEMENTS_SEND_IN_ONE_PACKET / 4) == 0) && (i != 0)) //Send every n verticies (and do not send for the first loop through(when i == 0))
//				{
//					k = 0; //Reset, since arrayForPayload[] must always be written within his range (range of COLOR_ARRAY_ELEMENTS_SEND_IN_ONE_PACKET)
//					ColorArrayPacket* colorArrayPacket = new ColorArrayPacket(arrayForPayload, numberOfAllVertexColors, numberOfCurrentPacket); //TODO: Memory Hole
//					m_networkManager->sendPacketOnAllConnectedSockets(colorArrayPacket); // packet send
//					++numberOfCurrentPacket;
//				}
//				arrayForPayload[4 * k] = colorArray->at(i).x();
//				arrayForPayload[4 * k + 1] = colorArray->at(i).y();
//				arrayForPayload[4 * k + 2] = colorArray->at(i).z();
//				arrayForPayload[4 * k + 3] = colorArray->at(i).w();
//				++k;
//			}
//
//			//If there is a "division rest" then send the last packet (the for loop only send a multiplier of VERTEX_ARRAY_ELEMENTS_SEND_IN_ONE_PACKET elements)
//			if (numberOfAllVertexColors % (COLOR_ARRAY_ELEMENTS_SEND_IN_ONE_PACKET / 4) > 0)
//			{
//				unsigned int lastPacketWasSendOnArrayID = numberOfCurrentPacket * (COLOR_ARRAY_ELEMENTS_SEND_IN_ONE_PACKET / 4);
//				unsigned int pendingArrayIDsForTransmitting = numberOfAllVertexColors % (COLOR_ARRAY_ELEMENTS_SEND_IN_ONE_PACKET / 4);
//				k = 0;
//				for (unsigned int i = lastPacketWasSendOnArrayID; i < lastPacketWasSendOnArrayID + pendingArrayIDsForTransmitting; ++i)
//				{
//					arrayForPayload[4 * k] = colorArray->at(i).x();
//					arrayForPayload[4 * k + 1] = colorArray->at(i).y();
//					arrayForPayload[4 * k + 2] = colorArray->at(i).z();
//					arrayForPayload[4 * k + 3] = colorArray->at(i).w();
//					++k;
//				}
//				ColorArrayPacket* colorArrayPacket = new ColorArrayPacket(arrayForPayload, numberOfAllVertexColors, numberOfCurrentPacket); //TODO: Memory Hole
//				m_networkManager->sendPacketOnAllConnectedSockets(colorArrayPacket); // packet send
//			}
//		}
//
//		// ##############################################################################################
//		// ######################################## INDEX ARRAY ########################################
//		// ##############################################################################################
//		{
//			osg::DrawElementsUInt *indexArray = dynamic_cast<osg::DrawElementsUInt*>(geometryDataIn->getOpenMeshGeo()->getPrimitiveSet(0)->getDrawElements());
//			unsigned int arrayForPayload[INDEX_ARRAY_ELEMENTS_SEND_IN_ONE_PACKET];
//			unsigned int numberOfAllIndices = geometryDataIn->getOpenMeshGeo()->getPrimitiveSet(0)->getDrawElements()->getNumIndices();
//			unsigned int numberOfCurrentPacket = 0;
//			unsigned int k = 0;
//			for (unsigned int i = 0; i < numberOfAllIndices; ++i)
//			{
//				if ((i % INDEX_ARRAY_ELEMENTS_SEND_IN_ONE_PACKET == 0) && (i != 0)) //Send every n indices (and do not send when i == 0)
//				{
//					k = 0; //Reset, since arrayForPayload[] must always be written within his 0 - INDEX_ARRAY_ELEMENTS_SEND_IN_ONE_PACKET range
//					IndexArrayPacket* indexArrayPacket = new IndexArrayPacket(arrayForPayload, numberOfAllIndices, numberOfCurrentPacket); //TODO: Memory Hole
//					m_networkManager->sendPacketOnAllConnectedSockets(indexArrayPacket); // packet send
//					++numberOfCurrentPacket;
//				}
//				arrayForPayload[k] = indexArray->at(i);
//				++k;
//			}
//
//			if (numberOfAllIndices % INDEX_ARRAY_ELEMENTS_SEND_IN_ONE_PACKET > 0) //If there is a "division rest" then send the last packet
//			{
//				unsigned int lastPacketWasSendOnArrayID = numberOfCurrentPacket * (INDEX_ARRAY_ELEMENTS_SEND_IN_ONE_PACKET);
//				unsigned int pendingArrayIDsForTransmitting = numberOfAllIndices % (INDEX_ARRAY_ELEMENTS_SEND_IN_ONE_PACKET);
//				k = 0;
//				for (unsigned int i = lastPacketWasSendOnArrayID; i < lastPacketWasSendOnArrayID + pendingArrayIDsForTransmitting; ++i)
//				{
//					arrayForPayload[k] = indexArray->at(i);
//					k++;
//				}
//				IndexArrayPacket* indexArrayPacket = new IndexArrayPacket(arrayForPayload, numberOfAllIndices, numberOfCurrentPacket); //TODO: Memory Hole
//				m_networkManager->sendPacketOnAllConnectedSockets(indexArrayPacket); // packet send
//			}
//		}
//
//		// ##############################################################################################
//		// ######################################## NORMAL ARRAY ########################################
//		// ##############################################################################################
//		{
//			osg::Vec3Array *normalArray = dynamic_cast<osg::Vec3Array*>(geometryDataIn->getOpenMeshGeo()->getNormalArray());
//			float arrayForPayload[VERTEX_ARRAY_ELEMENTS_SEND_IN_ONE_PACKET];
//			unsigned int numberOfAllNormals = geometryDataIn->getOpenMeshGeo()->getNormalArray()->getNumElements();
//
//			unsigned int numberOfCurrentPacket = 0;
//			unsigned int k = 0; // Helper variable for filling a float[] like a Vec3Array
//			for (unsigned int i = 0; i < numberOfAllNormals; ++i)
//			{
//				if ((i % (VERTEX_ARRAY_ELEMENTS_SEND_IN_ONE_PACKET / 3) == 0) && (i != 0)) //Send every n verticies (and do not send for the first loop through(when i == 0))
//				{
//					k = 0; //Reset, since arrayForPayload[] must always be written within his range (range of VERTEX_ARRAY_ELEMENTS_SEND_IN_ONE_PACKET)
//					NormalArrayPacket* normalArrayPacket = new NormalArrayPacket(arrayForPayload, numberOfAllNormals, numberOfCurrentPacket); //TODO: Memory Hole
//					m_networkManager->sendPacketOnAllConnectedSockets(normalArrayPacket); // packet send
//					++numberOfCurrentPacket;
//				}
//				arrayForPayload[3 * k] = normalArray->at(i).x();
//				arrayForPayload[3 * k + 1] = normalArray->at(i).y();
//				arrayForPayload[3 * k + 2] = normalArray->at(i).z();
//				++k;
//			}
//
//			//If there is a "division rest" then send the last packet (the for loop only send a multiplier of VERTEX_ARRAY_ELEMENTS_SEND_IN_ONE_PACKET elements)
//			if (numberOfAllNormals % (VERTEX_ARRAY_ELEMENTS_SEND_IN_ONE_PACKET / 3) > 0)
//			{
//				unsigned int lastPacketWasSendOnArrayID = numberOfCurrentPacket * (VERTEX_ARRAY_ELEMENTS_SEND_IN_ONE_PACKET / 3);
//				unsigned int pendingArrayIDsForTransmitting = numberOfAllNormals % (VERTEX_ARRAY_ELEMENTS_SEND_IN_ONE_PACKET / 3);
//				k = 0;
//				for (unsigned int i = lastPacketWasSendOnArrayID; i < lastPacketWasSendOnArrayID + pendingArrayIDsForTransmitting; ++i)
//				{
//					arrayForPayload[3 * k] = normalArray->at(i).x();
//					arrayForPayload[3 * k + 1] = normalArray->at(i).y();
//					arrayForPayload[3 * k + 2] = normalArray->at(i).z();
//					++k;
//				}
//				NormalArrayPacket* normalArrayPacket = new NormalArrayPacket(arrayForPayload, numberOfAllNormals, numberOfCurrentPacket); //TODO: Memory Hole
//				m_networkManager->sendPacketOnAllConnectedSockets(normalArrayPacket); // packet send
//			}
//		}
//	}
//}
//
//
//void GeometryPacketSender::sendGeometryRakNet(GeometryData* geometryDataIn)
//{
//	// ##############################################################################################
//	// ######################################## VERTEX ARRAY ########################################
//	// ##############################################################################################
//	{
//		osg::Vec3Array *vertexArray = dynamic_cast<osg::Vec3Array*>(geometryDataIn->getOpenMeshGeo()->getVertexArray());
//		float arrayForPayload[VERTEX_ARRAY_ELEMENTS_SEND_IN_ONE_PACKET_RAKNET];
//		unsigned int numberOfAllVertices = geometryDataIn->getOpenMeshGeo()->getVertexArray()->getNumElements();
//
//		unsigned int numberOfCurrentPacket = 0;
//		unsigned int k = 0; // Helper variable for filling a float[] like a Vec3Array
//		for (unsigned int i = 0; i < numberOfAllVertices; ++i)
//		{
//			if ((i % (VERTEX_ARRAY_ELEMENTS_SEND_IN_ONE_PACKET_RAKNET / 3) == 0) && (i != 0)) //Send every n verticies (and do not send for the first loop through(when i == 0))
//			{
//				k = 0; //Reset, since arrayForPayload[] must always be written within his range (range of VERTEX_ARRAY_ELEMENTS_SEND_IN_ONE_PACKET)
//				VertexArrayPacketPayloadRakNet* vapp = new VertexArrayPacketPayloadRakNet();
//				vapp->payloadType = (RakNet::MessageID)VERTEX_ARRAY;
//				vapp->numberOfCurrentPacket = numberOfCurrentPacket;
//				vapp->numberOfAllElements = numberOfAllVertices;
//				std::copy(arrayForPayload, arrayForPayload + VERTEX_ARRAY_ELEMENTS_SEND_IN_ONE_PACKET_RAKNET, vapp->vertexArray);
//				m_networkManagerRakNet->sendPacketOnAllConnectedSockets(vapp); // packet send
//
//				//VertexArrayPacket* vertexArrayPacket = new VertexArrayPacket(arrayForPayload, numberOfAllVertices, numberOfCurrentPacket); //TODO: Memory Hole
//				//m_networkManager->sendPacketOnAllConnectedSockets(vertexArrayPacket); // packet send
//				++numberOfCurrentPacket;
//			}
//			arrayForPayload[3 * k] = vertexArray->at(i).x();
//			arrayForPayload[3 * k + 1] = vertexArray->at(i).y();
//			arrayForPayload[3 * k + 2] = vertexArray->at(i).z();
//			++k;
//		}
//
//		//If there is a "division rest" then send the last packet (the for loop only send a multiplier of VERTEX_ARRAY_ELEMENTS_SEND_IN_ONE_PACKET elements)
//		if (numberOfAllVertices % (VERTEX_ARRAY_ELEMENTS_SEND_IN_ONE_PACKET / 3) > 0)
//		{
//			unsigned int lastPacketWasSendOnArrayID = numberOfCurrentPacket * (VERTEX_ARRAY_ELEMENTS_SEND_IN_ONE_PACKET / 3);
//			unsigned int pendingArrayIDsForTransmitting = numberOfAllVertices % (VERTEX_ARRAY_ELEMENTS_SEND_IN_ONE_PACKET / 3);
//			k = 0;
//			for (unsigned int i = lastPacketWasSendOnArrayID; i < lastPacketWasSendOnArrayID + pendingArrayIDsForTransmitting; ++i)
//			{
//				arrayForPayload[3 * k] = vertexArray->at(i).x();
//				arrayForPayload[3 * k + 1] = vertexArray->at(i).y();
//				arrayForPayload[3 * k + 2] = vertexArray->at(i).z();
//				++k;
//			}
//			VertexArrayPacketPayloadRakNet* vapp = new VertexArrayPacketPayloadRakNet();
//			vapp->payloadType = (RakNet::MessageID)VERTEX_ARRAY;
//			vapp->numberOfCurrentPacket = numberOfCurrentPacket;
//			vapp->numberOfAllElements = numberOfAllVertices;
//			std::copy(arrayForPayload, arrayForPayload + VERTEX_ARRAY_ELEMENTS_SEND_IN_ONE_PACKET, vapp->vertexArray);
//			m_networkManagerRakNet->sendPacketOnAllConnectedSockets(vapp); // packet send
//			//VertexArrayPacket* vertexArrayPacket = new VertexArrayPacket(arrayForPayload, numberOfAllVertices, numberOfCurrentPacket); //TODO: Memory Hole
//			//m_networkManager->sendPacketOnAllConnectedSockets(vertexArrayPacket); // packet send
//		}
//	}
//
//}


//THE OSC WAY OF TRANSMISSION
void GeometryPacketSender::sendGeometryOSC(GeometryData* geometryDataIn)
{
		osg::Vec3Array *vertexArray = dynamic_cast<osg::Vec3Array*>(geometryDataIn->getOpenMeshGeo()->getVertexArray());
		m_networkManagerOSC->sendVertexArray(vertexArray);

		osg::DrawElementsUInt *indexArray = dynamic_cast<osg::DrawElementsUInt*>(geometryDataIn->getOpenMeshGeo()->getPrimitiveSet(0)->getDrawElements());
		m_networkManagerOSC->sendIndexArray(indexArray);

		osg::Vec4Array *colorArray = dynamic_cast<osg::Vec4Array*>(geometryDataIn->getOpenMeshGeo()->getColorArray());
		m_networkManagerOSC->sendColorArray(colorArray);

		osg::Vec3Array *normalArray = dynamic_cast<osg::Vec3Array*>(geometryDataIn->getOpenMeshGeo()->getNormalArray());
		m_networkManagerOSC->sendNormalArray(normalArray);
}