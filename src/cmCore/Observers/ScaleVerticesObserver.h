// ClayMore - Immersive Mesh Modelling --- Copyright (c) 2014-2017 Philipp Ladwig, Jannik Fiedler, Jan Beutgen
#pragma once
#include <iostream>
#include <osg/PositionAttitudeTransform>
#include "../../cmInteractionManagement/Observer.h"

#include "cmGeometryManagement\Constraints.h"

class GeometryManager;
class MenuManager;
class InteractionManager;
class MiscDataStorage;
class HistoryManager;

class ScaleVerticesObserver : public Observer
{
public:
	ScaleVerticesObserver();
	~ScaleVerticesObserver();

	inline void setGeometryManager(osg::ref_ptr<GeometryManager> geometryManager) { m_geometryManager = geometryManager; };
	inline void setMenuManager(MenuManager* menuManager) { m_menuManager = menuManager; };
	inline void setInteractionManager(InteractionManager* interactionManager) { m_interactionManager = interactionManager; };
	inline void setMiscDataStorage(MiscDataStorage* miscDataStorageIn) { m_miscDataStorage = miscDataStorageIn; };
	inline void setHistoryManager(HistoryManager* historyManagerIn) { m_historyManager = historyManagerIn; };

	inline void waitAndDoNothingUntilTriggerPressedAgain(bool valueIn) { m_waitTriggerStateHasChanged = valueIn; }

protected:
	bool notify(Controller* registeredControllerIn, Controller* otherControllerIn, BUTTON_STATE registeredButtonState);

private:
	//Just a function for code reducing
	void start(Controller* registeredControllerIn, Controller* otherControllerIn);

	MenuManager* m_menuManager;
	osg::ref_ptr<GeometryManager> m_geometryManager;
	InteractionManager* m_interactionManager;
	MiscDataStorage* m_miscDataStorage;
	HistoryManager* m_historyManager;

	float m_distanceForScale;
	Constraints m_lastConstraint;

	bool m_waitTriggerStateHasChanged;
	osg::Matrix m_local2WorldMatrixControllerAtStartGrab;
};