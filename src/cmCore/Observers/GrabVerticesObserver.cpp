// ClayMore - Immersive Mesh Modelling --- Copyright (c) 2014-2017 Philipp Ladwig, Jannik Fiedler, Jan Beutgen
#include "GrabVerticesObserver.h"

#include "cmGeometryManagement\GeometryManager.h"
#include "cmMenuManagement\MenuManager.h"
#include "cmInteractionManagement\InteractionManager.h"
#include "../../cmUtil/MiscDataStorage.h"
#include "../HistoryManager.h"

#include "..\ControllerTools\VisualizeAxisAndGridControllerTool.h"
#include "..\ControllerTools\VisualizeBarycentrumControllerTool.h"
#include "..\ControllerTools\RulerControllerTool.h"

#include "cmMenuManagement\MenuObservers\TouchMenuObserver.h"

//#include <osg/io_utils> //for "c-outing" osg::matrices

GrabVerticesObserver::GrabVerticesObserver()
{

	m_requiredControllerTools.push_back(ControllerTool::VISUALIZE_AXIS_AND_GRID);
	m_requiredControllerTools.push_back(ControllerTool::VISUALIZE_BARYCENTRUM);
	m_requiredControllerTools.push_back(ControllerTool::RULER);
	m_waitTriggerStateHasChanged = false;
}

GrabVerticesObserver::~GrabVerticesObserver()
{

}

void GrabVerticesObserver::start(Controller* registeredControllerIn, Controller* otherControllerIn)
{
	m_geometryManager->grabVerticesStart(registeredControllerIn->getMatrixTransform()->getMatrix().getTrans());
	m_menuManager->hideMenuWithUniqueName(TOUCH_EDIMODE_TRANS_ROT_SCALE);
	m_menuManager->showMenuWithUniqueName(TOUCH_CONSTRAINT);

	TouchMenuObserver *touchMenuObserver = new TouchMenuObserver(TOUCH_CONSTRAINT, m_menuManager, m_miscDataStorage);
	m_interactionManager->registerObserver(CONTROLLER_TYPE::PRIMARY, Observer::JOYSTICK, touchMenuObserver);

	//m_geometryManager->setConstraint(Constraints::NO_CONSTRAIN);

	m_miscDataStorage->setLastBaryCentrum(m_geometryManager->getBarycentrumOfSELECTEDVertsForActiveGeomData());

	m_visualizeAxisAndGridControllerTool = static_cast<VisualizeAxisAndGridControllerTool*>(registeredControllerIn->getControllerTool(ControllerTool::VISUALIZE_AXIS_AND_GRID));
	m_startGrabPosition = registeredControllerIn->getPosition();

	//if (m_geometryManager->getConstraint() != Constraints::NO_CONSTRAIN)
	//{
	//	m_visualizeAxisAndGridControllerTool->update(m_geometryManager->isSnappingActive(), m_geometryManager->getConstraint(), false, m_miscDataStorage->getLastBaryCentrum(), deltaPosition);
	//}

	m_rulerControllerTool = static_cast<RulerControllerTool*>(registeredControllerIn->getControllerTool(ControllerTool::RULER));
	m_rulerControllerTool->hide();
}

bool GrabVerticesObserver::notify(Controller* registeredControllerIn, Controller* otherControllerIn, BUTTON_STATE registeredButtonState)
{
	if (!m_waitTriggerStateHasChanged)
	{
		VisualizeBarycentrumControllerTool* visualizeBarycentrumControllerTool = static_cast<VisualizeBarycentrumControllerTool*>(registeredControllerIn->getControllerTool(ControllerTool::VISUALIZE_BARYCENTRUM));
		visualizeBarycentrumControllerTool->setGeometryManager(m_geometryManager);

		if (m_geometryManager->getActiveGeometryData() != NULL &&	registeredControllerIn->getButtonState(BUTTON_TYPE::REAR_TRIGGER) == PUSH)
		{
			start(registeredControllerIn, otherControllerIn);

		}
		if (m_geometryManager->getActiveGeometryData() != NULL &&	registeredControllerIn->getButtonState(BUTTON_TYPE::REAR_TRIGGER) == HOLD)
		{
			osg::Matrix matrixWorldToLocal = osg::computeWorldToLocal(m_geometryManager->getActiveGeometryData()->getParentalNodePaths()[0]);
			m_geometryManager->grabVertices(registeredControllerIn->getMatrixTransform()->getMatrix().getTrans(), true, matrixWorldToLocal);

			osg::Vec3f deltaPosition = registeredControllerIn->getPosition() - m_startGrabPosition;
			deltaPosition = matrixWorldToLocal.getRotate() * deltaPosition;
			//std::cout << "CurrentConstraint: " << m_geometryManager->getConstraint() << std::endl;
			m_visualizeAxisAndGridControllerTool->update(m_geometryManager->isSnappingActive(), m_geometryManager->getConstraint(),
				false, m_miscDataStorage->getLastBaryCentrum(), deltaPosition);

			visualizeBarycentrumControllerTool->updateCross();

			m_rulerControllerTool->setShowRulerLine(false);
			//TODO: Find a better way instead of always calculating the barycentrum just only for the ruler tool, it's to expen
			m_rulerControllerTool->setStartAndEndPoint(m_miscDataStorage->getLastBaryCentrum(), m_geometryManager->getBarycentrumOfSELECTEDVertsForActiveGeomData());
			//true is only for changing function type from update() to another. Update() will always be called...update(bool) is "own created"
			m_rulerControllerTool->update(true);

		}
		if (m_geometryManager->getActiveGeometryData() != NULL &&	registeredControllerIn->getButtonState(BUTTON_TYPE::REAR_TRIGGER) == RELEASE)
		{
			m_rulerControllerTool->hide();
			m_visualizeAxisAndGridControllerTool->removeLines();
			visualizeBarycentrumControllerTool->removeLines();

			m_menuManager->hideMenuWithUniqueName(TOUCH_CONSTRAINT);
			m_menuManager->showMenuWithUniqueName(TOUCH_EDIMODE_TRANS_ROT_SCALE);

			TouchMenuObserver *touchMenuObserver = new TouchMenuObserver(TOUCH_EDIMODE_TRANS_ROT_SCALE, m_menuManager, m_miscDataStorage);
			m_interactionManager->registerObserver(CONTROLLER_TYPE::PRIMARY, Observer::PRIMARY_JOYSTICK_MENU, touchMenuObserver);

			m_geometryManager->getActiveGeometryData()->updateBoundingBox();
			m_historyManager->addAction(HistoryAction(ActionType::EDI_GRAB));
		}
	}
	else
	{
		if (registeredControllerIn->getButtonState(BUTTON_TYPE::REAR_TRIGGER) == PUSH)
		{
			start(registeredControllerIn, otherControllerIn);
			m_waitTriggerStateHasChanged = false;
		}
	}
	return false;
}
