// ClayMore - Immersive Mesh Modelling --- Copyright (c) 2014-2017 Philipp Ladwig, Jannik Fiedler, Jan Beutgen
#pragma once
#include <iostream>

#include "../../cmInteractionManagement/Observer.h"

class DebugObserver : public Observer
{
	public:
		inline DebugObserver(std::string debugMessage) : m_debugMessage(debugMessage) { }
		inline ~DebugObserver() {}

	protected:
		std::string const m_debugMessage;
		bool notify(Controller* registeredControllerIn, Controller* otherControllerIn, BUTTON_STATE registeredButtonState) 
		{ 
			std::cout << m_debugMessage <<  " State is " 
			<< registeredButtonState << std::endl; 
			return true;
		}
};

