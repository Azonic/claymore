// ClayMore - Immersive Mesh Modelling --- Copyright (c) 2014-2017 Philipp Ladwig, Jannik Fiedler, Jan Beutgen
#pragma once
#include <iostream>
#include <osg/PositionAttitudeTransform>
#include "../../cmInteractionManagement/Observer.h"

class GeometryManager;
class MenuManager;
class InteractionManager;
class HistoryManager;
class MiscDataStorage;

class RotateVerticesObserver : public Observer
{
public:
	RotateVerticesObserver();
	~RotateVerticesObserver();

	void start(Controller* registeredControllerIn, Controller* otherControllerIn, BUTTON_STATE registeredButtonState);

	inline void setGeometryManager(osg::ref_ptr<GeometryManager> geometryManager) { m_geometryManager = geometryManager; };
	inline void setMenuManager(MenuManager* menuManager) { m_menuManager = menuManager; };
	inline void setInteractionManager(InteractionManager* interactionManager) { m_interactionManager = interactionManager; };
	inline void setMiscDataStorage(MiscDataStorage* miscDataStorageIn) { m_miscDataStorage = miscDataStorageIn; };
	inline void setHistoryManager(HistoryManager* historyManagerIn) { m_historyManager = historyManagerIn; };

	inline void waitAndDoNothingUntilTriggerPressedAgain(bool valueIn) { m_waitTriggerStateHasChanged = valueIn; }

protected:
	bool notify(Controller* registeredControllerIn, Controller* otherControllerIn, BUTTON_STATE registeredButtonState);

private:
	osg::ref_ptr<GeometryManager> m_geometryManager;
	MenuManager* m_menuManager;
	InteractionManager* m_interactionManager;
	MiscDataStorage* m_miscDataStorage;
	HistoryManager* m_historyManager;
	
	bool m_waitTriggerStateHasChanged;

	osg::Quat m_startRotMat;
	osg::Vec3d m_barycentrumStart;
};