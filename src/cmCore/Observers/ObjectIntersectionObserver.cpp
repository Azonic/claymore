// ClayMore - Immersive Mesh Modelling --- Copyright (c) 2014-2017 Philipp Ladwig, Jannik Fiedler, Jan Beutgen
#include "ObjectIntersectionObserver.h"
#include "cmGeometryManagement\GeometryManager.h"
#include "..\ControllerTools\PickRayControllerTool.h"

ObjectIntersectionObserver::ObjectIntersectionObserver()
{
	m_requiredControllerTools.push_back(ControllerTool::PICK_RAY);
}

ObjectIntersectionObserver::~ObjectIntersectionObserver()
{

}

bool ObjectIntersectionObserver::notify(Controller* registeredControllerIn, Controller* otherControllerIn, BUTTON_STATE registeredButtonState)
{
	osg::Vec3f dir = osg::Vec3f(0.0, 0.0, 50.0) - registeredControllerIn->getMatrixTransform()->getMatrix().getTrans();
	dir = registeredControllerIn->getMatrixTransform()->getMatrix().getRotate() * dir;
	osg::Vec3f lineEndPosition = dir + registeredControllerIn->getMatrixTransform()->getMatrix().getTrans();

	std::string nameOfIntersectedObject = m_geometryManager->checkIntersectionWithGeometry(
		registeredControllerIn->getMatrixTransform()->getMatrix().getTrans(),
		registeredControllerIn->getMatrixTransform()->getMatrix().getTrans() + registeredControllerIn->getMatrixTransform()->getMatrix().getRotate() * osg::Vec3f(0.0, 0.0, -50.0),
		&m_isIntersectionFound);

	if (registeredControllerIn->getButtonState(REAR_TRIGGER) == PUSH)
	{
		if (m_isIntersectionFound)
		{
			m_geometryManager->setActiveGeometry(nameOfIntersectedObject);
			m_geometryManager->getGeometryDataByName(nameOfIntersectedObject)->setIsSelected(true);

			//Selection of multiple objects with hold down squeeze button
			if (registeredControllerIn->getButtonState(BUTTON_TYPE::SQUEEZE) == HOLD)
			{
				//A list of all geometries will be iterated through
				for (unsigned int i = 0; i < m_geometryManager->getNumChildren(); i++)
				{
					if (dynamic_cast<GeometryData*>(m_geometryManager->getChild(i))->getIsSelected())
					{
						dynamic_cast<GeometryData*>(m_geometryManager->getChild(i))->getGeometryVisualization()->selectTechnique(INDICATED_AS_SELECTED_SHADING);
					}
					else
					{
						dynamic_cast<GeometryData*>(m_geometryManager->getChild(i))->getGeometryVisualization()->selectTechnique(FLAT_SHADING);
					}
				}
			}
			else
			{
				//A list of all geometries will be iterated through
				for (unsigned int i = 0; i < m_geometryManager->getNumChildren(); i++)
				{
					dynamic_cast<GeometryData*>(m_geometryManager->getChild(i))->setIsSelected(false);
					dynamic_cast<GeometryData*>(m_geometryManager->getChild(i))->getGeometryVisualization()->selectTechnique(FLAT_SHADING);
				}
				m_geometryManager->getGeometryDataByName(nameOfIntersectedObject)->setIsSelected(true);
				m_geometryManager->getGeometryDataByName(nameOfIntersectedObject)->getGeometryVisualization()->selectTechnique(INDICATED_AS_SELECTED_SHADING);
			}
			m_geometryManager->getGeometryDataByName(nameOfIntersectedObject)->getGeometryVisualization()->selectTechnique(INDICATED_AS_SELECTED_AND_ACTIVE_SHADING);
			
		}
	}
	if (registeredControllerIn->getButtonState(REAR_TRIGGER) == IDLE)
	{
		//TODO: Theoreticially here must be function call for the Controller tool to color the pickray or toggle a shader of the object
		//dynamic_cast<PickRayControllerTool*>(m_controllerTool)->update(m_isIntersectionFound);
		//SphereControllerTool* sphereControllerTool = static_cast<SphereControllerTool*>(registeredControllerIn->getControllerTool(ControllerTool::SPHERE));
	}
	return true;
}
