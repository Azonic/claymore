// ClayMore - Immersive Mesh Modelling --- Copyright (c) 2014-2017 Philipp Ladwig, Jannik Fiedler, Jan Beutgen
#pragma once
#include <iostream>
#include <osg/PositionAttitudeTransform>
#include "../../cmInteractionManagement/Observer.h"

#include "../../cmOpenMeshBinding/OpenMeshGeometry.h"

class GeometryManager;

class AddEdgeLineObserver : public Observer
{
public:
	AddEdgeLineObserver();
	~AddEdgeLineObserver();

	inline void setGeometryManager(osg::ref_ptr<GeometryManager> geometryManager) { m_geometryManager = geometryManager; };

	void resetCounter();

protected:
	bool notify(Controller* registeredControllerIn, Controller* otherControllerIn, BUTTON_STATE registeredButtonState);

private:
	osg::ref_ptr<GeometryManager> m_geometryManager;
	size_t m_iter;
	int m_lastVhIdx;
};