// ClayMore - Immersive Mesh Modelling --- Copyright (c) 2014-2017 Philipp Ladwig, Jannik Fiedler, Jan Beutgen
#pragma once
#include <iostream>

#include <osg/PositionAttitudeTransform>

#include "../../cmInteractionManagement/Observer.h"

class GeometryManager;
class HistoryManager;

//Observer for scaling the entire active geometry with both squeeze buttons
class ScaleSelectedObjectSqueezeObserver : public Observer
{
public:
	//Info about who is prim or sec is necessary for managing the different states of both controller-squeeze-buttons
	ScaleSelectedObjectSqueezeObserver(bool m_isObservingToPrimaryControllerIn);
	~ScaleSelectedObjectSqueezeObserver();

	inline void setGeometryManager(osg::ref_ptr<GeometryManager> geometryManager) { m_geometryManager = geometryManager; };
	//Scaling is also a meshHistoryAction
	inline void setHistoryManager(HistoryManager* historyManagerIn) { m_historyManager = historyManagerIn; };

	//initialiseStart can be evoked at two positions in notify() -> code reduction function
	void initialiseStart(Controller* registeredControllerIn, Controller* otherControllerIn, BUTTON_STATE registeredButtonState);
	//hideControllerTools can be evoked at two positions in notify() -> code reduction function
	void hideControllerTools(Controller* registeredControllerIn, Controller* otherControllerIn, BUTTON_STATE registeredButtonState);
	

protected:

	bool notify(Controller* registeredControllerIn, Controller* otherControllerIn, BUTTON_STATE registeredButtonState);

private:

	osg::ref_ptr<GeometryManager> m_geometryManager;
	HistoryManager* m_historyManager;

	//Distance between both controller
	double m_distanceForScale;

	osg::Vec3d m_startScale;

	//Info about who is prim or sec is necessary for managing the different states of both controller-squeeze-buttons
	bool m_isObservingToPrimaryController;

	//Needed, if primary controller squeeze button is already pressed (HOLD state), but secondary not. ...
	//...With this bool a PUSHED state for primary button is emulated
	bool m_initialiseStartIsAlreadyTriggered;
	
	osg::Vec3d m_barycentrumStart;
	osg::Quat m_startQuatForRotation;
	bool m_isRotatedOnce;
	double m_lastSnappedAngY;
};

