// ClayMore - Immersive Mesh Modelling --- Copyright (c) 2014-2017 Philipp Ladwig, Jannik Fiedler, Jan Beutgen
#include "SelectVerticesObserver.h"
#include "cmGeometryManagement\GeometryManager.h"
#include "../../cmUtil/MiscDataStorage.h"
#include "../ControllerTools/SphereControllerTool.h"



SelectVerticesObserver::SelectVerticesObserver()
{
	m_requiredControllerTools.push_back(ControllerTool::SPHERE);
	m_waitTriggerStateHasChanged = false;
}

SelectVerticesObserver::~SelectVerticesObserver()
{
}

bool SelectVerticesObserver::notify(Controller* registeredControllerIn, Controller* otherControllerIn, BUTTON_STATE registeredButtonState)
{
	//std::cout << "NOTIFY SVO" << std::endl;
	if (!m_waitTriggerStateHasChanged)
	{
		if (m_geometryManager->getActiveGeometryData() != NULL && registeredControllerIn->getButtonState(BUTTON_TYPE::REAR_TRIGGER) == HOLD)
		{
			SphereControllerTool* sphereControllerTool = static_cast<SphereControllerTool*>(registeredControllerIn->getControllerTool(ControllerTool::SPHERE));
			m_geometryManager->sphereSelectVertexConfirm(registeredControllerIn->getMatrixTransform()->getMatrix(), 0.025f * sphereControllerTool->getSphereScaleValue());

			MiscDataStorage* miscDataStorage = m_geometryManager->getMiscDataStorage();
			if (miscDataStorage == nullptr)
			{
				std::cout << "\n############################################################ \nSelectVerticesObserver NULLPTR in SelectVerticesObserver \n############################################################" << std::endl;
			}
			else
			{
				if (*miscDataStorage->getRumbleShortState())
				{
					registeredControllerIn->triggerHapticEvent(HAPTIC_EVENT::JOYSTICK_TINY);
					miscDataStorage->setRumbleShortState(false);
				}
			}
		}
	}
	else
	{
		if (registeredControllerIn->getButtonState(BUTTON_TYPE::REAR_TRIGGER) == PUSH)
		{
			//start(registeredControllerIn, otherControllerIn);
			m_waitTriggerStateHasChanged = false;
		}
	}

	return true;
}