// ClayMore - Immersive Mesh Modelling --- Copyright (c) 2014-2017 Philipp Ladwig, Jannik Fiedler, Jan Beutgen
#include "ScaleVerticesObserver.h"

#include "cmGeometryManagement\GeometryManager.h"
#include "cmMenuManagement\MenuManager.h"
#include "cmInteractionManagement\InteractionManager.h"
#include "../../cmUtil/MiscDataStorage.h"
#include "../HistoryManager.h"

#include "../ControllerTools/visualizeAxisAndGridControllerTool.h"
#include "../ControllerTools/VisualizeBarycentrumControllerTool.h"
#include "../ControllerTools/DashedLineControllerTool.h"
#include "../ControllerTools/RulerControllerTool.h"

#include "cmMenuManagement\MenuObservers\TouchMenuObserver.h"

ScaleVerticesObserver::ScaleVerticesObserver()
{
	m_requiredControllerTools.push_back(ControllerTool::VISUALIZE_AXIS_AND_GRID);
	m_requiredControllerTools.push_back(ControllerTool::VISUALIZE_BARYCENTRUM);
	m_requiredControllerTools.push_back(ControllerTool::DASHED_LINE);
	m_requiredControllerTools.push_back(ControllerTool::RULER);
}

ScaleVerticesObserver::~ScaleVerticesObserver()
{

}

void ScaleVerticesObserver::start(Controller* registeredControllerIn, Controller* otherControllerIn)
{


	DashedLineControllerTool* dashedLineControllerTool = static_cast<DashedLineControllerTool*>(registeredControllerIn->getControllerTool(ControllerTool::DASHED_LINE));

	m_lastConstraint = m_geometryManager->getConstraint();
	//Draw a dashed line from controller to barycentrum
	dashedLineControllerTool->setStartPoint(registeredControllerIn->getPosition());

	//osg::Vec3f barycentrumDashedLine = m_geometryManager->getBarycentrumOfSELECTEDVertsForActiveGeomData();
	////osg::Matrix local2WorldMatrixController = osg::computeLocalToWorld(m_matrixTransform->getParentalNodePaths()[0]);
	//osg::Matrix local2WorldMatrixGeom = osg::computeLocalToWorld(m_geometryManager->getActiveGeometryData()->getParentalNodePaths()[0]);
	dashedLineControllerTool->setEndPoint(otherControllerIn->getMatrixTransform()->getMatrix().getTrans());
	//----------

	//osg::Quat rotation = m_geometryManager->getActiveGeometryData()->getAttitude();
	//osg::Vec3f barycentrum = rotation * m_geometryManager->getBarycentrumOfSELECTEDVertsForActiveGeomData()
	//	+ m_geometryManager->getActiveGeometryData()->getPosition();

	m_distanceForScale = (registeredControllerIn->getPosition() - otherControllerIn->getMatrixTransform()->getMatrix().getTrans()).length();

	m_geometryManager->scaleSelectedVerticesStart();

	m_menuManager->hideMenuWithUniqueName(TOUCH_EDIMODE_TRANS_ROT_SCALE);
	m_menuManager->showMenuWithUniqueName(TOUCH_CONSTRAINT);

	TouchMenuObserver *touchMenuObserver = new TouchMenuObserver(TOUCH_CONSTRAINT, m_menuManager, m_miscDataStorage);
	m_interactionManager->registerObserver(CONTROLLER_TYPE::PRIMARY, Observer::JOYSTICK, touchMenuObserver);
	//m_geometryManager->setConstraint(Constraints::NO_CONSTRAIN);

	RulerControllerTool* rulerControllerTool = static_cast<RulerControllerTool*>(registeredControllerIn->getControllerTool(ControllerTool::RULER));
	rulerControllerTool->setStartScale(m_distanceForScale);
}

bool ScaleVerticesObserver::notify(Controller* registeredControllerIn, Controller* otherControllerIn, BUTTON_STATE registeredButtonState)
{
	if (!m_waitTriggerStateHasChanged)
	{
		DashedLineControllerTool* dashedLineControllerTool = static_cast<DashedLineControllerTool*>(registeredControllerIn->getControllerTool(ControllerTool::DASHED_LINE));

		VisualizeAxisAndGridControllerTool* visualizeAxisAndGridControllerTool = static_cast<VisualizeAxisAndGridControllerTool*>(registeredControllerIn->getControllerTool(ControllerTool::VISUALIZE_AXIS_AND_GRID));

		VisualizeBarycentrumControllerTool* visualizeBarycentrumControllerTool = static_cast<VisualizeBarycentrumControllerTool*>(registeredControllerIn->getControllerTool(ControllerTool::VISUALIZE_BARYCENTRUM));
		visualizeBarycentrumControllerTool->setGeometryManager(m_geometryManager);

		RulerControllerTool* rulerControllerTool = static_cast<RulerControllerTool*>(registeredControllerIn->getControllerTool(ControllerTool::RULER));

		if (m_geometryManager->getActiveGeometryData() != NULL &&	registeredButtonState == PUSH)
		{
			start(registeredControllerIn, otherControllerIn);
		}
		if (m_geometryManager->getActiveGeometryData() != NULL &&	registeredButtonState == HOLD)
		{
			//Resets the mesh, if constraint is changed (uses the last mesh in history manager)
			if (m_lastConstraint != m_geometryManager->getConstraint())
				m_geometryManager->resetScaleGeometry();


			m_lastConstraint = m_geometryManager->getConstraint();


			visualizeBarycentrumControllerTool->updateCross();

			dashedLineControllerTool->setStartPoint(registeredControllerIn->getPosition());

			//osg::Vec3f barycentrumDashedLine = m_geometryManager->getBarycentrumOfSELECTEDVertsForActiveGeomData();
			////osg::Matrix local2WorldMatrixController = osg::computeLocalToWorld(m_matrixTransform->getParentalNodePaths()[0]);
			//osg::Matrix local2WorldMatrixGeom = osg::computeLocalToWorld(m_geometryManager->getActiveGeometryData()->getParentalNodePaths()[0]);
			dashedLineControllerTool->setEndPoint(otherControllerIn->getMatrixTransform()->getMatrix().getTrans());
			dashedLineControllerTool->showLine();

			//EXP: m_miscDataStorage->getLastBaryCentrum() will only be used for constraint::GRID
			visualizeAxisAndGridControllerTool->update(m_geometryManager->isSnappingActive(), m_geometryManager->getConstraint(), false, m_miscDataStorage->getLastBaryCentrum(), osg::Vec3f(0.0, 0.0, 0.0));
			osg::Quat rotation = m_geometryManager->getActiveGeometryData()->getAttitude();
			osg::Vec3f barycentrum = rotation * m_geometryManager->getBarycentrumOfSELECTEDVertsForActiveGeomData()
				+ m_geometryManager->getActiveGeometryData()->getPosition();

			float currentDistance = (registeredControllerIn->getMatrixTransform()->getMatrix().getTrans() - otherControllerIn->getMatrixTransform()->getMatrix().getTrans()).length();



			rulerControllerTool->setCurrentScale(currentDistance);
			rulerControllerTool->setShowRulerLine(false);
			rulerControllerTool->update(true);


			//EXP: Example for scaling bigger: 2 * (1 - (3.0/2.9)) = -0,069
			//		ScalingVector in OpenMeshGeometry (the "sliding vector")  looks to barycentrum - this is why we need a negative value to scale bigger (away from barycentrum)

			//TODO: Insert: m_isLocalCoordSysSelected and  m_isLocalCoordSysSelected:
			//m_geometryManager->scaleSelectedVertices(2 * (1 - (currentDistance / m_distanceForScale)), m_isLocalCoordSysSelected, m_snapToAxis);

			m_geometryManager->scaleSelectedVertices((1 - (currentDistance / m_distanceForScale)), true);

			//std::cout << "currentDistance: " << currentDistance << "m_distanceForScale: " << m_distanceForScale << "scaleFactor:" << currentDistance / m_distanceForScale << std::endl;

			m_distanceForScale = currentDistance;
		}
		if (m_geometryManager->getActiveGeometryData() != NULL &&	registeredControllerIn->getButtonState(BUTTON_TYPE::REAR_TRIGGER) == RELEASE)
		{
			//TODO: RemoveLines() and hideLine()....are the same.... put a virtual function in ControllerTool.h like dismiss() or something similar
			dashedLineControllerTool->hideLine();
			visualizeBarycentrumControllerTool->removeLines();
			visualizeAxisAndGridControllerTool->removeLines();
			
			rulerControllerTool->hide();

			m_menuManager->hideMenuWithUniqueName(TOUCH_CONSTRAINT);
			m_menuManager->showMenuWithUniqueName(TOUCH_EDIMODE_TRANS_ROT_SCALE);

			TouchMenuObserver *touchMenuObserver = new TouchMenuObserver(TOUCH_EDIMODE_TRANS_ROT_SCALE, m_menuManager, m_miscDataStorage);
			m_interactionManager->registerObserver(CONTROLLER_TYPE::PRIMARY, Observer::JOYSTICK, touchMenuObserver);
			//m_geometryManager->resetConstraint();
			m_geometryManager->getActiveGeometryData()->updateBoundingBox();
			m_historyManager->addAction(HistoryAction(ActionType::EDI_SCALE));
		}
	}
	else
	{
		if (registeredControllerIn->getButtonState(BUTTON_TYPE::REAR_TRIGGER) == PUSH)
		{
			start(registeredControllerIn, otherControllerIn);
			m_waitTriggerStateHasChanged = false;
		}
	}

	return true;
}