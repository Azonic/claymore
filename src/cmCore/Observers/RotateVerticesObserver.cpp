// ClayMore - Immersive Mesh Modelling --- Copyright (c) 2014-2017 Philipp Ladwig, Jannik Fiedler, Jan Beutgen
#include "RotateVerticesObserver.h"
#include "cmGeometryManagement\GeometryManager.h"
#include "cmMenuManagement\MenuManager.h"
#include "cmInteractionManagement\InteractionManager.h"
#include "../../cmUtil/MiscDataStorage.h"
#include "../HistoryManager.h"

#include "cmMenuManagement\MenuObservers\TouchMenuObserver.h"

#include "../ControllerTools/visualizeAxisAndGridControllerTool.h"
#include "../ControllerTools/VisualizeBarycentrumControllerTool.h"
#include "../ControllerTools/DashedLineControllerTool.h"
#include "../ControllerTools/RulerControllerTool.h"

RotateVerticesObserver::RotateVerticesObserver()
{
	m_requiredControllerTools.push_back(ControllerTool::VISUALIZE_AXIS_AND_GRID);
	m_requiredControllerTools.push_back(ControllerTool::VISUALIZE_BARYCENTRUM);
	m_requiredControllerTools.push_back(ControllerTool::DASHED_LINE);
	m_requiredControllerTools.push_back(ControllerTool::RULER);
}

RotateVerticesObserver::~RotateVerticesObserver()
{

}

void RotateVerticesObserver::start(Controller* registeredControllerIn, Controller* otherControllerIn, BUTTON_STATE registeredButtonState)
{
	DashedLineControllerTool* dashedLineControllerTool = static_cast<DashedLineControllerTool*>(registeredControllerIn->getControllerTool(ControllerTool::DASHED_LINE));

	//Draw a dashed line from controller to barycentrum
	dashedLineControllerTool->setStartPoint(registeredControllerIn->getPosition());

	osg::Vec3f barycentrumDashedLine = m_geometryManager->getBarycentrumOfSELECTEDVertsForActiveGeomData();
	osg::Matrix local2WorldMatrixGeom = osg::computeLocalToWorld(m_geometryManager->getActiveGeometryData()->getParentalNodePaths()[0]);
	dashedLineControllerTool->setEndPoint(barycentrumDashedLine *  local2WorldMatrixGeom);

	//Set start position if button is in PUSH state
	m_startRotMat = registeredControllerIn->getRotation().inverse();
	//Set barycentrum start position if button is in PUSH state -> I can use the barycentrumDashedLine, because the barycentrum was already calculated there
	m_barycentrumStart = barycentrumDashedLine;
	//HistoryManager do a copy of the mesh
	m_geometryManager->rotateSelectedVerticesStart();

	m_menuManager->hideMenuWithUniqueName(TOUCH_EDIMODE_TRANS_ROT_SCALE);
	m_menuManager->showMenuWithUniqueName(TOUCH_CONSTRAINT);

	TouchMenuObserver *touchMenuObserver = new TouchMenuObserver(TOUCH_CONSTRAINT, m_menuManager, m_miscDataStorage);
	m_interactionManager->registerObserver(CONTROLLER_TYPE::PRIMARY, Observer::JOYSTICK, touchMenuObserver);
	//m_geometryManager->setConstraint(Constraints::NO_CONSTRAIN);
}

bool RotateVerticesObserver::notify(Controller* registeredControllerIn, Controller* otherControllerIn, BUTTON_STATE registeredButtonState)
{
	if (!m_waitTriggerStateHasChanged)
	{
		DashedLineControllerTool* dashedLineControllerTool = static_cast<DashedLineControllerTool*>(registeredControllerIn->getControllerTool(ControllerTool::DASHED_LINE));

		VisualizeAxisAndGridControllerTool* visualizeAxisAndGridControllerTool = static_cast<VisualizeAxisAndGridControllerTool*>(registeredControllerIn->getControllerTool(ControllerTool::VISUALIZE_AXIS_AND_GRID));

		VisualizeBarycentrumControllerTool* visualizeBarycentrumControllerTool = static_cast<VisualizeBarycentrumControllerTool*>(registeredControllerIn->getControllerTool(ControllerTool::VISUALIZE_BARYCENTRUM));
		visualizeBarycentrumControllerTool->setGeometryManager(m_geometryManager);

		RulerControllerTool* rulerControllerTool = static_cast<RulerControllerTool*>(registeredControllerIn->getControllerTool(ControllerTool::RULER));
		//m_rulerControllerTool->hide();

		//TODO4: Make more efficient if else instead of 3 ifs
		if (m_geometryManager->getActiveGeometryData() != NULL &&	registeredButtonState == PUSH)
		{
			start(registeredControllerIn, otherControllerIn, registeredButtonState);
		}
		if (m_geometryManager->getActiveGeometryData() != NULL &&	registeredButtonState == HOLD)
		{
			visualizeBarycentrumControllerTool->updateCross();

			//Update one end of the dashed line every frame if button is hold
			dashedLineControllerTool->setStartPoint(registeredControllerIn->getPosition());

			rulerControllerTool->setStartAndEndRotation(m_startRotMat, m_startRotMat * registeredControllerIn->getRotation());
			rulerControllerTool->setShowRulerLine(false);
			rulerControllerTool->update(true);

			osg::Vec3f barycentrumDashedLine = m_geometryManager->getBarycentrumOfSELECTEDVertsForActiveGeomData();
			//osg::Matrix local2WorldMatrixController = osg::computeLocalToWorld(m_matrixTransform->getParentalNodePaths()[0]);
			osg::Matrix local2WorldMatrixGeom = osg::computeLocalToWorld(m_geometryManager->getActiveGeometryData()->getParentalNodePaths()[0]);
			dashedLineControllerTool->setEndPoint(barycentrumDashedLine *  local2WorldMatrixGeom);
			dashedLineControllerTool->showLine();

			visualizeAxisAndGridControllerTool->update(m_geometryManager->isSnappingActive(), m_geometryManager->getConstraint(), false, barycentrumDashedLine, osg::Vec3f(0.0, 0.0, 0.0));
			osg::Quat rotation = m_geometryManager->getActiveGeometryData()->getAttitude();
			osg::Vec3f barycentrum = rotation * m_geometryManager->getBarycentrumOfSELECTEDVertsForActiveGeomData()
				+ m_geometryManager->getActiveGeometryData()->getPosition();

			m_geometryManager->rotateSelectedVertices(osg::Matrixd(m_startRotMat * registeredControllerIn->getRotation()), m_barycentrumStart);

			//EXP: Save current rotation state for calculating in the next call of this function the difference between the last and the current rotation of the controller
			//m_deltaRotMat = newRotMatForDeltaRotation;
		}
		if (m_geometryManager->getActiveGeometryData() != NULL && registeredButtonState == RELEASE)
		{
			rulerControllerTool->hide();
			dashedLineControllerTool->hideLine();
			visualizeBarycentrumControllerTool->removeLines();
			visualizeAxisAndGridControllerTool->removeLines();

			m_menuManager->hideMenuWithUniqueName(TOUCH_CONSTRAINT);
			m_menuManager->showMenuWithUniqueName(TOUCH_EDIMODE_TRANS_ROT_SCALE);

			TouchMenuObserver *touchMenuObserver = new TouchMenuObserver(TOUCH_EDIMODE_TRANS_ROT_SCALE, m_menuManager, m_miscDataStorage);
			m_interactionManager->registerObserver(CONTROLLER_TYPE::PRIMARY, Observer::JOYSTICK, touchMenuObserver);
			//m_geometryManager->resetConstraint();

			m_geometryManager->getActiveGeometryData()->updateBoundingBox();
			m_historyManager->addAction(HistoryAction(ActionType::EDI_ROTATE));
		}
	}
	else
	{
		if (registeredControllerIn->getButtonState(BUTTON_TYPE::REAR_TRIGGER) == PUSH)
		{
			start(registeredControllerIn, otherControllerIn, registeredButtonState);
			m_waitTriggerStateHasChanged = false;
		}
	}
	return true;
}