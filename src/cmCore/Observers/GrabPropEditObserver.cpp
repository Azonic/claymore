// ClayMore - Immersive Mesh Modelling --- Copyright (c) 2014-2017 Philipp Ladwig, Jannik Fiedler, Jan Beutgen
#include "GrabPropEditObserver.h"
#include "cmGeometryManagement\GeometryManager.h"
#include "../../cmCore/ControllerTools/SphereControllerTool.h"

GrabPropEditObserver::GrabPropEditObserver()
{
	m_constrain = Constraints::NO_CONSTRAIN;
	m_requiredControllerTools.push_back(ControllerTool::SPHERE);
}

GrabPropEditObserver::~GrabPropEditObserver()
{

}

//void GrabPropEditObserver::setSize(float sizeIn)
//{
//	m_geometryManager->setProbEditRadius(sizeIn);
//	m_geometryManager->grabProbEditVerticesSetValues(m_geometryManager->getProbEditRadius );
//}

bool GrabPropEditObserver::notify(Controller* registeredControllerIn, Controller* otherControllerIn, BUTTON_STATE registeredButtonState)
{
	//m_geometryManager->grabProbEditVerticesSetValues(10.0, 1.25);
	
	if (m_geometryManager->getActiveGeometryData() != NULL &&	registeredButtonState == PUSH)
	{
		//m_geometryManager->grabProbEditVerticesStart(registeredControllerIn->getMatrixTransform()->getMatrix().getTrans());
		m_geometryManager->grabProbEditVerticesStart(registeredControllerIn->getMatrixTransform()->getMatrix().getTrans());
		m_constrain = Constraints::NO_CONSTRAIN;

		//TODO: Finish the work for attaching the spherecontroller tool at the barycentrum of the selected vetices, or better, to the active probEdit vertex
		//	and employ some functionality for changing the sphere size and also the prop edit function exponent
		//static_cast<SphereControllerTool*>(registeredControllerIn->getControllerTool(ControllerTool::SPHERE))->setWorldPosition(m_geometryManager->get));
		//static_cast<SphereControllerTool*>(registeredControllerIn->getControllerTool(ControllerTool::SPHERE))->show();
	}
	if (m_geometryManager->getActiveGeometryData() != NULL &&	registeredButtonState  == HOLD)
	{
		
		osg::Matrix matrixWorldToLocal = osg::computeWorldToLocal(m_geometryManager->getActiveGeometryData()->getParentalNodePaths()[0]);
		m_geometryManager->grabProbEditVertices(registeredControllerIn->getMatrixTransform()->getMatrix().getTrans(), true, matrixWorldToLocal);
		//static_cast<SphereControllerTool*>(registeredControllerIn->getControllerTool(ControllerTool::SPHERE))->setPosition(registeredControllerIn->getPosition());
	}
	if (m_geometryManager->getActiveGeometryData() != NULL &&	registeredButtonState == RELEASE)
	{
		//static_cast<SphereControllerTool*>(registeredControllerIn->getControllerTool(ControllerTool::SPHERE))->hide();
	}

	//if (registeredControllerIn->getButtonState(BUTTON_TYPE::JOYSTICK) == PUSH)
	//{
	//	m_geometryManager->setProbEditRadius(m_geometryManager->getProbEditRadius() * 1.2);
	//}
	//if (registeredControllerIn->getButtonState(BUTTON_TYPE::MENU) == PUSH)
	//{
	//	m_geometryManager->setProbEditRadius(m_geometryManager->getProbEditRadius() * 0.8);
	//}

	return true;
}