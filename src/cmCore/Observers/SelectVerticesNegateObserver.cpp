// ClayMore - Immersive Mesh Modelling --- Copyright (c) 2014-2017 Philipp Ladwig, Jannik Fiedler, Jan Beutgen
#include "SelectVerticesNegateObserver.h"
#include "cmGeometryManagement\GeometryManager.h"
#include "../../cmUtil/MiscDataStorage.h"

SelectVerticesNegateObserver::SelectVerticesNegateObserver()
{
	m_requiredControllerTools.push_back(ControllerTool::SPHERE);
	std::cout << "CTOR SVNO" << std::endl;
}

SelectVerticesNegateObserver::~SelectVerticesNegateObserver()
{
	std::cout << "DTOR SVNO" << std::endl;
}

bool SelectVerticesNegateObserver::notify(Controller* registeredControllerIn, Controller* otherControllerIn, BUTTON_STATE registeredButtonState)
{
	//std::cout << "NOTIFY SVNO" << std::endl;
	if (m_geometryManager->getActiveGeometryData() != NULL && registeredControllerIn->getButtonState(BUTTON_TYPE::REAR_TRIGGER) == HOLD)
	{
		SphereControllerTool* sphereControllerTool = static_cast<SphereControllerTool*>(registeredControllerIn->getControllerTool(ControllerTool::SPHERE));
		m_geometryManager->sphereSelectVertexNegate(registeredControllerIn->getMatrixTransform()->getMatrix(), 0.025f * sphereControllerTool->getSphereScaleValue());

		MiscDataStorage* miscDataStorage = m_geometryManager->getMiscDataStorage();
		if (miscDataStorage == nullptr)
		{
			std::cout << "\n############################################################ \nSelectVerticesObserver NULLPTR in SelectVerticesObserver \n############################################################" << std::endl;
		}
		else
		{
			if (*miscDataStorage->getRumbleShortState())
			{
				registeredControllerIn->triggerHapticEvent(HAPTIC_EVENT::JOYSTICK_TINY);
				miscDataStorage->setRumbleShortState(false);
			}
		}
	}

	return true;
}