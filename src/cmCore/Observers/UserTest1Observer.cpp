// ClayMore - Immersive Mesh Modelling --- Copyright (c) 2014-2017 Philipp Ladwig, Jannik Fiedler, Jan Beutgen
#include "UserTest1Observer.h"
#include "cmGeometryManagement\GeometryManager.h"
#include "cmUtil/UserTestLogger.h"

UserTest1Observer::UserTest1Observer()
{
	//m_requiredControllerTools.push_back(ControllerTool::SPHERE);
}

UserTest1Observer::~UserTest1Observer()
{

}

bool UserTest1Observer::notify(Controller* registeredControllerIn, Controller* otherControllerIn, BUTTON_STATE registeredButtonState)
{
	//std::cout << "UserTest1Observer::notify" << std::endl;
	if (registeredControllerIn->getButtonState(BUTTON_TYPE::MENU) == PUSH)
	{
		m_userTestLogger->userTrigger();
	}


	//if (m_geometryManager->getActiveGeometryData() != NULL &&	registeredButtonState == HOLD)
	//{
	//	SphereControllerTool* sphereControllerTool = static_cast<SphereControllerTool*>(registeredControllerIn->getControllerTool(ControllerTool::SPHERE));
	//	m_geometryManager->sphereSelectVertexConfirm(registeredControllerIn->getMatrixTransform()->getMatrix(), 0.025f * sphereControllerTool->getSpherScaleValue());
	//}

	return true;
}