// ClayMore - Immersive Mesh Modelling --- Copyright (c) 2014-2017 Philipp Ladwig, Jannik Fiedler, Jan Beutgen
#include "ConnectMeshElementsObserver.h"

#include "cmGeometryManagement\GeometryManager.h"
#include "../HistoryManager.h"

#include "../ControllerTools/SphereControllerTool.h"

#include "../../cmOpenMeshBinding/OpenMeshGeometry.h"

ConnectMeshElementsObserver::ConnectMeshElementsObserver()
{
	m_requiredControllerTools.push_back(ControllerTool::SPHERE);
}

ConnectMeshElementsObserver::~ConnectMeshElementsObserver()
{

}

bool ConnectMeshElementsObserver::notify(Controller* registeredControllerIn, Controller* otherControllerIn, BUTTON_STATE registeredButtonState)
{
	//Get controller tool an set grey color
	SphereControllerTool* sphereControllerTool = static_cast<SphereControllerTool*>(registeredControllerIn->getControllerTool(ControllerTool::SPHERE));
	sphereControllerTool->setSphereColor(osg::Vec4f(0.9f, 0.9f, 0.9f, 0.15f));

	//Only confirm selection in one frame while pushing the trigger (One time calling confirm avoids reselecting the last vertex, instead of doing it in hold)
	if (m_geometryManager->getActiveGeometryData() != NULL &&	registeredControllerIn->getButtonState(BUTTON_TYPE::REAR_TRIGGER) == PUSH)
	{
		sphereControllerTool->setSphereColor(osg::Vec4f(1.0f, 1.0f, 0.0f, 1.0f));
		m_geometryManager->sphereSelectVertexConfirm(registeredControllerIn->getMatrixTransform()->getMatrix(), 0.025f * sphereControllerTool->getSphereScaleValue());
	}
	if (m_geometryManager->getActiveGeometryData() != NULL &&	registeredControllerIn->getButtonState(BUTTON_TYPE::REAR_TRIGGER) == HOLD)
	{
		//If 2 verts are selected, add a line
		if (m_geometryManager->getNumberOfSelectedVerts() == 2)
		{
			//std::cout << "Number of selected verts are 2" << std::endl;
			//Insert the selected verts
			m_geometryManager->addEdgeBasedOnSelectedVerts();
			m_geometryManager->getActiveGeometryData()->getOpenMeshGeo()->getVertexArray()->dirty();
			m_geometryManager->updateFilthyGeometries();

			m_historyManager->addAction(HistoryAction(ActionType::EDI_ADD_EDGE));
			//m_geometryManager->getActiveGeometryData()->addFilthyEdge(vh.idx(), m_lastVhIdx);
		}
		//If 3 verts are selected, add a face and deselect all verts again
		else if (m_geometryManager->getNumberOfSelectedVerts() == 3)
		{
			//std::cout << "Number of selected verts are 3" << std::endl;
			m_geometryManager->addFaceBasedOnSelectedVerts();
			m_geometryManager->toggleSelectOrDeselectAll();
			m_geometryManager->getActiveGeometryData()->getOpenMeshGeo()->getVertexArray()->dirty();
			m_geometryManager->getActiveGeometryData()->getOpenMeshGeo()->getColorArray()->dirty();
			m_geometryManager->updateFilthyGeometries();
			m_geometryManager->getActiveGeometryData()->updateBoundingBox();

			m_historyManager->addAction(HistoryAction(ActionType::EDI_ADD_FACE));
		}
	}
	return true;
}