// ClayMore - Immersive Mesh Modelling --- Copyright (c) 2014-2017 Philipp Ladwig, Jannik Fiedler, Jan Beutgen

//#pragma once
//#include <iostream>
//#include <osg/PositionAttitudeTransform>
//#include "../../imInteractionManagement/Observer.h"
//
////#include "imGeometryManagement\Constraints.h"
//
//class GeometryManager;
////class MenuManager;
////class InteractionManager;
//
//class AddMeshElementObserver : public Observer
//{
//public:
//	AddMeshElementObserver();
//	~AddMeshElementObserver();
//
//	inline void setGeometryManager(osg::ref_ptr<GeometryManager> geometryManager) { m_geometryManager = geometryManager; };
//	//inline void setMenuManager(MenuManager* menuManager) { m_menuManager = menuManager; };
//	//inline void setInteractionManager(InteractionManager* interactionManager) { m_interactionManager = interactionManager; };
//
//protected:
//	bool notify(Controller* registeredControllerIn, Controller* otherControllerIn, BUTTON_STATE registeredButtonState);
//
//private:
//	//Just a function for code reducing
//	//void start(Controller* registeredControllerIn, Controller* otherControllerIn);
//	//MenuManager* m_menuManager;
//	osg::ref_ptr<GeometryManager> m_geometryManager;
//	//InteractionManager* m_interactionManager;
//	//bool m_waitTriggerStateHasChanged;
//};