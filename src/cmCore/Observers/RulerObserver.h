// ClayMore - Immersive Mesh Modelling --- Copyright (c) 2014-2017 Philipp Ladwig, Jannik Fiedler, Jan Beutgen
#pragma once
#include <iostream>
#include <osg/PositionAttitudeTransform>
#include "../../cmInteractionManagement/Observer.h"


class RulerObserver : public Observer
{
public:
	RulerObserver();
	~RulerObserver();

protected:
	//Must be called constantly, cause update(bool) must be called constantly, otherwise the line is attached to the controller movements
	bool notify(Controller* registeredControllerIn, Controller* otherControllerIn, BUTTON_STATE registeredButtonState);

private:
	osg::Vec3 m_startPoint;
	osg::Vec3 m_endPoint;
	bool m_isPathMeasurementEnabled;

};