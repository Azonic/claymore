// ClayMore - Immersive Mesh Modelling --- Copyright (c) 2014-2017 Philipp Ladwig, Jannik Fiedler, Jan Beutgen
#pragma once
#include <iostream>
#include <osg/PositionAttitudeTransform>
#include "../../cmInteractionManagement/Observer.h"

//#include "imGeometryManagement\Constraints.h"

class GeometryManager;
class MenuManager;
class InteractionManager;
class MiscDataStorage;
class HistoryManager;
class VisualizeAxisAndGridControllerTool;
class RulerControllerTool;

class GrabVerticesObserver : public Observer
{
public:
	GrabVerticesObserver();
	~GrabVerticesObserver();

	inline void setGeometryManager(osg::ref_ptr<GeometryManager> geometryManager) { m_geometryManager = geometryManager; };
	inline void setMenuManager(MenuManager* menuManager) { m_menuManager = menuManager; };
	inline void setInteractionManager(InteractionManager* interactionManager) { m_interactionManager = interactionManager; };
	inline void setMiscDataStorage(MiscDataStorage* miscDataStorageIn) { m_miscDataStorage = miscDataStorageIn; };
	inline void setHistoryManager(HistoryManager* historyManagerIn) { m_historyManager = historyManagerIn; };

	inline void waitAndDoNothingUntilTriggerPressedAgain(bool valueIn) { m_waitTriggerStateHasChanged = valueIn; }

protected:
	bool notify(Controller* registeredControllerIn, Controller* otherControllerIn, BUTTON_STATE registeredButtonState);

private:
	//Just a function for code reducing
	void start(Controller* registeredControllerIn, Controller* otherControllerIn);
	MenuManager* m_menuManager;
	osg::ref_ptr<GeometryManager> m_geometryManager;
	MiscDataStorage* m_miscDataStorage;
	InteractionManager* m_interactionManager;
	HistoryManager* m_historyManager;
	bool m_waitTriggerStateHasChanged;
	osg::Matrix m_local2WorldMatrixControllerAtStartGrab;
	osg::Vec3f m_startGrabPosition;
	osg::Vec3f m_startBaryCentrum;
	VisualizeAxisAndGridControllerTool* m_visualizeAxisAndGridControllerTool;
	RulerControllerTool* m_rulerControllerTool;
};