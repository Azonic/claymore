// ClayMore - Immersive Mesh Modelling --- Copyright (c) 2014-2017 Philipp Ladwig, Jannik Fiedler, Jan Beutgen
#pragma once
#include <iostream>
#include <osg/PositionAttitudeTransform>
#include "../../cmInteractionManagement/Observer.h"

class SpeechCallbackHandler;

class SpeechSqueezeObserver : public Observer
{
public:
	SpeechSqueezeObserver();
	~SpeechSqueezeObserver();
	inline void setSpeechCallbackHandler(SpeechCallbackHandler* g_speechCallbackHandler) { m_speechCallbackHandler = g_speechCallbackHandler; }

protected:
	bool notify(Controller* registeredControllerIn, Controller* otherControllerIn, BUTTON_STATE registeredButtonState);

private:
	SpeechCallbackHandler* m_speechCallbackHandler;

};