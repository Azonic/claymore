// ClayMore - Immersive Mesh Modelling --- Copyright (c) 2014-2017 Philipp Ladwig, Jannik Fiedler, Jan Beutgen
#include "SphereVertexProspectObserver.h"

SphereVertexProspectObserver::SphereVertexProspectObserver()
{
	m_requiredControllerTools.push_back(ControllerTool::SPHERE);
}

SphereVertexProspectObserver::~SphereVertexProspectObserver()
{

}

bool SphereVertexProspectObserver::notify(Controller* registeredControllerIn, Controller* otherControllerIn, BUTTON_STATE registeredButtonState)
{
	SphereControllerTool* sphereControllerTool = static_cast<SphereControllerTool*>(registeredControllerIn->getControllerTool(ControllerTool::SPHERE));

	m_geometryManager->sphereSelectVertex(registeredControllerIn->getMatrixTransform()->getMatrix(), 0.025f * sphereControllerTool->getSphereScaleValue());

	return true;
}