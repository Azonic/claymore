// ClayMore - Immersive Mesh Modelling --- Copyright (c) 2014-2017 Philipp Ladwig, Jannik Fiedler, Jan Beutgen
#pragma once
#include <iostream>

#include <osg/PositionAttitudeTransform>

#include "../../cmInteractionManagement/Observer.h"

class GeometryManager;
class MenuManager;
class InteractionManager;

class TranslateAndRotateSelectedObserver : public Observer
{
public:
	TranslateAndRotateSelectedObserver();
	~TranslateAndRotateSelectedObserver();

	//inline void setTargetMatrixTransform(osg::ref_ptr<osg::PositionAttitudeTransform> targetMatrixTransform) { m_targetMatrixTransform = targetMatrixTransform; }
	inline void setGeometryManager(osg::ref_ptr<GeometryManager> geometryManager) { m_geometryManager = geometryManager; };
	inline void setMenuManager(MenuManager* menuManager) { m_menuManager = menuManager; };
	inline void setInteractionManager(InteractionManager* interactionManager) { m_interactionManager = interactionManager; };

	inline void waitAndDoNothingUntilTriggerPressedAgain(bool valueIn) { m_waitTriggerStateHasChanged = valueIn; }

protected:
	bool notify(Controller* registeredControllerIn, Controller* otherControllerIn, BUTTON_STATE registeredButtonState);

private:
	osg::ref_ptr<GeometryManager> m_geometryManager;
	MenuManager* m_menuManager;
	InteractionManager* m_interactionManager;
	osg::ref_ptr<osg::PositionAttitudeTransform> m_targetMatrixTransform;
	osg::Vec3d m_lastControllerPosition;
	bool m_waitTriggerStateHasChanged;

	//Rotation Stuff
	osg::Quat m_startRotMat;
	osg::Vec3d m_barycentrumStart;
};

