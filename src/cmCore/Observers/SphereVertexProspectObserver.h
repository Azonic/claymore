// ClayMore - Immersive Mesh Modelling --- Copyright (c) 2014-2017 Philipp Ladwig, Jannik Fiedler, Jan Beutgen
#pragma once

#include "../../cmInteractionManagement/Observer.h"

#include "../ControllerTools/SphereControllerTool.h"

#include "cmGeometryManagement/GeometryManager.h"

class SphereVertexProspectObserver : public Observer
{
	public:
		SphereVertexProspectObserver();
		~SphereVertexProspectObserver();

		bool notify(Controller* registeredControllerIn, Controller* otherControllerIn, BUTTON_STATE registeredButtonState);
		inline void setGeometryManager(osg::ref_ptr<GeometryManager> geometryManager) { m_geometryManager = geometryManager; };

	private:
		osg::ref_ptr<GeometryManager> m_geometryManager;

};