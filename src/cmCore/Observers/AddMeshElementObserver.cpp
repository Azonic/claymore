// ClayMore - Immersive Mesh Modelling --- Copyright (c) 2014-2017 Philipp Ladwig, Jannik Fiedler, Jan Beutgen
#include "AddMeshElementObserver.h"

#include "cmGeometryManagement\GeometryManager.h"

AddMeshElementObserver::AddMeshElementObserver()
{
	m_iter = 0;
}

AddMeshElementObserver::~AddMeshElementObserver()
{

}

//TODO: Better to do all the OpenMesh stuff in geometryManagement
bool AddMeshElementObserver::notify(Controller* registeredControllerIn, Controller* otherControllerIn, BUTTON_STATE registeredButtonState)
{

	if (m_geometryManager->getActiveGeometryData() != NULL &&	registeredControllerIn->getButtonState(BUTTON_TYPE::REAR_TRIGGER) == PUSH)
	{

		OpenMesh::VertexHandle vh = m_geometryManager->getActiveGeometryData()->getOpenMeshGeo()->getMesh()->add_vertex(registeredControllerIn->getPosition());

		m_partsOfTriangle.push_back(vh);
		m_geometryManager->getActiveGeometryData()->getOpenMeshGeo()->getVertexArray()->dirty();
		m_iter++;

		if (m_iter == 2)
		{
			
			m_geometryManager->getActiveGeometryData()->addFilthyEdge(vh.idx(), m_lastVhIdx);
		
		}

		m_lastVhIdx = vh.idx();

	}



	if (m_geometryManager->getActiveGeometryData() != NULL &&	registeredControllerIn->getButtonState(BUTTON_TYPE::REAR_TRIGGER) == HOLD)
	{



	}



	if (m_geometryManager->getActiveGeometryData() != NULL &&	registeredControllerIn->getButtonState(BUTTON_TYPE::REAR_TRIGGER) == RELEASE)
	{

		if (m_partsOfTriangle.size() == 3)
		{
			
			m_geometryManager->getActiveGeometryData()->getOpenMeshGeo()->getMesh()->add_face(m_partsOfTriangle.at(0), m_partsOfTriangle.at(1), m_partsOfTriangle.at(2));
			m_geometryManager->getActiveGeometryData()->getOpenMeshGeo()->getVertexArray()->dirty();
			m_partsOfTriangle.clear();
			m_iter = 0;
			m_geometryManager->getActiveGeometryData()->removeFilthyEdges();

		}

	}

	return true;

}
