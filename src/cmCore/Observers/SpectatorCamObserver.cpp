// ClayMore - Immersive Mesh Modelling --- Copyright (c) 2014-2017 Philipp Ladwig, Jannik Fiedler, Jan Beutgen
#include "SpectatorCamObserver.h"
#include "cmGeometryManagement\GeometryManager.h"
#include "../ControllerTools/SphereControllerTool.h"

SpectatorCamObserver::SpectatorCamObserver()
{

}

SpectatorCamObserver::~SpectatorCamObserver()
{

}

bool SpectatorCamObserver::notify(Controller* registeredControllerIn, Controller* otherControllerIn, BUTTON_STATE registeredButtonState)
{
	if (registeredControllerIn->getButtonState(BUTTON_TYPE::REAR_TRIGGER) == HOLD)
	{
		m_spectatorViewer->getCamera()->setViewMatrixAsLookAt(registeredControllerIn->getMatrixTransform()->getMatrix().getTrans() + registeredControllerIn->getMatrixTransform()->getMatrix().getRotate()
			* osg::Vec3f(0.0, 0.0, 0.1),
			registeredControllerIn->getMatrixTransform()->getMatrix().getTrans() + registeredControllerIn->getMatrixTransform()->getMatrix().getRotate()
			* osg::Vec3f(0.0, 0.0, 1.0),
			osg::Y_AXIS);
	}

	return true;
}