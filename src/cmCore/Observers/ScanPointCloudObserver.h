// ClayMore - Immersive Mesh Modelling --- Copyright (c) 2014-2017 Philipp Ladwig, Jannik Fiedler, Jan Beutgen
#pragma once
#include "../../cmInteractionManagement/Observer.h"
#include "../../cmPointCloudManagement/PointCloudManager.h"

class ScanPointCloudObserver : public Observer
{
public:
	ScanPointCloudObserver();
	~ScanPointCloudObserver();

	void setPointCloudManager(PointCloudManager* pointCloudManager);
	inline void waitAndDoNothingUntilTriggerPressedAgain(bool valueIn) { m_waitTriggerStateHasChanged = valueIn; }
protected:
	bool notify(Controller* registeredControllerIn, Controller* otherControllerIn, BUTTON_STATE registeredButtonState);
private:
	PointCloudManager* m_pointCloudManager;
	bool m_waitTriggerStateHasChanged;
};