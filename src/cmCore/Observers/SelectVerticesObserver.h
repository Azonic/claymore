// ClayMore - Immersive Mesh Modelling --- Copyright (c) 2014-2017 Philipp Ladwig, Jannik Fiedler, Jan Beutgen
#pragma once
#include <iostream>
#include <osg/PositionAttitudeTransform>
#include "../../cmInteractionManagement/Observer.h"

class GeometryManager;
class MiscDataStorage;

class SelectVerticesObserver : public Observer
{
public:
	SelectVerticesObserver();
	~SelectVerticesObserver();

	inline void setGeometryManager(osg::ref_ptr<GeometryManager> geometryManager) { m_geometryManager = geometryManager; };

	inline void waitAndDoNothingUntilTriggerPressedAgain(bool valueIn) { m_waitTriggerStateHasChanged = valueIn; }

protected:
	bool notify(Controller* registeredControllerIn, Controller* otherControllerIn, BUTTON_STATE registeredButtonState);

private:
	osg::ref_ptr<GeometryManager> m_geometryManager;
	bool m_waitTriggerStateHasChanged;
};