// ClayMore - Immersive Mesh Modelling --- Copyright (c) 2014-2017 Philipp Ladwig, Jannik Fiedler, Jan Beutgen
#include "ScanPointCloudObserver.h"

ScanPointCloudObserver::ScanPointCloudObserver() : m_pointCloudManager{ nullptr } { }

ScanPointCloudObserver::~ScanPointCloudObserver() { }

void ScanPointCloudObserver::setPointCloudManager(PointCloudManager* pointCloudManager) {
	m_pointCloudManager = pointCloudManager;
}

bool ScanPointCloudObserver::notify(Controller* registeredControllerIn, Controller* otherControllerIn, BUTTON_STATE registeredButtonState) {
	if (!m_waitTriggerStateHasChanged) {
		if (m_pointCloudManager == nullptr) return false;
		if (registeredButtonState == PUSH || registeredButtonState == HOLD) {
			m_pointCloudManager->setPointCloudDisplay(true);
			//m_pointCloudManager->updateAndRenderPointCloud();
			m_pointCloudManager->attemptToScan();
		}
	} else if (registeredButtonState == PUSH) m_waitTriggerStateHasChanged = false;
	return true;
}