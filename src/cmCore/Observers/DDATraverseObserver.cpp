// ClayMore - Immersive Mesh Modelling --- Copyright (c) 2014-2017 Philipp Ladwig, Jannik Fiedler, Jan Beutgen
#include "DDATraverseObserver.h"

DDATraverseObserver::DDATraverseObserver() : m_pointCloudManager{ nullptr }, m_controllerTarget{ nullptr } { }

DDATraverseObserver::~DDATraverseObserver() { }

bool DDATraverseObserver::notify(Controller* registeredControllerIn, Controller* otherControllerIn, BUTTON_STATE registeredButtonState) {
	if (!m_waitTriggerStateHasChanged) {
		if (m_pointCloudManager == nullptr || m_controllerTarget == nullptr) return false;
		if (registeredButtonState == PUSH || registeredButtonState == HOLD) {
			m_pointCloudManager->setDDADisplay(true);
			m_pointCloudManager->drawDDA(registeredControllerIn->getPosition(), m_controllerTarget->getPosition());
		}
	} else if (registeredButtonState == PUSH) m_waitTriggerStateHasChanged = false;
	return true;
}