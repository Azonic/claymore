// ClayMore - Immersive Mesh Modelling --- Copyright (c) 2014-2017 Philipp Ladwig, Jannik Fiedler, Jan Beutgen

//#include "TranslateSelectedObjectObserver.h"
//#include "imGeometryManagement/GeometryManager.h"
//#include "../../imMenuManagement/MenuManager.h"
//#include "../../imInteractionManagement/InteractionManager.h"
//#include "imMenuManagement/MenuObservers/PieMenuObserver.h"
//
//#include "../ControllerTools/BoundingBoxControllerTool.h"
//
//TranslateSelectedObjectObserver::TranslateSelectedObjectObserver()
//{
//	m_targetMatrixTransform = nullptr;
//	m_lastControllerPosition.set(0.0, 0.0, 0.0);
//	m_waitTriggerStateHasChanged = true;
//
//	m_requiredControllerTools.push_back(ControllerTool::BOUNDING_BOX);
//}
//
//TranslateSelectedObjectObserver::~TranslateSelectedObjectObserver()
//{
//
//}
//
//bool TranslateSelectedObjectObserver::notify(Controller* registeredControllerIn, Controller* otherControllerIn, BUTTON_STATE registeredButtonState)
//{
//	BoundingBoxControllerTool* bBoxControllerTool = static_cast<BoundingBoxControllerTool*>(registeredControllerIn->getControllerTool(ControllerTool::BOUNDING_BOX));
//	bBoxControllerTool->setGeometryManager(m_geometryManager);
//
//	if (!m_waitTriggerStateHasChanged)
//	{
//		//std::cout << "in m_waitTriggerStateHasChanged" << std::endl;
//		if (m_geometryManager->getActiveGeometryData() != nullptr && registeredControllerIn->getButtonState(BUTTON_TYPE::SQUEEZE) == PUSH)
//		{
//			//If this is enabled and the other same line at the bottom of this iff commented, it's a nice gogo interface
//			//m_lastControllerPosition.set(registeredControllerIn->getMatrixTransform()->getMatrix().getTrans());
//			bBoxControllerTool->show();
//		}
//		if (m_geometryManager->getActiveGeometryData() != nullptr && registeredControllerIn->getButtonState(BUTTON_TYPE::SQUEEZE) == HOLD)
//		{
//			//std::cout << "in HOLD m_waitTriggerStateHasChanged" << std::endl;
//			m_geometryManager->getActiveGeometryData()->setPosition(
//				m_geometryManager->getActiveGeometryData()->getPosition()
//				+ (registeredControllerIn->getMatrixTransform()->getMatrix().getTrans() - m_lastControllerPosition));
//
//			if (registeredControllerIn->getButtonState(BUTTON_TYPE::REAR_TRIGGER) == PUSH)
//			{
//				m_barycentrumStart = m_geometryManager->getBarycentrumOfALLVertsForActiveGeomData();
//				m_geometryManager->rotateSelectedVerticesStart();
//				m_startRotMat = registeredControllerIn->getRotation().inverse();
//			}
//			if (registeredControllerIn->getButtonState(BUTTON_TYPE::REAR_TRIGGER) == HOLD)
//			{
//				m_geometryManager->rotateEntireGeometry(osg::Matrixd(m_startRotMat * registeredControllerIn->getRotation()), m_barycentrumStart);
//
//			}
//			//std::cout << "m_geometryManager->getActiveGeometryData()->getBound().center" << m_geometryManager->getActiveGeometryData()->getBound().center() << std::endl;
//		}
//		if (m_geometryManager->getActiveGeometryData() != nullptr && registeredControllerIn->getButtonState(BUTTON_TYPE::SQUEEZE) == RELEASE)
//		{
//		//	//Attach the correct menu again to the sec. controller depending on the current applicationMode
//		//	if (m_geometryManager->getCurrentApplicationMode() == OBJECT_MODE)
//		//	{
//
//		//	}
//		//	else if (m_geometryManager->getCurrentApplicationMode() == EDIT_MODE)
//		//	{
//
//		//	}
//			bBoxControllerTool->hide();
//		}
//		m_lastControllerPosition.set(registeredControllerIn->getMatrixTransform()->getMatrix().getTrans());
//	}
//	else
//	{
//		//std::cout << "in ELSE m_waitTriggerStateHasChanged" << std::endl;
//		if (registeredControllerIn->getButtonState(BUTTON_TYPE::SQUEEZE) == PUSH)
//		{
//			m_waitTriggerStateHasChanged = false;
//			m_lastControllerPosition.set(registeredControllerIn->getMatrixTransform()->getMatrix().getTrans());
//			bBoxControllerTool->show();
//		}
//	}
//	return true;
//}