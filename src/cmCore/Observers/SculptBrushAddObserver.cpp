// ClayMore - Immersive Mesh Modelling --- Copyright (c) 2014-2017 Philipp Ladwig, Jannik Fiedler, Jan Beutgen
#include "SculptBrushAddObserver.h"
#include "cmGeometryManagement\GeometryManager.h"

SculptBrushAddObserver::SculptBrushAddObserver()
{
	m_requiredControllerTools.push_back(ControllerTool::SPHERE);
}

SculptBrushAddObserver::~SculptBrushAddObserver()
{

}

bool SculptBrushAddObserver::notify(Controller* registeredControllerIn, Controller* otherControllerIn, BUTTON_STATE registeredButtonState)
{
	if (m_geometryManager->getActiveGeometryData() != NULL && registeredButtonState == PUSH)
	{
		m_geometryManager->sculptStart(0.15f, 0.2f);
	}

	if (m_geometryManager->getActiveGeometryData() != NULL && registeredButtonState == HOLD)
	{
		osg::Matrix geomMatrixLtW = osg::computeLocalToWorld(m_geometryManager->getActiveGeometryData()->getParentalNodePaths()[0]);
		m_geometryManager->sculptAdd(0.15f, 0.1f, registeredControllerIn->getMatrixTransform()->getMatrix().getTrans(), geomMatrixLtW);
	}

	//m_geometryManager->getActiveGeometryData();
	//osg::Matrix geomMatrixLtW = osg::computeLocalToWorld(m_geometryManager->getActiveGeometryData()->getParentalNodePaths()[0]);

	//osg::Matrix cursorMatrixLeftLtW = osg::computeLocalToWorld(test);
	//m_geometryManager->sculptStart(interactionDataPaket.controllerPos, 1.0, 0.02, interactionDataPaket.controllerPos, geomMatrixLtW);

	return true;
}