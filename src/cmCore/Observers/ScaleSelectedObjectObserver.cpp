// ClayMore - Immersive Mesh Modelling --- Copyright (c) 2014-2017 Philipp Ladwig, Jannik Fiedler, Jan Beutgen
#include "ScaleSelectedObjectObserver.h"
#include "cmGeometryManagement\GeometryManager.h"

ScaleSelectedObjectObserver::ScaleSelectedObjectObserver()
{
	m_lastControllerPosition.set(0.0, 0.0, 0.0);
	m_controllerRotationAtPush.set(0.0, 0.0, 0.0, 0.0);
}

ScaleSelectedObjectObserver::~ScaleSelectedObjectObserver()
{

}

bool ScaleSelectedObjectObserver::notify(Controller* registeredControllerIn, Controller* otherControllerIn, BUTTON_STATE registeredButtonState)
{
	if (m_geometryManager->getActiveGeometryData() != NULL && registeredButtonState == PUSH)
	{
		//osg::Quat controllerRotationAsQuat = registeredControllerIn->getMatrixTransform()->getMatrix().getRotate();

		//m_controllerRotationAtPush = m_geometryManager->getGeometryDataByName(
		//	m_geometryManager->getNameOfActiveGeometry())->getAttitude() * controllerRotationAsQuat.inverse();
	}
	if (m_geometryManager->getActiveGeometryData() != NULL && registeredButtonState == HOLD)
	{
		m_geometryManager->getActiveGeometryData()->setScale(
			m_geometryManager->getActiveGeometryData()->getScale()
			+ (registeredControllerIn->getMatrixTransform()->getMatrix().getTrans() - m_lastControllerPosition));


		//m_geometryManager->getActiveGeometryData()->setPosition(
		//	m_geometryManager->getActiveGeometryData()->getPosition() 
		//	+ (registeredControllerIn->getMatrixTransform()->getMatrix().getTrans() - m_lastControllerPosition));
		//

		//osg::Matrixd rotMat;
		//m_geometryManager->getActiveGeometryData()->setAttitude(
		//	m_controllerRotationAtPush * registeredControllerIn->getMatrixTransform()->getMatrix().getRotate());
	}
	if (m_geometryManager->getActiveGeometryData() != NULL && registeredButtonState == RELEASE)
	{
		m_geometryManager->getActiveGeometryData()->updateBoundingBox();
	}

	m_lastControllerPosition.set(registeredControllerIn->getMatrixTransform()->getMatrix().getTrans());

	return true;
}