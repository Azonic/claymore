// ClayMore - Immersive Mesh Modelling --- Copyright (c) 2014-2017 Philipp Ladwig, Jannik Fiedler, Jan Beutgen
#include "AddEdgeLineObserver.h"

#include "cmGeometryManagement\GeometryManager.h"

AddEdgeLineObserver::AddEdgeLineObserver()
{
	m_iter = 0;
}

AddEdgeLineObserver::~AddEdgeLineObserver()
{

}

bool AddEdgeLineObserver::notify(Controller* registeredControllerIn, Controller* otherControllerIn, BUTTON_STATE registeredButtonState)
{

	if (m_geometryManager->getActiveGeometryData() != NULL &&	registeredControllerIn->getButtonState(BUTTON_TYPE::REAR_TRIGGER) == PUSH)
	{
		//std::cout << std::endl << "Add something" << std::endl;
		osg::Matrix geomMatrix = osg::computeWorldToLocal(m_geometryManager->getActiveGeometryData()->getParentalNodePaths()[0]);
		OpenMesh::VertexHandle vh = m_geometryManager->getActiveGeometryData()->getOpenMeshGeo()->getMesh()->add_vertex(registeredControllerIn->getPosition() * geomMatrix);

		m_geometryManager->getActiveGeometryData()->getOpenMeshGeo()->getVertexArray()->dirty();
		m_iter++;

		if (m_iter >= 2)
		{
			m_geometryManager->getActiveGeometryData()->addFilthyEdge(vh.idx(), m_lastVhIdx);
		}
		m_lastVhIdx = vh.idx();
		//std::cout << std::endl << "m_iter % 2: " << m_iter % 2 << std::endl;
		//std::cout << std::endl << "m_iter: " << m_iter << std::endl;
		m_geometryManager->updateFilthyGeometries();
	}
	if (m_geometryManager->getActiveGeometryData() != NULL &&	registeredControllerIn->getButtonState(BUTTON_TYPE::REAR_TRIGGER) == HOLD)
	{

	}
	if (m_geometryManager->getActiveGeometryData() != NULL &&	registeredControllerIn->getButtonState(BUTTON_TYPE::REAR_TRIGGER) == RELEASE)
	{

	}
	return true;
}

void AddEdgeLineObserver::resetCounter()
{
	m_iter = 0;
}
