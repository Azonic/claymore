// ClayMore - Immersive Mesh Modelling --- Copyright (c) 2014-2017 Philipp Ladwig, Jannik Fiedler, Jan Beutgen
#include "RulerObserver.h"
#include "../ControllerTools/RulerControllerTool.h"

RulerObserver::RulerObserver()
{
	m_requiredControllerTools.push_back(ControllerTool::RULER);
	m_isPathMeasurementEnabled = false;
}

RulerObserver::~RulerObserver()
{

}

bool RulerObserver::notify(Controller* registeredControllerIn, Controller* otherControllerIn, BUTTON_STATE registeredButtonState)
{
	RulerControllerTool* rulerControllerTool = static_cast<RulerControllerTool*>(registeredControllerIn->getControllerTool(ControllerTool::RULER));
	rulerControllerTool->update(true);
	rulerControllerTool->setShowRulerLine(true);

	if (registeredControllerIn->getButtonState(BUTTON_TYPE::REAR_TRIGGER) == PUSH)
	{
		m_startPoint = registeredControllerIn->getMatrixTransform()->getMatrix().getTrans();
	}

	if (registeredControllerIn->getButtonState(BUTTON_TYPE::REAR_TRIGGER) == HOLD)
	{
		m_endPoint = registeredControllerIn->getMatrixTransform()->getMatrix().getTrans();
		rulerControllerTool->setStartAndEndPoint(m_startPoint, m_endPoint);
	}
	if (registeredControllerIn->getButtonState(BUTTON_TYPE::JOYSTICK) == PUSH)
	{

		if (m_isPathMeasurementEnabled)
		{
			rulerControllerTool->setPathMeasurement(false);
			m_isPathMeasurementEnabled = false;
		}
		else
		{
			rulerControllerTool->setPathMeasurement(true);
			m_isPathMeasurementEnabled = true;
		}
	}

	return true;
}
