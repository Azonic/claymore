// ClayMore - Immersive Mesh Modelling --- Copyright (c) 2014-2017 Philipp Ladwig, Jannik Fiedler, Jan Beutgen
#include "AddVertsObserver.h"

#include "cmGeometryManagement\GeometryManager.h"
#include "../HistoryManager.h"

AddVertsObserver::AddVertsObserver()
{

}

AddVertsObserver::~AddVertsObserver()
{

}

bool AddVertsObserver::notify(Controller* registeredControllerIn, Controller* otherControllerIn, BUTTON_STATE registeredButtonState)
{
	if (m_geometryManager->getActiveGeometryData() != NULL &&	registeredControllerIn->getButtonState(BUTTON_TYPE::REAR_TRIGGER) == PUSH)
	{
		std::cout << std::endl << "Add a vertex" << std::endl;
		osg::Matrix geomMatrix = osg::computeWorldToLocal(m_geometryManager->getActiveGeometryData()->getParentalNodePaths()[0]);
		OpenMesh::VertexHandle vh = m_geometryManager->getActiveGeometryData()->getOpenMeshGeo()->getMesh()->add_vertex(registeredControllerIn->getPosition() * geomMatrix);
		m_geometryManager->getActiveGeometryData()->getOpenMeshGeo()->getVertexArray()->dirty();
		m_geometryManager->updateFilthyGeometries();
		m_historyManager->addAction(HistoryAction(ActionType::EDI_ADD_VERTEX));
	}
	if (m_geometryManager->getActiveGeometryData() != NULL &&	registeredControllerIn->getButtonState(BUTTON_TYPE::REAR_TRIGGER) == HOLD)
	{

	}
	if (m_geometryManager->getActiveGeometryData() != NULL &&	registeredControllerIn->getButtonState(BUTTON_TYPE::REAR_TRIGGER) == RELEASE)
	{

	}
	return true;
}