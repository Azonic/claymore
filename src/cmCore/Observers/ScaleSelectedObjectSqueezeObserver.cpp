// ClayMore - Immersive Mesh Modelling --- Copyright (c) 2014-2017 Philipp Ladwig, Jannik Fiedler, Jan Beutgen
#include "ScaleSelectedObjectSqueezeObserver.h"

#include "cmGeometryManagement\GeometryManager.h"
#include "cmMenuManagement\MenuManager.h"
#include "cmInteractionManagement\InteractionManager.h"
#include "../HistoryManager.h"

#include "../ControllerTools/visualizeAxisAndGridControllerTool.h"
#include "../ControllerTools/VisualizeBarycentrumControllerTool.h"
#include "../ControllerTools/DashedLineControllerTool.h"
#include "../ControllerTools/BoundingBoxControllerTool.h"
//#include "../ControllerTools/ThreeDIconControllerTool.h"

#include "cmMenuManagement\MenuObservers\TouchMenuObserver.h"

ScaleSelectedObjectSqueezeObserver::ScaleSelectedObjectSqueezeObserver(bool isObservingToPrimaryControllerIn) : m_isObservingToPrimaryController(isObservingToPrimaryControllerIn)
{
	m_requiredControllerTools.push_back(ControllerTool::VISUALIZE_BARYCENTRUM);
	m_requiredControllerTools.push_back(ControllerTool::DASHED_LINE);
	m_requiredControllerTools.push_back(ControllerTool::BOUNDING_BOX);
	//m_requiredControllerTools.push_back(ControllerTool::THREE_D_ICON);

	m_initialiseStartIsAlreadyTriggered = false;

	// ### Rotation Stuff (doesn't work very nice) (Search for Rotation Stuff in this cpp for uncommenting) ### 
	//m_isRotatedOnce = false;
	//m_lastSnappedAngY = M_PI;
}

ScaleSelectedObjectSqueezeObserver::~ScaleSelectedObjectSqueezeObserver()
{

}

bool ScaleSelectedObjectSqueezeObserver::notify(Controller* registeredControllerIn, Controller* otherControllerIn, BUTTON_STATE registeredButtonState)
{
	//std::cout << " ScaleSelectedObjectSqueezeObserver::notify()" << std::endl;

	DashedLineControllerTool* dashedLineControllerTool = static_cast<DashedLineControllerTool*>(registeredControllerIn->getControllerTool(ControllerTool::DASHED_LINE));

	VisualizeBarycentrumControllerTool* visualizeBarycentrumControllerTool = static_cast<VisualizeBarycentrumControllerTool*>(registeredControllerIn->getControllerTool(ControllerTool::VISUALIZE_BARYCENTRUM));
	visualizeBarycentrumControllerTool->setGeometryManager(m_geometryManager);

	BoundingBoxControllerTool* bBoxControllerTool = static_cast<BoundingBoxControllerTool*>(registeredControllerIn->getControllerTool(ControllerTool::BOUNDING_BOX));
	bBoxControllerTool->setGeometryManager(m_geometryManager);

	//ThreeDIconControllerTool* threeDIconControllerTool = static_cast<ThreeDIconControllerTool*>(registeredControllerIn->getControllerTool(ControllerTool::THREE_D_ICON));

	if (m_isObservingToPrimaryController)
	{
		if (otherControllerIn->getButtonState(BUTTON_TYPE::SQUEEZE) == PUSH || otherControllerIn->getButtonState(BUTTON_TYPE::SQUEEZE) == HOLD)
		{
			if (m_geometryManager->getActiveGeometryData() != NULL &&	registeredButtonState == PUSH)
			{
				initialiseStart(registeredControllerIn, otherControllerIn, registeredButtonState);
				m_lastSnappedAngY = M_PI;
			}

			if (m_geometryManager->getActiveGeometryData() != NULL &&	registeredButtonState == HOLD)
			{
				if (m_initialiseStartIsAlreadyTriggered)
				{
					osg::Vec3f barycentrum;
					if (m_geometryManager->isAnyVertexSelected())
					{
						barycentrum = m_geometryManager->getBarycentrumOfSELECTEDVertsForActiveGeomData();
					}
					else
					{
						barycentrum = m_geometryManager->getBarycentrumOfALLVertsForActiveGeomData();
					}
					visualizeBarycentrumControllerTool->updateCross(barycentrum);

					dashedLineControllerTool->setStartPoint(registeredControllerIn->getPosition());


					osg::Matrix local2WorldMatrixGeom = osg::computeLocalToWorld(m_geometryManager->getActiveGeometryData()->getParentalNodePaths()[0]);
					//dashedLineControllerTool->setEndPoint(barycentrum *  local2WorldMatrixGeom);
					dashedLineControllerTool->setEndPoint(otherControllerIn->getPosition());
					dashedLineControllerTool->showLine();



					////Rotation Stuff
					//if (otherControllerIn->getButtonState(BUTTON_TYPE::REAR_TRIGGER) == PUSH)
					//{
					//	m_geometryManager->rotateSelectedVerticesStart();
					//}
					//if (otherControllerIn->getButtonState(BUTTON_TYPE::REAR_TRIGGER) == HOLD)
					//{
					//	osg::Vec3f eye = otherControllerIn->getPosition();
					//	osg::Vec3f center = registeredControllerIn->getPosition();
					//	osg::Quat currentRotationQuat;
					//	currentRotationQuat = osg::Matrix::lookAt(eye, center, -osg::Y_AXIS).getRotate();

					//	osg::Matrixd rotMat = osg::Matrixd(m_startQuatForRotation * currentRotationQuat);

					//	m_geometryManager->rotateEntireGeometry(rotMat, m_barycentrumStart);
					//}
					//else
					//{
					float currentDistance = (registeredControllerIn->getMatrixTransform()->getMatrix().getTrans() - otherControllerIn->getMatrixTransform()->getMatrix().getTrans()).length();

					//EXP: Example for scaling bigger: 2 * (1 - (3.0/2.9)) = -0,069
					//		ScalingVector in OpenMeshGeometry (the "sliding vector")  looks to barycentrum - this is why we need a negative value to scale bigger (away from barycentrum)

					m_geometryManager->scaleEntireGeometry((1 - (currentDistance / m_distanceForScale)));
					m_distanceForScale = currentDistance;


					// ### Rotation Stuff (doesn't work very nice) (Search for Rotation Stuff in this cpp for uncommenting) ### 
					//osg::Vec3f eye = otherControllerIn->getPosition();

					//osg::Vec3f center = registeredControllerIn->getPosition();
					//osg::Quat currentRotationQuat;
					//currentRotationQuat = osg::Matrix::lookAt(eye, center, -osg::Y_AXIS).getRotate();
					//osg::Quat justAQuat = m_startQuatForRotation * currentRotationQuat;

					//justAQuat.x() = 0.0;
					//justAQuat.z() = 0.0;

					//double mag = sqrt(justAQuat.w() * justAQuat.w() + justAQuat.y() * justAQuat.y());
					//justAQuat.y() /= mag;
					//justAQuat.w() /= mag;

					//double snappingValue = M_PI_4; //45 degrees
					//double angY = 2 * acos(justAQuat.y());
					//double snappedAngY = (floorf((angY / snappingValue) + 0.5) * snappingValue);
					//justAQuat.y() = cos(snappedAngY / 2.0);
					//double angW = 2 * acos(justAQuat.w());
					//double snappedAngW = (floorf((angW / snappingValue) + 0.5) * snappingValue);
					//justAQuat.w() = cos(snappedAngW / 2.0);
					//std::cout << "snappedAngY: " << snappedAngY << std::endl;
					//std::cout << "m_snappedAngY: " << m_lastSnappedAngY << std::endl;
					//if (snappedAngY != m_lastSnappedAngY)
					//{
					//	osg::Matrixd rotMat;
					//	m_geometryManager->rotateSelectedVerticesStart();
					//	if (snappedAngY < m_lastSnappedAngY)
					//	{
					//		std::cout << "OBEN: " << m_lastSnappedAngY << std::endl;
					//		rotMat.makeRotate(M_PI_4, 0.0, 1.0, 0.0);
					//		m_geometryManager->rotateEntireGeometry(rotMat, m_barycentrumStart);
					//	}
					//	else 
					//	{
					//		std::cout << "UNTEN: " << m_lastSnappedAngY << std::endl;
					//		rotMat.makeRotate(-M_PI_4, 0.0, 1.0, 0.0);
					//		m_geometryManager->rotateEntireGeometry(rotMat, m_barycentrumStart);
					//	}
					//	//osg::Matrixd rotMat = osg::Matrixd(m_startQuatForRotation * currentRotationQuat);
					//}

					//m_lastSnappedAngY = snappedAngY;

					//
				}
				else
				{
					initialiseStart(registeredControllerIn, otherControllerIn, registeredButtonState);
				}
			}
		}
		else
		{
			hideControllerTools(registeredControllerIn, otherControllerIn, registeredButtonState);
		}
		if (m_geometryManager->getActiveGeometryData() != NULL && registeredButtonState == RELEASE)
		{
			hideControllerTools(registeredControllerIn, otherControllerIn, registeredButtonState);
		}
	}
	return true;
}

void ScaleSelectedObjectSqueezeObserver::initialiseStart(Controller * registeredControllerIn, Controller * otherControllerIn, BUTTON_STATE registeredButtonState)
{
	DashedLineControllerTool* dashedLineControllerTool = static_cast<DashedLineControllerTool*>(registeredControllerIn->getControllerTool(ControllerTool::DASHED_LINE));
	dashedLineControllerTool->setStartPoint(registeredControllerIn->getPosition());

	//VisualizeBarycentrumControllerTool* visualizeBarycentrumControllerTool = static_cast<VisualizeBarycentrumControllerTool*>(registeredControllerIn->getControllerTool(ControllerTool::VISUALIZE_BARYCENTRUM));

	BoundingBoxControllerTool* bBoxControllerTool = static_cast<BoundingBoxControllerTool*>(registeredControllerIn->getControllerTool(ControllerTool::BOUNDING_BOX));
	bBoxControllerTool->setGeometryManager(m_geometryManager);
	bBoxControllerTool->show();

	//osg::Vec3f barycentrum;
	//if (m_geometryManager->isAnyVertexSelected())
	//{
	//	barycentrum = m_geometryManager->getBarycentrumOfSELECTEDVertsForActiveGeomData();
	//}
	//else
	//{
	//	barycentrum = m_geometryManager->getBarycentrumOfALLVertsForActiveGeomData();
	//}

	//osg::Matrix local2WorldMatrixGeom = osg::computeLocalToWorld(m_geometryManager->getActiveGeometryData()->getParentalNodePaths()[0]);
	//dashedLineControllerTool->setEndPoint(barycentrum *  local2WorldMatrixGeom);
	dashedLineControllerTool->setEndPoint(otherControllerIn->getPosition());

	m_distanceForScale = (registeredControllerIn->getMatrixTransform()->getMatrix().getTrans() - otherControllerIn->getMatrixTransform()->getMatrix().getTrans()).length();

	m_geometryManager->scaleSelectedVerticesStart();

	m_initialiseStartIsAlreadyTriggered = true;

	// ### Rotation Stuff (doesn't work very nice) (Search for Rotation Stuff in this cpp for uncommenting) ### 
	//m_barycentrumStart = m_geometryManager->getBarycentrumOfALLVertsForActiveGeomData();

	//osg::Vec3f eye = otherControllerIn->getPosition();
	//osg::Vec3f center = registeredControllerIn->getPosition();
	//m_startQuatForRotation = osg::Matrix::lookAt(eye, center, -osg::Y_AXIS).getRotate().inverse();
}

void ScaleSelectedObjectSqueezeObserver::hideControllerTools(Controller* registeredControllerIn, Controller* otherControllerIn, BUTTON_STATE registeredButtonState)
{
	m_initialiseStartIsAlreadyTriggered = false;
	DashedLineControllerTool* dashedLineControllerTool = static_cast<DashedLineControllerTool*>(registeredControllerIn->getControllerTool(ControllerTool::DASHED_LINE));

	VisualizeBarycentrumControllerTool* visualizeBarycentrumControllerTool = static_cast<VisualizeBarycentrumControllerTool*>(registeredControllerIn->getControllerTool(ControllerTool::VISUALIZE_BARYCENTRUM));

	BoundingBoxControllerTool* bBoxControllerTool = static_cast<BoundingBoxControllerTool*>(registeredControllerIn->getControllerTool(ControllerTool::BOUNDING_BOX));
	bBoxControllerTool->setGeometryManager(m_geometryManager);

	//ThreeDIconControllerTool* threeDIconControllerTool = static_cast<ThreeDIconControllerTool*>(registeredControllerIn->getControllerTool(ControllerTool::THREE_D_ICON));

	bBoxControllerTool->hide();

	//threeDIconControllerTool->setIsShowScaleIconActivated(false);

	dashedLineControllerTool->hideLine();
	visualizeBarycentrumControllerTool->removeLines();

	m_geometryManager->getActiveGeometryData()->updateBoundingBox();
	m_historyManager->addAction(HistoryAction(ActionType::EDI_SCALE));
}