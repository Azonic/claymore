// ClayMore - Immersive Mesh Modelling --- Copyright (c) 2014-2017 Philipp Ladwig, Jannik Fiedler, Jan Beutgen
#include "SpeechSqueezeObserver.h"
#include "../SpeechCallbackHandler.h"

SpeechSqueezeObserver::SpeechSqueezeObserver()
{
	
}

SpeechSqueezeObserver::~SpeechSqueezeObserver()
{

}

bool SpeechSqueezeObserver::notify(Controller* registeredControllerIn, Controller* otherControllerIn, BUTTON_STATE registeredButtonState)
{
	if (registeredControllerIn->getButtonState(BUTTON_TYPE::SQUEEZE) == PUSH)
	{
		m_speechCallbackHandler->startListen();
	}
	if (registeredControllerIn->getButtonState(BUTTON_TYPE::SQUEEZE) == HOLD)
	{
		m_speechCallbackHandler->lastKeepAwakeListing();
	}

	return true;
}
