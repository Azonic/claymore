// ClayMore - Immersive Mesh Modelling --- Copyright (c) 2014-2017 Philipp Ladwig, Jannik Fiedler, Jan Beutgen
#pragma once
#include <iostream>
#include <osg/PositionAttitudeTransform>
#include "../../cmInteractionManagement/Observer.h"
#include "../ControllerTools/SphereControllerTool.h"
class GeometryManager;
class UserTestLogger;

class UserTest1Observer : public Observer
{
public:
	UserTest1Observer();
	~UserTest1Observer();

	inline void setGeometryManager(osg::ref_ptr<GeometryManager> geometryManager) { m_geometryManager = geometryManager; };
	inline void setUserTestLogger(UserTestLogger *userTestLogger) { m_userTestLogger = userTestLogger; };

protected:
	bool notify(Controller* registeredControllerIn, Controller* otherControllerIn, BUTTON_STATE registeredButtonState);

private:
	osg::ref_ptr<GeometryManager> m_geometryManager;
	UserTestLogger *m_userTestLogger;
};