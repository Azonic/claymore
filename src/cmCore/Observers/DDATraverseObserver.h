// ClayMore - Immersive Mesh Modelling --- Copyright (c) 2014-2017 Philipp Ladwig, Jannik Fiedler, Jan Beutgen
#pragma once
#include "../../cmInteractionManagement/Observer.h"
#include "../../cmPointCloudManagement/PointCloudManager.h"

class DDATraverseObserver : public Observer
{
public:
	DDATraverseObserver();
	~DDATraverseObserver();

	inline void setPointCloudManager(PointCloudManager* pointCloudManager) { m_pointCloudManager = pointCloudManager; };
	inline void setControllerTarget(Controller* controllerTarget) { m_controllerTarget = controllerTarget; };
	inline void waitAndDoNothingUntilTriggerPressedAgain(bool valueIn) { m_waitTriggerStateHasChanged = valueIn; }
protected:
	bool notify(Controller* registeredControllerIn, Controller* otherControllerIn, BUTTON_STATE registeredButtonState);
private:
	PointCloudManager* m_pointCloudManager;
	Controller* m_controllerTarget;
	bool m_waitTriggerStateHasChanged;
};