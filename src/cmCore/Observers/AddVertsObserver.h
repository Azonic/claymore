// ClayMore - Immersive Mesh Modelling --- Copyright (c) 2014-2017 Philipp Ladwig, Jannik Fiedler, Jan Beutgen
#pragma once
#include <iostream>
#include <osg/PositionAttitudeTransform>
#include "../../cmInteractionManagement/Observer.h"

#include "../../cmOpenMeshBinding/OpenMeshGeometry.h"

class GeometryManager;
class HistoryManager;

class AddVertsObserver : public Observer
{
public:
	AddVertsObserver();
	~AddVertsObserver();

	inline void setGeometryManager(osg::ref_ptr<GeometryManager> geometryManager) { m_geometryManager = geometryManager; };
	inline void setHistoryManager(HistoryManager* historyManagerIn) { m_historyManager = historyManagerIn; };

protected:
	bool notify(Controller* registeredControllerIn, Controller* otherControllerIn, BUTTON_STATE registeredButtonState);

private:
	osg::ref_ptr<GeometryManager> m_geometryManager;
	HistoryManager* m_historyManager;

};