#include "StaticPoseDriver.h"
#include <iostream>

StaticPoseDriver::StaticPoseDriver(const osg::Matrix& pose, const osg::Matrix& sensorToDriver) : PoseDriver(sensorToDriver), m_pose(pose), m_transform(sensorToDriver * pose) { }

StaticPoseDriver::~StaticPoseDriver() {
}

osg::Matrix StaticPoseDriver::getTransform(unsigned long long timestamp_usec) {
	return m_transform;
}

osg::Matrix StaticPoseDriver::getPose(unsigned long long timestamp_usec) {
	return m_pose;
}

void StaticPoseDriver::updateTransform(const osg::Matrix& sensorToDriver) {
	m_transform = sensorToDriver * m_pose;
}

void StaticPoseDriver::updatePose(const osg::Matrix& pose, const osg::Matrix& sensorToDriver) {
	m_pose = pose;
	m_transform = sensorToDriver * pose;
}

