#pragma once

#define def_xyz(name, T, offset) virtual inline const float name(const unsigned int index) override { return _depthScale * (((T*)_vertexData)[index * 3u + offset]); }
#define def_uv(name, T, offset) virtual inline const float name(const unsigned int index) override { return (float)(((T*)_texCoordData)[index * 2u + offset]); }
#define def_bytes(name, T, dimension) virtual const size_t name() override { return size() * dimension * sizeof(T); }

struct PointcloudData {
protected:
	const void* _vertexData;
	const void* _texCoordData;
	const unsigned int _width;
	const unsigned int _height;
	const float _depthScale;
public:
	PointcloudData(const void* vertexData, const void* texCoordData, const unsigned int width, const unsigned int height, const float depthScale) : _vertexData(vertexData), _texCoordData(texCoordData), _width(width), _height(height), _depthScale(depthScale) {};
	virtual ~PointcloudData() {};
	// Do only ever access the data directly, if you know exactly which data layout you are dealing with.
	// Otherwise use the x-, y-, z-functions to retrieve the specific component.
	const void* vertices() { return _vertexData; }
	// Do only ever access the data directly, if you know exactly which data layout you are dealing with.
	// Otherwise use the u-, v-functions to retrieve the specific component.
	const void* textureCoordinates() { return _texCoordData; }
	const size_t size() { return (size_t)_width * _height; }
	const unsigned int width() { return _width; }
	const unsigned int height() { return _height; }
	virtual const size_t bytesVertices() = 0;
	virtual const size_t bytesTextureCoordinates() = 0;
	virtual inline const float x(const unsigned int index) = 0;
	virtual inline const float y(const unsigned int index) = 0;
	virtual inline const float z(const unsigned int index) = 0;
	virtual inline const float u(const unsigned int index) = 0;
	virtual inline const float v(const unsigned int index) = 0;
};

struct PointcloudData_XYZ32F_UV32F : public PointcloudData {
	PointcloudData_XYZ32F_UV32F(const void* vertexData, const void* texCoordData, const unsigned int width, const unsigned int height, const float depthScale) : PointcloudData(vertexData, texCoordData, width, height, depthScale) {}
	def_xyz(x, float, 0u)
	def_xyz(y, float, 1u)
	def_xyz(z, float, 2u)
	def_uv(u, float, 0u)
	def_uv(v, float, 1u)
	def_bytes(bytesVertices, float, 3u)
	def_bytes(bytesTextureCoordinates, float, 2u)
};

struct PointcloudData_XYZ16_UV32F : public PointcloudData {
	PointcloudData_XYZ16_UV32F(const void* vertexData, const void* texCoordData, const unsigned int width, const unsigned int height, const float depthScale) : PointcloudData(vertexData, texCoordData, width, height, depthScale) {}
	def_xyz(x, short, 0u)
	def_xyz(y, short, 1u)
	def_xyz(z, short, 2u)
	def_uv(u, float, 0u)
	def_uv(v, float, 1u)
	def_bytes(bytesVertices, short, 3u)
	def_bytes(bytesTextureCoordinates, float, 2u)
};

/*struct PointcloudData {
protected:
	const Vertex* _vertexData;
	const TextureCoordinate* _texCoordData;
	const unsigned int _width;
	const unsigned int _height;
public:
	PointcloudData(const void* vertexData, const void* texCoordData, const unsigned int width, const unsigned int height) : _vertexData((Vertex*)vertexData), _texCoordData((TextureCoordinate*)texCoordData), _width(width), _height(height) {};
	virtual ~PointcloudData() {};
	const Vertex* vertices() { return _vertexData; }
	inline const Vertex& vertex(const unsigned int index) { return _vertexData[index]; }
	inline const TextureCoordinate& textureCoordinate(const unsigned int index) { return _texCoordData[index]; }
	const TextureCoordinate* textureCoordinates() { return _texCoordData; }
	const size_t bytesVertices() { return size() * sizeof(Vertex); }
	const size_t bytesTextureCoordinates() { return size() * sizeof(TextureCoordinate); }
	const size_t size() { return (size_t)_width * _height; }
	const unsigned int width() { return _width; }
	const unsigned int height() { return _height; }
};*/

#undef def_xyz
#undef def_uv
#undef def_bytes