// ClayMore - Immersive Mesh Modelling --- Copyright (c) 2014-2017 Philipp Ladwig, Jannik Fiedler, Jan Beutgen
#include "PointCloudVisualizer.h"

//Der Config Reader lie�t die config.xml in /bin/data
#include "../cmUtil/ConfigReader.h"
//ShaderUtils hat eine "convienent" function mit Error Handling um ein ShaderFile zu laden
#include "../cmUtil/ShaderUtils.h"

#include "../cmUtil/ThreadSafeStack.cpp"
#include "ViewFrustum.h"

//Bisschen Mathe
#include <osg/Math>

//For saving files on HDD 
#include <osgDB/WriteFile>

//Debug OSG for printing vec and matrices
#include <osg/io_utils>

#include <opencv2/opencv.hpp>

//Input-Output Stream -> for printing stuff on the console with for example:
//std::cout << "Bla Bla" << std::endl;
#include <iostream>

#include <queue>
#include <future>

#include <cmUtil/TimeWriter.h>

//#include <pcl/registration/icp.h>

#define OCTREE_INITIAL_VALUE 42.f

//Hier wird per "InitialsierungsListe" der Constructor (CTOR auch abgek�rzt) erstellt. Die InitialsierungsListe ist das hinter dem Doppelpunkt.
//Dies weist direkt die �bergabeparameter zu Membervariablen der Klasse (Member sind Variablen die mit m_ anfangen)
//TODO: Move m_bodyScanning and m_ARCoreTracking to config.xml
PointCloudVisualizer::PointCloudVisualizer(ConfigReader* configReaderIn,
	osg::ref_ptr<osg::Group> pointCloudGroup) :
	m_configReader(configReaderIn),
	m_pointCloudGroup(pointCloudGroup)
{
	m_pointCloudMode = configReaderIn->getBoolFromStartupConfig("enable_body_scanning") ? PointCloudMode::BODY_SCANNING : PointCloudMode::ROOM_SCANNING;
	m_ARCoreTracking = configReaderIn->getBoolFromStartupConfig("enable_osc_arcore_tracking");
	m_useMultipleRealSenseDevices = configReaderIn->getBoolFromStartupConfig("use_multiple_real_sense_device");

	//Der CTOR (Constructor) wird einmal am Anfang(wenn die Klasse erstellt wird) aufgerufen und mit "new" wird Speicher auf dem Heap allokiert.
	//New gibt immer einen Pointer auf den allokierten Speicher zur�ck
	m_PcVertices = new osg::Vec3Array; //Das ist eine VertexListe. Also die 3D Punkte des Meshes. Das ist unser VBO = VertexBufferObject (ist ein OpenGL Begriff)
	m_PcIndicies = new osg::DrawElementsUInt(osg::PrimitiveSet::POINTS, 0); //Das sind die Indizies. Ein Dreieck hat z.B. drei Indizies. Ein Dreieck ist ein Face (oder auch Polygon genannt). Jedes Face besitzt eine Anzahl von Indizies (Dreiecke halt drei), mit denen es definiert ist. Ein Indizie beschreibt einen Vertex einmalig. So kann einem Face klare 3DPunkte im Raum zugewiesen werden
	m_PcColorArray = new osg::Vec4Array; //Das ColorArray speichert f�r jeden Vertex eine Farbe in RGBA
	m_PcColorArray->setBinding(osg::Array::BIND_PER_VERTEX);
	m_PcNormals = new osg::Vec3Array;
	m_PcRanks = new std::vector<unsigned char>;

	m_PcGeometry = new osg::Geometry(); //Die Geometrie bekommt sp�ter das VertexArray, IndexArray und ColorArray und ist halt die zu rendernde Geometry
	m_PcGeode = new osg::Geode(); //An die Geode kann man Geometrien "childen". An Geoden kann man Shader packen, die dann definieren, wie die angehangenden (gechildeten) Geometrien aussehen
	m_PcTexGeode = new osg::Geode();

	m_matTrans = new osg::MatrixTransform(); //Die Matrix, woran diese ganze Klasse h�ngt.
	m_matTrans->setName("MatTransPointCloudVisulizer");
	//osg::Matrix matrix;
	//matrix.makeRotate(osg::DegreesToRadians(180.0f), osg::Vec3f(0.0, 0.0, 1.0)); //Rotationsmatrix um 180Grad um positive Z Achse (Die PointCloud stand urspr�nglich auf dem Kopf)
	//m_matTrans->setMatrix(matrix);
	//m_PcGeode->setCullingActive(false);
	m_matTrans->addChild(m_PcGeode);
	//m_matTrans->addChild(m_PcTexGeode);
	//m_geometryManager->addChild(m_matTrans);
	pointCloudGroup->addChild(m_matTrans);

	//For real sense depth frame data
	//In diesem Abschnitt wird ein PolyMode erstellt, der an die Geode gehangen wird. Anstatt hier ein Shader einzuh�ngen, sage ich manuell, 
	// dass die Geomtrie als einfach Punkte gerendert werden soll.
	osg::ref_ptr<osg::PolygonMode> polymode = new osg::PolygonMode;
	//##########################################################################################
	// BE AWARE OF THE UGLY NVIDIA QUADRO HACK!!!!
	//##########################################################################################
	polymode->setMode(osg::PolygonMode::FRONT, osg::PolygonMode::POINT);
	m_PcGeode->getOrCreateStateSet()->setAttributeAndModes(polymode.get(), osg::StateAttribute::OVERRIDE | osg::StateAttribute::ON);
	m_pointSize = 4.0f; //TODO: Move into config
	m_PcGeode->getOrCreateStateSet()->setAttributeAndModes(new osg::Point(m_pointSize), osg::StateAttribute::OVERRIDE | osg::StateAttribute::ON);
	m_PcGeode->getOrCreateStateSet()->setMode(GL_LIGHTING, osg::StateAttribute::OFF); //Keine Shading (also Licht-) Effekte (kein Schatten)
	//m_PcGeode->getOrCreateStateSet()->setTextureMode(0, GL_TEXTURE_2D, osg::StateAttribute::OVERRIDE | osg::StateAttribute::OFF);

	//MarchingCubes VaVec for bodyScanning
	if (m_pointCloudMode == PointCloudMode::BODY_SCANNING) {
		m_bodyScanOrigin = RoundVector(BODY_SCAN_AREA_ORIGIN);
		m_bodyScanMax = RoundVector(BODY_SCAN_AREA_MAX);
		osg::Vec3 gridOffsetVector = osg::Vec3(GRID_OFFSET, GRID_OFFSET, GRID_OFFSET);
		m_bodyScanVolume = osg::BoundingBox(m_bodyScanOrigin - gridOffsetVector, BODY_SCAN_AREA_MAX + gridOffsetVector);
		m_bodyScanDimX = (size_t) roundf((m_bodyScanVolume._max.x() - m_bodyScanVolume._min.x()) / GRID);
		m_bodyScanDimY = (size_t) roundf((m_bodyScanVolume._max.y() - m_bodyScanVolume._min.y()) / GRID);
		m_bodyScanDimZ = (size_t) roundf((m_bodyScanVolume._max.z() - m_bodyScanVolume._min.z()) / GRID);
		m_bodyScanDimXY = m_bodyScanDimX * m_bodyScanDimY;
		m_PcVaVec = new OptimizedBoolVector(m_bodyScanDimXY * m_bodyScanDimZ);
	}

	//Hashing
	//m_vertexSet = new std::unordered_set<osg::Vec3, Vec3fHasher, Vec3fComparer>();
	//Replaced the unorderedSet by an unorderdMap. While the key remains the same, the vertex indices are stored as the value and thus, allows for accessing the additional data like uvs, once the vertices are out of the buffer.
	//m_PcVertexMap = new std::unordered_map<osg::Vec3, int, Vec3fHasher, Vec3fComparer>();

	m_bufferMaxWidth = 0;
	m_bufferMaxHeight = 0;

	//History stores the number of added elements for one snapshot, so that can be reversed later on
	m_PcHistory = new std::stack<int>();

	m_use3Dprint = m_configReader->getBoolFromStartupConfig("scanning_use_3d_print");
	m_filterPointCloudBasedOnOctree = m_configReader->getBoolFromStartupConfig("filter_point_cloud_based_on_octree");
	m_scanningClippingDistanceNear = m_configReader->getFloatFromStartupConfig("scanning_clipping_distance_near");
	m_scanningClippingDistanceFar = m_configReader->getFloatFromStartupConfig("scanning_clipping_distance_far");
	m_scanningTSDFTruncationRegion = m_configReader->getFloatFromStartupConfig("scanning_tsdf_truncation_region");

	m_enableTextureProcessing = m_configReader->getBoolFromStartupConfig("enable_texture_processing");

	float volumeSize = m_configReader->getFloatFromStartupConfig("scanning_octree_volume_size");
	osg::Vec3 volumeCenterPosition;
	if (m_configReader->getBoolFromStartupConfig("scanning_octree_center_position_auto_compute"))
	{
		volumeCenterPosition = osg::Vec3(0, volumeSize / 2.0f - 0.1f * volumeSize, 0);
	} 	else
	{
		volumeCenterPosition = m_configReader->getVec3fFromStartupConfig("scanning_octree_center_position");
	}

	m_octreeVolume = new OctreeVolume<float>(
		OCTREE_INITIAL_VALUE,
		volumeCenterPosition,
		volumeSize,
		m_configReader->getFloatFromStartupConfig("scanning_octree_grid_size"),
		m_configReader->getFloatFromStartupConfig("scanning_tsdf_truncation_region"),
		m_configReader->getFloatFromStartupConfig("scanning_tsdf_truncation_region_base_distance")
		);

	//For real sense color frame data
	m_texture = new osg::Texture2D;
	m_texture->setDataVariance(osg::Object::DYNAMIC); //Texture wird erstellt und festgelegt, dass die textur sich st�ndig �ndert -> DYNAMIC
	m_PcTexcoords = new osg::Vec2Array(); //TexturKoordinaten sind immer 2D
	m_PcGeometry->setTexCoordArray(0, m_PcTexcoords);
	m_imageStream = new osg::ImageStream(); //Ein OSG DatenTyp, der das Video von der RealSense entgegennimmt, wird fertig gemacht
	m_texture->setImage(m_imageStream);
	m_texture->setResizeNonPowerOfTwoHint(false); //Textur kann auch andere Werte als 256x256 oder 1024x1024 haben, n�mlich auch 1920x1080

	m_texture->setFilter(osg::Texture::MIN_FILTER, osg::Texture::LINEAR);
	m_texture->setFilter(osg::Texture2D::MAG_FILTER, osg::Texture2D::LINEAR);
	m_texture->setWrap(osg::Texture::WRAP_S, osg::Texture::CLAMP_TO_BORDER);//CLAMP_TO_EDGE);
	m_texture->setWrap(osg::Texture::WRAP_T, osg::Texture::CLAMP_TO_BORDER);
	//m_imageStream->allocateImage(RS_WIDTH, RS_HEIGHT, 1, GL_RGB, GL_UNSIGNED_BYTE); //GL_UNSIGNED_BYTE sagt hier, wie die einzlenen Farbwerte von der Kamera interpretiert werden sollen

	//Hier wird jetzt gesagt, dass das Programm VBOs anstatt DisplayLists nutzen soll
	//1. EXTREME PERFORMANCE BOOST: Use VBOs!
	m_PcGeometry->setUseDisplayList(false);
	m_PcGeometry->setUseVertexBufferObjects(true);
	m_PcGeometry->setDataVariance(osg::Object::DYNAMIC);

	//2. EXTREME PERFORMANCE BOOST: Don't do this every frame. This is neseccary every frame if using displayList, but not for using VBO
	m_PcGeometry->setVertexArray(m_PcVertices);
	m_PcGeometry->addPrimitiveSet(m_PcIndicies);
	m_PcGeometry->setColorArray(m_PcColorArray);

	//Quad f�r die VideoTextur
	osg::ref_ptr<osg::Drawable> quad =
		osg::createTexturedQuadGeometry(
			osg::Vec3(5.0f, 5.0f, 15.0f),
			osg::Vec3(15.0f * 1.777f, 0.0f, 0.0f),
			osg::Vec3(0.0f, 15.0f, 0.0f));
	quad->getOrCreateStateSet()->setTextureAttributeAndModes(0, m_texture.get(), osg::StateAttribute::ON);
	m_PcTexGeode->addChild(quad.get());
	m_PcTexGeode->getOrCreateStateSet();
	m_PcTexGeode->getOrCreateStateSet()->setMode(GL_LIGHTING, osg::StateAttribute::OFF);

	m_PcGeometry->setInitialBound(osg::BoundingBox(-10.0f, -10.0f, -10.0f, 10.0f, 10.0f, 10.0f));
	m_PcGeometry->dirtyBound();

	//Marching Cubes geometry
	m_MCmeshGeode = new osg::Geode();
	m_MCmeshGeode->getOrCreateStateSet()->setMode(GL_LIGHTING, osg::StateAttribute::OVERRIDE | osg::StateAttribute::ON);

	////Texture projection
	m_textureProcessingOutputResolution = m_configReader->getIntFromStartupConfig("texture_processing_output_resolution");
	m_textureProcessingDepthBias = m_configReader->getFloatFromStartupConfig("texture_processing_depth_bias");
	/*m_polygonizedPCTexture = new osg::Texture2D;
	m_polygonizedPCTexture->setDataVariance(osg::Object::DYNAMIC);
	m_polygonizedPCImageStream = new osg::ImageStream();
	m_polygonizedPCTexture->setImage(m_polygonizedPCImageStream);
	m_polygonizedPCTexture->setResizeNonPowerOfTwoHint(false); //Textur kann auch andere Werte als 256x256 oder 1024x1024 haben, n�mlich auch 1920x1080
	*/

	//Add a light into the dark
	osg::ref_ptr<osg::Light> light = new osg::Light;
	//light->setLightNum(num);
	//light->setDiffuse(color);
	light->setPosition(osg::Vec4(1.0f, 2.0f, 0.0f, 1.0f));
	osg::ref_ptr<osg::LightSource> lightSource = new osg::LightSource;
	lightSource->setLight(light);
	//light->setDirection(osg::Vec3f(0.0, -1.0, 0.0));
	osg::ref_ptr<osg::MatrixTransform> sourceTrans = new osg::MatrixTransform;
	sourceTrans->setMatrix(osg::Matrix::translate(osg::Vec3f(0.0f, 0.0f, 0.0f)));
	sourceTrans->addChild(lightSource.get());
	m_matTrans->addChild(sourceTrans);

	m_matTrans->getOrCreateStateSet()->setMode(GL_LIGHT1, osg::StateAttribute::ON);

	if (m_use3Dprint) {
		osg::Matrix rsToTrackerRot = osg::Matrix::rotate(osg::DegreesToRadians(180.0f), osg::Vec3(0, 0, 1));
		osg::Matrix rsToTrackerTrans = osg::Matrix::translate(osg::Vec3(0.0225, -0.025743, 0.0471));

		osg::Matrix trackerToClayMoreRot1 = osg::Matrix::rotate(osg::DegreesToRadians(90.0f), osg::Vec3(1, 0, 0));
		osg::Matrix trackerToClayMoreRot2 = osg::Matrix::rotate(osg::DegreesToRadians(180.0f), osg::Vec3(0, 1, 0));

		m_trackerToSensorMatrix = rsToTrackerRot * rsToTrackerTrans * trackerToClayMoreRot1 * trackerToClayMoreRot2;
	} else {
		osg::Matrix rot = osg::Matrix::rotate(osg::DegreesToRadians(90.0f), osg::Vec3(1, 0, 0));
		osg::Matrix rot2 = osg::Matrix::rotate(osg::DegreesToRadians(180.0f), osg::Vec3(0, 0, 1));
		osg::Matrix trans = osg::Matrix::translate(osg::Vec3(-0.0199999, -0.0070963, 0.00877001)); // Value for directly connected tracker and RealSense
		m_trackerToSensorMatrix = trans * rot2 * rot;
	}
}

PointCloudVisualizer::~PointCloudVisualizer() {
	delete m_PcHistory;
	delete m_octreeVolume;
}

//Neue Funktion, die auch den ColorFrame der RealSense verarbeitet. Wird �ber main.cpp jeden frame geupdatet
void PointCloudVisualizer::visualizeRSPointCloud(/*rs2::video_frame colorFrameIn*/)
{
	//std::cout << "In visualizeRSPointCloud" << std::endl;
	//Wenn in m_PcGeode keine Geometrie (auch Drawable genannt) gechildet ist...
	if (m_PcGeode->getNumDrawables() == 0)
	{
		//... dann f�ge eine hinzu
		m_PcGeode->addDrawable(m_PcGeometry);
		//...und auch das "PrimitiveSet". Das PrimitiveSet ist die IndexListe, welche die Triangles (Faces) bzw. hier im Fall die Punkte visualisert
		m_PcGeometry->addPrimitiveSet(m_PcIndicies);
	}

	//Jeder Frame werden die Arrays leer gemacht um sie sp�ter wieder aufzuf�llen mit neuen Daten
	//m_PcVertices->clear();
	//m_PcIndicies->clear();
	//m_PcTexcoords->clear();
	//m_PcColorArray->clear();

	//Falls kein Colorframe drin ist(wieso auch immer), dann raus gehen aus der Funktion, da es sonst knallt
	/*if (!colorFrameIn) {
		std::cout << "No Color frame - returning" << std::endl;
		return;
	}


	//auto format = colorFrameIn.get_profile().format();
	auto width = colorFrameIn.get_width();
	auto height = colorFrameIn.get_height();
	//auto stream = colorFrameIn.get_profile().stream_type();

	//std::cout << "width: " << width << std::endl;
	//std::cout << "height: " << height << std::endl;

	m_imageStream->setImage(
		width,
		height,
		1,
		GL_RGB,
		GL_RGB,
		GL_UNSIGNED_BYTE,
		const_cast<unsigned char*>(static_cast<const unsigned char*>(colorFrameIn.get_data())), //Muss man nicht verstehen, da wird nur etwas recht kompliziert gecastet
		osg::Image::AllocationMode::NO_DELETE); //Bild der RealSense wird auf den ImageStream gesetzt
	//Read the data from the buffer and push them into the lists*/
	int count = m_PcVertices->getNumElements();

	unsigned int addedPoints = 0;
	for (unsigned int b = 0; b < m_PcBuffers.size(); b++) {
		PointCloudBuffer& pcBuffer = m_PcBuffers.at(b);
		osg::Vec3 normal = pcBuffer.getNormal();
		for (unsigned int i = 0; i < pcBuffer.getValidEntries(); i++) {
			m_PcIndicies->push_back(count++);
			m_PcVertices->push_back(pcBuffer.getVertex(i));
			m_PcColorArray->push_back(pcBuffer.getColor(i));
			//m_PcColorArray->push_back(m_imageStream->getColor(m_PcBuffer->getTexcoord(i)));
			m_PcTexcoords->push_back(pcBuffer.getTexcoord(i));
			m_PcNormals->push_back(normal);
			m_PcRanks->push_back(1);
		}
		for (unsigned int i = 0; i < pcBuffer.getValidBlendEntries(); i++) {
			//Do not add these values but blend them with the exisiting data
			unsigned int& globalIndex = pcBuffer.getGlobalIndex(i);
			unsigned char& rank = (*m_PcRanks)[globalIndex];
			if (rank < 255) { //maximum of char
				float a = 1.0f / ++rank;
				osg::Vec4& currentColor = (*m_PcColorArray)[globalIndex];
				currentColor = currentColor * (1.0f - a) + pcBuffer.getBlendColor(i) * a;
				osg::Vec3& currentNormal = (*m_PcNormals)[globalIndex];
				currentNormal = currentNormal * (1.0f - a) + normal * a;
			}
		}
		addedPoints += pcBuffer.getValidEntries();
		pcBuffer.clear(); //Signals the buffer that its data got read and thus, the remaining data is invalid from now on
	}
	m_PcHistory->push(addedPoints);

	m_PcGeometry->getPrimitiveSet(0)->dirty();
	m_PcVertices->dirty(); //dirty hei�t, dass ein flag gesetzt wird, dass sich das Objekt nun ver�ndert hat, damit der neue Stand dargestellt wird
	m_PcColorArray->dirty();
	m_texture->dirtyTextureParameters();
	m_texture->dirtyTextureObject();
	m_PcTexcoords->dirty();
	m_PcNormals->dirty();
	m_imageStream->dirty();
}

void PointCloudVisualizer::clearPointCloudDisplay() {
	m_PcVertices->clear();
	m_PcIndicies->clear();
	m_PcTexcoords->clear();
	m_PcColorArray->clear();
	m_PcNormals->clear();
	m_PcRanks->clear();

	m_PcVertices->dirty();
	m_PcIndicies->dirty();
	m_PcTexcoords->dirty();
	m_PcColorArray->dirty();
	m_PcNormals->dirty();
}

void PointCloudVisualizer::clearPointCloudData() {
	while (!m_PcHistory->empty()) m_PcHistory->pop();
	for (auto& pcBuffer : m_PcBuffers) pcBuffer.clear();
	//m_vertexSet->clear();
	//m_PcVertexMap->clear();
	m_octreeVolume->getOctree().clear();
	if (m_pointCloudMode == PointCloudMode::BODY_SCANNING) m_PcVaVec->fill(false);
}

void PointCloudVisualizer::processAKPointCloud()
{

}


void PointCloudVisualizer::clearPointCloudFull() {
	clearPointCloudData();
	clearPointCloudDisplay();
}

// ### START - Old Scan Pipeline ###
//void PointCloudVisualizer::processRSPointCloud(rs2::points pointsIn,
//	osg::Matrix matrixTrackingIn,
//	rs2::video_frame colorFrameIn,
//	rs2::depth_frame depthFrameIn)
//{
//	//std::cout << "--- sizeof(pointsIn): " << sizeof(pointsIn) << std::endl;
//
//	if (!colorFrameIn) {
//		std::cout << "No Color frame - returning" << std::endl;
//		return;
//	}
//	//auto format = colorFrameIn.get_profile().format();
//	auto width = colorFrameIn.get_width();
//	auto height = colorFrameIn.get_height();
//	//auto stream = colorFrameIn.get_profile().stream_type();
//
//	//double cfTs = colorFrameIn.get_timestamp();
//
//	//std::cout << "### colorFrameIn.get_frame_timestamp_domain: " << colorFrameIn.get_frame_timestamp_domain() << std::endl;
//	//std::cout << "### colorFrameIn.get_frame_timestamp: " << std::fixed << cfTs << std::endl;
//
//	//std::cout << "### colorFrameIn.get_stride_in_bytes: " << colorFrameIn.get_stride_in_bytes() << std::endl;
//	//std::cout << "### colorFrameIn.get_profile().format(): " << colorFrameIn.get_profile().format() << std::endl;
//	//std::cout << "### colorFrameIn.get_bytes_per_pixel: " << colorFrameIn.get_bytes_per_pixel() << std::endl;
//
//	m_imageStream->setImage(
//		width,
//		height,
//		1,
//		GL_RGB,
//		GL_RGB,
//		GL_UNSIGNED_BYTE,
//		const_cast<unsigned char*>(reinterpret_cast<const unsigned char*>(colorFrameIn.get_data())), //Muss man nicht verstehen, da wird nur etwas recht kompliziert gecastet
//		osg::Image::AllocationMode::NO_DELETE); //Bild der RealSense wird auf den ImageStream gesetzt
//
//	//const_cast<unsigned char*>(static_cast<const unsigned char*>(colorFrameIn.get_data()))
//	auto vertices = pointsIn.get_vertices();
//	auto tex_coords = pointsIn.get_texture_coordinates();
//
//	unsigned int size = m_PcVertices->size();
//	unsigned int localSize = size;
//	m_PcBuffer->clear();
//
//	osg::Matrix sensorMatrix;
//	osg::Vec3 normal;
//	if (m_ARCoreTracking)
//	{
//		sensorMatrix = matrixTrackingIn;
//		normal = sensorMatrix.getRotate() * -RS_FORWARD;
//	}
//	else
//	{
//		if (m_useMultipleRealSenseDevices)
//		{
//			const osg::Matrixf RS_TRACKER_OFFSET_MATRIX_TEMP = osg::Matrixf(
//				1.0f, 0.0f, 0.0f, 0.0f,
//				0.0f, 1.0f, 0.0f, 0.0f,
//				0.0f, 0.0f, 1.0f, 0.0f,
//				0.032f, 0.0f, 0.0f, 1.0f);
//			sensorMatrix = RS_TRACKER_OFFSET_MATRIX_TEMP * matrixTrackingIn;
//			normal = sensorMatrix.getRotate() * -RS_FORWARD;
//		}
//		else
//		{
//			//trackerMatrix = m_PcBuffer->getMatrix().getMatrix(); //Frage an Damian von Philipp: Wieso holst du dir das aus dem Buffer? War das noch zu Zeiten der HapticGruppe bez�glich zeitlicher Synchronisation?
//			//normal = trackerMatrix.getRotate() * -RS_FORWARD;
//			//trackerMatrix = RS_TO_TRACKER_MATRIX * trackerMatrix;
//
//			//osg::Matrixf test = osg::Matrixf::rotate(osg::DegreesToRadians(-90.0f), osg::Vec3f(0.0f, 1.0f, 0.0f));
//
//			//osg::Matrix inverseTrackerMatrix = m_PcBuffer->getMatrix().getMatrix();
//			////osg::Matrix inverseTrackerMatrix = matrixTrackingIn->getMatrix();
//			////osg::Matrix inverseTrackerMatrix = m_PcBuffer->getMatrix().getInverseMatrix();
//			//inverseTrackerMatrix = osg::Matrix::inverse(inverseTrackerMatrix);
//
//
//			//trackerMatrix = test * inverseTrackerMatrix;
//
//			//osg::Vec3f transformedPointBasedOnTracking(vertices->x, vertices->y, vertices->z);
//
//			//TODO 3: Add the tracker-to-RS-transforms to the config file so that arbitrary tracker-RS-Rigs are possible
//			sensorMatrix = m_trackerToSensorMatrix * matrixTrackingIn;
//		}
//	}
//
//	m_PcBuffer->pushNormal(normal);
//	Octree<float>& octree = m_octreeVolume->getOctree();
//	osg::Vec3f sensorPosition = sensorMatrix.getTrans();
//	osg::Vec3f sensorPositionRel = m_octreeVolume->makeRelative(sensorPosition);
//	//std::cout << "### PointsIn.size: " << pointsIn.size() << std::endl;
//
//#define ASYNC_INSERT_RS_POINTS 1
//#define ASYNC_DDA 1
//
//#if ASYNC_INSERT_RS_POINTS == 1 || ASYNC_DDA == 1
//	const unsigned int threadCount = 6u;// std::thread::hardware_concurrency();
//	std::future<void> threads[threadCount];
//	size_t start = 0;
//	const size_t slice = pointsIn.size() / threadCount;
//#endif
//
//	TimeWriter tw;
//	tw.startMetering("Insert RS Points", true, false);
//#if ASYNC_INSERT_RS_POINTS == 1
//	// ### InsertRSPoints ###
//	//Create temporary arrays to hold the data that has to be copied to the buffer
//	unsigned int bufferSize[threadCount]{0};
//	osg::Vec3* vertexBuffer[threadCount];
//	osg::Vec2* texCoordBuffer[threadCount];
//	osg::Vec4* colorBuffer[threadCount];
//	for (int i = 0; i < threadCount; i++) {
//		size_t dataSlice = i == threadCount - 1 ? pointsIn.size() - start : slice;
//		vertexBuffer[i] = new osg::Vec3[dataSlice];
//		texCoordBuffer[i] = new osg::Vec2[dataSlice];
//		colorBuffer[i] = new osg::Vec4[dataSlice];
//	}
//
//	// Start threads
//	for (int t = 0; t < threadCount; t++) {
//		size_t dataSlice = t == threadCount - 1 ? pointsIn.size() - start : slice;
//		threads[t] = std::async(std::launch::async, &PointCloudVisualizer::insertRSPoints, this, dataSlice, vertices + start, tex_coords + start, sensorMatrix, bufferSize + t, vertexBuffer[t], texCoordBuffer[t], colorBuffer[t]);
//		//insertRSPoints(dataSlice, vertices + start, tex_coords + start, trackerMatrix, m_filterPointCloudBasedOnOctree, bufferSize + t, vertexBuffer[t], texCoordBuffer[t], colorBuffer[t]);
//		//std::cout << "Thread " << &threads[t] << ": " << threads[t].valid() << std::endl;
//		start += slice;
//	}
//	// Wait for threads to finish
//	for (int t = 0; t < threadCount; t++) {
//		threads[t].wait();
//		//std::cout << "Thread " << &threads[t] << ": stopped" << std::endl;
//	}
//
//	//Copy data from bufferArrays to the PointCloudBuffer and dispense the temporary arrays
//	for (int i = 0; i < threadCount; i++) {
//		m_PcBuffer->pushData(bufferSize[i], vertexBuffer[i], texCoordBuffer[i], colorBuffer[i]);
//		delete[] vertexBuffer[i];
//		delete[] texCoordBuffer[i];
//		delete[] colorBuffer[i];
//	}
//#else
//	for (unsigned int i = 0; i < pointsIn.size(); i++)
//	{
//		osg::Vec3f vertex(vertices->x, vertices->y, vertices->z);
//		if (!(vertex.x() == 0.0 && vertex.y() == 0.0 && vertex.z() == 0.0))
//		{
//			vertex = vertex * trackerMatrix;
//			//RoundVectorSet(vertex);
//			if (m_pointCloudMode == PointCloudMode::ROOM_SCANNING || (m_pointCloudMode == PointCloudMode::BODY_SCANNING && m_bodyScanVolume.contains(vertex))) //For RoomScanning accept all points
//			{
//				osg::Vec4 color = m_imageStream->getColor(osg::Vec2(tex_coords->u, tex_coords->v));
//				//if (m_vertexSet->insert(vertex).second) {
//
//				osg::Vec3 vertexRel = m_octreeVolume->makeRelative(vertex);
//
//				Octree<float>* child = octree.getOctree(vertexRel, false);
//				if (child != nullptr) {
//					const bool isMaxDepth = child->isMaxDepth();
//					if (!isMaxDepth) {
//						// insert new value
//						child->setValueInHierarchy(0.f, vertexRel);
//
//						if (m_pointCloudMode == PointCloudMode::BODY_SCANNING) {
//							//Insert the point directly into the vavec as well
//							setVaVecValue(vertex, true);
//						}
//						size++;
//					} else {
//						// blend values maybe
//					}
//					if (!filterPointCloudBasedOnOctree || !isMaxDepth) m_PcBuffer->emplaceBack(vertex.x(), vertex.y(), vertex.z(), tex_coords->u, tex_coords->v, color.r(), color.g(), color.b(), color.a());
//				}
//
//				/*
//				//Insert the vertex into the map. This returns a pair which second value is a bool indicating if the vertex was added.
//				auto kv = m_PcVertexMap->insert({ vertex, size });
//				if (kv.second) {
//					m_PcBuffer->emplaceBack(vertex.x(), vertex.y(), vertex.z(), tex_coords->u, tex_coords->v, color.r(), color.g(), color.b(), color.a());
//
//					octree.setValueInHierarchy(color.r(), m_octreeVolume->makeRelative(vertex.x(), 0), m_octreeVolume->makeRelative(vertex.y(), 1), m_octreeVolume->makeRelative(vertex.z(), 2));
//					if (m_pointCloudMode == PointCloudMode::BODY_SCANNING) {
//						//Insert the point directly into the vavec as well
//						setVaVecValue(vertex, true);
//					}
//					size++;
//				}
//				else if (kv.first->second < m_PcVertices->size()) {
//					m_PcBuffer->emplaceBack(kv.first->second, color.r(), color.g(), color.b(), color.a()); //Does not blend values from same frame
//				}*/
//			}
//		}
//		vertices++;
//		tex_coords++;
//	}
//#endif // ASYNC INSERT RS POINTS
//	tw.endMetering();
//	tw.startMetering("DDA", true, false);
//#if ASYNC_DDA == 1
//	//### DDA ###
//	auto vertices2 = pointsIn.get_vertices();
//	//uint16_t* data = (uint16_t*)depthFrameIn.get_data();
//
//	start = 0;
//	for (int t = 0; t < threadCount; t++) {
//		size_t dataSlice = t == threadCount - 1 ? pointsIn.size() - start : slice;
//		threads[t] = std::async(std::launch::async, &PointCloudVisualizer::ddaTraversalForRSPoints, this, dataSlice, vertices2 + start, sensorMatrix);
//		//std::cout << "Thread " << &threads[t] << ": " << threads[t].valid() << std::endl;
//		start += slice;
//	}
//	for (int t = 0; t < threadCount; t++) {
//		threads[t].wait();
//		//std::cout << "Thread " << &threads[t] << ": stopped" << std::endl;
//	}
//#else
//	//DDA
//	//std::cout << "start DDA" << std::endl;
//
//	auto vertices2 = pointsIn.get_vertices();
//	//uint16_t* data = (uint16_t*)depthFrameIn.get_data();
//
//	//debugClearLines();
//	//m_DbLVertices->reserve(pointsIn.size());
//	//m_DbLColorArray->reserve(pointsIn.size()/2);
//
//	for (unsigned int i = 0; i < pointsIn.size(); i++)
//	{
//		osg::Vec3 vertex(vertices2->x, vertices2->y, vertices2->z);
//		//std::cout << "vertex " << i << ":" << vertex << std::endl;
//		if (!(vertex.x() == 0.0 && vertex.y() == 0.0 && vertex.z() == 0.0))
//		{
//			vertex = vertex * trackerMatrix;
//			osg::Vec3f sensorToVertex = vertex - sensorPosition;
//			Ray ray(sensorPosition, sensorToVertex, sensorToVertex.length()); //TODO: Avoid SQRT() in length. Better to get the original depth map
//			//debugDrawRay(ray.getOrigin(), ray.getDirection(), 2, osg::Vec4(1, 0, 0, 0.5), osg::Vec4(1, 0, 0, 0.5));
//			m_octreeVolume->ddaTraverse(ray, true, false);
//		} else {
//			//std::cout << "Fehler!" << vertex << std::endl;
//		}
//		vertices2++;
//
//		//std::cout << "data distance: " << data[i] * depthScaleIn << std::endl;
//		//std::cout << "ray distance: " << ray.getDistance() << std::endl;
//	}
//	//debugRedrawLines();
//	//std::cout << "end DDA" << std::endl;
//
//	//m_octreeVolume->ddaTraverse(ray);
//
//	//Hier ist der Octree aufgebaut und mit default Werten gef�llt
//	/*for each RSPixel p
//	{
//		ray = createRay(p, trackerPos);
//		ddaTraverse(ray); //TSDF wird geschrieben
//	}
//	do n frames
//	createMeshWithMachringCubes()
//	*/
//#endif // ASYNC DDA
//	tw.endMetering();
//}
//
//
//void PointCloudVisualizer::insertRSPoints(const size_t size, const rs2::vertex* vertices, const rs2::texture_coordinate* tex_coords, const osg::Matrix sensorMatrix, unsigned int* bufferSize, osg::Vec3* vertexBuffer, osg::Vec2* texCoordBuffer, osg::Vec4* colorBuffer) {
//	Octree<float>* octree = &m_octreeVolume->getOctree();
//	for (unsigned int i = 0; i < size; i++) {
//		osg::Vec3f vertex(vertices->x, vertices->y, vertices->z);
//		float depth = vertex.length();
//		if (depth >= m_scanningClippingDistanceNear && depth <= m_scanningClippingDistanceFar) {
//		//if (!(vertex.x() == 0.0f && vertex.y() == 0.0f && vertex.z() == 0.0f)) {
//			vertex = vertex * sensorMatrix;
//			if (m_pointCloudMode == PointCloudMode::ROOM_SCANNING || (m_pointCloudMode == PointCloudMode::BODY_SCANNING && m_bodyScanVolume.contains(vertex))) { //For RoomScanning accept all points
//				osg::Vec4 color = m_imageStream->getColor(osg::Vec2(tex_coords->u, tex_coords->v));
//				osg::Vec3 vertexRel = m_octreeVolume->makeRelative(vertex);
//				
//				Octree<float>* child = octree->getOctree(vertexRel, false, true);
//				if (child != nullptr) {
//					const bool isMaxDepth = child->isMaxDepth();
//					if (!isMaxDepth) {
//						child->setValueInHierarchy(0.f, vertexRel, true, true);
//						
//						unsigned int maxSize = child->getMaxSize();
//						unsigned int x = vertexRel.x() * maxSize;
//						unsigned int y = vertexRel.y() * maxSize;
//						unsigned int z = vertexRel.z() * maxSize;
//						int xInc = child->roundToGrid(x) == x ? -1 : 1;
//						int yInc = child->roundToGrid(y) == y ? -1 : 1;
//						int zInc = child->roundToGrid(z) == z ? -1 : 1;
//
//						octree->getOctreeLocal(x + xInc, y, z, true, true);
//						octree->getOctreeLocal(x, y + yInc, z, true, true);
//						octree->getOctreeLocal(x + xInc, y + yInc, z, true, true);
//						octree->getOctreeLocal(x, y, z + zInc, true, true);
//						octree->getOctreeLocal(x + xInc, y, z + zInc, true, true);
//						octree->getOctreeLocal(x, y + yInc, z + zInc, true, true);
//						octree->getOctreeLocal(x + xInc, y + yInc, z + zInc, true, true);
//
//						/*if (m_pointCloudMode == PointCloudMode::BODY_SCANNING) {
//							//Insert the point directly into the vavec as well
//							setVaVecValue(vertex, true);
//						}*/
//					}
//					if (!m_filterPointCloudBasedOnOctree || !isMaxDepth) {
//						// insert new value into the pointCloud
//						//m_PcBuffer->acquireSemaphore(); // This is most likely a large bottleneck; So either remove this from the multithreading section completely and do this singlethreaded afterwards or split the buffer into 8 separate ones - one for each thread.
//						//m_PcBuffer->emplaceBack(vertex.x(), vertex.y(), vertex.z(), tex_coords->u, tex_coords->v, color.r(), color.g(), color.b(), color.a());
//						//m_PcBuffer->releaseSemaphore();
//
//						*vertexBuffer = vertex;
//						*texCoordBuffer = osg::Vec2(tex_coords->u, tex_coords->v);
//						*colorBuffer = color;
//
//						vertexBuffer++;
//						texCoordBuffer++;
//						colorBuffer++;
//						(*bufferSize)++;
//					}
//				}
//			}
//		}
//		vertices++;
//		tex_coords++;
//	}
//}
//
//void PointCloudVisualizer::ddaTraversalForRSPoints(const size_t size, const rs2::vertex* vertices, const osg::Matrix sensorMatrix) {
//	for (unsigned int i = 0; i < size; i++) {
//		osg::Vec3 vertex(vertices->x, vertices->y, vertices->z);
//		float depth = vertex.length();
//		if (depth >= m_scanningClippingDistanceNear) {
//		//if (!(vertex.x() == 0.f && vertex.y() == 0.f && vertex.z() == 0.f)) {
//			//vertex = vertex * sensorMatrix;
//			//osg::Vec3f sensorToVertex = vertex - sensorPosition;
//			//Ray ray(sensorPosition, sensorToVertex, depth > m_scanningClippingDistanceFar ? -1.f : sensorToVertex.length()); //TODO: Avoid SQRT() in length. Better to get the original depth map
//			Ray ray(sensorMatrix.getTrans(), sensorMatrix.getRotate() * vertex, depth); 
//			m_octreeVolume->ddaTraverse(ray, true, m_scanningClippingDistanceFar, true);
//		}
//		vertices++;
//	}
//}
// ### END - Old Scan Pipeline ###

void PointCloudVisualizer::processPointCloud(int bufferIndex, ColorData* colorData, PointcloudData* pointcloudData, osg::Matrix scanDevicePose, ViewFrustum& debugFrustum, bool writeTSDF) {
	typedef std::pair<osg::Vec3f, Octree<float>*> OctreePair;
	const unsigned int pointCloudMargin = 1;
	ThreadSafeStack<OctreePair> stack;
	/*float west, east, north, south;
	int uMin, uMax, vMin, vMax;

	// Crop view frustum to point cloud
	// West
	for (unsigned int u = 0, bool b = true; u < pointcloudData->width() && b; ++u) {
		for (unsigned int v = 0; v < pointcloudData->height(); ++v) {
			unsigned int i = pointcloudData->width() * v + u;
			float depth = pointcloudData->z(i);
			if (m_scanningClippingDistanceNear <= depth && depth <= m_scanningClippingDistanceFar) {
				west = pointcloudData->x(i) * m_scanningClippingDistanceNear / depth;
				uMin = u;
				b = false;
				break;
			}
		}
	}
	// East
	for (unsigned int u = pointcloudData->width() - 1, bool b = true; u >= 0; --u) {
		for (unsigned int v = 0; v < pointcloudData->height(); ++v) {
			unsigned int i = pointcloudData->width() * v + u;
			float depth = pointcloudData->z(i);
			if (m_scanningClippingDistanceNear <= depth && depth <= m_scanningClippingDistanceFar) {
				east = pointcloudData->x(i) * m_scanningClippingDistanceNear / depth;
				uMax = u;
				b = false;
				break;
			}
		}
	}
	// North
	for (unsigned int v = 0, bool b = true; v < pointcloudData->height(); ++v) {
		for (unsigned int u = 0; u < pointcloudData->width(); ++u) {
			unsigned int i = pointcloudData->width() * v + u;
			float depth = pointcloudData->z(i);
			if (m_scanningClippingDistanceNear <= depth && depth <= m_scanningClippingDistanceFar) {
				north = pointcloudData->y(i) * m_scanningClippingDistanceNear / depth;
				vMin = v;
				b = false;
				break;
			}
		}
	}
	// South
	for (unsigned int v = pointcloudData->height() - 1, bool b = true; v >= 0; --v) {
		for (unsigned int u = 0; u < pointcloudData->width(); ++u) {
			unsigned int i = pointcloudData->width() * v + u;
			float depth = pointcloudData->z(i);
			if (m_scanningClippingDistanceNear <= depth && depth <= m_scanningClippingDistanceFar) {
				south = pointcloudData->y(i) * m_scanningClippingDistanceNear / depth;
				vMax = v;
				b = false;
				break;
			}
		}
	}*/

	unsigned int uMin, uMax, vMin, vMax;
	float west = pointcloudData->width(), east = 0.f, north = pointcloudData->height(), south = 0.f;

	// Fill PointCloudBuffer
	PointCloudBuffer* pcBuffer = nullptr;
	if (bufferIndex > -1) {
		pcBuffer = &(m_PcBuffers[bufferIndex]);
		pcBuffer->clear();
	}
	for (unsigned int i = 0u; i < pointcloudData->size(); ++i) {
		float depth = pointcloudData->z(i);
		if (m_scanningClippingDistanceNear <= depth && depth <= m_scanningClippingDistanceFar) {
			// Crop point cloud area
			// West
			if (west * depth > pointcloudData->x(i) * m_scanningClippingDistanceNear) {
				west = pointcloudData->x(i) * m_scanningClippingDistanceNear / depth;
				uMin = i % pointcloudData->width();
			}
			// East
			if (east * depth < pointcloudData->x(i) * m_scanningClippingDistanceNear) {
				east = pointcloudData->x(i) * m_scanningClippingDistanceNear / depth;
				uMax = i % pointcloudData->width();
			}
			// North
			if (north * depth > pointcloudData->y(i) * m_scanningClippingDistanceNear) {
				north = pointcloudData->y(i) * m_scanningClippingDistanceNear / depth;
				vMin = i / pointcloudData->width();
			}
			// South
			if (south * depth < pointcloudData->y(i) * m_scanningClippingDistanceNear) {
				south = pointcloudData->y(i) * m_scanningClippingDistanceNear / depth;
				vMax = i / pointcloudData->width();
			}

			// Insert point into point cloud
			if (pcBuffer) {
				osg::Vec3f vertex = osg::Vec3f(pointcloudData->x(i), pointcloudData->y(i), pointcloudData->z(i)) * scanDevicePose;

				osg::Vec4 color(1.f, 0.f, 1.f, 1.f);
				if (colorData) {
					unsigned int xCol = pointcloudData->u(i) * colorData->width();
					unsigned int yCol = pointcloudData->v(i) * colorData->height();
					size_t indexCol = yCol * colorData->width() + xCol;
					color = osg::Vec4(colorData->r(indexCol), colorData->g(indexCol), colorData->b(indexCol), colorData->a(indexCol));
				}

				pcBuffer->emplaceBack(vertex.x(), vertex.y(), vertex.z(), pointcloudData->u(i), pointcloudData->v(i), color.r(), color.g(), color.b(), color.a());
			}
		}
	}

	if (!writeTSDF) return;

	// Construct view frustum
	osg::Vec3f NE = osg::Vec3f(east, north, m_scanningClippingDistanceNear);
	osg::Vec3f SE = osg::Vec3f(east, south, m_scanningClippingDistanceNear);
	osg::Vec3f SW = osg::Vec3f(west, south, m_scanningClippingDistanceNear);
	osg::Vec3f NW = osg::Vec3f(west, north, m_scanningClippingDistanceNear);

	float frustumScale = m_scanningClippingDistanceFar / m_scanningClippingDistanceNear;
	float frustumScaleWithTruncationRegion = (m_scanningClippingDistanceFar + 2 * m_scanningTSDFTruncationRegion) / m_scanningClippingDistanceNear;
	ViewFrustum viewFrustum(NE * scanDevicePose, 
							SE * scanDevicePose, 
							SW * scanDevicePose,
							NW * scanDevicePose,
							(NE * frustumScale) * scanDevicePose,
							(SE * frustumScale) * scanDevicePose,
							(SW * frustumScale) * scanDevicePose,
							(NW * frustumScale) * scanDevicePose
	);
	debugFrustum = viewFrustum;

	float octreeVolumeExtents = m_octreeVolume->getExtents();
	float octreeVolumeDiagonal = sqrt(2) * octreeVolumeExtents;
	OctreePair octreePair(m_octreeVolume->getPosition(), &m_octreeVolume->getOctree());
	stack.push(octreePair);
	while (stack.pop(octreePair)) {
		Octree<float>& octree = *octreePair.second;
		osg::Vec3f& octreePos = octreePair.first;
		float extents = octreeVolumeExtents / (1u << octree.getDepth());
		float diagonal = octreeVolumeDiagonal / (1u << octree.getDepth());
		if(!viewFrustum.intersectCubicAABB(octreePos, extents)) continue;
		
		// Back projection
		osg::Vec3f octreePosCam = octreePos * osg::Matrix::inverse(scanDevicePose);

		float u = (octreePosCam.x() / octreePosCam.z() * m_scanningClippingDistanceNear - west) / (east - west) * (uMax - uMin) + uMin;
		float v = (octreePosCam.y() / octreePosCam.z() * m_scanningClippingDistanceNear - north) / (south - north) * (vMax - vMin) + vMin;

#define FILTERING 3
		bool outside = false;
#if FILTERING == 1 // Nearest neighbor
		unsigned int i = pointcloudData->width() * roundf(v) + roundf(u);
		osg::Vec3f pcPos(pointcloudData->x(i), pointcloudData->y(i), pointcloudData->z(i));
#elif FILTERING == 2 // Bilinear
		unsigned int uFloor;
		unsigned int uCeil;
		unsigned int vFloor;
		unsigned int vCeil;

		if (u < uMin) {
			uFloor = uCeil = u = uMin;
			outside = true;
		} else if (u > uMax) {
			uFloor = uCeil = u = uMax;
			outside = true;
		} else {
			uFloor = floorf(u);
			uCeil = ceilf(u);
		}
		if (v < vMin) {
			vFloor = vCeil = v = vMin;
			outside = true;
		} else if (v > vMax) {
			vFloor = vCeil = v = vMax;
			outside = true;
		} else {
			vFloor = floorf(v);
			vCeil = ceilf(v);
		}
		float uWeight = u - uFloor;
		float vWeight = v - vFloor;

		unsigned int i = pointcloudData->width() * v + u;
		osg::Vec3f pcPos((pointcloudData->x(pointcloudData->width() * vFloor + uFloor) * uWeight + pointcloudData->x(pointcloudData->width() * vFloor + uCeil) * (1 - uWeight)) * vWeight +
			(pointcloudData->x(pointcloudData->width() * vCeil + uFloor) * uWeight + pointcloudData->x(pointcloudData->width() * vCeil + uCeil) * (1 - uWeight)) * (1 - vWeight),
			(pointcloudData->y(pointcloudData->width() * vFloor + uFloor) * uWeight + pointcloudData->y(pointcloudData->width() * vFloor + uCeil) * (1 - uWeight)) * vWeight +
			(pointcloudData->y(pointcloudData->width() * vCeil + uFloor) * uWeight + pointcloudData->y(pointcloudData->width() * vCeil + uCeil) * (1 - uWeight)) * (1 - vWeight),
			(pointcloudData->z(pointcloudData->width() * vFloor + uFloor) * uWeight + pointcloudData->z(pointcloudData->width() * vFloor + uCeil) * (1 - uWeight)) * vWeight +
			(pointcloudData->z(pointcloudData->width() * vCeil + uFloor) * uWeight + pointcloudData->z(pointcloudData->width() * vCeil + uCeil) * (1 - uWeight)) * (1 - vWeight));
#elif FILTERING == 3 // Bicubic

		unsigned int uFloor;
		unsigned int vFloor;

		if (u < uMin + pointCloudMargin) {
			uFloor = u = uMin + pointCloudMargin;
			outside = true;
		} else if (u >= uMax - pointCloudMargin) {
			uFloor = u = uMax - pointCloudMargin - 1;
			outside = true;
		} else {
			uFloor = floorf(u);
		}
		if (v < vMin + pointCloudMargin) {
			vFloor = v = vMin + pointCloudMargin;
			outside = true;
		} else if (v >= vMax - pointCloudMargin) {
			vFloor = v = vMax - pointCloudMargin - 1;
			outside = true;
		} else {
			vFloor = floorf(v);
		}
		unsigned int i = pointcloudData->width() * (vFloor - 1) + uFloor;

		float uWeight = u - uFloor;
		float vWeight = v - vFloor;

		//p[1] + 0.5 * x * (p[2] - p[0] + x * (2.0 * p[0] - 5.0 * p[1] + 4.0 * p[2] - p[3] + x * (3.0 * (p[1] - p[2]) + p[3] - p[0])));
		osg::Vec3f p_v[4];
		for (unsigned int j = 0u; j < 4u; ++j) {
			osg::Vec3f p_u[4];
			for (unsigned int k = 0u; k < 4u; ++k) {
				p_u[k] = osg::Vec3f(pointcloudData->x(i + k - 1), pointcloudData->y(i + k - 1), pointcloudData->z(i + k - 1));
				//if (pointcloudData->z(i + k - 1) > m_scanningClippingDistanceNear && pointcloudData->z(i + k - 1) < m_scanningClippingDistanceFar) p_u[k] = osg::Vec3f(pointcloudData->x(i + k - 1), pointcloudData->y(i + k - 1), pointcloudData->z(i + k - 1));
				//else p_u[k] = osg::Vec3f((west + (east - west) * (uFloor + k - 1 - uMin) / (uMax - uMin)), (north + (south - north) * (vFloor + k - 1 - vMin) / (vMax - vMin)), m_scanningClippingDistanceNear) * frustumScaleWithTruncationRegion;
			}
			p_v[j] = p_u[1] + (p_u[2] - p_u[0] + (p_u[0] * 2.f - p_u[1] * 5.f + p_u[2] * 4.f - p_u[3] + ((p_u[1] - p_u[2]) * 3.f + p_u[3] - p_u[0]) * uWeight) * uWeight) * .5f * uWeight;
			i += pointcloudData->width();
		}
		osg::Vec3f pcPos = p_v[1] + (p_v[2] - p_v[0] + (p_v[0] * 2.f - p_v[1] * 5.f + p_v[2] * 4.f - p_v[3] + ((p_v[1] - p_v[2]) * 3.f + p_v[3] - p_v[0]) * uWeight) * uWeight) * .5f * uWeight;
#endif
#undef FILTERING
		// TSDF logic
		float currentTsdf = octree.getValue();
		float sdf = outside ? (pcPos - octreePosCam).length() : pcPos.length() - octreePosCam.length();
		if (sdf > -m_scanningTSDFTruncationRegion && (sdf >= 0 || currentTsdf < 0 || currentTsdf == OCTREE_INITIAL_VALUE)) {
			float tsdf = fmaxf(-1.f, fminf(1.f, sdf / m_scanningTSDFTruncationRegion));
			if ((currentTsdf < 0 && tsdf >= 0) || (currentTsdf * tsdf >= 0 && abs(currentTsdf) > abs(tsdf)) || currentTsdf == OCTREE_INITIAL_VALUE) {
				if (viewFrustum.contains(octreePos)) {
					octree.setValue((octree.getValue() * octree.getWeight() + tsdf) / (octree.getWeight() + 1));
					octree.increaseWeight();
				}
			}
		}
		// Push children
		if (octree.isMaxDepth() || (abs(sdf) > m_scanningTSDFTruncationRegion + diagonal && octree.getMaxDepth() - octree.getDepth() <= 3u  )) continue;
		if (!octree.hasChildren()) octree.split(OCTREE_INITIAL_VALUE);

		float childExtents = extents / 2;
		OctreePair childPairs[8]{
			OctreePair(octreePos + osg::Vec3f(-childExtents, -childExtents, -childExtents), octree.getOctree(0)),	// 000 = 0
			OctreePair(octreePos + osg::Vec3f(childExtents, -childExtents, -childExtents), octree.getOctree(1)),	// 001 = 1
			OctreePair(octreePos + osg::Vec3f(-childExtents, childExtents, -childExtents), octree.getOctree(2)),	// 010 = 2
			OctreePair(octreePos + osg::Vec3f(childExtents, childExtents, -childExtents), octree.getOctree(3)),		// 011 = 3
			OctreePair(octreePos + osg::Vec3f(-childExtents, -childExtents, childExtents), octree.getOctree(4)),	// 100 = 4
			OctreePair(octreePos + osg::Vec3f(childExtents, -childExtents, childExtents), octree.getOctree(5)),		// 101 = 5
			OctreePair(octreePos + osg::Vec3f(-childExtents, childExtents, childExtents), octree.getOctree(6)),		// 110 = 6
			OctreePair(octreePos + osg::Vec3f(childExtents, childExtents, childExtents), octree.getOctree(7))		// 111 = 7
		};
		stack.pushBatch(childPairs, 8);
	}
	//m_octreeVolume->getOctree().minimize();
}

void PointCloudVisualizer::processPointCloud(unsigned int bufferIndex, DepthData* depthData, ColorData* colorData, PointcloudData* pointcloudData, osg::Matrix scanDevicePose, bool writeTSDF) {
	if (!colorData) {
		std::cout << "No Color frame - returning" << std::endl;
		return;
	}
	renderVideoTexture(colorData);

	PointCloudBuffer& pcBuffer = m_PcBuffers.at(bufferIndex);
	pcBuffer.clear();

	size_t size = pointcloudData->size();
	Octree<float>& octree = m_octreeVolume->getOctree();
	osg::Vec3f sensorPosition = scanDevicePose.getTrans();
	osg::Vec3f sensorPositionRel = m_octreeVolume->makeRelative(sensorPosition);

#define ASYNC_INSERT_RS_POINTS 1
#define ASYNC_DDA 1

#if ASYNC_INSERT_RS_POINTS == 1 || ASYNC_DDA == 1
	const unsigned int slices = 6u;// std::thread::hardware_concurrency();
	const unsigned int threadCount = slices - 1u;
	std::vector<std::future<void>> threads;
	threads.reserve(threadCount);
	size_t start = 0;
	const size_t slice = size / slices + 1u; // size must be atleast slices^2 in order for the code to work as expected. idea: The workload of the other threads is increased in order to relieve the current thread.
#endif

	TimeWriter tw;
	const bool writeToConsole = false;
	tw.startMetering("Insert RS Points", writeToConsole, false);
#if ASYNC_INSERT_RS_POINTS == 1
	// ### InsertRSPoints ###
	// Create temporary arrays to hold the data that has to be copied to the buffer
	unsigned int bufferSize[slices]{0};
	osg::Vec3* vertexBuffer[slices];
	osg::Vec2* texCoordBuffer[slices];
	osg::Vec4* colorBuffer[slices];
	for (unsigned int i = 0; i < threadCount; i++) {
		vertexBuffer[i] = new osg::Vec3[slice];
		texCoordBuffer[i] = new osg::Vec2[slice];
		colorBuffer[i] = new osg::Vec4[slice];
	}
	// After starting the other threads, this one will process the last slice; therefore we need to create its buffers of slightly smaller size aswell
	size_t lastSlice = size - slice * threadCount;
	vertexBuffer[threadCount] = new osg::Vec3[lastSlice];
	texCoordBuffer[threadCount] = new osg::Vec2[lastSlice];
	colorBuffer[threadCount] = new osg::Vec4[lastSlice];

	// Start threads
	for (int t = 0; t < threadCount; t++) {
		//threads[t] = std::async(std::launch::async, &PointCloudVisualizer::insertPoints, this, start, slice, depthData, colorData, pointcloudData, scanDevicePose, bufferSize + t, vertexBuffer[t], texCoordBuffer[t], colorBuffer[t], writeTSDF);
		threads.push_back(std::async(std::launch::async, &PointCloudVisualizer::insertPoints, this, start, slice, depthData, colorData, pointcloudData, scanDevicePose, bufferSize + t, vertexBuffer[t], texCoordBuffer[t], colorBuffer[t], writeTSDF));
		start += slice;
	}
	// Process the last slice
	insertPoints(start, lastSlice, depthData, colorData, pointcloudData, scanDevicePose, bufferSize + threadCount, vertexBuffer[threadCount], texCoordBuffer[threadCount], colorBuffer[threadCount], writeTSDF);
	// Wait for threads to finish
	for (int t = 0; t < threadCount; t++) {
		threads[t].wait();
	}

	//Copy data from bufferArrays to the PointCloudBuffer and dispense the temporary arrays
	for (int i = 0; i < slices; i++) {
		pcBuffer.pushData(bufferSize[i], vertexBuffer[i], texCoordBuffer[i], colorBuffer[i]);
		delete[] vertexBuffer[i];
		delete[] texCoordBuffer[i];
		delete[] colorBuffer[i];
	}
#else
	for (unsigned int i = 0; i < size; i++) {
		//osg::Vec3f vertex(vertices->x, vertices->y, vertices->z);
		osg::Vec3f vertex(pointcloudData->x(i), pointcloudData->y(i), pointcloudData->z(i));
		//std::cout << "x: " << vertex.x() << " y: " << vertex.y() << " z: " << vertex.z() << std::endl;
		if (!(vertex.x() == 0.0 && vertex.y() == 0.0 && vertex.z() == 0.0)) {
			vertex = vertex * scanDevicePose;
			if (m_pointCloudMode == PointCloudMode::ROOM_SCANNING || (m_pointCloudMode == PointCloudMode::BODY_SCANNING && m_bodyScanVolume.contains(vertex))) //For RoomScanning accept all points
			{
				//osg::Vec4 color = m_imageStream->getColor(osg::Vec2(tex_coords->u, tex_coords->v));
				osg::Vec4 color = osg::Vec4(colorData->r(i), colorData->g(i), colorData->b(i), colorData->a(i));
				osg::Vec3 vertexRel = m_octreeVolume->makeRelative(vertex);

				Octree<float>* child = octree.getOctree(vertexRel, false);
				if (child != nullptr) {
					const bool isMaxDepth = child->isMaxDepth();
					if (!isMaxDepth) {
						// insert new value
						child->setValueInHierarchy(0.f, vertexRel);

						if (m_pointCloudMode == PointCloudMode::BODY_SCANNING) {
							//Insert the point directly into the vavec as well
							setVaVecValue(vertex, true);
						}
					} else {
						// blend values maybe
					}
					if (!m_filterPointCloudBasedOnOctree || !isMaxDepth) m_PcBuffer->emplaceBack(vertex.x(), vertex.y(), vertex.z(), pointcloudData->u(i), pointcloudData->v(i), color.r(), color.g(), color.b(), color.a());
				}
			}
		}
	}
#endif // ASYNC INSERT RS POINTS
	tw.endMetering();

	if (writeTSDF) {
		tw.startMetering("DDA", writeToConsole, false);
#if ASYNC_DDA == 1
		//### DDA ###
		start = 0;
		for (int t = 0; t < threadCount; t++) {
			//threads[t] = std::async(std::launch::async, &PointCloudVisualizer::ddaTraversalForRSPoints, this, dataSlice, vertices2 + start, sensorMatrix);
			threads[t] = std::async(std::launch::async, &PointCloudVisualizer::ddaTraversalForPoints, this, start, slice, pointcloudData, colorData, scanDevicePose);
			start += slice;
		}
		// Process the last slice
		ddaTraversalForPoints(start, lastSlice, pointcloudData, colorData, scanDevicePose);
		for (int t = 0; t < threadCount; t++) {
			threads[t].wait();
		}
#else
		//DDA
		auto sensorRotation = scanDevicePose.getRotate();
		for (unsigned int i = 0u; i < size; i++) {
			osg::Vec3 vertex(pointcloudData->x(i), pointcloudData->y(i), pointcloudData->z(i));
			float depth = vertex.length();
			if (depth >= m_scanningClippingDistanceNear) {
				Ray ray(sensorPosition, sensorRotation * vertex, depth);
				m_octreeVolume->ddaTraverse(ray, true, m_scanningClippingDistanceFar, true);
			}
		}
#endif // ASYNC DDA
		tw.endMetering();
	}
}

//void PointCloudVisualizer::insertRSPoints(const size_t size, const rs2::vertex* vertices, const rs2::texture_coordinate* tex_coords, const osg::Matrix sensorMatrix, unsigned int* bufferSize, osg::Vec3* vertexBuffer, osg::Vec2* texCoordBuffer, osg::Vec4* colorBuffer) {
void PointCloudVisualizer::insertPoints(const size_t start, const size_t size, DepthData* depthData, ColorData* colorData, PointcloudData* pointcloudData, osg::Matrix scanDevicePose, unsigned int* bufferSize, osg::Vec3* vertexBuffer, osg::Vec2* texCoordBuffer, osg::Vec4* colorBuffer, bool writeTSDF) {
	Octree<float>* octree = &m_octreeVolume->getOctree();
	for (unsigned int i = start; i < start + size; i++) {
		osg::Vec3f vertex(pointcloudData->x(i), pointcloudData->y(i), pointcloudData->z(i));
		float depth = vertex.length();
		//float depth = depthData->z(i);
		if (depth >= m_scanningClippingDistanceNear && depth <= m_scanningClippingDistanceFar) {
			vertex = vertex * scanDevicePose;
			if (m_pointCloudMode == PointCloudMode::ROOM_SCANNING || (m_pointCloudMode == PointCloudMode::BODY_SCANNING && m_bodyScanVolume.contains(vertex))) { //For RoomScanning accept all points
				//osg::Vec4 color = osg::Vec4(colorData->r(i), colorData->g(i), colorData->b(i), colorData->a(i));
				//Obtain color value from texCoords and the respective colorData index
				unsigned int xCol = pointcloudData->u(i) * colorData->width();
				unsigned int yCol = pointcloudData->v(i) * colorData->height();
				size_t indexCol = yCol * colorData->width() + xCol;
				osg::Vec4 color = osg::Vec4(colorData->r(indexCol), colorData->g(indexCol), colorData->b(indexCol), colorData->a(indexCol));

				osg::Vec3 vertexRel = m_octreeVolume->makeRelative(vertex);

				Octree<float>* child = octree->getOctree<true>(vertexRel, false);
				if (child != nullptr) {
					const bool isMaxDepth = child->isMaxDepth();
					if (!isMaxDepth) {
						Octree<float>* o = child->setValueInHierarchy<true>(0.f, vertexRel, true);
						if (o && o->isMaxDepth()) {
							// ### Start - Add VertexColors to the octree ###
							OctreeData octreeData{(unsigned char) (255u * color.r()), (unsigned char) (255u * color.g()), (unsigned char) (255u * color.b())};
							o->setData<true>(octreeData);
							// ### End - VertexColors ###

							if (writeTSDF) {
								// Force the surrounding cells to create additional leaf octrees TODO 4: Check again if this is still necessary to increase the quality of our mesh
								unsigned int maxSize = child->getMaxSize();
								unsigned int x = vertexRel.x() * maxSize;
								unsigned int y = vertexRel.y() * maxSize;
								unsigned int z = vertexRel.z() * maxSize;
								int xInc = child->roundToGrid(x) == x ? -1 : 1;
								int yInc = child->roundToGrid(y) == y ? -1 : 1;
								int zInc = child->roundToGrid(z) == z ? -1 : 1;

								//TODO 3: these octrees do not have a specific value asigned. The assignment will happen during the next DDA-call. Colors might be important here though.
								octree->getOctreeLocal<true>(x + xInc, y, z, true);
								octree->getOctreeLocal<true>(x, y + yInc, z, true);
								octree->getOctreeLocal<true>(x + xInc, y + yInc, z, true);
								octree->getOctreeLocal<true>(x, y, z + zInc, true);
								octree->getOctreeLocal<true>(x + xInc, y, z + zInc, true);
								octree->getOctreeLocal<true>(x, y + yInc, z + zInc, true);
								octree->getOctreeLocal<true>(x + xInc, y + yInc, z + zInc, true);
							}
						}
					}
					if (!m_filterPointCloudBasedOnOctree || !isMaxDepth) {
						// insert new value into the pointCloud
						*vertexBuffer = vertex;
						*texCoordBuffer = osg::Vec2(pointcloudData->u(i), pointcloudData->v(i));
						*colorBuffer = color;

						vertexBuffer++;
						texCoordBuffer++;
						colorBuffer++;
						(*bufferSize)++;
					}
				}
			}
		}
	}
}

//void PointCloudVisualizer::ddaTraversalForRSPoints(const size_t size, const rs2::vertex* vertices, const osg::Matrix sensorMatrix) {
void PointCloudVisualizer::ddaTraversalForPoints(const size_t start, const size_t size, PointcloudData* pointcloudData, ColorData* colorData, const osg::Matrix scanDevicePose) {
	auto sensorPos = scanDevicePose.getTrans();
	auto sensorRot = scanDevicePose.getRotate();
	for (unsigned int i = start; i < start + size; i++) {
		osg::Vec3 vertex(pointcloudData->x(i), pointcloudData->y(i), pointcloudData->z(i));
		OctreeData data{(unsigned char) (colorData->r(i) * 255u), (unsigned char) (colorData->g(i) * 255u), (unsigned char) (colorData->b(i) * 255u)};
		float depth = abs(vertex.length());
		//if (depth >= m_scanningClippingDistanceNear) {
		if (depth > 0) {
			Ray ray(sensorPos, sensorRot * vertex, depth);
			m_octreeVolume->ddaTraverse<true>(ray, &data, true, m_scanningClippingDistanceFar);
		}
	}
}

/*
void PointCloudVisualizer::pushMatrix(osg::ref_ptr<osg::MatrixTransform> matrixTransformIn)
{
	m_PcBuffer->pushMatrix(matrixTransformIn);
}

void PointCloudVisualizer::pushMatrix(osg::Matrix matrixTransformIn)
{
	m_PcBuffer->pushMatrix2(matrixTransformIn);
}
*/

void PointCloudVisualizer::savePointCloudWithOSG(std::string savePathIn)
{
	osgDB::writeNodeFile(*m_PcGeometry, savePathIn);

	pcl::io::savePCDFileBinaryCompressed(savePathIn, *convertPointCloudToPCL()); //Works only with pathes containing /, not \
	//pcl::io::savePCDFileASCII("test_pcd.pcd", cloud);
	std::cout << "point cloud saved at " << savePathIn << std::endl;
}

void PointCloudVisualizer::savePointCloudWithOSG(std::string savePathIn, osg::ref_ptr<osg::OpenMeshGeometry> openMeshGeo)
{
	//osgDB::writeNodeFile(*m_PcGeometry, savePathIn);

	pcl::io::savePCDFileBinaryCompressed(savePathIn, *convertPointCloudToPCL(openMeshGeo)); //Works only with pathes containing /, not \
		//pcl::io::savePCDFileASCII("test_pcd.pcd", cloud);
	std::cout << "point cloud saved at " << savePathIn << std::endl;
}

//This function is actually deprecated and only for compatibitly -> it is involved in saving the current point cloud of scanning which is
// attached to the pointcloudmanager root. In future, it is better, when only openMeshGeom can be passed (cleaner API design)
pcl::PointCloud<pcl::PointXYZRGB>::Ptr PointCloudVisualizer::convertPointCloudToPCL()
{
	pcl::PointCloud<pcl::PointXYZRGB>::Ptr cloud(new pcl::PointCloud<pcl::PointXYZRGB>);

	//auto sp = points.get_profile().as<rs2::video_stream_profile>();
	//cloud->width = sp.width();
	//cloud->height = sp.height();
	//cloud->is_dense = false;

	osg::Vec3Array* vertexArray = dynamic_cast<osg::Vec3Array*>(m_PcGeometry->getVertexArray());
	//osg::Vec4Array *colorArray = dynamic_cast<osg::Vec4Array*>(m_PcGeometry->getColorArray());
	cloud->points.resize(vertexArray->getNumElements());
	//auto ptr = points.get_vertices();

	for (unsigned int i = 0; i < m_PcGeometry->getVertexArray()->getNumElements(); i++)
	{
		cloud->points.at(i).x = vertexArray->at(i).x();
		cloud->points.at(i).y = vertexArray->at(i).y();
		cloud->points.at(i).z = vertexArray->at(i).z();
		cloud->points.at(i).r = static_cast<unsigned char>(m_PcColorArray->at(i).x() * 255);
		cloud->points.at(i).g = static_cast<unsigned char>(m_PcColorArray->at(i).y() * 255);
		cloud->points.at(i).b = static_cast<unsigned char>(m_PcColorArray->at(i).z() * 255);
	}

	return cloud;
}


pcl::PointCloud<pcl::PointXYZRGB>::Ptr PointCloudVisualizer::convertPointCloudToPCL(osg::ref_ptr<osg::OpenMeshGeometry> openMeshGeo)
{
	pcl::PointCloud<pcl::PointXYZRGB>::Ptr cloud(new pcl::PointCloud<pcl::PointXYZRGB>);

	//auto sp = points.get_profile().as<rs2::video_stream_profile>();
	//cloud->width = sp.width();
	//cloud->height = sp.height();
	//cloud->is_dense = false;

	osg::Vec3Array* vertexArray = dynamic_cast<osg::Vec3Array*>(openMeshGeo->getVertexArray());
	osg::Vec4Array* colorArray = dynamic_cast<osg::Vec4Array*>(openMeshGeo->getColorArray());
	cloud->points.resize(vertexArray->getNumElements());
	//auto ptr = points.get_vertices();

	for (unsigned int i = 0; i < openMeshGeo->getVertexArray()->getNumElements(); i++)
	{
		cloud->points.at(i).x = vertexArray->at(i).x();
		cloud->points.at(i).y = vertexArray->at(i).y();
		cloud->points.at(i).z = vertexArray->at(i).z();

		//TODO REMOVE: Just DEBUG, that the viewer shows something
		cloud->points.at(i).r = static_cast<unsigned char>(255);
		cloud->points.at(i).g = static_cast<unsigned char>(5);
		cloud->points.at(i).b = static_cast<unsigned char>(5);
		//cloud->points.at(i).r = static_cast<unsigned char>(colorArray->at(i).x() * 255);
		//cloud->points.at(i).g = static_cast<unsigned char>(colorArray->at(i).y() * 255);
		//cloud->points.at(i).b = static_cast<unsigned char>(colorArray->at(i).z() * 255);
	}

	return cloud;
}


void PointCloudVisualizer::loadPointCloudWithOSG(osg::ref_ptr<osg::OpenMeshGeometry> openMeshGeo, std::string loadPathIn)
{
	openMeshGeo->setGeometryType(GeometryType::POINT_CLOUD);
	pcl::PointCloud<pcl::PointXYZRGB>::Ptr cloud(new pcl::PointCloud<pcl::PointXYZRGB>);
	pcl::io::loadPCDFile(loadPathIn, *cloud); // Be aware of the pointer!!! 
	convertPointCloudToOSG(openMeshGeo, cloud);

	std::cout << "point cloud load from " << loadPathIn << std::endl;
}


// Hier muss ein OSG Objekt zur�ck gegeben werden -> bzw besser eine OpenMeshGeometry. Ich sollte ja drauf arbeiten k�nnen.
// Kann ich mit der OpenMeshGeo nur Points rendern?
void PointCloudVisualizer::convertPointCloudToOSG(osg::ref_ptr<osg::OpenMeshGeometry> openMeshGeo, pcl::PointCloud<pcl::PointXYZRGB>::Ptr cloud)
{
	osg::Vec3Array* vertexArray = dynamic_cast<osg::Vec3Array*>(openMeshGeo->getVertexArray());
	osg::Vec4Array* colorArray = dynamic_cast<osg::Vec4Array*>(openMeshGeo->getColorArray());
	osg::DrawElementsUInt* indexArray = dynamic_cast<osg::DrawElementsUInt*>(openMeshGeo->getPrimitiveSet(0)->getDrawElements());

	//vertexArray->resize(cloud->size()); //reserve?
	//colorArray->resize(cloud->size());
	std::cout << "cloud->size() is  " << cloud->size() << std::endl;

	for (unsigned int i = 0; i < cloud->size(); i++)
	{
		openMeshGeo->getMesh()->add_vertex(osg::Vec3f(cloud->points.at(i).x, cloud->points.at(i).y, cloud->points.at(i).z));
		//vertexArray->push_back(osg::Vec3f(cloud->points.at(i).x, cloud->points.at(i).y, cloud->points.at(i).z));
		colorArray->push_back(osg::Vec4f(cloud->points.at(i).r / 255.0f, cloud->points.at(i).g / 255.0f, cloud->points.at(i).b / 255.0f, 1.0f));
	}
	//std::cout << "Vertex Array Size() after loop: " << vertexArray->size() << std::endl;
	//std::cout << "Vertex Array getElementSize after loop: " << vertexArray->getElementSize() << std::endl;

	//UtilProcessor utilProcessor = UtilProcessor();
	//utilProcessor.setActiveGeometry(openMeshGeo);
	//utilProcessor.cleanUpIndicies();
	//utilProcessor.calcNormals();

	//return &cloud;
}


bool PointCloudVisualizer::bufferDataAvailable() {
	for (auto& pcBuffer : m_PcBuffers)
		if (pcBuffer.getValidEntries() != 0) return true;
	return false;
}

void PointCloudVisualizer::updatePointCloudBuffers(unsigned int devicesPerScan, unsigned int newMaxWidth, unsigned int newMaxHeight) {
	//Instantiate Buffers for holding the processed frames of the latest scan
	if (newMaxWidth != m_bufferMaxWidth || newMaxHeight != m_bufferMaxHeight) {
		std::vector<PointCloudBuffer> newBuffer;
		newBuffer.reserve(devicesPerScan);
		for (int i = 0; i < devicesPerScan; ++i) newBuffer.emplace_back(newMaxWidth, newMaxHeight);
		m_PcBuffers.swap(newBuffer); //newBuffer now holds the old deprecated buffers and will be destroyed after leaving the current scope.
	} else {
		int diff = devicesPerScan - m_PcBuffers.size();
		if (diff < 0) { // Currently the buffer has more allocated space than necessary
			std::vector<PointCloudBuffer> newBuffer;
			newBuffer.reserve(devicesPerScan);
			for (unsigned int i = 0; i < devicesPerScan; i++) newBuffer.push_back(std::move(m_PcBuffers.at(i)));
			m_PcBuffers.swap(newBuffer);
		} else if (diff > 0) { // The buffer does not have enough memory to hold all the incoming data
			m_PcBuffers.reserve(devicesPerScan);
			for (unsigned int i = 0; i < diff; i++) m_PcBuffers.emplace_back(newMaxWidth, newMaxHeight);
		}
	}
	m_bufferMaxWidth = newMaxWidth;
	m_bufferMaxHeight = newMaxHeight;
}

bool PointCloudVisualizer::revertSnapshot() {
	if (m_PcHistory->size() == 0) return false;
	int start = m_PcVertices->size() - m_PcHistory->top();
	Octree<float>* octree = &m_octreeVolume->getOctree();
	for (int i = start; i < m_PcVertices->size(); i++) {
		//m_vertexSet->erase((*m_PcVertices)[i]);
		osg::Vec3 vertex = (*m_PcVertices)[i];
		//m_PcVertexMap->erase(vertex);
		//TODO: Octree does not support minimizing the tree or erasing values
		if (m_pointCloudMode == PointCloudMode::BODY_SCANNING)
		{
			setVaVecValue(vertex, false);
		}
		Octree<float>* parent = nullptr;
		Octree<float>* child = octree->getOctree(m_octreeVolume->makeRelative(vertex), parent);
		if (parent != nullptr && child != nullptr) {
			child->setValue(1.0f);
		}
	}
	octree->minimize();
	std::cout << octree->hasChildren() << std::endl;
	m_PcVertices->erase(m_PcVertices->begin() + start, m_PcVertices->end());
	m_PcIndicies->erase(m_PcIndicies->begin() + start, m_PcIndicies->end());
	m_PcColorArray->erase(m_PcColorArray->begin() + start, m_PcColorArray->end());
	m_PcTexcoords->erase(m_PcTexcoords->begin() + start, m_PcTexcoords->end());
	m_PcNormals->erase(m_PcNormals->begin() + start, m_PcNormals->end());
	m_PcRanks->erase(m_PcRanks->begin() + start, m_PcRanks->end());

	m_PcVertices->dirty();
	m_PcIndicies->dirty();
	m_PcTexcoords->dirty();
	m_PcColorArray->dirty();
	m_PcNormals->dirty();
	m_PcHistory->pop();
	return true;
}

/* ### START - Old Scan Pipeline ###
void PointCloudVisualizer::renderVideoTexture(rs2::video_frame colorFrameIn) {
	if (!colorFrameIn) {
		std::cout << "No Color frame - returning" << std::endl;
		return;
	}

	//auto format = colorFrameIn.get_profile().format();
	auto width = colorFrameIn.get_width();
	auto height = colorFrameIn.get_height();
	//auto stream = colorFrameIn.get_profile().stream_type();


	m_imageStream->setImage(
		width,
		height,
		1,
		GL_RGB,
		GL_RGB,
		GL_UNSIGNED_BYTE,
		const_cast<unsigned char*>(static_cast<const unsigned char*>(colorFrameIn.get_data())), //Muss man nicht verstehen, da wird nur etwas recht kompliziert gecastet
		osg::Image::AllocationMode::NO_DELETE); //Bild der RealSense wird auf den ImageStream gesetzt
	m_imageStream->dirty();
}
// ### END - Old Scan Pipeline ### */

void PointCloudVisualizer::renderVideoTexture(ColorData* colorData) {
	if (!colorData) {
		std::cout << "No Color frame - returning" << std::endl;
		return;
	}
	auto width = colorData->width();
	auto height = colorData->height();

	GLenum glFormat = 0;
	GLenum glType = 0;
	switch (colorData->format()) {
	case ColorData::ColorFormat::RGB8:
		glFormat = GL_RGB;
		glType = GL_UNSIGNED_BYTE;
		break;
	case ColorData::ColorFormat::BGR8:
		glFormat = GL_BGR;
		glType = GL_UNSIGNED_BYTE;
		break;
	case ColorData::ColorFormat::BGRA8:
		glFormat = GL_BGRA;
		glType = GL_UNSIGNED_BYTE;
		break;
	case ColorData::ColorFormat::RGBA8:
		glFormat = GL_RGBA;
		glType = GL_UNSIGNED_BYTE;
		break;
	default:
		std::cout << "The color format is unsupported" << std::endl;
		return;
	}
	m_imageStream->setImage(
		width,
		height,
		1,
		glFormat,
		glFormat,
		glType,
		const_cast<unsigned char*>(static_cast<const unsigned char*>(colorData->data())), //Muss man nicht verstehen, da wird nur etwas recht kompliziert gecastet
		osg::Image::AllocationMode::NO_DELETE); //Bild der RealSense wird auf den ImageStream gesetzt
	m_imageStream->dirty();
}

void PointCloudVisualizer::renderVideoTexture(osg::Image* color_image)
{



	//uint8_t* color_image_buffer = k4a_image_get_buffer(*color_image);


	//std::cout << "sizeOf colorImage: " << sizeof(color_image) << std::endl;
	//std::cout << "sizeOf *colorImage: " << sizeof(*color_image) << std::endl;
	//std::cout << "sizeOf color_image_buffer: " << sizeof(color_image_buffer) << std::endl;
	//std::cout << "sizeOf *color_image_buffer: " << sizeof(*color_image_buffer) << std::endl;


	//int width = k4a_image_get_width_pixels(*color_image);
	//int height = k4a_image_get_height_pixels(*color_image);

	//for (unsigned short x = 0; x < width; x++) {
	//	for (unsigned short y = 0; y < height; y++) {
	//		m_PcTexcoords->push_back(osg::Vec2f((float)x*(1.0f / width), (float)y*(1.0f / height)));
	//	}
	//}
	//m_PcTexcoords->dirty();

	//	m_imageStream->allocateImage(width, height, 1, GL_BGRA, GL_UNSIGNED_BYTE);
	//	m_imageStream->setImage(
	//		width,
	//		height,
	//		1,
	//		GL_BGRA,
	//		GL_BGRA,
	//		GL_UNSIGNED_BYTE,
	//		const_cast<unsigned char*>(static_cast<const unsigned char*>(color_image_buffer)), //Muss man nicht verstehen, da wird nur etwas recht kompliziert gecastet
	//		osg::Image::AllocationMode::NO_DELETE); //Bild der RealSense wird auf den ImageStream gesetzt
	//	m_imageStream->dirty();

	//	m_texture->dirtyTextureParameters();
	//	m_texture->dirtyTextureObject();






	/*std::cout << "sizeOf colorImage: " << sizeof(color_image) << std::endl;
	std::cout << "sizeOf *colorImage: " << sizeof(*color_image) << std::endl;*/



	int width = 1280;
	int height = 720;

	//for (unsigned short x = 0; x < width; x++) {
	//	for (unsigned short y = 0; y < height; y++) {
	//		m_PcTexcoords->push_back(osg::Vec2f((float)x*(1.0f / width), (float)y*(1.0f / height)));
	//	}
	//}
	//m_PcTexcoords->dirty();

	m_imageStream->allocateImage(width, height, 1, GL_BGRA, GL_UNSIGNED_BYTE);
	m_imageStream->setImage(
		width,
		height,
		1,
		GL_BGRA,
		GL_BGRA,
		GL_UNSIGNED_BYTE,
		const_cast<unsigned char*>(static_cast<const unsigned char*>(color_image->getDataPointer())), //Muss man nicht verstehen, da wird nur etwas recht kompliziert gecastet
		osg::Image::AllocationMode::NO_DELETE); //Bild der RealSense wird auf den ImageStream gesetzt
	m_imageStream->dirty();

	m_texture->dirtyTextureParameters();
	m_texture->dirtyTextureObject();
}


void PointCloudVisualizer::setVaVecValue(const osg::Vec3& v, bool in) {
	osg::Vec3 w = v - m_bodyScanVolume._min;
	size_t x = (size_t) (w.x() / GRID);
	size_t y = (size_t) (w.y() / GRID);
	size_t z = (size_t) (w.z() / GRID);
	//if (m_PcVaVec->at(x + y * m_bodyScanDimX + z * m_bodyScanDimXY)) {
	//	std::cout << "x: " << x << " y: " << y << " z: " << z << std::endl;
	//}
	m_PcVaVec->set(x + y * m_bodyScanDimX + z * m_bodyScanDimXY, in);
}

std::vector<bool>* PointCloudVisualizer::pointCloudToVaVec(osg::Vec3& boundingBoxCenterInOut, osg::Vec3& boundingBoxExtentsInOut, unsigned int& volumeDimXOut, unsigned int& volumeDimYOut, unsigned int& volumeDimZOut) {
	RoundVector(boundingBoxCenterInOut);
	RoundVector(boundingBoxExtentsInOut);
	osg::Vec3 cellOrigin = RoundVector(boundingBoxCenterInOut - boundingBoxExtentsInOut);
	std::cout << cellOrigin << std::endl;
	volumeDimZOut = (int) (boundingBoxExtentsInOut.z() * 2 / GRID);
	volumeDimYOut = (int) (boundingBoxExtentsInOut.y() * 2 / GRID);
	volumeDimXOut = (int) (boundingBoxExtentsInOut.x() * 2 / GRID);
	std::vector<bool>* vaVec = new std::vector<bool>();
	vaVec->reserve(volumeDimZOut * volumeDimYOut * volumeDimXOut);
	Octree<float>& octree = m_octreeVolume->getOctree();
	int count = 0;
	for (unsigned int z = 0; z < volumeDimZOut; z++) {
		for (unsigned int y = 0; y < volumeDimYOut; y++) {
			osg::Vec3 lastCell;
			for (unsigned int x = 0; x < volumeDimXOut; x++) {
				osg::Vec3 cell = RoundVector(cellOrigin.x() + GRID * x, cellOrigin.y() + GRID * y, cellOrigin.z() + GRID * z);
				vaVec->push_back(octree.hasMaxDepthValue(cell));
				//vaVec->push_back(m_PcVertexMap->find(cell) != m_PcVertexMap->end()); // Only if the cell is contained within the vertexMap, we write true to the vaVec.
				/*bool value = (x % 2 == 0 && y % 2 > 0) || (x % 2 > 0 && y % 2 == 0);
				if (z % 2 > 0) value = !value;
				vaVec->push_back(value);*/
				if (cell == lastCell) {
					std::cout << "Cells are identical" << std::endl;
					count++;
				}
				lastCell = cell;
			}
		}
	}
	std::cout << count << ": Doubles\n";
	return vaVec;
}

void PointCloudVisualizer::pointCloudToVaVecTask(const osg::Vec3& cellOrigin, OptimizedBoolVector* vaVec, const unsigned int start, const unsigned int end, const unsigned int volumeDimX, const unsigned int volumeDimY) {
	size_t volumeDimXY = volumeDimX * volumeDimY;
	//int count = 0;
	Octree<float>& octree = m_octreeVolume->getOctree();
	for (size_t z = start; z < end; z++) {
		size_t zStep = z * volumeDimXY;
		for (size_t y = 0; y < volumeDimY; y++) {
			//osg::Vec3 lastCell;
			size_t yStep = y * volumeDimX;
			for (size_t x = 0; x < volumeDimX; x++) {
				osg::Vec3 cell = RoundVector(cellOrigin.x() + x * GRID, cellOrigin.y() + y * GRID, cellOrigin.z() + z * GRID);
				vaVec->set(x + yStep + zStep, octree.hasMaxDepthValue(cell)); // Only if the vertexMap contains the cell, we write true to the vaVec.
				//vaVec->set(x + yStep + zStep, m_PcVertexMap->find(cell) != m_PcVertexMap->end()); // Only if the vertexMap contains the cell, we write true to the vaVec.
				/*if (cell == lastCell) {
					std::cout << "Cells are identical" << std::endl;
					count++;
				}
				lastCell = cell;*/
			}
		}
	}
	//std::cout << count << ": Doubles\n";
}

OptimizedBoolVector* PointCloudVisualizer::pointCloudToVaVecMultithreaded(osg::Vec3& boundingBoxCenterInOut, osg::Vec3& boundingBoxExtentsInOut, unsigned int& volumeDimXOut, unsigned int& volumeDimYOut, unsigned int& volumeDimZOut) {
	RoundVectorSet(boundingBoxExtentsInOut);
	RoundVectorSet(boundingBoxCenterInOut);
	osg::Vec3 cellOrigin = RoundVector(boundingBoxCenterInOut - boundingBoxExtentsInOut);

	volumeDimZOut = (unsigned int) (boundingBoxExtentsInOut.z() * 2 / GRID);
	volumeDimYOut = (unsigned int) (boundingBoxExtentsInOut.y() * 2 / GRID);
	volumeDimXOut = (unsigned int) (boundingBoxExtentsInOut.x() * 2 / GRID);

	size_t size = volumeDimZOut * volumeDimYOut * volumeDimXOut;
	OptimizedBoolVector* vaVec = new OptimizedBoolVector(size);
	unsigned int threadCount = std::thread::hardware_concurrency();
	std::vector<std::thread> threads;
	threads.reserve(threadCount);

	unsigned int step = ((unsigned int) (vaVec->bytes() / threadCount)) * 8; // 8: byte -> bits, not number of threads
	for (int i = 0; i < threadCount; i++) {
		threads.push_back(std::thread(&PointCloudVisualizer::pointCloudToVaVecTask, this, cellOrigin, vaVec, step * i, std::min<unsigned int>(step * (i + 1), volumeDimZOut), volumeDimXOut, volumeDimYOut));
	}
	for (int i = 0; i < threadCount; i++) {
		threads[i].join();
	}
	return vaVec;
}

void PointCloudVisualizer::polygonizePointCloudMultithreaded(const osg::Vec3& boundingBoxCenter, const osg::Vec3& boundingBoxExtents) {
	BoolMarchingCubesProcessor mcProcessor = BoolMarchingCubesProcessor(m_configReader, nullptr);
	unsigned int volumeDimZ, volumeDimY, volumeDimX;

	TimeWriter timeWriter;
	timeWriter.startMetering("pointCloudToVaVec", true, false);

	osg::Vec3 center(boundingBoxCenter), extents(boundingBoxExtents); //pointCloudToVaVec rounds the vectors to the grid. We want to use those values later on instead of the input parameters.
	OptimizedBoolVector* vaVec = pointCloudToVaVecMultithreaded(center, extents, volumeDimX, volumeDimY, volumeDimZ);
	timeWriter.endMetering();

	mcProcessor.setVolumeDimensions(volumeDimX, volumeDimY, volumeDimZ);
	const float stepSize = 1000.0f;
	const float displayStepSize = GRID;
	timeWriter.startMetering("polygonize", true, false);
	mcProcessor.setUnifiedStepSize(stepSize);
	mcProcessor.polygonize(vaVec);
	timeWriter.endMetering();

	//Display the generated mesh, but clear the point cloud first
	//clearPointCloudFull();
	osg::ref_ptr<osg::PositionAttitudeTransform> meshMatTrans = new osg::PositionAttitudeTransform();
	float scale = displayStepSize / stepSize;
	meshMatTrans->setPosition(center - extents + (osg::Vec3(0.5f, 0.5f, 0.5f) * displayStepSize));
	meshMatTrans->setScale(osg::Vec3(scale, scale, scale));

	//TODO: Maybe store the generated geode pointer somewhere for easier access?
	osg::Geode* meshGeode = new osg::Geode();

	meshGeode->getOrCreateStateSet()->setMode(GL_RESCALE_NORMAL, osg::StateAttribute::ON);
	meshGeode->addDrawable(mcProcessor.getOpenMeshGeometry());
	meshGeode->dirtyBound();

	meshMatTrans->addChild(meshGeode);
	m_matTrans->addChild(meshMatTrans);

	//Free the memory of the vaVec since it is no member and no longer needed after Marching Cubes has finished.
	delete(vaVec);
}

/*void PointCloudVisualizer::polygonizePointCloud(const osg::Vec3& boundingBoxCenter, const osg::Vec3& boundingBoxExtents) {
	BoolMarchingCubesProcessor mcProcessor = BoolMarchingCubesProcessor(m_configReader, nullptr);
	unsigned int volumeDimZ, volumeDimY, volumeDimX;

	TimeWriter timeWriter;
	timeWriter.startMetering("pointCloudToVaVec", true, false);

	osg::Vec3 center(boundingBoxCenter), extents(boundingBoxExtents); //pointCloudToVaVec rounds the vectors to the grid. We want to use those values later on instead of the input parameters.
	std::vector<bool>* vaVec = pointCloudToVaVec(center, extents, volumeDimX, volumeDimY, volumeDimZ);
	timeWriter.endMetering();

	mcProcessor.setVolumeDimensions(volumeDimX, volumeDimY, volumeDimZ);
	const float stepSize = GRID;
	const float displayStepSize = GRID;
	timeWriter.startMetering("polygonize", true, false);
	mcProcessor.setUnifiedStepSize(stepSize);
	mcProcessor.polygonize(vaVec);
	timeWriter.endMetering();

	//Display the generated mesh, but clear the point cloud first
	//clearPointCloudFull();
	osg::ref_ptr<osg::PositionAttitudeTransform> meshMatTrans = new osg::PositionAttitudeTransform();
	const float scale = displayStepSize / stepSize;
	meshMatTrans->setPosition(center - extents + (osg::Vec3(0.5f, 0.5f, 0.5f) * displayStepSize));
	meshMatTrans->setScale(osg::Vec3(scale, scale, scale));

	//TODO: Maybe store the generated geode pointer somewhere for easier access?
	osg::Geode* meshGeode = new osg::Geode();

	meshGeode->getOrCreateStateSet()->setMode(GL_RESCALE_NORMAL, osg::StateAttribute::ON);
	meshGeode->addDrawable(mcProcessor.getOpenMeshGeometry());
	meshGeode->dirtyBound();

	meshMatTrans->addChild(meshGeode);
	m_matTrans->addChild(meshMatTrans);

	//Free the memory of the vaVec since it is no member and no longer needed after Marching Cubes has finished.
	delete(vaVec);
}*/

void PointCloudVisualizer::polygonizePointCloud(OptimizedBoolVector* vaVec, const osg::Vec3& boundingBoxMin, const unsigned int volumeDimX, const unsigned int volumeDimY, const unsigned int volumeDimZ) {
	BoolMarchingCubesProcessor mcProcessor = BoolMarchingCubesProcessor(m_configReader, nullptr);
	TimeWriter timeWriter;
	timeWriter.startMetering("filterVaVec", true, false);
	filterVaVec(vaVec, volumeDimX, volumeDimY, volumeDimZ);
	timeWriter.endMetering();

	timeWriter.startMetering("polygonize", true, false);
	mcProcessor.setVolumeDimensions(volumeDimX, volumeDimY, volumeDimZ);
	const float stepSize = 10;
	const float displayStepSize = GRID;
	mcProcessor.setUnifiedStepSize(stepSize);
	mcProcessor.polygonizeCUDA(vaVec);
	timeWriter.endMetering();

	//Display the generated mesh, but clear the point cloud first
	//clearPointCloudFull();
	osg::ref_ptr<osg::PositionAttitudeTransform> meshMatTrans = new osg::PositionAttitudeTransform();
	const float scale = displayStepSize / stepSize;
	meshMatTrans->setPosition(boundingBoxMin);
	meshMatTrans->setScale(osg::Vec3(scale, scale, scale));

	//TODO: Maybe store the geode pointer somewhere for easier access?
	osg::Geode* meshGeode = new osg::Geode();

	meshGeode->getOrCreateStateSet()->setMode(GL_RESCALE_NORMAL, osg::StateAttribute::ON);
	meshGeode->addDrawable(mcProcessor.getOpenMeshGeometry());
	meshGeode->dirtyBound();

	meshMatTrans->addChild(meshGeode);
	m_matTrans->addChild(meshMatTrans);
}

void PointCloudVisualizer::polygonizePointCloudFromOctree() {
	BoolMarchingCubesProcessor mcProcessor(m_configReader, nullptr);
	TimeWriter timeWriter;

	timeWriter.startMetering("polygonizeOctree", true, false);
	//OptimizedBoolVector* vaVec = m_octreeVolume->toVaVec();
	//std::vector<std::pair<float, unsigned char>> * vaVec = m_octreeVolume->toVaVecFloat();

	size_t s = (1ULL << m_octreeVolume->getOctree().getMaxDepth()) + 1;
	mcProcessor.setVolumeDimensions(s, s, s);
	const float stepSize = m_octreeVolume->getSize() / (1ULL << m_octreeVolume->getOctree().getMaxDepth());
	std::cout << stepSize << std::endl;
	const float displayStepSize = 1.0f;
	mcProcessor.setUnifiedStepSize(stepSize);
	//mcProcessor.polygonizeFloatCUDA(vaVec, m_octreeVolume->getOctree().getMaxDepth());
	mcProcessor.polygonizeFloatCUDAHierarchical(m_octreeVolume, m_enableTextureProcessing, m_textureProcessingOutputResolution);
	timeWriter.endMetering();

	//Display the generated mesh, but clear the point cloud first
	//clearPointCloudFull();
	osg::ref_ptr<osg::PositionAttitudeTransform> meshMatTrans = new osg::PositionAttitudeTransform();
	//const float scale = 1.0f;
	//float e = m_octreeVolume->getExtents();
	//meshMatTrans->setPosition(m_octreeVolume->getPosition() - osg::Vec3f(e, e, e));
	//meshMatTrans->setScale(osg::Vec3(scale, scale, scale));

	//UtilProcessor utilProcessor = UtilProcessor();
	//utilProcessor.setActiveGeometry(mcProcessor.getOpenMeshGeometry());
	//utilProcessor.calcNormals();
	if (!mcProcessor.getOpenMeshGeometry()) return;
	m_polygonizedPointCloud = mcProcessor.getOpenMeshGeometry();
	m_MCmeshGeode->addDrawable(m_polygonizedPointCloud);

	m_MCmeshGeode->dirtyBound();

	meshMatTrans->addChild(m_MCmeshGeode);
	m_matTrans->addChild(meshMatTrans);

	//auto omg = mcProcessor.getOpenMeshGeometry();
	//osg::Vec4Array* colorArray = new osg::Vec4Array();
	//colorArray->push_back(osg::Vec4f(0.5f, 0.5f, 0.5f, 1.0f));
	//colorArray->setBinding(osg::Array::BIND_OVERALL);
	//omg->setColorArray(colorArray);

	//osg::Vec3Array *normals = dynamic_cast<osg::Vec3Array*>(omg->getNormalArray());
	//osg::Vec3Array *vertices = dynamic_cast<osg::Vec3Array*>(omg->getVertexArray());
	//osg::Matrix geomMatrix = osg::computeLocalToWorld(m_MCmeshGeode->getParentalNodePaths()[0]);
	//for (int i = 0; i < normals->getNumElements(); i++) {
	//	normals->at(i) = -normals->at(i);
	//	debugDrawLine(vertices->at(i) * geomMatrix, (vertices->at(i) + normals->at(i) * 0.01) * geomMatrix, osg::Vec4(0, 0.5f, 0.5f, 1), osg::Vec4(0, 0.5f, 0.5f, 1));
	//}
	//normals->dirty();
}

void PointCloudVisualizer::polygonizePointCloudForBodyScanning() {
	polygonizePointCloud(m_PcVaVec, m_bodyScanOrigin, m_bodyScanDimX, m_bodyScanDimY, m_bodyScanDimZ);
}

// Filter mechanisms working directly with a vaVec. This way accessing the individual cells of the volume should be way faster than checking every cell with the vertexMap.
void PointCloudVisualizer::filterVaVec(OptimizedBoolVector* vaVec, const size_t volumeDimX, const size_t volumeDimY, const size_t volumeDimZ) {
	//vaVecProximityFilter(vaVec, volumeDimX, volumeDimY, volumeDimZ, 1, 1, 4u);
	//vaVecFillVolume(vaVec, volumeDimX, volumeDimY, volumeDimZ);
	vaVecClearBorders(vaVec, volumeDimX, volumeDimY, volumeDimZ);
	OptimizedBoolVector* vaVecTemp = new OptimizedBoolVector(vaVec->bits());
	for (int i = 0; i < 3; i++) {
		memcpy(vaVecTemp->getBegin(), vaVec->getBegin(), vaVec->bytes());
		vaVecMicroHoleFilling(vaVec, vaVecTemp, volumeDimX, volumeDimY, volumeDimZ, i != 0);
	}
	//vaVecFillVolume(vaVec, volumeDimX, volumeDimY, volumeDimZ);
	//memcpy(vaVec->getBegin(), vaVecTemp->getBegin(), vaVec->bytes());
	delete vaVecTemp;
}

void PointCloudVisualizer::vaVecClearBorders(OptimizedBoolVector* vaVec, const size_t volumeDimX, const size_t volumeDimY, const size_t volumeDimZ) {
	size_t xyz[3] = {0, 0, 0};
	size_t volumeDim[3] = {volumeDimX, volumeDimY, volumeDimZ};
	for (int i = 0; i < 3; i++) {
		size_t& volumeDimW = volumeDim[(i + 2) % 3];
		size_t& volumeDimV = volumeDim[(i + 1) % 3];
		size_t& volumeDimU = volumeDim[i];
		for (size_t& w = xyz[(i + 2) % 3] = 0; w < volumeDimW; w += volumeDimW - 1) {
			for (size_t& v = xyz[(i + 1) % 3] = 0; v < volumeDimV; v++) {
				for (size_t& u = xyz[i] = 0; u < volumeDimU; u++) {
					vaVec->set(xyz[0] + volumeDimX * xyz[1] + volumeDimX * volumeDimY * xyz[2], false);
				}
			}
		}
	}
}

void PointCloudVisualizer::vaVecMicroHoleFilling(OptimizedBoolVector* vaVec, OptimizedBoolVector* vaVecTemp, const size_t volumeDimX, const size_t volumeDimY, const size_t volumeDimZ, const bool removeOutlier) {
	size_t volumeDimXY = volumeDimX * volumeDimY;
	const size_t indexInc[3] = {1, volumeDimX, volumeDimXY};
	for (size_t z = 1; z < volumeDimZ - 1; z++) {
		for (size_t y = 1; y < volumeDimY - 1; y++) {
			for (size_t x = 1; x < volumeDimX - 1; x++) {
				size_t index = x + y * volumeDimX + z * volumeDimXY;
				char neighbours = 0;
				if (!vaVecTemp->at(index)) {
					for (int i = 0; i < 3; i++) {
						for (int j = -1; j < 2; j += 2) {
							if (vaVecTemp->at(index + indexInc[i] * j)) neighbours++;
							if (neighbours > 1) {
								vaVec->set(index, true);
								break;
							}
						}
					}
				} 				else if (removeOutlier) {
					for (int i = 0; i < 3; i++) {
						if (vaVecTemp->at(index + indexInc[i])) neighbours++;
						if (vaVecTemp->at(index - indexInc[i])) neighbours++;
					}
					if (neighbours <= 1) vaVec->set(index, false);
				}
			}
		}
	}
}

/*
void PointCloudVisualizer::vaVecOutlierRemoval(OptimizedBoolVector* vaVec, OptimizedBoolVector* vaVecTemp, const size_t volumeDimX, const size_t volumeDimY, const size_t volumeDimZ) {
	size_t volumeDimXY = volumeDimX * volumeDimY;
	const size_t indexInc[3] = {1, volumeDimX, volumeDimXY};
	for (size_t z = 1; z < volumeDimZ - 1; z++) {
		for (size_t y = 1; y < volumeDimY - 1; y++) {
			for (size_t x = 1; x < volumeDimX - 1; x++) {
				size_t index = x + y * volumeDimX + z * volumeDimXY;
				for (int i = 0; i < 3; i++) {
					size_t linkedIndex = index + indexInc[i];
					if (vaVec->at(linkedIndex)) {
						vaVecFindCompound();
					}
				}
			}
		}
	}
}

inline void PointCloudVisualizer::vaVecFindCompound(OptimizedBoolVector* vaVec, size_t index, const size_t indexInc[]) {

	for (int i = 0; i < 3; i++) {

	}
}*/

void PointCloudVisualizer::vaVecProximityFilter(OptimizedBoolVector* vaVec, const size_t volumeDimX, const size_t volumeDimY, const size_t volumeDimZ,
	const int kernel, const unsigned int iterations, const float proximityThreshold)
{
	vaVecProximityFilter(vaVec, volumeDimX, volumeDimY, volumeDimZ, kernel, iterations, (unsigned int) (powf(1 + kernel * 2, 3) * proximityThreshold));
}

void PointCloudVisualizer::vaVecFillVolume(OptimizedBoolVector* vaVec, const size_t volumeDimX, const size_t volumeDimY, const size_t volumeDimZ) {
	OptimizedBoolVector* vaVecResultZYX = new OptimizedBoolVector(vaVec->bits());
	OptimizedBoolVector* vaVecResultYXZ = new OptimizedBoolVector(vaVec->bits());
	OptimizedBoolVector* vaVecResultXZY = new OptimizedBoolVector(vaVec->bits());

	const size_t volumeDimXY = volumeDimX * volumeDimY;

	//x Scanline 
	size_t x0 = 0, y0 = 0, z0 = 0;
	std::thread threadZYX(&PointCloudVisualizer::vaVecVolumeScanlineTask, this, vaVec, vaVecResultZYX,
		ScanlineOrder::zyx,
		volumeDimX, volumeDimY, volumeDimZ,
		3u
	);

	//z Scanline 
	size_t x1 = 0, y1 = 0, z1 = 0;
	std::thread threadYXZ(&PointCloudVisualizer::vaVecVolumeScanlineTask, this, vaVec, vaVecResultYXZ,
		ScanlineOrder::yxz,
		volumeDimZ, volumeDimY, volumeDimX,
		3u
	);
	//y Scanline 
	size_t x2 = 0, y2 = 0, z2 = 0;
	std::thread threadXZY(&PointCloudVisualizer::vaVecVolumeScanlineTask, this, vaVec, vaVecResultXZY,
		ScanlineOrder::xzy,
		volumeDimY, volumeDimZ, volumeDimX,
		3u
	);

	threadZYX.join();
	threadYXZ.join();
	threadXZY.join();

	//Analyze and join scanline results
	for (size_t byte = 0; byte < vaVec->bytes(); byte++) {
		for (unsigned char bit = 0; bit < 8; bit++) {
			unsigned char value = vaVecResultZYX->at(byte, bit) + vaVecResultYXZ->at(byte, bit) + vaVecResultXZY->at(byte, bit);
			vaVec->set(byte, bit, value >= 2);
		}
	}

	delete vaVecResultZYX;
	delete vaVecResultYXZ;
	delete vaVecResultXZY;
}

void PointCloudVisualizer::vaVecVolumeScanlineTask(const OptimizedBoolVector* vaVec, OptimizedBoolVector* vaVecResult,
	ScanlineOrder order,
	const size_t volumeDimX, const size_t volumeDimY, const size_t volumeDimZ,
	const unsigned int surfaceThicknessThreshold)
{
	//Access initialization 
	size_t u, v, w, volumeDimU, volumeDimV, volumeDimW;
	size_t* xPtr, * yPtr, * zPtr;
	if (order == ScanlineOrder::zyx) {
		zPtr = &w;
		yPtr = &v;
		xPtr = &u;
		volumeDimW = volumeDimZ;
		volumeDimV = volumeDimY;
		volumeDimU = volumeDimX;
	} 	else if (order == ScanlineOrder::xzy) {
		xPtr = &w;
		zPtr = &v;
		yPtr = &u;
		volumeDimW = volumeDimX;
		volumeDimV = volumeDimZ;
		volumeDimU = volumeDimY;
	} 	else { //yzx
		yPtr = &w;
		zPtr = &v;
		xPtr = &u;
		volumeDimW = volumeDimY;
		volumeDimV = volumeDimZ;
		volumeDimU = volumeDimX;
	}
	size_t& x = *xPtr;
	size_t& y = *yPtr;
	size_t& z = *zPtr;

	size_t volumeDimXY = volumeDimX * volumeDimY;
	for (w = 0; w < volumeDimW; w++) {
		for (v = 0; v < volumeDimV; v++) {
			bool in = false;
			size_t lastEnter = 0;
			size_t lastCollision = 0;
			size_t lastExit = 0;
			for (u = 0; u < volumeDimU; u++) {
				size_t index = x + y * volumeDimX + z * volumeDimXY;
				bool collision = vaVec->at(index);
				if (collision) vaVecResult->set(index, true);
				//Currently Inside
				if (in) {
					if (collision) {
						lastExit = index;
						lastCollision = index;
						in = false;
					} 					else {
						vaVecResult->set(index, true);
					}
				}
				//Currently Outside
				else if (collision) {
					lastEnter = index;
					lastCollision = index;
					in = true;
				}
			}
		}
	}
	std::cout << "ScanlineTask done\n";
}

void PointCloudVisualizer::vaVecProximityFilter(OptimizedBoolVector* vaVec, const size_t volumeDimX, const size_t volumeDimY, const size_t volumeDimZ,
	const int kernel, const unsigned int iterations, const unsigned int neighbourThreshold)
{
	size_t volumeDimXY = volumeDimX * volumeDimY;
	for (int i = 0; i < iterations; i++) {
		for (size_t z = kernel; z < volumeDimZ - kernel; z++) {
			size_t zStep = z * volumeDimXY;
			for (size_t y = kernel; y < volumeDimY - kernel; y++) {
				size_t yStep = y * volumeDimX;
				for (size_t x = kernel; x < volumeDimX - kernel; x++) {
					size_t index = x + yStep + zStep;
					unsigned int neighbourCount = 0;
					for (int u = -kernel; u <= kernel; u++) {
						for (int v = -kernel; v <= kernel; v++) {
							for (int w = -kernel; w <= kernel; w++) {
								if (vaVec->at(x + w + (y + v) * volumeDimX + (z + u) * volumeDimXY)) neighbourCount++;
								if (neighbourCount > neighbourThreshold) break;
							}
							if (neighbourCount > neighbourThreshold) break;
						}
						if (neighbourCount > neighbourThreshold) break;
					}
					vaVec->set(index, neighbourCount > neighbourThreshold);
				}
			}
		}
	}
}

// First implementation fo filtering mechanisms on the pointCloud using the vertexMap
void PointCloudVisualizer::filterRSPointCloud(unsigned int kernel, const unsigned int linkThreshold) {
	if (kernel % 2 == 0) kernel += 1; //The kernel has to be an odd number for the current point to be the center
	float radius = kernel * GRID;

	std::queue<unsigned int> q = std::queue<unsigned int>();
	std::unordered_set<unsigned int> pending = std::unordered_set<unsigned int>();
	std::unordered_set<unsigned int> visited = std::unordered_set<unsigned int>();

	std::vector<std::vector<unsigned int>*> compounds = std::vector<std::vector<unsigned int>*>();
	int largestCompound = -1;

	std::vector<unsigned int> links = std::vector<unsigned int>();

	Octree<float>& octree = m_octreeVolume->getOctree();

	int start = 0;
	do {
		bool foundStart = false;
		for (int i = start; i < m_PcVertices->size(); i++) {
			if (visited.count(i) != 0) {
				q.push(i);
				start = i + 1;
				foundStart = true;
				break;
			}
		}
		if (!foundStart) break;
		std::vector<unsigned int>* compound = new std::vector<unsigned int>();
		compounds.push_back(compound);
		while (!q.empty()) {
			links.clear();
			unsigned int current = q.front();
			osg::Vec3 currentPoint = (*m_PcVertices)[current];
			q.pop();
			visited.insert(current);
			pending.erase(current);

			//Find all links within range
			for (float x = currentPoint.x() - radius; x <= currentPoint.x() + radius; x += GRID) {
				for (float y = currentPoint.y() - radius; y <= currentPoint.y() + radius; y += GRID) {
					for (float z = currentPoint.z() - radius; z <= currentPoint.z() + radius; z += GRID) {
						if (x == 0 && y == 0 && z == 0) continue;
						//auto kv = m_PcVertexMap->find(osg::Vec3(x, y, z));
						//if (kv != m_PcVertexMap->end()) links.push_back(kv->second);
						//TODO: Implement for Octree
					}
				}
			}
			//Check link count against linkThreshold
			if (links.size() > linkThreshold) { //Accept point as part of the compound
				compound->push_back(current);
				for each (unsigned int link in links) {
					if (visited.count(link) == 0 && pending.count(link) == 0) {
						pending.insert(link);
						q.push(link);
					}
				}
			}
		}
		if (largestCompound == -1 || compound->size() >= compounds[largestCompound]->size()) largestCompound = compounds.size() - 1;
	} while (compounds[largestCompound]->size() >= m_PcVertices->size() - visited.size());

	//Test
	for each (unsigned int index in *compounds[largestCompound]) {
		(*m_PcColorArray)[index] = osg::Vec4(1, 0, 0, 1);
	}
	m_PcColorArray->dirty();

	//CleanUp
	for each (auto compound in compounds) delete(compound);
}

void PointCloudVisualizer::detectBoundingBox(osg::Vec3& origin, osg::Vec3& extents, osg::Vec3& size, osg::Vec3& min, osg::Vec3& max) {
	min = osg::Vec3(FLT_MAX, FLT_MAX, FLT_MAX);
	max = osg::Vec3(-FLT_MAX, -FLT_MAX, -FLT_MAX);
	for each (osg::Vec3 point in *m_PcVertices) {
		if (point.x() > max.x()) max.x() = point.x();
		else if (point.x() < min.x()) min.x() = point.x();

		if (point.y() > max.y()) max.y() = point.y();
		else if (point.y() < min.y()) min.y() = point.y();

		if (point.z() > max.z()) max.z() = point.z();
		else if (point.z() < min.z()) min.z() = point.z();
	}
	origin = osg::Vec3((max - min) * 0.5f);
	extents = max - origin;
	size = max - min;
}

void PointCloudVisualizer::detectVolume() {
	osg::Vec3 bbOrigin, bbExtents, bbSize, bbMin, bbMax;
	detectBoundingBox(bbOrigin, bbExtents, bbSize, bbMin, bbMax);
	std::vector<char>* vaVec = new std::vector<char>();
	unsigned int sizeX = std::ceilf(bbSize.x() / GRID);
	unsigned int sizeY = std::ceilf(bbSize.y() / GRID);
	unsigned int sizeZ = std::ceilf(bbSize.z() / GRID);
	vaVec->resize(sizeX * sizeY * sizeZ);


	volumeScanline(ScanlineOrder::zyx, bbMin, sizeX, sizeY, sizeZ, vaVec);
	//volumeScanline(ScanlineOrder::yxz, bbMin, sizeX, sizeY, sizeZ, vaVec);
	//volumeScanline(ScanlineOrder::xzy, bbMin, sizeX, sizeY, sizeZ, vaVec);
}

void PointCloudVisualizer::volumeScanline(ScanlineOrder order, const osg::Vec3& min, const unsigned int uSize, const unsigned int vSize, const unsigned int wSize, std::vector<char>* vaVec) {
	std::cout << "Scanline" << order << "was started." << std::endl;
	unsigned int xSize, ySize, uFrom, uTo, vFrom, vTo, wFrom, wTo;
	switch (order) {
	case xzy:
		xSize = uSize;
		ySize = wSize;
		break;
	case yxz:
		xSize = vSize;
		ySize = uSize;
		break;
	default: //zyx
		xSize = wSize;
		ySize = vSize;
		break;
	}

	unsigned int threadCount = std::thread::hardware_concurrency();
	std::vector<std::thread> threads = std::vector<std::thread>(threadCount);
	std::vector<std::vector<char>*> vaVecs = std::vector<std::vector<char>*>(threadCount);

	unsigned int uPart = (int) (uSize / threadCount);
	unsigned int current;
	unsigned int next = 0;
	for (int i = 0; i < threadCount; i++) {
		current = next;
		next = i < threadCount - 1 ? current + uPart : uSize;
		std::vector<char>* partialVaVec = new std::vector<char>((next - current) * vSize * wSize);
		vaVecs.push_back(partialVaVec);
		//std::thread thread(&PointCloudVisualizer::volumeScanlinePart, this, order, min, xSize, ySize, current, next, 0, vSize, wSize, partialVaVec);
		//threads.push_back(thread);
		std::cout << "Thread" << i << "was started." << std::endl;
	}

	current = 0;
	for (int i = 0; i < threadCount; i++) {
		threads[i].join();
		std::cout << "Thread" << i << "is done." << std::endl;
		std::vector<char>* partialVaVec = vaVecs[i];
		for (unsigned int j = 0; j < partialVaVec->size(); j++) {
			unsigned int k = j + current;
			//if (k < vaVec->size()) (*vaVec)[k] += (*partialVaVec)[j]; //A value was already written to this index. So, add the new data to the current element
			//else vaVec->push_back((*partialVaVec)[j]); //No value exists. Thus, we are writing to this index for the first time.
		}
		current += partialVaVec->size();
	}
	std::cout << "Scanline" << order << "is done." << std::endl;
}

void PointCloudVisualizer::volumeScanlinePart(ScanlineOrder order, const osg::Vec3& min, const unsigned int xSize, const unsigned int ySize,
	const unsigned int uFrom, const unsigned int uTo, const unsigned int vFrom, const unsigned int vTo, const unsigned int wTo, std::vector<char>* vaVec)
{
	unsigned int u, v, w; //used in the for-loops
	unsigned int* x, * y, * z; //allowing access of correct x,y,z-value of the current cell
	osg::Vec3 scanDir;
	switch (order) {
	case xzy:
		x = &u;
		y = &w;
		z = &v;
		scanDir = osg::Vec3(0.0f, 1.0f, 0.0f);
		break;
	case yxz:
		x = &v;
		y = &u;
		z = &w;
		scanDir = osg::Vec3(0.0f, 0.0f, 1.0f);
		break;
	default: //zyx
		x = &w;
		y = &v;
		z = &u;
		scanDir = osg::Vec3(1.0f, 0.0f, 0.0f);
		break;
	}

	for (u = uFrom; u < uTo; u++) {
		for (v = vFrom; v < vTo; v++) {
			char state = -1; //0: FAULT, 1: IN, -1: OUT
			int stateCell = 0; //last cell initiating a change of state
			for (w = 0; w < wTo; w++) {
				unsigned int cellIndex = xSize * ySize * *z + ySize * *y + *x;
				osg::Vec3 cellPoint = min + RoundVector(*x * GRID, *y * GRID, *z * GRID);

				//TODO: Implement for Octree
				/*auto kv = m_PcVertexMap->find(cellPoint);
				if (kv != m_PcVertexMap->end()) vaVec->push_back(1);

				if (kv == m_PcVertexMap->end()) {
					//cellPoint is not in m_PcVertexMap
					if (state != 0) (*vaVec)[cellIndex] += state;
					//If the state is still IN when reaching the last cell in the row while it does not contain a point, the scanline is assumed to be corrupt.
					//All cells after the stateCell are getting a FAULT-mark.
				}
				else {
					osg::Vec3 n = (*m_PcNormals)[kv->second];
					float angle = std::acosf(scanDir * n);
					(*vaVec)[cellIndex] += 1; //If the cell contains a point, then mark the cell as IN independent of what the normal indicates.

					if (state == -1) angle = 180.0f - angle;
					if (angle < 90) {
						if (state == 1) {

						}
						state = -state;
						stateCell = cellIndex;
					}
				}
				*/
			}
		}
	}
}

void PointCloudVisualizer::generatePointCloudFromImageData(const std::vector<ImageData*>* imageData) {
	if (imageData == nullptr) return;
	clearPointCloudFull();

	for (ImageData* img : *imageData) {
		cv::Mat colorData = cv::imdecode(cv::Mat(1, img->_colorBuffer->size(), CV_8UC1, img->_colorBuffer->data()), cv::ImreadModes::IMREAD_UNCHANGED);
		cv::Mat depthData = cv::imdecode(cv::Mat(1, img->_depthBuffer->size(), CV_8UC1, img->_depthBuffer->data()), cv::ImreadModes::IMREAD_UNCHANGED);

		for (unsigned int v = 0u; v < img->_height; ++v) {
			for (unsigned int u = 0u; u < img->_width; ++u) {
				// Image data
				osg::Vec3ub colorVec = colorData.at<osg::Vec3ub>(v, u);
				osg::Vec4 color(colorVec.b() / 255.f, colorVec.g() / 255.f, colorVec.r() / 255.f, 1);

				//osg::Vec3ub depthVec = depthData.at<osg::Vec3ub>(v, u);
				//float depth = (float) (((unsigned int) depthVec.r()) + (((unsigned int) depthVec.g()) << 8));
				float depth = (float) (*((unsigned short*) (depthData.data + 3u * (v * img->_width + u))));
				//float depth = (float) depthData.at<unsigned short>(v, u);
				depth *= img->_depthScale;
				if (depth <= 0.3) continue;
				
				// Image space to camera coordinates
				osg::Vec3f p_cam((u - img->_imageCenter.x()) / (img->_imageSamplePoint.x() - img->_imageCenter.x()), (v - img->_imageCenter.y()) / (img->_imageSamplePoint.y() - img->_imageCenter.y()), 1);
				//p_cam.normalize();
				p_cam *= depth;

				// Camera coordinates to global coordinates
				osg::Vec3f p = p_cam * img->_cameraTransform;

				m_PcIndicies->push_back(m_PcVertices->size());
				m_PcVertices->push_back(p);
				m_PcColorArray->push_back(color);
			}
		}
	}
	m_PcVertices->dirty();
	m_PcColorArray->dirty();
	std::cout << m_PcVertices->size() << std::endl;
}

cv::Mat* PointCloudVisualizer::generateTextureFromImageData(osg::Geometry* geom, const std::vector<ImageData*>* imageData, int texSize, int margin) {
	if (imageData == nullptr) return nullptr;

	cv::Mat texture = cv::Mat(texSize, texSize, CV_32FC3);
	float* data = (float*) texture.data;
	float* weights = new float[texSize * texSize]{0};

	osg::Vec3Array* vertices = (osg::Vec3Array*) geom->getVertexArray();
	osg::Vec3Array* normals = (osg::Vec3Array*) geom->getNormalArray();
	osg::Vec2Array* uvCoords = (osg::Vec2Array*) geom->getTexCoordArray(0);
	osg::DrawElementsUInt* triangles = (osg::DrawElementsUInt*) geom->getPrimitiveSet(0);

	unsigned int triCount = triangles->size() / 3;
	unsigned int quadsPerRow = ceil(sqrt(ceil(.5f * triCount)));
	unsigned int edgeLength = texSize / quadsPerRow;

	int count = 0;

	const unsigned int sqrTexSize = texSize * texSize;

	unsigned int threadCount = std::min(std::thread::hardware_concurrency(), sqrTexSize);
	std::vector<std::future<void>> futures;
	futures.reserve(threadCount - 1);
	unsigned int slice = sqrTexSize / threadCount;

	for (ImageData* img : *imageData) {
		cv::Mat colorData = cv::imdecode(cv::Mat(1, img->_colorBuffer->size(), CV_8UC1, img->_colorBuffer->data()), cv::ImreadModes::IMREAD_UNCHANGED);
		cv::Mat depthData = cv::imdecode(cv::Mat(1, img->_depthBuffer->size(), CV_8UC1, img->_depthBuffer->data()), cv::ImreadModes::IMREAD_UNCHANGED);

		// future CUDA implementation
		futures.clear();
		unsigned int start = 0;
		for (unsigned int t = 0; t < futures.size(); t++) {
			futures.push_back(std::async(std::launch::async, &PointCloudVisualizer::generatePartialTextureFromImageData, this, start, slice, data, weights, img, &colorData, &depthData, texSize, quadsPerRow, edgeLength, vertices, normals, uvCoords, triangles));
			start += slice;
		}
		generatePartialTextureFromImageData(start, sqrTexSize - start, data, weights, img, &colorData, &depthData, texSize, quadsPerRow, edgeLength, vertices, normals, uvCoords, triangles);
		for (std::future<void>& future : futures) {
			if (future.valid()) future.get();
		}
	}
	//std::cout << std::to_string(count) << std::endl;

	for (int i = 0; i < 3 * texSize * texSize; i++) {
		if (weights[i / 3] != 0) data[i] = data[i] / weights[i / 3];
	}
	delete[] weights;
	cv::Mat* textureOut = new cv::Mat(texSize, texSize, CV_8UC3);
	texture.convertTo(*textureOut, CV_8UC3);
	return textureOut;
}

void PointCloudVisualizer::generatePartialTextureFromImageData(const unsigned int start, const unsigned int slice, float* data, float* weights, ImageData* img, cv::Mat* colorData, cv::Mat* depthData, const unsigned int texSize, const unsigned int quadsPerRow, const unsigned int edgeLength, const osg::Vec3Array* vertices, const osg::Vec3Array* normals, const osg::Vec2Array* uvCoords, const osg::DrawElementsUInt* triangles) {
	//for (unsigned int i = 0; i < texSize * texSize; ++i) {
	for (unsigned int i = start; i < start + slice; ++i) {
		int u = i % texSize;
		int v = (texSize * texSize - i) / texSize;

		if (u > quadsPerRow * edgeLength || v > quadsPerRow * edgeLength) continue;

		int triIndex = 2 * ((v / edgeLength) * quadsPerRow + (u / edgeLength));
		if ((v % edgeLength) >= edgeLength - (u % edgeLength)) ++triIndex;

		if (3 * triIndex >= triangles->size()) continue;

		// uv coordinate weights
		osg::Vec2f a = uvCoords->at(triangles->at(3 * triIndex));
		osg::Vec2f b = uvCoords->at(triangles->at(3 * triIndex + 1));
		osg::Vec2f c = uvCoords->at(triangles->at(3 * triIndex + 2));

		osg::Vec2f r = osg::Vec2f(((float) u + .5f) / texSize, ((float) v + .5f) / texSize);

		float alpha = ((c.x() - b.x()) * (r.y() - a.y()) - (c.y() - b.y()) * (r.x() - a.x())) / ((c.x() - b.x()) * (b.y() - a.y()) - (c.y() - b.y()) * (b.x() - a.x()));
		float beta = ((b.x() - a.x()) * (r.y() - a.y()) - (b.y() - a.y()) * (r.x() - a.x())) / ((r.x() - a.x()) * (c.y() - b.y()) - (r.y() - a.y()) * (c.x() - b.x()));

		alpha = std::fmax(std::fmin(alpha, 1), 0);
		beta = std::fmax(std::fmin(beta, 1), 0);

		//data[3 * i] += 255.f * alpha;
		//data[3 * i + 1] += 0;
		//data[3 * i + 2] += 255.f * beta;

		// calculate 3d position & normal
		osg::Vec3f p = vertices->at(triangles->at(3 * triIndex)) * (1 - alpha) + (vertices->at(triangles->at(3 * triIndex + 1)) * (1 - beta) + vertices->at(triangles->at(3 * triIndex + 2)) * beta) * alpha;
		osg::Vec3f n = normals->at(triangles->at(3 * triIndex)) * (1 - alpha) + (normals->at(triangles->at(3 * triIndex + 1)) * (1 - beta) + normals->at(triangles->at(3 * triIndex + 2)) * beta) * alpha;

		// calculate weight via dot product of camera view direction in global space & normal
		osg::Vec3f p_to_cam = img->_cameraTransform.getTrans() - p;
		float weight = (p_to_cam * n) / p_to_cam.length();
		if (weight <= 0.f) continue;

		// transform to camera coordinates
		osg::Vec3f p_cam = p * osg::Matrix::inverse(img->_cameraTransform);
		if (p_cam.z() < 0) continue;

		// project to image space
		osg::Vec2f p_image((img->_imageCenter.x() + (img->_imageSamplePoint.x() - img->_imageCenter.x()) * p_cam.x() / p_cam.z()), (img->_imageCenter.y() + (img->_imageSamplePoint.y() - img->_imageCenter.y()) * p_cam.y() / p_cam.z()));
		if (p_image.x() < 0 || p_image.x() > img->_width - 1 || p_image.y() < 0 || p_image.y() > img->_height - 1) continue;

		//bilinear interpolation of depth data
		float gamma = p_image.x() - floorf(p_image.x());
		float delta = p_image.y() - floorf(p_image.y());

		osg::Vec3ub depthVec00 = depthData->at<osg::Vec3ub>(floorf(p_image.y()), floorf(p_image.x()));
		osg::Vec3ub depthVec01 = depthData->at<osg::Vec3ub>(ceilf(p_image.y()), floorf(p_image.x()));
		osg::Vec3ub depthVec10 = depthData->at<osg::Vec3ub>(floorf(p_image.y()), ceilf(p_image.x()));
		osg::Vec3ub depthVec11 = depthData->at<osg::Vec3ub>(ceilf(p_image.y()), ceilf(p_image.x()));

		int depth00 = ((int) depthVec00.b()) + ((int) depthVec00.g()) << 8;
		int depth01 = ((int) depthVec01.b()) + ((int) depthVec01.g()) << 8;
		int depth10 = ((int) depthVec10.b()) + ((int) depthVec10.g()) << 8;
		int depth11 = ((int) depthVec11.b()) + ((int) depthVec11.g()) << 8;

		float depth = img->_depthScale * ((1 - delta) * ((1 - gamma) * depth00 + gamma * depth10) + delta * ((1 - gamma) * depth01 + gamma * depth11));
		// TODO 2: find a reasonable bias
		if (m_textureProcessingDepthBias >= 0 && p_cam.length() > depth * (1 + m_textureProcessingDepthBias)) continue;

		//weight *= ((1 - delta) * ((1 - gamma) * depthVec00.z() + gamma * depthVec10.z()) + delta * ((1 - gamma) * depthVec01.z() + gamma * depthVec11.z())) / 255.f;

		// bilinear interpolation of image color
		osg::Vec3ub color00 = colorData->at<osg::Vec3ub>(floorf(p_image.y()), floorf(p_image.x()));
		osg::Vec3ub color01 = colorData->at<osg::Vec3ub>(ceilf(p_image.y()), floorf(p_image.x()));
		osg::Vec3ub color10 = colorData->at<osg::Vec3ub>(floorf(p_image.y()), ceilf(p_image.x()));
		osg::Vec3ub color11 = colorData->at<osg::Vec3ub>(ceilf(p_image.y()), ceilf(p_image.x()));

		osg::Vec3f color((1 - delta) * ((1 - gamma) * color00.x() + gamma * color10.x()) + delta * ((1 - gamma) * color01.x() + gamma * color11.x()),
			(1 - delta) * ((1 - gamma) * color00.y() + gamma * color10.y()) + delta * ((1 - gamma) * color01.y() + gamma * color11.y()),
			(1 - delta) * ((1 - gamma) * color00.z() + gamma * color10.z()) + delta * ((1 - gamma) * color01.z() + gamma * color11.z()));

		//osg::Vec3ub colorTest = colorData.at<osg::Vec3ub>(roundf(p_image.y() * img->_heightColor), roundf(p_image.x() * img->_widthColor));

		// add weighted color onto color data & weight onto total weight
		data[3 * i] += color.x() * weight;
		data[3 * i + 1] += color.y() * weight;
		data[3 * i + 2] += color.z() * weight;
		weights[i] += weight;
		//++count;
	}
}

OctreeVolume<float>* PointCloudVisualizer::getOctreeVolume() {
	return m_octreeVolume;
}

bool PointCloudVisualizer::exportPointCloudBinary() {
	//NOTE: custom PLY export; Binary version doesnt work properly with m_PcVertices for unknown reasons
	//      Use exportPointCloudToPLY instead
	std::string path = m_configReader->getStringFromStartupConfig("export_path_point_cloud");
	if (!generateExportPath(path)) return false;
	std::string filePath = generateExportFilePath(path, ".ply");
	std::ofstream ofstream(filePath);
	if (!ofstream.is_open()) {
		std::cout << "Error: File could not be opened." << std::endl;
		return false;
	}
	ofstream << "ply" << std::endl;
	{
		//Testing the endianess of the system
		unsigned int x = 1;
		char* ptr = (char*) &x;
		ofstream << "format " << (ptr[0] == 1 ? "binary_little_endian" : "binary_big_endian") << " 1.0" << std::endl;
	}
	ofstream << "comment exported from ClayMore" << std::endl;
	ofstream << "element vertex " << std::to_string(m_PcVertices->size()) << std::endl;
	ofstream << "property float x" << std::endl;
	ofstream << "property float y" << std::endl;
	ofstream << "property float z" << std::endl;
	/*ofstream << "property uchar red" << std::endl;
	ofstream << "property uchar green" << std::endl;
	ofstream << "property uchar blue" << std::endl;*/
	ofstream << "end_header" << std::endl;

	const unsigned int pointByteSize = 12u;
	unsigned int threadCount = std::thread::hardware_concurrency();
	std::cout << "threadCount: " << threadCount << std::endl;
	std::cout << "m_PcVertices: " << m_PcVertices->size() << std::endl;

	size_t slice = m_PcVertices->size() / threadCount;
	size_t start = 0;
	std::vector<std::future<void>> futures;
	futures.reserve(threadCount - 1);
	std::vector<char*> buffers;
	buffers.reserve(threadCount);
	//start threads to write vertices to ply
	for (unsigned int t = 0; t < threadCount - 1; t++) {
		std::cout << t << ": " << start << " | " << slice << std::endl;
		char* buffer = new char[slice * pointByteSize];
		buffers.push_back(buffer);
		futures.push_back(std::async(std::launch::async, &PointCloudVisualizer::exportPoints, this, start, slice, buffer));
		start += slice;
	}
	//last slice will be processed on the main thread
	size_t lastSlice = m_PcVertices->size() - start;
	std::cout << threadCount - 1 << ": " << start << " | " << lastSlice << std::endl;
	buffers.push_back(new char[lastSlice * pointByteSize]);
	exportPoints(start, lastSlice, buffers.at(buffers.size() - 1));

	//wait for all threads to finish
	for (auto& future : futures) {
		if (future.valid()) future.get();
	}
	//Write buffers to file
	for (int t = 0; t < threadCount - 1; t++) ofstream.write(buffers.at(t), slice * pointByteSize);
	ofstream.write(buffers.at(threadCount - 1), lastSlice * pointByteSize);
	//free allocated memory
	for (int t = 0; t < threadCount; t++) delete[] buffers.at(t);

	ofstream.flush();
	ofstream.close();
	std::cout << "PointCloud exported to: " << filePath << std::endl;
	return true;
}

void PointCloudVisualizer::exportPoints(const size_t start, const size_t count, char* bytes_output) {
	const unsigned int pointByteSize = 12u;
	for (size_t i = 0; i < count; i++) {
		size_t c = i + start;

		//copy the vertex data to the output byte array
		osg::Vec3f& vertex = m_PcVertices->at(c);
		float* vertexBytePtr = (float*) (bytes_output + i * pointByteSize);
		vertexBytePtr[0] = (float) vertex.x();
		vertexBytePtr[1] = (float) vertex.y();
		vertexBytePtr[2] = (float) vertex.z();

		//copy the color data to the output byte array
		/*osg::Vec4& color = m_PcColorArray->at(c);
		unsigned char* colorBytePtr = (unsigned char*)(vertexBytePtr + 3);
		colorBytePtr[0] = color.r() * 255;
		colorBytePtr[1] = color.g() * 255;
		colorBytePtr[2] = color.b() * 255;*/
	}
}

bool PointCloudVisualizer::exportPointCloudAscii() {
	//NOTE: custom PLY export; Binary version doesnt work properly with m_PcVertices for unknown reasons
	//      Use exportPointCloudToPLY instead
	std::string path = m_configReader->getStringFromStartupConfig("export_path_point_cloud");
	if (!generateExportPath(path)) return false;
	std::string filePath = generateExportFilePath(path, ".ply");
	std::ofstream ofstream(filePath);
	if (!ofstream.is_open()) {
		std::cout << "Error: File could not be opened." << std::endl;
		return false;
	}
	ofstream << "ply" << std::endl;
	ofstream << "format ascii 1.0" << std::endl;
	ofstream << "comment exported from ClayMore" << std::endl;
	ofstream << "element vertex " << std::to_string(m_PcVertices->size()) << std::endl;
	ofstream << "property float x" << std::endl;
	ofstream << "property float y" << std::endl;
	ofstream << "property float z" << std::endl;
	/*ofstream << "property uchar red" << std::endl;
	ofstream << "property uchar green" << std::endl;
	ofstream << "property uchar blue" << std::endl;*/
	ofstream << "end_header" << std::endl;

	const unsigned int pointByteSize = 12u;

	for (size_t i = 0; i < m_PcVertices->size(); i++) {
		osg::Vec3f& vertex = m_PcVertices->at(i);
		ofstream << std::to_string(vertex.x()) << " " << std::to_string(vertex.y()) << " " << std::to_string(vertex.z()) << std::endl;
	}

	ofstream.flush();
	ofstream.close();
	std::cout << "PointCloud exported to: " << filePath << std::endl;
	return true;
}

bool PointCloudVisualizer::exportPointCloudMesh(std::string fileName) {
	std::string path = m_configReader->getStringFromStartupConfig("export_path_point_cloud");
	if (!generateExportPath(path)) return false;
	std::string filePath;
	if (fileName == "") filePath = generateExportFilePath(path, ".obj");
	else filePath = path + "/" + fileName + ".obj";

	std::cout << "Exporting PointCloudMesh..." << std::endl;
	if (m_MCmeshGeode->getNumChildren() > 0 && osgDB::writeNodeFile(*m_MCmeshGeode->getChild(0), filePath)) {
		std::cout << "PointCloudMesh exported to: " << filePath << std::endl;
		return true;
	} else {
		std::cout << "Error: PointCloudMesh could not be exported." << std::endl;
		return false;
	}
}

bool PointCloudVisualizer::exportPointCloudToPLY(bool binary) {
	std::string path = m_configReader->getStringFromStartupConfig("export_path_point_cloud");
	std::string filePath = generateExportFilePath(path, ".ply");

	pcl::PointCloud<pcl::PointXYZRGB>* pcl = convertToPCL();
	if (binary) pcl::io::savePLYFileBinary(filePath, *pcl);
	else pcl::io::savePLYFileASCII(filePath, *pcl);
	delete pcl;

	std::cout << "PointCloud exported to: " << filePath << std::endl;
	return true;
}

bool PointCloudVisualizer::exportPointCloudToPCD(bool binary) {
	std::string path = m_configReader->getStringFromStartupConfig("export_path_point_cloud");
	std::string filePath = generateExportFilePath(path, ".pcd");

	pcl::PointCloud<pcl::PointXYZRGB>* pcl = convertToPCL();
	if (binary) pcl::io::savePCDFileBinaryCompressed(filePath, *pcl);
	else pcl::io::savePCDFileASCII(filePath, *pcl);
	delete pcl;

	std::cout << "PointCloud exported to: " << filePath << std::endl;
	return true;
}

pcl::PointCloud<pcl::PointXYZRGB>* PointCloudVisualizer::convertToPCL() {
	pcl::PointCloud<pcl::PointXYZRGB>* pcl = new pcl::PointCloud<pcl::PointXYZRGB>;
	pcl->resize(m_PcVertices->size());

	unsigned int threadCount = std::thread::hardware_concurrency();
	std::vector<std::future<void>> futures;
	futures.reserve(threadCount - 1);
	size_t start = 0;
	size_t slice = pcl->size() / threadCount;

	//Execute data copying on multiple threads
	for (unsigned int t = 0; t < threadCount - 1; t++) {
		futures.push_back(std::async(std::launch::async, &PointCloudVisualizer::copyDataSliceToPCL, this, pcl, start, slice));
		start += slice;
	}
	//Copy last slice on main thread
	copyDataSliceToPCL(pcl, start, pcl->size() - start);
	//Wait for all threads to finish
	for (std::future<void>& future : futures) {
		if (future.valid()) future.get();
	}
	return pcl;
}

void PointCloudVisualizer::copyDataSliceToPCL(pcl::PointCloud<pcl::PointXYZRGB>* pcl, size_t start, size_t count) {
	for (size_t i = start; i < start + count; i++) {
		osg::Vec3f& vertex = m_PcVertices->at(i);
		osg::Vec4f& color = m_PcColorArray->at(i);

		pcl::PointXYZRGB& point = pcl->at(i);
		point.x = vertex.x();
		point.y = vertex.y();
		point.z = vertex.z();
		point.r = color.r() * 255u;
		point.g = color.g() * 255u;
		point.b = color.b() * 255u;
	}
}

bool PointCloudVisualizer::exportPointCloudTexture(const std::vector<ImageData*>* imageData, std::string fileName) {
	//generatePointCloudFromImageData(imageData);
	//return true;
	std::string path = m_configReader->getStringFromStartupConfig("export_path_point_cloud");
	if (!generateExportPath(path)) return false;
	std::string filePath;
	if (fileName == "") filePath = generateExportFilePath(path, ".png");
	else filePath = path + "/" + fileName + ".png";

	std::cout << "Exporting PointCloudTexture..." << std::endl;
	if (m_MCmeshGeode->getNumChildren() > 0) {
		m_lastProjectedTexture = generateTextureFromImageData((osg::Geometry*) m_MCmeshGeode->getChild(0), imageData, m_textureProcessingOutputResolution, 2);
		cv::imwrite(filePath, *m_lastProjectedTexture);
		std::cout << "PointCloudTexture exported to: " << filePath << std::endl;
		delete m_lastProjectedTexture;
		/*m_newestTexturePath = filePath;

		m_polygonizedPCImageStream->setImage(
			m_textureProcessingOutputResolution,
			m_textureProcessingOutputResolution,
			1,
			GL_BGR,
			GL_BGR,
			GL_UNSIGNED_BYTE,
			m_lastProjectedTexture->data,
			osg::Image::AllocationMode::NO_DELETE); //Bild der RealSense wird auf den ImageStream gesetzt
		m_polygonizedPCImageStream->dirty();

		m_polygonizedPointCloud->getOrCreateStateSet()->setTextureAttributeAndModes(0, m_polygonizedPCTexture.get(), osg::StateAttribute::ON);*/
		return true;
	} else {
		std::cout << "Error: PointCloudTexture could not be exported." << std::endl;
		m_newestTexturePath = "";
		return false;
	}
}

bool PointCloudVisualizer::generateExportPath(const std::string& path) {
	if (!boost::filesystem::exists(path)) {
		std::cout << "File path does not exist. Attmepting to create all missing directories..." << std::endl;
		if (!boost::filesystem::create_directories(path)) {
			std::cout << "Error: Directory could not be created." << std::endl;
			return false;
		}
		std::cout << "Success: The path was successfully created." << std::endl;
	}
	return true;
}

std::string PointCloudVisualizer::generateExportFilePath(const std::string& path, const std::string& ext) {
	return path + "/" + std::to_string(std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::system_clock::now().time_since_epoch()).count()) + ext;
}

/*
osg::Matrix PointCloudVisualizer::getTransformationMatrixFromBuffer() {
	return m_PcBuffer->getMatrix().getMatrix();
}
*/

osg::ref_ptr<osg::Group> PointCloudVisualizer::getRootNode()
{
	return m_pointCloudGroup;;
}

void PointCloudVisualizer::togglePointCloudDisplay() {
	setPointCloudDisplay(m_PcGeode->getNodeMask() == 0);
}

void PointCloudVisualizer::toggleMarchingCubesDisplay() {
	setMarchingCubesDisplay(m_MCmeshGeode->getNodeMask() == 0);
}

void PointCloudVisualizer::clearMarchingCubesMeshes() {
	m_MCmeshGeode->removeChildren(0, m_MCmeshGeode->getNumChildren());
}

void PointCloudVisualizer::setPointCloudDisplay(bool displayState) {
	m_PcGeode->setNodeMask(displayState ? 0xffffffff : 0x0);
}

void PointCloudVisualizer::setMarchingCubesDisplay(bool displayState) {
	m_MCmeshGeode->setNodeMask(displayState ? 0xffffffff : 0x0);
}