#pragma once

#include "ScanDevice.h"
#include <k4a/k4a.h>
#include <k4a/k4a.hpp>

inline static int k4aGetColorResolution(const unsigned int& width, const unsigned int& height) {
	if (width == 0 && height == 0) return K4A_COLOR_RESOLUTION_OFF;
	else if (width == 1280 && height == 720) return K4A_COLOR_RESOLUTION_720P;
	else if (width == 1920 && height == 1080) return K4A_COLOR_RESOLUTION_1080P;
	else if (width == 2560 && height == 1440) return K4A_COLOR_RESOLUTION_1440P;
	else if (width == 2048 && height == 1536) return K4A_COLOR_RESOLUTION_1536P;
	else if (width == 3840 && height == 2160) return K4A_COLOR_RESOLUTION_2160P;
	else if (width == 4096 && height == 3072) return K4A_COLOR_RESOLUTION_3072P;
	return -1;
}

class AzureKinectScanDevice : public ScanDevice {
private:
	k4a_device_t m_k4aDevice;
	k4a_device_configuration_t m_k4aConfig;
	k4a_transformation_t m_k4aTransformation;
	//k4a_image_t m_k4aCalibrationTable;
	//Data from last scan
	k4a_capture_t m_k4aCapture;
	k4a_image_t m_k4aColor;
	k4a_image_t m_k4aDepth;
	k4a_image_t m_k4aDepthTransformed;
	k4a_image_t m_k4aPointcloud;
	k4a_image_t m_k4aTexCoords;
public:
	AzureKinectScanDevice() = delete;
	AzureKinectScanDevice(k4a_device_t device, k4a_device_configuration_t& config, PoseDriver* poseDriver, const unsigned int& pointCloudWidth, const unsigned int& pointCloudHeight, cv::Mat* cameraMatrix, cv::Mat* distortionCoefficients);
	~AzureKinectScanDevice();
	virtual StartDeviceResult start() override;
	virtual StopDeviceResult stop() override;
	virtual ScanResult scan(const unsigned int ms_timeout = 1000u) override;
	virtual PointcloudCalculationResult calculatePointcloud() override;
	virtual void releaseScan() override;
	void loadConfig(k4a_device_configuration_t& config);
private:
	inline void reloadColorFormat();
	void generateTextureCoordinates(const k4a_calibration_t& calibration);
	void calculateTextureCoordinates(const int width, const int height);
	void clearTextureCoordinates();
	//void generateCalibrationTable();
};