#include "DisplayTarget.h"

#include <osg/MatrixTransform>
#include <osg/Geode>
#include <osg/geometry>
#include <osg/Vec3f>


DisplayTarget::DisplayTarget(osg::ref_ptr<osg::MatrixTransform> matTrans, const bool isOverlay) : m_isDirty{ false }, m_isDisplayed{ true } {
	//Setup  geometry to draw lines
	m_geode = new osg::Geode();
	m_geode->getOrCreateStateSet()->setMode(GL_LIGHTING, osg::StateAttribute::OFF); //No shading -> no light
	m_geode->getOrCreateStateSet()->setMode(GL_BLEND, osg::StateAttribute::ON);
	if (isOverlay) m_geode->getOrCreateStateSet()->setMode(GL_DEPTH_TEST, osg::StateAttribute::OFF);

	m_geom = new osg::Geometry();
	//##### NEW Speedup by using VBOs instead of displayLists
	m_geom->setUseDisplayList(false);
	m_geom->setUseVertexBufferObjects(true);
	m_geom->setDataVariance(osg::Object::DYNAMIC);
	//##### NEW END
	m_drawArrays = new osg::DrawArrays(osg::PrimitiveSet::LINES);
	m_geom->addPrimitiveSet(m_drawArrays);
	m_colors = new osg::Vec4Array();
	m_colors->setBinding(osg::Array::BIND_PER_VERTEX);
	m_geom->setColorArray(m_colors);
	m_vertices = new osg::Vec3Array();
	m_geom->setVertexArray(m_vertices);
	m_geode->addDrawable(m_geom);

	matTrans->addChild(m_geode);
};

DisplayTarget::~DisplayTarget() {
	for (auto parent : m_geode->getParents()) parent->removeChild(m_geode);
};

void DisplayTarget::drawLine(const osg::Vec3& start, const osg::Vec3& end, const osg::Vec4& colorStart, const osg::Vec4& colorEnd) {
	m_vertices->push_back(start);
	m_vertices->push_back(end);
	m_colors->push_back(colorStart);
	m_colors->push_back(colorEnd);
	//RedrawLines();
	m_isDirty = true;
}

void DisplayTarget::drawRay(const osg::Vec3& origin, const osg::Vec3& dir, const float length, const osg::Vec4& colorStart, const osg::Vec4& colorEnd) {
	drawLine(origin, origin + dir.operator*(length), colorStart, colorEnd);
}

void DisplayTarget::setLine(const unsigned int index, const osg::Vec3& start, const osg::Vec3& end, const osg::Vec4& colorStart, const osg::Vec4& colorEnd) {
	(*m_vertices)[index] = start;
	(*m_vertices)[index + 1u] = end;
	(*m_colors)[index] = colorStart;
	(*m_colors)[index + 1u] = colorEnd;
	m_isDirty = true;
}

void DisplayTarget::setLine(const unsigned int index, const osg::Vec3& origin, const osg::Vec3& dir, const float length, const osg::Vec4& colorStart, const osg::Vec4& colorEnd) {
	setLine(index, origin, origin + dir.operator*(length), colorStart, colorEnd);
}

void DisplayTarget::redraw() {
	if (!m_isDirty) return;
	m_drawArrays->setFirst(0);
	m_drawArrays->setCount(m_vertices->size());
	m_geom->dirtyBound();
	m_geom->dirtyGLObjects();
	m_vertices->dirty();
	m_colors->dirty();
	m_drawArrays->dirty();
	m_isDirty = false;
}

void DisplayTarget::drawQuad(const osg::Vec3& origin, osg::Vec3 normal, osg::Vec3 forward, const osg::Vec2 extents, const osg::Vec4& color) {
	forward.normalize();
	normal.normalize();
	osg::Vec3 t = forward ^ normal;
	t.normalize();

	osg::Vec3 c1 = origin + t * extents.x() + forward * extents.y();
	osg::Vec3 c2 = origin + t * extents.x() - forward * extents.y();
	osg::Vec3 c3 = origin - t * extents.x() - forward * extents.y();
	osg::Vec3 c4 = origin - t * extents.x() + forward * extents.y();

	drawLine(c1, c2, color, color);
	drawLine(c2, c3, color, color);
	drawLine(c3, c4, color, color);
	drawLine(c4, c1, color, color);
}

void DisplayTarget::drawBox(const osg::Vec3& origin, osg::Vec3 normal, osg::Vec3 forward, const osg::Vec3 extents, const osg::Vec4& color) {
	forward.normalize();
	normal.normalize();
	osg::Vec3 t = forward ^ normal;

	osg::Vec3 c1 = origin + t * extents.x() + forward * extents.z() + normal * extents.y();
	osg::Vec3 c2 = origin + t * extents.x() - forward * extents.z() + normal * extents.y();
	osg::Vec3 c3 = origin - t * extents.x() - forward * extents.z() + normal * extents.y();
	osg::Vec3 c4 = origin - t * extents.x() + forward * extents.z() + normal * extents.y();

	osg::Vec3 c5 = origin + t * extents.x() + forward * extents.z() - normal * extents.y();
	osg::Vec3 c6 = origin + t * extents.x() - forward * extents.z() - normal * extents.y();
	osg::Vec3 c7 = origin - t * extents.x() - forward * extents.z() - normal * extents.y();
	osg::Vec3 c8 = origin - t * extents.x() + forward * extents.z() - normal * extents.y();

	drawLine(c1, c2, color, color);
	drawLine(c2, c3, color, color);
	drawLine(c3, c4, color, color);
	drawLine(c4, c1, color, color);

	drawLine(c5, c6, color, color);
	drawLine(c6, c7, color, color);
	drawLine(c7, c8, color, color);
	drawLine(c8, c5, color, color);

	drawLine(c1, c5, color, color);
	drawLine(c2, c6, color, color);
	drawLine(c3, c7, color, color);
	drawLine(c4, c8, color, color);
}

void DisplayTarget::drawEmpty(const osg::Vec3& point, const float extent, const osg::Vec4& color) {
	drawLine(osg::Vec3(point.x() - extent, point.y(), point.z()), osg::Vec3(point.x() + extent, point.y(), point.z()), color, color);
	drawLine(osg::Vec3(point.x(), point.y() - extent, point.z()), osg::Vec3(point.x(), point.y() + extent, point.z()), color, color);
	drawLine(osg::Vec3(point.x(), point.y(), point.z() - extent), osg::Vec3(point.x(), point.y(), point.z() + extent), color, color);
}

void DisplayTarget::clear() {
	m_vertices->clear();
	m_colors->clear();
	m_isDirty = true;
	//RedrawLines();
}

bool DisplayTarget::getDisplayState() {
	return m_isDisplayed;
}

void DisplayTarget::setDisplayState(bool displayState) {
	m_isDisplayed = displayState;
	m_geode->setNodeMask(displayState ? 0xffffffff : 0x0);
}

bool DisplayTarget::toggleDisplayState() {
	setDisplayState(!m_isDisplayed);
	return !m_isDisplayed;
}