#pragma once

//#define def_toFloat(T) inline float toFloat(T& value) { return _toFloatConversion * value; }
#define def_component(name, T, dimension, offset) virtual inline const float name(const unsigned int index) override { return _toFloatConversion * ((T*) _data)[index * dimension + offset]; }
#define def_componentConst(name, value) virtual inline const float name(const unsigned int index) override { return value; }
//#define def_rgba(T, dimension, rOffset, gOffset, bOffset, aOffset)\
//	virtual inline const osg::Vec4f& rgba(const unsigned int index) override {\
//		T* p = ((T*)_data) + index * dimension;\
//		return osg::Vec4f{ toFloat(p[rOffset]), toFloat(p[gOffset]), toFloat(p[bOffset]), toFloat(p[aOffset]) };\
//	}

struct ColorData {
	enum class ColorFormat {
		RGBA8,		// RS
		RGB8,		// RS
		BGRA8,		// Kinect, RS
		BGR8,		// RS
		UNSUPPORTED
	};
protected:
	const void* _data;
	const ColorFormat _format;
	const size_t _bytes;
	const unsigned int _width;
	const unsigned int _height;
	const float _toFloatConversion;
public:
	ColorData(const void* data, const ColorFormat format, const size_t bytes, const unsigned int width, const unsigned int height, const unsigned int maxValue) : _data(data), _format(format), _bytes(bytes), _width(width), _height(height), _toFloatConversion(1.f / maxValue) {}
	virtual ~ColorData() {}
	virtual inline const float r(const unsigned int index) = 0;
	virtual inline const float g(const unsigned int index) = 0;
	virtual inline const float b(const unsigned int index) = 0;
	virtual inline const float a(const unsigned int index) = 0;
	const void* data() { return _data; }
	const ColorFormat format() { return _format; }
	const size_t bytes() { return _bytes; }
	const size_t size() { return (size_t) _width * _height; }
	const unsigned int width() { return _width; }
	const unsigned int height() { return _height; }
};

struct ColorData_RGBA8 : public ColorData {
	ColorData_RGBA8(const void* data, const size_t bytes, const unsigned int width, const unsigned int height) : ColorData(data, ColorFormat::RGBA8, bytes, width, height, 255u) {}
	def_component(r, unsigned char, 4u, 0u)
	def_component(g, unsigned char, 4u, 1u)
	def_component(b, unsigned char, 4u, 2u)
	def_component(a, unsigned char, 4u, 3u)
};

struct ColorData_RGB8 : public ColorData {
	ColorData_RGB8(const void* data, const size_t bytes, const unsigned int width, const unsigned int height) : ColorData(data, ColorFormat::RGB8, bytes, width, height, 255u) {}
	def_component(r, unsigned char, 3u, 0u)
	def_component(g, unsigned char, 3u, 1u)
	def_component(b, unsigned char, 3u, 2u)
	def_componentConst(a, 1.f)
};

struct ColorData_BGRA8 : public ColorData {
	ColorData_BGRA8(const void* data, const size_t bytes, const unsigned int width, const unsigned int height) : ColorData(data, ColorFormat::BGRA8, bytes, width, height, 255u) {}
	def_component(r, unsigned char, 4u, 2u)
	def_component(g, unsigned char, 4u, 1u)
	def_component(b, unsigned char, 4u, 0u)
	def_component(a, unsigned char, 4u, 3u)
};

struct ColorData_BGR8 : public ColorData {
	ColorData_BGR8(const void* data, const size_t bytes, const unsigned int width, const unsigned int height) : ColorData(data, ColorFormat::BGR8, bytes, width, height, 255u) {}
	def_component(r, unsigned char, 3u, 2u)
	def_component(g, unsigned char, 3u, 1u)
	def_component(b, unsigned char, 3u, 0u)
	def_componentConst(a, 1.f)
};

//#undef def_toFloat
#undef def_component
#undef def_componentConst