#include "ScanDevice.h"
#include <future>

ScanDevice::ScanDevice(PoseDriver* poseDriver, const std::string& serial, const std::string& name, cv::Mat* cameraMatrix, cv::Mat* distortionCoefficients, const unsigned int& colorWidth, const unsigned int& colorHeight, const unsigned int& pointCloudWidth, const unsigned int& pointCloudHeight, const unsigned int& fps, const float& depthScale) :
	m_systemClockOrigin(0ull),
	m_deviceClockOrigin(0ull),
	m_isRunning(false), m_isScanning(false), m_serial(serial), m_name(name),
	m_colorWidth(colorWidth), m_colorHeight(colorHeight), m_pointCloudWidth(std::min(pointCloudWidth, colorWidth)), m_pointCloudHeight(std::min(pointCloudHeight, colorHeight)), m_fps(fps), m_depthScale(depthScale),
	m_poseDriver(poseDriver), m_colorData(nullptr), m_depthData(nullptr), m_pointcloudData(nullptr), m_pointcloudDataDownsampled(nullptr), 
	m_pointCloudVerticesDownsampled(nullptr), m_pointCloudTexCoordsDownsampled(nullptr),
	m_colorFormat(ColorData::ColorFormat::UNSUPPORTED), m_depthFormat(DepthData::DepthFormat::UNSUPPORTED),
	m_deviceTimestamp(0ull),
	m_systemTimestamp(0ull), 
	m_cameraMatrix(cameraMatrix),
	m_distortionCoefficients(distortionCoefficients)
{ }

ScanDevice::~ScanDevice() {
	releaseScan();
	delete[] m_pointCloudVerticesDownsampled;
	delete[] m_pointCloudTexCoordsDownsampled;
	delete m_poseDriver;
	delete m_cameraMatrix;
	delete m_distortionCoefficients;
}

bool ScanDevice::isRunning() {
	return m_isRunning.load();
}

const std::string& ScanDevice::getSerial() {
	return m_serial;
}

const std::string& ScanDevice::getName() {
	return m_name;
}

osg::Matrix ScanDevice::getTransform() const {
	return m_poseDriver->getTransform(m_systemTimestamp);
}

osg::Matrix ScanDevice::getPose() const {
	return m_poseDriver->getPose(m_systemTimestamp);
}

osg::Matrix ScanDevice::getSensorToDriverMatrix() const {
	return m_poseDriver->getSensorToDriverMatrix();
}

PoseDriver* ScanDevice::getDriver() const {
	return m_poseDriver;
}

ColorData::ColorFormat ScanDevice::getColorFormat() const {
	return m_colorFormat;
}

DepthData::DepthFormat ScanDevice::getDepthFormat() const {
	return m_depthFormat;
}

unsigned long long ScanDevice::getDeviceTimestamp() {
	return m_deviceTimestamp;
}

unsigned int ScanDevice::getColorWidth() const {
	return m_colorWidth;
}

unsigned int ScanDevice::getColorHeight() const {
	return m_colorHeight;
}

unsigned int ScanDevice::getPointCloudWidth(bool downsampled) const {
	return downsampled ? m_pointCloudWidth : m_colorWidth;
}

unsigned int ScanDevice::getPointCloudHeight(bool downsampled) const {
	return downsampled ? m_pointCloudHeight : m_colorHeight;
}

unsigned long long ScanDevice::getSystemTimestamp() {
	return m_systemTimestamp;
}

bool ScanDevice::attemptToStart() {
	bool expected = false;
	return m_isRunning.compare_exchange_weak(expected, true);
}

unsigned long long ScanDevice::deviceToSystemClock(unsigned long long deviceTimestamp) {
	return m_systemClockOrigin + deviceTimestamp - m_deviceClockOrigin;
}

ScanDevice::ScanResult ScanDevice::scanStart() {
	if (!m_isRunning.load()) return ScanResult::DEVICE_IS_NOT_RUNNING;
	bool expected = false;
	if (!m_isScanning.compare_exchange_strong(expected, true)) return ScanResult::DEVICE_IS_ALREADY_SCANNING;
	releaseScan();
	return ScanResult::SUCCESS;
}

ScanDevice::ScanResult ScanDevice::scanEnd(ScanDevice::ScanResult scanResult, bool releaseScan) {
	if (releaseScan && scanResult != ScanResult::SUCCESS) this->releaseScan();
	m_isScanning.store(false);
	return scanResult;
}

void ScanDevice::releaseScan() {
	if (m_colorData) {
		delete m_colorData;
		m_colorData = nullptr;
	}
	if (m_depthData) {
		delete m_depthData;
		m_depthData = nullptr;
	}
	if (m_pointcloudData) {
		delete m_pointcloudData;
		m_pointcloudData = nullptr;
	}
	if (m_pointcloudDataDownsampled) {
		delete m_pointcloudDataDownsampled;
		m_pointcloudDataDownsampled = nullptr;
	}
}

ScanDevice::DownsamplePointCloudResult ScanDevice::downsamplePointCloud() {
	if (!m_pointcloudData) return DownsamplePointCloudResult::NO_POINT_CLOUD_DATA;
	if (m_pointCloudWidth == m_colorWidth && m_pointCloudHeight == m_colorHeight) return DownsamplePointCloudResult::DOWNSAMPLING_IMPOSSIBLE;
	
	if (!m_pointCloudVerticesDownsampled) m_pointCloudVerticesDownsampled = new float[3ull * m_pointCloudWidth * m_pointCloudHeight];
	if (!m_pointCloudTexCoordsDownsampled) m_pointCloudTexCoordsDownsampled = new float[2ull * m_pointCloudWidth * m_pointCloudHeight];
	if (!m_pointcloudDataDownsampled) m_pointcloudDataDownsampled = new PointcloudData_XYZ32F_UV32F(m_pointCloudVerticesDownsampled, m_pointCloudTexCoordsDownsampled, m_pointCloudWidth, m_pointCloudHeight, 1.f);

	//for (unsigned int y = 0; y < m_pointCloudHeight; ++y) {
	//	for (unsigned int x = 0; x < m_pointCloudWidth; ++x) {
	//		float u = (0.5f + x) / m_pointCloudWidth;
	//		float v = (0.5f + y) / m_pointCloudHeight;
	//		
	//		unsigned int xCol = (unsigned int) (u * m_colorWidth);
	//		unsigned int yCol = (unsigned int) (v * m_colorHeight);

	//		
	//		size_t i = (size_t) m_pointCloudWidth * y + x; //index of the downsampled vertex
	//		size_t iCol = (size_t) m_colorWidth * yCol + xCol; //index of the original vertex

	//		float* vertex = m_pointCloudVerticesDownsampled + 3ull * i;
	//		vertex[0] = m_pointcloudData->x(iCol);
	//		vertex[1] = m_pointcloudData->y(iCol);
	//		vertex[2] = m_pointcloudData->z(iCol);
	//		
	//		float* texCoord = m_pointCloudTexCoordsDownsampled + 2ull * i;
	//		texCoord[0] = m_pointcloudData->u(iCol);
	//		texCoord[1] = m_pointcloudData->v(iCol);
	//	}
	//}

	unsigned int threadCount = std::min((unsigned int) (0.75f * std::thread::hardware_concurrency()), m_pointCloudHeight);
	std::vector<std::future<void>> futures;
	futures.reserve(threadCount - 1);
	size_t start = 0;
	size_t slice = m_pointCloudHeight / threadCount;

	for (unsigned int t = 0; t < futures.size(); t++) {
		futures.push_back(std::async(std::launch::async, &ScanDevice::downsamplePointCloudPartial, this, start, slice));
		start += slice;
	}
	downsamplePointCloudPartial(start, m_pointCloudHeight - start);
	for (std::future<void>& future : futures) {
		if (future.valid())	future.get();
	}
	return DownsamplePointCloudResult::SUCCESS;
}

void ScanDevice::downsamplePointCloudPartial(const unsigned int yStart, const unsigned int ySlice) {
	for (unsigned int y = yStart; y < yStart + ySlice; ++y) {
		for (unsigned int x = 0; x < m_pointCloudWidth; ++x) {
			float u = (0.5f + x) / m_pointCloudWidth;
			float v = (0.5f + y) / m_pointCloudHeight;

			unsigned int xCol = (unsigned int) (u * m_colorWidth);
			unsigned int yCol = (unsigned int) (v * m_colorHeight);


			size_t i = (size_t) m_pointCloudWidth * y + x; //index of the downsampled vertex
			size_t iCol = (size_t) m_colorWidth * yCol + xCol; //index of the original vertex

			float* vertex = m_pointCloudVerticesDownsampled + 3ull * i;
			vertex[0] = m_pointcloudData->x(iCol);
			vertex[1] = m_pointcloudData->y(iCol);
			vertex[2] = m_pointcloudData->z(iCol);

			float* texCoord = m_pointCloudTexCoordsDownsampled + 2ull * i;
			texCoord[0] = m_pointcloudData->u(iCol);
			texCoord[1] = m_pointcloudData->v(iCol);
		}
	}
}

PointcloudData* ScanDevice::unhookDownsampledPointcloud() {
	PointcloudData* pcd = m_pointcloudDataDownsampled;
	m_pointcloudDataDownsampled = nullptr;
	m_pointCloudVerticesDownsampled = nullptr;
	m_pointCloudTexCoordsDownsampled = nullptr;
	return pcd;
}

ColorData* ScanDevice::getColorData() {
	return m_colorData;
}

DepthData* ScanDevice::getDepthData() {
	return m_depthData;
}

PointcloudData* ScanDevice::getPointcloudData(const bool downsampled) const {
	if (downsampled && m_pointcloudDataDownsampled) return m_pointcloudDataDownsampled;
	return m_pointcloudData;
}

const cv::Mat* ScanDevice::getCameraMatrix() const {
	return m_cameraMatrix;
}

const cv::Mat* ScanDevice::getDistortionCoefficients() const {
	return m_distortionCoefficients;
}