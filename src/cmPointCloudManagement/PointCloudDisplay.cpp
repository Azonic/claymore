#include "PointCloudDisplay.h"

#include "osg/Vec3d"
#include "osg/Geometry"

PointCloudDisplay::PointCloudDisplay(osg::ref_ptr<osg::MatrixTransform> matTrans) {
	m_octreeDisplay = new DisplayTarget(matTrans);
	m_tsdfDisplay = new DisplayTarget(matTrans);
	m_ddaDisplay = new DisplayTarget(matTrans, true);
#ifdef DEBUG_DISPLAY
	m_debugDisplay = new DisplayTarget(matTrans);
#endif
}

PointCloudDisplay::~PointCloudDisplay() {
	delete m_octreeDisplay;
	delete m_tsdfDisplay;
	delete m_ddaDisplay;
#ifdef DEBUG_DISPLAY
	delete m_debugDisplay;
#endif
}

#ifdef DEBUG_DISPLAY
void PointCloudDisplay::drawDebug(const osg::Matrix trackerMatrix, std::vector<std::pair<osg::Matrix, ViewFrustum>>& cameraPoses, osg::Matrix patternToTrackerTrans, osg::Matrix patternToTrackerRot) {
	// This function is solely for various visual debugging scenarios
	// If needed DEBUG_DISPLAY has to be defined and this function has to be called somewhere

	m_debugDisplay->clear();

	/*osg::Matrix rsToTrackerRot = osg::Matrix::rotate(osg::DegreesToRadians(180.0f), osg::Vec3(0, 0, 1));
	osg::Matrix rsToTrackerTrans = osg::Matrix::translate(osg::Vec3(0.0225, -0.025743, 0.0471));

	osg::Matrix trackerToClayMoreRot1 = osg::Matrix::rotate(osg::DegreesToRadians(90.0f), osg::Vec3(1, 0, 0));
	osg::Matrix trackerToClayMoreRot2 = osg::Matrix::rotate(osg::DegreesToRadians(180.0f), osg::Vec3(0, 1, 0));

	osg::Matrix sensorMatrix = rsToTrackerRot * rsToTrackerTrans * trackerToClayMoreRot1 * trackerToClayMoreRot2 * trackerMatrix;

	m_debugDisplay->drawLine(osg::Vec3(0, 0, 0) * sensorMatrix, osg::Vec3(0, 0, 0.4) * sensorMatrix, osg::Vec4(1, 1, 1, 1), osg::Vec4(1, 1, 1, 1));

	m_debugDisplay->drawLine(osg::Vec3(0, 0.1, 0), osg::Vec3(1, 0.1, 0), osg::Vec4(1, 0, 0, 1), osg::Vec4(1, 0, 0, 1));
	m_debugDisplay->drawLine(osg::Vec3(0, 0.1, 0), osg::Vec3(0, 1.1, 0), osg::Vec4(0, 1, 0, 1), osg::Vec4(0, 1, 0, 1));
	m_debugDisplay->drawLine(osg::Vec3(0, 0.1, 0), osg::Vec3(0, 0.1, 1), osg::Vec4(0, 0, 1, 1), osg::Vec4(0, 0, 1, 1));

	m_debugDisplay->drawLine(osg::Vec3(0, 0, 0) * trackerMatrix, osg::Vec3(0.4, 0, 0) * trackerMatrix, osg::Vec4(1, 0, 0, 1), osg::Vec4(1, 0, 0, 1));
	m_debugDisplay->drawLine(osg::Vec3(0, 0, 0) * trackerMatrix, osg::Vec3(0, 0.4, 0) * trackerMatrix, osg::Vec4(0, 1, 0, 1), osg::Vec4(0, 1, 0, 1));
	m_debugDisplay->drawLine(osg::Vec3(0, 0, 0) * trackerMatrix, osg::Vec3(0, 0, 0.4) * trackerMatrix, osg::Vec4(0, 0, 1, 1), osg::Vec4(0, 0, 1, 1));*/
	
	/*m_debugDisplay->drawLine(osg::Vec3(0, 0, 0) * trackerMatrix, patternToTrackerOffset * trackerMatrix, osg::Vec4(1, 1, 0, 1), osg::Vec4(1, 1, 0, 1));
	m_debugDisplay->drawLine(patternToTrackerOffset * trackerMatrix, (vector * osg::Matrix::inverse(trackerToClayMoreRot1 * trackerToClayMoreRot2) + patternToTrackerOffset) * trackerMatrix, osg::Vec4(1, 1, 0, 1), osg::Vec4(1, 1, 0, 1));
	
	m_debugDisplay->drawLine(vector * osg::Matrix::inverse(trackerToClayMoreRot1 * trackerToClayMoreRot2) * patternToTrackerTrans * trackerMatrix, osg::Vec3(1, 0, 0) * calculatedCamPose * osg::Matrix::inverse(trackerToClayMoreRot1 * trackerToClayMoreRot2) * patternToTrackerTrans * trackerMatrix, osg::Vec4(1, 0, 0, 1), osg::Vec4(1, 0, 0, 1));
	m_debugDisplay->drawLine(vector * osg::Matrix::inverse(trackerToClayMoreRot1 * trackerToClayMoreRot2) * patternToTrackerTrans * trackerMatrix, osg::Vec3(0, 1, 0) * calculatedCamPose * osg::Matrix::inverse(trackerToClayMoreRot1 * trackerToClayMoreRot2) * patternToTrackerTrans * trackerMatrix, osg::Vec4(0, 1, 0, 1), osg::Vec4(0, 1, 0, 1));
	m_debugDisplay->drawLine(vector * osg::Matrix::inverse(trackerToClayMoreRot1 * trackerToClayMoreRot2) * patternToTrackerTrans * trackerMatrix, osg::Vec3(0, 0, 1) * calculatedCamPose * osg::Matrix::inverse(trackerToClayMoreRot1 * trackerToClayMoreRot2) * patternToTrackerTrans * trackerMatrix, osg::Vec4(0, 0, 1, 1), osg::Vec4(0, 0, 1, 1));*/

	osg::Vec3 o(0, 0, 0);
	osg::Vec3 x(.1, 0, 0);
	osg::Vec3 y(0, .1, 0);
	osg::Vec3 z(0, 0, .1);
	
	osg::Vec4 r(1, 0, 0, 1);
	osg::Vec4 g(0, 1, 0, 1);
	osg::Vec4 b(0, 0, 1, 1);

	osg::Vec4 c1(1, 0, 0, 1);
	osg::Vec4 c2(1, 1, 0, 1);
	osg::Vec4 c3(0, 1, 0, 1);
	osg::Vec4 c4(0, 1, 1, 1);
	osg::Vec4 c5(0, 0, 1, 1);

	// TrackerMatrix
	osg::Matrix mat = trackerMatrix;
	m_debugDisplay->drawLine(o, o * mat, c1, c2);
	m_debugDisplay->drawLine(o * mat, x * mat, r, r);
	m_debugDisplay->drawLine(o * mat, y * mat, g, g);
	m_debugDisplay->drawLine(o * mat, z * mat, b, b);

	// Tracker Offset
	osg::Matrix mat2 = patternToTrackerTrans * trackerMatrix;
	m_debugDisplay->drawLine(o * mat, o * mat2, c2, c3);

	// To OpenCV Coordinates
	osg::Matrix mat3 = patternToTrackerRot * patternToTrackerTrans * trackerMatrix;
	m_debugDisplay->drawLine(o * mat3, x * mat3, r, r);
	m_debugDisplay->drawLine(o * mat3, y * mat3, g, g);
	m_debugDisplay->drawLine(o * mat3, z * mat3, b, b);

	// Camera Transform
	for (auto& camPose : cameraPoses) {
		osg::Matrix mat4 = camPose.first;
		m_debugDisplay->drawLine(o * mat3, o * mat4, c3, c4);
		/*m_debugDisplay->drawLine(o * mat4, x * mat4, r, r);
		m_debugDisplay->drawLine(o * mat4, y * mat4, g, g);
		m_debugDisplay->drawLine(o * mat4, z * mat4, b, b);*/

		osg::Matrix mat5 = osg::Matrix::translate(0.015, 0, 0) * mat4;
		m_debugDisplay->drawLine(o * mat4, o * mat5, c4, c5);
		m_debugDisplay->drawLine(o * mat5, x * mat5, r, r);
		m_debugDisplay->drawLine(o * mat5, y * mat5, g, g);
		m_debugDisplay->drawLine(o * mat5, z * mat5, b, b);

		//Draw View Frustum
		ViewFrustum& frustum = camPose.second;
		const osg::Vec3f* frustumCorners = frustum.corners();
		const osg::Vec3f* frustumNormals = frustum.normals();

		osg::Vec4 frustumColor = frustum.contains(osg::Vec3f() * trackerMatrix) ? osg::Vec4(0.f, .5f, .5f, 1.f) : osg::Vec4(1.f, .5f, .5f, 1.f);
		// Corners
		m_debugDisplay->drawLine(frustumCorners[0], frustumCorners[1], frustumColor, frustumColor);
		m_debugDisplay->drawLine(frustumCorners[1], frustumCorners[2], frustumColor, frustumColor);
		m_debugDisplay->drawLine(frustumCorners[2], frustumCorners[3], frustumColor, frustumColor);
		m_debugDisplay->drawLine(frustumCorners[3], frustumCorners[0], frustumColor, frustumColor);
		m_debugDisplay->drawLine(frustumCorners[4], frustumCorners[5], frustumColor, frustumColor);
		m_debugDisplay->drawLine(frustumCorners[5], frustumCorners[6], frustumColor, frustumColor);
		m_debugDisplay->drawLine(frustumCorners[6], frustumCorners[7], frustumColor, frustumColor);
		m_debugDisplay->drawLine(frustumCorners[7], frustumCorners[4], frustumColor, frustumColor);
		m_debugDisplay->drawLine(frustumCorners[0], frustumCorners[4], frustumColor, frustumColor);
		m_debugDisplay->drawLine(frustumCorners[1], frustumCorners[5], frustumColor, frustumColor);
		m_debugDisplay->drawLine(frustumCorners[2], frustumCorners[6], frustumColor, frustumColor);
		m_debugDisplay->drawLine(frustumCorners[3], frustumCorners[7], frustumColor, frustumColor);

		// Normals
		osg::Vec3f near = (frustumCorners[0] + frustumCorners[1] + frustumCorners[2] + frustumCorners[3]) * 0.25f;
		osg::Vec3f far = (frustumCorners[4] + frustumCorners[5] + frustumCorners[6] + frustumCorners[7]) * 0.25f;
		osg::Vec3f top = (frustumCorners[0] + frustumCorners[3] + frustumCorners[4] + frustumCorners[7]) * 0.25f;
		osg::Vec3f bottom = (frustumCorners[1] + frustumCorners[2] + frustumCorners[5] + frustumCorners[6]) * 0.25f;
		osg::Vec3f left = (frustumCorners[2] + frustumCorners[3] + frustumCorners[6] + frustumCorners[7]) * 0.25f;
		osg::Vec3f right = (frustumCorners[0] + frustumCorners[1] + frustumCorners[4] + frustumCorners[5]) * 0.25f;

		osg::Vec4 colorNear(1.f, 0.f, 1.f, 1.f);
		osg::Vec4 colorFar(0.f, 1.f, 0.f, 1.f);
		osg::Vec4 colorTop(0.f, 0.f, 1.f, 1.f);
		osg::Vec4 colorBottom(1.f, 1.f, 0.f, 1.f);
		osg::Vec4 colorLeft(0.f, 1.f, 1.f, 1.f);
		osg::Vec4 colorRight(1.f, 0.f, 0.f, 1.f);

		m_debugDisplay->drawLine(near, near + frustumNormals[0], frustumColor, colorNear);
		m_debugDisplay->drawLine(far, far + frustumNormals[1], frustumColor, colorFar);
		m_debugDisplay->drawLine(top, top + frustumNormals[2], frustumColor, colorTop);
		m_debugDisplay->drawLine(bottom, bottom + frustumNormals[3], frustumColor, colorBottom);
		m_debugDisplay->drawLine(left, left + frustumNormals[4], frustumColor, colorLeft);
		m_debugDisplay->drawLine(right, right + frustumNormals[5], frustumColor, colorRight);
	}

	//osg::Matrix rotZ = osg::Matrix::rotate(osg::DegreesToRadians(90.0f), osg::Vec3(0, 0, 1));
	//osg::Matrix rotY = osg::Matrix::rotate(osg::DegreesToRadians(180.0f), osg::Vec3(0, 1, 0));


	//m_debugDisplay->drawLine(vector, vector + osg::Vec3(1, 0, 0) * calculatedCamPose, osg::Vec4(1, 0, 0, 1), osg::Vec4(1, 0, 0, 1));
	//m_debugDisplay->drawLine(vector, vector + osg::Vec3(0, 1, 0) * calculatedCamPose, osg::Vec4(0, 1, 0, 1), osg::Vec4(0, 1, 0, 1));
	//m_debugDisplay->drawLine(vector, vector + osg::Vec3(0, 0, 1) * calculatedCamPose, osg::Vec4(0, 0, 1, 1), osg::Vec4(0, 0, 1, 1));
	
	//osg::Vec3 test = calculatedCamPose.getTrans();
	//std::cout << "calculatedCamPose.getTrans().x(): " << test.x() << std::endl;
	//std::cout << "calculatedCamPose.getTrans().y(): " << test.y() << std::endl;
	//std::cout << "calculatedCamPose.getTrans().z(): " << test.z() << std::endl;

	m_debugDisplay->redraw();
}

void PointCloudDisplay::drawDebug(std::vector<osg::Matrix>& cameraPoses){
	m_debugDisplay->clear();
	for (osg::Matrix& cameraPose : cameraPoses) m_debugDisplay->drawRay(osg::Vec3(0, 0, 0) * cameraPose, osg::Vec3(0, 0, 1) * cameraPose, 0.1f, osg::Vec4(0, 0.2, 0.8, 1.0), osg::Vec4(0, 0, 0, 0));
	m_debugDisplay->redraw();
}
#endif

bool PointCloudDisplay::toggleOctreeDisplay() {
	return m_octreeDisplay->toggleDisplayState();
}

void PointCloudDisplay::setOctreeDisplay(bool isVisible) {
	m_octreeDisplay->setDisplayState(isVisible);
}

bool PointCloudDisplay::toggleTSDFDisplay() {
	return m_tsdfDisplay->toggleDisplayState();
}

void PointCloudDisplay::setTSDFDisplay(bool isVisible) {
	m_tsdfDisplay->setDisplayState(isVisible);
}

bool PointCloudDisplay::toggleDDADisplay() {
	return m_ddaDisplay->toggleDisplayState();
}

void PointCloudDisplay::setDDADisplay(bool isVisible) {
	m_ddaDisplay->setDisplayState(isVisible);
}

//void PointCloudDisplay::drawNormals() {
//	osg::Vec4 colorStart = osg::Vec4(0, 0.5f, 0.75f, 1);
//	osg::Vec4 colorEnd = osg::Vec4(0, 0.6f, 0.6f, 1);
//	for (int i = 0; i < m_PcVertices->size(); i++) {
//		osg::Vec3& vertex = (*m_PcVertices)[i];
//		drawRay(vertex, (*m_PcNormals)[i], 0.1f, colorStart, colorEnd);
//	}
//}

void PointCloudDisplay::drawOctreeVolume(OctreeVolume<float>* octreeVolume, const osg::Vec4& color) {
	//osg::Vec4 color(0, 0.25f, 0.75f, 0.3f);
	//Draw Bounding Box of the octree volume first
	m_octreeDisplay->clear();
	float volExtent = octreeVolume->getExtents();
	m_octreeDisplay->drawBox(octreeVolume->getPosition(), osg::Vec3(0, 1, 0), osg::Vec3(0, 0, 1), osg::Vec3(volExtent, volExtent, volExtent), color);
	drawOctree(octreeVolume->getOctree(), octreeVolume->getPosition(), volExtent, color);
	//drawTSDF(octreeVolume->getOctree(), octreeVolume->getPosition(), volExtent);
	m_octreeDisplay->redraw();
}

void PointCloudDisplay::drawDDA(OctreeVolume<float>* octreeVolume, osg::Vec3f from, osg::Vec3f to, const osg::Vec4& color) {
	m_ddaDisplay->clear();
	Ray ray(from, to - from, 100000.f); //Set distance rather high, so that the ray is definitly traced through the whole volume
	octreeVolume->getOctree().deselect(true);
	octreeVolume->ddaTraverseDebug(ray);
	m_ddaDisplay->drawRay(ray.getOrigin(), ray.getDirection(), ray.getDistance(), color, color);
	drawOctreeSelection(octreeVolume->getOctree(), octreeVolume->getPosition(), octreeVolume->getExtents(), color);
	m_ddaDisplay->redraw();
}

void PointCloudDisplay::drawOctreeSelection(Octree<float>& octree, const osg::Vec3& center, const float extent, const osg::Vec4& color) {
	if (!octree.hasChildren())
	{
		//if (octree.isSelected()) drawEmpty(center, extent * 0.5f, osg::Vec4f(1.0f, 0.0f, 0.0f, 1.0f));
		if (octree.isSelected()) m_ddaDisplay->drawBox(center, osg::Vec3(0, 1, 0), osg::Vec3(0, 0, 1), osg::Vec3(extent, extent, extent), color);
		return;
	}
	float childExtent = extent * .5f;

	int i = 0;
	for (int z = 0; z < 2; z++) {
		float cz = center.z() - childExtent + extent * z;
		for (int y = 0; y < 2; y++) {
			float cy = center.y() - childExtent + extent * y;
			for (int x = 0; x < 2; x++) {
				float cx = center.x() - childExtent + extent * x;
				drawOctreeSelection(*octree.getOctree(i), osg::Vec3(cx, cy, cz), childExtent, color);
				i++;
			}
		}
	}
}

void PointCloudDisplay::drawOctree(Octree<float>& octree, const osg::Vec3& center, const float extent, const osg::Vec4& color) {
	if (!octree.hasChildren()) return;
	m_octreeDisplay->drawEmpty(center, extent, color);
	osg::Vec2 extentsXY(extent, extent);
	m_octreeDisplay->drawQuad(center, osg::Vec3(0, 1, 0), osg::Vec3(0, 0, 1), extentsXY, color);
	m_octreeDisplay->drawQuad(center, osg::Vec3(1, 0, 0), osg::Vec3(0, 0, 1), extentsXY, color);
	m_octreeDisplay->drawQuad(center, osg::Vec3(0, 0, 1), osg::Vec3(0, 1, 0), extentsXY, color);
	float childExtent = extent * .5f;

	int i = 0;
	for (int z = 0; z < 2; z++) {
		float cz = center.z() - childExtent + extent * z;
		for (int y = 0; y < 2; y++) {
			float cy = center.y() - childExtent + extent * y;
			for (int x = 0; x < 2; x++) {
				float cx = center.x() - childExtent + extent * x;
				drawOctree(*octree.getOctree(i), osg::Vec3(cx, cy, cz), childExtent, color);
				i++;
			}
		}
	}
}

void PointCloudDisplay::clearOctreeVolumeDisplay() {
	m_octreeDisplay->clear();
	m_octreeDisplay->redraw();
}

void PointCloudDisplay::drawTSDF(Octree<float>& octree, const osg::Vec3& center, const float extent, const osg::Vec4& colorPositive, const osg::Vec4& colorNegative) {
	if (!octree.hasChildren()) {
		float tsdf = octree.getValue();
		if (tsdf < 0.0f) {
			if (tsdf >= -1.0f) m_tsdfDisplay->drawEmpty(center, extent * 0.5f, osg::Vec4(colorNegative.r() * -tsdf, colorNegative.g() * -tsdf, colorNegative.b() * -tsdf, colorNegative.a()));
		}
		else {
			if (tsdf <= 1.0f) m_tsdfDisplay->drawEmpty(center, extent * 0.5f, osg::Vec4(colorPositive.r() * tsdf, colorPositive.g() * tsdf, colorPositive.b() * tsdf, colorPositive.a()));
		}
		return;
	}
	float childExtent = extent * .5f;

	int i = 0;
	for (int z = 0; z < 2; z++) {
		float cz = center.z() - childExtent + extent * z;
		for (int y = 0; y < 2; y++) {
			float cy = center.y() - childExtent + extent * y;
			for (int x = 0; x < 2; x++) {
				float cx = center.x() - childExtent + extent * x;
				drawTSDF(*octree.getOctree(i), osg::Vec3(cx, cy, cz), childExtent, colorPositive, colorNegative);
				i++;
			}
		}
	}
	m_tsdfDisplay->redraw();
}

void PointCloudDisplay::drawTSDF(OctreeVolume<float>* octreeVolume, const osg::Vec4& colorPositive, const osg::Vec4& colorNegative) {
	m_tsdfDisplay->clear();
	drawTSDF(octreeVolume->getOctree(), octreeVolume->getPosition(), octreeVolume->getExtents(), colorPositive, colorNegative);
}

void PointCloudDisplay::clearTSDFDisplay() {
	m_tsdfDisplay->clear();
	m_tsdfDisplay->redraw();
}

void PointCloudDisplay::clearDDADisplay() {
	m_ddaDisplay->clear();
	m_ddaDisplay->redraw();
}