#pragma once

struct OctreeData {
	unsigned char _r, _g, _b;

	OctreeData() : _r{0u}, _g{0u}, _b{0u} {}
	OctreeData(unsigned char r, unsigned char g, unsigned char b) : _r{r}, _g{g}, _b{b} {};
};