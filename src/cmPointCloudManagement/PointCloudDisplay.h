#pragma once 

#include <osg/MatrixTransform>

#include "OctreeVolume.cpp"
#include "Octree.cpp"
#include "DisplayTarget.h"

#include "ViewFrustum.h"

#define DEBUG_DISPLAY

class PointCloudDisplay {
private:
	DisplayTarget* m_octreeDisplay;
	DisplayTarget* m_tsdfDisplay;
	DisplayTarget* m_ddaDisplay;
#ifdef DEBUG_DISPLAY
	DisplayTarget* m_debugDisplay;
#endif
public:
	PointCloudDisplay(osg::ref_ptr<osg::MatrixTransform> matTrans);
	~PointCloudDisplay();

#ifdef DEBUG_DISPLAY
	void drawDebug(const osg::Matrix trackerMatrix, std::vector<std::pair<osg::Matrix, ViewFrustum>>& cameraPoses, osg::Matrix patternToTrackerTrans, osg::Matrix patternToTrackerRot);
	void drawDebug(std::vector<osg::Matrix>& cameraPoses);
#endif

	bool toggleOctreeDisplay();
	bool toggleTSDFDisplay();
	bool toggleDDADisplay();
	void setOctreeDisplay(bool displayState);
	void setTSDFDisplay(bool displayState);
	void setDDADisplay(bool displayState);

	//void drawNormals();
	void drawDDA(OctreeVolume<float>* octreeVolume, osg::Vec3f from, osg::Vec3f to, const osg::Vec4& color);
	void drawOctreeVolume(OctreeVolume<float>* octreeVolume, const osg::Vec4& color);
	void drawOctree(Octree<float>& octree, const osg::Vec3& center, const float extent, const osg::Vec4& color);
	void drawOctreeSelection(Octree<float>& octree, const osg::Vec3& center, const float extent, const osg::Vec4& color);
	void clearOctreeVolumeDisplay();
	void drawTSDF(Octree<float>& octree, const osg::Vec3& center, const float extent, const osg::Vec4& colorPositive, const osg::Vec4& colorNegative);
	void drawTSDF(OctreeVolume<float>* octreeVolume, const osg::Vec4& colorPositive, const osg::Vec4& colorNegative);
	void clearTSDFDisplay();
	void clearDDADisplay();
};