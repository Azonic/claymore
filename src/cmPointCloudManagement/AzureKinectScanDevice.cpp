#include "AzureKinectScanDevice.h"
#include <iostream>

AzureKinectScanDevice::AzureKinectScanDevice(k4a_device_t device, k4a_device_configuration_t& config, PoseDriver* poseDriver, const unsigned int& pointCloudWidth, const unsigned int& pointCloudHeight, cv::Mat* cameraMatrix, cv::Mat* distortionCoefficients) :
	ScanDevice(poseDriver, "", "Azure Kinect", cameraMatrix, distortionCoefficients, 0, 0, 0, 0, 0, 0.001f), m_k4aDevice(device), m_k4aConfig(config), /*m_k4aCalibrationTable(nullptr)*/
	m_k4aTransformation(nullptr), m_k4aCapture(nullptr), m_k4aColor(nullptr), m_k4aDepth(nullptr), m_k4aDepthTransformed(nullptr), m_k4aPointcloud(nullptr), m_k4aTexCoords(nullptr)
{ 
	reloadColorFormat();
	m_depthFormat = DepthData::DepthFormat::Z16; // TODO 3: Rethink the format structure; How do we determine the format of depth and color? Maybe it would make more sense to hide these settings behind the various APIs
	size_t serial_num_size = NULL;
	if (k4a_buffer_result_t::K4A_BUFFER_RESULT_TOO_SMALL == k4a_device_get_serialnum(device, NULL, &serial_num_size)) {
		char* serial = new char[serial_num_size];
		if (k4a_buffer_result_t::K4A_BUFFER_RESULT_SUCCEEDED == k4a_device_get_serialnum(device, serial, &serial_num_size)) m_serial = serial;
		delete[] serial;
	}

	switch (config.color_resolution) {
	case k4a_color_resolution_t::K4A_COLOR_RESOLUTION_OFF:
		m_colorWidth = 0u;
		m_colorHeight = 0u;
		break;
	case k4a_color_resolution_t::K4A_COLOR_RESOLUTION_720P:
		m_colorWidth = 1280u;
		m_colorHeight = 720u;
		break;
	case k4a_color_resolution_t::K4A_COLOR_RESOLUTION_1080P:
		m_colorWidth = 1920u;
		m_colorHeight = 1080u;
		break;
	case k4a_color_resolution_t::K4A_COLOR_RESOLUTION_1440P:
		m_colorWidth = 2560u;
		m_colorHeight = 1440u;
		break;
	case k4a_color_resolution_t::K4A_COLOR_RESOLUTION_1536P:
		m_colorWidth = 2048u;
		m_colorHeight = 1536u;
		break;
	case k4a_color_resolution_t::K4A_COLOR_RESOLUTION_2160P:
		m_colorWidth = 3840u;
		m_colorHeight = 2160u;
		break;
	case k4a_color_resolution_t::K4A_COLOR_RESOLUTION_3072P:
		m_colorWidth = 4096u;
		m_colorHeight = 3072u;
		break;
	}
	m_pointCloudWidth = std::min(pointCloudWidth, m_colorWidth);
	m_pointCloudHeight = std::min(pointCloudHeight, m_colorHeight);

	switch (config.camera_fps) {
	case k4a_fps_t::K4A_FRAMES_PER_SECOND_5:
		m_fps = 5u;
		break;
	case k4a_fps_t::K4A_FRAMES_PER_SECOND_15:
		m_fps = 15u;
		break;
	case k4a_fps_t::K4A_FRAMES_PER_SECOND_30:
		m_fps = 30u;
		break;
	}

	k4a_calibration_t calibration;
	k4a_device_get_calibration(device, config.depth_mode, config.color_resolution, &calibration);
	m_k4aTransformation = k4a_transformation_create(&calibration);
	//generateCalibrationTable();
	generateTextureCoordinates(calibration);
}

AzureKinectScanDevice::~AzureKinectScanDevice() {
	if (m_isRunning.load()) stop();
	releaseScan();
	clearTextureCoordinates();
	if (m_k4aTransformation) {
		k4a_transformation_destroy(m_k4aTransformation);
		m_k4aTransformation = nullptr;
	}
	//if (m_k4aCalibrationTable) k4a_image_release(m_k4aCalibrationTable);
	k4a_device_close(m_k4aDevice);
}

ScanDevice::StartDeviceResult AzureKinectScanDevice::start() {
	if (!attemptToStart()) return StartDeviceResult::ERROR_IS_RUNNING;
	
	if (k4a_result_t::K4A_RESULT_FAILED == k4a_device_start_cameras(m_k4aDevice, &m_k4aConfig)) return StartDeviceResult::FAILURE;
	
	m_systemClockOrigin = std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::system_clock::now().time_since_epoch()).count();
	if (k4a_wait_result_t::K4A_WAIT_RESULT_FAILED == k4a_device_get_capture(m_k4aDevice, &m_k4aCapture, 10000)) return StartDeviceResult::FAILURE;
	m_systemClockOrigin += (std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::system_clock::now().time_since_epoch()).count() - m_systemClockOrigin) / 2ULL; 
	
	m_k4aColor = k4a_capture_get_color_image(m_k4aCapture);
	m_deviceClockOrigin = k4a_image_get_device_timestamp_usec(m_k4aColor);

	//m_deviceClockOrigin = 0ull; // The device_start_cameras function does reset the hardware timestamp (observed in the kinect viewer app)
	//m_systemClockOrigin = std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::system_clock::now().time_since_epoch()).count();
	//auto systemClockOrigin = m_systemClockOrigin;
	//if (k4a_result_t::K4A_RESULT_FAILED == k4a_device_start_cameras(m_k4aDevice, &m_k4aConfig)) return StartDeviceResult::FAILURE;

	//// Interpolate the system timestamp before requesting the device timestamp and the one after the pipeline is started. 
	//// Reason: we do not know the exact point in time when the hardware timestamp is set to zero.
	//m_systemClockOrigin += (std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::system_clock::now().time_since_epoch()).count() - m_systemClockOrigin) / 2ULL; 
	
	std::cout << "Kinect SystemClockOrigin: " << m_systemClockOrigin << std::endl;
	//std::cout << "Kinect start time: " << (std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::system_clock::now().time_since_epoch()).count() - m_systemClockOrigin) << std::endl;
	
	return StartDeviceResult::SUCCESS;
}

ScanDevice::StopDeviceResult AzureKinectScanDevice::stop() {
	if (!isRunning()) return StopDeviceResult::DEVICE_IS_NOT_RUNNING;
	k4a_device_stop_cameras(m_k4aDevice);
	m_isRunning.store(false);
	return StopDeviceResult::SUCCESS;
}

ScanDevice::ScanResult AzureKinectScanDevice::scan(const unsigned int timeout_ms) { //TODO 3: Look up whether the various systems react differently to various values, i.e. timeout = 0 || timeout < 0 (-1 = K4A_WAIT_INFIITE)
	{
		ScanResult scanResult = scanStart();
		if (scanResult != ScanResult::SUCCESS) return scanResult;
	}

	// ### Capturing ###
	switch (k4a_device_get_capture(m_k4aDevice, &m_k4aCapture, timeout_ms)) {
	case k4a_wait_result_t::K4A_WAIT_RESULT_SUCCEEDED:
		break;
	case k4a_wait_result_t::K4A_WAIT_RESULT_TIMEOUT:
		return scanEnd(ScanResult::TIMEOUT, false);
	case k4a_wait_result_t::K4A_WAIT_RESULT_FAILED:
		return scanEnd(ScanResult::FAILURE, false);
	default:
		return scanEnd(ScanResult::FAILURE, false);
	}
	
	// ### Color ###
	m_k4aColor = k4a_capture_get_color_image(m_k4aCapture);
	m_deviceTimestamp = k4a_image_get_device_timestamp_usec(m_k4aColor);
	m_systemTimestamp = deviceToSystemClock(m_deviceTimestamp);
	//std::cout << "k4a color image Timestamp: " << m_systemTimestamp << std::endl;
	
	unsigned char* colorData = k4a_image_get_buffer(m_k4aColor);
	switch (m_colorFormat) {
	case ColorData::ColorFormat::BGRA8:
		m_colorData = new ColorData_BGRA8(colorData, k4a_image_get_size(m_k4aColor), k4a_image_get_width_pixels(m_k4aColor), k4a_image_get_height_pixels(m_k4aColor));
		break;
	default:
		return scanEnd(ScanResult::UNSUPPORTED_PROFILE);
	}

	// ### Depth ###
	switch (m_depthFormat) {
	case DepthData::DepthFormat::Z16:
		break;
	default:
		return scanEnd(ScanResult::UNSUPPORTED_PROFILE);
	}
	m_k4aDepth = k4a_capture_get_depth_image(m_k4aCapture);
	auto deviceTimestamp = k4a_image_get_device_timestamp_usec(m_k4aDepth);
	auto systemTimestamp = deviceToSystemClock(deviceTimestamp);
	//std::cout << "k4a depth image Timestamp: " << systemTimestamp << std::endl;

	// ### Depth Transform ###
	// Depth Sensor has a fisheye lens. The resulting image has a different resolution than the color camera, so the depth and color data do not match without the proper transformation.
	if (m_k4aDepthTransformed) {
		k4a_image_release(m_k4aDepthTransformed);
		m_k4aDepthTransformed = nullptr;
	}
	int colorWidth = k4a_image_get_width_pixels(m_k4aColor);
	int colorHeight = k4a_image_get_height_pixels(m_k4aColor);
	if (K4A_RESULT_SUCCEEDED != k4a_image_create(K4A_IMAGE_FORMAT_DEPTH16, colorWidth, colorHeight, colorWidth * (int)sizeof(unsigned short), &m_k4aDepthTransformed)) {
		return scanEnd(ScanResult::ALLOCATION_ERROR);
	}
	if (K4A_RESULT_SUCCEEDED != k4a_transformation_depth_image_to_color_camera(m_k4aTransformation, m_k4aDepth, m_k4aDepthTransformed)) {
		return scanEnd(ScanResult::TRANSFORMATION_ERROR);
	}
	unsigned char* depthData = k4a_image_get_buffer(m_k4aDepthTransformed);
	switch (m_depthFormat) {
	case DepthData::DepthFormat::Z16:
		m_depthData = new DepthData_Z16(depthData, k4a_image_get_size(m_k4aDepthTransformed), k4a_image_get_width_pixels(m_k4aDepthTransformed), k4a_image_get_height_pixels(m_k4aDepthTransformed), m_depthScale);
		break;
	default:
		return scanEnd(ScanResult::UNSUPPORTED_PROFILE);
	}

	return scanEnd(ScanResult::SUCCESS);
}

ScanDevice::PointcloudCalculationResult AzureKinectScanDevice::calculatePointcloud() {
	if (m_k4aDepthTransformed == nullptr || m_k4aColor == nullptr) return PointcloudCalculationResult::MISSING_SCAN_DATA;

	int colorWidth = k4a_image_get_width_pixels(m_k4aColor);
	int colorHeight = k4a_image_get_height_pixels(m_k4aColor);
	if (m_k4aPointcloud) {
		k4a_image_release(m_k4aPointcloud);
		m_k4aPointcloud = nullptr;
	}
	if (K4A_RESULT_SUCCEEDED != k4a_image_create(K4A_IMAGE_FORMAT_CUSTOM, colorWidth, colorHeight, colorWidth * 3 * (int)sizeof(int16_t), &m_k4aPointcloud)) return PointcloudCalculationResult::ALLOCATION_ERROR;
	if (k4a_result_t::K4A_RESULT_SUCCEEDED != k4a_transformation_depth_image_to_point_cloud(m_k4aTransformation, m_k4aDepthTransformed, K4A_CALIBRATION_TYPE_COLOR, m_k4aPointcloud)) return PointcloudCalculationResult::TRANSFORMATION_ERROR;

	if (m_pointcloudData) delete m_pointcloudData;
	m_pointcloudData = new PointcloudData_XYZ16_UV32F(k4a_image_get_buffer(m_k4aPointcloud), k4a_image_get_buffer(m_k4aTexCoords), colorWidth, colorHeight, m_depthScale); //NOTE: The pointcloud is stored as vectors with xyz as short. Thus the depthScale is necessary to convert the values back to float point when reading from the pointCloudData

	return PointcloudCalculationResult::SUCCESS;
}

/*ScanDevice::PointcloudCalculationResult AzureKinectScanDevice::calculatePointcloud() {
	if (m_k4aDepth == nullptr) return PointcloudCalculationResult::MISSING_SCAN_DATA;
	unsigned int width = k4a_image_get_width_pixels(m_k4aDepth);
	unsigned int height = k4a_image_get_height_pixels(m_k4aDepth);
	unsigned short* depth_data = (unsigned short*)(void*)k4a_image_get_buffer(m_k4aDepth);
	k4a_float2_t* calibration_table_data = (k4a_float2_t*)(void*)k4a_image_get_buffer(m_k4aCalibrationTable);
	
	if (m_k4aPointcloud) k4a_image_release(m_k4aPointcloud);
	k4a_calibration_t calibration;
	if (k4a_result_t::K4A_RESULT_SUCCEEDED != k4a_device_get_calibration(m_k4aDevice, m_k4aConfig.depth_mode, m_k4aConfig.color_resolution, &calibration)) return PointcloudCalculationResult::CALIBRATION_ERROR;

	// Note: For this we could alternatively just allocate a float3[] ourself, however, this way we stick with the kinect framework in case a followup operation would require a k4a_image_t
	if (k4a_result_t::K4A_RESULT_SUCCEEDED != k4a_image_create(K4A_IMAGE_FORMAT_CUSTOM,
		calibration.depth_camera_calibration.resolution_width,
		calibration.depth_camera_calibration.resolution_height,
		calibration.depth_camera_calibration.resolution_width * (int)sizeof(k4a_float3_t),
		&m_k4aPointcloud
	)) return PointcloudCalculationResult::ALLOCATION_ERROR;

	k4a_float3_t* point_cloud_data = (k4a_float3_t*)(void*)k4a_image_get_buffer(m_k4aPointcloud);
	for (int i = 0; i < width * height; i++) {
		point_cloud_data[i].xyz.x = calibration_table_data[i].xy.x * (float)depth_data[i];
		point_cloud_data[i].xyz.y = calibration_table_data[i].xy.y * (float)depth_data[i];
		point_cloud_data[i].xyz.z = (float)depth_data[i];
	}

	return PointcloudCalculationResult::SUCCESS;
}*/

static bool point_cloud_depth_to_color(k4a_transformation_t transformation_handle, const k4a_image_t depth_image, const k4a_image_t color_image) {
	// transform color image into depth camera geometry
	int color_image_width_pixels = k4a_image_get_width_pixels(color_image);
	int color_image_height_pixels = k4a_image_get_height_pixels(color_image);
	k4a_image_t transformed_depth_image = NULL;
	if (K4A_RESULT_SUCCEEDED != k4a_image_create(K4A_IMAGE_FORMAT_DEPTH16,
		color_image_width_pixels,
		color_image_height_pixels,
		color_image_width_pixels * (int)sizeof(uint16_t),
		&transformed_depth_image))
	{
		return false;
	}

	k4a_image_t point_cloud_image = NULL;
	if (K4A_RESULT_SUCCEEDED != k4a_image_create(K4A_IMAGE_FORMAT_CUSTOM,
		color_image_width_pixels,
		color_image_height_pixels,
		color_image_width_pixels * 3 * (int)sizeof(int16_t),
		&point_cloud_image))
	{
		return false;
	}

	if (K4A_RESULT_SUCCEEDED !=
		k4a_transformation_depth_image_to_color_camera(transformation_handle, depth_image, transformed_depth_image))
	{
		return false;
	}

	if (K4A_RESULT_SUCCEEDED != k4a_transformation_depth_image_to_point_cloud(transformation_handle,
		transformed_depth_image,
		K4A_CALIBRATION_TYPE_COLOR,
		point_cloud_image))
	{
		return false;
	}

	k4a_image_release(transformed_depth_image);
	k4a_image_release(point_cloud_image);

	return true;
}

void AzureKinectScanDevice::releaseScan() {
	if (m_k4aColor) {
		k4a_image_release(m_k4aColor);
		m_k4aColor = nullptr;
	}
	if (m_k4aDepth) {
		k4a_image_release(m_k4aDepth);
		m_k4aDepth = nullptr;
	}
	if (m_k4aDepthTransformed) {
		k4a_image_release(m_k4aDepthTransformed);
		m_k4aDepthTransformed = nullptr;
	}
	if (m_k4aPointcloud) {
		k4a_image_release(m_k4aPointcloud);
		m_k4aPointcloud = nullptr;
	}
	if (m_k4aCapture) {
		k4a_capture_release(m_k4aCapture);
		m_k4aCapture = nullptr;
	}
	ScanDevice::releaseScan();
}

void AzureKinectScanDevice::loadConfig(k4a_device_configuration_t& config) {
	bool running = isRunning();
	if (running) stop();
	m_k4aConfig = config;
	reloadColorFormat();
	if (running) start();
}

inline void AzureKinectScanDevice::reloadColorFormat() {
	switch (m_k4aConfig.color_format) {
	case k4a_image_format_t::K4A_IMAGE_FORMAT_COLOR_BGRA32:
		m_colorFormat = ColorData::ColorFormat::BGRA8;
		break;
	default:
		m_colorFormat = ColorData::ColorFormat::UNSUPPORTED;
	}
}

void AzureKinectScanDevice::generateTextureCoordinates(const k4a_calibration_t& calibration) {
	clearTextureCoordinates();
	int colorWidth = calibration.color_camera_calibration.resolution_width;
	int colorHeight = calibration.color_camera_calibration.resolution_height;
	if (k4a_result_t::K4A_RESULT_SUCCEEDED != k4a_image_create(K4A_IMAGE_FORMAT_CUSTOM, colorWidth, colorHeight, colorWidth * (int)sizeof(k4a_float2_t), &m_k4aTexCoords)) { // 2 x float: uv
		return;
	}
	calculateTextureCoordinates(colorWidth, colorHeight);
}

void AzureKinectScanDevice::calculateTextureCoordinates(const int width, const int height) {
	const float halfTexelWidth = .5f / width;
	const float halfTexelHeight = .5f / height;
	k4a_float2_t* uv_data = (k4a_float2_t*)(void*)k4a_image_get_buffer(m_k4aTexCoords);
	for (int y = 0, index = 0; y < height; y++) {
		for (int x = 0; x < width; x++, index++) {
			uv_data[index].xy.x = (float)x / width + halfTexelWidth;
			uv_data[index].xy.y = (float)y / height + halfTexelHeight;
		}
	}
}

void AzureKinectScanDevice::clearTextureCoordinates() {
	if (m_k4aTexCoords) {
		k4a_image_release(m_k4aTexCoords);
		m_k4aTexCoords = nullptr;
	}
}

//void AzureKinectScanDevice::generateCalibrationTable() {
//	k4a_calibration_t calibration;
//	k4a_device_get_calibration(m_k4aDevice, m_k4aConfig.depth_mode, m_k4aConfig.color_resolution, &calibration);
//	k4a_image_create(K4A_IMAGE_FORMAT_CUSTOM, 
//		calibration.depth_camera_calibration.resolution_width,
//		calibration.depth_camera_calibration.resolution_height,
//		calibration.depth_camera_calibration.resolution_width * (int)sizeof(k4a_float2_t),
//		&m_k4aCalibrationTable
//	);
//
//	k4a_float2_t* table_data = (k4a_float2_t*)(void*)k4a_image_get_buffer(m_k4aCalibrationTable);
//
//	int width = calibration.depth_camera_calibration.resolution_width;
//	int height = calibration.depth_camera_calibration.resolution_height;
//
//	k4a_float2_t p;
//	k4a_float3_t ray;
//	int valid;
//
//	for (int y = 0, idx = 0; y < height; y++) {
//		p.xy.y = (float)y;
//		for (int x = 0; x < width; x++, idx++) {
//			p.xy.x = (float)x;
//
//			k4a_calibration_2d_to_3d(&calibration, &p, 1.f, 
//				K4A_CALIBRATION_TYPE_DEPTH, 
//				K4A_CALIBRATION_TYPE_DEPTH, 
//				&ray, &valid
//			);
//
//			if (valid) {
//				table_data[idx].xy.x = ray.xyz.x;
//				table_data[idx].xy.y = ray.xyz.y;
//			} else {
//				table_data[idx].xy.x = 0.f;
//				table_data[idx].xy.y = 0.f;
//			}
//		}
//	}
//}
