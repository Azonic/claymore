#include "RealSenseScanDevice.h"

RealSenseScanDevice::RealSenseScanDevice(rs2::device& device, rs2::context& context, const unsigned int& colorWidth, const unsigned int& colorHeight, const unsigned int& depthWidth, const unsigned int& depthHeight, const unsigned int& fps, rs2_format colorFormat, rs2_format depthFormat, PoseDriver* poseDriver, const unsigned int& pointCloudWidth, const unsigned int& pointCloudHeight, cv::Mat* cameraMatrix, cv::Mat* distortionCoefficients) :
	ScanDevice(poseDriver, device.get_info(RS2_CAMERA_INFO_SERIAL_NUMBER), device.get_info(RS2_CAMERA_INFO_NAME), cameraMatrix, distortionCoefficients, colorWidth, colorHeight, pointCloudWidth, pointCloudHeight, fps), m_rsDevice(device), m_rsPc(), m_scanDataAvailable(false), m_rsFrames(), m_rsPoints()
{
	m_rsPipe = rs2::pipeline(context);
	for (auto&& sensor : device.query_sensors()) {
		if (rs2::depth_sensor depth_sensor = sensor.as<rs2::depth_sensor>()) {
			m_depthScale = depth_sensor.get_depth_scale();
			break;
		}
	}
		
	m_rsConfig.enable_stream(RS2_STREAM_COLOR, -1, colorWidth, colorHeight, colorFormat, fps);
	m_rsConfig.enable_stream(RS2_STREAM_DEPTH, -1, depthWidth, depthHeight, depthFormat, fps);
	m_rsConfig.enable_device(m_serial);

	reloadColorFormat(colorFormat);
	reloadDepthFormat(depthFormat);

	//Create filters
	//Decimation filter
	m_rs_dec_filter = new rs2::decimation_filter();
	m_rs_dec_filter->set_option(RS2_OPTION_FILTER_MAGNITUDE, 3);		// 1 - 5

	//Spatial filter													
	m_rs_spat_filter = new rs2::spatial_filter();
	m_rs_spat_filter->set_option(RS2_OPTION_FILTER_MAGNITUDE, 4); 		// 1	- 5
	m_rs_spat_filter->set_option(RS2_OPTION_FILTER_SMOOTH_ALPHA, 0.25f); // 0.25	- 1
	m_rs_spat_filter->set_option(RS2_OPTION_FILTER_SMOOTH_DELTA, 50);	// 1	- 50
	m_rs_spat_filter->set_option(RS2_OPTION_HOLES_FILL, 0);				// [0]: Disabled, [1]: 2px, [2]: 4px, [3]: 8px [4]: 16px [5]: Unlimited
}

RealSenseScanDevice::~RealSenseScanDevice() {
	if (m_isRunning.load()) stop();
}

ScanDevice::StartDeviceResult RealSenseScanDevice::start() {
	if (!attemptToStart()) return StartDeviceResult::ERROR_IS_RUNNING;

	m_rsPipe.start(m_rsConfig);
	//Set timestamp origins
	{
		// Define the origin of both clocks (System and Device) by requesting a timestamp of each system at nearly the same time
		uint8_t data[]{ 0x14, 00, 0xab, 0xcd, 01, 00, 00, 00, 0x3c, 0x61, 01, 00, 0x40, 0x61, 01, 00, 00, 00, 00, 00, 00, 00, 00, 00 };
		std::vector<uint8_t> command(data, data + sizeof(data));

		rs2::debug_protocol device = m_rsDevice.as<rs2::debug_protocol>();

		m_systemClockOrigin = std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::system_clock::now().time_since_epoch()).count();
		auto res = device.send_and_receive_raw_data(command); // USB latency should occurr twice here: once when sending the request command to the RS and once when the data will be send back from the RS to the host. Because of that the assumption is, that the timestamp will be read in the middle of the time needed for the whole operation.
		unsigned int* ptr = reinterpret_cast<unsigned int*>(res.data());
		m_deviceClockOrigin = ptr[1]; // usec
		// Interpolate the system timestamp before requesting the device timestamp and the one after the data is received.
		m_systemClockOrigin += (std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::system_clock::now().time_since_epoch()).count() - m_systemClockOrigin) / 2ULL;
	}

	return StartDeviceResult::SUCCESS;
}

ScanDevice::StopDeviceResult RealSenseScanDevice::stop() {
	if (!isRunning()) return StopDeviceResult::DEVICE_IS_NOT_RUNNING;

	m_rsPipe.stop();

	return StopDeviceResult::SUCCESS;
}

ScanDevice::ScanResult RealSenseScanDevice::scan(const unsigned int timeout_ms) {
	{ 
		ScanResult scanResult = scanStart();
		if (scanResult != ScanResult::SUCCESS) return scanResult;
	}

	if (!m_rsPipe.try_wait_for_frames(&m_rsFrames, timeout_ms)) return scanEnd(ScanResult::TIMEOUT, false);	
	m_scanDataAvailable = true;
	
	rs2::align align(RS2_STREAM_COLOR);
	m_rsFrames = align.process(m_rsFrames);
	m_rsFrames.keep();

	// ### Color ###
	rs2::video_frame colorFrame = m_rsFrames.get_color_frame();
	size_t bytes = colorFrame.get_data_size();
	unsigned int width = colorFrame.get_width();
	unsigned int height = colorFrame.get_height();

	switch (m_colorFormat) {
	case ColorData::ColorFormat::BGRA8:
		m_colorData = new ColorData_BGRA8(colorFrame.get_data(), bytes, width, height);
		break;
	case ColorData::ColorFormat::BGR8:
		m_colorData = new ColorData_BGR8(colorFrame.get_data(), bytes, width, height);
		break;
	case ColorData::ColorFormat::RGBA8:
		m_colorData = new ColorData_RGBA8(colorFrame.get_data(), bytes, width, height);
		break;
	case ColorData::ColorFormat::RGB8:
		m_colorData = new ColorData_RGB8(colorFrame.get_data(), bytes, width, height);
		break;
	default:
		return scanEnd(ScanResult::UNSUPPORTED_PROFILE);
	}

	// Test for outputting the color values to the console
	//for (int i = 0; i < width * height; i++) {
	//	std::cout << "r: " << m_colorData->r(i) << " g: " << m_colorData->g(i) << " b: " << m_colorData->b(i) << " a: " << m_colorData->a(i) << std::endl;
	//}

	// ### Depth ###
	rs2::depth_frame depthFrame = m_rsFrames.get_depth_frame();
	bytes = depthFrame.get_data_size();
	width = depthFrame.get_width();
	height = depthFrame.get_height();

	//Process filter
	//depthFrame = m_rs_dec_filter->process(depthFrame); //Must be applied before a transformation to disparity is done
	//depthFrame = m_rs_spat_filter->process(depthFrame);

	if (depthFrame.supports_frame_metadata(rs2_frame_metadata_value::RS2_FRAME_METADATA_SENSOR_TIMESTAMP)) {
		m_deviceTimestamp = depthFrame.get_frame_metadata(rs2_frame_metadata_value::RS2_FRAME_METADATA_SENSOR_TIMESTAMP); // in usec from device clock; thus conversion to system clock is needed
		m_systemTimestamp = deviceToSystemClock(m_deviceTimestamp);
	} else {
		return scanEnd(ScanResult::TIMESTAMP_ERROR);
	}

	switch (m_depthFormat) {
	case DepthData::DepthFormat::Z16:
		m_depthData = new DepthData_Z16(depthFrame.get_data(), bytes, width, height, m_depthScale);
		break;
	default:
		return scanEnd(ScanResult::UNSUPPORTED_PROFILE);
	}
		
	return scanEnd(ScanResult::SUCCESS);
}

ScanDevice::PointcloudCalculationResult RealSenseScanDevice::calculatePointcloud() {
	if (!m_scanDataAvailable) return PointcloudCalculationResult::MISSING_SCAN_DATA;
	const rs2::video_frame& color = m_rsFrames.get_color_frame();
	m_rsPc.map_to(color);
	m_rsPoints = m_rsPc.calculate(m_rsFrames.get_depth_frame());
	m_rsPoints.keep();
	
	if (m_pointcloudData) delete m_pointcloudData;
	m_pointcloudData = new PointcloudData_XYZ32F_UV32F(m_rsPoints.get_vertices(), m_rsPoints.get_texture_coordinates(), color.get_width(), color.get_height(), 1.f);

	return PointcloudCalculationResult::SUCCESS;
}

void RealSenseScanDevice::releaseScan() {
	if (m_scanDataAvailable) {
		m_rsFrames = rs2::frameset();
		m_rsPoints = rs2::points();
		m_scanDataAvailable = false;
	}
	ScanDevice::releaseScan();
}

void RealSenseScanDevice::loadConfig(rs2::config& config, rs2_format colorFormat, rs2_format depthFormat) {
	bool running = isRunning();
	if (running) stop();
	m_rsConfig = config;
	reloadColorFormat(colorFormat);
	reloadDepthFormat(depthFormat);
	if (running) start();
}

inline void RealSenseScanDevice::reloadColorFormat(rs2_format colorFormat) {
	switch (colorFormat) {
	case rs2_format::RS2_FORMAT_BGRA8:
		m_colorFormat = ColorData::ColorFormat::BGRA8;
		break;
	case rs2_format::RS2_FORMAT_BGR8:
		m_colorFormat = ColorData::ColorFormat::BGR8;
		break;
	case rs2_format::RS2_FORMAT_RGBA8:
		m_colorFormat = ColorData::ColorFormat::RGBA8;
		break;
	case rs2_format::RS2_FORMAT_RGB8:
		m_colorFormat = ColorData::ColorFormat::RGB8;
		break;
	default:
		m_colorFormat = ColorData::ColorFormat::UNSUPPORTED;
	}
}

inline void RealSenseScanDevice::reloadDepthFormat(rs2_format depthFormat) {
	switch (depthFormat) {
	case rs2_format::RS2_FORMAT_Z16:
		m_depthFormat = DepthData::DepthFormat::Z16;
		break;
	default:
		m_depthFormat = DepthData::DepthFormat::UNSUPPORTED;
	}
}