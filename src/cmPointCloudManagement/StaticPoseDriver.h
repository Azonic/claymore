#include "PoseDriver.h"

/**
 * @brief Provides a static pose for a ScanDevice in 3D space.
*/
class StaticPoseDriver : public PoseDriver {
private:
	/**
	 * @brief Pose matrix for the associated device.
	*/
	osg::Matrix m_pose;
	/**
	 * @brief Matrix to transform coordinates from local sensor space to global space.
	*/
	osg::Matrix m_transform;
public:
	/**
	 * @brief Creates a new StaticPoseDriver with a given pose to be associated with the device.
	 * @param pose defines the static pose of the device.
	 * @param sensorToDriver transforms coordinates from the sensor's to the driver's coordinate system. Necessary to counter the offset of the sensor and the tracking device's origin.
	 * @return 
	*/
	StaticPoseDriver(const osg::Matrix& pose, const osg::Matrix& sensorToDriver);
	/**
	* @brief Deconstructs the StaticPoseDriver.
	*/
	~StaticPoseDriver();
	/**
	 * @brief Retrieve the transform matrix at a given timestamp.
	 * @param timestamp_usec system timestamp in microseconds corresponding to the desired transform.
	 * @return Matrix to transform coordinates from local sensor space to global space.
	*/
	virtual osg::Matrix getTransform(unsigned long long timestamp_usec) override;
	/**
	 * @brief Retrieve the pose matrix at a given timestamp.
	 * @param timestamp_usec system timestamp in microseconds corresponding to the desired pose.
	 * @return Pose matrix of the associated tracking device.
	*/
	virtual osg::Matrix getPose(unsigned long long timestamp_usec) override;

	void updateTransform(const osg::Matrix& sensorToDriver);
	
	void updatePose(const osg::Matrix& pose, const osg::Matrix& sensorToDriver);
};