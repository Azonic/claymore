#pragma once

#include <osg/Matrix>
#include <osg/Vec2f>

struct ImageData {
	// Reference points in pixel coordinates
	osg::Vec2f _imageSamplePoint;
	osg::Vec2f _imageCenter;
	// Image size
	unsigned int _width;
	unsigned int _height;
	float _depthScale;
	std::vector<unsigned char>* _colorBuffer;
	std::vector<unsigned char>* _depthBuffer;
	osg::Matrix _cameraTransform;
};

//TODO 2: width and height are redundant, because the decoded image knows its rows and cols