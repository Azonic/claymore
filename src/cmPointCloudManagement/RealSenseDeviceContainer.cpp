#include "RealSenseDeviceContainer.h"

#include "defines.h"

#include "RealSenseUtils.h"

//TODO: Remove:
#include <iostream>

// Helper struct per pipeline
struct view_port
{
	std::map<int, rs2::frame> frames_per_stream;
	rs2::colorizer colorize_frame;
	//texture tex;
	rs2::pipeline pipe;
	rs2::pipeline_profile profile;
};

RealSenseDeviceContainer::RealSenseDeviceContainer()
{
	_devices = new std::map<std::string, view_port>;
}

void RealSenseDeviceContainer::enableDevice(rs2::device dev, unsigned int width, unsigned int height, unsigned int fps)
{
	std::string serial_number(dev.get_info(RS2_CAMERA_INFO_SERIAL_NUMBER));
	std::lock_guard<std::mutex> lock(_mutex);

	if (_devices->find(serial_number) != _devices->end())
	{
		return; //already in
	}

	// Ignoring platform cameras (web cams, etc..)
	if (platform_camera_name == dev.get_info(RS2_CAMERA_INFO_NAME))
	{
		return;
	}
	// Create a pipeline from the given device
	rs2::pipeline pipeline;
	rs2::config config;
	RealSenseUtils::setUpConfig(&config, width, height, fps);
	config.enable_device(serial_number);
	// Start the pipeline with the configuration
	rs2::pipeline_profile profile = pipeline.start(config);
	// Hold it internally
	_devices->emplace(serial_number, view_port{ {},{}, pipeline, profile });
	
	std::cout << "_devices->size();" << _devices->size() << std::endl;
	std::cout << "_devices->emplace: " << std::endl;
}

void RealSenseDeviceContainer::removeDevices(const rs2::event_information& info)
{
	std::lock_guard<std::mutex> lock(_mutex);
	// Go over the list of devices and check if it was disconnected
	auto itr = _devices->begin();
	while (itr != _devices->end())
	{
		if (info.was_removed(itr->second.profile.get_device()))
		{
			itr = _devices->erase(itr);
		}
		else
		{
			++itr;
		}
	}
}

size_t RealSenseDeviceContainer::deviceCount()
{
	std::lock_guard<std::mutex> lock(_mutex);
	return _devices->size();
}

int RealSenseDeviceContainer::streamCount()
{
	std::lock_guard<std::mutex> lock(_mutex);
	int count = 0;
	for (auto&& sn_to_dev : *_devices)
	{
		for (auto&& stream : sn_to_dev.second.frames_per_stream)
		{
			if (stream.second)
			{
				count++;
			}
		}
	}
	return count;
}

void RealSenseDeviceContainer::pollFrames()
{
	std::lock_guard<std::mutex> lock(_mutex);
	// Go over all device
	for (auto&& view : *_devices)
	{
		// Ask each pipeline if there are new frames available
		rs2::frameset frameset;
		if (view.second.pipe.poll_for_frames(&frameset))
		{
			for (int i = 0; i < frameset.size(); i++)
			{
				rs2::frame new_frame = frameset[i];
				int stream_id = new_frame.get_profile().unique_id();
				view.second.frames_per_stream[stream_id] = view.second.colorize_frame.process(new_frame); //update view port with the new stream
			}
		}
	}
}

void RealSenseDeviceContainer::renderTextures(int cols, int rows, float view_width, float view_height)
{
	std::lock_guard<std::mutex> lock(_mutex);
	int stream_no = 0;
	for (auto&& view : *_devices)
	{
		// For each device get its frames
		for (auto&& id_to_frame : view.second.frames_per_stream)
		{
			//rect frame_location{ view_width * (stream_no % cols), view_height * (stream_no / cols), view_width, view_height };
			if (rs2::video_frame vid_frame = id_to_frame.second.as<rs2::video_frame>())
			{
				//view.second.tex.render(vid_frame, frame_location);
				stream_no++;
			}
		}
	}
}

//std::map<std::string, view_port> RealSenseDeviceContainer::getDevices()
//{
//}