// ClayMore - Immersive Mesh Modelling --- Copyright (c) 2014-2017 Philipp Ladwig, Jannik Fiedler, Jan Beutgen
#pragma once

#include "ConfigDllExportPointCloudManagement.h"
#include "defines.h"

#include <osg/ref_ptr>
#include <osg/Group>
#include <osg/MatrixTransform>
#include <osg/ImageStream>
#include <osg/Texture2D>
#include "osg/LightModel"

#include <vector>
#include <atomic>
#include <future>
#include <thread>
#include <mutex>

#include "cmUtil/TimedOpenVRMatrix.h"
#include <boost/circular_buffer.hpp>
#include "../cmOpenMeshBinding/OpenMeshGeometry.h"

#include "LiveMesh.h"

#include "PointCloudDisplay.h"

#include <k4a/k4a.h>
#include <k4a/k4a.hpp>

#include <Eigen/Dense>

#include "cmGeometryManagement/GeometryVisualization.h"

#include <opencv2/core/types.hpp>
#include <opencv2/aruco/charuco.hpp>

#include "ColorTypes.h"
#include "ImageData.h"
#include "DepthTypes.h"
#include "PointcloudTypes.h"

#include "ViewFrustum.h"


//Der folgende Block nennt sich "Forward declaration" -> Damit baut der Compiler schneller, weil er die folgende Klassen nicht komplett includieren werden m�ssen,
 // da einfach ein Pointer als Representation ausreicht. Ein C++ Header file ist dazu da, um die Gr��e des Objektes, welches von dieser Klasse erstellt wird
// zu kennen, damit Byte-genau der richtige Speicher allokiert werden kann. Pointer sind immer gleich gro� - deshalb muss man nicht alles inkludieren,
// was dem Compiler viel Arbeit spart, weil er dann den ganzen Code in dem eigentlich includierten File ignorieren kann (nicht "verstehen" muss)
// , und setzt einfach nur ein Verweis (Pointer) an diese stelle -> Pointer sind immer gleich gro� ;) 32bit auf 32bit Systemen (auch x86 gennant)
// und 64bit auf 64bit Systemen (auch x64) genannt

class ConfigReader;
class GeometryManager;
class PointCloudVisualizer;
class RealSenseDeviceContainer;

namespace cv {
	class Mat;
}

namespace rs2 {
	class pointcloud;
	class pipeline;
	class decimation_filter;
	class spatial_filter;
	class temporal_filter;
	class disparity_transform;
	class context;
}

class ScanPipeline;
class ScanDevice;

class imPointCloudManagement_DLL_import_export PointCloudManager
{
	// #### CONSTRUCTOR & DESTRUCTOR ###############
public:
	PointCloudManager(ConfigReader* configReaderIn, osg::ref_ptr<osg::Group> pointCloudGroup);
	~PointCloudManager();

	// PCL PointCloud object stores all points
	//pcl::PointCloud<pcl::PointXYZRGB>::Ptr m_pclPc;

	// Declare pointcloud object, for calculating pointclouds and texture mappings
	rs2::pointcloud* m_rsPc;
	// We want the points object to be persistent so we can display the last cloud when a frame drops

	// Declare RealSense pipeline, encapsulating the actual device and sensors
	rs2::pipeline* m_rsPipe;
	
	// Declare filters
	rs2::decimation_filter* m_rs_dec_filter;  // Decimation - reduces depth frame density
	rs2::spatial_filter* m_rs_spat_filter;    // Spatial    - edge-preserving spatial smoothing
	rs2::temporal_filter* m_rs_temp_filter;   // Temporal   - reduces temporal noise
	
	// Declare disparity transform from depth to disparity and vice versa
	rs2::disparity_transform* m_rs_depth_to_disparity;
	rs2::disparity_transform* m_rs_disparity_to_depth;

	// #### MEMBER VARIABLES ###############
private:

	//std::thread m_telepresenceProcessingThread;
	std::thread m_telepresenceThread;
	std::atomic<bool> m_processing;
	enum class MarkerType {
		CHESSBOARD,
		CHARUCO
	};

	std::thread* m_processingThread;
	std::atomic<bool> m_isProcessing;
	std::atomic<bool> m_closeProcessingThread;
	unsigned long long m_processingThreadStartTime; // in ms
	unsigned long long m_processingThreadMaxDowntime; // in ms

	osg::ref_ptr<osg::Group> m_pointCloudGroup;
	ConfigReader* m_configReader;
	PointCloudVisualizer* m_pointCloudVisualizer;
	RealSenseDeviceContainer* m_realSenseDeviceContainer;
	rs2::context* m_context;
	bool m_useAzureKinectDevice;
	bool m_useRealSenseDevice;
	bool m_useMultipleRealSenseDevices;
	bool m_useAzureTelepresence;
	bool m_useColorAndDepthShooter;
	unsigned int m_realSenseDepthWidth;
	unsigned int m_realSenseDepthHeight;
	unsigned int m_realSenseFPS;
	k4a_depth_mode_t m_azureKinectDepthMode;
	k4a_fps_t m_azureKinectFPS;
	unsigned int m_pointCloudWidth;
	unsigned int m_pointCloudHeight;

	int m_maxIterationForSubPixel = 30;
	int m_epsilonForSubPixel = 0.015;

	std::atomic<bool>* m_timedCircularTrackerMatrixBuffer_Lock;
	boost::circular_buffer<TimedOpenVRMatrix>* m_timedCircularTrackerMatrixBuffer;
	bool m_isRecording = false;
	bool m_enableRealtimePointCloud = false;
	bool m_writeTSDF;
	bool m_scanInParallel;
	bool m_enableTextureProcessing = false;
	unsigned int m_realtimePointCloudFPS = 30u;
	size_t m_timeSinceLastScan = std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::system_clock::now().time_since_epoch()).count();
	int m_textureProcessingJpegQuality = 95;

	// Settings to determine how to find the scanDevice that should be connected to the vive tracker in code.
	bool m_trackedScanDeviceFound = false;
	bool m_useSerialToFindTrackedScanDevice = false;
	std::string m_serialOfTrackedScanDevice;

	//MulticamCalibration
	bool m_useMultiCamCalibration = false;

	// MultiCamCalibration Optimization
	std::atomic<bool> m_multiCamOptimizationLock;

	//OpenCV
	MarkerType m_markerType;

	//OpenCV Checkerboard
	unsigned int m_checkerboardDimX;
	unsigned int m_checkerboardDimY;
	float m_checkerboardSquareSizeInMM;
	osg::Vec3 m_patternToTrackerOffset;
	osg::Matrix m_patternToTrackerTrans;
	osg::Vec3 m_patternToTrackerRotOffset;
	osg::Matrix m_patternToTrackerRot;
	osg::Matrix m_patternToTrackerTransform;
	std::vector<cv::Point3f> m_checkerboardPattern;

	//OpenCV ChArUco
	float m_charucoMarkerSizeInMM;
	unsigned int m_charucoMinDetectedPoints;
	cv::Ptr<cv::aruco::Dictionary> m_charucoDictionary;
	cv::Ptr<cv::aruco::CharucoBoard> m_charucoBoard;
	cv::Ptr<cv::aruco::DetectorParameters> m_charucoParams;

	//Texture Processing
	std::vector<ImageData*>* m_imageData;

	//Recording
	std::vector<PointcloudData*>* m_pointcloudRecording = nullptr;

	osg::ref_ptr<osg::MatrixTransform> m_displayTransform;
	PointCloudDisplay* m_pointCloudDisplay;

	unsigned long long m_system_clock_origin;
	unsigned long long m_device_clock_origin;

	// ### Unified Scan Pipeline ###
	ScanPipeline* m_scanPipeline;
	unsigned int m_devicesPerScan; //0: A scan is performed for all connected devices

	// ScanDevice Initializer Thread
	std::future<void> m_scanDeviceInitAsync;
	std::atomic<bool> m_scanDeviceInitLock;

	std::future<std::vector<ScanDevice*>> m_calibrateCameraPosesAsync;
	std::atomic_uchar m_calibrateCameraPosesState; // 0: no ongoing calibration | 1: calibration thread started | 2: stop calibration process | 3: finalize calibration process and add the scanDevices to the pipeline

	void getMaxScanDeviceResolution(unsigned int& width_out, unsigned int& height_out);
	void addAzureKinectScanDevices();
	void addRealSenseScanDevices();
	void addAzureKinectViveTrackerScanDevice();
	void addRealSenseViveTrackerScanDevice();
	bool loadCameraCalibration(const char* serial, cv::Mat* cameraMatrix_out, cv::Mat* distCoeffs_out, unsigned int& width_out, unsigned int& height_out);
	std::vector<cv::Point3f> generateCheckerboardPattern();
	bool calculateScanDevicePoseViaPattern(const cv::Mat& cameraMatrix, const cv::Mat& distCoeffs, cv::Mat& colorImage, osg::Matrix* calculatedCamPose);
	// #############################

	// #### MEMBER FUNCTIONS ###############
	void processPointCloud();
	void generate_point_cloud(k4a::image point_cloud);
	/* ### START - Old Scan Pipeline ###
	void generate_point_cloud(k4a::image point_cloud);
	void processPointCloudWithMultipleDevices(std::vector<osg::MatrixTransform*> transformationMatricesForPointsIn);
	// ### END - Old Scan Pipeline ### */
	//void warmUpRealSense(int cycles);
	osg::Matrix findCorrespondingTrackerMatrixToTimestamp(unsigned long long depthFrameTimeStamp);
	unsigned long long deviceToSystemClock(unsigned long long deviceTimestamp);

	//Azure Kinect (Live View)
	//all kinect data values
	k4a::device m_azureKinectSensor;
	k4a::calibration m_calibration;
	k4a::transformation m_transformation;
	k4a_device_configuration_t m_config;
	k4a::capture* m_kinectCapture;
	k4a::image m_kinectColorImage;

	std::vector<LiveMesh*> m_liveMeshPool;

	std::vector<std::thread*> m_threadPool;

	std::atomic<bool> m_isTelepresenceLoopActive;
	std::atomic<bool>* m_isSceneGraphLockedForRendering;

	osg::Geode* m_telePresenceGeode;
	osg::OpenMeshGeometry* m_cloth;

	float m_backgroundClippingDistance;

	int m_vertCount;
	std::vector<bool> m_nullVector;
	osg::Vec3Array* m_pointcloud;

	osg::ref_ptr<osg::Vec3Array> m_liveViewQuatVertices;
	osg::ref_ptr<osg::Vec4Array> m_liveViewQuatVertColors;
	osg::ref_ptr<osg::Vec3Array> m_liveViewQuatNormals;
	osg::ref_ptr<osg::Vec2Array> m_liveViewTexcoords;
	osg::ref_ptr<osg::Geometry> m_liveViewQuad;
	osg::ref_ptr<osg::Image> m_liveViewImage;
	osg::ref_ptr<osg::Texture2D> m_liveViewTexture;
	osg::ref_ptr<osg::Geode> m_liveViewQuadGeode;
	
	//Interesting for face scanning
	int m_framesToCapture = 1;
	short m_clippingDistance = 1024;

	void saveImage(osg::Image* image, std::string path);
	void initGeometries();

	cv::Mat convertColorDataToOpenCVMat(ColorData* colorIn);
	cv::Mat convertDepthDataToOpenCVMat(DepthData* depthIn);

	int m_maxNumThreads;
	std::vector <std::thread>				m_threads;
	std::vector <osg::OpenMeshGeometry*>	m_geometries;
	std::vector <osg::Geode*>				m_geodes;
	//osg::ref_ptr<GeometryVisualization> m_geometryVisualization;

	void manageMeshes();
	void checkLockSceneGraph();



public:
	void updateProcessingThread();
	bool attemptToScan();
	void updateAndRenderPointCloud(); //Main loop call method for ONE sensor (in main.cpp)
	void updateAndRenderPointCloud(std::vector<osg::MatrixTransform*> transformationMatricesForPointsIn); //Main loop call method for MULTIPLE sensors (in main.cpp)
	void processingLoop();
	void stopPointCloudPipe();
	bool visualizePointCloud();
	void clearPointCloud();
	//void convertPointCloudToPCL(osg::ref_ptr<osg::OpenMeshGeometry> openMeshGeo);
	void savePointCloud(std::string savePathIn);
	void savePointCloud(std::string savePathIn, osg::ref_ptr<osg::OpenMeshGeometry> openMeshGeo);
	void loadPointCloud(osg::ref_ptr<osg::OpenMeshGeometry> openMeshGeo, std::string loadPathIn);
	bool toggleBodyScanning();
	bool revertSnapshot();
	void renderVideoTexture();
	//pcl::PointCloud<pcl::PointXYZRGB>::Ptr rsPoints_to_pcl(const rs2::points& points, const rs2::video_frame& colorFrameIn);
	void setTimedCircularTrackerMatrixBuffer(
		boost::circular_buffer<TimedOpenVRMatrix>* timedCircularTrackerMatrixBuffer,
		std::atomic<bool>* timedCircularTrackerMatrixBuffer_Lock);

	void findAndInitializeScanDevices();
	void findAndInitializeScanDevicesFunction();

	void toggleCameraPoseCalibration();
	std::vector<ScanDevice*> cameraPoseCalibrationLoop();
	std::vector<ScanDevice*> multiCamPoseCalibrationLoop();
	void finalizeCameraPoseCalibration();
	bool calibrateCameraPose(const cv::Mat& cameraMatrix, const cv::Mat& distCoeffs, cv::Mat& image, unsigned int& camPoseWeight, osg::Matrix& camPose);
	bool calibrateCameraPose(const cv::Mat& cameraMatrix, const cv::Mat& distCoeffs, cv::Mat& image, unsigned int& camPoseWeight, osg::Matrix& camPose, osg::Matrix& camOffset, osg::Matrix& multiCamPose);

	bool loadMultiCamCalibration(std::map<std::string, osg::Matrix>& calibrationData, std::string* startDeviceSerial = nullptr);
	bool updateMultiCamCalibration(std::map<std::string, osg::Matrix>& calibrationData);

	void startMultiCamOptimization();
	void multiCamOptimizationLoop(const unsigned int iterations);

	void startAzureKinect();
	void makeAndSaveAFewDepthImagesWithKinect(std::string dirSavePath); //Function for Bachelor thesis of Alex Pech
	void paintKinectImageOnQuad();
	void shutDownAzureKinect();


	void exportPointCloud();
	void exportPointCloudMesh(std::string fileName = "");
	void exportPointCloudTexture();
	void exportPointCloudTexture(const std::vector<ImageData*>* imageData, std::string fileName = "");

	// Recording
	bool realtimePointCloud();
	bool toggleRealtimePointCloud();
	void toggleRecording();
	bool exportRecording();

	void drawDebugDisplay(const osg::Matrix trackerMatrix);

	//void debugRedraw();

	//void debugDraw(osg::ref_ptr<osg::MatrixTransform> genericTracker);
	
	//Mainly triggered via the interaction management (Controller/Menu-Input)
	void polygonizePointCloud();	
	void drawOctreeVolume();
	void drawTSDF();
	void drawDDA(osg::Vec3f from, osg::Vec3f to);
	void clearOctreeVolumeDisplay();
	void clearTSDFDisplay();
	void clearDDADisplay();
	void clearPolygonize();
	void togglePointCloudDisplay();
	void toggleOctreeDisplay();
	void toggleTSDFDisplay();
	void toggleDDADisplay();
	void toggleMarchingCubesDisplay();
	void setPointCloudDisplay(bool displayState);
	void setOctreeDisplay(bool displayState);
	void setTSDFDisplay(bool displayState);
	void setDDADisplay(bool displayState);
	void setPolygonizeDisplay(bool displayState);

	void joinTelepresenceThread();
	void lockSceneGraph();
	void unlockSceneGraph();
	std::condition_variable m_cv;
	std::mutex m_mutex;

	void runICP();
};