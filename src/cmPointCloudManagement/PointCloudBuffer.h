#pragma once

#include <osg/Geometry>
#include <atomic>

struct PointCloudBuffer {
private:
	unsigned int m_width;
	unsigned int m_height;
	unsigned int m_validEntries;
	unsigned int m_validBlendEntries;
	//### This is approximately 49,21875 MB in Memory
	osg::Vec3* m_PcVertexBuffer;			//Array with m_width * m_height elements
	osg::Vec2* m_PcTexcoordBuffer;			//Array with m_width * m_height elements
	osg::Vec4* m_PcColorBuffer;				//Array with m_width * m_height elements
	osg::Vec4* m_PcBlendColorBuffer;		//Array with m_width * m_height elements
	unsigned int* m_PcGlobalIndexBuffer;	//Array with m_width * m_height elements
	//###
	osg::Vec3 m_PcNormal;
	//osg::ref_ptr<osg::MatrixTransform> m_matrixTracking;
public:
	PointCloudBuffer(unsigned int width, unsigned int height);
	PointCloudBuffer(PointCloudBuffer&& other) noexcept;
	PointCloudBuffer(const PointCloudBuffer& other);
	~PointCloudBuffer();
	void clear();
	void pushBack(const osg::Vec3& vertex, const osg::Vec2& uv, const osg::Vec4& color);
	void pushBack(const unsigned int& globalIndex, const osg::Vec4& color);
	void emplaceBack(const float & x, const float & y, const float & z, const float & u, const float & v, const float& r, const float& g, const float& b, const float& a);
	void emplaceBack(const unsigned int& globalIndex, const float& r, const float& g, const float& b, const float& a);
	//void pushMatrix(osg::ref_ptr<osg::MatrixTransform> matrixTrackingIn);
	void pushMatrix2(osg::Matrix matrixTrackingIn);
	void pushNormal(const osg::Vec3& normal);
	unsigned int getValidEntries();
	unsigned int getValidBlendEntries();
	osg::Vec3& getVertex(const unsigned int& index);
	osg::Vec2& getTexcoord(const unsigned int& index);
	osg::Vec4& getColor(const unsigned int& index);
	osg::Vec4& getBlendColor(const unsigned int& index);
	unsigned int& getGlobalIndex(const unsigned int& index);
	osg::Vec3& getNormal();
	//osg::MatrixTransform& getMatrix();
	void pushData(const unsigned int size, osg::Vec3* vertexData, osg::Vec2* texCoordData, osg::Vec4* colorData);
};