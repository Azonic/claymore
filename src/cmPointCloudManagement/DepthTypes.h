#pragma once

#include <intrin.h>

struct DepthData {
	enum class DepthFormat {
		Z16,		// 16bit Depth values with big endian encoding; RS & KINECT
		Z16BE,		// 16bit Depth values with little endian encoding;
		UNSUPPORTED
	};
protected:
	const void* _data;
	const DepthFormat _format;
	const size_t _bytes;
	const unsigned int _width;
	const unsigned int _height;
	const float _depthScale;
public:
	DepthData(const void* data, const DepthFormat format, const size_t bytes, const unsigned int width, const unsigned int height, const float depthScale) : _data(data), _format(format), _bytes(bytes), _width(width), _height(height), _depthScale(depthScale) {}
	virtual ~DepthData() {}
	virtual inline const float z(const unsigned int index) = 0;
	const void* data() { return _data; }
	const DepthFormat format() { return _format; }
	const size_t bytes() { return _bytes; }
	const size_t size() { return (size_t) _width * _height; }
	const unsigned int width() { return _width; }
	const unsigned int height() { return _height; }
	const float depthScale() { return _depthScale; }
};

struct DepthData_Z16BE : DepthData { //BE: Big Endian
	DepthData_Z16BE(const void* data, const size_t bytes, const unsigned int width, const unsigned int height, const float depthScale) : DepthData(data, DepthFormat::Z16BE, bytes, width, height, depthScale) { }
	virtual inline const float z(const unsigned int index) override { 
		return _depthScale * _byteswap_ushort(((unsigned short*)_data)[index]); 
	}
};

struct DepthData_Z16 : DepthData { // Little Endian
	DepthData_Z16(const void* data, const size_t bytes, const unsigned int width, const unsigned int height, const float depthScale) : DepthData(data, DepthFormat::Z16, bytes, width, height, depthScale) { }
	virtual inline const float z(const unsigned int index) override { 
		return _depthScale * ((unsigned short*)_data)[index]; 
	}
};