#pragma once

#include "OctreeData.h"

#include <osg\Vec3f>
#include <atomic>
#include <iostream>

#define DEBUG_SPLITS_PER_OCTREE 0 // Should never exceed 1; Each octree will keep track of how many times it has been split, if this flag is defined

template<class T>
class Octree {
private:
	//TODO: Current memory consumption is not the most efficient since we have 15(short) / 17(float) / 21(double)
	//		To utilize the full memory needed to store all the data, we should occupy exactly a multitude of 8 due to the byte alignment (pointer = 8 byte)
	unsigned char m_depth;			// 1 byte 
	unsigned char m_maxDepth;		// 1 byte
	T m_value;						// 2 byte (short), 4 byte (int, float), 8 byte (double)
	unsigned short m_weight;		// 2 byte
	//Octree<T>* m_children;			// 8 byte
	void* m_children;				// 8 byte //NOTE: for every leaf this pointer is == nullptr anyways, since there are no children to link anymore; so, to minimize the memory usage, we store additional data only for the highest depth here as an OctreeData object. Therefore the void* can hold the 2 types Octree* and OctreeData*, depending on whether m_depth < m_maxDepth.
	bool m_selected;				// 1 byte //NOTE: maybe remove the selection flag later on; due to the byte alignment (8 byte because of the pointer; so we need 16 bytes anyway) this should not consume any additional memory
	//std::atomic_bool m_semaphore;	// 1 byte //NOTE: tested; according to sizeof(atomic_bool) the size is 1 byte, while sizeof(mutex) on 32bit is 40 byte and 80 on 64bit. The class shared_mutex uses 4 or 8 byte depending on the architecture.
	std::atomic_uchar m_lock;	// 1 byte //NOTE: tested; according to sizeof(atomic_uchar) the size is 1 byte, while sizeof(mutex) on 32bit is 40 byte and 80 on 64bit. The class shared_mutex uses 4 or 8 byte depending on the architecture.
#if DEBUG_SPLITS_PER_OCTREE == 1
public:
	std::atomic_int m_split;
#endif // DEBUG_SPLITS_PER_OCTREE
private:
	void copyMembers(const Octree<T>& other);
public:
	Octree() = delete;
	Octree(T value, const unsigned char maxDepth, const unsigned char depth = 0);
	~Octree();
	Octree(Octree<T>&& other);
	Octree(const Octree<T>& other);
	Octree<T>& operator=(const Octree<T>& other);
	unsigned char getDepth() const;
	unsigned char getMaxDepth() const;
	bool isMaxDepth() const;
	template<bool threadSafe = false>
	T getValue();
	template<bool threadSafe = false>
	T getHierarchyValue(const float x, const float y, const float z);
	template<bool threadSafe = false>
	T getHierarchyValue(const osg::Vec3f& xyz);
	template<bool threadSafe = false>
	bool hasMaxDepthValue(const float x, const float y, const float z);
	template<bool threadSafe = false>
	bool hasMaxDepthValue(const osg::Vec3f& xyz);
	template<bool threadSafe = false>
	void setValue(const T value);
	template<bool threadSafe = false>
	void setWeight(const unsigned short weight);
	template<bool threadSafe = false>
	unsigned short getWeight();
	template<bool threadSafe = false>
	void increaseWeight();
	template<bool threadSafe = false>
	Octree<T>* setValueInHierarchy(const T value, const float x, const float y, const float z, bool compareValues = true);
	template<bool threadSafe = false>
	Octree<T>* setValueInHierarchy(const T value, const osg::Vec3f& xyz, bool compareValues = true);
	template<bool threadSafe = false>
	Octree<T>* getOctree(const float x, const float y, const float z, const bool forceMaxDepth = false);
	template<bool threadSafe = false>
	Octree<T>* getOctree(const float x, const float y, const float z, Octree<T>** parent, const bool forceMaxDepth = false);
	template<bool threadSafe = false>
	Octree<T>* getOctree(const osg::Vec3f& xyz, const bool forceMaxDepth = false);
	template<bool threadSafe = false>
	Octree<T>* getOctree(const osg::Vec3f& xyz, Octree<T>** parent, const bool forceMaxDepth = false);
	template<bool threadSafe = false>
	Octree<T>* getOctree(const unsigned char childIndex);
	template<bool threadSafe = false>
	Octree<T>* getOctreeLocal(const unsigned int x, const unsigned int y, const unsigned int z, const bool forceMaxDepth = false);
	template<bool threadSafe = false>
	Octree<T>* getOctreeLocal(const unsigned int x, const unsigned int y, const unsigned int z, Octree<T>** parent, const bool forceMaxDepth = false);
	unsigned char calculateChildIndex(const unsigned int x, const unsigned int y, const unsigned int z);
	template<bool threadSafe = false>
	bool hasChildren();
	template<bool threadSafe = false>
	bool split(T value);
	template<bool threadSafe = false>
	bool split();
	template<bool threadSafe = false>
	bool hasData();
	template<bool threadSafe = false>
	OctreeData* getData();
	template<bool threadSafe = false>
	bool setData(const OctreeData& octreeData);
	template<bool threadSafe = false, const int maxThreads = 0>
	void minimize(std::atomic<int>* threadCount, const bool onThread);
	template<bool threadSafe = false>
	void minimize();
	template<bool threadSafe = false>
	void minimizeMultithreaded();
	template<bool threadSafe = false>
	void clear(bool retainValue = false);
	template<bool threadSafe = false>
	void setSelected(bool state, bool recursive = false);
	template<bool threadSafe = false>
	void select(bool recursive = false);
	template<bool threadSafe = false>
	void deselect(bool recursive = false);
	template<bool threadSafe = false>
	bool isSelected();
	template<bool threadSafe = false>
	bool compareChildren();
	unsigned int roundToGrid(const unsigned int value) const;
	unsigned int roundToLocalGrid(const unsigned int value) const;
	unsigned int getSize() const;
	unsigned int getMaxSize() const;
	//void acquireSemaphore();
	//void releaseSemaphore();
	//bool tryAcquiringSemaphore(const unsigned int attempts = 1);
	//void awaitSemaphoreRelease();
	//bool testSemaphore();
	void writeLock();
	void writeUnlock();
	void readLock();
	void readUnlock();
};
