// ClayMore - Immersive Mesh Modelling --- Copyright (c) 2014-2017 Philipp Ladwig, Jannik Fiedler, Jan Beutgen
//Header File of this class
#include "RealSenseUtils.h"

#include <rs.hpp> 
//#include <hpp\rs_pipeline.hpp>
//#include <hpp\rs_frame.hpp>

//Constructor
RealSenseUtils::RealSenseUtils()
{

}

RealSenseUtils::~RealSenseUtils()
{
	
}

/// \brief A static function for setting up the custom config, since there are multiple points for setting up the realsense config
void RealSenseUtils::setUpConfig(rs2::config* configIn, unsigned int width, unsigned int height, unsigned int fps)
{
	//ToDo5: Move formats to config.xml? Any needs for it?
	configIn->enable_stream(RS2_STREAM_DEPTH, -1, width, height, rs2_format::RS2_FORMAT_Z16, fps); // Enable default depth
	//c.enable_stream(RS2_STREAM_DEPTH, -1, 424, 240, rs2_format::RS2_FORMAT_Z16, 90); // Enable default depth
	// For the color stream, set format to RGBA
	// To allow blending of the color frame on top of the depth frame
	configIn->enable_stream(RS2_STREAM_COLOR, -1, width, height, rs2_format::RS2_FORMAT_RGB8, fps);
}

//Grab the first few frames at startup and immediately dump them. The first frame does not seem to contain color information.
void RealSenseUtils::warmUpRealSenses(int cycles) {
	//if (RS_IS_MULTI_CAM_ACTIVE)
	//{

	//}
	//else {
	//	for (int i = 0; i < cycles; i++) {
	//		rs2::frameset frames = m_rsPipe->wait_for_frames();
	//		rs2::frame depth = frames.get_depth_frame();
	//		rs2::video_frame colorFrame = frames.get_color_frame();
	//		m_rsPc->map_to(colorFrame);
	//	}
	//}
}