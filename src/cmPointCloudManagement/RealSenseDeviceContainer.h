// ClayMore - Immersive Mesh Modelling --- Copyright (c) 2014-2017 Philipp Ladwig, Jannik Fiedler, Jan Beutgen
#pragma once

#include "ConfigDllExportPointCloudManagement.h"

#include <rs.hpp>
#include <map>
#include <mutex>

const std::string platform_camera_name = "Platform Camera";

class RealSenseDeviceContainer
{
public:
	RealSenseDeviceContainer();

	// Helper struct per pipeline
	struct view_port
	{
		std::map<int, rs2::frame> frames_per_stream;
		rs2::colorizer colorize_frame;
		//texture tex;
		rs2::pipeline pipe;
		rs2::pipeline_profile profile;
	};

	void enableDevice(rs2::device dev, unsigned int width, unsigned int height, unsigned int fps);
	void removeDevices(const rs2::event_information& info);
	size_t deviceCount();
	int streamCount();
	void pollFrames();
	void renderTextures(int cols, int rows, float view_width, float view_height);
	std::map<std::string, view_port>* getDevices() {
		return _devices;
	};

private:
	std::mutex _mutex;
	std::map<std::string, view_port>* _devices;
};