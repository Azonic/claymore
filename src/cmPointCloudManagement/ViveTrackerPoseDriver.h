#pragma once

#include "PoseDriver.h"
#include <atomic>
#include <boost/circular_buffer.hpp>
#include "cmUtil/TimedOpenVRMatrix.h"

class ViveTrackerPoseDriver : public PoseDriver {
private:
	boost::circular_buffer<TimedOpenVRMatrix>*& m_trackerMatrixBuffer;
	std::atomic<bool>*& m_trackerMatrixBuffer_lock;
public:
	ViveTrackerPoseDriver(boost::circular_buffer<TimedOpenVRMatrix>*& trackerMatrixBuffer, std::atomic<bool>*& trackerMatrixBuffer_lock, const osg::Matrix& sensorToDriver);
	~ViveTrackerPoseDriver();
	virtual osg::Matrix getTransform(unsigned long long timestamp_usec) override;
	virtual osg::Matrix getPose(unsigned long long timestamp_usec) override;
private:
	osg::Matrix getMatrixByTimestamp(unsigned long long systemTimestamp_usec);
};