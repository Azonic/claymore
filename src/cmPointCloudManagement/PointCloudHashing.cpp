#include "defines.h"
#include "PointCloudHashing.h"

static const unsigned short bitsXZ = 22;
static const uint64_t mask = ((uint64_t)1 << bitsXZ) - 1;
static const uint64_t signMask = ((uint64_t)1 << 61) - 1;

/*
//---Vec3fKey---
Vec3fKey::Vec3fKey(float x, float y, float z) {
	this->x = x;
	this->y = y;
	this->z = z;
	index = 1;
}

Vec3fKey::Vec3fKey() : Vec3fKey::Vec3fKey(0, 0, 0) {}
Vec3fKey::Vec3fKey(const osg::Vec3f& v) : Vec3fKey::Vec3fKey(
	Vec3fHasher::RoundFloat(v.x(), GRID.x(), GRID_OFFSET.x()),
	Vec3fHasher::RoundFloat(v.y(), GRID.y(), GRID_OFFSET.y()),
	Vec3fHasher::RoundFloat(v.z(), GRID.z(), GRID_OFFSET.z())) {}

float Vec3fKey::X() const { return this->x; }
float Vec3fKey::Y() const { return this->y; }
float Vec3fKey::Z() const { return this->z; }

void Vec3fKey::AddVec3f(osg::Vec3f& vec, const osg::Vec3f& vecNew) {
	int a = index / ++index;
	int b = 1 - index;
	vec.set(
		a * vec.x() + b * vecNew.x(),
		a * vec.y() + b * vecNew.y(),
		a * vec.z() + b * vecNew.z()
	);
}

bool Vec3fKey::Compare(float x, float y, float z) const {
	return 
		fabsf(this->x - x) <= GRID_OFFSET.x() &&
		fabsf(this->y - y) <= GRID_OFFSET.y() &&
		fabsf(this->z - z) <= GRID_OFFSET.z();
}

bool Vec3fKey::operator==(const Vec3fKey& other) const {
	return Compare(other.x, other.y, other.z);
}

bool Vec3fKey::operator==(const osg::Vec3f& other) const {
	return Compare(other.x(), other.y(), other.z());
}

//---Vec3fHasher---
float Vec3fHasher::RoundFloat(const float value, const float grid, const float gridOffset) {
	float f = (float)(((int)(value / grid)) * grid + gridOffset);
	return f;
}

/*
std::size_t Vec3fHasher::HashCode(float x, float y, float z) {
	return (x + y) * z;
}

std::size_t Vec3fHasher::operator()(const osg::Vec3f& v) const {
	return HashCode(v.x(), v.y(), v.z());
}

std::size_t Vec3fHasher::operator()(const Vec3fKey& v) const {
	return HashCode(v.X(), v.Y(), v.Z());
}*/

/*std::size_t Vec3fHash::operator()(const osg::Vec3f& vec) const {
	std::size_t seed = vec.length();
	seed ^= (int) (vec.x() / GRID.x()) + 0x9e3779b9 + (seed << 6) + (seed >> 2);
	seed ^= (int) (vec.y() / GRID.y()) + 0x9e3779b9 + (seed << 6) + (seed >> 2);
	seed ^= (int) (vec.z() / GRID.z()) + 0x9e3779b9 + (seed << 6) + (seed >> 2);
	
	return seed;
}*/

/*size_t Vec3fHash::operator()(const osg::Vec3f & v) const
{
	unsigned int x = v.x() / GRID.x();
	unsigned int y = v.y() / GRID.y();
	unsigned int z = v.z() / GRID.z();

	uint64_t signs = ((uint64_t)((v.z() < 0) + (v.y() < 0) * 2 + (v.x() < 0) * 4)) << 61;

	uint64_t hash = ((x & mask) | ((y & mask) << preciseBits) | ((z & mask) << (preciseBits * 2))) | // precise bits
		(((x & y ^ z) & chunkMask) << preciseBits * 2) & // chunk bits: precision is not that important
		signMask | signs;
	return hash;
}*/

bool Vec3fComparer::operator()(const osg::Vec3f& v1, const osg::Vec3f& v2) const {
	return v1.x() == v2.x() &&
		v1.y() == v2.y() &&
		v1.z() == v2.z();
}

float RoundFloat(float f) {
	//char abs = (f < 0 ? -1 : 1);
	//f = ((int) (f / GRID * abs)) * GRID * abs + GRID_OFFSET;
	f = (floorf(f / GRID)) * GRID + GRID_OFFSET;
	return f;
}

osg::Vec3f RoundVector(float x, float y, float z)
{
	return osg::Vec3f(RoundFloat(x), RoundFloat(y), RoundFloat(z));
}

osg::Vec3f RoundVector(const osg::Vec3f& v)
{
	return osg::Vec3f(RoundFloat(v.x()), RoundFloat(v.y()), RoundFloat(v.z()));
}

void RoundVectorSet(osg::Vec3f& v)
{
	v.set(RoundFloat(v.x()), RoundFloat(v.y()), RoundFloat(v.z()));
}

size_t Vec3fHasher::operator()(const osg::Vec3f & v) const
{
	/*uint64_t ix = v.x() / GRID;
	uint64_t iy = v.y() / GRID;
	uint64_t iz = v.z() / GRID;

	uint64_t signs = ((uint64_t)((v.z() < 0) + (v.y() < 0) * 2 + (v.x() < 0) * 4)) << 61;

	uint64_t hash = ((ix & mask) | ((iz & mask) << bitsXZ) | (iy << (bitsXZ * 2))) & signMask | signs;
	return hash;*/

	/*uint64_t ix = v.x() / GRID;
	uint64_t iy = v.y() / GRID;
	uint64_t iz = v.z() / GRID;

	uint64_t signs = 0;
	if (v.z() < 0) signs += 0x2000000000000000;
	if (v.y() < 0) signs += 0x4000000000000000;
	if (v.x() < 0) signs += 0x8000000000000000;
	//signs <<= 61;

	uint64_t hash = ((ix & mask) | ((iz & mask) << bitsXZ) | (iy << (bitsXZ * 2))) & signMask | signs;
	return hash;*/
	
	std::size_t seed = 3;
	seed ^= (int)(v.x() / GRID) + 0x9e3779b9 + (seed << 6) + (seed >> 2);
	seed ^= (int)(v.y() / GRID) + 0x9e3779b9 + (seed << 6) + (seed >> 2);
	seed ^= (int)(v.z() / GRID) + 0x9e3779b9 + (seed << 6) + (seed >> 2);

	return seed;
}
