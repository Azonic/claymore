#pragma once

#include <osg/Matrix>

/**
 * @brief Handles positioning of a ScanDevice in 3D space and transforming their pointcloud data in local coordinates to global space.
*/
class PoseDriver {
protected:
	/**
	 * @brief Transforms coordinates from the sensor's to the driver's coordinate system. Necessary to counter the offset of the sensor and the tracking device's origin.
	*/
	osg::Matrix m_sensorToDriver;
public:
	/**
	 * @brief Creates a new PoseDriver with a given sensorToDriver-matrix
	 * @param sensorToDriver transforms coordinates from the sensor's to the driver's coordinate system. Necessary to counter the offset of the sensor and the tracking device's origin.
	*/
	PoseDriver(const osg::Matrix& sensorToDriver);
	/**
	 * @brief Deconstructs the PoseDriver.
	*/
	virtual ~PoseDriver();
	/**
	 * @brief Retrieve the transform matrix at a given timestamp.
	 * @param timestamp_usec system timestamp in microseconds corresponding to the desired transform.
	 * @return Matrix to transform coordinates from local sensor space to global space.
	*/
	virtual osg::Matrix getTransform(unsigned long long timestamp_usec) = 0;
	/**
	* @brief Retrieve the pose matrix at a given timestamp.
	* @param timestamp_usec system timestamp in microseconds corresponding to the desired pose.
	* @return Pose matrix of the associated tracking device.
	*/
	virtual osg::Matrix getPose(unsigned long long timestamp_usec) = 0;
	osg::Matrix getSensorToDriverMatrix();
};