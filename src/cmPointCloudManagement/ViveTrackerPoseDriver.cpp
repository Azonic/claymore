#include "ViveTrackerPoseDriver.h"
#include <thread>

ViveTrackerPoseDriver::ViveTrackerPoseDriver(boost::circular_buffer<TimedOpenVRMatrix>*& trackerMatrixBuffer, std::atomic<bool>*& trackerMatrixBuffer_lock, const osg::Matrix& sensorToDriver) : 
	PoseDriver(sensorToDriver), m_trackerMatrixBuffer(trackerMatrixBuffer), m_trackerMatrixBuffer_lock(trackerMatrixBuffer_lock)
{ }

ViveTrackerPoseDriver::~ViveTrackerPoseDriver() { }

osg::Matrix ViveTrackerPoseDriver::getTransform(unsigned long long timestamp_usec) {
	return m_sensorToDriver * getPose(timestamp_usec);
}

osg::Matrix ViveTrackerPoseDriver::getPose(unsigned long long timestamp_usec) {
	return getMatrixByTimestamp(timestamp_usec);
}

osg::Matrix ViveTrackerPoseDriver::getMatrixByTimestamp(unsigned long long systemTimestamp_usec) {
	if (m_trackerMatrixBuffer_lock == nullptr) return osg::Matrix();

	bool expected;
	while (!m_trackerMatrixBuffer_lock->compare_exchange_weak(expected = false, true)) std::this_thread::sleep_for(std::chrono::nanoseconds(1));

	/*osg::Matrix matrix(
		0.0, 0.1, 0.2, 0.3,
		1.0, 1.1, 1.2, 1.3,
		2.0, 2.1, 2.2, 2.3,
		3.0, 3.1, 3.2, 3.3);*/
	osg::Matrix matrix(
		0.0, 0.0, 0.0, 0.0,
		0.0, 0.0, 0.0, 0.0,
		0.0, 0.0, 0.0, 0.0,
		0.0, 0.0, 0.0, 0.0
	);
	
	int i = 0;
	for (boost::circular_buffer<TimedOpenVRMatrix>::iterator iter = m_trackerMatrixBuffer->end() - 1; iter != m_trackerMatrixBuffer->begin(); iter--) {
		if (iter->timestamp < systemTimestamp_usec) {
			if (iter == m_trackerMatrixBuffer->end() - 1) {
				Matrix34 pose0 = iter->matrix;

				m_trackerMatrixBuffer_lock->store(false);
				matrix.set(
					pose0.m[0][0], pose0.m[1][0], pose0.m[2][0], 0.0,
					pose0.m[0][1], pose0.m[1][1], pose0.m[2][1], 0.0,
					pose0.m[0][2], pose0.m[1][2], pose0.m[2][2], 0.0,
					pose0.m[0][3], pose0.m[1][3], pose0.m[2][3], 1.0f
				);
			} else {
				Matrix34 pose0 = iter->matrix;
				Matrix34 pose1 = (iter + 1)->matrix;

				unsigned long long t0 = iter->timestamp;
				unsigned long long t1 = (iter + 1)->timestamp;

				m_trackerMatrixBuffer_lock->store(false);

				double b = (double)(systemTimestamp_usec - t0) / (double)(t1 - t0);
				double a = 1.f - b;

				matrix.set(
					pose0.m[0][0] * a + pose1.m[0][0] * b, pose0.m[1][0] * a + pose1.m[1][0] * b, pose0.m[2][0] * a + pose1.m[2][0] * b, 0.0,
					pose0.m[0][1] * a + pose1.m[0][1] * b, pose0.m[1][1] * a + pose1.m[1][1] * b, pose0.m[2][1] * a + pose1.m[2][1] * b, 0.0,
					pose0.m[0][2] * a + pose1.m[0][2] * b, pose0.m[1][2] * a + pose1.m[1][2] * b, pose0.m[2][2] * a + pose1.m[2][2] * b, 0.0,
					pose0.m[0][3] * a + pose1.m[0][3] * b, pose0.m[1][3] * a + pose1.m[1][3] * b, pose0.m[2][3] * a + pose1.m[2][3] * b, 1.0f
				);
			}
			std::cout << "iter timestamp number: " << i << std::endl;
			return matrix;
		}
		i++;
	}
	return matrix;
}