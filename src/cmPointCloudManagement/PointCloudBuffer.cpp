#include "PointCloudBuffer.h"
#include <osg/MatrixTransform>

PointCloudBuffer::PointCloudBuffer(unsigned int width, unsigned int height) :
	m_width{ width },
	m_height{ height },
	m_validEntries(0),
	m_validBlendEntries(0)
{
	const unsigned int size = m_width * m_height;
	m_PcVertexBuffer = new osg::Vec3[size];
	m_PcTexcoordBuffer = new osg::Vec2[size];
	m_PcColorBuffer = new osg::Vec4[size];
	m_PcBlendColorBuffer = new osg::Vec4[size];
	m_PcGlobalIndexBuffer = new unsigned int[size];
}

PointCloudBuffer::PointCloudBuffer(PointCloudBuffer&& other) noexcept :
	m_width{ other.m_width },
	m_height{ other.m_height },
	m_validEntries{ other.m_validEntries },
	m_validBlendEntries{ other.m_validBlendEntries },
	m_PcVertexBuffer{ other.m_PcVertexBuffer },
	m_PcTexcoordBuffer{ other.m_PcTexcoordBuffer },
	m_PcColorBuffer{ other.m_PcColorBuffer },
	m_PcBlendColorBuffer{ other.m_PcBlendColorBuffer },
	m_PcGlobalIndexBuffer{ other.m_PcGlobalIndexBuffer }
{
	other.m_PcVertexBuffer = nullptr;
	other.m_PcTexcoordBuffer = nullptr;
	other.m_PcColorBuffer = nullptr;
	other.m_PcBlendColorBuffer = nullptr;
	other.m_PcGlobalIndexBuffer = nullptr;
}

PointCloudBuffer::PointCloudBuffer(const PointCloudBuffer& other) :
	m_width{ other.m_width },
	m_height{ other.m_height },
	m_validEntries{ other.m_validEntries },
	m_validBlendEntries{ other.m_validBlendEntries }
{
	const unsigned int size = m_width * m_height;
	m_PcVertexBuffer = other.m_PcVertexBuffer;
	m_PcTexcoordBuffer = other.m_PcTexcoordBuffer;
	m_PcColorBuffer = other.m_PcColorBuffer;
	m_PcBlendColorBuffer = other.m_PcBlendColorBuffer;
	m_PcGlobalIndexBuffer = other.m_PcGlobalIndexBuffer;
}

PointCloudBuffer::~PointCloudBuffer() {
	delete[] m_PcVertexBuffer;
	delete[] m_PcTexcoordBuffer;
	delete[] m_PcColorBuffer;
	delete[] m_PcBlendColorBuffer;
	delete[] m_PcGlobalIndexBuffer;
}

void PointCloudBuffer::clear() {
	m_validEntries = 0;
	m_validBlendEntries = 0;
}

void PointCloudBuffer::pushBack(const osg::Vec3& vertex, const osg::Vec2& uv, const osg::Vec4& color) {
	m_PcVertexBuffer[m_validEntries] = vertex;
	m_PcTexcoordBuffer[m_validEntries] = uv;
	m_PcColorBuffer[m_validEntries] = color;
	m_validEntries++;
}

void PointCloudBuffer::pushBack(const unsigned int& globalIndex, const osg::Vec4& color) {
	m_PcGlobalIndexBuffer[m_validBlendEntries] = globalIndex;
	m_PcBlendColorBuffer[m_validBlendEntries] = color;
	m_validBlendEntries++;
}

void PointCloudBuffer::emplaceBack(const float& x, const float& y, const float& z,
	const float& u, const float& v,
	const float& r, const float& g, const float& b, const float& a)
{
	new (m_PcVertexBuffer + m_validEntries) osg::Vec3(x, y, z);
	new (m_PcTexcoordBuffer + m_validEntries) osg::Vec2(u, v);
	new (m_PcColorBuffer + m_validEntries) osg::Vec4(r, g, b, a);
	m_validEntries++;
}

void PointCloudBuffer::emplaceBack(const unsigned int& globalIndex, const float& r, const float& g, const float& b, const float& a) {
	new (m_PcBlendColorBuffer + m_validBlendEntries) osg::Vec4(r, g, b, a);
	new (m_PcGlobalIndexBuffer + m_validBlendEntries) unsigned int(globalIndex);
	m_validBlendEntries++;
}

//void PointCloudBuffer::pushMatrix(osg::ref_ptr<osg::MatrixTransform> matrixTrackingIn) {
//	new (m_matrixTracking) osg::MatrixTransform(*matrixTrackingIn);
//	//*m_matrixTracking = *matrixTrackingIn;
//}
//
//void PointCloudBuffer::pushMatrix2(osg::Matrix matrixTrackingIn) {
//	m_matrixTracking->setMatrix(matrixTrackingIn);
//}
//
void PointCloudBuffer::pushNormal(const osg::Vec3& normal) {
	m_PcNormal = normal;
	m_PcNormal.normalize();
}

uint32_t PointCloudBuffer::getValidEntries() {
	return m_validEntries;
}

unsigned int PointCloudBuffer::getValidBlendEntries() {
	return m_validBlendEntries;
}

osg::Vec3& PointCloudBuffer::getVertex(const unsigned int& index) {
	return m_PcVertexBuffer[index];
}

osg::Vec2& PointCloudBuffer::getTexcoord(const unsigned int& index) {
	return m_PcTexcoordBuffer[index];
}

osg::Vec4& PointCloudBuffer::getColor(const unsigned int& index) {
	return m_PcColorBuffer[index];
}

osg::Vec4& PointCloudBuffer::getBlendColor(const unsigned int& index) {
	return m_PcBlendColorBuffer[index];
}

unsigned int& PointCloudBuffer::getGlobalIndex(const unsigned int& index) {
	return m_PcGlobalIndexBuffer[index];
}

osg::Vec3& PointCloudBuffer::getNormal() {
	return m_PcNormal;
}

//osg::MatrixTransform& PointCloudBuffer::getMatrix() {
//	return *m_matrixTracking;
//}

void PointCloudBuffer::pushData(const unsigned int size, osg::Vec3* vertexData, osg::Vec2* texCoordData, osg::Vec4* colorData) {
	memcpy(m_PcVertexBuffer + m_validEntries, vertexData, size * sizeof(osg::Vec3));
	memcpy(m_PcTexcoordBuffer + m_validEntries, texCoordData, size * sizeof(osg::Vec2));
	memcpy(m_PcColorBuffer + m_validEntries, colorData, size * sizeof(osg::Vec4));
	m_validEntries += size;
}