// ClayMore - Immersive Mesh Modelling --- Copyright (c) 2014-2017 Philipp Ladwig, Jannik Fiedler, Jan Beutgen
#pragma once

#include "ConfigDllExportPointCloudManagement.h"
#include "defines.h"

#include <osg/Vec3d>
#include <osg/Geode>
#include <osg/Geometry>
#include <osg/MatrixTransform>
#include <osg/Point>
#include <osg/PolygonMode>
#include <osg/ImageStream>
#include <osg/Texture2D>
#include <osg/ShapeDrawable>

#include "../cmGeometryManagement/GeometryManager.h"
#include "ViewFrustum.h"

//#include <rs.hpp>

//PointCloud includes
#include <pcl/point_types.h>
#include <pcl/filters/passthrough.h>
//Further PointCloudIncludes
#include <pcl/io/pcd_io.h>
#include <pcl/io/ply_io.h>

#include <unordered_set>
#include <unordered_map>
#include "PointCloudHashing.h"
#include "PointCloudBuffer.h"
#include "../cmUtil/OptimizedBoolVector.h"
//#include "OctreeVolume.h"
#include "OctreeVolume.cpp"
//#include "Octree.h"
#include "Octree.cpp"

#include <cmGeometryProcessing/BoolMarchingCubesProcessor.h>
#include <cmGeometryProcessing/UtilProcessor.h>

//#include <k4a/k4a.h>
//#include <k4a/k4a.hpp>

// New Scan Pipeline 
#include "PointcloudTypes.h"
#include "ColorTypes.h"
#include "DepthTypes.h"
#include <cmPointCloudManagement\ImageData.h>

class ConfigReader;
//using pcl_ptr = pcl::PointCloud<pcl::PointXYZ>::Ptr;

namespace cv {
	class Mat;
}

enum ScanlineOrder {
	zyx,
	xzy,
	yxz
};

enum PointCloudMode {
	BODY_SCANNING,
	ROOM_SCANNING
};

class imPointCloudManagement_DLL_import_export PointCloudVisualizer
{
	// #### CONSTRUCTOR & DESTRUCTOR ###############
public:
	//PointCloudManager(osg::ref_ptr<GeometryManager> geometryManagerIn);
	PointCloudVisualizer(ConfigReader* configReaderIn, osg::ref_ptr<osg::Group> pointCloudGroup);
	~PointCloudVisualizer();

	// #### MEMBER VARIABLES ###############
private:
	osg::ref_ptr<osg::Group> m_pointCloudGroup;
	ConfigReader* m_configReader;

	osg::ref_ptr<osg::Geode> m_PcGeode;
	osg::ref_ptr<osg::Geode> m_PcTexGeode;
	osg::ref_ptr<osg::Geometry> m_PcGeometry;
	osg::ref_ptr<osg::Vec3Array> m_PcVertices;
	osg::ref_ptr<osg::DrawElementsUInt> m_PcIndicies;
	osg::ref_ptr<osg::Vec2Array> m_PcTexcoords;
	osg::ref_ptr<osg::Vec4Array> m_PcColorArray;
	osg::ref_ptr<osg::MatrixTransform> m_matTrans;
	osg::ref_ptr<osg::Vec3Array> m_PcNormals;
	std::vector<unsigned char>* m_PcRanks;
	
	osg::ref_ptr<osg::Geode> m_MCmeshGeode;

	OptimizedBoolVector* m_PcVaVec;
	size_t m_bodyScanDimX;
	size_t m_bodyScanDimY;
	size_t m_bodyScanDimZ;
	size_t m_bodyScanDimXY;
	osg::Vec3 m_bodyScanOrigin;
	osg::Vec3 m_bodyScanMax;
	osg::BoundingBox m_bodyScanVolume;

	float m_pointSize;
	osg::ref_ptr<osg::ImageStream> m_imageStream;
	osg::ref_ptr<osg::Texture2D> m_texture;
	//std::unordered_set<osg::Vec3, Vec3fHasher, Vec3fComparer>* m_vertexSet;	
	//std::unordered_map<osg::Vec3, int, Vec3fHasher, Vec3fComparer>* m_PcVertexMap;
	std::stack<int>* m_PcHistory;
	OctreeVolume<float>* m_octreeVolume;

	bool m_ARCoreTracking;
	bool m_useMultipleRealSenseDevices;
	PointCloudMode m_pointCloudMode;

	//PointCloudBuffers
	std::vector<PointCloudBuffer> m_PcBuffers;
	unsigned int m_bufferMaxWidth;
	unsigned int m_bufferMaxHeight;

	//Scanning configs
	bool m_filterPointCloudBasedOnOctree;
	bool m_use3Dprint;
	float m_scanningClippingDistanceNear;
	float m_scanningClippingDistanceFar;
	float m_scanningTSDFTruncationRegion;

	bool m_enableTextureProcessing = false;
	std::string m_newestTexturePath;

	osg::Geometry* m_polygonizedPointCloud = nullptr;

	osg::Matrix m_trackerToSensorMatrix;

	//Texture Projection
	unsigned int m_textureProcessingOutputResolution = 0;
	float m_textureProcessingDepthBias = -1;
	cv::Mat* m_lastProjectedTexture = nullptr;
	osg::ref_ptr<osg::ImageStream> m_polygonizedPCImageStream;
	osg::ref_ptr<osg::Texture2D> m_polygonizedPCTexture;

	// #### MEMBER FUNCTIONS ###############
	std::vector<bool>* pointCloudToVaVec(osg::Vec3& boundingBoxCenterInOut, osg::Vec3& boundingBoxExtentsInOut, unsigned int& volumeDimXOut, unsigned int& volumeDimYOut, unsigned int& volumeDimZOut);
	OptimizedBoolVector* pointCloudToVaVecMultithreaded(osg::Vec3& boundingBoxCenterInOut, osg::Vec3& boundingBoxExtentsInOut, unsigned int& volumeDimXOut, unsigned int& volumeDimYOut, unsigned int& volumeDimZOut);
	void pointCloudToVaVecTask(const osg::Vec3& cellOrigin, OptimizedBoolVector* vaVec, const unsigned int start, const unsigned int end, const unsigned int volumeDimX, const unsigned int volumeDimY);
	void setVaVecValue(const osg::Vec3& v, bool in);
	void filterVaVec(OptimizedBoolVector* vaVec, const size_t volumeDimX, const size_t volumeDimY, const size_t volumeDimZ);
	void vaVecProximityFilter(OptimizedBoolVector* vaVec, const size_t volumeDimX, const size_t volumeDimY, const size_t volumeDimZ, const int kernel, const unsigned int iterations, const unsigned int neighbourThreshold);
	void vaVecProximityFilter(OptimizedBoolVector* vaVec, const size_t volumeDimX, const size_t volumeDimY, const size_t volumeDimZ, const int kernel, const unsigned int iterations, const float proximityThreshold);
	void vaVecFillVolume(OptimizedBoolVector* vaVec, const size_t volumeDimX, const size_t volumeDimY, const size_t volumeDimZ);
	void vaVecVolumeScanlineTask(const OptimizedBoolVector* vaVec, OptimizedBoolVector* vaVecResult, ScanlineOrder order, const size_t volumeDimX, const size_t volumeDimY, const size_t volumeDimZ, const unsigned int collapseDistance);
	void vaVecMicroHoleFilling(OptimizedBoolVector* vaVec, OptimizedBoolVector* vaVecTemp, const size_t volumeDimX, const size_t volumeDimY, const size_t volumeDimZ, const bool removeOutlier);
	void vaVecClearBorders(OptimizedBoolVector* vaVec, const size_t volumeDimX, const size_t volumeDimY, const size_t volumeDimZ);

	bool generateExportPath(const std::string& path);
	std::string generateExportFilePath(const std::string& path, const std::string& ext);

	void PointCloudVisualizer::generatePartialTextureFromImageData(const unsigned int start, const unsigned int slice, float* data, float* weights, ImageData* img, cv::Mat* colorData, cv::Mat* depthData, const unsigned int texSize, const unsigned int quadsPerRow, const unsigned int edgeLength, const osg::Vec3Array* vertices, const osg::Vec3Array* normals, const osg::Vec2Array* uvCoords, const osg::DrawElementsUInt* triangles);
public:
	void visualizeRSPointCloud(/*rs2::video_frame colorFrameIn*/);

	void processAKPointCloud();
	void clearPointCloudFull();
	void clearPointCloudData();
	void clearPointCloudDisplay();
	/* ### START - Old Scan Pipeline ###
	void processRSPointCloud(rs2::points pointsIn, osg::Matrix matrixTrackingIn, rs2::video_frame colorFrameIn, rs2::depth_frame depthFrameIn);
	void insertRSPoints(const size_t size, const rs2::vertex* vertices, const rs2::texture_coordinate* tex_coords, const osg::Matrix trackerMatrix, unsigned int* bufferSize, osg::Vec3* vertexBuffer, osg::Vec2* texCoordBuffer, osg::Vec4* colorBuffer);
	void ddaTraversalForRSPoints(const size_t size, const rs2::vertex* vertices, const osg::Matrix sensorMatrix);
	// ### END - Old Scan Pipeline ### */
	void processPointCloud(int bufferIndex, ColorData* colorData, PointcloudData* pointcloudData, osg::Matrix scanDevicePose, ViewFrustum& debugFrustum, bool writeTSDF); // NEW FUNCTION
	void processPointCloud(unsigned int bufferIndex, DepthData* depthData, ColorData* colorData, PointcloudData* pointcloudData, osg::Matrix scanDevicePose, bool writeTSDF);
	void insertPoints(const size_t start, const size_t size, DepthData* depthData, ColorData* colorData, PointcloudData* pointcloudData, osg::Matrix scanDevicePose, unsigned int* bufferSize, osg::Vec3* vertexBuffer, osg::Vec2* texCoordBuffer, osg::Vec4* colorBuffer, bool writeTSDF);
	void ddaTraversalForPoints(const size_t start, const size_t size, PointcloudData* pointcloudData, ColorData* colorData, const osg::Matrix scanDevicePose);
	//void pushMatrix(osg::ref_ptr<osg::MatrixTransform> matrixTransformIn);
	//void pushMatrix(osg::Matrix matrixTransformIn);
	//osg::Matrix getTransformationMatrixFromBuffer();
	osg::ref_ptr<osg::Group> getRootNode();

	// View Frustum Octree Intersection
	//bool checkIntersection();


	void savePointCloudWithOSG(std::string savePathIn); //Uses the member m_PcGeometry for saving
	void savePointCloudWithOSG(std::string savePathIn, osg::ref_ptr<osg::OpenMeshGeometry> openMeshGeo);
	pcl::PointCloud<pcl::PointXYZRGB>::Ptr convertPointCloudToPCL();
	pcl::PointCloud<pcl::PointXYZRGB>::Ptr convertPointCloudToPCL(osg::ref_ptr<osg::OpenMeshGeometry> openMeshGeo);

	void loadPointCloudWithOSG(osg::ref_ptr<osg::OpenMeshGeometry> openMeshGeo, std::string loadPath);
	void convertPointCloudToOSG(osg::ref_ptr<osg::OpenMeshGeometry> openMeshGeo, pcl::PointCloud<pcl::PointXYZRGB>::Ptr cloud);

	bool bufferDataAvailable();
	void updatePointCloudBuffers(unsigned int devicesPerScan, unsigned int newMaxWidth, unsigned int newMaxHeight);
	bool revertSnapshot();
	/* ### START - Old Scan Pipeline ###
	void renderVideoTexture(rs2::video_frame colorFrameIn);
	// ### END - Old Scan Pipeline ### */
	void renderVideoTexture(ColorData* colorData);
	void renderVideoTexture(osg::Image* color_image);
	//void polygonizePointCloud(const osg::Vec3& origin, const osg::Vec3& extents);
	void polygonizePointCloud(OptimizedBoolVector* vaVec, const osg::Vec3& boundingBoxMin, const unsigned int volumeDimX, const unsigned int volumeDimY, const unsigned int volumeDimZ);
	void polygonizePointCloudFromOctree();
	void polygonizePointCloudForBodyScanning();
	void polygonizePointCloudMultithreaded(const osg::Vec3& origin, const osg::Vec3& extents);
	void filterRSPointCloud(unsigned int kernel, const unsigned int linkThreshold);
	void detectBoundingBox(osg::Vec3 &origin, osg::Vec3 &extents, osg::Vec3 &size, osg::Vec3 &min, osg::Vec3 &max);
	void detectVolume();
	void volumeScanline(ScanlineOrder order, const osg::Vec3& min, const unsigned int uSize, const unsigned int vSize, const unsigned int wSize, std::vector<char>* vaVec);
	void volumeScanlinePart(ScanlineOrder order, const osg::Vec3& min, const unsigned int xSize, const unsigned int ySize, const unsigned int uFrom, const unsigned int uTo, const unsigned int vFrom, const unsigned int vTo, const unsigned int wTo, std::vector<char>* vaVec);
	
	void generatePointCloudFromImageData(const std::vector<ImageData*>* imageData);

	cv::Mat* generateTextureFromImageData(osg::Geometry* geom, const std::vector<ImageData*>* imageData, int texSize, int margin);

	//inline OctreeVolume<float>* getOctreeVolume() { return m_octreeVolume; };
	OctreeVolume<float>* getOctreeVolume();

	bool exportPointCloudBinary();
	bool exportPointCloudAscii();
	void exportPoints(const size_t start, const size_t count, char* bytes_output);
	
	bool exportPointCloudToPCD(bool binary = false);
	bool exportPointCloudToPLY(bool binary = false);

	pcl::PointCloud<pcl::PointXYZRGB>* convertToPCL();
	void copyDataSliceToPCL(pcl::PointCloud<pcl::PointXYZRGB>* pcl, size_t start, size_t count);

	bool exportPointCloudMesh(std::string fileName = "");
	bool exportPointCloudTexture(const std::vector<ImageData*>* imageData, std::string fileName = "");

	/* ### START - Old Scan Pipeline ###
	void paintKinectImageOnQuad(k4a_image_t* color_image);
	// ### END - Old Scan Pipeline ### */

	void togglePointCloudDisplay();
	void toggleMarchingCubesDisplay();
	void clearMarchingCubesMeshes();
	void setPointCloudDisplay(bool displayState);
	void setMarchingCubesDisplay(bool displayState);
};
