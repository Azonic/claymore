#pragma once
//#include "Octree.h"
#include "Octree.cpp"
#include "osg\Vec3f"
#include "osg\Vec3i"
#include "../cmUtil/OptimizedBoolVector.h"
#include "../cmUtil/Ray.h"
#include <vector>

template<class T>
class OctreeVolume {
private:
	osg::Vec3f m_position;
	//Volume must be cubic for the octree to work properly (designed that way for optimization purposes)
	float m_size;
	//Minimal size of one cell
	float m_targetGridSize;
	float m_gridSize;
	Octree<T> m_octreeRoot;
	float m_baseTruncationRegion;
	float m_baseTruncationRegionDistance;
public:
	OctreeVolume(T initialValue, osg::Vec3f position, const float size, const float targetGridSize, const float baseTruncationRegion, const float baseTruncationRegionDistance);
	~OctreeVolume();
	osg::Vec3f getPosition() const;
	float getSize() const;
	float getExtents() const;
	Octree<T>& getOctree();
	float makeRelative(const float coord, const unsigned int axis) const;
	osg::Vec3f makeRelative(const osg::Vec3f& xyz) const;
	float roundToGrid(const float value);
	osg::Vec3f roundToGrid(const osg::Vec3f& value);
	OptimizedBoolVector* toVaVec();
	std::vector<std::pair<float, unsigned char>>* toVaVecFloat();
	template<bool threadSafe = false>
	void ddaTraverse(Ray ray, OctreeData* octreeData, const bool stopAtMaxTrunc, const float farClipping = -1.f);
	void ddaTraverseDebug(Ray ray);
	void writeOctreeToVaVec(Octree<T>* octree, OptimizedBoolVector* vaVec, unsigned int x, unsigned int y, unsigned int z);
private:
	template<bool threadSafe = false>
	bool ddaRecursion(Octree<T>* parent, const osg::Vec3f startingPoint, const osg::Vec3f direction, osg::Vec3i xyz, osg::Vec3f tMaxPrev, osg::Vec3i prevStep, const osg::Vec3f tDeltaGlobal, const osg::Vec3i stepGlobal, Ray ray, OctreeData* octreeData, const bool stopAtMaxTrunc, const float farClipping = -1.f);
	void ddaRecursionDebug(Octree<T>* parent, const osg::Vec3f startingPoint, const osg::Vec3f direction, osg::Vec3i xyz, osg::Vec3f tMaxPrev, osg::Vec3i prevStep, const osg::Vec3f tDeltaGlobal, const osg::Vec3i stepGlobal, Ray ray);
};