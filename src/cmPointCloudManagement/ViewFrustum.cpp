#pragma once

#include "ViewFrustum.h"

ViewFrustum::Plane::Plane(const osg::Vec3f& p, const osg::Vec3f& n) : _n(n) {
	_n.normalize();
	_d = _n * p;
}

ViewFrustum::Plane::Plane(const osg::Vec3f& p1, const osg::Vec3f& p2, const osg::Vec3f& p3) : _n((p2 - p1) ^ (p3 - p1)) {
	_n.normalize();
	_d = _n * p1;
}

float ViewFrustum::Plane::signedDistance(osg::Vec3f x) const {
	return _n * x - _d;
}

ViewFrustum::ViewFrustum(osg::Vec3f& near_NE, osg::Vec3f& near_SE, osg::Vec3f& near_SW, osg::Vec3f& near_NW, osg::Vec3f& far_NE, osg::Vec3f& far_SE, osg::Vec3f& far_SW, osg::Vec3f& far_NW) : 
	_boundaries{ Plane(near_SW, near_SE, near_NW), 
				 Plane(far_SE, far_SW, far_NE),
				 Plane(near_NW, near_NE, far_NW),
				 Plane(near_SE, near_SW, far_SE),
				 Plane(near_SW, near_NW, far_SW),
				 Plane(near_SE, far_SE, near_NE)},
	_corners{ near_NE, near_SE, near_SW, near_NW, far_NE, far_SE, far_SW, far_NW },
	_normals{ _boundaries[0].n(), _boundaries[1].n(), _boundaries[2].n(), _boundaries[3].n(), _boundaries[4].n(), _boundaries[5].n() }
{}

bool ViewFrustum::contains(osg::Vec3f& p) {
	for (int i = 0; i < 6; ++i) {
		if (_boundaries[i].signedDistance(p) >= 0.f) return false;
	}
	return true;
}

bool ViewFrustum::intersectCubicAABB(osg::Vec3f& p, float l) {
	for (int i = 0; i < 6; ++i) {
		const osg::Vec3f& n = _boundaries[i].n();
		osg::Vec3f q(p.x() + l * (n.x() > 0 ? -1 : 1), p.y() + l * (n.y() > 0 ? -1 : 1), p.z() + l * (n.z() > 0 ? -1 : 1));
		if (_boundaries[i].signedDistance(q) >= 0.f) return false;
	}
	return true;
}

const osg::Vec3f* ViewFrustum::corners() {
	return _corners;
}

const osg::Vec3f* ViewFrustum::normals() {
	return _normals;
}
