#pragma once

#include <osg/Vec3f>
#include <osg/Matrix>

const unsigned int RS_WIDTH = 640; //Is read from the config.xml now
const unsigned int RS_HEIGHT = 480; //Is read from the config.xml now
const unsigned int RS_FPS = 60; //Is read from the config.xml now

const unsigned int RS_MAX_LINKS = 5;
const osg::Vec3f RS_FORWARD = osg::Vec3f(0.0f, -1.0f, 0.0f);
//const osg::Matrixf RS_TO_TRACKER_MATRIX = osg::Matrixf::rotate(osg::DegreesToRadians(90.0f), osg::Vec3f(1, 0, 0));
const osg::Matrixf RS_TO_TRACKER_MATRIX = osg::Matrixf::rotate(osg::DegreesToRadians(90.0f), osg::Vec3f(0.0f, 0.0f, 0.0f));

//const osg::Matrixf RS_TRACKER_OFFSET_MATRIX = osg::Matrixf(
//	1, 0, 0, 0,
//	0, 1, 0, 0,
//	0, 0, 1, 0,
//	//0, 0, 0, 1);
//	-0.02f, -0.0481501f, 0.0257426f, 1); //As far as I understood Intel's specfication sheet, this one should fix our offset issues (although it doesn't)

//TODO1: Put into config.xml
//const osg::Matrixf RS_TRACKER_OFFSET_MATRIX = osg::Matrixf(
//	1.0f, 0.0f, 0.0f, 0.0f,
//	0.0f, 1.0f, 0.0f, 0.0f,
//	0.0f, 0.0f, 1.0f, 0.0f,
//	0.016f, 0.0f, 0.2f, 1.0f);
//const osg::Matrixf RS_OFFSET_CORRECTION_MATRIX = RS_TO_TRACKER_MATRIX * RS_TRACKER_OFFSET_MATRIX;

const float GRID = 0.005f; // Ideal for exam: 0.007f  //Is read from the config.xml now
const float GRID_OFFSET = GRID * 0.5f;

const osg::Vec3f BODY_SCAN_AREA_ORIGIN = osg::Vec3f(-1, -1, -0.4f);
const osg::Vec3f BODY_SCAN_AREA_MAX = osg::Vec3f(1, 1, 0.4f);