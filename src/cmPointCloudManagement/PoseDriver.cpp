#include "PoseDriver.h"

PoseDriver::PoseDriver(const osg::Matrix& sensorToDriver) : m_sensorToDriver(sensorToDriver) { }

PoseDriver::~PoseDriver() { }

osg::Matrix PoseDriver::getSensorToDriverMatrix() {
    return m_sensorToDriver;
}
