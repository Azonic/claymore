#pragma once

#include "OctreeVolume.h"

#include <algorithm>

template<class T>
OctreeVolume<T>::OctreeVolume(T initialValue, osg::Vec3f position, const float size, const float targetGridSize, const float baseTruncationRegion, const float baseTruncationRegionDistance)
	: m_position{ position },
	m_size{ size },
	m_targetGridSize{ targetGridSize },
	m_octreeRoot(initialValue,
	(int)log2(size / targetGridSize), 0),
	m_baseTruncationRegion{baseTruncationRegion}, 
	m_baseTruncationRegionDistance{baseTruncationRegionDistance}
{
	m_gridSize = size / powf(2.0f, (int)m_octreeRoot.getMaxDepth());
}

template<class T>
OctreeVolume<T>::~OctreeVolume() {
}

template<class T>
osg::Vec3f OctreeVolume<T>::getPosition() const {
	return m_position;
}

template<class T>
float OctreeVolume<T>::getSize() const {
	return m_size;
}

template<class T>
float OctreeVolume<T>::getExtents() const {
	return m_size * 0.5f;
}

template<class T>
Octree<T>& OctreeVolume<T>::getOctree() {
	return m_octreeRoot;
}

template<class T>
float OctreeVolume<T>::makeRelative(const float coord, const unsigned int axis) const {
	return (coord - m_position[axis] + getExtents()) / m_size;
}

template<class T>
osg::Vec3f OctreeVolume<T>::makeRelative(const osg::Vec3f& xyz) const {
	return osg::Vec3f((xyz.x() - m_position.x() + getExtents()) / m_size,
		(xyz.y() - m_position.y() + getExtents()) / m_size,
		(xyz.z() - m_position.z() + getExtents()) / m_size);
}

template<class T>
float OctreeVolume<T>::roundToGrid(const float value) {
	return m_gridSize * ((floorf(value / m_gridSize)) + 0.5f);
}

template<class T>
osg::Vec3f OctreeVolume<T>::roundToGrid(const osg::Vec3f& value) {
	return osg::Vec3f(roundToGrid(value.x()), roundToGrid(value.y()), roundToGrid(value.z()));
}

template<class T>
OptimizedBoolVector* OctreeVolume<T>::toVaVec() {
	std::cout << "----------In toVaVec------------" << std::endl;
	unsigned int s = (1 << m_octreeRoot.getMaxDepth()) + 1;
	size_t voxelCount = s * s * s;// 1ULL << (3u * m_octreeRoot.getMaxDepth()) + 3; // 2 ^ (maxDepth * 3)
	std::cout << "voxelCount: " << voxelCount << std::endl;
	OptimizedBoolVector* vaVec = new OptimizedBoolVector(voxelCount);
	//writeOctreeToVaVec(&m_octreeRoot, vaVec);

	std::cout << "s: " << s << std::endl;
	for (unsigned int z = 0; z < s; z++)
	{
		for (unsigned int y = 0; y < s; y++)
		{
			for (unsigned int x = 0; x < s; x++)
			{
				Octree<T>* octree = m_octreeRoot.getOctreeLocal(x, y, z, false);
				unsigned char tValue = octree->getDepth();
				if (tValue > 8)
				{
					vaVec->set(x + y * s + z * s * s, true); // => vaVec->push_back(true);
				}
				else
				{
					vaVec->set(x + y * s + z * s * s, false);
				}
			}
		}
	}
	return vaVec;
}


template<class T>
std::vector<std::pair<float, unsigned char>>* OctreeVolume<T>::toVaVecFloat() {
	std::cout << "----------In toVaVec for std::vector<float>------------" << std::endl;
	unsigned int s = (1 << m_octreeRoot.getMaxDepth()) + 1;
	size_t voxelCount = (size_t) s * s * s;// 1ULL << (3u * m_octreeRoot.getMaxDepth()) + 3; // 2 ^ (maxDepth * 3)
	std::cout << "voxelCount: " << voxelCount << std::endl;
	//OptimizedBoolVector* vaVec = new OptimizedBoolVector(voxelCount);
	std::vector<std::pair<float, unsigned char>>* vaVec = new std::vector<std::pair<float, unsigned char>>();
	vaVec->resize(voxelCount);

	//std::cout << "s: " << s << std::endl;
	for (unsigned int z = 0; z < s; z++)
	{
		for (unsigned int y = 0; y < s; y++)
		{
			for (unsigned int x = 0; x < s; x++)
			{
				Octree<T>* octree = m_octreeRoot.getOctreeLocal(x, y, z, false);
				//if (octree->getDepth() > (m_octreeRoot.getMaxDepth()-1))
				//{
					std::pair<float, unsigned char>& pair = vaVec->at((size_t) x + y * s + z * s * s);
					pair.first = octree->getValue();
					pair.second = octree->getDepth();
				//}
				/*else
				{
					vaVec->at(x + y * s + z * s * s) = 1.0f;
				}*/
				//vaVec->set(x + y * s + z * s * s, octree.getValue()); // => vaVec->push_back(true);
			}
		}
	}
	return vaVec;
}




//TODO 3: Potentially the ddaTraversal should be moved over to the PointCloudVisualizer. 
//		  Since the Octree & OctreeVolume are kindof our dataModel this function feels a bit too specific to be placed inside the model itself.
//		  Besides, we handle the blending of the TSDF values inside this function aswell, which is more of a framework component.
template<class T>
template<bool threadSafe>
void OctreeVolume<T>::ddaTraverse(Ray ray, OctreeData* octreeData, const bool stopAtMaxTrunc, const float farClipping) {
	if (!m_octreeRoot.hasChildren()) return; //TODO 2: If octree has no children it has to be marked as hit itself as long as the ray intersects it; done like this it atleast prevents crashing when the function is called for an empty octreeVolume

	//std::cout << "ddaTraversal start" << std::endl;
	//TimeWriter* tw = new TimeWriter();

	//tw->startMetering("ddaTraverse", false, false);
	unsigned int max = 1u << m_octreeRoot.getMaxDepth();
	unsigned int gridScale = 1u << (m_octreeRoot.getMaxDepth() - 1);

	osg::Vec3f& direction = ray.getDirection();

	osg::Vec3i step(
		direction.x() >= 0 ? 1 : -1,
		direction.y() >= 0 ? 1 : -1,
		direction.z() >= 0 ? 1 : -1
	);
	//osg::Vec3i initialStep(0, 0, 0); //first prevStep

	//Bounds bounds = new Bounds(transform.position, Vector3.one * m_size);
	osg::Vec3f startingPoint = ray.getOrigin();
	/*if (!bounds.Contains(startingPoint)) {
		float d;
		bounds.IntersectRay(new Ray(startingPoint, ray.Direction), out d);
		startingPoint = startingPoint + ray.Direction * d;


		Vector3 startingPointOffset = ray.Direction * d;
		if (Mathf.Abs(startingPointOffset.x) >= Mathf.Abs(startingPointOffset.y)) {
			if (Mathf.Abs(startingPointOffset.x) >= Mathf.Abs(startingPointOffset.z)) initialStep = new Vector3Int(step.x, 0, 0);
			else initialStep = new Vector3Int(0, 0, step.z);
		}
		else {
			if (Mathf.Abs(startingPointOffset.y) >= Mathf.Abs(startingPointOffset.z)) initialStep = new Vector3Int(0, step.y, 0);
			else initialStep = new Vector3Int(0, 0, step.z);
		}
	}*/
	osg::Vec3f localStartingPoint = ((startingPoint - m_position) * (1.0f / m_size) + osg::Vec3f(.5f, .5f, .5f)) * max;
	//osg::Vec3f localStartingPoint = (transform.InverseTransformPoint(startingPoint) + osg::Vec3f(.5f, .5f, .5f)) * max;
	//TODO 4:
	// getParentalNodePath is the equivalent to Unity's InverseTransformPoint and must be applied at m_position, 
	// but that is currently not possible, because it only a VEc3f and must rather be a scenegraph object 
	

	//Vector3Int xyz = new Vector3Int(
	//    (int) (localStartingPoint.x / gridScale) * gridScale, 
	//    (int) (localStartingPoint.y / gridScale) * gridScale, 
	//    (int) (localStartingPoint.z / gridScale) * gridScale
	//);
	char n = m_octreeRoot.getMaxDepth() - 1;
	osg::Vec3i xyz(
		((int) localStartingPoint.x()) >> n << n,
		((int) localStartingPoint.y()) >> n << n,
		((int) localStartingPoint.z()) >> n << n
	);

	if (xyz.x() >= max) xyz.x() = max - gridScale;
	if (xyz.y() >= max) xyz.y() = max - gridScale;
	if (xyz.z() >= max) xyz.z() = max - gridScale;

	osg::Vec3f tDelta(
		1.0f / direction.x() * step.x(),
		1.0f / direction.y() * step.y(),
		1.0f / direction.z() * step.z()
	);

	ddaRecursion<threadSafe>(&m_octreeRoot, localStartingPoint, direction, xyz, osg::Vec3f(0, 0, 0), osg::Vec3i(0, 0, 0), tDelta, step, ray, octreeData, stopAtMaxTrunc, farClipping);
	//tw->endMetering();
	//delete tw;
	//std::cout << "### m_ddaVoxelCount: " << m_ddaVoxelCount << std::endl;
	//std::cout << "ddaTraversal end" << std::endl;
}

template<class T>
template<bool threadSafe>
bool OctreeVolume<T>::ddaRecursion(Octree<T>* parent, 
	const osg::Vec3f startingPoint,
	const osg::Vec3f direction,
	osg::Vec3i xyz,
	osg::Vec3f tMaxPrev,
	osg::Vec3i prevStep,
	const osg::Vec3f tDeltaGlobal,
	const osg::Vec3i stepGlobal,
	Ray ray,
	OctreeData* octreeData,
	const bool stopAtMaxTrunc,
	const float farClipping) 
{
	unsigned char targetDepth = parent->getDepth() + 1u; // Depth of children
	unsigned int gridScale = 1u << (parent->getMaxDepth() - targetDepth); //gridScale = 2 ^ (maxDepth - targetDepth)
	osg::Vec3f tDelta = tDeltaGlobal * gridScale;
	osg::Vec3f step = osg::Vec3f(stepGlobal.x(), stepGlobal.y(), stepGlobal.z()) * gridScale;

	unsigned int n = parent->getMaxDepth() - parent->getDepth();
	osg::Vec3i min(
		(xyz.x() >> n) << n,
		(xyz.y() >> n) << n,
		(xyz.z() >> n) << n
	);

	osg::Vec3i max = min + osg::Vec3i(1, 1, 1) * (1u << n);

	osg::Vec3f tMax(
		direction.x() == .0f ? UINT_MAX : ((stepGlobal.x() >= 0 ? gridScale : 0) + xyz.x() - startingPoint.x()) / direction.x(),
		direction.y() == .0f ? UINT_MAX : ((stepGlobal.y() >= 0 ? gridScale : 0) + xyz.y() - startingPoint.y()) / direction.y(),
		direction.z() == .0f ? UINT_MAX : ((stepGlobal.z() >= 0 ? gridScale : 0) + xyz.z() - startingPoint.z()) / direction.z()
	);

	int iterations = 0;
	do {
		unsigned char childIndex = parent->calculateChildIndex(xyz.x(), xyz.y(), xyz.z());
		Octree<T>* octree = parent->getOctree(childIndex);
		//octree->setValue(0.0f);
		//octree->deselect();

		unsigned int sizeInt = 1u << parent->getMaxDepth();
		float t = tMaxPrev.x() * abs(prevStep.x()) + tMaxPrev.y() * abs(prevStep.y()) + tMaxPrev.z() * abs(prevStep.z());

		float tGlobal = t / sizeInt * m_size;
		if (farClipping >= 0.f && tGlobal > farClipping) return false;
		if (octree->hasChildren()) {
			int m = octree->getMaxDepth() - octree->getDepth() - 1;
			osg::Vec3i xyzNew(
				((int)(startingPoint.x() + direction.x() * t + .5f * prevStep.x())) >> m << m,
				((int)(startingPoint.y() + direction.y() * t + .5f * prevStep.y())) >> m << m,
				((int)(startingPoint.z() + direction.z() * t + .5f * prevStep.z())) >> m << m
			);

			if (!ddaRecursion<threadSafe>(octree, startingPoint, direction, xyzNew, tMaxPrev, prevStep, tDeltaGlobal, stepGlobal, ray, octreeData, stopAtMaxTrunc, farClipping)) return false;
		} else {
			osg::Vec3f octreeGlobal3DCentrum(
				(xyz.x() / (float)sizeInt - 0.5f) * m_size + m_position.x(),
				(xyz.y() / (float)sizeInt - 0.5f) * m_size + m_position.y(),
				(xyz.z() / (float)sizeInt - 0.5f) * m_size + m_position.z()
			);
			unsigned short currentWeight = octree->getWeight();

			//bool ignoreDistance = ray.getDistance() < 0.f;

			float currentTsdf = octree->getValue();

			float sdf = ray.getDistance() - (ray.getOrigin() - octreeGlobal3DCentrum).length();
			float tsdf = sdf >= 0.0f ? std::min<float>(1.0f, sdf / m_baseTruncationRegion) : std::max<float>(-1.0f, sdf / m_baseTruncationRegion); 
			unsigned short newWeight = std::min<unsigned int>(USHRT_MAX, currentWeight + 1u);
			float tsdfAvg = (currentTsdf * (float)currentWeight + tsdf) / (float)newWeight;

			//bool done = tsdf < 0.f && abs(sdf) > m_truncationRegion;// Only integrate the new tsdf value if it's measured infront of the current surface or if it's inside the truncation region
			//if (done) return false;
			octree->select<threadSafe>();
			
			if (sdf > 0.f || abs(sdf) <= m_baseTruncationRegion) { 
				octree->setValue<threadSafe>(tsdfAvg);
				octree->setWeight<threadSafe>(newWeight);
				// ### Start - Blend OctreeData ###
				if (octreeData) {
					OctreeData* data = octree->getData<threadSafe>();
					if (!data) octree->setData<threadSafe>(*octreeData);
					else {
						if (threadSafe) octree->writeLock();
						data->_r = (data->_r * (float)currentWeight + octreeData->_r) / (float)newWeight;
						data->_g = (data->_g * (float)currentWeight + octreeData->_g) / (float)newWeight;
						data->_b = (data->_b * (float)currentWeight + octreeData->_b) / (float)newWeight;
						if (threadSafe) octree->writeUnlock();
					}
				}
				// ### End - Blend OctreeData ###
			} else if (stopAtMaxTrunc) return false;
		}
		iterations++;
		tMaxPrev = tMax;
		if (tMax.x() < tMax.y()) {
			if (tMax.x() < tMax.z()) {
				xyz.x() += step.x();
				tMax.x() += tDelta.x();
				prevStep.set(stepGlobal.x(), 0, 0);
			}
			else {
				xyz.z() += step.z();
				tMax.z() += tDelta.z();
				prevStep.set(0, 0, stepGlobal.z());
			}
		}
		else {
			if (tMax.y() < tMax.z()) {
				xyz.y() += step.y();
				tMax.y() += tDelta.y();
				prevStep.set(0, stepGlobal.y(), 0);
			}
			else {
				xyz.z() += step.z();
				tMax.z() += tDelta.z();
				prevStep.set(0, 0, stepGlobal.z());
			}
		}
	} while (iterations <= 4 && // A straight line can only intersect at maximum 4 of the 8 children of an octree
		xyz.x() >= min.x() &&
		xyz.y() >= min.y() &&
		xyz.z() >= min.z() &&
		xyz.x() < max.x() &&
		xyz.y() < max.y() &&
		xyz.z() < max.z()
	);
	return true;
}



template<class T>
void OctreeVolume<T>::ddaTraverseDebug(Ray ray) {
	if (!m_octreeRoot.hasChildren()) return; //TODO 2: If octree has no children it has to be marked as hit itself as long as the ray intersects it; done like this it atleast prevents crashing when the function is called for an empty octreeVolume

	unsigned int max = 1u << m_octreeRoot.getMaxDepth();
	unsigned int gridScale = 1u << (m_octreeRoot.getMaxDepth() - 1);

	osg::Vec3f& direction = ray.getDirection();

	osg::Vec3i step(
		direction.x() >= 0 ? 1 : -1,
		direction.y() >= 0 ? 1 : -1,
		direction.z() >= 0 ? 1 : -1
	);
	//osg::Vec3i initialStep(0, 0, 0); //first prevStep

	//Bounds bounds = new Bounds(transform.position, Vector3.one * m_size);
	osg::Vec3f startingPoint = ray.getOrigin();
	/*if (!bounds.Contains(startingPoint)) {
		float d;
		bounds.IntersectRay(new Ray(startingPoint, ray.Direction), out d);
		startingPoint = startingPoint + ray.Direction * d;


		Vector3 startingPointOffset = ray.Direction * d;
		if (Mathf.Abs(startingPointOffset.x) >= Mathf.Abs(startingPointOffset.y)) {
			if (Mathf.Abs(startingPointOffset.x) >= Mathf.Abs(startingPointOffset.z)) initialStep = new Vector3Int(step.x, 0, 0);
			else initialStep = new Vector3Int(0, 0, step.z);
		}
		else {
			if (Mathf.Abs(startingPointOffset.y) >= Mathf.Abs(startingPointOffset.z)) initialStep = new Vector3Int(0, step.y, 0);
			else initialStep = new Vector3Int(0, 0, step.z);
		}
	}*/
	osg::Vec3f localStartingPoint = ((startingPoint - m_position) * (1.0f / m_size) + osg::Vec3f(.5f, .5f, .5f)) * max;
	//osg::Vec3f localStartingPoint = (transform.InverseTransformPoint(startingPoint) + osg::Vec3f(.5f, .5f, .5f)) * max;
	//TODO 4:
	// getParentalNodePath is the equivalent to Unity's InverseTransformPoint and must be applied at m_position, 
	// but that is currently not possible, because it only a VEc3f and must rather be a scenegraph object 


	//Vector3Int xyz = new Vector3Int(
	//    (int) (localStartingPoint.x / gridScale) * gridScale, 
	//    (int) (localStartingPoint.y / gridScale) * gridScale, 
	//    (int) (localStartingPoint.z / gridScale) * gridScale
	//);
	char n = m_octreeRoot.getMaxDepth() - 1;
	osg::Vec3i xyz(
		((int)localStartingPoint.x()) >> n << n,
		((int)localStartingPoint.y()) >> n << n,
		((int)localStartingPoint.z()) >> n << n
	);

	if (xyz.x() >= max) xyz.x() = max - gridScale;
	if (xyz.y() >= max) xyz.y() = max - gridScale;
	if (xyz.z() >= max) xyz.z() = max - gridScale;

	osg::Vec3f tDelta(
		1.0f / direction.x() * step.x(),
		1.0f / direction.y() * step.y(),
		1.0f / direction.z() * step.z()
	);

	ddaRecursionDebug(&m_octreeRoot, localStartingPoint, direction, xyz, osg::Vec3f(0, 0, 0), osg::Vec3i(0, 0, 0), tDelta, step, ray);
}

template<class T>
void OctreeVolume<T>::ddaRecursionDebug(Octree<T>* parent,
	const osg::Vec3f startingPoint,
	const osg::Vec3f direction,
	osg::Vec3i xyz,
	osg::Vec3f tMaxPrev,
	osg::Vec3i prevStep,
	const osg::Vec3f tDeltaGlobal,
	const osg::Vec3i stepGlobal,
	Ray ray)
{
	unsigned char targetDepth = parent->getDepth() + 1u; // Depth of children
	unsigned int gridScale = 1u << (parent->getMaxDepth() - targetDepth); //gridScale = 2 ^ (maxDepth - targetDepth)
	osg::Vec3f tDelta = tDeltaGlobal * gridScale;
	osg::Vec3f step = osg::Vec3f(stepGlobal.x(), stepGlobal.y(), stepGlobal.z()) * gridScale;

	unsigned int n = parent->getMaxDepth() - parent->getDepth();
	osg::Vec3i min(
		(xyz.x() >> n) << n,
		(xyz.y() >> n) << n,
		(xyz.z() >> n) << n
	);

	osg::Vec3i max = min + osg::Vec3i(1, 1, 1) * (1u << n);

	osg::Vec3f tMax(
		direction.x() == .0f ? UINT_MAX : ((stepGlobal.x() >= 0 ? gridScale : 0) + xyz.x() - startingPoint.x()) / direction.x(),
		direction.y() == .0f ? UINT_MAX : ((stepGlobal.y() >= 0 ? gridScale : 0) + xyz.y() - startingPoint.y()) / direction.y(),
		direction.z() == .0f ? UINT_MAX : ((stepGlobal.z() >= 0 ? gridScale : 0) + xyz.z() - startingPoint.z()) / direction.z()
	);

	int iterations = 0;
	do {
		unsigned char childIndex = parent->calculateChildIndex(xyz.x(), xyz.y(), xyz.z());
		Octree<T>* octree = parent->getOctree(childIndex);
		if (octree->hasChildren()) {
			float t = tMaxPrev.x() * abs(prevStep.x()) + tMaxPrev.y() * abs(prevStep.y()) + tMaxPrev.z() * abs(prevStep.z());
			int m = octree->getMaxDepth() - octree->getDepth() - 1;
			osg::Vec3i xyzNew(
				((int)(startingPoint.x() + direction.x() * t + .5f * prevStep.x())) >> m << m,
				((int)(startingPoint.y() + direction.y() * t + .5f * prevStep.y())) >> m << m,
				((int)(startingPoint.z() + direction.z() * t + .5f * prevStep.z())) >> m << m
			);
			ddaRecursionDebug(octree, startingPoint, direction, xyzNew, tMaxPrev, prevStep, tDeltaGlobal, stepGlobal, ray);
		} else octree->select();
		iterations++;
		tMaxPrev = tMax;
		if (tMax.x() < tMax.y()) {
			if (tMax.x() < tMax.z()) {
				xyz.x() += step.x();
				tMax.x() += tDelta.x();
				prevStep.set(stepGlobal.x(), 0, 0);
			} else {
				xyz.z() += step.z();
				tMax.z() += tDelta.z();
				prevStep.set(0, 0, stepGlobal.z());
			}
		} else {
			if (tMax.y() < tMax.z()) {
				xyz.y() += step.y();
				tMax.y() += tDelta.y();
				prevStep.set(0, stepGlobal.y(), 0);
			} else {
				xyz.z() += step.z();
				tMax.z() += tDelta.z();
				prevStep.set(0, 0, stepGlobal.z());
			}
		}
	} while (iterations <= 4 && // A straight line can only intersect at maximum 4 of the 8 children of an octree
		xyz.x() >= min.x() &&
		xyz.y() >= min.y() &&
		xyz.z() >= min.z() &&
		xyz.x() < max.x() &&
		xyz.y() < max.y() &&
		xyz.z() < max.z()
	);
}



/*template<class T>
void OctreeVolume<T>::writeOctreeToVaVec(Octree<T>* octree, OptimizedBoolVector* vaVec, unsigned int x, unsigned int y, unsigned int z) {
	if (octree->hasChildren()) { // No need to write anything, the child function will write the appropriate values
		for (int i = 0; i < 8; i++) {

			writeOctreeToVaVec(octree->getOctree(i), vaVec);
		}
	}
	else { // no children -> write the octree's value into all the voxels this octree represent; for max_Depth this should just be a single value

	}
}*/
