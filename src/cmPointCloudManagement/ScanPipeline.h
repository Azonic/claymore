#pragma once

#include <vector>
#include <future>

#include "ScanDevice.h"
#include "ColorTypes.h"

/**
 * @brief Manages all registered devices for scanning via the generalized ScanDevice-interface.
*/
class ScanPipeline {
public:
	enum class ScanResult {
		SUCCESS,
		TIMEOUT,
		EMPTY_DEVICE_LIST,
		DEVICE_IS_ALREADY_SCANNING,
		FAILURE
	};
	enum class PointcloudCalculationResult {
		SUCCESS,
		EMPTY_DEVICE_LIST,
		FAILURE
	};

	static char* scanResultToString(ScanResult scanResult) {
		switch (scanResult) {
		case ScanResult::SUCCESS:
			return "SUCCESS";
		case ScanResult::TIMEOUT:
			return "TIMEOUT";
		case ScanResult::EMPTY_DEVICE_LIST:
			return "EMPTY_DEVICE_LIST";
		case ScanResult::DEVICE_IS_ALREADY_SCANNING:
			return "DEVICE_IS_ALREADY_SCANNING";
		default:
			return "FAILURE";
		}
	};
	static char* pointcloudCalculationResultToString(PointcloudCalculationResult pointcloudCalculationResult) {
		switch (pointcloudCalculationResult) {
		case PointcloudCalculationResult::SUCCESS:
			return "SUCCESS";
		case PointcloudCalculationResult::EMPTY_DEVICE_LIST:
			return "EMPTY_DEVICE_LIST";
		default:
			return "FAILURE";
		}
	};
private:
	/**
	 * @brief Contains all registered devices added via addDevice(...)
	*/
	std::vector<ScanDevice*>* m_devices;
	/**
	 * @brief Index of the currently selected device. Used by various functions if no device index is specified. -1 if no device is selected.
	*/
	int m_selectedDevice;
public:
	/**
	 * @brief Create a new ScanPipeline.
	*/
	ScanPipeline();
	/**
	 * @brief Closes and deletes all registered ScanDevices before finally deconstructing the ScanPipeline intself.
	*/
	~ScanPipeline();
	/**
	 * @brief Register a device.
	 * @param device will be added to the device list.
	*/
	void addDevice(ScanDevice* device);
	/**
	 * @brief Retrieve a registered device associated with the given index.
	 * @param deviceIndex Index of the desired device
	 * @return Device at the given index.
	*/
	ScanDevice* getDevice(const unsigned int deviceIndex);
	/**
	 * @brief Retrieve a registered device associated with the given serial.
	 * @param serial Serial of the desired device
	 * @return Device with the given serial.
	*/
	ScanDevice* getDevice(const std::string serial);
	/**
	 * @brief Retrieve the index associated with a registered device.
	 * @param device Pointer to the device.
	 * @return Index of the given device or -1, if device is not found.
	*/
	int getDeviceIndex(ScanDevice* device);
	/**
	 * @brief Check if a device was already registered as a ScanDevice.
	 * @param serial Serial of the desired device
	 * @return True if a ScanDevice with the given serial was found, false otherwise.
	*/
	bool hasDevice(const std::string serial);
	/**
	 * @brief Retrieve the currently selected device.
	 * @return Selected device or nullptr if no device is registered
	*/
	ScanDevice* getSelectedDevice();
	/**
	 * @return Index of the currently selected device
	*/
	int getSelectedDeviceIndex() const;
	/**
	 * @brief Clamps the given deviceIndex to the range of available devices by applying a modulo operation.
	 * @param deviceIndex The index of the desired device.
	 * @return Returns -1 if no device is currently connected. Otherwise returns an index between 0 and the device count.
	*/
	inline int clampDeviceIndex(const unsigned int deviceIndex) {
		return m_devices->empty() ? -1 : deviceIndex % m_devices->size();
	};
	/**
	 * @brief Get the number of registered devices.
	 * @return Number of registered devices
	*/
	unsigned int getDeviceCount();
	/**
	 * @brief Remove the device at the given index.
	 * @param deviceIndex index of the device to be removed
	*/
	void removeDevice(const unsigned int deviceIndex);
	/**
	 * @brief Removes all registered devices. All remaining scan data of the removed devices will be deleted in the process.
	*/
	void clearDevices();
	/**
	 * @brief Select a specific device. If no device is registered the selected device index will be set to -1.
	 * @param deviceIndex index of the desired device. If larger than the number of registered devices a modulo operation will be used to obtain a viable index.
	*/
	void selectDevice(const unsigned int deviceIndex);
	/**
	* @brief Select the previous device by decrementing the currently selected index. When the first device is selected, the next one will be the last device.
	*/
	void selectPreviousDevice();
	/**
	* @brief Select the previous device by decrementing the currently selected index by decrement. When the first device is selected, the next one will be the last device.
	*/
	void selectPreviousDevice(const unsigned int decrement);
	/**
	 * @brief Select the next device by incrementing the currently selected index. When the last device is selected, the next one will be the first device.
	*/
	void selectNextDevice();
	/**
	 * @brief Select the next device by incrementing the currently selected index by increment. When the last device is selected, the next one will be the first device.
	 * @param increment added to the currently selected device index
	*/
	void selectNextDevice(const unsigned int increment);
	/**
	 * @brief Starts all registered devices. After that each device is able to capture depth and color data.
	*/
	void startAllDevices();
	/**
	 * @brief Initiates a scan with the selected device. If successful the captured data will be accessible via the colorData and depthData arguments.
	 *		  Previously captured scan data will be released, if it still available.
	 * @param colorData will point to the color data captured by the device on success. The input value will then be overridden and thus can initially be nullptr.
	 * @param depthData will point to the depth data captured by the device on success. The input value will then be overridden and thus can initially be nullptr.
	 * @param ms_timeout time in microseconds to wait for the device to finish scanning. If exceeded the function will exit without obtaining the desired data.
	 * @return The return enum value will provide information on whether the function succeeded or encountered an error.
	*/
	ScanResult scan(ColorData** colorData, DepthData** depthData, const unsigned int ms_timeout = 1000u);
	/**
	 * @brief Initiates a scan with a specific device. If successful the captured data will be accessible via the colorData and depthData arguments.
	 *		  Previously captured scan data will be released, if it still available.
	 * @param deviceIndex index of the device to scan with.
	 * @param colorData will point to the color data captured by the device on success. The input value will then be overridden and thus can initially be nullptr.
	 * @param depthData will point to the depth data captured by the device on success. The input value will then be overridden and thus can initially be nullptr.
	 * @param ms_timeout time in microseconds to wait for the device to finish scanning. If exceeded the function will exit without obtaining the desired data.
	 * @return The return enum value will provide information on whether the function succeeded or encountered an error.
	*/
	ScanResult scan(const unsigned int deviceIndex, ColorData** colorData, DepthData** depthData, const unsigned int ms_timeout = 1000u);
	std::vector<ScanResult> scan(const unsigned int deviceRange, std::vector<ColorData*>* colorData, std::vector<DepthData*>* depthData, const unsigned int ms_timeout = 1000u);
	std::vector<ScanResult> scan(const unsigned int deviceIndex, const unsigned int deviceRange, std::vector<ColorData*>* colorData, std::vector<DepthData*>* depthData, const unsigned int ms_timeout = 1000u);

	std::future<ScanResult> scanAsync(ColorData** colorData, DepthData** depthData, const unsigned int ms_timeout = 1000u);
	std::future<ScanResult> scanAsync(const unsigned int deviceIndex, ColorData** colorData, DepthData** depthData, const unsigned int ms_timeout = 1000u);
	std::vector<std::future<ScanResult>> scanAsync(const unsigned int deviceRange, std::vector<ColorData*>* colorData, std::vector<DepthData*>* depthData, const unsigned int ms_timeout = 1000u);
	std::vector<std::future<ScanResult>> scanAsync(const unsigned int deviceIndex, const unsigned int deviceRange, std::vector<ColorData*>* colorData, std::vector<DepthData*>* depthData, const unsigned int ms_timeout = 1000u);
	/**
	 * @brief Calculates a pointcloud from the color and depth data of the selected device. Scan(...) has to have been successfully executed for this device in order to retrieve the correct data.
	 * @param pointcloudData will point to the pointcloud data calculated by the device on success. The input value will then be overridden and thus can initially be nullptr.
	 * @return The return enum value will provide information on whether the function succeeded or encountered an error.
	*/
	PointcloudCalculationResult calculatePointcloud(PointcloudData** pointcloudData, const bool downsampled = true);
	/**
	 * @brief 
	 * @param deviceIndex 
	 * @param pointcloudData 
	 * @return The return enum value will provide information on whether the function succeeded or encountered an error.
	*/
	PointcloudCalculationResult calculatePointcloud(const unsigned int deviceIndex, PointcloudData** pointcloudData, const bool downsampled = true);
	/**
	 * @brief Obtain the transform matrix provided by the associated poseDriver of the selected device. Can raise an exception, if no device is registered.
	 * @return Matrix to transform the pointcloud data from the device's local coordinate system to global coordinates.
	*/
	osg::Matrix getTransform();
	/**
	 * @brief Obtain the transform matrix provided by the associated poseDriver of the device at the given index. Can raise an exception, if deviceIndex is bigger than the number of registered devices.
	 * @param deviceIndex index of the desired device
	 * @return Matrix to transform the pointcloud data from the device's local coordinate system to global coordinates.
	*/
	osg::Matrix getTransform(const unsigned int deviceIndex);
	/**
	 * @brief Releases all currently stored scan data for every registered device.
	*/
	void releaseScans();
};