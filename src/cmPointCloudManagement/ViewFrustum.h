#pragma once

#include <osg/Vec3f>
#include <osg/MatrixTransform>

class ViewFrustum {
	class Plane {
		osg::Vec3f _n;
		float _d;

		Plane() = delete;
	public:
		Plane(const osg::Vec3f& p, const osg::Vec3f& n);
		Plane(const osg::Vec3f& p1, const osg::Vec3f& p2, const osg::Vec3f& p3);

		const osg::Vec3f& n() const { return _n; };
		float signedDistance(osg::Vec3f x) const;
	};

	Plane _boundaries[6];
	osg::Vec3f _corners[8];
	osg::Vec3f _normals[6];

	/*Plane& near() { return boundaries[0]; };
	Plane& far() { return boundaries[1]; };
	Plane& top() { return boundaries[2]; };
	Plane& bottom() { return boundaries[3]; };
	Plane& left() { return boundaries[4]; };
	Plane& right() { return boundaries[5]; };*/

	ViewFrustum() = delete;
public:
	ViewFrustum(osg::Vec3f& near_NE, osg::Vec3f& near_SE, osg::Vec3f& near_SW, osg::Vec3f& near_NW, osg::Vec3f& far_NE, osg::Vec3f& far_SE, osg::Vec3f& far_SW, osg::Vec3f& far_NW);

	bool contains(osg::Vec3f& p);
	bool intersectCubicAABB(osg::Vec3f& p, float l);
	const osg::Vec3f* corners();
	const osg::Vec3f* normals();

};