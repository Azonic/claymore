// ClayMore - Immersive Mesh Modelling --- Copyright (c) 2014-2017 Philipp Ladwig, Jannik Fiedler, Jan Beutgen
//Header File of this class
#include "PointCloudManager.h"
//Header File of PointCloudVisualizer
#include "PointCloudVisualizer.h"
#include <osg/MatrixTransform>

//Includes of Intel Real Sense
#include <rs.hpp> // Include RealSense Cross Platform API
#include <hpp\rs_frame.hpp>

//#include "RealSenseDeviceContainer.h"
//#include "RealSenseUtils.h"
#include "LiveMesh.h"


//#include <pcl/registration/icp.h>
//#include <pcl/filters/statistical_outlier_removal.h>
//#include <pcl/filters/radius_outlier_removal.h>

//Input-Output Stream -> for printing stuff on the console with for example:
//std::cout << "Bla Bla" << std::endl;
#include <iostream>

#include "cmUtil/TimedOpenVRMatrix.h"
#include <cmUtil/TimeWriter.h>

//#include <osg/io_utils>
#include <osgDB/WriteFile>
//#include <osgDB/ReadFile>
#include <opencv2/highgui/highgui_c.h>
#include <opencv2/opencv.hpp>   // Include OpenCV API

// BETTER CAMERA CALIB
#include <opencv2/calib3d/calib3d.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>


#include <string>
#include <ctime>

// LiveMesh threadManagement
#include <condition_variable>

#include "ScanPipeline.h"
#include "RealSenseScanDevice.h"
#include "AzureKinectScanDevice.h"
#include "StaticPoseDriver.h"
#include "ViveTrackerPoseDriver.h"

// ICP
#include <pcl/io/pcd_io.h>
#include <pcl/point_types.h>
#include <pcl/registration/icp.h>
#include <pcl/filters/random_sample.h>
//#include <pcl/impl/point_types.hpp>

#define LOG_PROCESSING_THREAD_MESSAGES 0

#define TIMEOUT_MS 15000u

//TODO 2: Implement a proper structure to quickly draw Gizmos in ClayMore with support for multithreading
std::atomic_bool g_cameraPosesLock(false);
std::vector<std::pair<osg::Matrix, ViewFrustum>> g_cameraPoses;

//Constructor
PointCloudManager::PointCloudManager(ConfigReader* configReaderIn, osg::ref_ptr<osg::Group> pointCloudGroup) :
	m_configReader(configReaderIn), m_pointCloudGroup(pointCloudGroup), m_processingThread(nullptr), m_isProcessing(false), m_closeProcessingThread(false)
{
	//BoolMarchingCubesProcessor* test = new BoolMarchingCubesProcessor(nullptr); // Just a Test
	m_useRealSenseDevice = configReaderIn->getBoolFromStartupConfig("use_real_sense_device");
	m_useAzureKinectDevice = configReaderIn->getBoolFromStartupConfig("use_azure_kinect_device");
	m_useMultipleRealSenseDevices = configReaderIn->getBoolFromStartupConfig("use_multiple_real_sense_device");
	m_useAzureTelepresence = configReaderIn->getBoolFromStartupConfig("use_azure_telepresence");
	m_useColorAndDepthShooter = configReaderIn->getBoolFromStartupConfig("use_color_and_depth_shooter");
	m_realSenseDepthWidth = configReaderIn->getIntFromStartupConfig("real_sense_depth_width");
	m_realSenseDepthHeight = configReaderIn->getIntFromStartupConfig("real_sense_depth_height");
	m_realSenseFPS = configReaderIn->getIntFromStartupConfig("real_sense_fps");

	{ //Initialize Kinect depth mode and fps
		std::string depthRes = configReaderIn->getStringFromStartupConfig("azure_kinect_depth_mode");
		if (depthRes == "NFOV_2X2BINNED") m_azureKinectDepthMode = k4a_depth_mode_t::K4A_DEPTH_MODE_NFOV_2X2BINNED;
		else if (depthRes == "WFOV_2X2BINNED") m_azureKinectDepthMode = k4a_depth_mode_t::K4A_DEPTH_MODE_WFOV_2X2BINNED;
		else if (depthRes == "WFOV_UNBINNED") m_azureKinectDepthMode = k4a_depth_mode_t::K4A_DEPTH_MODE_WFOV_UNBINNED;
		else if (depthRes == "PASSIVE_IR") m_azureKinectDepthMode = k4a_depth_mode_t::K4A_DEPTH_MODE_PASSIVE_IR;
		else m_azureKinectDepthMode = k4a_depth_mode_t::K4A_DEPTH_MODE_NFOV_UNBINNED;

		int fps = configReaderIn->getIntFromStartupConfig("azure_kinect_fps");
		if (fps == 5) m_azureKinectFPS = k4a_fps_t::K4A_FRAMES_PER_SECOND_5;
		else if (fps == 15) m_azureKinectFPS = k4a_fps_t::K4A_FRAMES_PER_SECOND_15;
		else m_azureKinectFPS = k4a_fps_t::K4A_FRAMES_PER_SECOND_30;
	}

	m_pointCloudWidth = m_configReader->getIntFromStartupConfig("point_cloud_width");
	m_pointCloudHeight = m_configReader->getIntFromStartupConfig("point_cloud_height");


	if (!configReaderIn->getBoolFromStartupConfig("use_azure_kinect_device"))
	{
		m_useColorAndDepthShooter = false;
	}
	m_processingThreadMaxDowntime = m_configReader->getIntFromStartupConfig("processing_thread_max_downtime");
	m_writeTSDF = m_configReader->getBoolFromStartupConfig("write_tsdf");
	m_scanInParallel = m_configReader->getBoolFromStartupConfig("scan_in_parallel");
	m_enableRealtimePointCloud = m_configReader->getBoolFromStartupConfig("enable_realtime_pointcloud");
	m_realtimePointCloudFPS = m_configReader->getIntFromStartupConfig("realtime_pointcloud_fps");

	m_useSerialToFindTrackedScanDevice = configReaderIn->getBoolFromStartupConfig("use_serial_to_find_tracked_scan_device");
	m_serialOfTrackedScanDevice = configReaderIn->getStringFromStartupConfig("serial_of_tracked_scan_device");

	//MultiCamCalibration
	m_useMultiCamCalibration = m_configReader->getBoolFromStartupConfig("use_multi_cam_calibration");

	//OpenCV
	std::string markerType = m_configReader->getStringFromStartupConfig("opencv_marker_type");
	if (markerType == "chessboard") m_markerType = MarkerType::CHESSBOARD;
	else if (markerType == "charuco") m_markerType = MarkerType::CHARUCO;

	//OpenCV Checkerboard
	m_checkerboardDimX = m_configReader->getIntFromStartupConfig("opencv_checkerboard_dimension_x");
	m_checkerboardDimY = m_configReader->getIntFromStartupConfig("opencv_checkerboard_dimension_y");
	m_checkerboardSquareSizeInMM = m_configReader->getFloatFromStartupConfig("opencv_checkerboard_square_size_in_mm");
	m_patternToTrackerOffset = m_configReader->getVec3fFromStartupConfig("opencv_checkerboard_to_tracker_offset");
	m_patternToTrackerTrans = osg::Matrix::translate(m_patternToTrackerOffset);
	m_patternToTrackerRotOffset = m_configReader->getVec3fFromStartupConfig("opencv_checkerboard_to_tracker_rotation_offset");
	m_patternToTrackerRot = osg::Matrix::rotate(osg::Quat(osg::DegreesToRadians(m_patternToTrackerRotOffset.x()), osg::Vec3(1, 0, 0),
		osg::DegreesToRadians(m_patternToTrackerRotOffset.y()), osg::Vec3(0, 1, 0),
		osg::DegreesToRadians(m_patternToTrackerRotOffset.z()), osg::Vec3(0, 0, 1)));
	m_patternToTrackerTransform = m_patternToTrackerRot * m_patternToTrackerTrans;
	m_calibrateCameraPosesState.store(0);

	m_checkerboardPattern = generateCheckerboardPattern();

	//OpenCV ChArUco
	float m_charucoMarkerSizeInMM = m_configReader->getFloatFromStartupConfig("opencv_charuco_marker_size_in_mm"); // 27.75f
	unsigned int m_charucoMinDetectedPoints = m_configReader->getIntFromStartupConfig("opencv_charuco_min_detected_points"); // 6

	std::string charucoDict = m_configReader->getStringFromStartupConfig("opencv_charuco_dictionary");
	if (charucoDict == "dict_aruco_original") m_charucoDictionary = cv::aruco::getPredefinedDictionary(cv::aruco::DICT_ARUCO_ORIGINAL);
	else if (charucoDict == "dict_4x4_50") m_charucoDictionary = cv::aruco::getPredefinedDictionary(cv::aruco::DICT_4X4_50);
	else if (charucoDict == "dict_4x4_100") m_charucoDictionary = cv::aruco::getPredefinedDictionary(cv::aruco::DICT_4X4_100);
	else if (charucoDict == "dict_4x4_250") m_charucoDictionary = cv::aruco::getPredefinedDictionary(cv::aruco::DICT_4X4_250);
	else if (charucoDict == "dict_4x4_1000") m_charucoDictionary = cv::aruco::getPredefinedDictionary(cv::aruco::DICT_4X4_1000);
	else if (charucoDict == "dict_5x5_50") m_charucoDictionary = cv::aruco::getPredefinedDictionary(cv::aruco::DICT_5X5_50);
	else if (charucoDict == "dict_5x5_100") m_charucoDictionary = cv::aruco::getPredefinedDictionary(cv::aruco::DICT_5X5_100);
	else if (charucoDict == "dict_5x5_250") m_charucoDictionary = cv::aruco::getPredefinedDictionary(cv::aruco::DICT_5X5_250);
	else if (charucoDict == "dict_5x5_1000") m_charucoDictionary = cv::aruco::getPredefinedDictionary(cv::aruco::DICT_5X5_1000);
	else if (charucoDict == "dict_6x6_50") m_charucoDictionary = cv::aruco::getPredefinedDictionary(cv::aruco::DICT_6X6_50);
	else if (charucoDict == "dict_6x6_100") m_charucoDictionary = cv::aruco::getPredefinedDictionary(cv::aruco::DICT_6X6_100);
	else if (charucoDict == "dict_6x6_250") m_charucoDictionary = cv::aruco::getPredefinedDictionary(cv::aruco::DICT_6X6_250);
	else if (charucoDict == "dict_6x6_1000") m_charucoDictionary = cv::aruco::getPredefinedDictionary(cv::aruco::DICT_6X6_1000);
	else if (charucoDict == "dict_7x7_50") m_charucoDictionary = cv::aruco::getPredefinedDictionary(cv::aruco::DICT_7X7_50);
	else if (charucoDict == "dict_7x7_100") m_charucoDictionary = cv::aruco::getPredefinedDictionary(cv::aruco::DICT_7X7_100);
	else if (charucoDict == "dict_7x7_250") m_charucoDictionary = cv::aruco::getPredefinedDictionary(cv::aruco::DICT_7X7_250);
	else if (charucoDict == "dict_7x7_1000") m_charucoDictionary = cv::aruco::getPredefinedDictionary(cv::aruco::DICT_7X7_1000);
	else if (charucoDict == "dict_apriltag_16h5") m_charucoDictionary = cv::aruco::getPredefinedDictionary(cv::aruco::DICT_APRILTAG_16h5);
	else if (charucoDict == "dict_apriltag_25h9") m_charucoDictionary = cv::aruco::getPredefinedDictionary(cv::aruco::DICT_APRILTAG_25h9);
	else if (charucoDict == "dict_apriltag_36h10") m_charucoDictionary = cv::aruco::getPredefinedDictionary(cv::aruco::DICT_APRILTAG_36h10);
	else if (charucoDict == "dict_apriltag_36h11") m_charucoDictionary = cv::aruco::getPredefinedDictionary(cv::aruco::DICT_APRILTAG_36h11);
	//m_charucoDictionary = cv::aruco::getPredefinedDictionary(cv::aruco::DICT_4X4_250);

	m_charucoBoard = cv::aruco::CharucoBoard::create(m_checkerboardDimX, m_checkerboardDimY, m_checkerboardSquareSizeInMM, m_charucoMarkerSizeInMM, m_charucoDictionary);
	m_charucoParams = cv::aruco::DetectorParameters::create();

	std::string cornerRefinement = m_configReader->getStringFromStartupConfig("opencv_charuco_corner_refinement");
	if (cornerRefinement == "corner_refine_none") m_charucoParams->cornerRefinementMethod = cv::aruco::CornerRefineMethod::CORNER_REFINE_NONE;
	else if (cornerRefinement == "corner_refine_subpix") m_charucoParams->cornerRefinementMethod = cv::aruco::CornerRefineMethod::CORNER_REFINE_SUBPIX;
	else if (cornerRefinement == "corner_refine_contour") m_charucoParams->cornerRefinementMethod = cv::aruco::CornerRefineMethod::CORNER_REFINE_CONTOUR;
	else if (cornerRefinement == "corner_refine_apriltag") m_charucoParams->cornerRefinementMethod = cv::aruco::CornerRefineMethod::CORNER_REFINE_APRILTAG;


	/*
	regards OpenCV:
	Criteria for termination of the iterative process of corner refinement in feature detection.
	That is, the process of corner position refinement stops either after g_maxIterationForSubPixel
	iterations or when the corner position moves by less than g_epsilonForSubPixel on some iteration.
	Have a look at https://docs.opencv.org/2.4/modules/imgproc/doc/feature_detection.html#cornersubpix
	Default is 30 and 0.01
*/
	m_maxIterationForSubPixel = 10; //default 30 (High Quality, good pattern with big squares, good camera, takes much time for calculation)
	m_epsilonForSubPixel = 0.050; // default 0.01 (High Quality, good pattern with big squares, good camera, takes much time for calculation 
									 // faster calculation 0.03 )

	// Texture Processing
	m_enableTextureProcessing = m_configReader->getBoolFromStartupConfig("enable_texture_processing");
	if (m_enableTextureProcessing) m_imageData = new std::vector<ImageData*>();
	else m_imageData = nullptr;
	m_textureProcessingJpegQuality = m_configReader->getIntFromStartupConfig("texture_processing_jpeg_quality");

	// Unified Scan Pipeline
	m_scanPipeline = new ScanPipeline();
	if (m_useRealSenseDevice) addRealSenseViveTrackerScanDevice();
	if (m_useAzureKinectDevice) addAzureKinectViveTrackerScanDevice();

	/*if (m_useRealSenseDevice) addRealSenseScanDevices();
	if (m_useAzureKinectDevice) addAzureKinectScanDevices();*/

	/* ### START - Old Scan Pipeline ###
	m_rsPc = new rs2::pointcloud();

	//m_pclPc = pcl::PointCloud<pcl::PointXYZRGB>::Ptr(new pcl::PointCloud<pcl::PointXYZRGB>());

	if (m_useMultipleRealSenseDevices)
	{
		m_realSenseDeviceContainer = new RealSenseDeviceContainer();
		m_context = new rs2::context();

		// Initial population of the device list
		for (auto&& dev : m_context->query_devices()) // Query the list of connected RealSense devices
		{
			m_realSenseDeviceContainer->enableDevice(dev, m_realSenseResolutionWidth, m_realSenseResolutionHeight, m_realSenseFPS);
		}

		m_context->set_devices_changed_callback([&](rs2::event_information& info)
			{
				m_realSenseDeviceContainer->removeDevices(info);
				for (auto&& dev : info.get_new_devices())
				{
					m_realSenseDeviceContainer->enableDevice(dev, m_realSenseResolutionWidth, m_realSenseResolutionHeight, m_realSenseFPS);
				}
			});
		//warmUpRealSense(5);

		//int count = 0;
		//// Initial population of the device list
		//for (auto&& dev : m_context->query_devices()) // Query the list of connected RealSense devices
		//{
		//	m_realSenseDeviceContainer->enableDevice(dev);
		//	std::cout << "count: " << count << std::endl;
		//	count++;
		//}
	}
	else if (m_useRealSenseDevice) {
		// Start streaming with custom configuration
		rs2::config config;
		RealSenseUtils::setUpConfig(&config, m_configReader->getIntFromStartupConfig("real_sense_resolution_width"), m_configReader->getIntFromStartupConfig("real_sense_resolution_height"), m_configReader->getIntFromStartupConfig("real_sense_fps"));
		m_rsPipe = new rs2::pipeline();


		rs2::pipeline_profile selection = m_rsPipe->start(config);

		rs2::device selected_device = selection.get_device();

		{
			// Define the origin of both clocks (System and Device) by requesting a timestamp of each system at nearly the same time
			uint8_t data[]{ 0x14, 00, 0xab, 0xcd, 01, 00, 00, 00, 0x3c, 0x61, 01, 00, 0x40, 0x61, 01, 00, 00, 00, 00, 00, 00, 00, 00, 00 };
			std::vector<uint8_t> command(data, data + sizeof(data));

			rs2::debug_protocol device = selected_device.as<rs2::debug_protocol>();

			m_system_clock_origin = std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::system_clock::now().time_since_epoch()).count();
			auto res = device.send_and_receive_raw_data(command); // USB latency should occurr twice here: once when sending the request command to the RS and once when the data will be send back from the RS to the host. Because of that the assumption is, that the timestamp will be read in the middle of the time needed for the whole operation.
			unsigned int* ptr = reinterpret_cast<unsigned int*>(res.data());
			m_device_clock_origin = ptr[1]; // usec

			m_system_clock_origin += (std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::system_clock::now().time_since_epoch()).count() - m_system_clock_origin) / 2ULL; // Interpolate the system timestamp before requesting the device timestamp and the one after the data is received.
		}

		std::cout << "### System clock origin: " << m_system_clock_origin << std::endl;
		std::cout << "### Device clock origin: " << m_device_clock_origin << std::endl;


		std::vector<rs2::sensor> sensors = selected_device.query_sensors();

		std::cout << "Device consists of " << sensors.size() << " sensors:\n" << std::endl;
		int index = 0;
		// We can now iterate the sensors and print their names
		for (rs2::sensor sensor : sensors)
		{
			std::cout << "  " << index++ << " : " << sensor.get_info(RS2_CAMERA_INFO_NAME) << std::endl;
		}


		m_isTelepresenceLoopActive.store(true);

		//auto depth_sensor = selected_device.first<rs2::depth_sensor>();

		//Find the rgb_sensor by querying all sensors
		rs2::sensor rgb_sensor;
		rs2::sensor depth_sensor;
		for (rs2::sensor sensor : selected_device.query_sensors()) {
			if (sensor.get_stream_profiles()[0].stream_type() == RS2_STREAM_COLOR) {
				//if (sensor.supports(RS2_CAMERA_INFO_NAME) && sensor.get_info(RS2_CAMERA_INFO_NAME) == "RGB Camera") { //Does not work this way... No idea why, though.
				rgb_sensor = sensor;
			}
			else if (sensor.get_stream_profiles()[0].stream_type() == RS2_STREAM_INFRARED) {
				depth_sensor = sensor;
			}
		}



		auto depth_sensor2 = selected_device.first<rs2::depth_sensor>();
		std::cout << depth_sensor2.get_info(RS2_CAMERA_INFO_NAME) << std::endl;

		std::cout << rgb_sensor.get_info(RS2_CAMERA_INFO_NAME) << std::endl;
		std::cout << depth_sensor.get_info(RS2_CAMERA_INFO_NAME) << std::endl;

		std::cout << "Scale: " << depth_sensor2.get_depth_scale() << std::endl;


		//Configure the sensor so that it does not alter colors and brightness (too much)
		rgb_sensor.set_option(RS2_OPTION_GLOBAL_TIME_ENABLED, 1);
		rgb_sensor.set_option(RS2_OPTION_ENABLE_AUTO_WHITE_BALANCE, 0);
		rgb_sensor.set_option(RS2_OPTION_ENABLE_AUTO_EXPOSURE, 0);
		rgb_sensor.set_option(RS2_OPTION_EXPOSURE, 500);
		rgb_sensor.set_option(RS2_OPTION_BACKLIGHT_COMPENSATION, 0);

		//rgb_sensor.set_option(RS2_OPTION_GLOBAL_TIME_ENABLED, 1); //???
		//depth_sensor.set_option(RS2_OPTION_BRIGHTNESS,1.f);

		depth_sensor.set_option(RS2_OPTION_ENABLE_AUTO_EXPOSURE, 1);
		//depth_sensor.set_option(RS2_OPTION_EXPOSURE, 5000);
		depth_sensor.set_option(RS2_OPTION_GLOBAL_TIME_ENABLED, 1);

		//Call the warm up after the RealSense-Device is setup. Grab and dump the first few frames to prevent errors with missing colors from occurring.
		//Although one cycle seems to be enough to fix the error, we repeat it a few times to be extra sure. Since we are only affecting the startup-time very little, it does not matter anyway.
		//warmUpRealSense(5);
	}

	//Decimation Filter
	m_rs_dec_filter = new rs2::decimation_filter();
	m_rs_dec_filter->set_option(RS2_OPTION_FILTER_MAGNITUDE, 3);		// 1 - 5

	//Spatial Filter
	m_rs_spat_filter = new rs2::spatial_filter();
	m_rs_spat_filter->set_option(RS2_OPTION_FILTER_MAGNITUDE, 4); 		// 1	- 5
	m_rs_spat_filter->set_option(RS2_OPTION_FILTER_SMOOTH_ALPHA, 0.25f); // 0.25	- 1
	m_rs_spat_filter->set_option(RS2_OPTION_FILTER_SMOOTH_DELTA, 50);	// 1	- 50
	m_rs_spat_filter->set_option(RS2_OPTION_HOLES_FILL, 0);				// [0]: Disabled, [1]: 2px, [2]: 4px, [3]: 8px [4]: 16px [5]: Unlimited

	//Temporal Filter
	m_rs_temp_filter = new rs2::temporal_filter();
	m_rs_temp_filter->set_option(RS2_OPTION_FILTER_SMOOTH_ALPHA, 0.4f);	// 0 - 1
	m_rs_temp_filter->set_option(RS2_OPTION_FILTER_SMOOTH_DELTA, 18);	// 1 - 100
	m_rs_spat_filter->set_option(RS2_OPTION_HOLES_FILL, 0);				// [0]: Disabled, [1]: Valid in 8/8, [2]: Valid in 2/last 3, [3]: Valid in 2/last 4, [4]: Valid in 2/8, [5]: Valid in 1/last 2, [6]: Valid in 1/last 5, [7]: Valid in 1/8, [8]: Always On

	//Disparity <> Depth Transforms
	m_rs_depth_to_disparity = new rs2::disparity_transform(true);
	m_rs_disparity_to_depth = new rs2::disparity_transform(false);

	m_rsPc = new rs2::pointcloud();
	// ### END - Old Scan Pipeline ### */

	m_pointCloudVisualizer = new PointCloudVisualizer(m_configReader, m_pointCloudGroup);

	m_scanDeviceInitLock.store(false);
	m_timedCircularTrackerMatrixBuffer = nullptr;

	//PointCloudDisplay
	m_displayTransform = new osg::MatrixTransform(); //Die Matrix, woran diese ganze Klasse hängt.
	m_displayTransform->setName("MatTransPointCloudDisplay");
	m_pointCloudDisplay = new PointCloudDisplay(m_displayTransform);
	pointCloudGroup->addChild(m_displayTransform);

	if (m_useColorAndDepthShooter)
	{
		//### START - Old Scan Pipeline ###
		//Azure Kinect
		m_azureKinectSensor = NULL;
		m_config = K4A_DEVICE_CONFIG_INIT_DISABLE_ALL;

		//float aspectRatio = 1280.0f / 720.0f;
		float aspectRatio = 1.333f;
		//Azure Kinect Live View
		m_liveViewQuatVertices = new osg::Vec3Array;

		m_liveViewQuatVertices->push_back(osg::Vec3(0.0f, 0.01f, 1.0f));
		m_liveViewQuatVertices->push_back(osg::Vec3(0.0f, 0.01f, 0.0f));
		m_liveViewQuatVertices->push_back(osg::Vec3(aspectRatio, 0.01f, 0.0f));
		m_liveViewQuatVertices->push_back(osg::Vec3(aspectRatio, 0.01f, 1.0f));

		m_liveViewQuatVertColors = new osg::Vec4Array;
		m_liveViewQuatVertColors->push_back(osg::Vec4(1.0f, 1.0f, 1.0f, 1.0f));

		m_liveViewQuatNormals = new osg::Vec3Array;
		m_liveViewQuatNormals->push_back(osg::Vec3(1.0f, 1.0f, 1.0f));

		m_liveViewTexcoords = new osg::Vec2Array;
		m_liveViewTexcoords->push_back(osg::Vec2(0.0f, 0.0f));
		m_liveViewTexcoords->push_back(osg::Vec2(0.0f, 1.0f));
		m_liveViewTexcoords->push_back(osg::Vec2(1.0f, 1.0f));
		m_liveViewTexcoords->push_back(osg::Vec2(1.0f, 0.0f));

		m_liveViewQuad = new osg::Geometry;
		m_liveViewQuad->setVertexArray(m_liveViewQuatVertices.get());
		m_liveViewQuad->setColorArray(m_liveViewQuatVertColors);
		m_liveViewQuad->setColorBinding(osg::Geometry::BIND_OVERALL);
		m_liveViewQuad->setNormalArray(m_liveViewQuatNormals.get());
		m_liveViewQuad->setNormalBinding(osg::Geometry::BIND_OVERALL);
		m_liveViewQuad->setTexCoordArray(0, m_liveViewTexcoords.get());
		m_liveViewQuad->addPrimitiveSet(new osg::DrawArrays(GL_QUADS, 0, 4));

		m_liveViewImage = new osg::Image();
		m_liveViewTexture = new osg::Texture2D();
		m_liveViewTexture->setDataVariance(osg::Object::DYNAMIC);
		m_liveViewTexture->setResizeNonPowerOfTwoHint(false);
		m_liveViewTexture->setImage(m_liveViewImage.get());
		m_liveViewTexture->setInternalFormat(GL_BGRA);
		m_liveViewTexture->setInternalFormatMode(osg::Texture::InternalFormatMode::USE_IMAGE_DATA_FORMAT);
		m_liveViewTexture->setFilter(osg::Texture::MIN_FILTER, osg::Texture::LINEAR);
		m_liveViewTexture->setFilter(osg::Texture2D::MAG_FILTER, osg::Texture2D::LINEAR);
		m_liveViewTexture->setWrap(osg::Texture::WRAP_S, osg::Texture::CLAMP_TO_BORDER);//CLAMP_TO_EDGE);
		m_liveViewTexture->setWrap(osg::Texture::WRAP_T, osg::Texture::CLAMP_TO_BORDER);

		m_liveViewQuadGeode = new osg::Geode;
		m_liveViewQuadGeode->addDrawable(m_liveViewQuad.get());
		m_liveViewQuadGeode->getOrCreateStateSet()->setTextureAttributeAndModes(0, m_liveViewTexture.get());
		m_liveViewQuadGeode->getOrCreateStateSet()->setMode(GL_LIGHTING, osg::StateAttribute::OVERRIDE | osg::StateAttribute::ON);

		m_liveViewQuadGeode->computeBound();
		m_liveViewQuadGeode->dirtyBound();
		// ### END - Old Scan Pipeline ###
	}

	//Interesting for face scanning
	m_framesToCapture = m_configReader->getIntFromStartupConfig("frames_to_capture");
	m_clippingDistance = m_configReader->getIntFromStartupConfig("clipping_distance"); // in millimeter

	//m_liveMesh = LiveMesh();
	//m_liveMesh.useProjectionMapping(false);
	//m_liveMesh.initialize(&m_azureKinectSensor, m_pointCloudGroup, &m_config, m_configReader);

	m_maxNumThreads = 25;
	m_isSceneGraphLockedForRendering = new std::atomic<bool>();
	m_isSceneGraphLockedForRendering->store(false);


}

PointCloudManager::~PointCloudManager()
{
	/*### START - Old Scan Pipeline ###
	if (m_useRealSenseDevice)
	{
		stopPointCloudPipe();
	}
	//### END - Old Scan Pipeline ###  */
	if (m_scanDeviceInitAsync.valid()) m_scanDeviceInitAsync.get();

	if (m_calibrateCameraPosesAsync.valid()) {
		m_calibrateCameraPosesState.store(3);
		m_calibrateCameraPosesAsync.get();
	}

	if (m_processingThread) {
		m_isProcessing.store(true);
		m_closeProcessingThread.store(true);
		if (m_processingThread->joinable()) m_processingThread->join();
		delete m_processingThread;
	}

	// Cleanup for texture processing
	if (m_imageData) {
		for (ImageData* img : *m_imageData) {
			delete img->_colorBuffer;
			delete img->_depthBuffer;
			delete img;
		}
		delete m_imageData;
	}

	delete m_scanPipeline;
	delete m_pointCloudDisplay;
}

void PointCloudManager::getMaxScanDeviceResolution(unsigned int& width_out, unsigned int& height_out) {
	width_out = 0;
	height_out = 0;
	for (unsigned int i = 0; i < m_scanPipeline->getDeviceCount(); i++) {
		ScanDevice* scanDevice = m_scanPipeline->getDevice(i);
		unsigned int width = scanDevice->getColorWidth();
		unsigned int height = scanDevice->getColorHeight();

		if (width_out < width) width_out = width;
		if (height_out < height) height_out = height;
	}
}

void PointCloudManager::findAndInitializeScanDevices() {
	if (m_scanDeviceInitLock.load()) return;
	if (m_scanDeviceInitAsync.valid()) m_scanDeviceInitAsync.get();
	m_scanDeviceInitLock.store(true);
	m_scanDeviceInitAsync = std::async(std::launch::async, &PointCloudManager::findAndInitializeScanDevicesFunction, this);
}

void PointCloudManager::findAndInitializeScanDevicesFunction() {
	if (m_useRealSenseDevice) addRealSenseScanDevices();
	if (m_useAzureKinectDevice) addAzureKinectScanDevices();

	m_scanPipeline->startAllDevices();
	if (m_scanPipeline->getSelectedDeviceIndex() == -1) m_scanPipeline->selectDevice(0);

	m_devicesPerScan = m_configReader->getIntFromStartupConfig("devices_per_scan");
	{
		size_t deviceCount = m_scanPipeline->getDeviceCount();
		if (m_devicesPerScan == 0 || m_devicesPerScan > deviceCount) m_devicesPerScan = deviceCount;
	}

	unsigned int width = 0u;
	unsigned int height = 0u;
	getMaxScanDeviceResolution(width, height);

	m_pointCloudVisualizer->updatePointCloudBuffers(m_devicesPerScan, width, height);
	std::cout << "devicesPerScan: " << m_devicesPerScan << std::endl;
	m_scanDeviceInitLock.store(false);
}

void PointCloudManager::toggleCameraPoseCalibration() {
	if (m_isRecording) return;
	if (!m_calibrateCameraPosesAsync.valid()) { // Start calibration loop
		m_calibrateCameraPosesState.store(1u);
		std::cout << "### Start camera pose calibration" << std::endl;
		m_calibrateCameraPosesAsync = m_useMultiCamCalibration ?
			std::async(std::launch::async, &PointCloudManager::multiCamPoseCalibrationLoop, this) :
			std::async(std::launch::async, &PointCloudManager::cameraPoseCalibrationLoop, this);
	}
	else if (m_calibrateCameraPosesState.load() == 1u) { // End current calibration loop
		std::cout << "### End camera pose calibration" << std::endl;
		m_calibrateCameraPosesState.store(2u);
	}
}

std::vector<ScanDevice*> PointCloudManager::cameraPoseCalibrationLoop() {
	std::cout << "CameraPoseCalibrationLoop started" << std::endl;
	// ### Draw camera positions ###
	size_t cameraPosesCount = g_cameraPoses.size();
	// ######

	struct rs2DeviceData {
		std::string _serial; rs2::pipeline _pipeline; rs2::device _device; unsigned int _checkerboardCount; osg::Matrix _pose; cv::Mat _cameraMatrix; cv::Mat _distCoeffs; unsigned int _width; unsigned int _height;
		rs2DeviceData(std::string&& serial, rs2::pipeline& pipeline, rs2::device& device, unsigned int checkerboardCount, osg::Matrix& pose, cv::Mat& cameraMatrix, cv::Mat& distCoeffs, unsigned int width, unsigned int height) :
			_serial(serial), _pipeline(pipeline), _device(device), _checkerboardCount(checkerboardCount), _pose(pose), _cameraMatrix(cameraMatrix), _distCoeffs(distCoeffs), _width(width), _height(height) {}
	};
	struct k4aDeviceData {
		std::string _serial; k4a_device_t _device; unsigned int _checkerboardCount; osg::Matrix _pose; cv::Mat _cameraMatrix; cv::Mat _distCoeffs; unsigned int _width; unsigned int _height; int _k4aColorResolution;
		k4aDeviceData(std::string&& serial, k4a_device_t& device, unsigned int checkerboardCount, osg::Matrix& pose, cv::Mat& cameraMatrix, cv::Mat& distCoeffs, unsigned int width, unsigned int height, int k4aColorResolution) :
			_serial(serial), _device(device), _checkerboardCount(checkerboardCount), _pose(pose), _cameraMatrix(cameraMatrix), _distCoeffs(distCoeffs), _width(width), _height(height), _k4aColorResolution(k4aColorResolution) {}
	};
	//Store the device and the number of successfully detected checkerboard patterns (used as weight) as well as the averaged pose
	std::vector<rs2DeviceData> rs2_devices;
	std::vector<k4aDeviceData> k4a_devices;

	// Query all available devices with valid CameraCalibration.xml for RealSense and Azure Kinect
	// RealSense:
	rs2::context rs2ctx;
	unsigned int realSenseCount = 0u;
	for (rs2::device&& device : rs2ctx.query_devices()) {
		const char* serial = device.get_info(RS2_CAMERA_INFO_SERIAL_NUMBER);
		++realSenseCount;
		if (m_scanPipeline->hasDevice(serial)) continue;
		cv::Mat cameraMatrix;
		cv::Mat distCoeffs;
		unsigned int width = 0u;
		unsigned int height = 0u;
		if (loadCameraCalibration(serial, &cameraMatrix, &distCoeffs, width, height)) {
			rs2::config solvePNPcfg;
			solvePNPcfg.enable_stream(RS2_STREAM_COLOR, -1, width, height, rs2_format::RS2_FORMAT_BGR8, m_realSenseFPS);
			solvePNPcfg.enable_device(serial);
			rs2_devices.emplace_back(serial, rs2::pipeline(rs2ctx), device, 0u, osg::Matrix(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0), cameraMatrix, distCoeffs, width, height); //Matrix needs to be all zeros at the start

			rs2::pipeline& rsPipe = rs2_devices.at(rs2_devices.size() - 1)._pipeline;
			rsPipe.start(solvePNPcfg);
			// Camera warmup - dropping several first frames to let auto-exposure stabilize
			for (int i = 0; i < 15; i++) rsPipe.wait_for_frames();
		}
	}
	std::cout << realSenseCount << " connected Real Sense devices" << std::endl;
	// Azure Kinect:
	unsigned int azureKinectCount = k4a_device_get_installed_count();
	for (unsigned int deviceIndex = 0; deviceIndex < azureKinectCount; deviceIndex++) {
		k4a_device_t device = nullptr;
		size_t serial_num_size = 0;
		char* serial = nullptr;
		if (k4a_result_t::K4A_RESULT_SUCCEEDED == k4a_device_open(deviceIndex, &device) &&
			k4a_buffer_result_t::K4A_BUFFER_RESULT_TOO_SMALL == k4a_device_get_serialnum(device, NULL, &serial_num_size) &&
			k4a_buffer_result_t::K4A_BUFFER_RESULT_SUCCEEDED == k4a_device_get_serialnum(device, serial = new char[serial_num_size], &serial_num_size) &&
			!m_scanPipeline->hasDevice(serial))
		{
			cv::Mat cameraMatrix;
			cv::Mat distCoeffs;
			unsigned int width = 0u;
			unsigned int height = 0u;
			int k4aColorResolution = -1u;
			if (loadCameraCalibration(serial, &cameraMatrix, &distCoeffs, width, height) && (k4aColorResolution = k4aGetColorResolution(width, height)) > 0) {
				k4a_device_configuration_t config = K4A_DEVICE_CONFIG_INIT_DISABLE_ALL;
				config.camera_fps = m_azureKinectFPS;
				config.color_format = K4A_IMAGE_FORMAT_COLOR_BGRA32;
				config.color_resolution = (k4a_color_resolution_t)k4aColorResolution;
				config.depth_mode = K4A_DEPTH_MODE_OFF;

				if (k4a_result_t::K4A_RESULT_SUCCEEDED == k4a_device_start_cameras(device, &config)) {
					// Camera warmup - dropping several first frames to let auto-exposure stabilize
					for (int i = 0; i < 15; i++) {
						k4a_capture_t capture = NULL;
						if (k4a_wait_result_t::K4A_WAIT_RESULT_SUCCEEDED == k4a_device_get_capture(device, &capture, 1000)) k4a_capture_release(capture);
					}
					k4a_devices.emplace_back(serial, device, 0u, osg::Matrix(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0), cameraMatrix, distCoeffs, width, height, k4aColorResolution); //Matrix needs to be all zeros at the start
					delete[] serial;
					continue;
				}
			}
		}
		if (device != NULL) k4a_device_close(device);
		delete[] serial;
	}
	std::cout << azureKinectCount << " connected Azure Kinect devices" << std::endl;

	std::vector<ScanDevice*> scanDevices;
	if (rs2_devices.empty() && k4a_devices.empty()) {
		std::cout << "ERROR: No connected devices found." << std::endl;
		m_calibrateCameraPosesState.store(3u);
		return scanDevices;
	}

	while (m_calibrateCameraPosesState.load() == 1u) {
		// Cycle through all devices previously found
		// RealSense:
		for (unsigned int i = 0; m_calibrateCameraPosesState.load() == 1u && i < rs2_devices.size(); i++) {
			rs2DeviceData& devData = rs2_devices.at(i);
			rs2::frameset frames;
			if (!devData._pipeline.try_wait_for_frames(&frames)) continue;

			rs2::video_frame colorFrame = frames.get_color_frame();
			cv::Mat colorImage(cv::Size(colorFrame.get_width(), colorFrame.get_height()), CV_8UC3, (void*)colorFrame.get_data(), cv::Mat::AUTO_STEP);

			if (!calibrateCameraPose(devData._cameraMatrix, devData._distCoeffs, colorImage, devData._checkerboardCount, devData._pose)) continue;
			std::cout << "RealSense " << devData._serial << " processed poses: " << devData._checkerboardCount << std::endl;

			// ### Draw camera positions ###
			bool expected;
			while (g_cameraPosesLock.compare_exchange_weak(expected = false, true));
			size_t index = cameraPosesCount + i;
			if (g_cameraPoses.size() <= index) g_cameraPoses.push_back(std::pair<osg::Matrix, ViewFrustum>(devData._pose, ViewFrustum(osg::Vec3f(), osg::Vec3f(), osg::Vec3f(), osg::Vec3f(), osg::Vec3f(), osg::Vec3f(), osg::Vec3f(), osg::Vec3f())));
			else g_cameraPoses.at(index).first = devData._pose;
			g_cameraPosesLock.store(false);
			// ######
		}

		// Azure Kinect:
		for (unsigned int i = 0; m_calibrateCameraPosesState.load() == 1u && i < k4a_devices.size(); i++) {
			k4aDeviceData& devData = k4a_devices.at(i);

			k4a_capture_t capture;
			if (K4A_RESULT_SUCCEEDED != k4a_device_get_capture(devData._device, &capture, 1000)) continue;

			k4a_image_t	colorFrame = k4a_capture_get_color_image(capture);
			cv::Mat rawImage(cv::Size(k4a_image_get_width_pixels(colorFrame), k4a_image_get_height_pixels(colorFrame)), CV_8UC4, (void*)k4a_image_get_buffer(colorFrame), cv::Mat::AUTO_STEP);
			cv::Mat colorImage;
			cv::cvtColor(rawImage, colorImage, cv::COLOR_BGRA2BGR);
			k4a_image_release(colorFrame);
			k4a_capture_release(capture);

			if (!calibrateCameraPose(devData._cameraMatrix, devData._distCoeffs, colorImage, devData._checkerboardCount, devData._pose)) continue;
			std::cout << "Azure Kinect " << devData._serial << " processed poses: " << devData._checkerboardCount << std::endl;

			// ### Draw camera positions ###
			bool expected;
			while (g_cameraPosesLock.compare_exchange_weak(expected = false, true));
			size_t index = cameraPosesCount + rs2_devices.size() + i;
			if (g_cameraPoses.size() <= index) g_cameraPoses.push_back(std::pair<osg::Matrix, ViewFrustum>(devData._pose, ViewFrustum(osg::Vec3f(), osg::Vec3f(), osg::Vec3f(), osg::Vec3f(), osg::Vec3f(), osg::Vec3f(), osg::Vec3f(), osg::Vec3f())));
			else g_cameraPoses.at(index).first = devData._pose;
			g_cameraPosesLock.store(false);
			// ######
		}
	}

	unsigned int realSensesAdded = 0u;
	for (rs2DeviceData& devData : rs2_devices) {
		devData._pipeline.stop();
		if (devData._checkerboardCount == 0) continue;
		PoseDriver* poseDriver = new StaticPoseDriver(devData._pose, osg::Matrix::identity());// osg::Matrix::translate(0.015, 0, 0));
		std::cout << "New ScanDevice: RealSense " << devData._serial << std::endl;

		RealSenseScanDevice* scanDevice = new RealSenseScanDevice(devData._device, rs2ctx, devData._width, devData._height, m_realSenseDepthWidth, m_realSenseDepthHeight, m_realSenseFPS, rs2_format::RS2_FORMAT_BGRA8, rs2_format::RS2_FORMAT_Z16, poseDriver, m_pointCloudWidth, m_pointCloudHeight, new cv::Mat(devData._cameraMatrix), new cv::Mat(devData._distCoeffs));
		scanDevices.push_back(scanDevice);
		++realSensesAdded;
	}
	unsigned int azureKinectsAdded = 0u;
	for (k4aDeviceData& devData : k4a_devices) {
		k4a_device_stop_cameras(devData._device);
		if (devData._checkerboardCount > 0) {
			PoseDriver* poseDriver = new StaticPoseDriver(devData._pose, osg::Matrix::identity());//) * osg::Matrix::rotate(osg::DegreesToRadians(-6.), osg::Vec3(1, 0, 0)) * osg::Matrix::translate(-0.032, 0, 0));

			k4a_device_configuration_t config = K4A_DEVICE_CONFIG_INIT_DISABLE_ALL;
			config.camera_fps = m_azureKinectFPS;
			config.color_format = K4A_IMAGE_FORMAT_COLOR_BGRA32;
			config.color_resolution = (k4a_color_resolution_t)devData._k4aColorResolution;
			config.depth_mode = m_azureKinectDepthMode;
			config.synchronized_images_only = true;

			AzureKinectScanDevice* scanDevice = new AzureKinectScanDevice(devData._device, config, poseDriver, m_pointCloudWidth, m_pointCloudHeight, new cv::Mat(devData._cameraMatrix), new cv::Mat(devData._distCoeffs));
			m_scanPipeline->addDevice(scanDevice);
			std::cout << "New ScanDevice: Azure Kinect " << devData._serial << std::endl;
			++azureKinectsAdded;
		}
		else k4a_device_close(devData._device);
	}
	std::cout << "Total RealSense devices: " << realSenseCount << " | RealSenseScanDevices added: " << realSensesAdded << std::endl;
	std::cout << "Total Azure Kinect devices: " << azureKinectCount << " | AzureKinectScanDevices added: " << azureKinectsAdded << std::endl;
	m_calibrateCameraPosesState.store(3u);
	return scanDevices;
}

bool PointCloudManager::loadMultiCamCalibration(std::map<std::string, osg::Matrix>& calibrationData, std::string* startDeviceSerial) {
	tinyxml2::XMLDocument xmlDoc;
	tinyxml2::XMLError eResult = xmlDoc.LoadFile("data/camera_calibration_files/multiCamCalibration.xml");
	if (eResult != tinyxml2::XML_SUCCESS) {
		std::cout << "### ERROR: 'data/camera_calibration_files/multiCamCalibration.xml' does not exist." << std::endl;
		return false;
	}
	try {
		tinyxml2::XMLElement* root = xmlDoc.FirstChildElement("MulticamCalibration");
		tinyxml2::XMLElement* device = root->FirstChildElement("StartDevice");
		std::string serial = device->Attribute("serial");
		if (startDeviceSerial) *startDeviceSerial = serial;
		calibrationData[serial] = osg::Matrix::identity();
		device = device->NextSiblingElement("Device");
		while (device) {
			tinyxml2::XMLElement* pos = device->FirstChildElement("translation");
			tinyxml2::XMLElement* rot = device->FirstChildElement("rotation");
			// Translation vector
			float t_x = pos->FloatAttribute("x") * 0.001f;
			float t_y = pos->FloatAttribute("y") * 0.001f;
			float t_z = pos->FloatAttribute("z") * 0.001f;
			// Rotation quaternion
			float q_x = rot->FloatAttribute("x");
			float q_y = rot->FloatAttribute("y");
			float q_z = rot->FloatAttribute("z");
			float q_w = rot->FloatAttribute("w");
			// Insert combined matrix into map
			calibrationData[device->Attribute("serial")] = osg::Matrix::rotate(osg::Quat(q_x, q_y, q_z, q_w)) * osg::Matrix::translate(t_x, t_y, t_z);
			device = device->NextSiblingElement("Device");
		}
	}
	catch (void* e) {
		std::cout << "### ERROR: Invalid multiCamCalibration file." << std::endl;
		return false;
	}
	return true;
}

bool PointCloudManager::updateMultiCamCalibration(std::map<std::string, osg::Matrix>& calibrationData) {
	tinyxml2::XMLDocument xmlDoc;
	tinyxml2::XMLError eResult = xmlDoc.LoadFile("data/camera_calibration_files/multiCamCalibration.xml");
	if (eResult != tinyxml2::XML_SUCCESS) {
		std::cout << "### ERROR: 'data/camera_calibration_files/multiCamCalibration.xml' does not exist." << std::endl;
		return false;
	}
	try {
		tinyxml2::XMLElement* root = xmlDoc.FirstChildElement("MulticamCalibration");
		tinyxml2::XMLElement* device = root->FirstChildElement("Device");
		while (device) {
			std::string serial = device->Attribute("serial");
			if (calibrationData.count(serial)) {
				osg::Vec3f t, s;
				osg::Quat q, so;
				calibrationData[serial].decompose(t, q, s, so);

				tinyxml2::XMLElement* pos = device->FirstChildElement("translation");
				tinyxml2::XMLElement* rot = device->FirstChildElement("rotation");
				// Translation vector
				pos->SetAttribute("x", t.x() * 1000);
				pos->SetAttribute("y", t.y() * 1000);
				pos->SetAttribute("z", t.z() * 1000);
				// Rotation quaternion
				rot->SetAttribute("x", q.x());
				rot->SetAttribute("y", q.y());
				rot->SetAttribute("z", q.z());
				rot->SetAttribute("w", q.w());
			}
			device = device->NextSiblingElement("Device");
		}
	}
	catch (void* e) {
		std::cout << "### ERROR: Invalid multiCamCalibration file." << std::endl;
		return false;
	}
	return tinyxml2::XML_SUCCESS == xmlDoc.SaveFile("data/camera_calibration_files/multiCamCalibration.xml");
}

std::vector<ScanDevice*> PointCloudManager::multiCamPoseCalibrationLoop() {
	std::cout << "MulitCamPoseCalibrationLoop started" << std::endl;
	std::vector<ScanDevice*> scanDevices;
	std::map<std::string, osg::Matrix> calibrationData;

	if (!loadMultiCamCalibration(calibrationData)) {
		std::cout << "ERROR: loadMuliCamCalibration failed." << std::endl;
		m_calibrateCameraPosesState.store(3u); return scanDevices;
	}

	// ### Draw camera positions ###
	size_t cameraPosesCount = g_cameraPoses.size();
	// ######

	struct rs2DeviceData {
		std::string _serial; rs2::pipeline _pipeline; rs2::device _device; osg::Matrix _pose; cv::Mat _cameraMatrix; cv::Mat _distCoeffs; unsigned int _width; unsigned int _height;
		rs2DeviceData(std::string&& serial, rs2::pipeline& pipeline, rs2::device& device, unsigned int checkerboardCount, osg::Matrix& pose, cv::Mat& cameraMatrix, cv::Mat& distCoeffs, unsigned int width, unsigned int height) :
			_serial(serial), _pipeline(pipeline), _device(device), _pose(pose), _cameraMatrix(cameraMatrix), _distCoeffs(distCoeffs), _width(width), _height(height) {}
	};
	struct k4aDeviceData {
		std::string _serial; k4a_device_t _device; osg::Matrix _pose; cv::Mat _cameraMatrix; cv::Mat _distCoeffs; unsigned int _width; unsigned int _height; int _k4aColorResolution;
		k4aDeviceData(std::string&& serial, k4a_device_t& device, unsigned int checkerboardCount, osg::Matrix& pose, cv::Mat& cameraMatrix, cv::Mat& distCoeffs, unsigned int width, unsigned int height, int k4aColorResolution) :
			_serial(serial), _device(device), _pose(pose), _cameraMatrix(cameraMatrix), _distCoeffs(distCoeffs), _width(width), _height(height), _k4aColorResolution(k4aColorResolution) {}
	};
	//Store the device and the number of successfully detected checkerboard patterns (used as weight) as well as the averaged pose
	std::vector<rs2DeviceData> rs2_devices;
	std::vector<k4aDeviceData> k4a_devices;

	// Query all available devices with valid CameraCalibration.xml for RealSense and Azure Kinect
	// RealSense:
	rs2::context rs2ctx;
	unsigned int realSenseCount = 0u;
	for (rs2::device&& device : rs2ctx.query_devices()) {
		const char* serial = device.get_info(RS2_CAMERA_INFO_SERIAL_NUMBER);
		++realSenseCount;
		if (m_scanPipeline->hasDevice(serial) || !calibrationData.count(serial)) continue;
		cv::Mat cameraMatrix;
		cv::Mat distCoeffs;
		unsigned int width = 0u;
		unsigned int height = 0u;
		if (loadCameraCalibration(serial, &cameraMatrix, &distCoeffs, width, height)) {
			rs2::config solvePNPcfg;
			solvePNPcfg.enable_stream(RS2_STREAM_COLOR, -1, width, height, rs2_format::RS2_FORMAT_BGR8, m_realSenseFPS);
			solvePNPcfg.enable_device(serial);
			rs2_devices.emplace_back(serial, rs2::pipeline(rs2ctx), device, 0u, osg::Matrix(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0), cameraMatrix, distCoeffs, width, height); //Matrix needs to be all zeros at the start

			rs2::pipeline& rsPipe = rs2_devices.at(rs2_devices.size() - 1)._pipeline;
			rsPipe.start(solvePNPcfg);
			// Camera warmup - dropping several first frames to let auto-exposure stabilize
			for (int i = 0; i < 15; i++) rsPipe.wait_for_frames();
		}
	}
	std::cout << realSenseCount << " connected Real Sense devices" << std::endl;
	// Azure Kinect:
	unsigned int azureKinectCount = k4a_device_get_installed_count();
	for (unsigned int deviceIndex = 0; deviceIndex < azureKinectCount; deviceIndex++) {
		k4a_device_t device = nullptr;
		size_t serial_num_size = 0;
		char* serial = nullptr;
		if (k4a_result_t::K4A_RESULT_SUCCEEDED == k4a_device_open(deviceIndex, &device) &&
			k4a_buffer_result_t::K4A_BUFFER_RESULT_TOO_SMALL == k4a_device_get_serialnum(device, NULL, &serial_num_size) &&
			k4a_buffer_result_t::K4A_BUFFER_RESULT_SUCCEEDED == k4a_device_get_serialnum(device, serial = new char[serial_num_size], &serial_num_size) &&
			!m_scanPipeline->hasDevice(serial) && calibrationData.count(serial))
		{
			cv::Mat cameraMatrix;
			cv::Mat distCoeffs;
			unsigned int width = 0u;
			unsigned int height = 0u;
			int k4aColorResolution = -1u;
			if (loadCameraCalibration(serial, &cameraMatrix, &distCoeffs, width, height) && (k4aColorResolution = k4aGetColorResolution(width, height)) > 0) {
				k4a_device_configuration_t config = K4A_DEVICE_CONFIG_INIT_DISABLE_ALL;
				config.camera_fps = m_azureKinectFPS;
				config.color_format = K4A_IMAGE_FORMAT_COLOR_BGRA32;
				config.color_resolution = (k4a_color_resolution_t)k4aColorResolution;
				config.depth_mode = K4A_DEPTH_MODE_OFF;

				if (k4a_result_t::K4A_RESULT_SUCCEEDED == k4a_device_start_cameras(device, &config)) {
					// Camera warmup - dropping several first frames to let auto-exposure stabilize
					for (int i = 0; i < 15; i++) {
						k4a_capture_t capture = NULL;
						if (k4a_wait_result_t::K4A_WAIT_RESULT_SUCCEEDED == k4a_device_get_capture(device, &capture, 1000)) k4a_capture_release(capture);
					}
					k4a_devices.emplace_back(serial, device, 0u, osg::Matrix(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0), cameraMatrix, distCoeffs, width, height, k4aColorResolution); //Matrix needs to be all zeros at the start
					delete[] serial;
					continue;
				}
			}
		}
		if (device != NULL) k4a_device_close(device);
		delete[] serial;
	}
	std::cout << azureKinectCount << " connected Azure Kinect devices" << std::endl;

	if (rs2_devices.empty() && k4a_devices.empty()) {
		std::cout << "ERROR: No connected devices found." << std::endl;
		m_calibrateCameraPosesState.store(3u);
		return scanDevices;
	}

	osg::Matrix pose;
	unsigned int checkerboardCount = 0;

	while (m_calibrateCameraPosesState.load() == 1u) {
		// Cycle through all devices previously found
		// RealSense:
		for (unsigned int i = 0; m_calibrateCameraPosesState.load() == 1u && i < rs2_devices.size(); i++) {
			rs2DeviceData& devData = rs2_devices.at(i);
			rs2::frameset frames;
			if (!devData._pipeline.try_wait_for_frames(&frames)) continue;

			rs2::video_frame colorFrame = frames.get_color_frame();
			cv::Mat colorImage(cv::Size(colorFrame.get_width(), colorFrame.get_height()), CV_8UC3, (void*)colorFrame.get_data(), cv::Mat::AUTO_STEP);

			if (!calibrateCameraPose(devData._cameraMatrix, devData._distCoeffs, colorImage, checkerboardCount, devData._pose, calibrationData[devData._serial], pose)) continue;
			std::cout << "RealSense " << devData._serial << " pose processed. Total poses: " << checkerboardCount << std::endl;

			// ### Draw camera positions ###
			bool expected;
			while (g_cameraPosesLock.compare_exchange_weak(expected = false, true));
			size_t index = cameraPosesCount + i;
			if (g_cameraPoses.size() <= index) g_cameraPoses.push_back(std::pair<osg::Matrix, ViewFrustum>(devData._pose, ViewFrustum(osg::Vec3f(), osg::Vec3f(), osg::Vec3f(), osg::Vec3f(), osg::Vec3f(), osg::Vec3f(), osg::Vec3f(), osg::Vec3f())));
			else g_cameraPoses.at(index).first = devData._pose;
			g_cameraPosesLock.store(false);
			// ######
		}

		// Azure Kinect:
		for (unsigned int i = 0; m_calibrateCameraPosesState.load() == 1u && i < k4a_devices.size(); i++) {
			k4aDeviceData& devData = k4a_devices.at(i);

			k4a_capture_t capture;
			if (K4A_RESULT_SUCCEEDED != k4a_device_get_capture(devData._device, &capture, 1000)) continue;

			k4a_image_t	colorFrame = k4a_capture_get_color_image(capture);
			cv::Mat rawImage(cv::Size(k4a_image_get_width_pixels(colorFrame), k4a_image_get_height_pixels(colorFrame)), CV_8UC4, (void*)k4a_image_get_buffer(colorFrame), cv::Mat::AUTO_STEP);
			cv::Mat colorImage;
			cv::cvtColor(rawImage, colorImage, cv::COLOR_BGRA2BGR);
			k4a_image_release(colorFrame);
			k4a_capture_release(capture);

			if (calibrateCameraPose(devData._cameraMatrix, devData._distCoeffs, colorImage, checkerboardCount, devData._pose, calibrationData[devData._serial], pose)) std::cout << "Azure Kinect " << devData._serial << " pose processed. Total poses: " << checkerboardCount << std::endl;
			if (checkerboardCount == 0) continue;

			// ### Draw camera positions ###
			bool expected;
			while (g_cameraPosesLock.compare_exchange_weak(expected = false, true));
			size_t index = cameraPosesCount + rs2_devices.size() + i;
			if (g_cameraPoses.size() <= index) g_cameraPoses.push_back(std::pair<osg::Matrix, ViewFrustum>(devData._pose, ViewFrustum(osg::Vec3f(), osg::Vec3f(), osg::Vec3f(), osg::Vec3f(), osg::Vec3f(), osg::Vec3f(), osg::Vec3f(), osg::Vec3f())));
			else g_cameraPoses.at(index).first = devData._pose;
			g_cameraPosesLock.store(false);
			// ######
		}
	}


	unsigned int realSensesAdded = 0u;
	for (rs2DeviceData& devData : rs2_devices) {
		devData._pipeline.stop();
		if (checkerboardCount == 0) continue;
		PoseDriver* poseDriver = new StaticPoseDriver(pose, calibrationData[devData._serial]);// osg::Matrix::translate(0.015, 0, 0));
		std::cout << "New ScanDevice: RealSense " << devData._serial << std::endl;

		RealSenseScanDevice* scanDevice = new RealSenseScanDevice(devData._device, rs2ctx, devData._width, devData._height, m_realSenseDepthWidth, m_realSenseDepthHeight, m_realSenseFPS, rs2_format::RS2_FORMAT_BGRA8, rs2_format::RS2_FORMAT_Z16, poseDriver, m_pointCloudWidth, m_pointCloudHeight, new cv::Mat(devData._cameraMatrix), new cv::Mat(devData._distCoeffs));
		scanDevices.push_back(scanDevice);
		++realSensesAdded;
	}
	unsigned int azureKinectsAdded = 0u;
	for (k4aDeviceData& devData : k4a_devices) {
		k4a_device_stop_cameras(devData._device);
		if (checkerboardCount > 0) {
			PoseDriver* poseDriver = new StaticPoseDriver(pose, calibrationData[devData._serial]);//) * osg::Matrix::rotate(osg::DegreesToRadians(-6.), osg::Vec3(1, 0, 0)) * osg::Matrix::translate(-0.032, 0, 0));

			k4a_device_configuration_t config = K4A_DEVICE_CONFIG_INIT_DISABLE_ALL;
			config.camera_fps = m_azureKinectFPS;
			config.color_format = K4A_IMAGE_FORMAT_COLOR_BGRA32;
			config.color_resolution = (k4a_color_resolution_t)devData._k4aColorResolution;
			config.depth_mode = m_azureKinectDepthMode;
			config.synchronized_images_only = true;

			AzureKinectScanDevice* scanDevice = new AzureKinectScanDevice(devData._device, config, poseDriver, m_pointCloudWidth, m_pointCloudHeight, new cv::Mat(devData._cameraMatrix), new cv::Mat(devData._distCoeffs));
			m_scanPipeline->addDevice(scanDevice);
			std::cout << "New ScanDevice: Azure Kinect " << devData._serial << std::endl;
			++azureKinectsAdded;
		}
		else k4a_device_close(devData._device);
	}
	std::cout << "Total RealSense devices: " << realSenseCount << " | RealSenseScanDevices added: " << realSensesAdded << std::endl;
	std::cout << "Total Azure Kinect devices: " << azureKinectCount << " | AzureKinectScanDevices added: " << azureKinectsAdded << std::endl;
	m_calibrateCameraPosesState.store(3u);
	return scanDevices;
}

void PointCloudManager::finalizeCameraPoseCalibration() {
	if (m_calibrateCameraPosesState.load() == 3u && m_calibrateCameraPosesAsync.valid()) {
		std::vector<ScanDevice*> scanDevices = m_calibrateCameraPosesAsync.get();
		for (ScanDevice* device : scanDevices) m_scanPipeline->addDevice(device);
		m_calibrateCameraPosesState.store(0u);
		m_scanPipeline->startAllDevices();
		if (m_scanPipeline->getSelectedDeviceIndex() == -1) m_scanPipeline->selectDevice(0);

		m_devicesPerScan = m_configReader->getIntFromStartupConfig("devices_per_scan");
		{
			size_t deviceCount = m_scanPipeline->getDeviceCount();
			if (m_devicesPerScan == 0 || m_devicesPerScan > deviceCount) m_devicesPerScan = deviceCount;
		}

		unsigned int maxWidth, maxHeight;
		getMaxScanDeviceResolution(maxWidth, maxHeight);

		m_pointCloudVisualizer->updatePointCloudBuffers(m_devicesPerScan, maxWidth, maxHeight);
		std::cout << "devicesPerScan: " << m_devicesPerScan << std::endl;
		std::cout << "### Camera pose calibration ended" << std::endl;
	}
}

bool PointCloudManager::calibrateCameraPose(const cv::Mat& cameraMatrix, const cv::Mat& distCoeffs, cv::Mat& image, unsigned int& camPoseWeight, osg::Matrix& camPose) {
	osg::Matrix calculatedCamPose;
	if (!calculateScanDevicePoseViaPattern(cameraMatrix, distCoeffs, image, &calculatedCamPose)) return false;

	// Setup transformation matrices to transform between ClayMore and the Tracker coordinate systems
	osg::Matrix trackerToClayMoreRot1 = osg::Matrix::rotate(osg::DegreesToRadians(90.0f), osg::Vec3(1, 0, 0));
	osg::Matrix trackerToClayMoreRot2 = osg::Matrix::rotate(osg::DegreesToRadians(180.0f), osg::Vec3(0, 1, 0));

	bool expected;
	while (!m_timedCircularTrackerMatrixBuffer_Lock->compare_exchange_weak(expected = false, true)); //Lock the trackerMatrixBuffer before accessing its data
	Matrix34 trackerPose = m_timedCircularTrackerMatrixBuffer->at(m_timedCircularTrackerMatrixBuffer->size() - 1).matrix;
	m_timedCircularTrackerMatrixBuffer_Lock->store(false); // Unlock the trackerMatrixBuffer after obtaining the newest trackerPosition

	osg::Matrix trackerMatrix(
		trackerPose.m[0][0], trackerPose.m[1][0], trackerPose.m[2][0], 0.0,
		trackerPose.m[0][1], trackerPose.m[1][1], trackerPose.m[2][1], 0.0,
		trackerPose.m[0][2], trackerPose.m[1][2], trackerPose.m[2][2], 0.0,
		trackerPose.m[0][3], trackerPose.m[1][3], trackerPose.m[2][3], 1.0f
	);

	calculatedCamPose = calculatedCamPose * m_patternToTrackerTransform * trackerMatrix;
	double w1 = ((double)camPoseWeight) / (camPoseWeight + 1u);
	double w2 = 1 - w1;
	++camPoseWeight;

	osg::Vec3 t, t1, s1, t2, s2;
	osg::Quat q, q1, so1, q2, so2;
	camPose.decompose(t1, q1, s1, so1);
	calculatedCamPose.decompose(t2, q2, s2, so2);

	q.slerp(w2, q1, q2);
	t = t1 * w1 + t2 * w2;
	camPose = osg::Matrix::rotate(q) * osg::Matrix::translate(t);
	/*camPose.set(
		calculatedCamPose.ptr()[0] * w2 + camPose.ptr()[0] * w1, calculatedCamPose.ptr()[1] * w2 + camPose.ptr()[1] * w1, calculatedCamPose.ptr()[2] * w2 + camPose.ptr()[2] * w1, calculatedCamPose.ptr()[3] * w2 + camPose.ptr()[3] * w1,
		calculatedCamPose.ptr()[4] * w2 + camPose.ptr()[4] * w1, calculatedCamPose.ptr()[5] * w2 + camPose.ptr()[5] * w1, calculatedCamPose.ptr()[6] * w2 + camPose.ptr()[6] * w1, calculatedCamPose.ptr()[7] * w2 + camPose.ptr()[7] * w1,
		calculatedCamPose.ptr()[8] * w2 + camPose.ptr()[8] * w1, calculatedCamPose.ptr()[9] * w2 + camPose.ptr()[9] * w1, calculatedCamPose.ptr()[10] * w2 + camPose.ptr()[10] * w1, calculatedCamPose.ptr()[11] * w2 + camPose.ptr()[11] * w1,
		calculatedCamPose.ptr()[12] * w2 + camPose.ptr()[12] * w1, calculatedCamPose.ptr()[13] * w2 + camPose.ptr()[13] * w1, calculatedCamPose.ptr()[14] * w2 + camPose.ptr()[14] * w1, calculatedCamPose.ptr()[15] * w2 + camPose.ptr()[15] * w1
	);*/
	return true;
}

bool PointCloudManager::calibrateCameraPose(const cv::Mat& cameraMatrix, const cv::Mat& distCoeffs, cv::Mat& image, unsigned int& camPoseWeight, osg::Matrix& camPose, osg::Matrix& camOffset, osg::Matrix& multiCamPose) {
	osg::Matrix calculatedCamPose;
	if (!calculateScanDevicePoseViaPattern(cameraMatrix, distCoeffs, image, &calculatedCamPose)) {
		camPose = camOffset * multiCamPose;
		return false;
	}

	// Setup transformation matrices to transform between ClayMore and the Tracker coordinate systems
	osg::Matrix trackerToClayMoreRot1 = osg::Matrix::rotate(osg::DegreesToRadians(90.0f), osg::Vec3(1, 0, 0));
	osg::Matrix trackerToClayMoreRot2 = osg::Matrix::rotate(osg::DegreesToRadians(180.0f), osg::Vec3(0, 1, 0));

	bool expected;
	while (!m_timedCircularTrackerMatrixBuffer_Lock->compare_exchange_weak(expected = false, true)); //Lock the trackerMatrixBuffer before accessing its data
	Matrix34 trackerPose = m_timedCircularTrackerMatrixBuffer->at(m_timedCircularTrackerMatrixBuffer->size() - 1).matrix;
	m_timedCircularTrackerMatrixBuffer_Lock->store(false); // Unlock the trackerMatrixBuffer after obtaining the newest trackerPosition

	osg::Matrix trackerMatrix(
		trackerPose.m[0][0], trackerPose.m[1][0], trackerPose.m[2][0], 0.0,
		trackerPose.m[0][1], trackerPose.m[1][1], trackerPose.m[2][1], 0.0,
		trackerPose.m[0][2], trackerPose.m[1][2], trackerPose.m[2][2], 0.0,
		trackerPose.m[0][3], trackerPose.m[1][3], trackerPose.m[2][3], 1.0f
	);

	calculatedCamPose = osg::Matrix::inverse(camOffset) * calculatedCamPose * m_patternToTrackerTransform * trackerMatrix;
	double w1 = ((double)camPoseWeight) / (camPoseWeight + 1u);
	double w2 = 1 - w1;
	++camPoseWeight;

	osg::Vec3 t, t1, s1, t2, s2;
	osg::Quat q, q1, so1, q2, so2;
	multiCamPose.decompose(t1, q1, s1, so1);
	calculatedCamPose.decompose(t2, q2, s2, so2);

	q.slerp(w2, q1, q2);
	t = t1 * w1 + t2 * w2;
	multiCamPose = osg::Matrix::rotate(q) * osg::Matrix::translate(t);
	camPose = camOffset * multiCamPose;

	return true;
}

void PointCloudManager::startMultiCamOptimization() {
	if (m_multiCamOptimizationLock.load()) { m_multiCamOptimizationLock.store(false); return; }
	m_multiCamOptimizationLock.store(true);
	std::thread(&PointCloudManager::multiCamOptimizationLoop, this, 100u).detach();
}

void PointCloudManager::multiCamOptimizationLoop(const unsigned int iterations) {
	unsigned int deviceCount = m_scanPipeline->getDeviceCount();
	//cv::setBreakOnError(true); //Enable when encountering OpenCV-related errors

	if (deviceCount == 0) {
		std::cout << "ERROR: No active ScanDevices." << std::endl;
		m_multiCamOptimizationLock.store(false); return;
	}

	//Find the startDevice for the current multiCamCalibration
	std::map<std::string, osg::Matrix> calibrationData;
	std::string startDeviceSerial;

	if (!loadMultiCamCalibration(calibrationData, &startDeviceSerial)) {
		std::cout << "ERROR: Couldn't load multiCamCalibration." << std::endl;
		m_multiCamOptimizationLock.store(false); return;
	}

	ScanDevice* startDevice = m_scanPipeline->getDevice(startDeviceSerial);
	int startDeviceIndex = m_scanPipeline->getDeviceIndex(startDevice);

	std::vector<std::map<int, osg::Vec3f>> patternFeatures(deviceCount);
	std::vector<std::pair<osg::Vec3f, osg::Quat>> offsets(deviceCount, { osg::Vec3f(0, 0, 0), osg::Quat(0, 0, 0, 1) });
	std::vector<unsigned int> successfulIterations(deviceCount, 0);

	for (unsigned int iteration = 0u; iteration < iterations; ++iteration) {
		bool skip = false;
		do {
			skip = false;
			// Get a new colorFrame for each device
			for (unsigned int d = 0u; d < deviceCount && !skip; ++d) {
				ScanDevice* device = m_scanPipeline->getDevice(d);
				if (ScanDevice::ScanResult::SUCCESS != device->scan(TIMEOUT_MS)) skip = true;
			}
			// Calculate the pointCloud for each device
			for (unsigned int d = 0u; d < deviceCount && !skip; ++d) {
				ScanDevice* device = m_scanPipeline->getDevice(d);
				if (ScanDevice::PointcloudCalculationResult::SUCCESS != device->calculatePointcloud()) skip = true;
			}
		} while (skip); // Repeat all if one device failed

		std::vector<cv::Mat> deviceColor;
		for (unsigned int d = 0u; d < deviceCount; ++d) {
			auto& featurePoints = patternFeatures[d];
			featurePoints.clear();

			ScanDevice* device = m_scanPipeline->getDevice(d);

			cv::Mat colorDataDistorted = convertColorDataToOpenCVMat(device->getColorData());
			cv::Mat colorData;

			if (device->getCameraMatrix() && device->getDistortionCoefficients()) cv::undistort(colorDataDistorted, colorData, *device->getCameraMatrix(), *device->getDistortionCoefficients());
			else colorData = colorDataDistorted;

			// Currently only implemented for ChArUco
			std::vector<int> markerIds;
			std::vector<std::vector<cv::Point2f>> markerCorners;

			std::vector<int> charucoIds;
			std::vector<cv::Point2f> charucoCorners;

			cv::aruco::detectMarkers(colorData, m_charucoDictionary, markerCorners, markerIds, m_charucoParams);
			if (markerIds.empty()) continue;
			if (cv::aruco::interpolateCornersCharuco(markerCorners, markerIds, colorData, m_charucoBoard, charucoCorners, charucoIds) < m_charucoMinDetectedPoints) continue;

			PointcloudData* pointcloudData = device->getPointcloudData();

			// Find point cloud point corresponding to pixel coordinates of the ChArUco corners
			for (unsigned int i = 0u; i < charucoCorners.size(); ++i) {
				float u = charucoCorners[i].x;
				float v = charucoCorners[i].y;

				float uWeight = u - floorf(u);
				float vWeight = v - floorf(v);

				int j = pointcloudData->width() * floorf(v) + floorf(u);
				osg::Vec3f p(pointcloudData->x(j), pointcloudData->y(j), pointcloudData->z(j));
				/*osg::Vec3f p((pointcloudData->x(j) * uWeight + pointcloudData->x(j + 1) * (1 - uWeight)) * vWeight + (pointcloudData->x(j + pointcloudData->width()) * uWeight + pointcloudData->x(j + pointcloudData->width() + 1) * (1 - uWeight)) * (1 - vWeight),
					(pointcloudData->y(j) * uWeight + pointcloudData->y(j + 1) * (1 - uWeight)) * vWeight + (pointcloudData->y(j + pointcloudData->width()) * uWeight + pointcloudData->y(j + pointcloudData->width() + 1) * (1 - uWeight)) * (1 - vWeight),
					(pointcloudData->z(j) * uWeight + pointcloudData->z(j + 1) * (1 - uWeight)) * vWeight + (pointcloudData->z(j + pointcloudData->width()) * uWeight + pointcloudData->z(j + pointcloudData->width() + 1) * (1 - uWeight)) * (1 - vWeight));*/
				featurePoints[charucoIds[i]] = p * device->getSensorToDriverMatrix();
			}
		}

		for (unsigned int d = 0u; d < deviceCount; ++d) {
			if (m_scanPipeline->getDevice(d) == startDevice) continue;
			auto& featurePoints = patternFeatures[d];
			auto& startDeviceFeaturePoints = patternFeatures[startDeviceIndex];
			if (featurePoints.empty() || startDeviceFeaturePoints.empty()) continue;
			osg::Vec3f p_d;
			osg::Vec3f p_s;
			unsigned int numPoints = 0;
			for (auto& kv : featurePoints) {
				if (!startDeviceFeaturePoints.count(kv.first)) continue;
				p_d += kv.second;
				p_s += startDeviceFeaturePoints[kv.first];
				++numPoints;
			}
			if (!numPoints) continue;

			p_d *= 1.f / numPoints;
			p_s *= 1.f / numPoints;

			//osg::Vec3f p = p_s - p_d;

			osg::Quat q;
			numPoints = 0;
			for (unsigned int u = 0u; u < m_checkerboardDimX - 1; ++u) {
				for (unsigned int v = 0u; v < m_checkerboardDimY - 1; ++v) {
					if (!featurePoints.count(v * m_checkerboardDimX + u + 1) || !featurePoints.count((v + 1) * m_checkerboardDimX + u) || !featurePoints.count(v * m_checkerboardDimX + u)) continue;
					if (!startDeviceFeaturePoints.count(v * m_checkerboardDimX + u + 1) || !startDeviceFeaturePoints.count((v + 1) * m_checkerboardDimX + u) || !startDeviceFeaturePoints.count(v * m_checkerboardDimX + u)) continue;

					osg::Vec3f p1_d = featurePoints[v * m_checkerboardDimX + u + 1] - featurePoints[v * m_checkerboardDimX + u];
					osg::Vec3f p2_d = featurePoints[(v + 1) * m_checkerboardDimX + u] - featurePoints[v * m_checkerboardDimX + u];
					osg::Vec3f n_d = p1_d ^ p2_d;

					osg::Vec3f p1_s = startDeviceFeaturePoints[v * m_checkerboardDimX + u + 1] - startDeviceFeaturePoints[v * m_checkerboardDimX + u];
					osg::Vec3f p2_s = startDeviceFeaturePoints[(v + 1) * m_checkerboardDimX + u] - startDeviceFeaturePoints[v * m_checkerboardDimX + u];
					osg::Vec3f n_s = p1_s ^ p2_s;

					p1_d.normalize();
					p2_d.normalize();
					n_d.normalize();

					p1_s.normalize();
					p2_s.normalize();
					n_s.normalize();

					osg::Matrix M_d(p1_d.x(), p1_d.y(), p1_d.z(), 0, p2_d.x(), p2_d.y(), p2_d.z(), 0, n_d.x(), n_d.y(), n_d.z(), 0, 0, 0, 0, 1);
					osg::Matrix M_s(p1_s.x(), p1_s.y(), p1_s.z(), 0, p2_s.x(), p2_s.y(), p2_s.z(), 0, n_s.x(), n_s.y(), n_s.z(), 0, 0, 0, 0, 1);

					++numPoints;
					q.slerp(1.f / numPoints, q, (osg::Matrix::inverse(M_d) * M_s).getRotate());
				}
			}
			if (!numPoints) continue;

			unsigned int& i = successfulIterations[d];
			osg::Matrix M = osg::Matrix::translate(-p_d) * osg::Matrix::rotate(q) * osg::Matrix::translate(p_s);
			offsets[d].first = (offsets[d].first * i + M.getTrans()) / (i + 1);
			offsets[d].second.slerp(1.f / (i + 1), offsets[d].second, M.getRotate());
			++i;
		}
		std::cout << "Updating MulitCamCalibration: " << iteration + 1 << "/" << iterations << " iterations" << std::endl;
	}
	for (unsigned int d = 0u; d < deviceCount; ++d) {
		if (m_scanPipeline->getDevice(d) == startDevice) continue;
		std::cout << offsets[d].first.x() << ", " << offsets[d].first.y() << ", " << offsets[d].first.z() << std::endl;

		auto& cd = calibrationData[m_scanPipeline->getDevice(d)->getSerial()];
		cd *= osg::Matrix::rotate(offsets[d].second) * osg::Matrix::translate(offsets[d].first);

		StaticPoseDriver* driver = (StaticPoseDriver*)m_scanPipeline->getDevice(d)->getDriver();
		driver->updateTransform(cd);

		bool expected;
		while (g_cameraPosesLock.compare_exchange_weak(expected = false, true));
		g_cameraPoses[d].first = driver->getTransform(0u);
		g_cameraPosesLock.store(false);
	}
	if (updateMultiCamCalibration(calibrationData)) std::cout << "MultiCamCalibration updated successfully" << std::endl;
	else std::cout << "ERROR: MulitCamCalibration could not be updated." << std::endl;
	m_multiCamOptimizationLock.store(false);
}

/* ### START - Old Scan Pipeline ###
void PointCloudManager::stopPointCloudPipe() {
	if (m_processingThread.joinable())
	{
		m_processingThread.join();
	}
	if (m_useMultipleRealSenseDevices)
	{
		std::map<std::string, RealSenseDeviceContainer::view_port>::iterator it;
		for (it = m_realSenseDeviceContainer->getDevices()->begin(); it != m_realSenseDeviceContainer->getDevices()->end(); it++) //Run over all connected
		{
			it->second.pipe.stop();
		}
	}
	else
	{
		m_rsPipe->stop();
	}
}

//Grab the first few frames at startup and immediately dump them. The first frame does not seem to contain color information.
void PointCloudManager::warmUpRealSense(int cycles) {
	if (m_useMultipleRealSenseDevices)
	{
		std::map<std::string, RealSenseDeviceContainer::view_port>::iterator it;
		for (it = m_realSenseDeviceContainer->getDevices()->begin(); it != m_realSenseDeviceContainer->getDevices()->end(); it++)
		{
			for (int i = 0; i < cycles; i++) {
				rs2::frameset frames = it->second.pipe.wait_for_frames();
				rs2::frame depth = frames.get_depth_frame();
				rs2::video_frame colorFrame = frames.get_color_frame();
				m_rsPc->map_to(colorFrame);
			}
		}
	}
	else {
		for (int i = 0; i < cycles; i++) {
			rs2::frameset frames = m_rsPipe->wait_for_frames();
			rs2::frame depth = frames.get_depth_frame();
			rs2::video_frame colorFrame = frames.get_color_frame();
			m_rsPc->map_to(colorFrame);
		}
	}
}
// ### END - Old Scan Pipeline ### */

//Source: https://github.com/IntelRealSense/librealsense/blob/master/wrappers/pcl/pcl/rs-pcl.cpp
//TODO: Do we already have an implementation for converting a pointcloud from rs to pcl?
/*pcl::PointCloud<pcl::PointXYZRGB>::Ptr PointCloudManager::rsPoints_to_pcl(const rs2::points& points, const rs2::video_frame& colorFrameIn)
{
	pcl::PointCloud<pcl::PointXYZRGB>::Ptr pointCloud(new pcl::PointCloud<pcl::PointXYZRGB>);
	auto sp = points.get_profile().as<rs2::video_stream_profile>();
	pointCloud->width = sp.width();
	pointCloud->height = sp.height();
	pointCloud->is_dense = false;
	pointCloud->points.resize(points.size());

	//osg::ref_ptr<osg::ImageStream> imageStream = new osg::ImageStream();
	//imageStream->allocateImage(RS_WIDTH, RS_HEIGHT, 1, GL_RGB, GL_UNSIGNED_BYTE);
	//imageStream->setImage(
	//	colorFrameIn.get_width(),
	//	colorFrameIn.get_height(),
	//	1,
	//	GL_RGB,
	//	GL_RGB,
	//	GL_UNSIGNED_BYTE,
	//	const_cast<unsigned char*>(static_cast<const unsigned char*>(colorFrameIn.get_data())), //Muss man nicht verstehen, da wird nur etwas recht kompliziert gecastet
	//	osg::Image::AllocationMode::NO_DELETE); //Bild der RealSense wird auf den ImageStream gesetzt

	auto ptr = points.get_vertices();
	//auto colorPtr = points.get_texture_coordinates();
	for (auto& p : pointCloud->points)
	{
		//osg::Vec4 color = imageStream->getColor(osg::Vec2(colorPtr->u, colorPtr->v));
		p.x = ptr->x;
		p.y = ptr->y;
		p.z = ptr->z;
		//TODO: Color data is currently lost at that point
		/*p.r = color.r();
		p.g = color.g();
		p.b = color.b();
		ptr++;
	}
	return pointCloud;
}*/

/* ### START - Old Scan Pipeline ###
void PointCloudManager::renderVideoTexture() {
	if (m_useMultipleRealSenseDevices)
	{

	}
	else {
		rs2::frameset frames = m_rsPipe->wait_for_frames();
		rs2::video_frame colorFrame = frames.get_color_frame();
		m_pointCloudVisualizer->renderVideoTexture(colorFrame);
	}
}
// ### END - Old Scan Pipeline ### */

void PointCloudManager::checkLockSceneGraph()
{
	double lastMeshInSceneGraph = std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::system_clock::now().time_since_epoch()).count();
	while (true)
	{
		if (m_isSceneGraphLockedForRendering->load())
		{
			for (int i = 0; i < m_maxNumThreads; i++)
			{
				if (m_liveMeshPool.at(i)->getCurrentState() == LiveMesh::IN_SCENE_GRAPH)
				{
					std::unique_lock<std::mutex> lock(m_mutex);
					m_cv.wait(lock);
					m_liveMeshPool.at(i)->setStateToRendered();
					std::cout << "########################################## lastMeshInSceneGraph: " <<
						(std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::system_clock::now().time_since_epoch()).count() - lastMeshInSceneGraph)
						<< std::endl;
					lastMeshInSceneGraph = std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::system_clock::now().time_since_epoch()).count();
				}
			}
		}
	}
}

void PointCloudManager::manageMeshes()
{
	std::map<std::chrono::microseconds, int> threadIDWithTimestamp;
	while (true)
	{
		if (!m_isSceneGraphLockedForRendering->load())
		{
			threadIDWithTimestamp.clear();

			for (int i = 0; i < m_maxNumThreads; i++)
			{
				if (m_liveMeshPool.at(i)->getCurrentState() == LiveMesh::READY_TO_RENDER)
				{
					threadIDWithTimestamp.insert(std::pair<std::chrono::microseconds, int>(m_liveMeshPool.at(i)->getKinectCaptureTimestamp(), i));
				}
			}

			std::map<std::chrono::microseconds, int>::iterator it;
			bool firstSkipped = false;
			for (it = threadIDWithTimestamp.begin(); it != threadIDWithTimestamp.end(); it++)
			{
				if (firstSkipped)
				{
					firstSkipped = true;
				}
				else
				{
					m_liveMeshPool.at(it->second)->setStateToRendered();
				}
			}


			if (threadIDWithTimestamp.size() > 0 && !m_isSceneGraphLockedForRendering->load())
			{
				for (int i = 0; i < m_maxNumThreads; i++)
				{
					if (i != threadIDWithTimestamp.begin()->second)
					{
						m_liveMeshPool.at(i)->setMeshInactive();

						//std::cout << "Deactivating mesh in thread nr. " << i << std::endl;
					}
					else
					{
						m_liveMeshPool.at(i)->setMeshActive();

						//std::cout << "Activating mesh in thread nr. " << i << std::endl;
					}
				}
			}
		}
		std::this_thread::sleep_for(std::chrono::milliseconds(10));
	}
}

unsigned long long PointCloudManager::deviceToSystemClock(unsigned long long deviceTimestamp) {
	return m_system_clock_origin + deviceTimestamp - m_device_clock_origin;
}

bool PointCloudManager::loadCameraCalibration(const char* serial, cv::Mat* cameraMatrix_out, cv::Mat* distCoeffs_out, unsigned int& width_out, unsigned int& height_out) {
	//TODO 1: This function is useless as of now -> fix it and implement it properly
	int seenCheckerImages = 0;
	cv::Mat R, T;

	*cameraMatrix_out = cv::Mat(3, 3, CV_64F);
	//*distCoeffs_out = cv::Mat(1, 5, CV_64F);

	//Read CameraParameters.xml and set cameraMatrix and DistCoeffs
	std::vector<double> xmlDataCameraMatrix;
	std::vector<double> xmlDataDissCoeff;
	tinyxml2::XMLDocument xmlDoc;

	std::string filePath("data/camera_calibration_files/");
	filePath += serial;
	filePath += ".xml";
	tinyxml2::XMLError eResult = xmlDoc.LoadFile(filePath.c_str());
	if (eResult != tinyxml2::XML_SUCCESS) return false;

	tinyxml2::XMLNode* pRoot = xmlDoc.FirstChild();
	if (pRoot == nullptr) return false;
	tinyxml2::XMLNode* cameraMatrixNode = pRoot->FirstChildElement("camera_matrix");
	if (cameraMatrixNode == nullptr) return false;

	double e00;
	eResult = cameraMatrixNode->FirstChildElement("e00")->QueryDoubleText(&e00);
	xmlDataCameraMatrix.push_back(e00);

	double e01;
	eResult = cameraMatrixNode->FirstChildElement("e01")->QueryDoubleText(&e01);
	xmlDataCameraMatrix.push_back(e01);

	double e02;
	eResult = cameraMatrixNode->FirstChildElement("e02")->QueryDoubleText(&e02);
	xmlDataCameraMatrix.push_back(e02);


	double e10;
	eResult = cameraMatrixNode->FirstChildElement("e10")->QueryDoubleText(&e10);
	xmlDataCameraMatrix.push_back(e10);

	double e11;
	eResult = cameraMatrixNode->FirstChildElement("e11")->QueryDoubleText(&e11);
	xmlDataCameraMatrix.push_back(e11);

	double e12;
	eResult = cameraMatrixNode->FirstChildElement("e12")->QueryDoubleText(&e12);
	xmlDataCameraMatrix.push_back(e12);


	double e20;
	eResult = cameraMatrixNode->FirstChildElement("e20")->QueryDoubleText(&e20);
	xmlDataCameraMatrix.push_back(e20);

	double e21;
	eResult = cameraMatrixNode->FirstChildElement("e21")->QueryDoubleText(&e21);
	xmlDataCameraMatrix.push_back(e21);

	double e22;
	eResult = cameraMatrixNode->FirstChildElement("e22")->QueryDoubleText(&e22);
	xmlDataCameraMatrix.push_back(e22);


	//Distortion Coefficients
	tinyxml2::XMLNode* distCoeffNode = pRoot->FirstChildElement("dist_coeff");
	if (distCoeffNode == nullptr) return false; // tinyxml2::XML_ERROR_FILE_READ_ERROR;

	/*double dC0;
	eResult = distCoeffNode->FirstChildElement("dC0")->QueryDoubleText(&dC0);
	xmlDataDissCoeff.push_back(dC0);

	double dC1;
	eResult = distCoeffNode->FirstChildElement("dC1")->QueryDoubleText(&dC1);
	xmlDataDissCoeff.push_back(dC1);

	double dC2;
	eResult = distCoeffNode->FirstChildElement("dC2")->QueryDoubleText(&dC2);
	xmlDataDissCoeff.push_back(dC2);

	double dC3;
	eResult = distCoeffNode->FirstChildElement("dC3")->QueryDoubleText(&dC3);
	xmlDataDissCoeff.push_back(dC3);

	double dC4;
	eResult = distCoeffNode->FirstChildElement("dC4")->QueryDoubleText(&dC4);
	xmlDataDissCoeff.push_back(dC4);*/

	tinyxml2::XMLElement* child = distCoeffNode->FirstChildElement();
	while (child) {
		double d;
		child->QueryDoubleText(&d);
		xmlDataDissCoeff.push_back(d);
		child = child->NextSiblingElement();
	}
	if (!(xmlDataDissCoeff.size() == 4 || xmlDataDissCoeff.size() == 5 || xmlDataDissCoeff.size() == 8 || xmlDataDissCoeff.size() == 12 || xmlDataDissCoeff.size() == 14)) return false;


	//Resolution
	tinyxml2::XMLNode* resolutionNode = pRoot->FirstChildElement("resolution");
	if (resolutionNode == nullptr) return false;

	//Only change data if the whole xml file could be parsed
	resolutionNode->FirstChildElement("width")->QueryUnsignedText(&width_out);
	resolutionNode->FirstChildElement("height")->QueryUnsignedText(&height_out);

	cameraMatrix_out->at<double>(0, 0) = e00;
	cameraMatrix_out->at<double>(0, 1) = e01;
	cameraMatrix_out->at<double>(0, 2) = e02;
	cameraMatrix_out->at<double>(1, 0) = e10;
	cameraMatrix_out->at<double>(1, 1) = e11;
	cameraMatrix_out->at<double>(1, 2) = e12;
	cameraMatrix_out->at<double>(2, 0) = e20;
	cameraMatrix_out->at<double>(2, 1) = e21;
	cameraMatrix_out->at<double>(2, 2) = e22;

	//memcpy(cameraMatrix->data, xmlDataCameraMatrix.data(), xmlDataCameraMatrix.size() * sizeof(double));

	*distCoeffs_out = cv::Mat(xmlDataDissCoeff.size(), 1, CV_64F);
	memcpy(distCoeffs_out->data, xmlDataDissCoeff.data(), xmlDataDissCoeff.size() * sizeof(double));

	return true;
}

std::vector<cv::Point3f> PointCloudManager::generateCheckerboardPattern() {
	std::vector<cv::Point3f> checkerboardPoints;
	for (int i{ 0 }; i < m_checkerboardDimY; i++) {
		for (int j{ 0 }; j < m_checkerboardDimX; j++) checkerboardPoints.push_back(cv::Point3f(j * m_checkerboardSquareSizeInMM, i * m_checkerboardSquareSizeInMM, 0.0f));
	}
	return checkerboardPoints;
}

bool PointCloudManager::calculateScanDevicePoseViaPattern(const cv::Mat& cameraMatrix, const cv::Mat& distCoeffs, cv::Mat& colorImage, osg::Matrix* calculatedCamPose) {
	//std::vector<cv::Point3f> objp = generateCheckerboardPattern();

	cv::Mat rvec, tvec;
	if (m_markerType == MarkerType::CHARUCO) {
		std::vector<int> markerIds;
		std::vector<std::vector<cv::Point2f>> markerCorners;

		std::vector<int> charucoIds;
		std::vector<cv::Point2f> charucoCorners;

		cv::aruco::detectMarkers(colorImage, m_charucoDictionary, markerCorners, markerIds, m_charucoParams);
		if (markerIds.empty()) return false;

		if (cv::aruco::interpolateCornersCharuco(markerCorners, markerIds, colorImage, m_charucoBoard, charucoCorners, charucoIds) < m_charucoMinDetectedPoints) return false;

		if (!cv::aruco::estimatePoseCharucoBoard(charucoCorners, charucoIds, m_charucoBoard, cameraMatrix, distCoeffs, rvec, tvec, false)) return false;
	}
	else if (m_markerType == MarkerType::CHESSBOARD) {
		std::vector<cv::Point2f> corner_pts;

		cv::Mat gray;
		cv::cvtColor(colorImage, gray, cv::COLOR_RGB2GRAY);
		//cv::imshow("Last seen image", *colorImage);
		// Finding checker board corners
		// If desired number of corners are found in the image then success = true  

		if (!cv::findChessboardCorners(gray, cv::Size(m_checkerboardDimX, m_checkerboardDimY), corner_pts, cv::CALIB_CB_ADAPTIVE_THRESH | cv::CALIB_CB_NORMALIZE_IMAGE | cv::CALIB_CB_FILTER_QUADS | cv::CALIB_CB_FAST_CHECK)) return false;

		//If desired number of corner are detected, we refine the pixel coordinates and display them on the images of checker board
		cv::TermCriteria criteria(CV_TERMCRIT_EPS | CV_TERMCRIT_ITER, m_maxIterationForSubPixel, m_epsilonForSubPixel);

		// refining pixel coordinates for given 2d points.
		cv::cornerSubPix(gray, corner_pts, cv::Size(11, 11), cv::Size(-1, -1), criteria);

		// Displaying the detected corner points on the checker board
		//cv::drawChessboardCorners(*colorImage, cv::Size(CHECKERBOARD[0], CHECKERBOARD[1]), corner_pts, success);

		std::cout << cameraMatrix << std::endl;
		std::cout << distCoeffs << std::endl;

		//cv::waitKeyEx(0);
		//imgpoints.push_back(corner_pts);

		if (!cv::solvePnP(m_checkerboardPattern, corner_pts, cameraMatrix, distCoeffs, rvec, tvec)) return false;
	}
	//cv::drawFrameAxes(*colorImage, *cameraMatrix, *distCoeffs, rvec, tvec, 100);
	//std::cout << "Rotation vector : " << rvec << std::endl;
	//std::cout << "Translation vector : " << tvec << std::endl;

	// rvec is 3x1, tvec is 3x1
	cv::Mat R;
	cv::Rodrigues(rvec, R); // R is 3x3

	R = R.t();  // rotation of inverse
	tvec = -R * tvec; // translation of inverse

	double* _r = R.ptr<double>();
	osg::Matrix cameraPose(
		_r[0], _r[3], _r[6], 0.0,
		_r[1], _r[4], _r[7], 0.0,
		_r[2], _r[5], _r[8], 0.0,
		tvec.at<double>(0) * 0.001, tvec.at<double>(1) * 0.001, tvec.at<double>(2) * 0.001, 1.0
	);

	*calculatedCamPose = cameraPose; // write matrix to output parameter

	// Temporary parameters for debugging
	/*vector = osg::Vec3(0, 0, 0) * cameraPose;
	g_calculatedCamPose = cameraPose;*/
	return true;
}

void PointCloudManager::addRealSenseScanDevices() {
	rs2::context rs2ctx;
	unsigned int deviceCount = 0;
	unsigned int devicesAdded = 0;
	std::vector<cv::Point3f> checkerboardPattern = generateCheckerboardPattern();
	for (rs2::device&& device : rs2ctx.query_devices()) {
		PoseDriver* poseDriver;
		const char* serial = device.get_info(RS2_CAMERA_INFO_SERIAL_NUMBER);
		deviceCount++;

		cv::Mat cameraMatrix;
		cv::Mat distCoeffs;
		unsigned int width = 0u;
		unsigned int height = 0u;
		if (m_scanPipeline->hasDevice(serial) || !loadCameraCalibration(serial, &cameraMatrix, &distCoeffs, width, height)) continue;

		rs2::config solvePNPcfg;
		solvePNPcfg.enable_stream(RS2_STREAM_COLOR, -1, width, height, rs2_format::RS2_FORMAT_BGR8, m_realSenseFPS);
		solvePNPcfg.enable_device(serial);

		if (!m_trackedScanDeviceFound &&
			(!m_useSerialToFindTrackedScanDevice ||
				(!m_serialOfTrackedScanDevice.empty() && m_serialOfTrackedScanDevice == serial)))
		{
			// Code for creating the a RealSenseScanDevice connected to the vive tracker.
			osg::Matrix sensorToDriver;
			if (m_configReader->getBoolFromStartupConfig("scanning_use_3D_print")) {
				osg::Matrix rsToTrackerRot = osg::Matrix::rotate(osg::DegreesToRadians(180.0f), osg::Vec3(0, 0, 1));
				osg::Matrix rsToTrackerTrans = osg::Matrix::translate(osg::Vec3(0.0225, -0.025743, 0.0471));

				osg::Matrix trackerToClayMoreRot1 = osg::Matrix::rotate(osg::DegreesToRadians(90.0f), osg::Vec3(1, 0, 0));
				osg::Matrix trackerToClayMoreRot2 = osg::Matrix::rotate(osg::DegreesToRadians(180.0f), osg::Vec3(0, 1, 0));

				sensorToDriver = rsToTrackerRot * rsToTrackerTrans * trackerToClayMoreRot1 * trackerToClayMoreRot2;
			}
			else {
				osg::Matrix rot = osg::Matrix::rotate(osg::DegreesToRadians(90.0f), osg::Vec3(1, 0, 0));
				osg::Matrix rot2 = osg::Matrix::rotate(osg::DegreesToRadians(180.0f), osg::Vec3(0, 0, 1));
				osg::Matrix trans = osg::Matrix::translate(osg::Vec3(-0.0199999, -0.0070963, 0.00877001)); // Value for directly connected tracker and RealSense
				sensorToDriver = trans * rot2 * rot;
			}
			poseDriver = new ViveTrackerPoseDriver(m_timedCircularTrackerMatrixBuffer, m_timedCircularTrackerMatrixBuffer_Lock, sensorToDriver);
			std::cout << "New ScanDevice: RealSense " << serial << " | connected to ViveTracker" << std::endl;
			m_trackedScanDeviceFound = true;
		}
		else {
			rs2::pipeline rsPipe(rs2ctx);
			rsPipe.start(solvePNPcfg);

			// Camera warmup - dropping several first frames to let auto-exposure stabilize
			rs2::frameset frames;
			for (int i = 0; i < 1; i++)
			{
				//Wait for all configured streams to produce a frame
				frames = rsPipe.wait_for_frames();
			}

			rs2::video_frame colorFrame = frames.get_color_frame();

			cv::Mat realSenseColorImage(cv::Size(width, height), CV_8UC3, (void*)colorFrame.get_data(), cv::Mat::AUTO_STEP);

			rsPipe.stop();

			osg::Matrix calculatedCamPose;
			if (!calculateScanDevicePoseViaPattern(cameraMatrix, distCoeffs, realSenseColorImage, &calculatedCamPose)) continue;

			// Setup transformation matrices to transform between ClayMore and the Tracker coordinate systems
			osg::Matrix trackerToClayMoreRot1 = osg::Matrix::rotate(osg::DegreesToRadians(90.0f), osg::Vec3(1, 0, 0));
			osg::Matrix trackerToClayMoreRot2 = osg::Matrix::rotate(osg::DegreesToRadians(180.0f), osg::Vec3(0, 1, 0));

			//osg::Vec3 patternToTrackerOffset(-0.085, 0.08, 0.005); m_configReader->getVec3fFromStartupConfig("opencv_checkerboard_to_tracker_offset");
			osg::Vec3 patternToTrackerOffset(m_configReader->getVec3fFromStartupConfig("opencv_checkerboard_to_tracker_offset"));
			osg::Matrix patternToTrackerTrans = osg::Matrix::translate(patternToTrackerOffset);

			bool expected;
			while (!m_timedCircularTrackerMatrixBuffer_Lock->compare_exchange_weak(expected = false, true)); //Lock the trackerMatrixBuffer before accessing its data
			Matrix34 pose = m_timedCircularTrackerMatrixBuffer->at(m_timedCircularTrackerMatrixBuffer->size() - 1).matrix;
			m_timedCircularTrackerMatrixBuffer_Lock->store(false); // Unlock the trackerMatrixBuffer after obtaining the newest trackerPosition

			osg::Matrix trackerMatrix(
				pose.m[0][0], pose.m[1][0], pose.m[2][0], 0.0,
				pose.m[0][1], pose.m[1][1], pose.m[2][1], 0.0,
				pose.m[0][2], pose.m[1][2], pose.m[2][2], 0.0,
				pose.m[0][3], pose.m[1][3], pose.m[2][3], 1.0f
			);

			calculatedCamPose = calculatedCamPose * osg::Matrix::inverse(trackerToClayMoreRot1 * trackerToClayMoreRot2) * patternToTrackerTrans * trackerMatrix;

			poseDriver = new StaticPoseDriver(calculatedCamPose,
				osg::Matrix(
					-1, 0, 0, 0,
					0, 0, -1, 0,
					0, -1, 0, 0,
					0, 0, 0, 1)
			);
			std::cout << "New ScanDevice: RealSense " << serial << " | loaded XML configuration" << std::endl;
		}

		RealSenseScanDevice* scanDevice = new RealSenseScanDevice(device, rs2ctx, width, height, m_realSenseDepthWidth, m_realSenseDepthHeight, m_realSenseFPS, rs2_format::RS2_FORMAT_BGRA8, rs2_format::RS2_FORMAT_Z16, poseDriver, m_pointCloudWidth, m_pointCloudHeight, new cv::Mat(cameraMatrix), new cv::Mat(distCoeffs));
		m_scanPipeline->addDevice(scanDevice);
		devicesAdded++;
	}
	std::cout << "Total RealSense devices: " << deviceCount << " | RealSenseScanDevices added: " << devicesAdded << std::endl;
}

void PointCloudManager::addAzureKinectScanDevices() {
	//TODO: still WIP; cleanup might not be perfect currently
	unsigned int deviceCount = k4a_device_get_installed_count();
	unsigned int devicesAdded = 0;
	std::vector<cv::Point3f> checkerboardPattern = generateCheckerboardPattern();
	for (unsigned int deviceIndex = 0; deviceIndex < deviceCount; deviceIndex++) {
		k4a_device_t device = nullptr;
		if (k4a_result_t::K4A_RESULT_SUCCEEDED == k4a_device_open(deviceIndex, &device)) {
			PoseDriver* poseDriver = nullptr;
			size_t serial_num_size = 0;
			if (k4a_buffer_result_t::K4A_BUFFER_RESULT_TOO_SMALL == k4a_device_get_serialnum(device, 0, &serial_num_size)) {
				char* serial = new char[serial_num_size];
				if (k4a_buffer_result_t::K4A_BUFFER_RESULT_SUCCEEDED == k4a_device_get_serialnum(device, serial, &serial_num_size) && !m_scanPipeline->hasDevice(serial)) {
					cv::Mat cameraMatrix;
					cv::Mat distCoeffs;
					unsigned int width = 0u;
					unsigned int height = 0u;
					int k4aColorResolution = -1;
					bool skipDevice = !loadCameraCalibration(serial, &cameraMatrix, &distCoeffs, width, height) || (k4aColorResolution = k4aGetColorResolution(width, height)) <= 0;
					if (!skipDevice) {
						if (!m_trackedScanDeviceFound && (!m_useSerialToFindTrackedScanDevice || (!m_serialOfTrackedScanDevice.empty() && m_serialOfTrackedScanDevice == serial))) {
							// Code for creating the a AzureKinectScanDevice connected to the vive tracker.
							osg::Matrix sensorToDriver;
							osg::Matrix rot = osg::Matrix::rotate(osg::DegreesToRadians(90.0f), osg::Vec3(1, 0, 0));
							osg::Matrix rot2 = osg::Matrix::rotate(osg::DegreesToRadians(180.0f), osg::Vec3(0, 0, 1));
							osg::Matrix trans = osg::Matrix::translate(osg::Vec3(0.0, -0.065, 0.040)); // Value for directly connected tracker and Azure
							sensorToDriver = trans * rot2 * rot;
							poseDriver = new ViveTrackerPoseDriver(m_timedCircularTrackerMatrixBuffer, m_timedCircularTrackerMatrixBuffer_Lock, sensorToDriver);
							std::cout << "New ScanDevice: Azure Kinect " << serial << " | connected to ViveTracker" << std::endl;
							m_trackedScanDeviceFound = true;
						}
						else {
							k4a_device_configuration_t config = K4A_DEVICE_CONFIG_INIT_DISABLE_ALL;
							config.camera_fps = m_azureKinectFPS;
							config.color_format = K4A_IMAGE_FORMAT_COLOR_BGRA32;
							config.color_resolution = (k4a_color_resolution_t)k4aColorResolution;
							config.depth_mode = m_azureKinectDepthMode;

							if (k4a_result_t::K4A_RESULT_SUCCEEDED == k4a_device_start_cameras(device, &config)) {
								k4a_capture_t capture = nullptr;
								// Camera warmup - dropping several first frames to let auto-exposure stabilize
								for (int i = 0; i < 1; i++) {
									k4a_device_get_capture(device, &capture, 10000);
									k4a_capture_release(capture);
								}
								if (k4a_wait_result_t::K4A_WAIT_RESULT_SUCCEEDED == k4a_device_get_capture(device, &capture, 10000)) {
									k4a_image_t k4aColor = k4a_capture_get_color_image(capture);

									if (k4aColor != NULL) {
										cv::Mat azureKinectColorImage(k4a_image_get_height_pixels(k4aColor), k4a_image_get_width_pixels(k4aColor), CV_8UC4, k4a_image_get_buffer(k4aColor), cv::Mat::AUTO_STEP);

										k4a_device_stop_cameras(device);

										osg::Matrix calculatedCamPose;
										if (calculateScanDevicePoseViaPattern(cameraMatrix, distCoeffs, azureKinectColorImage, &calculatedCamPose)) {

											// Setup transformation matrices to transform between ClayMore and the Tracker coordinate systems
											osg::Matrix trackerToClayMoreRot1 = osg::Matrix::rotate(osg::DegreesToRadians(90.0f), osg::Vec3(1, 0, 0));
											osg::Matrix trackerToClayMoreRot2 = osg::Matrix::rotate(osg::DegreesToRadians(180.0f), osg::Vec3(0, 1, 0));

											//osg::Vec3 patternToTrackerOffset(-0.085, 0.08, 0.005); m_configReader->getVec3fFromStartupConfig("opencv_checkerboard_to_tracker_offset");
											osg::Vec3 patternToTrackerOffset(m_configReader->getVec3fFromStartupConfig("opencv_checkerboard_to_tracker_offset"));
											osg::Matrix patternToTrackerTrans = osg::Matrix::translate(patternToTrackerOffset);

											bool expected;
											while (!m_timedCircularTrackerMatrixBuffer_Lock->compare_exchange_weak(expected = false, true)); //Lock the trackerMatrixBuffer before accessing its data
											// TODO 2: Find the proper tracker position related to the timestamp of the image capture
											Matrix34 pose = m_timedCircularTrackerMatrixBuffer->at(m_timedCircularTrackerMatrixBuffer->size() - 1).matrix;
											m_timedCircularTrackerMatrixBuffer_Lock->store(false); // Unlock the trackerMatrixBuffer after obtaining the newest trackerPosition

											osg::Matrix trackerMatrix(
												pose.m[0][0], pose.m[1][0], pose.m[2][0], 0.0,
												pose.m[0][1], pose.m[1][1], pose.m[2][1], 0.0,
												pose.m[0][2], pose.m[1][2], pose.m[2][2], 0.0,
												pose.m[0][3], pose.m[1][3], pose.m[2][3], 1.0f
											);

											calculatedCamPose = calculatedCamPose * osg::Matrix::inverse(trackerToClayMoreRot1 * trackerToClayMoreRot2) * patternToTrackerTrans * trackerMatrix;

											poseDriver = new StaticPoseDriver(calculatedCamPose,
												osg::Matrix(
													-1, 0, 0, 0,
													0, 0, -1, 0,
													0, -1, 0, 0,
													0, 0, 0, 1)
											);
											std::cout << "New ScanDevice: Azure Kinect " << serial << " | loaded XML configuration" << std::endl;
										}
										else skipDevice = true;
									}
									k4a_image_release(k4aColor);
								}
								k4a_capture_release(capture);
							}
							else {
								k4a_device_stop_cameras(device);
								k4a_device_close(device);
							}
						}
					}
					//skipDevice might have changed, so it needs to be checked again.
					if (!skipDevice) {
						k4a_device_configuration_t config = K4A_DEVICE_CONFIG_INIT_DISABLE_ALL;
						config.camera_fps = m_azureKinectFPS;
						config.color_format = K4A_IMAGE_FORMAT_COLOR_BGRA32;
						config.color_resolution = (k4a_color_resolution_t)k4aColorResolution;
						config.depth_mode = m_azureKinectDepthMode;
						config.synchronized_images_only = true;

						AzureKinectScanDevice* scanDevice = new AzureKinectScanDevice(device, config, poseDriver, m_pointCloudWidth, m_pointCloudHeight, new cv::Mat(cameraMatrix), new cv::Mat(distCoeffs));
						m_scanPipeline->addDevice(scanDevice);
						devicesAdded++;
					}
				}
				delete[] serial;
			}
		}
		k4a_device_close(device);
	}
	std::cout << "Total Azure Kinect devices: " << deviceCount << " | AzureKinectScanDevices added: " << devicesAdded << std::endl;
}

void PointCloudManager::addRealSenseViveTrackerScanDevice() {
	if (!m_useSerialToFindTrackedScanDevice || m_trackedScanDeviceFound || m_serialOfTrackedScanDevice.empty()) return;

	rs2::context rs2ctx;
	for (rs2::device&& device : rs2ctx.query_devices()) {
		PoseDriver* poseDriver;
		const char* serial = device.get_info(RS2_CAMERA_INFO_SERIAL_NUMBER);

		cv::Mat cameraMatrix;
		cv::Mat distCoeffs;
		unsigned int width = 0u;
		unsigned int height = 0u;
		int k4aColorResolution = -1;
		if (m_scanPipeline->hasDevice(serial) || !loadCameraCalibration(serial, &cameraMatrix, &distCoeffs, width, height)) continue;

		if (m_serialOfTrackedScanDevice == serial) {
			// Code for creating the RealSenseScanDevice connected to the vive tracker.
			osg::Matrix sensorToDriver;
			if (m_configReader->getBoolFromStartupConfig("scanning_use_3D_print")) {
				osg::Matrix rsToTrackerRot = osg::Matrix::rotate(osg::DegreesToRadians(180.0f), osg::Vec3(0, 0, 1));
				osg::Matrix rsToTrackerTrans = osg::Matrix::translate(osg::Vec3(0.0225, -0.025743, 0.0471));

				osg::Matrix trackerToClayMoreRot1 = osg::Matrix::rotate(osg::DegreesToRadians(90.0f), osg::Vec3(1, 0, 0));
				osg::Matrix trackerToClayMoreRot2 = osg::Matrix::rotate(osg::DegreesToRadians(180.0f), osg::Vec3(0, 1, 0));

				sensorToDriver = rsToTrackerRot * rsToTrackerTrans * trackerToClayMoreRot1 * trackerToClayMoreRot2;
			}
			else {
				osg::Matrix rot = osg::Matrix::rotate(osg::DegreesToRadians(90.0f), osg::Vec3(1, 0, 0));
				osg::Matrix rot2 = osg::Matrix::rotate(osg::DegreesToRadians(180.0f), osg::Vec3(0, 0, 1));
				osg::Matrix trans = osg::Matrix::translate(osg::Vec3(-0.0199999, -0.0070963, 0.00877001)); // Value for directly connected tracker and RealSense
				sensorToDriver = trans * rot2 * rot;
			}
			poseDriver = new ViveTrackerPoseDriver(m_timedCircularTrackerMatrixBuffer, m_timedCircularTrackerMatrixBuffer_Lock, sensorToDriver);
			std::cout << "New ScanDevice: RealSense " << serial << " | connected to ViveTracker" << std::endl;

			m_trackedScanDeviceFound = true;
			RealSenseScanDevice* scanDevice = new RealSenseScanDevice(device, rs2ctx, width, height, m_realSenseDepthWidth, m_realSenseDepthHeight, m_realSenseFPS, rs2_format::RS2_FORMAT_BGRA8, rs2_format::RS2_FORMAT_Z16, poseDriver, width, height, new cv::Mat(cameraMatrix), new cv::Mat(distCoeffs));
			m_scanPipeline->addDevice(scanDevice);
			break;
		}
	}
}

void PointCloudManager::addAzureKinectViveTrackerScanDevice() {
	if (!m_useSerialToFindTrackedScanDevice || m_trackedScanDeviceFound || m_serialOfTrackedScanDevice.empty()) return;

	unsigned int deviceCount = k4a_device_get_installed_count();
	for (unsigned int deviceIndex = 0; deviceIndex < deviceCount; deviceIndex++) {
		k4a_device_t device = nullptr;
		if (k4a_result_t::K4A_RESULT_SUCCEEDED == k4a_device_open(deviceIndex, &device)) {
			PoseDriver* poseDriver;

			size_t serial_num_size = 0;
			if (k4a_buffer_result_t::K4A_BUFFER_RESULT_TOO_SMALL == k4a_device_get_serialnum(device, 0, &serial_num_size)) {
				char* serial = new char[serial_num_size];
				if (k4a_buffer_result_t::K4A_BUFFER_RESULT_SUCCEEDED == k4a_device_get_serialnum(device, serial, &serial_num_size)) {
					cv::Mat cameraMatrix;
					cv::Mat distCoeffs;
					unsigned int width = 0u;
					unsigned int height = 0u;
					int k4aColorResolution = -1;
					if (loadCameraCalibration(serial, &cameraMatrix, &distCoeffs, width, height) && (k4aColorResolution = k4aGetColorResolution(width, height)) > 0 && m_serialOfTrackedScanDevice == serial) {
						osg::Matrix sensorToDriver;

						osg::Matrix rot = osg::Matrix::rotate(osg::DegreesToRadians(90.0f), osg::Vec3(1, 0, 0));
						osg::Matrix rot2 = osg::Matrix::rotate(osg::DegreesToRadians(180.0f), osg::Vec3(0, 0, 1));
						osg::Matrix trans = osg::Matrix::translate(osg::Vec3(0.0, -0.065, 0.040)); // Value for directly connected tracker and Azure
						//osg::Matrix trans = osg::Matrix::translate(osg::Vec3(-0.0199999, -0.0070963, 0.00877001)); // Value for directly connected tracker and Azure
						sensorToDriver = trans * rot2 * rot;

						poseDriver = new ViveTrackerPoseDriver(m_timedCircularTrackerMatrixBuffer, m_timedCircularTrackerMatrixBuffer_Lock, sensorToDriver);
						std::cout << "New ScanDevice: Azure Kinect " << serial << " | connected to ViveTracker" << std::endl;
						//poseDriver = new ViveTrackerLivePoseDriver(m_timedCircularTrackerMatrixBuffer, m_timedCircularTrackerMatrixBuffer_Lock, sensorToDriver);
						m_trackedScanDeviceFound = true;
					}

					k4a_device_configuration_t config = K4A_DEVICE_CONFIG_INIT_DISABLE_ALL;
					config.camera_fps = m_azureKinectFPS;
					config.color_format = K4A_IMAGE_FORMAT_COLOR_BGRA32;
					config.color_resolution = (k4a_color_resolution_t)k4aColorResolution;
					config.depth_mode = m_azureKinectDepthMode;
					config.synchronized_images_only = true;

					AzureKinectScanDevice* scanDevice = new AzureKinectScanDevice(device, config, poseDriver, m_pointCloudWidth, m_pointCloudHeight, new cv::Mat(cameraMatrix), new cv::Mat(distCoeffs));
					m_scanPipeline->addDevice(scanDevice);
				}
				delete[] serial;
			}
		}
	}
}

//Kester's Telepresence
void PointCloudManager::generate_point_cloud(k4a::image point_cloud)
{
	//Die Punktwolke wird ja recht komisch dargestellt, wenn du T drückst.
	// Das liegt daran, dass in point_cloud etwas anderes liegt, als du vermutest.
	// Was drin ist kannst du mit folgenden Funktionen sehen:
	//std::cout << "point_cloud.get_format(): " << point_cloud.get_format() << std::endl;
	//std::cout << "point_cloud.get_size(): " << point_cloud.get_size() << std::endl;
	//std::cout << "point_cloud.get_stride_bytes(): " << point_cloud.get_stride_bytes() << std::endl;

	//Offentsichlich hat jedes Pixel 6Byte für x, y und z. Das wären 2 Byte pro Koordinate
	// Bei point_cloud.get_format() steht "8", das ist laut k4atypes.h (aus dem KinectSDK) der Typ: 
	// K4A_IMAGE_FORMAT_CUSTOM (steht im Enum an 7er (mit 0 gezählt an 8er) Stelle)
	// Irgendwo im Code des Kinect SDKs habe ich gesehen, dass es 16bit unsigned int Werte für dieses point_cloud Image
	// abgespeichert wird. Das heißt, deine Konvertierung mit dem (float) unten ist nicht korrekt und 
	// erklärt die kaputte Punktwolke (und mein Versuch mit "*0.01" oben es zu retten). Das heißt,
	// du musst hier die Werte in u_int16 casten. Versuchs mal mit:
	// uint16_t depth = (uint16_t)(depth_buffer[i + 1] << 8 | depth_buffer[i]);
	// Die Zeile ist aus makeAndSaveAFewDepthImagesWithKinect().
	// Um die nun ins OpenGL Fenster zu bekommen, musst du die nun noch mit einem Faktor skalieren und in float casten,
	// damit sie nicht riesig sind. So wie ich das KinectSDK einschätze, werden die integer in Millimeter skaliert sein.
	// In ClayMore ist alles in Meter skaliert. Wenn du Faktor 1000 nimmst, bzw. 1/1000 sollte das passen

	uint8_t* points = point_cloud.get_buffer();
	size_t depth_buffer_size = point_cloud.get_size();



	int width = point_cloud.get_width_pixels();
	int height = point_cloud.get_height_pixels();

	float x;
	float y;
	float z;



	m_pointcloud = new osg::Vec3Array;

	m_pointcloud->clear();

	for (size_t currVal = 0; currVal < depth_buffer_size; currVal += 6)
	{

		//cast 2 1-Byte values to a single 2-Byte value
		int16_t x16 = (int16_t)(points[currVal + 1] << 8 | points[currVal]);
		int16_t y16 = (int16_t)(points[currVal + 3] << 8 | points[currVal + 2]);
		int16_t z16 = (int16_t)(points[currVal + 5] << 8 | points[currVal + 4]);

		//scale millimeter to meter
		x = x16 * 0.001;
		y = y16 * (-0.001); //flip Y-axis, Microsoft makes Y->down
		z = z16 * 0.001;

		if (z == 0 || z > m_backgroundClippingDistance)
		{

			m_nullVector.push_back(true);

		}
		else
		{

			m_nullVector.push_back(false);

		}

		m_pointcloud->push_back(osg::Vec3(x, y, z));

	}

}



// ### START - Old Scan Pipeline ###
//Function is running on an additional thread, so it does not slow down the rendering of processed data.
//void PointCloudManager::processPointCloud()
//{
//	//osg::Matrix vorherMatrix = transformationMatrixForPointsIn->getMatrix();
//	if (m_useMultipleRealSenseDevices)
//	{
//		std::cout << "Error: Function for single device is called, but the configuration file contains information to use multiple devices (device: RealSense, Kinect or any other Depth camera)" << std::endl;
//	}
//	else {
//		//if (m_processing.load()) return; //Prevents writing additional data to the buffer while data is being processed. However we handle this outside the method, so it just takes up time
//		m_processing.store(true); //acts as a lock -> atomic bool is free of race condition
//
//		rs2::frameset frames = m_rsPipe->wait_for_frames();
//
//		//rs2::frameset frames;
//		//std::cout << "poll_for_frames: " << m_rsPipe->poll_for_frames(&frames) << std::endl;
//
//
//		/*std::cout << "Davor: " << vorherMatrix << std::endl;
//		m_pointCloudVisualizer->pushMatrix(vorherMatrix);
//		std::cout << "Danach: " << transformationMatrixForPointsIn->getMatrix() << std::endl;
//		std::cout << "Danach getTransformationMatrixFromBuffer: " << m_pointCloudVisualizer->getTransformationMatrixFromBuffer() << std::endl;*/
//
//		rs2::depth_frame depthFrame = frames.get_depth_frame();
//
//		//Apply predefined filters. Maybe create a selection menu to toggle those?
//		//depth = m_rs_dec_filter->process(depth); //Must be applied before a transformation to disparity is done
//		//depth = m_rs_depth_to_disparity->process(depth);
//		//depth = m_rs_spat_filter->process(depth);
//		//depth = m_rs_temp_filter->process(depth);	
//		//depth = m_rs_disparity_to_depth->process(depth);
//
//		// Generate the pointcloud and texture mappings
//		// Notice: Very important is that the colorFrame is generated and mapped to the PoincCloud before calculating the depth!!!
//
//
//		rs2::video_frame colorFrame = frames.get_color_frame();
//
//
//		m_rsPc->map_to(colorFrame);
//		rs2::points rsPoints = m_rsPc->calculate(depthFrame);
//
//		unsigned long long depthFrameTimeStamp = 0;
//		if (depthFrame.supports_frame_metadata(rs2_frame_metadata_value::RS2_FRAME_METADATA_SENSOR_TIMESTAMP)) {
//			depthFrameTimeStamp = deviceToSystemClock(depthFrame.get_frame_metadata(rs2_frame_metadata_value::RS2_FRAME_METADATA_SENSOR_TIMESTAMP)); // in usec from device clock; thus conversion to system clock is needed
//			//std::cout << "Current System Time: " << std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::system_clock::now().time_since_epoch()).count() << std::endl;
//			//std::cout << "Sensor Frame in System Time: " << depthFrameTimeStamp << std::endl;
//		} else std::cout << "RS2_FRAME_METADATA_SENSOR_TIMESTAMP is not supported!!!" << std::endl;
//
//		//Funktion für timeStamp abgleich -> return value wäre die passende Matrix zum timestamp
//
//		//depthFrameTimeStamp = std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::system_clock::now().time_since_epoch()).count();
//		osg::Matrix corespondingMatrix = findCorrespondingTrackerMatrixToTimestamp(depthFrameTimeStamp);
//		//std::cout << "Von PCManager nach findCorrespondingTrackerMatrixToTimestamp(): depthFrameTimeStamp: " << depthFrameTimeStamp << std::endl;
//
//		//##### Bingo Starts!!
//		// This is the solution for the synchronisation of System Clock and RealSense Clock. These request should last less than 1ms.
//		// Further details: https://github.com/IntelRealSense/librealsense/issues/2922#issuecomment-448536049
//		/*
//		auto dev = m_rsPipe->get_active_profile().get_device().as<rs2::debug_protocol>();
//		uint8_t data[]{
//			0x14, 00, 0xab, 0xcd, 01, 00, 00, 00, 0x3c, 0x61, 01, 00, 0x40, 0x61, 01, 00, 00, 00, 00, 00, 00, 00, 00, 00
//		};
//		std::vector<uint8_t> command(data, data + sizeof(data));
//		auto res = dev.send_and_receive_raw_data(command);
//		auto ptr = reinterpret_cast<uint32_t*>(res.data());
//		auto ts = ptr[1] * 0.001;
//		std::cout << "HW TIME:" << ts << std::endl;*/
//		//Bingo Ends
//
//		//TODO 1 Performance: Pass data by call by ref and not by value
//		m_pointCloudVisualizer->processRSPointCloud(rsPoints, corespondingMatrix, colorFrame, depthFrame);
//
//		m_processing.store(false);
//	}
//}
// ### END - Old Scan Pipeline ###

void PointCloudManager::runICP() {

	// code in parts from Zhaoyang-Wang -> https://github.com/wz18207/azure_kinect_pcl_grabber -> MIT License -> azure_kinect_pcl_grabber/src/k4a_grabber.cpp
	k4a::device azureKinectSensor0;
	k4a::device azureKinectSensor1;
	k4a::capture* kinectCapture0 = new k4a::capture();
	k4a::capture* kinectCapture1 = new k4a::capture();

	try
	{
		uint32_t sensor0 = 0;
		uint32_t sensor1 = 1;
		azureKinectSensor0 = k4a::device::open(sensor0);
		azureKinectSensor1 = k4a::device::open(sensor1);

	}
	catch (const std::exception&)
	{
		std::cerr << "Device cannot be opened." << std::endl;
	}

	k4a_device_configuration_t config;
	config = K4A_DEVICE_CONFIG_INIT_DISABLE_ALL;
	config.color_format = K4A_IMAGE_FORMAT_COLOR_BGRA32;
	config.color_resolution = (k4a_color_resolution_t)m_configReader->getIntFromStartupConfig("k4a_color_resolution_t");
	config.depth_mode = (k4a_depth_mode_t)m_configReader->getIntFromStartupConfig("k4a_depth_mode_t");
	config.camera_fps = (k4a_fps_t)m_configReader->getIntFromStartupConfig("k4a_fps_t");
	config.synchronized_images_only = true; // ensures that depth and color images are both available in the capture

	std::cout << "k4a::device::get_installed_count: " << k4a::device::get_installed_count() << std::endl;
	azureKinectSensor0.start_cameras(&config);
	azureKinectSensor1.start_cameras(&config);

	k4a::calibration calibration0 = azureKinectSensor0.get_calibration(config.depth_mode, config.color_resolution);
	k4a::calibration calibration1 = azureKinectSensor1.get_calibration(config.depth_mode, config.color_resolution);
	k4a::transformation transformation0 = k4a::transformation(calibration0);
	k4a::transformation transformation1 = k4a::transformation(calibration1);

	azureKinectSensor0.get_capture(kinectCapture0);
	azureKinectSensor1.get_capture(kinectCapture1);

	k4a::image depthMap0 = kinectCapture0->get_depth_image();
	k4a::image depthMap1 = kinectCapture1->get_depth_image();
	int width = depthMap0.get_width_pixels();
	int height = depthMap0.get_height_pixels();

	k4a::image colorImage0 = kinectCapture0->get_color_image();
	k4a::image colorImage1 = kinectCapture1->get_color_image();
	int color_image_width_pixels = colorImage0.get_width_pixels();
	int color_image_height_pixels = colorImage0.get_height_pixels();

	// create poincloud 0
	pcl::PointCloud<pcl::PointXYZ>::Ptr cloud0(new pcl::PointCloud<pcl::PointXYZ>());
	cloud0->width = width;
	cloud0->height = height;
	cloud0->is_dense = false;
	cloud0->points.resize(cloud0->height * cloud0->width);

	k4a::image transformed_depth_image0 = NULL;
	transformed_depth_image0 = k4a::image::create(K4A_IMAGE_FORMAT_DEPTH16,
		color_image_width_pixels,
		color_image_height_pixels,
		color_image_width_pixels * (int)sizeof(uint16_t));
	k4a::image point_cloud_image0 = NULL;
	point_cloud_image0 = k4a::image::create(K4A_IMAGE_FORMAT_CUSTOM,
		color_image_width_pixels,
		color_image_height_pixels,
		color_image_width_pixels * 3 * (int)sizeof(int16_t));
	transformation0.depth_image_to_color_camera(depthMap0, &transformed_depth_image0);
	transformation0.depth_image_to_point_cloud(transformed_depth_image0, K4A_CALIBRATION_TYPE_COLOR, &point_cloud_image0);

	int16_t* point_cloud_image_data0 = (int16_t*)(void*)point_cloud_image0.get_buffer();
	for (int i = 0; i < width * height; ++i) {
		pcl::PointXYZ point;
		point.x = point_cloud_image_data0[3 * i + 0] / 1000.0f;
		point.y = point_cloud_image_data0[3 * i + 1] / 1000.0f;
		point.z = point_cloud_image_data0[3 * i + 2] / 1000.0f;
		if (point.z == 0) {
			continue;
		}
		cloud0->points[i] = point;
	}

	// create poincloud 1
	pcl::PointCloud<pcl::PointXYZ>::Ptr cloud1(new pcl::PointCloud<pcl::PointXYZ>());
	cloud1->width = width;
	cloud1->height = height;
	cloud1->is_dense = false;
	cloud1->points.resize(cloud1->height * cloud1->width);

	k4a::image transformed_depth_image1 = NULL;
	transformed_depth_image1 = k4a::image::create(K4A_IMAGE_FORMAT_DEPTH16,
		color_image_width_pixels,
		color_image_height_pixels,
		color_image_width_pixels * (int)sizeof(uint16_t));
	k4a::image point_cloud_image1 = NULL;
	point_cloud_image1 = k4a::image::create(K4A_IMAGE_FORMAT_CUSTOM,
		color_image_width_pixels,
		color_image_height_pixels,
		color_image_width_pixels * 3 * (int)sizeof(int16_t));
	transformation1.depth_image_to_color_camera(depthMap1, &transformed_depth_image1);
	transformation1.depth_image_to_point_cloud(transformed_depth_image1, K4A_CALIBRATION_TYPE_COLOR, &point_cloud_image1);

	int16_t* point_cloud_image_data1 = (int16_t*)(void*)point_cloud_image1.get_buffer();
	for (int j = 0; j < width * height; ++j) {
		pcl::PointXYZ point;
		point.x = point_cloud_image_data1[3 * j + 0] / 1000.0f;
		point.y = point_cloud_image_data1[3 * j + 1] / 1000.0f;
		point.z = point_cloud_image_data1[3 * j + 2] / 1000.0f;
		if (point.z == 0) {
			continue;
		}
		cloud1->points[j] = point;

	}

	//int pointsToSamplePercentage = 20;
	//pcl::RandomSample<pcl::PointXYZ> random0;
	//random0.setInputCloud(cloud0);
	//random0.setSeed(std::rand());
	//random0.setSample((unsigned int)(cloud0->size()* (pointsToSamplePercentage / 100)));
	//random0.filter(*cloud0);

	//pcl::RandomSample<pcl::PointXYZ> random1;
	//random1.setInputCloud(cloud1);
	//random1.setSeed(std::rand());
	//random1.setSample((unsigned int)(cloud1->size() * (pointsToSamplePercentage / 100)));
	//random1.filter(*cloud1);

	pcl::IterativeClosestPoint<pcl::PointXYZ, pcl::PointXYZ> icp;
	int maxNumIters = 50;
	pcl::PointCloud<pcl::PointXYZ>::Ptr finalCloud(new pcl::PointCloud<pcl::PointXYZ>());
	for (int k = 0; k < maxNumIters; k++)
	{
		if (k == 0)
		{
			icp.setInputSource(cloud0);
			icp.setInputTarget(cloud1);
		}
		else
		{
			icp.setInputSource(finalCloud);
			icp.setInputTarget(cloud1);
		}
		icp.setMaximumIterations(1);
		//icp.setTransformationEpsilon(1e-1);
		//icp.setMaxCorrespondenceDistance(0.5);
		//icp.setEuclideanFitnessEpsilon(0.00001);
		//icp.setRANSACOutlierRejectionThreshold(1.5);
		std::cout << "Before align " << std::endl;

		icp.align(*finalCloud);


		std::cout << "has converged:" << icp.hasConverged() << " score: " <<
			icp.getFitnessScore() << std::endl;
		std::cout << icp.getFinalTransformation() << std::endl;
	}




	std::cout << "has converged:" << icp.hasConverged() << " score: " <<
		icp.getFitnessScore() << std::endl;
	std::cout << icp.getFinalTransformation() << std::endl;
	std::cout << "end" << std::endl;


}


//Function is running on an additional thread, so it does not slow down the rendering of processed data.
void PointCloudManager::processPointCloud() {
	//if (m_processing.load()) return; //Prevents writing additional data to the buffer while data is being processed. However we handle this outside the method, so it just takes up time
	//m_isProcessing.store(true); //acts as a lock -> atomic bool is free of race condition
	if (m_useAzureTelepresence)
	{
		m_isTelepresenceLoopActive.store(true);

		//get a new capture and use it to get the depth resolution
		m_azureKinectSensor.get_capture(m_kinectCapture);
		//TimeWriter tw;

		//m_geometryVisualization = new GeometryVisualization(m_configReader);
		//m_pointCloudGroup->addChild(m_geometryVisualization);
		//m_geometryVisualization->selectTechnique(FLAT_AND_WIRE_SHADING);
		//setup loop
		for (size_t currLiveMeshIndex = 0; currLiveMeshIndex < m_maxNumThreads; currLiveMeshIndex++)
		{
			m_liveMeshPool.push_back(new LiveMesh(currLiveMeshIndex));
			//m_liveMeshPool.at(currLiveMeshIndex)->initialize(&m_azureKinectSensor, m_geometryVisualization, &m_config, m_configReader);
			m_liveMeshPool.at(currLiveMeshIndex)->initialize(&m_azureKinectSensor, m_pointCloudGroup, &m_config, m_configReader);
			m_liveMeshPool.at(currLiveMeshIndex)->calcImageSize(m_kinectCapture);

			m_threadPool.push_back(new std::thread(&LiveMesh::startRendering, m_liveMeshPool.at(currLiveMeshIndex)));

			m_threadPool.at(currLiveMeshIndex)->detach();
		}

		std::thread* meshManager = new std::thread(&PointCloudManager::manageMeshes, this);
		meshManager->detach();
		std::thread* checkLockSceneGraph = new std::thread(&PointCloudManager::checkLockSceneGraph, this);
		checkLockSceneGraph->detach();

		int currLiveMeshIndex = 0;

		//loop until joinTelepresenceThread() was called
		while (m_isTelepresenceLoopActive.load())
		{
			if (m_azureKinectSensor.get_capture(m_kinectCapture))
			{
				for (int i = 0; i < m_maxNumThreads; i++)
				{
					if (m_liveMeshPool.at(i)->getCurrentState() == LiveMesh::RENDERED)
					{
						m_liveMeshPool.at(i)->setKinectCapture(m_kinectCapture);
					}
				}
			}
			else
			{
				std::cout << "ERROR: get_capture from Kinect TIMEOUT" << std::endl;
			}
		}
		return;
		///////////////////////////////// RETURN /////////////////////////////////
		// Hier wird nichts mehr erreicht darunter:






		//std::cout << "Start capturing pointcloud." << std::endl;
		if (m_cloth != nullptr)
		{

			m_telePresenceGeode->removeChild(m_cloth);

		}

		k4a::image colorMap;
		k4a::image depthMap;

		int height;
		int width;
		int numVerts;

		k4a::image pointcloudImage;

		k4a::calibration calibration;
		k4a::transformation transformation;



		//create empty mesh
		m_cloth = new osg::OpenMeshGeometry(GeometryType::MODELLING, m_configReader, "kester-cloth");


		//Es muss immer erst über den OSG-Weg jeweils ein VertexArray bzw. eine IndexListe erstellt werden, damit dort eine Speicheradresse 
		//abgelegt ist. Mit dieser Speicheradresse kann OpenMesh dann arbeiten. Nicht ganz trivial.
		m_cloth->setVertexArray(new osg::Vec3Array());
		m_cloth->addPrimitiveSet(new osg::DrawElementsUInt(osg::PrimitiveSet::TRIANGLES, 0));

		//Hab jetzt noch eine "osg::Geode" vor cloth gehangen, damit man hier später noch Material und ähnliche Sachen einstellen kann
		m_telePresenceGeode = new osg::Geode();
		//Das sollte eigentlich das hell/dunkel flackern aller Flächen unterbinden, tut es aber nicht... da stimmt noch irgendwas nicht
		//aber gerade nicht so wichtig.
		m_telePresenceGeode->getOrCreateStateSet()->setMode(GL_RESCALE_NORMAL, osg::StateAttribute::ON);
		m_telePresenceGeode->addChild(m_cloth);
		m_telePresenceGeode->dirtyBound();
		m_pointCloudGroup->addChild(m_telePresenceGeode);



		m_backgroundClippingDistance = 2.0f;

		m_nullVector = std::vector<bool>();

		//get calibration data
		calibration = m_azureKinectSensor.get_calibration(m_config.depth_mode, m_config.color_resolution);

		//create transform
		transformation = k4a::transformation(calibration);



		//######################
		//get kinect vertex data
		//######################

		//get kinect capture
		m_azureKinectSensor.get_capture(m_kinectCapture);

		//get color and depth images
		colorMap = m_kinectCapture->get_color_image();
		depthMap = m_kinectCapture->get_depth_image();



		//get image size
		height = depthMap.get_height_pixels();
		width = depthMap.get_width_pixels();



		//std::cout << "captured image size: " << width << " x " << height << "." << std::endl;


		//transform depth image to pointcloud
		pointcloudImage = transformation.depth_image_to_point_cloud(depthMap, K4A_CALIBRATION_TYPE_DEPTH);

		//convert pointcloud image to Vector array
		generate_point_cloud(pointcloudImage);

		numVerts = m_pointcloud->size();

		//std::cout << "numVerts: " << numVerts << std::endl;

		//std::cout << "Generated pointcloud in vertex array." << std::endl;





		//#######################
		//write vertices to cloth
		//#######################

		auto currMesh = m_cloth->getMesh();

		std::vector<OpenMesh::VertexHandle> partsOfTriangle;


		//cloth->setVertexArray(m_pointcloud);

		OpenMesh::VertexHandle vh;

		for (int i = 0; i < numVerts; i++)
		{

			vh = currMesh->add_vertex(m_pointcloud->at(i));

			partsOfTriangle.push_back(vh);

		}


		//std::cout << "vertices added to mesh." << std::endl;





		//#####################
		//create faces in cloth
		//#####################

		//std::cout << "partsOfTriangle.size() : " << partsOfTriangle.size() << std::endl;


		for (int i = 0; i < numVerts - width; i++)
		{

			if ((i + 1) % width != 0 && !m_nullVector[i + 1] && !m_nullVector[i + width])
			{

				if (!m_nullVector[i])
				{

					//top left triangle
					currMesh->add_face(partsOfTriangle.at(i), partsOfTriangle.at(i + width), partsOfTriangle.at(i + 1));

				}

				if (!m_nullVector[i + width + 1])
				{

					//bottom right triangle
					currMesh->add_face(partsOfTriangle.at(i + 1), partsOfTriangle.at(i + width), partsOfTriangle.at(i + width + 1));

				}

			}

		}



		//std::cout << "tris added " << std::endl;




		//Hiermit gehe ich über alle faces und lese jeweils die vertex ID der anliegenden verticies aus und schreibe damit ein neues IndexArray
		//Das war ein altes bekanntes Problem zwischen der Anbindung von OSG und OpenMesh. Das kostet in unserem Fall viel Zeit.
		//Dafür gibt es sicherlich eine bessere Lösung. Ich weiß nicht mehr genau, wieso man das nochmal zuweisen muss,
		//aber das ist nicht die schönste Lösung.
		std::vector<OpenMesh::VertexHandle> vhandles;
		unsigned int idx;
		for (int i = 0, nF = m_cloth->getMesh()->n_faces(); i < nF; ++i)
		{
			vhandles.clear();
			for (auto fv_it = m_cloth->getMesh()->cfv_iter(OpenMesh::FaceHandle(int(i))); fv_it.is_valid(); ++fv_it)
			{
				vhandles.push_back(*fv_it);
			}

			std::vector<int> indiciesCleanedUp;
			for (int j = 0; j < vhandles.size(); ++j)
			{
				idx = vhandles[j].idx(); //+ 1;
				indiciesCleanedUp.push_back(idx);
			}
			//m_utilProcessor->sync_indices((int)i, indiciesCleanedUp[0], indiciesCleanedUp[1], indiciesCleanedUp[2]);
			OpenMesh::FPropHandleT<GLuint> indicesPropH;
			m_cloth->getMesh()->get_property_handle(indicesPropH, "indices");
			OpenMesh::PropertyT<GLuint> indices = m_cloth->getMesh()->property(indicesPropH);

			indices[3 * i] = indiciesCleanedUp[0];
			indices[3 * i + 1] = indiciesCleanedUp[1];
			indices[3 * i + 2] = indiciesCleanedUp[2];
		}



		//Normalen updaten
		m_cloth->getMesh()->update_normals();
		//"dirty" ist ein flag, damit OpenGL weiß, dass sich etwas geändert hat. Sonst werden Änderungen nicht übernommen. Der Grund ist performance
		m_cloth->getNormalArray()->dirty();
		m_cloth->getVertexArray()->dirty();
		m_cloth->getPrimitiveSet(0)->dirty();







		m_cloth->getMesh();


		std::cout << "created mesh" << std::endl;


	}
	else if (!m_scanInParallel || m_devicesPerScan == 1) {
		for (int i = 0; i < m_devicesPerScan; i++) {
			ScanDevice* device = m_scanPipeline->getSelectedDevice();
			ColorData* colorData = nullptr;
			DepthData* depthData = nullptr;

			auto time = std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::system_clock::now().time_since_epoch()).count();
			//std::cout << "ProcessPointCloud Before Scan: " << time << std::endl;

			auto scanResult = m_scanPipeline->scan(&colorData, &depthData, TIMEOUT_MS);
			if (ScanPipeline::ScanResult::SUCCESS != scanResult) {
				std::cout << "### Scan Error at scan #" << i << ": " << ScanPipeline::scanResultToString(scanResult) << " ###" << std::endl;
				m_scanPipeline->selectNextDevice();
				continue; // TODO: Error Handling
			}

			// Texture Processing
			if (m_enableTextureProcessing) {
				//Encode colorData as JPEG
				cv::Mat colorBufferIn = convertColorDataToOpenCVMat(colorData);
				cv::Mat colorBufferUndistorted;
				if (device->getCameraMatrix() && device->getDistortionCoefficients()) cv::undistort(colorBufferIn, colorBufferUndistorted, *device->getCameraMatrix(), *device->getDistortionCoefficients());
				else colorBufferUndistorted = colorBufferIn;

				std::vector<uchar>* colorBufferOut = new std::vector<uchar>();
				cv::imencode(".jpg", colorBufferUndistorted, *colorBufferOut, std::vector<int>{cv::IMWRITE_JPEG_QUALITY, m_textureProcessingJpegQuality});

				//Encode depthData as JPEG
				cv::Mat depthBufferIn = convertDepthDataToOpenCVMat(depthData);

				/*cv::Mat depthBufferIn = cv::Mat(depthData->height(), depthData->width(), CV_16UC1);
				for (int i = 0; i < depthData->width() * depthData->height(); i++) {
					((unsigned short*) depthBufferIn.data)[i] = ((unsigned short*) depthData->data())[i];
				}*/

				/*for (int u = 0; u < depthData->width(); u++) {
					for (int v = 0; v < depthData->height(); v++) {
						depthBufferIn.at<osg::Vec3ub>(v, u).z() = (unsigned char) (255.f * pow((1.f - abs(1.f - 2.f * u / depthData->width())) * (1.f - abs(1.f - 2.f * v / depthData->height())), 2));
					}
				}*/
				cv::Mat depthBufferUndistorted;
				if (device->getCameraMatrix() && device->getDistortionCoefficients()) cv::undistort(depthBufferIn, depthBufferUndistorted, *device->getCameraMatrix(), *device->getDistortionCoefficients());
				else depthBufferUndistorted = depthBufferIn;

				std::vector<uchar>* depthBufferOut = new std::vector<uchar>();
				cv::imencode(".jpg", depthBufferUndistorted, *depthBufferOut, std::vector<int>{cv::IMWRITE_JPEG_QUALITY, m_textureProcessingJpegQuality});
				//cv::imencode(".png", depthBufferUndistorted, *depthBufferOut);

				std::vector<cv::Point3f> pIn{ cv::Point3f(1.f, 1.f, 1.f), cv::Point3f(0.f, 0.f, 1.f) };
				std::vector<cv::Point2f> pOut;
				cv::projectPoints(pIn, cv::Vec3f(0, 0, 0), cv::Vec3f(0, 0, 0), *device->getCameraMatrix(), cv::Mat(), pOut);
				//std::cout << "(" << std::to_string(pOut[0].x) << ", " << std::to_string(pOut[0].y) << "), (" << std::to_string(pOut[1].x) << ", " << std::to_string(pOut[1].y) << ")" << std::endl;

				m_imageData->emplace_back(new ImageData{ osg::Vec2f(pOut[0].x, pOut[0].y), osg::Vec2f(pOut[1].x, pOut[1].y), colorData->width(), colorData->height(), depthData->depthScale(), colorBufferOut, depthBufferOut, m_scanPipeline->getTransform() });

				//cv::imwrite("data/export/testColor.png", cv::imdecode(cv::Mat(1, m_imageData->at(m_imageData->size() - 1)->_colorBuffer->size(), CV_8UC1, m_imageData->at(m_imageData->size() - 1)->_colorBuffer->data()), cv::ImreadModes::IMREAD_UNCHANGED));
				//cv::imwrite("data/export/testColorDistorted.png", colorBufferIn);
				//cv::imwrite("data/export/testColorUndistorted.png", colorBufferUndistorted);
				//cv::imwrite("data/export/testDepth.png", cv::imdecode(cv::Mat(1, m_imageData->at(m_imageData->size() - 1)->_depthBuffer->size(), CV_8UC1, m_imageData->at(m_imageData->size() - 1)->_depthBuffer->data()), cv::ImreadModes::IMREAD_UNCHANGED));
				//cv::imwrite("data/export/testDepth2.png", depthBufferIn);

				//cv::Mat depthTest = cv::Mat(depthData->height(), depthData->width(), CV_16UC1);
				//for (int i = 0; i < depthData->width() * depthData->height(); i++) {
				//	((unsigned short*) depthTest.data)[i] = *((unsigned short*) (&depthBufferIn.data[3 * i]));
				//}
				//cv::imwrite("data/export/testDepth3.png", depthTest);
			}

			PointcloudData* pointcloudData = nullptr;
			auto calculationResult = m_scanPipeline->calculatePointcloud(&pointcloudData);
			if (ScanPipeline::PointcloudCalculationResult::SUCCESS != calculationResult) {
				std::cout << "### PointcloudCalculation Error at scan #" << i << ": " << ScanPipeline::pointcloudCalculationResultToString(calculationResult) << "###" << std::endl;
				return; // TODO: Error Handling
			}

			//m_pointCloudVisualizer->processPointCloud(i, depthData, colorData, pointcloudData, m_scanPipeline->getTransform(), m_writeTSDF);
			m_pointCloudVisualizer->processPointCloud(i, colorData, pointcloudData, m_scanPipeline->getTransform(), g_cameraPoses[m_scanPipeline->getSelectedDeviceIndex()].second, m_writeTSDF); // NEW FUNCTION

			auto time2 = std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::system_clock::now().time_since_epoch()).count();
			//std::cout << "ProcessPointCloud After Scan: " << time2 << std::endl;
			//std::cout << "ProcessPointCloud Scan time: " << time2 - time << std::endl;

			if (m_isRecording) m_pointcloudRecording->push_back(device->unhookDownsampledPointcloud());
			m_scanPipeline->selectNextDevice();
		}
	}
	else { // TODO 3: Test functionality
		std::vector<ColorData*> colorData(m_devicesPerScan);
		std::vector<DepthData*> depthData(m_devicesPerScan);

		std::vector<std::future<ScanPipeline::ScanResult>> futures = m_scanPipeline->scanAsync(m_devicesPerScan, &colorData, &depthData, 0u);

		for (int i = 0; i < m_devicesPerScan; i++) { //It might be better to wait for all threads to complete the scan before continuing with the pointcloud calculation, in order to not slow down the image capturing by no means.
			std::future<ScanPipeline::ScanResult>& future = futures.at(i);
			if (future.valid()) future.wait();
		}

		for (int i = 0; i < m_devicesPerScan; i++) {
			ScanPipeline::ScanResult scanResult = futures.at(i).get();
			ScanDevice* device = m_scanPipeline->getSelectedDevice();

			if (ScanPipeline::ScanResult::SUCCESS != scanResult) {
				std::cout << "### Scan Error at scan #" << i << ": " << ScanPipeline::scanResultToString(scanResult) << " ###" << std::endl;
				continue;
			}

			// Texture Processing
			if (m_enableTextureProcessing) {
				// TODO 2: Fix scanning with multiple devices

				//Encode colorData as JPEG
				cv::Mat colorBufferIn = convertColorDataToOpenCVMat(colorData[i]);
				std::vector<uchar>* colorBufferOut = new std::vector<uchar>();
				cv::imencode(".jpg", colorBufferIn, *colorBufferOut, std::vector<int>{cv::IMWRITE_JPEG_QUALITY, m_textureProcessingJpegQuality});

				//Encode depthData as JPEG
				cv::Mat depthBufferIn = convertDepthDataToOpenCVMat(depthData[i]);
				std::vector<uchar>* depthBufferOut = new std::vector<uchar>();
				cv::imencode(".jpg", depthBufferIn, *depthBufferOut, std::vector<int>{cv::IMWRITE_JPEG_QUALITY, m_textureProcessingJpegQuality});

				std::vector<cv::Point3f> pIn{ cv::Point3f(1.f, 1.f, 1.f), cv::Point3f(0.f, 0.f, 1.f) };
				std::vector<cv::Point2f> pOut;
				cv::projectPoints(pIn, cv::Vec3f(0, 0, 0), cv::Vec3f(0, 0, 0), *device->getCameraMatrix(), cv::Mat(), pOut);
				std::cout << "(" << std::to_string(pOut[0].x) << ", " << std::to_string(pOut[0].y) << "), (" << std::to_string(pOut[1].x) << ", " << std::to_string(pOut[1].y) << ")" << std::endl;

				m_imageData->emplace_back(new ImageData{ osg::Vec2f(pOut[0].x, pOut[0].y), osg::Vec2f(pOut[1].x, pOut[1].y), colorData[i]->width(), colorData[i]->height(), depthData[i]->depthScale(), colorBufferOut, depthBufferOut, m_scanPipeline->getTransform() });

			}

			PointcloudData* pointcloudData = nullptr;
			auto calculationResult = m_scanPipeline->calculatePointcloud(&pointcloudData); //TODO 3: Maybe implement multithreaded alternatve for pointcloudCalculation in the ScanPipeline as well
			if (ScanPipeline::PointcloudCalculationResult::SUCCESS != calculationResult) {
				std::cout << "### PointcloudCalculation Error at scan #" << i << ": " << ScanPipeline::pointcloudCalculationResultToString(calculationResult) << "###" << std::endl;
				continue;
			}
			//m_pointCloudVisualizer->processPointCloud(i, depthData.at(i), colorData.at(i), pointcloudData, m_scanPipeline->getTransform(), m_writeTSDF);
			m_pointCloudVisualizer->processPointCloud(i, colorData.at(i), pointcloudData, m_scanPipeline->getTransform(), g_cameraPoses[m_scanPipeline->getSelectedDeviceIndex()].second, m_writeTSDF); // NEW FUNCTION
			if (m_isRecording) m_pointcloudRecording->push_back(device->unhookDownsampledPointcloud());
			m_scanPipeline->selectNextDevice();
		}
	}

	//m_isProcessing.store(false);
}

cv::Mat PointCloudManager::convertColorDataToOpenCVMat(ColorData* colorIn) {
	unsigned int width = colorIn->width();
	unsigned int height = colorIn->height();
	switch (colorIn->format()) {
	case ColorData::ColorFormat::BGR8:
		return cv::Mat(cv::Size(width, height), CV_8UC3, (void*)colorIn->data(), cv::Mat::AUTO_STEP);
	case ColorData::ColorFormat::BGRA8:
	{
		cv::Mat in(cv::Size(width, height), CV_8UC4, (void*)colorIn->data(), cv::Mat::AUTO_STEP);
		cv::Mat out(cv::Size(width, height), CV_8UC3, cv::Mat::AUTO_STEP);
		cv::cvtColor(in, out, CV_BGRA2BGR);
		return out;
	}
	case ColorData::ColorFormat::RGB8:
	{
		cv::Mat in(cv::Size(width, height), CV_8UC3, (void*)colorIn->data(), cv::Mat::AUTO_STEP);
		cv::Mat out(cv::Size(width, height), CV_8UC3, cv::Mat::AUTO_STEP);
		cv::cvtColor(in, out, cv::ColorConversionCodes::COLOR_RGB2BGR);
		return out;
	}
	case ColorData::ColorFormat::RGBA8:
	{
		cv::Mat in(cv::Size(width, height), CV_8UC4, (void*)colorIn->data(), cv::Mat::AUTO_STEP);
		cv::Mat out(cv::Size(width, height), CV_8UC4, cv::Mat::AUTO_STEP);
		cv::cvtColor(in, out, cv::ColorConversionCodes::COLOR_RGBA2BGRA);
		return out;
	}
	}
	return cv::Mat();
}

cv::Mat PointCloudManager::convertDepthDataToOpenCVMat(DepthData* depthIn) {
	unsigned int width = depthIn->width();
	unsigned int height = depthIn->height();

	switch (depthIn->format()) {
	case DepthData::DepthFormat::Z16:
	{
		cv::Mat depthOut = cv::Mat(height, width, CV_8UC3);
		unsigned char* depthToColor = depthOut.data;
		for (unsigned int i = 0; i < width * height; i++) {
			/*depthToColor[3 * i] = ((unsigned char*) depthIn->data())[2 * i];
			depthToColor[3 * i + 1] = ((unsigned char*) depthIn->data())[2 * i + 1];*/
			*((unsigned short*)(depthToColor + 3 * i)) = ((unsigned short*)depthIn->data())[i];
			depthToColor[3 * i + 2] = 0;
		}
		return depthOut;
	}
	case DepthData::DepthFormat::Z16BE:
	{
		cv::Mat depthOut = cv::Mat(height, width, CV_8UC3);
		unsigned char* depthToColor = depthOut.data;
		for (int i = 0; i < width * height; i++) {
			*((unsigned short*)(depthToColor + 3 * i)) = _byteswap_ushort(((unsigned short*)depthIn->data())[i]);
			depthToColor[3 * i + 2] = 0;
		}
		return depthOut;
	}
	}
	return cv::Mat();
}

// ### START - Old Scan Pipeline ###
//Function is running on an additional thread, so it does not slow down the rendering of processed data.
//void PointCloudManager::processPointCloudWithMultipleDevices(std::vector<osg::MatrixTransform*> transformationMatricesForPointsIn)
//{
//	m_processing.store(true); //acts as a lock -> atomic bool is free of race condition
//	if (m_useMultipleRealSenseDevices)
//	{
//		//warmUpRealSense(5);
//		auto total_number_of_streams = m_realSenseDeviceContainer->streamCount();
//		std::cout << "total_number_of_streams: " << total_number_of_streams << std::endl;
//
//		unsigned short realSenseID = 0;
//		std::map<std::string, RealSenseDeviceContainer::view_port>::iterator it;
//		for (it = m_realSenseDeviceContainer->getDevices()->begin(); it != m_realSenseDeviceContainer->getDevices()->end(); it++)
//		{
//			rs2::frameset frames = it->second.pipe.wait_for_frames();
//
//
//			//std::cout << "frames.size(): " << frames.size() << std::endl;
//
//			rs2::depth_frame depthFrame = frames.get_depth_frame();
//
//			//Apply predefined filters. Maybe create a selection menu to toggle those?
//			//depth = m_rs_dec_filter->process(depth); //Must be applied before a transformation to disparity is done
//			//depth = m_rs_depth_to_disparity->process(depth);
//			//depth = m_rs_spat_filter->process(depth);
//			//depth = m_rs_temp_filter->process(depth);	
//			//depth = m_rs_disparity_to_depth->process(depth);
//
//			// Generate the pointcloud and texture mappings
//			// Notice: Very important is that the colorFrame is generated and mapped to the PoincCloud before calculating the depth!!!
//			rs2::video_frame colorFrame = frames.get_color_frame();
//			m_rsPc->map_to(colorFrame);
//			rs2::points rsPoints = m_rsPc->calculate(depthFrame);
//
//			std::cout << "rsPoints.size(): " << rsPoints.size() << std::endl;
//			std::cout << "realSenseID: " << realSenseID << std::endl;
//			std::cout << "m_realSenseDeviceContainer->getDevices()->size(): " << m_realSenseDeviceContainer->getDevices()->size() << std::endl;
//			std::cout << "m_realSenseDeviceContainer->deviceCount(): " << m_realSenseDeviceContainer->deviceCount() << std::endl;
//
//
//			// Copy rsPoints to PCL-pointCloud
//			//pcl::PointCloud<pcl::PointXYZRGB>::Ptr pclPoints = rsPoints_to_pcl(rsPoints, colorFrame);
//
//			//Apply PCL Filters on new frame
//			/*std::cout << "Start RadiusOutlierRemoval\n";
//			pcl::RadiusOutlierRemoval<pcl::PointXYZRGB> ror;
//			ror.setInputCloud(pclPoints);
//			ror.setRadiusSearch(0.2);
//			ror.setMinNeighborsInRadius(2);
//			ror.filter(*pclPoints);
//			std::cout << "End RadiusOutlierRemoval\n";*/
//
//			/*std::cout << "Start StatisticalOutlierRemoval\n";
//			pcl::StatisticalOutlierRemoval<pcl::PointXYZRGB> sor;
//			sor.setInputCloud(pclPoints);
//			sor.setMeanK(40);
//			sor.setStddevMulThresh(1.0);
//			sor.filter(*pclPoints);
//			std::cout << "End StatisticalOutlierRemoval\n";*/
//
//			// ICP: http://pointclouds.org/documentation/tutorials/iterative_closest_point.php
//			// Note: ICP seems to be really slow at the moment
//			/*if (!m_pclPc->empty()) {
//				pcl::IterativeClosestPoint<pcl::PointXYZRGB, pcl::PointXYZRGB> icp;
//				icp.setMaximumIterations(1);
//				//icp.setMaxCorrespondenceDistance(1);
//				icp.setInputSource(pclPoints);
//				icp.setInputTarget(m_pclPc);
//				std::cout << "Before ICP\n";
//				icp.align(*pclPoints); //Takes very long
//				std::cout << "After ICP\n";
//				//std::cout << "has converged:" << icp.hasConverged() << " score: " << icp.getFitnessScore() << std::endl;
//				//std::cout << icp.getFinalTransformation() << std::endl;
//			}*/
//			//Add the points of the new Frame to the full PointCloud
//			//*m_pclPc += *pclPoints;
//
//
//			m_pointCloudVisualizer->processRSPointCloud(rsPoints,
//				transformationMatricesForPointsIn[realSenseID]->getMatrix(),
//				colorFrame, depthFrame
//			);
//
//			m_pointCloudVisualizer->visualizeRSPointCloud();
//			++realSenseID;
//		}
//
//		//int neuerCounter = 0;
//		//for (auto&& sn_to_dev : *m_realSenseDeviceContainer->getDevices())
//		//{
//		//	neuerCounter++;
//		//}
//		//std::cout << "neuerCounter: " << neuerCounter << std::endl;
//
//
//
//
////		//77 Hol intern alle Frames ab von allen Devices
////		m_realSenseDeviceContainer->pollFrames(); //77 Eventuell erweitern und auch TransformationsMatrix reinschreiben lassen
////		m_realSenseDeviceContainer->getDevices().;
////			
////		//77 Wieviele Devices sind connected und gehe über alle devices
////		//77 Hol die Frames aus dem Device Container und hol die entsprechenden Matrizen da auch raus
////		//for (size_t i = 0; i <= m_realSenseDeviceContainer->deviceCount(); ++i)
////		//{
////		//	auto itr = m_realSenseDeviceContainer->getDevices()->begin();
////		//	while (itr != m_realSenseDeviceContainer->getDevices()->end())
////		//	{
////		//		//Get frame
////		//		itr->second.colorize_frame
////		//	}
////		unsigned short realSenseID = 0;
////		for (auto&& view : *m_realSenseDeviceContainer->getDevices())
////		{
////			//if (m_processing.load()) return; //Prevents writing additional data to the buffer while data is being processed. However we handle this outside the method, so it just takes up time
////			m_processing.store(true); //acts as a lock -> atomic bool is free of race condition
////
////
////			rs2::frame frame = view.second.frames_per_stream.at(0);
////			
////
////			//77 ACHTUNG - klappt nicht mehr!!!!!!!!!!!!!!
////			//m_pointCloudVisualizer->pushMatrix(transformationMatricesForPointsIn[i]); //77 ACHTUNG - klappt nicht mehr!!!!!!!!!!!!!!
////			//77 ACHTUNG - klappt nicht mehr!!!!!!!!!!!!!!
////			//77 Brauch ich einen speziellen MultiDeviceVis??? Hab das gefühl das der Manager eine Menge des MultiKrams abnehmen kann
////
////			// Ask each pipeline if there are new frames available
////			//rs2::frameset frames;
////
////
////
////			//if (view.second.pipe.poll_for_frames(&frames))
////			//{
////			//	//for (int i = 0; i < frames.size(); i++)
////			//	if (frames.size() > 0)
////			//	{
////			//		for (int i = 0; i < 1; i++) //ToDo: Can be done better. Just only process the first frame in the frameset
////			//		{
////						std::cout << "view.second.pipe.poll_for_frames is true" << std::endl;
////
////						//rs2::frame new_frame = frames[0];
/////*						int stream_id = new_frame.get_profile().unique_id();
////						view.second.frames_per_stream[stream_id] = view.second.colorize_frame.process(new_frame);*/ //update view port with the new stream
////
////
////						//rs2::frameset frames = m_rsPipe->wait_for_frames(); //77 poll_for_frames ist sicherlich schneller als wait_for_frames()!
////						//rs2::frame frame = frames.get_depth_frame();
////						if (frame)
////						{
////							std::cout << "In if(frame)" << std::endl;
////						}
////						else
////						{
////							std::cout << "In else(frame)" << std::endl;
////						}
////
////						//Apply predefined filters. Maybe create a selection menu to toggle those?
////						//depth = m_rs_dec_filter->process(depth); //Must be applied before a transformation to disparity is done
////						//depth = m_rs_depth_to_disparity->process(depth);
////						//depth = m_rs_spat_filter->process(depth);
////						//depth = m_rs_temp_filter->process(depth);	
////						//depth = m_rs_disparity_to_depth->process(depth);
////
////						// Generate the pointcloud and texture mappings
////						rs2::points rsPoints = m_rsPc->calculate(frame);
////						rs2::video_frame colorFrame = view.second.colorize_frame.colorize(frame);
////						m_rsPc->map_to(colorFrame);
////
////						m_pointCloudVisualizer->processRSPointCloud(rsPoints, transformationMatricesForPointsIn[realSenseID], colorFrame); //77 Diese Funktion braucht keine ...
////						//... Info darüber welcher Realsense genau angesprochen wird, nur die richtige transformationsMatrix
////			//		}
////			//	}
////			//}
////
////			std::cout << "Nicht so tief drin" << std::endl;
////
////
////			m_processing.store(false);
////			++realSenseID;
////		}
//	}
//	else {
//
//		std::cout << "Error: Function for multiple devices is called, but the configuration file contains information to use just only one device (device: RealSense, Kinect or any other Depth camera)" << std::endl;
//	}
//	m_processing.store(false);
//}
// ### END - Old Scan Pipeline ###

//Only visualizes the data from the buffer after a frame is fully processed. The data is then written to the actual lists for rendering.
bool PointCloudManager::visualizePointCloud()
{
	if (m_isProcessing.load() || !m_pointCloudVisualizer->bufferDataAvailable()) return false; //Prevents reading while data is being processed.
	if (m_enableRealtimePointCloud) m_pointCloudVisualizer->clearPointCloudDisplay();
	m_pointCloudVisualizer->visualizeRSPointCloud(/*colorFrame*/);
	return true;
}

void PointCloudManager::clearPointCloud()
{
	m_pointCloudVisualizer->clearPointCloudFull();
}

void PointCloudManager::updateProcessingThread() {
	if (m_processingThread && !m_isProcessing.load()) {
		unsigned long long processingThreadDowntime = std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::system_clock::now().time_since_epoch()).count() - m_processingThreadStartTime;
		if (processingThreadDowntime > m_processingThreadMaxDowntime) {
			m_closeProcessingThread.store(true);
			if (m_processingThread->joinable()) m_processingThread->join();
			delete m_processingThread;
			m_processingThread = nullptr;
#if LOG_PROCESSING_THREAD_MESSAGES == 1
			std::cout << "ProcessingThread: closed after " << processingThreadDowntime << "ms downtime" << std::endl;
#endif
			m_closeProcessingThread.store(false);
		}
	}
}

void PointCloudManager::initGeometries()
{
	//create 2-sided lightmodel
	osg::LightModel* ltModel = new osg::LightModel;
	ltModel->setTwoSided(false);

	//a geode and a geometry for each thread
	for (size_t currThreadNr = 0; currThreadNr < m_maxNumThreads; currThreadNr++)
	{
		//create geode and adjust settings
		m_geodes[currThreadNr] = new osg::Geode();
		m_geodes[currThreadNr]->getOrCreateStateSet()->setMode(GL_RESCALE_NORMAL, osg::StateAttribute::ON);
		m_geodes[currThreadNr]->getOrCreateStateSet()->setMode(GL_LIGHTING, osg::StateAttribute::OVERRIDE | osg::StateAttribute::OFF);

		//create geometry and adjust settings
		m_geometries[currThreadNr] = new osg::OpenMeshGeometry(GeometryType::MODELLING, m_configReader, "livemesh_cloth_" + std::to_string(currThreadNr));
		m_geometries[currThreadNr]->addPrimitiveSet(new osg::DrawElementsUInt(osg::PrimitiveSet::TRIANGLES, 0));
		m_geometries[currThreadNr]->setColorBinding(osg::Geometry::BIND_PER_VERTEX);

		//add geometry as a child to the geode and mark dirty
		m_geodes[currThreadNr]->addChild(m_geometries[currThreadNr]);
		m_geodes[currThreadNr]->dirtyBound();

		//set light model
		m_geodes[currThreadNr]->getOrCreateStateSet()->setAttribute(ltModel);
		m_geometries[currThreadNr]->getOrCreateStateSet()->setAttribute(ltModel);

		//set cullFace mode on
		m_geodes[currThreadNr]->getOrCreateStateSet()->setMode(GL_CULL_FACE, osg::StateAttribute::ON);
		m_geometries[currThreadNr]->getOrCreateStateSet()->setMode(GL_CULL_FACE, osg::StateAttribute::ON);
	}

}//Just simple update function for calling once from the "main loop" in ClayMore/Source Files/main.cpp
//This function handles one RealSense device and is only called when useMultipleRealSenseDevices is false from the main loop
//For single devices
void PointCloudManager::updateAndRenderPointCloud()
{
	if (m_useAzureTelepresence)
	{
		//processPointCloud();
		m_telepresenceThread = std::thread(&PointCloudManager::processPointCloud, this);
		m_telepresenceThread.detach();
	}
	else {

		if (m_isProcessing.load() || m_pointCloudVisualizer->bufferDataAvailable()) return;

		//Properly dispose the thread used for processing
		if (m_processingThread->joinable()) {
			m_processingThread->join();
			delete m_processingThread;
			m_processingThread = nullptr;
		}

		//m_pointCloudVisualizer->pushMatrix(transformationMatrixForPointsIn);
		//visualizePointCloud(); //pushing each element from the buffer to the list one after another is causing lag spikes, if done on the main thread.

		m_isProcessing.store(true);
		m_processingThread = new std::thread(&PointCloudManager::processPointCloud, this);
	}
}

bool PointCloudManager::attemptToScan() {
	size_t deltaTime = std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::system_clock::now().time_since_epoch()).count();
	if (m_scanDeviceInitLock.load() || m_calibrateCameraPosesState.load() != 0 || m_isProcessing.load() || m_pointCloudVisualizer->bufferDataAvailable() || (m_enableRealtimePointCloud && deltaTime - m_timeSinceLastScan < 1000 / m_realtimePointCloudFPS)) return false; //Starting a new scan right now would collide with the latest processing task
	m_timeSinceLastScan = deltaTime;

	if (m_enableRealtimePointCloud) m_pointCloudVisualizer->clearPointCloudData();

	m_isProcessing.store(true);
	if (!m_processingThread) {
		m_processingThread = new std::thread(&PointCloudManager::processingLoop, this);
#if LOG_PROCESSING_THREAD_MESSAGES == 1
		std::cout << "ProcessingThread: started" << std::endl;
#endif
	}

	return true;
	}

//This function runs on the processing thread
void PointCloudManager::processingLoop() {
	do {
		// This is the actual processing step: processPointCloud
		if (m_isProcessing.load()) {
#if LOG_PROCESSING_THREAD_MESSAGES == 1
			std::cout << "ProcessingThread: started task" << std::endl;
#endif
			processPointCloud();
			m_processingThreadStartTime = std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::system_clock::now().time_since_epoch()).count(); // Reset the startTime to allow checking the actual downtime of the thread
			m_isProcessing.store(false);
#if LOG_PROCESSING_THREAD_MESSAGES == 1
			std::cout << "ProcessingThread: completed task" << std::endl;
#endif
		}
		} while (!m_closeProcessingThread.load());
#if LOG_PROCESSING_THREAD_MESSAGES == 1
		std::cout << "ProcessingThread: loop ended" << std::endl;
#endif
}

//Just simple update function for calling once from the "main loop" in ClayMore/Source Files/main.cpp
//This function handles multiple RealSense devices and is only called when useMultipleRealSenseDevices is true from the main loop
//For multiple devices
/* ### START - Old Scan Pipeline ###
void PointCloudManager::updateAndRenderPointCloud(std::vector<osg::MatrixTransform*> transformationMatricesForPointsIn)
{
	if (m_processing.load())
	{
		return;
	}

	if (m_processingThread.joinable())
	{
		m_processingThread.join();
	}

	//	m_pointCloudVisualizer->pushMatrix(transformationMatrixForPointsIn);
		//visualizePointCloud(); //pushing each element from the buffer to the list one after another is causing lag spikes, if done on the main thread.
	m_processingThread = std::thread(&PointCloudManager::processPointCloudWithMultipleDevices, this, transformationMatricesForPointsIn);
}
// ### END - Old Scan Pipeline ### */

//void PointCloudManager::convertPointCloudToPCL(osg::ref_ptr<osg::OpenMeshGeometry> openMeshGeo)
//{
//	m_pointCloudVisualizer->convertPointCloudToPCL(openMeshGeo);
//}

void PointCloudManager::savePointCloud(std::string savePathIn)
{
	m_pointCloudVisualizer->savePointCloudWithOSG(savePathIn);
}

void PointCloudManager::savePointCloud(std::string savePathIn, osg::ref_ptr<osg::OpenMeshGeometry> openMeshGeo)
{
	m_pointCloudVisualizer->savePointCloudWithOSG(savePathIn, openMeshGeo);
}

void PointCloudManager::loadPointCloud(osg::ref_ptr<osg::OpenMeshGeometry> openMeshGeo, std::string loadPathIn)
{
	m_pointCloudVisualizer->loadPointCloudWithOSG(openMeshGeo, loadPathIn);
}


bool PointCloudManager::revertSnapshot()
{
	return m_pointCloudVisualizer->revertSnapshot();
}

void PointCloudManager::polygonizePointCloud() {
	m_pointCloudVisualizer->polygonizePointCloudFromOctree();
	//m_pointCloudVisualizer->polygonizePointCloudForBodyScanning();
	//m_pointCloudVisualizer->polygonizePointCloudMultithreaded(SCAN_AREA_CENTER, SCAN_AREA_EXTENTS);
}

/*void PointCloudManager::debugDraw(osg::ref_ptr<osg::MatrixTransform> genericTracker) {
	m_pointCloudVisualizer->debugClearLines();

	osg::Vec4 debugColor0 = osg::Vec4(0, 1, 0, 1);
	osg::Vec4 debugColor1 = osg::Vec4(0, 1, 0.5f, 1);

	osg::Matrix trackerMatrix = genericTracker->getMatrix();

	m_pointCloudVisualizer->debugDrawLine(trackerMatrix.getTrans(), RS_FORWARD * 0.5f * trackerMatrix, debugColor0, debugColor1);
	//m_pointCloudVisualizer->debugDrawLine(trackerMatrix.getTrans(), osg::Vec3() * RS_TRACKER_OFFSET_MATRIX * trackerMatrix, debugColor0, debugColor1);

	//m_pointCloudVisualizer->debugDrawOctreeVolume();

	//osg::Matrix rot = osg::Matrix::rotate(osg::DegreesToRadians(90.0f), osg::Vec3(1, 0, 0));
	//osg::Matrix rot2 = osg::Matrix::rotate(osg::DegreesToRadians(180.0f), osg::Vec3(0, 0, 1));
	//// Orginal: 0.0199999, 0.00877001, -0.0170963
	//osg::Matrix trans = osg::Matrix::translate(osg::Vec3(-0.0199999, -0.0170963, 0.00877001));
	//trackerMatrix = trans * rot2 * rot * trackerMatrix;


	osg::Matrix trans = osg::Matrix::translate(osg::Vec3(-0.0200003, 0.0257426, 0.0481501));
	osg::Matrix rot = osg::Matrix::rotate(osg::DegreesToRadians(90.0f), osg::Vec3(1, 0, 0));

	trackerMatrix = trans * rot * trackerMatrix;

	m_pointCloudVisualizer->debugDrawLine(genericTracker->getMatrix().getTrans(), osg::Vec3() * trackerMatrix, debugColor0, debugColor1);

	//m_pointCloudVisualizer->debugDrawLine(genericTracker->getMatrix().getTrans(), osg::Vec3() * trackerMatrix, debugColor0, debugColor1);

	//m_pointCloudVisualizer->debugDrawNormals();
}*/

void PointCloudManager::drawDDA(osg::Vec3f from, osg::Vec3f to) {
	m_pointCloudDisplay->drawDDA(m_pointCloudVisualizer->getOctreeVolume(), from, to, osg::Vec4(1.f, 0.f, 0.f, 1.f));
}

void PointCloudManager::clearOctreeVolumeDisplay() {
	m_pointCloudDisplay->clearOctreeVolumeDisplay();
}

void PointCloudManager::clearTSDFDisplay() {
	m_pointCloudDisplay->clearTSDFDisplay();
}

void PointCloudManager::clearDDADisplay() {
	m_pointCloudDisplay->clearDDADisplay();
}

void PointCloudManager::clearPolygonize() {
	m_pointCloudVisualizer->clearMarchingCubesMeshes();
}

void PointCloudManager::togglePointCloudDisplay() {
	m_pointCloudVisualizer->togglePointCloudDisplay();
}

void PointCloudManager::toggleOctreeDisplay() {
	m_pointCloudDisplay->toggleOctreeDisplay();
}

void PointCloudManager::toggleTSDFDisplay() {
	m_pointCloudDisplay->toggleTSDFDisplay();
}

void PointCloudManager::toggleDDADisplay() {
	m_pointCloudDisplay->toggleDDADisplay();
}

void PointCloudManager::drawOctreeVolume() {
	m_pointCloudDisplay->drawOctreeVolume(m_pointCloudVisualizer->getOctreeVolume(), osg::Vec4(0.f, .25f, .75f, .3f));
}

void PointCloudManager::drawTSDF() {
	m_pointCloudDisplay->drawTSDF(m_pointCloudVisualizer->getOctreeVolume(), osg::Vec4(0.f, 1.f, 0.f, 1.f), osg::Vec4(1.f, 0.f, 0.f, 1.f));
}

/*void PointCloudManager::debugRedraw()
{
	if (!m_processing.load())
	{
		m_pointCloudVisualizer->debugRedrawLines();
	}
}*/

void PointCloudManager::setTimedCircularTrackerMatrixBuffer(
	boost::circular_buffer<TimedOpenVRMatrix>* timedCircularTrackerMatrixBuffer,
	std::atomic<bool>* timedCircularTrackerMatrixBuffer_Lock)
{
	m_timedCircularTrackerMatrixBuffer = timedCircularTrackerMatrixBuffer;
	m_timedCircularTrackerMatrixBuffer_Lock = timedCircularTrackerMatrixBuffer_Lock;
}

void PointCloudManager::startAzureKinect()
{
	//If KinectShooter (Alex Pechs Thesis) is active, then attach the live view quad
	if (m_useColorAndDepthShooter)
	{
		m_pointCloudGroup->addChild(m_liveViewQuadGeode);
	}


	m_kinectCapture = new k4a::capture();

	try
	{
		m_azureKinectSensor = k4a::device::open(K4A_DEVICE_DEFAULT);

	}
	catch (const std::exception&)
	{
		std::cerr << "Device cannot be opened." << std::endl;
	}

	m_config.color_format = K4A_IMAGE_FORMAT_COLOR_BGRA32;
	m_config.color_resolution = (k4a_color_resolution_t)m_configReader->getIntFromStartupConfig("k4a_color_resolution_t");
	m_config.depth_mode = (k4a_depth_mode_t)m_configReader->getIntFromStartupConfig("k4a_depth_mode_t");
	m_config.camera_fps = (k4a_fps_t)m_configReader->getIntFromStartupConfig("k4a_fps_t");
	m_config.synchronized_images_only = true; // ensures that depth and color images are both available in the capture
	m_azureKinectSensor.start_cameras(&m_config);

	m_calibration = m_azureKinectSensor.get_calibration(m_config.depth_mode, m_config.color_resolution);
	m_transformation = k4a::transformation(m_calibration);
}

void PointCloudManager::makeAndSaveAFewDepthImagesWithKinect(std::string dirSavePath)
{
	TimeWriter tw;
	tw.startMetering("makeAndSaveAFewDepthImagesWithKinect", true, false);
	//std::cout << "Start writing K4A images for face scanning ..." << std::endl;

	//processPointCloud();

	std::cout << "Start writing K4A images for face scanning ..." << std::endl;
	//####################################
	//Initilization
	k4a::image color_image;
	k4a::image depthImage;
	k4a::image ir_image;
	k4a::image transformedDepthImage;
	std::vector<k4a::capture> captures;

	//####################################

	//captures.reserve(m_framesToCapture);

	//####################################
	//Unimportant stuff for face scanning
	//k4a::calibration calibration;
	//calibration = m_azureKinectSensor.get_calibration(m_config.depth_mode, m_config.color_resolution);

	//m_transformation = k4a::transformation(calibration);


	//####################################
	//Now it's getting interesting for face scanning
	//Take a number of frames
	//for (int j = 0; j < m_framesToCapture + 1; j++)
	//{
	m_azureKinectSensor.get_capture(m_kinectCapture);
	captures.push_back(*m_kinectCapture);
	//}

	//k4a::image depthAverageImage = captures.at(0).get_depth_image();
	//uint8_t* depth_buffer_average = depthAverageImage.get_buffer();
	//for (size_t p = 0; p < depthAverageImage.get_size(); p += 2)
	//{
	//	depth_buffer_average[p] = 0;
	//	depth_buffer_average[p + 1] = 0;
	//}


	//for (int j = 1; j < m_framesToCapture + 1; j++)
	//{
	int j = 0;
	//Get a color image capture
	color_image = captures.at(j).get_color_image();

	//Get a depth image capture
	//depthImage = k4a_capture_get_depth_image(captures.at(j));
	depthImage = captures.at(j).get_depth_image();

	ir_image = captures.at(j).get_ir_image();

	//Check if results are ok
	if (color_image && depthImage && ir_image)
	{
		//Create a color image buffer
		uint8_t* color_buffer = color_image.get_buffer();
		size_t color_buffer_size = color_image.get_size();

		// The YUY2 image format is the same stride as the 16-bit depth image, so we can modify it in-place.
		uint8_t* depth_buffer = depthImage.get_buffer();
		size_t depth_buffer_size = depthImage.get_size();

		//Create IR image buffer
		uint8_t* ir_buffer = ir_image.get_buffer();
		size_t ir_buffer_size = ir_image.get_size();

		//Depth Clipping
		for (size_t i = 0; i < depth_buffer_size; i += 2)
		{
			//If the distance is farther than clip (format is YUY2 -> Two byte per pixel results in 16bit and 65535mm depth resolution)
			uint16_t depth = (uint16_t)(depth_buffer[i + 1] << 8 | depth_buffer[i]);
			if (depth > m_clippingDistance)
			{
				depth_buffer[i] = 0;
				depth_buffer[i + 1] = 0;
			}
		}

		//Flip around vertical axis
		////////int widthPixelColor = color_image.get_width_pixels();
		////////int heightPixelColor = color_image.get_height_pixels();
		////////int lineStrideinBytes = color_image.get_stride_bytes();
		////////int currentLineColor = 0;
		////////int currentPixelInLineColor = 0; //Bis 1280
		////////bool isFirstLoop = true;
		////////for (size_t i = 0; i < color_buffer_size; i++)
		////////{
		////////	int iMod = lineStrideinBytes % i;
		////////	std::swap(color_buffer[lineStrideinBytes * currentLineColor + i],
		////////		color_buffer[lineStrideinBytes * currentLineColor + lineStrideinBytes - iMod]);

		////////	//if (currentPixelInLineColor > (widthPixelColor / 2))
		////////	//{
		////////	//	std::swap(color_buffer[lineStrideinBytes * currentLineColor + currentPixelInLineColor],
		////////	//		color_buffer[lineStrideinBytes * currentLineColor + (widthPixelColor - currentPixelInLineColor- 3)]);
		////////	//	std::swap(color_buffer[lineStrideinBytes * currentLineColor + currentPixelInLineColor+ 1],
		////////	//		color_buffer[lineStrideinBytes * currentLineColor + (widthPixelColor - currentPixelInLineColor - 2)]);
		////////	//	std::swap(color_buffer[lineStrideinBytes * currentLineColor + currentPixelInLineColor + 2],
		////////	//		color_buffer[lineStrideinBytes * currentLineColor + (widthPixelColor - currentPixelInLineColor - 1)]);
		////////	//	std::swap(color_buffer[lineStrideinBytes * currentLineColor + currentPixelInLineColor + 3],
		////////	//		color_buffer[lineStrideinBytes * currentLineColor + (widthPixelColor - currentPixelInLineColor)]);
		////////	//}
		////////	//currentPixelInLineColor++;

		////////	if ((lineStrideinBytes % i) == 0 && !isFirstLoop)
		////////	{
		////////		currentLineColor++;
		////////		currentPixelInLineColor = 0;
		////////	}
		////////	isFirstLoop = false;
		////////	//std::cout << "currentPixelInLineColor: " << currentPixelInLineColor << std::endl;
		////////	//std::cout << "currentLineColor: " << currentLineColor << std::endl;
		////////	//std::cout << "i: " << i << std::endl;

		////////	////if (currentPixelInLineColor < (widthPixelColor/2) )
		////////	////{
		////////	//	currentPixelInLineColor++;
		////////	//	std::swap(color_buffer[widthPixelColor * currentLineColor + currentPixelInLineColor],
		////////	//		color_buffer[widthPixelColor * currentLineColor - currentPixelInLineColor]);
		////////	//	//std::swap(color_buffer[widthPixelColor * currentLineColor + currentPixelInLineColor],
		////////	//	//	color_buffer[widthPixelColor * currentLineColor - currentPixelInLineColor - 3]);
		////////	//	//std::swap(color_buffer[widthPixelColor * currentLineColor + currentPixelInLineColor + 1],
		////////	//	//	color_buffer[widthPixelColor * currentLineColor - currentPixelInLineColor - 2]);
		////////	//	//std::swap(color_buffer[widthPixelColor * currentLineColor + currentPixelInLineColor + 2],
		////////	//	//	color_buffer[widthPixelColor * currentLineColor - currentPixelInLineColor - 1]);
		////////	//	//std::swap(color_buffer[widthPixelColor * currentLineColor + currentPixelInLineColor + 3],
		////////	//	//	color_buffer[widthPixelColor * currentLineColor - currentPixelInLineColor]);
		////////	////}

		////////	//if (i % widthPixelColor == 0)
		////////	//{
		////////	//	currentLineColor++;
		////////	//	currentPixelInLineColor = 0;
		////////	//}
		////////}
		////////std::cout << "currentLineColor: " << currentLineColor << std::endl;
		////////std::cout << "widthPixelColor: " << widthPixelColor << std::endl;
		////////std::cout << "heightPixelColor: " << heightPixelColor << std::endl;
		////////std::cout << "color_image.get_stride_bytes(): " << color_image.get_stride_bytes() << std::endl;
		////////std::cout << "color_image.get_format(): " << color_image.get_format() << std::endl;


		//Need dimension in order to allocate memory
		int colorWidth = m_kinectColorImage.get_width_pixels();
		int colorHeight = m_kinectColorImage.get_height_pixels();

		//Write ColorImage
		//osg::Image* osgColorImage = new osg::Image();
		//osgColorImage->setImage(colorWidth,
		//	colorHeight,
		//	1,
		//	GL_BGRA,
		//	GL_BGRA,
		//	GL_UNSIGNED_BYTE,
		//	static_cast<unsigned char*>(color_buffer),
		//	osg::Image::AllocationMode::NO_DELETE);

		//Create a file name
		std::string colorFileName;
		colorFileName.append(dirSavePath);
		colorFileName.append("_colorImage_");
		colorFileName.append(std::to_string(j));
		colorFileName.append(".png");
		std::thread* threadColorImage;

		cv::Mat colorMat(colorHeight, colorWidth, CV_8UC4, (void*)color_buffer, cv::Mat::AUTO_STEP);
		threadColorImage = new std::thread(&cv::imwrite, colorFileName, colorMat, std::vector<int>());
		//Tooo slow
		//threadColorImage = new std::thread(&PointCloudManager::saveImage, this, osgColorImage, colorFileName);
		//osgDB::writeImageFile(*osgColorImage, colorFileName);


		//Adpat the depth image to the size, aspect ratio and intrinsic parameters of the color cam
		k4a::image transformedDepthImage = k4a::image::create(K4A_IMAGE_FORMAT_DEPTH16,
			m_calibration.color_camera_calibration.resolution_width,
			m_calibration.color_camera_calibration.resolution_height,
			m_calibration.color_camera_calibration.resolution_width *
			static_cast<int>(sizeof(uint16_t)));
		m_transformation.depth_image_to_color_camera(depthImage, &transformedDepthImage);
		uint8_t* transformed_depth_buffer = transformedDepthImage.get_buffer();


		////Flip around vertical axis
		//int widthPixelsDepth = transformedDepthImage.get_width_pixels();
		//int heightPixelsDepth = transformedDepthImage.get_height_pixels();
		//int stride = transformedDepthImage.get_stride_bytes();
		//int currentLine = 0;
		//int currentPixelInLine = 0;
		//std::cout << "The image has lines: " << currentLine << std::endl;
		//std::cout << "heightPixelsDepth: " << heightPixelsDepth << std::endl;
		//std::cout << "stride: " << stride << std::endl;
		//for (size_t i = 0; i < depth_buffer_size; i += 2)
		//{
		//	if (currentPixelInLine < widthPixelsDepth / 2)
		//	{
		//		std::swap(transformed_depth_buffer[stride * currentLine + currentPixelInLine * 2],
		//			transformed_depth_buffer[stride * currentLine + stride - 1 - currentPixelInLine * 2 - 1]);
		//		std::swap(transformed_depth_buffer[stride * currentLine + currentPixelInLine * 2 + 1], //HWM: I assume, the +1 lead to a crash...
		//			transformed_depth_buffer[stride * currentLine - currentPixelInLine * 2]);
		//	}
		//	currentPixelInLine++;
		//	if (currentPixelInLine == widthPixelsDepth)
		//	//if ((i / 2) % widthPixelsDepth == 0)
		//	{
		//		currentLine++;
		//		currentPixelInLine = 0;
		//	}
		//}
		//std::cout << "The image has lines2: " << currentLine << std::endl;


		//Get dimensions of the "new" transformed depth image and get the dimension in order to allocate memory
		int transformedDepthWidth = transformedDepthImage.get_width_pixels();
		int transformedDepthHeight = transformedDepthImage.get_height_pixels();

		//Write DepthImage to 16bit png
		//osg::Image* osgDepthImage = new osg::Image();
		//osgDepthImage->setImage(transformedDepthWidth,
		//	transformedDepthHeight,
		//	1,
		//	GL_LUMINANCE16,
		//	GL_LUMINANCE,
		//	GL_UNSIGNED_SHORT,
		//	const_cast<unsigned char*>(static_cast<const unsigned char*>(transformed_depth_buffer)),
		//	osg::Image::AllocationMode::NO_DELETE);

		//Create a file name
		std::string depthFileName;
		depthFileName.append(dirSavePath);
		depthFileName.append("_depthImage_");
		depthFileName.append(std::to_string(j));
		depthFileName.append(".png");
		std::thread* threadDepthImage;

		cv::Mat depthMat(transformedDepthHeight, transformedDepthWidth, CV_16UC1, (void*)transformed_depth_buffer, cv::Mat::AUTO_STEP);
		threadDepthImage = new std::thread(&cv::imwrite, depthFileName, depthMat, std::vector<int>());
		//Tooo slow
		//threadDepthImage = new std::thread(&PointCloudManager::saveImage, this, osgDepthImage, depthFileName);
		//osgDB::writeImageFile(*osgDepthImage, depthFileName);



		//SAVE IR IMAGE #######################################################
		k4a::image customIrImage = NULL;
		int cirImageWidth = ir_image.get_width_pixels();
		int cirImageHeight = ir_image.get_height_pixels();
		int cirImageStrideBytes = ir_image.get_stride_bytes();
		uint8_t* cirImageBuffer = ir_image.get_buffer();
		customIrImage = k4a::image::create_from_buffer(K4A_IMAGE_FORMAT_CUSTOM16,
			cirImageWidth,
			cirImageHeight,
			cirImageStrideBytes,
			cirImageBuffer,
			cirImageHeight * cirImageStrideBytes,
			NULL,
			NULL);

		//k4a::image transformedIRImage = k4a::image::create(K4A_IMAGE_FORMAT_CUSTOM16,
		//	calibration.color_camera_calibration.resolution_width,
		//	calibration.color_camera_calibration.resolution_height,
		//	calibration.color_camera_calibration.resolution_width *
		//	static_cast<int>(sizeof(uint16_t)));

		//Adpat the ir image to the size, aspect ratio and intrinsic parameters of the color cam
		//k4a::image transformedIRImage = k4a::image::create(K4A_IMAGE_FORMAT_IR16,
		//	calibration.color_camera_calibration.resolution_width,
		//	calibration.color_camera_calibration.resolution_height,
		//	calibration.color_camera_calibration.resolution_width *
		//	static_cast<int>(sizeof(uint16_t)));

		std::pair<k4a::image, k4a::image> images = m_transformation.depth_image_to_color_camera_custom(depthImage,
			customIrImage,
			K4A_TRANSFORMATION_INTERPOLATION_TYPE_NEAREST,
			0);
		//transformation.depth_image_to_color_camera_custom(ir_image, &transformedIRImage);
		uint8_t* transformed_ir_buffer = customIrImage.get_buffer();


		//Need dimension in order to allocate memory
		int irImageWidth = images.second.get_width_pixels();
		int irImageHeight = images.second.get_height_pixels();
		//Write IRImage
		//osg::Image* osgIRimage = new osg::Image();
		//osgIRimage->setImage(irImageWidth,
		//	irImageHeight,
		//	1,
		//	GL_LUMINANCE16,
		//	GL_LUMINANCE,
		//	GL_UNSIGNED_SHORT,
		//	static_cast<unsigned char*>(images.second.get_buffer()),
		//	osg::Image::AllocationMode::NO_DELETE);

		//Create a file name
		std::string irFileName;
		irFileName.append(dirSavePath);
		irFileName.append("_IRimage_");
		irFileName.append(std::to_string(j));
		irFileName.append(".png");

		std::thread* threadIrImage;
		cv::Mat irMat(irImageHeight, irImageWidth, CV_16UC1, (void*)images.second.get_buffer(), cv::Mat::AUTO_STEP);
		//TODO2: Add threads as members an avoid 'new' keyword
		threadIrImage = new std::thread(&cv::imwrite, irFileName, irMat, std::vector<int>());
		//threadIrImage = new std::thread(&PointCloudManager::saveImage, this, osgIRimage, irFileName);
		//cv::imwrite(irFileName, irMat);

		threadDepthImage->join();
		threadColorImage->join();
		threadIrImage->join();
		//}
	}

	////Calculate average depth image
	//for (size_t o = 0; o < depthAverageImage.get_size(); o += 2)
	//{
	//	uint16_t depthAverageBuffer = (uint16_t)(depth_buffer_average[o + 1] << 8 | depth_buffer_average[o]);
	//	uint16_t depthDivied = depthAverageBuffer / framesToCapture;
	//	char* splitted = reinterpret_cast<char*>(&depthDivied);
	//	depth_buffer_average[o] = splitted[0];
	//	depth_buffer_average[o + 1] = splitted[1];
	//}

	////Adpat the depth image to the size, aspect ratio and intrinsic parameters of the color cam
	//k4a::image transformedDepthAverageImage = k4a::image::create(K4A_IMAGE_FORMAT_DEPTH16,
	//	calibration.color_camera_calibration.resolution_width,
	//	calibration.color_camera_calibration.resolution_height,
	//	calibration.color_camera_calibration.resolution_width *
	//	static_cast<int>(sizeof(uint16_t)));
	//transformation.depth_image_to_color_camera(depthAverageImage, &transformedDepthAverageImage);

	//uint8_t* transformed_depth_average_buffer = transformedDepthAverageImage.get_buffer();

	////Get dimensions of the "new" transformed depth image and get the dimension in order to allocate memory
	//int transformedDepthAverageWidth = transformedDepthAverageImage.get_width_pixels();
	//int transformedDepthAverageHeight = transformedDepthAverageImage.get_height_pixels();

	////Write DepthImage to 16bit png
	//osg::Image* osgDepthAverageImage = new osg::Image();
	//osgDepthAverageImage->setImage(transformedDepthAverageWidth,
	//	transformedDepthAverageHeight,
	//	1,
	//	GL_LUMINANCE16,
	//	GL_LUMINANCE,
	//	GL_UNSIGNED_SHORT,
	//	const_cast<unsigned char*>(static_cast<const unsigned char*>(transformed_depth_average_buffer)),
	//	osg::Image::AllocationMode::NO_DELETE);

	////Create a file name
	//std::string depthAverageFileName;
	//depthAverageFileName.append(dirSavePath);
	//depthAverageFileName.append("_depthAverageImage_");
	//depthAverageFileName.append(".png");
	//osgDB::writeImageFile(*osgDepthAverageImage, depthAverageFileName);

	std::cout << "K4A images saved..." << std::endl;
	tw.endMetering();
}

void  PointCloudManager::paintKinectImageOnQuad()
{
	m_azureKinectSensor.get_capture(m_kinectCapture);
	m_kinectColorImage = m_kinectCapture->get_color_image();

	uint8_t* color_buffer = m_kinectColorImage.get_buffer();
	int colorWidth = m_kinectColorImage.get_width_pixels();
	int colorHeight = m_kinectColorImage.get_height_pixels();

	m_liveViewImage->setImage(colorWidth,
		colorHeight,
		1,
		GL_RGBA,
		GL_BGRA,
		GL_UNSIGNED_BYTE,
		const_cast<unsigned char*>(static_cast<const unsigned char*>(color_buffer)),
		osg::Image::AllocationMode::NO_DELETE);
}

void PointCloudManager::shutDownAzureKinect()
{
	if (m_azureKinectSensor != NULL)
	{
		m_azureKinectSensor.close();
	}
}

void PointCloudManager::exportPointCloud() {
	bool binary = m_configReader->getBoolFromStartupConfig("export_point_cloud_as_binary");
	std::string format = m_configReader->getStringFromStartupConfig("export_point_cloud_format");
	if (format == "PLY" || format == "ply") m_pointCloudVisualizer->exportPointCloudToPLY(binary);
	else if (format == "PCD" || format == "pcd") m_pointCloudVisualizer->exportPointCloudToPCD(binary);
	else std::cout << "Error: Specified output format is not supported" << std::endl;
}

void PointCloudManager::exportPointCloudMesh(std::string fileName) {
	m_pointCloudVisualizer->exportPointCloudMesh(fileName);
}

void PointCloudManager::exportPointCloudTexture() {
	exportPointCloudTexture(m_imageData);
}

void PointCloudManager::exportPointCloudTexture(const std::vector<ImageData*>* imageData, std::string fileName) {
	m_pointCloudVisualizer->exportPointCloudTexture(imageData, fileName);
}

bool PointCloudManager::exportRecording() {
	int processedFrames = 0;
	int totalFrames = m_pointcloudRecording->size() / m_devicesPerScan;
	ViewFrustum frustum(osg::Vec3f(0, 0, 0), osg::Vec3f(0, 0, 0), osg::Vec3f(0, 0, 0), osg::Vec3f(0, 0, 0), osg::Vec3f(0, 0, 0), osg::Vec3f(0, 0, 0), osg::Vec3f(0, 0, 0), osg::Vec3f(0, 0, 0));
	for (unsigned int frame = 0; frame < m_pointcloudRecording->size(); frame += m_devicesPerScan) {
		std::vector<ImageData*> imageData;
		for (unsigned int i = 0u; i < m_devicesPerScan; ++i) {
			osg::Matrix scanDevicePose = m_scanPipeline->getDevice(i)->getTransform();
			m_pointCloudVisualizer->processPointCloud(-1, nullptr, m_pointcloudRecording->at(frame + i), scanDevicePose, frustum, true);
			imageData.push_back(m_imageData->at(frame + i));
		}
		m_pointCloudVisualizer->polygonizePointCloudFromOctree();
		exportPointCloudMesh("Recording/Recording_Frame_" + std::to_string(frame));
		exportPointCloudTexture(&imageData, "Recording/Recording_Frame_" + std::to_string(frame));
		m_pointCloudVisualizer->clearPointCloudFull();
		m_pointCloudVisualizer->clearMarchingCubesMeshes();
		std::cout << (frame + 1) << "/" << totalFrames << " frames processed..." << std::endl;;
	}
	return true;
}

bool PointCloudManager::realtimePointCloud() {
	return m_enableRealtimePointCloud;
}

bool PointCloudManager::toggleRealtimePointCloud() {
	m_enableRealtimePointCloud = !m_enableRealtimePointCloud;
	return !m_enableRealtimePointCloud;
}

void PointCloudManager::toggleRecording() {
	while (m_isProcessing.load());
	m_isRecording = !m_isRecording;
	if (m_isRecording) {
		std::cout << "Recording started..." << std::endl;
		if (!m_pointcloudRecording) m_pointcloudRecording = new std::vector<PointcloudData*>;
		m_pointCloudVisualizer->clearPointCloudFull();
		m_pointCloudVisualizer->clearMarchingCubesMeshes();
		m_writeTSDF = false;
		m_enableRealtimePointCloud = true;
	}
	else {
		std::cout << "Recording stopped. Started exporting." << std::endl;
		m_enableRealtimePointCloud = false;

		exportRecording();

		// Clean up
		for (PointcloudData*& pcd : *m_pointcloudRecording) {
			delete[] pcd->vertices();
			delete[] pcd->textureCoordinates();
			delete pcd;
		}
		delete m_pointcloudRecording;
		m_pointcloudRecording = nullptr;
		m_writeTSDF = m_configReader->getBoolFromStartupConfig("write_tsdf");
		m_enableRealtimePointCloud = m_configReader->getBoolFromStartupConfig("enable_realtime_pointcloud");
	}
}

void PointCloudManager::drawDebugDisplay(const osg::Matrix trackerMatrix) {
	if (m_scanDeviceInitLock.load()) return;
	//m_pointCloudDisplay->drawDebug(trackerMatrix, g_calculatedCamPose, vector);
	bool expected;
	while (g_cameraPosesLock.compare_exchange_weak(expected = false, true));
	//m_pointCloudDisplay->drawDebug(g_cameraPoses);
	m_pointCloudDisplay->drawDebug(trackerMatrix, g_cameraPoses, m_patternToTrackerTrans, m_patternToTrackerRot);
	g_cameraPosesLock.store(false);
}

//osg::Matrix PointCloudManager::findCorrespondingTrackerMatrixToTimestamp(double depthFrameTimeStamp)
//{
//	//Lock
//	double timestamp = std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::system_clock::now().time_since_epoch()).count();
//	//std::cout << "------------- For While: " << std::fixed << timestamp << std::endl;
//	if (m_timedCircularTrackerMatrixBuffer_Lock == nullptr) return osg::Matrix();
//	
//	//while (m_timedCircularTrackerMatrixBuffer_Lock->load());
//	//m_timedCircularTrackerMatrixBuffer_Lock->store(true);
//	bool expected = false;
//	const bool desired = true;
//	while (!m_timedCircularTrackerMatrixBuffer_Lock->compare_exchange_weak(expected, desired)) std::this_thread::sleep_for(std::chrono::nanoseconds(1));
//
//
//	timestamp = std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::system_clock::now().time_since_epoch()).count();
//	//std::cout << "------------- After While: " << std::fixed << timestamp << std::endl;
//	
//
//	osg::Matrix matrix(
//		0.0, 0.1, 0.2, 0.3,
//		1.0, 1.1, 1.2, 1.3,
//		2.0, 2.1, 2.2, 2.3,
//		3.0, 3.1, 3.2, 3.3);
//
//	//DEBUG: just for printing out all values in the circular buffer
//	/*for (boost::circular_buffer<TimedOpenVRMatrix>::iterator iter = m_timedCircularTrackerMatrixBuffer->begin();
//		iter != m_timedCircularTrackerMatrixBuffer->end();
//		++iter)
//	{
//		std::cout << "iter->timestamp " << iter->timestamp << std::endl;
//
//		Matrix34 pose = iter->matrix;
//		matrix.set(
//			pose.m[0][0], pose.m[1][0], pose.m[2][0], 0.0,
//			pose.m[0][1], pose.m[1][1], pose.m[2][1], 0.0,
//			pose.m[0][2], pose.m[1][2], pose.m[2][2], 0.0,
//			pose.m[0][3], pose.m[1][3], pose.m[2][3], 1.0f);
//
//		std::cout << "iter->matrix " << matrix << std::endl;
//	}*/
//
//	int i = 0;
//	for (boost::circular_buffer<TimedOpenVRMatrix>::iterator iter = m_timedCircularTrackerMatrixBuffer->begin();
//		iter != m_timedCircularTrackerMatrixBuffer->end();
//		++iter)
//	{
//		if (iter->timestamp > depthFrameTimeStamp - 100)
//		{
//			//std::cout << "!!!!!!!!!!!!!!!!!!!!!!!!!!depthFrameTimeStamp: " << depthFrameTimeStamp << std::endl;
//			//std::cout << "!!!!!!Der Timestamp von iter->timestamp isses: " << iter->timestamp << std::endl;
//			//--iter;
//			//std::cout << "Der Timestamp kommt danach: " << iter->timestamp << std::endl;
//			//--iter;
//			Matrix34 pose = iter->matrix;
//
//			m_timedCircularTrackerMatrixBuffer_Lock->store(false);
//
//			matrix.set(
//				pose.m[0][0], pose.m[1][0], pose.m[2][0], 0.0,
//				pose.m[0][1], pose.m[1][1], pose.m[2][1], 0.0,
//				pose.m[0][2], pose.m[1][2], pose.m[2][2], 0.0,
//				pose.m[0][3], pose.m[1][3], pose.m[2][3], 1.0f);
//
//			//std::cout << "!!!!!!!!!!!!!!!!!!!FINAL: iter->matrix " << matrix << std::endl;
//			//osg::Matrix matrix2;
//			//matrix2.makeTranslate(0.0, 0.0, 0.0);
//			//return matrix2;
//			//std::cout << "########## i:" << i << std::endl;
//			return matrix;
//		}
//		i++;
//	}
//	i++;
//
//
//
//	/*for (TimedOpenVRMatrix t : *m_timedCircularTrackerMatrixBuffer) {
//		std::cout << "Timestamp:" << m_timedCircularTrackerMatrixBuffer << t.timestamp << std::endl;
//		if (t.timestamp < depthFrameTimeStamp) {
//
//		}
//	}*/
//
//	//Matrix34 pose = m_timedCircularTrackerMatrixBuffer->back().matrix;
//	//m_timedCircularTrackerMatrixBuffer_Lock->store(false);
//
//
//
//	//osg::Matrix matrix2;
//	//matrix2.makeTranslate(0.0, 0.0, 0.0);
//	//return matrix2;
//
//	return matrix;
//}

osg::Matrix PointCloudManager::findCorrespondingTrackerMatrixToTimestamp(unsigned long long depthFrameTimeStamp) {

	if (m_timedCircularTrackerMatrixBuffer_Lock == nullptr) return osg::Matrix();

	bool expected = false;
	const bool desired = true;
	while (!m_timedCircularTrackerMatrixBuffer_Lock->compare_exchange_weak(expected, desired)) std::this_thread::sleep_for(std::chrono::nanoseconds(1));

	osg::Matrix matrix(
		0.0, 0.1, 0.2, 0.3,
		1.0, 1.1, 1.2, 1.3,
		2.0, 2.1, 2.2, 2.3,
		3.0, 3.1, 3.2, 3.3);

	for (boost::circular_buffer<TimedOpenVRMatrix>::iterator iter = m_timedCircularTrackerMatrixBuffer->end() - 1; iter != m_timedCircularTrackerMatrixBuffer->begin(); iter--) {
		if (iter->timestamp < depthFrameTimeStamp) {
			if (iter == m_timedCircularTrackerMatrixBuffer->end() - 1) {
				Matrix34 pose0 = iter->matrix;

				m_timedCircularTrackerMatrixBuffer_Lock->store(false);
				matrix.set(
					pose0.m[0][0], pose0.m[1][0], pose0.m[2][0], 0.0,
					pose0.m[0][1], pose0.m[1][1], pose0.m[2][1], 0.0,
					pose0.m[0][2], pose0.m[1][2], pose0.m[2][2], 0.0,
					pose0.m[0][3], pose0.m[1][3], pose0.m[2][3], 1.0f
				);

				return matrix;
			}
			else {
				Matrix34 pose0 = iter->matrix;
				Matrix34 pose1 = (iter + 1)->matrix;

				unsigned long long t0 = iter->timestamp;
				unsigned long long t1 = (iter + 1)->timestamp;

				m_timedCircularTrackerMatrixBuffer_Lock->store(false);

				double b = (float)(depthFrameTimeStamp - t0) / (float)(t1 - t0);
				double a = 1.f - b;

				matrix.set(
					pose0.m[0][0] * a + pose1.m[0][0] * b, pose0.m[1][0] * a + pose1.m[1][0] * b, pose0.m[2][0] * a + pose1.m[2][0] * b, 0.0,
					pose0.m[0][1] * a + pose1.m[0][1] * b, pose0.m[1][1] * a + pose1.m[1][1] * b, pose0.m[2][1] * a + pose1.m[2][1] * b, 0.0,
					pose0.m[0][2] * a + pose1.m[0][2] * b, pose0.m[1][2] * a + pose1.m[1][2] * b, pose0.m[2][2] * a + pose1.m[2][2] * b, 0.0,
					pose0.m[0][3] * a + pose1.m[0][3] * b, pose0.m[1][3] * a + pose1.m[1][3] * b, pose0.m[2][3] * a + pose1.m[2][3] * b, 1.0f
				);

				return matrix;
			}
		}
	}
	return matrix;
}

//bool PointCloudManager::startProcessingThread(float updateRate) {
//
//}
//
//bool PointCloudManager::closeProcessingThread() {
//
//}

void PointCloudManager::toggleMarchingCubesDisplay() {
	m_pointCloudVisualizer->toggleMarchingCubesDisplay();
}

void PointCloudManager::setPointCloudDisplay(bool displayState) {
	m_pointCloudVisualizer->setPointCloudDisplay(displayState);
}

void PointCloudManager::setOctreeDisplay(bool displayState) {
	m_pointCloudDisplay->setOctreeDisplay(displayState);
}

void PointCloudManager::setTSDFDisplay(bool displayState) {
	m_pointCloudDisplay->setTSDFDisplay(displayState);
}

void PointCloudManager::setDDADisplay(bool displayState) {
	m_pointCloudDisplay->setDDADisplay(displayState);
}

void PointCloudManager::setPolygonizeDisplay(bool displayState) {
	m_pointCloudVisualizer->setMarchingCubesDisplay(displayState);
}

void PointCloudManager::joinTelepresenceThread()
{

	m_isTelepresenceLoopActive.store(false);

	m_telepresenceThread.~thread();
	std::this_thread::sleep_for(std::chrono::milliseconds(100));
	m_azureKinectSensor.close();
}

void PointCloudManager::lockSceneGraph()
{
	m_isSceneGraphLockedForRendering->store(true);
}

void PointCloudManager::unlockSceneGraph()
{
	m_isSceneGraphLockedForRendering->store(false);
	m_cv.notify_all();
}

void PointCloudManager::saveImage(osg::Image* image, std::string path) {

	osgDB::writeImageFile(*image, path);
}