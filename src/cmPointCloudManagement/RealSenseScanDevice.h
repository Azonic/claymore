#pragma once

#include "ScanDevice.h"
#include <rs.hpp>
//#include <librealsense2/rs.hpp>

class RealSenseScanDevice : public ScanDevice {
private:
	rs2::device m_rsDevice;
	rs2::config m_rsConfig;
	rs2::pipeline m_rsPipe;
	rs2::pointcloud m_rsPc;
	//Data from last scan
	bool m_scanDataAvailable;
	rs2::frameset m_rsFrames;
	rs2::points m_rsPoints;
	rs2::decimation_filter* m_rs_dec_filter;
	rs2::spatial_filter* m_rs_spat_filter;

public:
	RealSenseScanDevice() = delete;
	RealSenseScanDevice(rs2::device& device, rs2::context& context, const unsigned int& colorWidth, const unsigned int& colorHeight, const unsigned int& depthWidth, const unsigned int& depthHeight, const unsigned int& fps, rs2_format colorFormat, rs2_format dpethFormat, PoseDriver* poseDriver, const unsigned int& pointCloudWidth, const unsigned int& pointCloudHeight, cv::Mat* cameraMatrix, cv::Mat* distortionCoefficients);
	~RealSenseScanDevice();
	virtual StartDeviceResult start() override;
	virtual StopDeviceResult stop() override;
	virtual ScanResult scan(const unsigned int ms_timeout = 1000u) override;
	virtual PointcloudCalculationResult calculatePointcloud() override;
	virtual void releaseScan() override;
	void loadConfig(rs2::config& config, rs2_format colorFormat, rs2_format dpethFormat);
private:
	inline void reloadColorFormat(rs2_format rs2Format);
	inline void reloadDepthFormat(rs2_format rs2Format);
};