#pragma once

#include <k4a/k4a.h>
#include <k4a/k4a.hpp>

#include <Eigen/Dense>

#include <osg/Node>
#include <osg/Group>
#include <osg/MatrixTransform>
#include <osg/Image>
#include "../cmOpenMeshBinding/OpenMeshGeometry.h"

#include <OpenMesh/Tools/Smoother/JacobiLaplaceSmootherT.hh>

#include <vector>
#include <atomic>
#include <thread>
#include <mutex>

typedef OpenMesh::OSG_TriMesh_BindableArrayKernelT myMesh;
typedef OpenMesh::Smoother::JacobiLaplaceSmootherT<myMesh> mySmoother;
typedef myMesh::Color Color;

class LiveMesh
{



public:
	enum LiveMeshStates
	{
		NEW_DATA_AVAILABLE,
		PROCESSING,
		READY_TO_RENDER,
		IN_SCENE_GRAPH,
		RENDERED
	};

	LiveMesh();
	LiveMesh(int id);

	void initialize(k4a::device* k4aDevice, osg::ref_ptr<osg::Group> pointCloudGroup, k4a_device_configuration_t* config, ConfigReader* configReader);
	void calcImageSize(k4a::capture* kinectCapture);
	void useProjectionMapping(bool onOrOff);

	void startRendering();
	void setKinectCapture(k4a::capture* kinectCapture);
	void setMeshInactive();
	void setMeshActive();
	void setStateToRendered();



	inline LiveMeshStates getCurrentState()
	{
		return m_currentLiveMeshState;
	}

	std::chrono::microseconds getKinectCaptureTimestamp();

	int m_id;





private:

	std::mutex m_mutex;

	void renderFrame();
	
	LiveMeshStates m_currentLiveMeshState;

	k4a::device* m_k4aDevice;

	k4a::capture* m_kinectCapture;

	k4a::image					m_colorMap;
	k4a::image					m_depthMap;
	k4a::image					m_pointcloudImage;

	k4a::image                  m_depthMapTransformed;
	k4a::image                  m_colorMapTransformed;

	osg::Vec4Array*				m_colorArrayA;
	osg::Vec4Array*				m_colorArrayB;

	osg::ref_ptr<osg::Vec2Array> m_texcoords;
	std::vector<k4a_float2_t>	 m_texcoordsDepthMapTemplate;

	//osg::Vec4Array*				m_colorArrayCurr;
	osg::Vec4Array*				m_colorArrayCurrNotBinded;


	int							m_depthMapWidth;
	int							m_depthMapHeight;
	int							m_colorMapWidth;
	int							m_colorMapHeight;
	int							m_colorMapTransformedWidth;
	int							m_colorMapTransformedHeight;
	int							m_numVerts;

	k4a::calibration			m_calibration;
	k4a::transformation			m_transformation;
	k4a_device_configuration_t*	m_config;

	osg::OpenMeshGeometry*		m_clothA;
	osg::OpenMeshGeometry*		m_clothB;


	osg::Geode*					m_telePresenceGeodeA;
	osg::Geode*					m_telePresenceGeodeB;

	//osg::Vec3Array*				m_pointcloudA;
	//osg::Vec3Array*				m_pointcloudB;
	//osg::Vec3Array*				m_currpointcloud;

	osg::Vec3Array*				m_vertexArrayA;
	osg::Vec3Array*				m_vertexArrayB;

	std::vector<Color>			m_colorList;
	std::vector<Color>			m_colorListScaledDown;


	std::vector<OpenMesh::VertexHandle> m_partsOfTriangle;

	ConfigReader*				m_configReader;

	osg::ref_ptr<osg::Group>	m_pointCloudGroup;
	osg::ref_ptr<osg::Group>	m_GroupRedundance;

	int							m_backgroundClippingDistance;

	std::vector<bool>			m_nullVector;

	bool						m_firstFrame;

	osg::ref_ptr<osg::StateSet> m_stateSetShader;

	unsigned int m_numberOfLoop;



	void convertPointCloudImageToVectorArray(k4a::image point_cloud);
	void initCloth();

	myMesh* m_currMesh;

	void smooth(int iterations);

	bool m_renderA;


	OpenMesh::PropertyT<GLuint> m_indices;
	OpenMesh::FPropHandleT<GLuint> m_indicesPropH;

	bool m_useProjectionMapping;
	osg::ref_ptr<osg::Image> m_image0;



	/**
	 * converts a k4a::image for colors to a vector<Color>
	 * 
	 * \param colorMap input color map
	 */
	void calcColorList(k4a::image colorMap);


	/**
	 * scales inputList down to Mesh size.
	 * 
	 * \param inputList the large ColorList
	 * \param outputList the pointer to the target list
	 * \param sourceWidth the inputList's width
	 * \param sourceHeight the inputList's height
	 */
	void scaleDownColorList(int sourceWidth, int sourceHeight);


	void transformDepthImage(k4a::image* input, k4a::image* output);
	void transformColorImage(k4a::image* input, k4a::image* output);


	std::vector<Color> m_colorLines;


	/**
	 * Deletes all edges from the mesh longer than maxLength.
	 * 
	 * \param maxLength maximum edge length
	 * \param mesh mesh where the deletion should be performed
	 */
	void deleteLongEdges(float maxLength, myMesh* mesh);


	void renderKinectColorImage(k4a::image* m_colorMap);
};