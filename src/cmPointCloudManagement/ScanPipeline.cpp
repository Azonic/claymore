#include "ScanPipeline.h"
#include <iostream>

ScanPipeline::ScanPipeline() : m_selectedDevice{ 0 } {
	m_devices = new std::vector<ScanDevice*>();
}

ScanPipeline::~ScanPipeline() {
	for (ScanDevice* device : *m_devices) delete device;
	delete m_devices;
}

void ScanPipeline::addDevice(ScanDevice* device) {
	m_devices->push_back(device);
}

ScanDevice* ScanPipeline::getDevice(const unsigned int deviceIndex) {
	int index = clampDeviceIndex(deviceIndex);
	if (index < 0) return nullptr;
	return m_devices->at(index);
}

ScanDevice* ScanPipeline::getDevice(const std::string serial) {
	for (ScanDevice* device : *m_devices) {
		if (device->getSerial() == serial) return device;
	}
	return nullptr;
}

int ScanPipeline::getDeviceIndex(ScanDevice* device) {
	for (unsigned int d = 0u; d < m_devices->size(); ++d) {
		if (m_devices->at(d) == device) return d;
	}
	return -1;
}

bool ScanPipeline::hasDevice(const std::string serial) {
	for (ScanDevice* device : *m_devices) {
		if (device->getSerial() == serial) return true;
	}
	return false;
}

ScanDevice* ScanPipeline::getSelectedDevice() {
	if (m_selectedDevice < 0) return nullptr;
	return m_devices->at(m_selectedDevice);
}

int ScanPipeline::getSelectedDeviceIndex() const {
	return m_selectedDevice;
}

unsigned int ScanPipeline::getDeviceCount(){
	return m_devices->size();
}

void ScanPipeline::removeDevice(const unsigned int deviceIndex) {
	int index = clampDeviceIndex(deviceIndex);
	if (index < 0) return;
	m_devices->erase(m_devices->begin() + index);
}

void ScanPipeline::clearDevices() {
	m_devices->clear();
}

void ScanPipeline::selectPreviousDevice() {
	selectDevice(m_selectedDevice == 0 ? m_devices->size() - 1 : m_selectedDevice - 1);
}

void ScanPipeline::selectPreviousDevice(const unsigned int decrement) {
	if (m_devices->empty()) m_selectedDevice = -1;
	else {
		unsigned int dec = decrement % m_devices->size();
		int diff = m_selectedDevice - dec;
		m_selectedDevice = diff < 0 ? m_devices->size() + diff : diff;
	}
}

void ScanPipeline::selectNextDevice() {
	selectDevice(m_selectedDevice + 1);
}

void ScanPipeline::selectNextDevice(const unsigned int increment) {
	selectDevice(m_selectedDevice + increment);
}

void ScanPipeline::startAllDevices() {
	for (ScanDevice* device : *m_devices) device->start();
}

ScanPipeline::ScanResult ScanPipeline::scan(ColorData** colorData, DepthData** depthData, unsigned int ms_timeout) {
	return scan(m_selectedDevice, colorData, depthData, ms_timeout);
}

ScanPipeline::ScanResult ScanPipeline::scan(const unsigned int deviceIndex, ColorData** colorData, DepthData** depthData, unsigned int ms_timeout) {
	int index = clampDeviceIndex(deviceIndex);
	if (index < 0) return ScanResult::EMPTY_DEVICE_LIST;
	ScanDevice& device = *m_devices->at(index);
	ScanDevice::ScanResult scanResult = device.scan(ms_timeout);
	if (scanResult != ScanDevice::ScanResult::SUCCESS) std::cout << "## ScanDevice::scan Error: " << (int) scanResult << " ##" << std::endl;
	switch (scanResult) {
	case::ScanDevice::ScanResult::SUCCESS:
		break;
	case ScanDevice::ScanResult::TIMEOUT:
		return ScanResult::TIMEOUT;
	case ScanDevice::ScanResult::DEVICE_IS_ALREADY_SCANNING:
		return ScanResult::DEVICE_IS_ALREADY_SCANNING;
	default:
		return ScanResult::FAILURE;
	}
	*colorData = device.getColorData();
	*depthData = device.getDepthData();
	return ScanResult::SUCCESS;
}

std::vector<ScanPipeline::ScanResult> ScanPipeline::scan(const unsigned int deviceRange, std::vector<ColorData*>* colorData, std::vector<DepthData*>* depthData, const unsigned int ms_timeout) {
	return scan(m_selectedDevice, deviceRange, colorData, depthData, ms_timeout);
}

std::vector<ScanPipeline::ScanResult> ScanPipeline::scan(const unsigned int deviceIndex, const unsigned int deviceRange, std::vector<ColorData*>* colorData, std::vector<DepthData*>* depthData, const unsigned int ms_timeout) {
	std::vector<ScanResult> results;
	results.reserve(deviceRange);
	for (unsigned int i = 0; i < deviceRange; i++) {
		ColorData* colorPtr = nullptr;
		DepthData* depthPtr = nullptr;
		results.push_back(i < m_devices->size() ? scan(deviceIndex + i, &colorPtr, &depthPtr, ms_timeout) : ScanResult::DEVICE_IS_ALREADY_SCANNING);
		colorData->push_back(colorPtr);
		depthData->push_back(depthPtr);
	}
	return results;
}

std::future<ScanPipeline::ScanResult> ScanPipeline::scanAsync(ColorData** colorData, DepthData** depthData, const unsigned int ms_timeout) {
	return scanAsync(m_selectedDevice, colorData, depthData, ms_timeout);
}

std::future<ScanPipeline::ScanResult> ScanPipeline::scanAsync(const unsigned int deviceIndex, ColorData** colorData, DepthData** depthData, unsigned int ms_timeout) {
	if (m_devices->empty()) {
		std::promise<ScanResult> promise;
		promise.set_value(ScanResult::EMPTY_DEVICE_LIST);
		return promise.get_future();
	}
	ScanPipeline::ScanResult (ScanPipeline::* scanFunc)(const unsigned int, ColorData**, DepthData**, unsigned int) = &ScanPipeline::scan;	
	return std::async(std::launch::async, scanFunc, this, deviceIndex, colorData, depthData, ms_timeout);
}

std::vector<std::future<ScanPipeline::ScanResult>> ScanPipeline::scanAsync(const unsigned int deviceRange, std::vector<ColorData*>* colorData, std::vector<DepthData*>* depthData, const unsigned int ms_timeout) {
	return scanAsync(m_selectedDevice, deviceRange, colorData, depthData, ms_timeout);
}

std::vector<std::future<ScanPipeline::ScanResult>> ScanPipeline::scanAsync(const unsigned int deviceIndex, const unsigned int deviceRange, std::vector<ColorData*>* colorData, std::vector<DepthData*>* depthData, const unsigned int ms_timeout) {
	std::vector<std::future<ScanResult>> futures;
	futures.reserve(deviceRange);
	for (unsigned int i = 0; i < deviceRange; i++) futures.push_back(scanAsync(deviceIndex, &colorData->at(i), &depthData->at(i), ms_timeout));
	return futures;
}

ScanPipeline::PointcloudCalculationResult ScanPipeline::calculatePointcloud(PointcloudData** pointcloudData, const bool downsampled) {
	return calculatePointcloud(m_selectedDevice, pointcloudData, downsampled);
}

ScanPipeline::PointcloudCalculationResult ScanPipeline::calculatePointcloud(const unsigned int deviceIndex, PointcloudData** pointcloudData, const bool downsampled) {
	int index = clampDeviceIndex(deviceIndex);
	if (index < 0) return PointcloudCalculationResult::EMPTY_DEVICE_LIST;
	ScanDevice& device = *m_devices->at(index);
	ScanDevice::PointcloudCalculationResult result = device.calculatePointcloud();
	if (ScanDevice::PointcloudCalculationResult::SUCCESS != result) {
		std::cout << "## ScanDevice::calculatePointcloud Error: " << (int) result << " ##" << std::endl;
		return PointcloudCalculationResult::FAILURE;
	}
	if (downsampled) {
		auto downsamplingResult = device.downsamplePointCloud();
		if (downsamplingResult != ScanDevice::DownsamplePointCloudResult::SUCCESS) std::cout << "## ScanDevice::downsamplePointcloud Error: " << (int) result << " ## >> original pointCloud returned instead." << std::endl;
	}
	*pointcloudData = device.getPointcloudData(downsampled);
	return PointcloudCalculationResult::SUCCESS;
}

osg::Matrix ScanPipeline::getTransform() {
	return getTransform(m_selectedDevice);
}

osg::Matrix ScanPipeline::getTransform(const unsigned int deviceIndex) {
	ScanDevice* device = getDevice(deviceIndex);
	if (device) return device->getTransform();
	return osg::Matrix();
}

void ScanPipeline::releaseScans() {
	for (ScanDevice* device : *m_devices) device->releaseScan();
}

void ScanPipeline::selectDevice(const unsigned int deviceIndex) {
	m_selectedDevice = clampDeviceIndex(deviceIndex);
}