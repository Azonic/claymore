#pragma once

#include "Octree.h"
#include <thread>
#include <algorithm>
#include <stdexcept>

//#define LOCK_LOOP_SLEEP_TIME std::chrono::nanoseconds(1)

template<class T>
Octree<T>::Octree(T value, const unsigned char maxDepth, const unsigned char depth)
	: m_depth{depth}, 
	m_maxDepth{maxDepth}, 
	m_value{value}, 
	m_weight{0}, 
	m_children{nullptr}, 
	m_selected{false}, 
	//m_semaphore(false),
	m_lock(0u)
#if DEBUG_SPLITS_PER_OCTREE == 1
	,m_split(0)
#endif
{ 
	//std::cout << "Constructor" << std::endl;
}

template<class T>
Octree<T>::~Octree() {
	clear<true>();
	//std::cout << "Default desctructor" << std::endl;
}

template<class T>
Octree<T>::Octree(Octree<T>&& other) :
	m_depth{other.m_depth},
	m_maxDepth{other.m_maxDepth},
	m_value{other.m_value},
	m_weight{other.m_weight},
	m_children{other.m_children},
	m_selected{other.m_selected},
	//m_semaphore(other.m_semaphore.load()),
	m_lock(other.m_lock.load())
#if DEBUG_SPLITS_PER_OCTREE == 1
	,m_split(other.m_split.load())
#endif
{ 
	//std::cout << "Move constructor" << std::endl;
}

template<class T>
Octree<T>::Octree(const Octree<T>& other) :
	m_depth{other.m_depth},
	m_maxDepth{other.m_maxDepth},
	m_value{other.m_value},
	m_weight{other.m_weight},
	m_children{other.m_children},
	m_selected{other.m_selected},
	//m_semaphore(other.m_semaphore.load()),
	m_lock(other.m_lock.load())
#if DEBUG_SPLITS_PER_OCTREE == 1
	,m_split(other.m_split.load())
#endif
{ 
	//std::cout << "Copy constructor" << std::endl;
}

template<class T>
Octree<T>& Octree<T>::operator=(const Octree<T>& other)
{
	copyMembers(other);
}

template<class T>
void Octree<T>::copyMembers(const Octree<T>& other) //Background is that atomic<bool> has no copyCTOR in lead to Error C2280. Therefore, the Octree CopyCTOR must not be called
{
	m_depth = other.m_depth;
	m_maxDepth = other.m_maxDepth;
	m_value = other.m_value;
	m_weight = other.m_weight;
	m_children = other.m_children;
	m_selected = other.m_selected;
	//m_semaphore.store(other.m_semaphore.load());
	m_lock.store(other.m_lock.load());
#if DEBUG_SPLITS_PER_OCTREE == 1
	m_split.store(other.m_split.load());
#endif
}

template<class T>
template<bool threadSafe>
void Octree<T>::clear(bool retainValue) {
	if (threadSafe) writeLock();
	if (m_children) {
		if (m_depth == m_maxDepth) { //OctreeData
			delete (OctreeData*) m_children;
		} else {
			if (retainValue) { //TODO 5: retain value should ideally average all the values of all of its following octrees
				T value = T();
				for (unsigned char c = 0; c < 8u; c++) {
					Octree<T>& child = ((Octree<T>*) m_children)[c];
					value += child.getValue<threadSafe>();
				}
				m_value = value / 8; 
			}
			delete[](Octree<T>*) m_children;
		}
		m_children = nullptr;
	}
#if DEBUG_SPLITS_PER_OCTREE == 1
	m_split.store(0);
#endif
	if (threadSafe) writeUnlock();
}

template<class T>
unsigned char Octree<T>::getDepth() const {
	return m_depth;
}

template<class T>
unsigned char Octree<T>::getMaxDepth() const {
	return m_maxDepth;
}

template<class T>
bool Octree<T>::isMaxDepth() const {
	return m_depth == m_maxDepth;
}

template<class T>
template<bool threadSafe>
T Octree<T>::getValue() {
	if (threadSafe) readLock();
	T value = m_value;
	if (threadSafe) readUnlock();
	return value;
}

template<class T>
template<bool threadSafe>
void Octree<T>::setWeight(unsigned short weight) {
	if (threadSafe) writeLock();
	m_weight = std::min<unsigned short>(weight, USHRT_MAX);
	if (threadSafe) writeUnlock();
}

template<class T>
template<bool threadSafe>
void Octree<T>::increaseWeight() {
	if (threadSafe) writeLock();
	if (m_weight < USHRT_MAX) m_weight++;
	if (threadSafe) writeUnlock();
}

template<class T>
template<bool threadSafe>
unsigned short Octree<T>::getWeight()  {
	if (threadSafe) readLock();
	unsigned short weight = m_weight;
	if (threadSafe) readUnlock();
	return weight;
}

template<class T>
template<bool threadSafe>
void Octree<T>::setSelected(bool state, bool recursive) {
	if (threadSafe) writeLock();
	m_selected = state;
	Octree<T>* children = (Octree<T>*) m_children;
	if (threadSafe) writeUnlock();
	if (recursive && hasChildren()) {
		for (int i = 0; i < 8; i++) children[i].setSelected(state, recursive);
	}
}

template<class T>
template<bool threadSafe>
bool Octree<T>::isSelected() {
	if (threadSafe) readLock();
	bool selected = m_selected;
	if (threadSafe) readUnlock();
	return selected;
}

template<class T>
template<bool threadSafe>
void Octree<T>::select(bool recursive) {
	if (threadSafe) writeLock();
	m_selected = true;
	Octree<T>* children = (Octree<T>*) m_children;
	if (threadSafe) writeUnlock();
	if (recursive && hasChildren()) {
		for (int i = 0; i < 8; i++) children[i].select<threadSafe>(recursive);
	}
}

template<class T>
template<bool threadSafe>
void Octree<T>::deselect(bool recursive) {
	if (threadSafe) writeLock();
	m_selected = false;
	Octree<T>* children = (Octree<T>*) m_children;
	if (threadSafe) writeUnlock();
	if (recursive && hasChildren()) {
		for (int i = 0; i < 8; i++) children[i].deselect<threadSafe>(recursive);
	}
}

template<class T>
template<bool threadSafe>
T Octree<T>::getHierarchyValue(const float x, const float y, const float z) {
	Octree<T>* octree = getOctree<threadSafe>(x, y, z, false);
	if (octree == nullptr) throw std::out_of_range("The specified point does not lie inside the volume.");
	return octree->getValue<threadSafe>();
}

template<class T>
template<bool threadSafe>
T Octree<T>::getHierarchyValue(const osg::Vec3f& xyz) {
	return getHierarchyValue<threadSafe>(xyz.x(), xyz.y(), xyz.z());
}

template<class T>
template<bool threadSafe>
bool Octree<T>::hasMaxDepthValue(const float x, const float y, const float z) {
	Octree<T>* octree = getOctree<threadSafe>(x, y, z, false);
	if (octree == nullptr) throw std::out_of_range("The specified point does not lie inside the volume.");
	return octree->m_depth == m_maxDepth;
}

template<class T>
template<bool threadSafe>
bool Octree<T>::hasMaxDepthValue(const osg::Vec3f& xyz) {
	return hasMaxDepthValue<threadSafe>(xyz.x(), xyz.y(), xyz.z());
}

template<class T>
template<bool threadSafe>
void Octree<T>::setValue(const T value) {
	if (threadSafe) writeLock();
	m_value = value;
	if (threadSafe) writeUnlock();
}

template<class T>
template<bool threadSafe>
Octree<T>* Octree<T>::setValueInHierarchy(const T value, const float x, const float y, const float z, bool compareValues) {
	//getOctree(x, y, z, true)->setValue(value);
	Octree<T>* octree = getOctree<threadSafe>(x, y, z, false);
	if (octree == nullptr) return nullptr;
	if (!compareValues || octree->getValue<threadSafe>() != value) {
		octree = octree->getOctree<threadSafe>(x, y, z, true);
		octree->setValue<threadSafe>(value);
	}
	return octree; // NOTE: This is not necessarily a leaf octree, most likely it will be though. If an octree of higher depth already stored the given value, there is no need to split it again. If this is the case the lower grade octree will be returned.
}

template<class T>
template<bool threadSafe>
Octree<T>* Octree<T>::setValueInHierarchy(const T value, const osg::Vec3f& xyz, bool compareValues) {
	return setValueInHierarchy<threadSafe>(value, xyz.x(), xyz.y(), xyz.z(), compareValues);
}

template<class T>
template<bool threadSafe>
Octree<T>* Octree<T>::getOctree(const unsigned char childIndex) {
	Octree<T>* octree = nullptr;
	if (threadSafe) readLock();
	if (m_children && m_depth < m_maxDepth) octree = ((Octree<T>*) m_children) + childIndex;
	if (threadSafe) readUnlock();
	return octree;
}

template<class T>
template<bool threadSafe>
Octree<T>* Octree<T>::getOctree(const float x, const float y, const float z, const bool forceMaxDepth) {
	return getOctree<threadSafe>(x, y, z, nullptr, forceMaxDepth);
}

template<class T>
template<bool threadSafe>
Octree<T>* Octree<T>::getOctree(const float x, const float y, const float z, Octree<T>** parent, const bool forceMaxDepth) {
	if (x < 0 || x >= 1 ||
		y < 0 || y >= 1 ||
		z < 0 || z >= 1) return nullptr;
	unsigned int max = (1 << m_maxDepth); // 2 ^ m_maxDepth !!! Note: Do not subtract 1 from the maximum; Since we use an integer cast to cut off the floating point digits the max-th element will never be accessed: e.g. max = 512, 0.999 * 512 = 511.488 -> 511
	//Multiply the coords to get an absolute value in the local coordinate system of our octree volume
	return getOctreeLocal<threadSafe>(x * max, y * max, z * max, parent, forceMaxDepth);
}

template<class T>
template<bool threadSafe>
Octree<T>* Octree<T>::getOctree(const osg::Vec3f& xyz, const bool forceMaxDepth) {
	return getOctree<threadSafe>(xyz.x(), xyz.y(), xyz.z(), forceMaxDepth);
}

template<class T>
template<bool threadSafe>
Octree<T>* Octree<T>::getOctree(const osg::Vec3f& xyz, Octree<T>** parent, const bool forceMaxDepth) {
	return getOctree<threadSafe>(xyz.x(), xyz.y(), xyz.z(), parent, forceMaxDepth);
}

template<class T>
template<bool threadSafe>
Octree<T>* Octree<T>::getOctreeLocal(const unsigned int x, const unsigned int y, const unsigned int z, const bool forceMaxDepth) {
	return getOctreeLocal<threadSafe>(x, y, z, nullptr, forceMaxDepth);
}

template<class T>
template<bool threadSafe>
Octree<T>* Octree<T>::getOctreeLocal(const unsigned int x, const unsigned int y, const unsigned int z, Octree<T>** parent, const bool forceMaxDepth) {
	if (m_depth == m_maxDepth) return this;
	if (threadSafe) readLock();
	if (!m_children) {
		if (threadSafe) readUnlock();
		if (!forceMaxDepth) return this;
		split<threadSafe>();
		if (threadSafe) readLock();
	}
	Octree<T>* children = (Octree<T>*) m_children;
	if (threadSafe) readUnlock();
	if (parent) *parent = this;
	Octree<T>* octree = children[calculateChildIndex(x, y, z)].getOctreeLocal<threadSafe>(x, y, z, forceMaxDepth);
	return octree;
}

template<class T>
template<bool threadSafe>
bool Octree<T>::hasChildren() {
	if (m_depth == m_maxDepth) return false;
	if (threadSafe) readLock();
	bool children = m_children;
	if (threadSafe) readUnlock();
	return children;
}

template<class T>
template<bool threadSafe>
bool Octree<T>::split(T value) {
	// Dont split if children already exist or the max depth is already reached; 
	// If this condition is true, it does not matter whether another thread has currently acquired the lock at this point because the octree must not be split in this case. Testing this without thread synchronisation can massively increase the performance.
	if (m_depth == m_maxDepth || hasChildren<threadSafe>()) return false;	
	if (threadSafe) {
		// However, if the condition was not met, there is still a small chance that the result changed between the if-statement and the acquisition of the semaphore due to another thread simultaneously exectuing this function on the same octree. 
		// Once the lock is acquired, the result will not be affected anymore by any other running thread, so the result of the conditional statement is valid. In this case the octree may be split.
		// This is necessary since wrong handling of multiple threads calling this function can lead to a massive memory leak.
		writeLock();
		if (m_children) {
			writeUnlock();
			return false;
		}
	}
	m_children = new Octree<T>[8]{ // This calls the correct constructor and initializes the elements directly in place; NOTE: This still calls the copy constructor, so the elements are sadly still not directly constructed inplace
		Octree<T>(value, m_maxDepth, m_depth + 1),
		Octree<T>(value, m_maxDepth, m_depth + 1),
		Octree<T>(value, m_maxDepth, m_depth + 1),
		Octree<T>(value, m_maxDepth, m_depth + 1),
		Octree<T>(value, m_maxDepth, m_depth + 1),
		Octree<T>(value, m_maxDepth, m_depth + 1),
		Octree<T>(value, m_maxDepth, m_depth + 1),
		Octree<T>(value, m_maxDepth, m_depth + 1)
	};
#if DEBUG_SPLITS_PER_OCTREE == 1
	m_split++;
#endif
	if (threadSafe) writeUnlock();
	return true;
}

template<class T>
template<bool threadSafe>
bool Octree<T>::split() {
	return split(m_value);
}

template<class T>
template<bool threadSafe>
bool Octree<T>::hasData() {
	if (m_depth < m_maxDepth) return false;
	if (threadSafe) readLock();
	bool children = m_children;
	if (threadSafe) readUnlock();
	return children;
}

template<class T>
template<bool threadSafe>
OctreeData* Octree<T>::getData() {
	OctreeData* data = nullptr;
	if (m_depth == m_maxDepth) {
		if (threadSafe) readLock();
		data = (OctreeData*) m_children;
		if (threadSafe) readUnlock();
	}
	return data;
}

template<class T>
template<bool threadSafe>
bool Octree<T>::setData(const OctreeData& octreeData) {
	if (m_depth != m_maxDepth) return false;
	if (threadSafe) writeLock();

	if (!m_children) m_children = new OctreeData(octreeData);
	else *((OctreeData*) m_children) = octreeData;

	if (threadSafe) writeUnlock();
	return true;
}

template<class T>
unsigned char Octree<T>::calculateChildIndex(const unsigned int x, const unsigned int y, const unsigned int z) {
	// 0 - 7 are possible
	//If the bit is 1, we need to move to the right half and proceed with that child-octree. Otherwise through the left one.
	//Combining bits for ZYX translates directly to the index of the child we are looking for

	//if ((x >> (m_maxDepth - m_depth - 1)) & 1 == 1) childIndex += 1; // 001 -> Bit 2^0
	//if ((y >> (m_maxDepth - m_depth - 1)) & 1 == 1) childIndex += 2; // 010 -> Bit 2^1
	//if ((z >> (m_maxDepth - m_depth - 1)) & 1 == 1) childIndex += 4; // 100 -> Bit 2^2*/
	const unsigned int c = m_maxDepth - m_depth - 1;
	return ((x >> c) & 1) +		// 001 -> Bit 2^0 -> x
		(((y >> c) & 1) << 1) +	// 010 -> Bit 2^1 -> y
		(((z >> c) & 1) << 2);	// 100 -> Bit 2^2 -> z
}

template<class T>
template<bool threadSafe, int maxThreads>
void Octree<T>::minimize(std::atomic<int>* threadCount, const bool onThread) {
	if (!hasChildren()) return;
	std::thread subThreads[8];
	int subThreadCount = 0;
	for (int i = 0; i < 8; i++) {
		Octree<T>* child = ((Octree<T>*)m_children) + i;
		if (child->hasChildren()) {
			if (threadCount->load() < maxThreads) {
				subThreads[subThreadCount] = std::thread(&Octree<T>::minimize<maxThreads>, child, threadCount, true);
				subThreadCount++;
			} else {
				child->minimize<maxThreads>(threadCount, false);
			}
		}
	}
	for (int i = 0; i < subThreadCount; i++) subThreads[i].join();
	if (compareChildren()) clear(true);
	if (onThread) (*threadCount)--;
}

template<class T>
template<bool threadSafe>
void Octree<T>::minimize() {
	if (!hasChildren()) return;
	for (unsigned int i = 0; i < 8; i++) ((Octree<T>*) m_children)[i].minimize();
	if (compareChildren()) clear(false);
}

template<class T>
template<bool threadSafe>
void Octree<T>::minimizeMultithreaded() {
	if (!hasChildren()) return;
	std::thread threads[8];
	for (int i = 0; i < 8; i++) threads[i] = std::thread(&Octree<T>::minimize, ((Octree<T>*) m_children) + i);
	for (int i = 0; i < 8; i++) threads[i].join();
	if (compareChildren()) clear(true);
}

template<class T>
template<bool threadSafe>
bool Octree<T>::compareChildren() { //TODO 3: Check if memory leakage is possible due to the leaf octree changes
	if (m_depth == m_maxDepth) return true;
	bool result = true;
	if (threadSafe) readLock();
	if (m_children) {
		Octree<T>* child = ((Octree<T>*) m_children);
		T value = child->getValue<threadSafe>();		
		for (unsigned char i = 0; i < 8; i++) {
			child = ((Octree<T>*) m_children) + i;
			if (threadSafe) child->readLock();
			if (child->m_value != value || child->hasChildren()) { //if the octree still has children at this point, we can not minimize the tree any further since the children have different values
				result = false;
				if (threadSafe) child->readUnlock();
				break;
			}
			if (threadSafe) child->readUnlock();
		}
	}
	if (threadSafe) readUnlock();
	return true;
}

template<class T>
unsigned int Octree<T>::roundToGrid(const unsigned int value) const {
	const unsigned int c = m_maxDepth - m_depth;
	return (value >> c) << c;
}

template<class T>
unsigned int Octree<T>::roundToLocalGrid(const unsigned int value) const {
	const unsigned int c = m_maxDepth - m_depth - 1u;
	return (value >> c) << c;
}

template<class T>
unsigned int Octree<T>::getSize() const {
	return 1u << (m_maxDepth - m_depth);
}

template<class T>
unsigned int Octree<T>::getMaxSize() const {
	return 1u << m_maxDepth;
}

template<class T>
void Octree<T>::writeLock() {
	unsigned char expected;
	while (!m_lock.compare_exchange_weak(expected = 0u, 1u)) {
#ifdef LOCK_LOOP_SLEEP_TIME
		std::this_thread::sleep_for(LOCK_LOOP_SLEEP_TIME);
#endif // LOCK_LOOP_SLEEP_TIME
	}
}

template<class T>
void Octree<T>::writeUnlock() {
	m_lock.fetch_sub(1);
}

template<class T>
void Octree<T>::readLock() {
	m_lock.fetch_add(2);
	bool countCollision = true;
	while ((unsigned char)(m_lock.load() << 7u)) {
#ifdef LOCK_LOOP_SLEEP_TIME
		std::this_thread::sleep_for(LOCK_LOOP_SLEEP_TIME);
#endif // LOCK_LOOP_SLEEP_TIME
	}
}

template<class T>
void Octree<T>::readUnlock() {
	m_lock.fetch_sub(2);
}

/*template<class T>template<bool threadSafe>
void Octree<T>::acquireSemaphore() {
	bool expected;
	while (!m_semaphore.compare_exchange_weak(expected = false, true)) { // In case compare_exchange returns false, expected will be set to the current value of m_semaphore. That's why it has to be reset before the next comparison.
		++collisionCounter;
		std::this_thread::sleep_for(SEMAPHORE_LOOP_SLEEP_TIME);
	}
}

template<class T>template<bool threadSafe>
void Octree<T>::releaseSemaphore() {
	m_semaphore.store(false);
}

template<class T>template<bool threadSafe>
bool Octree<T>::tryAcquiringSemaphore(const unsigned int attempts) {
	bool expected;
	for (unsigned int i = 0; i < attempts; i++) {
		if (m_semaphore.compare_exchange_weak(expected = false, true)) return true;
		std::this_thread::sleep_for(SEMAPHORE_LOOP_SLEEP_TIME);
	}
	return false;
}

template<class T>template<bool threadSafe>
void Octree<T>::awaitSemaphoreRelease() {
	while (m_semaphore.load()) std::this_thread::sleep_for(SEMAPHORE_LOOP_SLEEP_TIME);
}

template<class T>template<bool threadSafe>
bool Octree<T>::testSemaphore() {
	return m_semaphore.load();
}*/
