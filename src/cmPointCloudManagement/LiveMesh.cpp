#include "LiveMesh.h"

#include "../cmOpenMeshBinding/OpenMeshGeometry.h"
#include <osg/Geode>
#include <osgDB/ReadFile>
#include <iostream>

#include <osg/PolygonMode>
#include <osg/CullFace>
#include "osg/LightModel"

#include "cmUtil/TimeWriter.h"

//ProjectionMapping
#include <osg/Texture2D>
#include <osg/TexMat>


LiveMesh::LiveMesh()
{
	m_useProjectionMapping = false; //default value -> value is externally set in the PointCloudManagr with m_liveMesh.useProjectionMapping(true);
}

LiveMesh::LiveMesh(int id)
{
	m_mutex.lock();
	m_currentLiveMeshState = RENDERED; //By initilaziation the state is rendered in order to treat the thread as an available thread to put new data in
	m_mutex.unlock();
	m_id = id;
	m_useProjectionMapping = false; //default value -> value is externally set in the PointCloudManagr with m_liveMesh.useProjectionMapping(true);
	m_numberOfLoop = 0;
}


void LiveMesh::initialize(k4a::device* k4aDevice, osg::ref_ptr<osg::Group> pointCloudGroup, k4a_device_configuration_t* config, ConfigReader* configReader)
{
	m_k4aDevice = k4aDevice;
	m_pointCloudGroup = pointCloudGroup;
	m_config = config;
	m_configReader = configReader;
	m_numberOfLoop = 0;

	//the distance at what the background gets cut out
	m_backgroundClippingDistance = 1.5f;

	//two array because we loop through m_pointcloud to get vertexHandles
	m_vertexArrayA = new osg::Vec3Array;
	m_vertexArrayB = new osg::Vec3Array;

	//init color arrays
	m_colorArrayA = new osg::Vec4Array;
	m_colorArrayB = new osg::Vec4Array;

	m_texcoords = new osg::Vec2Array;

	//additional Color-Array for temporal memory
	m_colorArrayCurrNotBinded = new osg::Vec4Array();


	//Hab jetzt noch eine "osg::Geode" vor cloth gehangen, damit man hier sp�ter noch Material und �hnliche Sachen einstellen kann
	m_telePresenceGeodeA = new osg::Geode();
	m_telePresenceGeodeB = new osg::Geode();

	//Das sollte eigentlich das hell/dunkel flackern aller Fl�chen unterbinden, tut es aber nicht... da stimmt noch irgendwas nicht
	//aber gerade nicht so wichtig.
	m_telePresenceGeodeA->getOrCreateStateSet()->setMode(GL_RESCALE_NORMAL, osg::StateAttribute::ON);
	m_telePresenceGeodeB->getOrCreateStateSet()->setMode(GL_RESCALE_NORMAL, osg::StateAttribute::ON);

	//set Lighting mode to Unlit
	m_telePresenceGeodeA->getOrCreateStateSet()->setMode(GL_LIGHTING, osg::StateAttribute::OVERRIDE | osg::StateAttribute::OFF);
	m_telePresenceGeodeB->getOrCreateStateSet()->setMode(GL_LIGHTING, osg::StateAttribute::OVERRIDE | osg::StateAttribute::OFF);

	initCloth();

	m_telePresenceGeodeA->addChild(m_clothA);
	m_telePresenceGeodeB->addChild(m_clothB);

	m_telePresenceGeodeA->dirtyBound();
	m_telePresenceGeodeB->dirtyBound();

	m_GroupRedundance = new osg::Group;

	m_GroupRedundance->addChild(m_telePresenceGeodeA);
	m_GroupRedundance->addChild(m_telePresenceGeodeB);


	m_pointCloudGroup->addChild(m_telePresenceGeodeB);
	//m_pointCloudGroup->removeChild(m_telePresenceGeodeB);

	osg::LightModel* ltModel = new osg::LightModel;
	ltModel->setTwoSided(false);
	m_GroupRedundance->getOrCreateStateSet()->setAttribute(ltModel);
	m_telePresenceGeodeA->getOrCreateStateSet()->setAttribute(ltModel);
	m_telePresenceGeodeB->getOrCreateStateSet()->setAttribute(ltModel);
	m_clothA->getOrCreateStateSet()->setAttribute(ltModel);
	m_clothB->getOrCreateStateSet()->setAttribute(ltModel);

	m_telePresenceGeodeA->getOrCreateStateSet()->setMode(GL_CULL_FACE, osg::StateAttribute::OFF);
	m_telePresenceGeodeB->getOrCreateStateSet()->setMode(GL_CULL_FACE, osg::StateAttribute::OFF);
	m_clothA->getOrCreateStateSet()->setMode(GL_CULL_FACE, osg::StateAttribute::OFF);
	m_clothB->getOrCreateStateSet()->setMode(GL_CULL_FACE, osg::StateAttribute::OFF);
	m_GroupRedundance->getOrCreateStateSet()->setMode(GL_CULL_FACE, osg::StateAttribute::OFF);

	m_telePresenceGeodeB->setNodeMask(0x0);


	//osg::ref_ptr<osg::PolygonMode> polymode = new osg::PolygonMode;
	//polymode->setMode(osg::PolygonMode::FRONT, osg::PolygonMode::FILL);
	//osg::CullFace* cull = new osg::CullFace();
	//cull->setMode(osg::CullFace::BACK);
	////m_telePresenceGeodeA->getOrCreateStateSet()->setAttributeAndModes(polymode.get(), osg::StateAttribute::OVERRIDE | osg::StateAttribute::ON);
	//m_telePresenceGeodeA->getOrCreateStateSet()->setAttributeAndModes(cull, osg::StateAttribute::OVERRIDE | osg::StateAttribute::ON);
	////m_telePresenceGeodeB->getOrCreateStateSet()->setAttributeAndModes(polymode.get(), osg::StateAttribute::OVERRIDE | osg::StateAttribute::ON);
	//m_telePresenceGeodeB->getOrCreateStateSet()->setAttributeAndModes(cull, osg::StateAttribute::OVERRIDE | osg::StateAttribute::ON);

	m_firstFrame = true;
	m_renderA = false;





	for (float i = 0; i < 576; i++)
	{
		for (float j = 0; j < 640; j++)
		{
			k4a_float2_t vec2;
			vec2.xy.x = j;
			vec2.xy.y = i;
			m_texcoordsDepthMapTemplate.push_back(vec2);
		}
	}



	m_image0 = new osg::Image();
	m_image0 = osgDB::readImageFile("data/picture2048x1536.jpg");
	//m_image0->allocateImage(m_colorMapWidth, m_colorMapHeight, 1, GL_BGRA, GL_UNSIGNED_BYTE);

	//osg::ref_ptr<VideoPlayer> imageStream0 = new VideoPlayer;
	//if (imageStream0)
	//{
	//	//imageStream->open("C:\\tmp\\Aufnahme8\\1.mp4");
	//	//imageStream->open("E:\\Daten\\FH D\\11. Semster\\DigBV\\goPro 4 Black Calibration Images\\JPEG\\GOPR0092.jpg");
	//	imageStream0->open(filePathes.at(0));
	//	imageStream0->play();
	//	imageStream0->setVolume(volume);
	////}
	//ColorMode 4
	//m_colorMapWidth = 2048; //ToDo and HWM: These two numbers must be set with respect to k4a_color_resolution_t in the config.xml. Is calcImageSize() the correct place?
	//m_colorMapHeight = 1536;

	//m_colorMapWidth = 4096; //ToDo and HWM: These two numbers must be set with respect to k4a_color_resolution_t in the config.xml. Is calcImageSize() the correct place?
	//m_colorMapHeight = 3072;

	//Color Mode 5
	//m_colorMapWidth = 3840; //ToDo and HWM: These two numbers must be set with respect to k4a_color_resolution_t in the config.xml. Is calcImageSize() the correct place?
	//m_colorMapHeight = 2160;

	//Color Mode 2
	m_colorMapWidth = 1920; //ToDo and HWM: These two numbers must be set with respect to k4a_color_resolution_t in the config.xml. Is calcImageSize() the correct place?
	m_colorMapHeight = 1080;

	
		

	osg::ref_ptr<osg::Texture2D> texture00 = new osg::Texture2D;
	texture00->setTextureSize(m_colorMapWidth, m_colorMapHeight);
	texture00->setDataVariance(osg::Object::DYNAMIC);
	texture00->setResizeNonPowerOfTwoHint(false);
	//texture00->setInternalFormat(GL_BGRA);
	texture00->setInternalFormatMode(osg::Texture::InternalFormatMode::USE_IMAGE_DATA_FORMAT);
	//texture00->setInternalFormatMode(osg::Texture::InternalFormatMode::USE_S3TC_DXT3_COMPRESSION);
	texture00->setFilter(osg::Texture2D::MIN_FILTER, osg::Texture2D::LINEAR);
	texture00->setFilter(osg::Texture2D::MAG_FILTER, osg::Texture2D::LINEAR);
	texture00->setWrap(osg::Texture::WRAP_S, osg::Texture::CLAMP_TO_BORDER);
	texture00->setWrap(osg::Texture::WRAP_T, osg::Texture::CLAMP_TO_BORDER);
	texture00->setImage(m_image0.get());

	m_telePresenceGeodeB->getOrCreateStateSet()->setTextureAttributeAndModes(0, texture00, osg::StateAttribute::ON | osg::StateAttribute::OVERRIDE);
	m_telePresenceGeodeB->getOrCreateStateSet()->setMode(GL_LIGHTING, osg::StateAttribute::OFF | osg::StateAttribute::OVERRIDE);
	m_telePresenceGeodeB->getOrCreateStateSet()->setTextureMode(0, GL_TEXTURE_GEN_S, osg::StateAttribute::ON);
	m_telePresenceGeodeB->getOrCreateStateSet()->setTextureMode(0, GL_TEXTURE_GEN_T, osg::StateAttribute::ON);
	m_telePresenceGeodeB->getOrCreateStateSet()->setTextureMode(0, GL_TEXTURE_GEN_R, osg::StateAttribute::ON);
	m_telePresenceGeodeB->getOrCreateStateSet()->setTextureMode(0, GL_TEXTURE_GEN_Q, osg::StateAttribute::ON);


	osg::ref_ptr<osg::Program> program = new osg::Program;
	osg::ref_ptr<osg::Shader> vshader = osg::Shader::readShaderFile(osg::Shader::VERTEX, "data/shader/SimpleTextureVertexShader.glsl");
	program->addShader(vshader.get());
	osg::ref_ptr<osg::Shader> fshader = osg::Shader::readShaderFile(osg::Shader::FRAGMENT, "data/shader/SimpleTextureFragmentShader.glsl");
	program->addShader(fshader.get());

	osg::ref_ptr<osg::StateSet> stateSet0 = new osg::StateSet;
	osg::Uniform* texUniform00 = new osg::Uniform(osg::Uniform::SAMPLER_2D, "tex0");
	texUniform00->set(0);
	stateSet0->setTextureAttributeAndModes(0, texture00.get(), osg::StateAttribute::ON | osg::StateAttribute::OVERRIDE);
	stateSet0->addUniform(texUniform00);
	stateSet0->setMode(GL_LIGHTING, osg::StateAttribute::OFF);
	stateSet0->setAttributeAndModes(program.get(), osg::StateAttribute::ON | osg::StateAttribute::OVERRIDE);
	m_telePresenceGeodeB->setStateSet(stateSet0);



	if (m_useProjectionMapping)
	{
		// #######################Texture projection starts here #######################
		// Some notes to this "broken" implementation:
		// The main challange is to align the colorMap and the depthMESH. Even though the kinect pipeline has functions as transform_color_map_to_depth_map and vice versa
		// the maximal colorTexture resolution is as "low" as from the depthMap... (maybe this could help: 
		// https://github.com/microsoft/Azure-Kinect-Sensor-SDK/pull/778/commits/7bd44225414ad4f79ab64ec5ee694aeb73330bea from https://github.com/microsoft/Azure-Kinect-Sensor-SDK/issues/762) 
		// However, the far more challenging task is the alignment. I have no clue, how to perfectly align the colorImage with depthImage, since the resulting
		// pointcloud (as can be seen in the current implementation) is not centered around the Z-axis... it is not symentrically.. even the depthSensor is tilted 6� to the color cam...
		// Have a look at color an depth alignment at this page: https://docs.microsoft.com/de-de/azure/kinect-dk/hardware-specification
		// I think, there may be two solutions: 
		// 1.) Wrap the head around the Kinect functions for transformation (the links above) and use their xy_tables - but there will be possibly no further clues for the
		// perspective matrix setup for both maps....
		// 2.) Get intrinsic and extrinsic parameters for color and depth Sensor with OpenCV (calibratecamera(), solvePNP(), Aruco). Theoretically, I can derive 
		// two suitable and aligned projection matrices! I think, this is the way to go, since this would also allow to concurrently calibrate and merge multiple Kinects!

		//Projective texture ##########################################
		//m_colorMapWidth = 2048; //ToDo and HWM: These two numbers must be set with respect to k4a_color_resolution_t in the config.xml. Is calcImageSize() the correct place?
		//m_colorMapHeight = 1536;
		float aspectRatio = m_colorMapWidth / m_colorMapHeight;
		unsigned int samples = 0;
		unsigned int colorSamples = 0;

		// radial distortion shader
		osg::ref_ptr<osg::Program> program = new osg::Program;
		osg::ref_ptr<osg::Shader> vshader = osg::Shader::readShaderFile(osg::Shader::VERTEX, "data/shader/radialDistortionVS.glsl");
		program->addShader(vshader.get());
		osg::ref_ptr<osg::Shader> fshader = osg::Shader::readShaderFile(osg::Shader::FRAGMENT, "data/shader/radialDistortionFS.glsl");
		program->addShader(fshader.get());

		//1. Movie File ###########################################################################################################################################################
		// Was initally used with VLC for realtime 3d stitching -> Project ProjectiveTexStitching.sln in dir "ProjectiveTexture" by Philipp Ladwig
		// if not loaded assume no arguments passed in, try use default mode instead.
		//osg::Node* loadedModelAsNode = osgDB::readNodeFile("C:/devel/MeshMill/bin/data/monkey.obj");
		osg::Group* loadedModel0 = new osg::Group();

		osg::ref_ptr<osg::StateSet> stateSet0 = new osg::StateSet;

		/* 1. Load the texture that will be projected */
		m_image0 = new osg::Image();
		m_image0 = osgDB::readImageFile("data/picture.jpg");
		//m_image0->allocateImage(m_colorMapWidth, m_colorMapHeight, 1, GL_BGRA, GL_UNSIGNED_BYTE);

		//osg::ref_ptr<VideoPlayer> imageStream0 = new VideoPlayer;
		//if (imageStream0)
		//{
		//	//imageStream->open("C:\\tmp\\Aufnahme8\\1.mp4");
		//	//imageStream->open("E:\\Daten\\FH D\\11. Semster\\DigBV\\goPro 4 Black Calibration Images\\JPEG\\GOPR0092.jpg");
		//	imageStream0->open(filePathes.at(0));
		//	imageStream0->play();
		//	imageStream0->setVolume(volume);
		//}
		osg::ref_ptr<osg::Texture2D> texture00 = new osg::Texture2D;
		texture00->setTextureSize(m_colorMapWidth, m_colorMapHeight);
		texture00->setDataVariance(osg::Object::DYNAMIC);
		texture00->setResizeNonPowerOfTwoHint(false);
		//texture00->setInternalFormat(GL_BGRA);
		texture00->setInternalFormatMode(osg::Texture::InternalFormatMode::USE_IMAGE_DATA_FORMAT);
		texture00->setFilter(osg::Texture2D::MIN_FILTER, osg::Texture2D::LINEAR);
		texture00->setFilter(osg::Texture2D::MAG_FILTER, osg::Texture2D::LINEAR);
		texture00->setWrap(osg::Texture::WRAP_S, osg::Texture::CLAMP_TO_BORDER);
		texture00->setWrap(osg::Texture::WRAP_T, osg::Texture::CLAMP_TO_BORDER);
		texture00->setImage(m_image0.get());

		osg::Uniform* texUniform00 = new osg::Uniform(osg::Uniform::SAMPLER_2D, "textureSampler");
		texUniform00->set(0);
		stateSet0->setTextureAttributeAndModes(0, texture00.get(), osg::StateAttribute::ON);
		stateSet0->addUniform(texUniform00);
		stateSet0->setMode(GL_LIGHTING, osg::StateAttribute::OFF);
		stateSet0->setAttributeAndModes(program.get(), osg::StateAttribute::ON | osg::StateAttribute::OVERRIDE);

		// Textured Quad used as a obj as a plane
		//osg::Geode* geode0 = new osg::Geode();
		//osg::Geometry* texturedQuad0 = osg::createTexturedQuadGeometry(osg::Vec3(0.0f, 0, 1.0f), osg::Vec3(-aspectRatio, 0.0f, 0.0f), osg::Vec3(0.0f, 0.0f, -1.0f), 0.0f, 0.0f, 1.0f, 1.0f);
		//texturedQuad0->setStateSet(stateSet0);
		//geode0->addDrawable(texturedQuad0);
		//loadedModel0->addChild(geode0);

		osg::Node* backgroundObject0 = osgDB::readNodeFile("data/live_mesh/withoutCorrection.obj");
		backgroundObject0->setStateSet(stateSet0);
		loadedModel0->addChild(backgroundObject0);
		m_pointCloudGroup->addChild(backgroundObject0);

		//Mit dieser vorerst LEEREN Textur muss ich die Innenseite der Kugel "bestrahlen" - aktuell wird es aber als Textur auf den Cube gesetzt
		// texture to render to and to use for rendering of flag.
		osg::Texture2D* texture0 = new osg::Texture2D;
		texture0->setTextureSize(m_colorMapWidth, m_colorMapHeight);
		texture0->setInternalFormat(GL_RGBA);
		texture0->setFilter(osg::Texture2D::MIN_FILTER, osg::Texture2D::LINEAR);
		texture0->setFilter(osg::Texture2D::MAG_FILTER, osg::Texture2D::LINEAR);
		texture0->setWrap(osg::Texture::WRAP_S, osg::Texture::CLAMP_TO_BORDER);
		texture0->setWrap(osg::Texture::WRAP_T, osg::Texture::CLAMP_TO_BORDER);

		//Hier startet eigentlich die FBO Geschichte
		osg::Camera* camera0 = new osg::Camera;

		// set up the background color and clear mask.
		camera0->setClearColor(osg::Vec4(0.9f, 0.1f, 0.3f, 1.0f));
		camera0->setClearMask(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		// set up projection.
		//camera->setProjectionMatrixAsFrustum(-proj_right, proj_right, -proj_top, proj_top, znear, zfar);
		camera0->setProjectionMatrixAsOrtho(0, aspectRatio, 0, 1, 0.9, 10.0); //ATTENTION: BE AWARE OF THE CORRECT ASPECT RATIO OF PLANE AND BOTH PROJ. MATRICES!

		// set view
		camera0->setReferenceFrame(osg::Transform::ABSOLUTE_RF);
		camera0->setViewMatrixAsLookAt(osg::Vec3(0.0f, 1.0f, 0.0f), osg::Vec3(0.0f, 0.0f, 0.0f), osg::Vec3(0.0f, 0.0f, 1.0f));

		// set viewport
		camera0->setViewport(0, 0, m_colorMapWidth, m_colorMapHeight);

		// set the camera to render before the main camera.
		camera0->setRenderOrder(osg::Camera::PRE_RENDER);

		// tell the camera to use OpenGL frame buffer object where supported.
		osg::Camera::RenderTargetImplementation renderImplementation0 = osg::Camera::FRAME_BUFFER_OBJECT;
		camera0->setRenderTargetImplementation(renderImplementation0);

		// attach the texture and use it as the color buffer.
		camera0->attach(osg::Camera::COLOR_BUFFER, texture0, 0, 0, false, samples, colorSamples);

		camera0->addChild(loadedModel0);

		m_pointCloudGroup->addChild(camera0);

		osg::ref_ptr<osg::Program> projProg = new osg::Program;
		osg::ref_ptr<osg::Shader> projvertexShader = osg::Shader::readShaderFile(osg::Shader::VERTEX, "data/shader/VertexShader.glsl");
		osg::ref_ptr<osg::Shader> projfragShader = osg::Shader::readShaderFile(osg::Shader::FRAGMENT, "data/shader/FragmentShader.glsl");
		projProg->addShader(projvertexShader.get());
		projProg->addShader(projfragShader.get());

		m_stateSetShader = new osg::StateSet();
		m_stateSetShader->setTextureAttributeAndModes(0, texture0, osg::StateAttribute::ON);

		// handover the textures to the fragment shader via uniforms
		osg::Uniform* texUniform0 = new osg::Uniform(osg::Uniform::SAMPLER_2D, "projectionMap0");
		texUniform0->set(0);
		m_stateSetShader->addUniform(texUniform0);


		m_stateSetShader->setTextureMode(0, GL_TEXTURE_GEN_S, osg::StateAttribute::ON);
		m_stateSetShader->setTextureMode(0, GL_TEXTURE_GEN_T, osg::StateAttribute::ON);
		m_stateSetShader->setTextureMode(0, GL_TEXTURE_GEN_R, osg::StateAttribute::ON);
		m_stateSetShader->setTextureMode(0, GL_TEXTURE_GEN_Q, osg::StateAttribute::ON);

		osg::TexMat* texMat = new osg::TexMat;
		osg::Matrix mat;
		//osg::Vec3 projectorPos0 = osg::Vec3(0.0, 0.0, 0.0);
		//osg::Vec3 projectorDirection0 = osg::Vec3(0.0, 0.0, -1.0);
		//projectorDirection0.normalize();
		//osg::Vec3 up0(0.0f, 1.0f, 0.0f);
		//LookAt is camera matrix

		//Create the texture matrix. This matrix descripes the projection onto the depthMap mesh
		float projectorAngle = 90.0;
		osg::Vec3f scale(1.0, 1.0, 1.0);
		osg::Vec3f translate(0.5, 0.5, 0.0);

		mat = osg::Matrixd::lookAt(osg::Vec3(0.0, 0.0, 0.0), osg::Vec3(0.0, 0.0, -1.0), -osg::Y_AXIS) *
			osg::Matrixd::perspective(projectorAngle, aspectRatio, 0.01, 10.0) *
			//osg::Matrix::scale(scale) *
			osg::Matrix::translate(translate); //Z im translate hat keinen effekt

		//mat0 = osg::Matrixd::lookAt(projectorPos0, projectorDirection0, up) * osg::Matrixd::ortho(-1.7, 1.7, -1.7, 1.7, 0.1, 100.0) * osg::Matrix::translate(0.5, 0.5, 0.5);
		texMat->setMatrix(mat);
		m_stateSetShader->setTextureAttributeAndModes(0, texMat, osg::StateAttribute::ON);

		m_stateSetShader->setAttribute(projProg.get());

		m_pointCloudGroup->setStateSet(m_stateSetShader);
	}
}

void LiveMesh::renderFrame()
{

	//Turn console metering on/off
	bool isMeteringOn = false;
	TimeWriter tw;
	tw.startMetering("LiveMeshFrame", true, false);
	if (isMeteringOn)
		tw.startMetering("1", true, false);
	if (!m_firstFrame)
	{
		//swap double buffer
		if (m_renderA)
		{
			////clear vertex array
			m_vertexArrayA->clear();

			////reset vertex array
			////APPERANTLY VERY IMPORTANT TO RESET IT EVERY FRAME -.-
			m_clothA->setVertexArray(m_vertexArrayA);

			m_clothA->getMesh()->clean_keep_reservation();

			//swap pointcloud pointer
			//m_currpointcloud = m_pointcloudA;

		}
		else
		{
			m_vertexArrayB->clear();
			m_clothB->setVertexArray(m_vertexArrayB);
			//m_currpointcloud = m_pointcloudB;
			m_clothB->getMesh()->clean_keep_reservation();

			m_texcoords->clear();
			m_clothB->setTexCoordArray(0, m_texcoords);
			//m_texcoords->setBinding(osg::Array::BIND_PER_VERTEX);

		}


	}
	else //first frame
	{
		//create new array for pointcloud
		//m_currpointcloud = new osg::Vec3Array();


		//get calibration data
		m_calibration = m_k4aDevice->get_calibration(m_config->depth_mode, m_config->color_resolution);

		//create transform
		m_transformation = k4a::transformation(m_calibration);

		////clear vertex array
		m_vertexArrayA->clear();
		m_vertexArrayB->clear();

		//TODO 8466 - Initilze ProjectionMapping here, cause only here we know the size of the texture

	}
	if (isMeteringOn)
		tw.endMetering();
	if (isMeteringOn)
		tw.startMetering("10", true, false);
	//if (m_renderA)
	//{
	//	m_pointCloudGroup->removeChild(m_telePresenceGeodeA);
	//	m_pointCloudGroup->addChild(m_telePresenceGeodeB);
	//}
	//else
	//{
	//	m_pointCloudGroup->removeChild(m_telePresenceGeodeB);
	//	m_pointCloudGroup->addChild(m_telePresenceGeodeA);
	//}
	if (isMeteringOn)
		tw.endMetering();

	if (isMeteringOn)
		tw.startMetering("20", true, false);
	//gets depth and color data from capture
	m_depthMap = m_kinectCapture->get_depth_image();
	m_colorMap = m_kinectCapture->get_color_image();
	if (isMeteringOn)
		tw.endMetering();
	if (isMeteringOn)
		tw.startMetering("30", true, false);
	try
	{
		//scale down the color image to the size of the depth image and store it in m_colorMapTransformed
		//m_colorMapTransformed = m_transformation.color_image_to_depth_camera(m_depthMap, m_colorMap);



		//std::vector<k4a_float2_t> points_2d = { { { 320.f, 288.f } }, // depth camera center
		//							   { { 0, 0.f } },  // depth camera top left
		//							   { { 640.f, 0.f } },   // depth camera top right
		//							   { { 640.f, 576.f } },    // depth camera bottom right
		//							   { { 0.f, 576.f } } }; // depth camera bottom left


			//std::vector<k4a_float3_t> points_3d = { { { 0.f, 0.f, 1000.f } },          // color camera center
			//						   { { -1000.f, -1000.f, 1000.f } },  // color camera top left
			//						   { { 1000.f, -1000.f, 1000.f } },   // color camera top right
			//						   { { 1000.f, 1000.f, 1000.f } },    // color camera bottom right
			//						   { { -1000.f, 1000.f, 1000.f } } }; // color camera bottom left


			//std::vector<k4a_float2_t> k4a_points_2d(points_3d.size());
		//std::vector<k4a_float2_t> k4a_points_2d(points_2d.size());
		std::vector<k4a_float2_t> k4aPoints2d(m_texcoordsDepthMapTemplate.size());
		std::cout << "----- m_numberOfLoop: " << m_numberOfLoop << std::endl;
		//if (m_numberOfLoop % 10 == 0)
		if (m_firstFrame)
		{
			std::cout << "Drin" << std::endl;
			m_texcoords->clear();
			m_texcoords->reserve(m_texcoordsDepthMapTemplate.size());
		}
		//for (int i = 0; i < points_3d.size(); i++)
		for (int i = 0; i < m_texcoordsDepthMapTemplate.size(); i++)
		{
			int valid = 0;
			if (m_firstFrame)
			{
				k4a_calibration_2d_to_2d(&m_calibration,
					&m_texcoordsDepthMapTemplate[i],
					222.0f,
					K4A_CALIBRATION_TYPE_DEPTH,
					K4A_CALIBRATION_TYPE_COLOR,
					&k4aPoints2d[i],
					&valid);

				m_texcoords->push_back(osg::Vec2(k4aPoints2d.at(i).xy.x / float(m_colorMapWidth), k4aPoints2d.at(i).xy.y / float(m_colorMapHeight)));
			}
			else
			{
				const uint16_t* depth_image_data = (const uint16_t*)(const void*)(m_depthMap.get_buffer());
				uint16_t d = depth_image_data[i];
				k4a_calibration_2d_to_2d(&m_calibration,
					&m_texcoordsDepthMapTemplate[i],
					d,
					K4A_CALIBRATION_TYPE_DEPTH,
					K4A_CALIBRATION_TYPE_COLOR,
					&k4aPoints2d[i],
					&valid);

					m_texcoords->push_back(osg::Vec2(k4aPoints2d.at(i).xy.x / float(m_colorMapWidth), k4aPoints2d.at(i).xy.y / float(m_colorMapHeight)));

					m_texcoords->dirty();
			}

		}
	}
	catch (const std::exception& e)
	{
		std::cerr << "ERROR color_image_to_depth_camera(): " << e.what() << std::endl;
	}
	if (isMeteringOn)
		tw.endMetering();

	if (isMeteringOn)
		tw.startMetering("40", true, false);
	//get color values and store them in m_colorArrayCurrNotBinded
	//calcColorList(m_colorMapTransformed);
	if (m_useProjectionMapping)
	{
		renderKinectColorImage(&m_colorMap); //ATTENTION: May crash, cause call be ref. Does the Kinect pipeline release the colorMap anytime soon after a frame is 
		// captured? Is there a ref count? Abvisouly not... just a pointer...there have to be an event like kinectCapture.release()?
	}
	renderKinectColorImage(&m_colorMap);
	if (isMeteringOn)
		tw.endMetering();

	if (isMeteringOn)
		tw.startMetering("50", true, false);
	//this try catch block creates a wierd sphere, which is also very slow
	try
	{
		//convert depth image to pointcloud (perspective transform)
		// Seems there is no GPU usage or less GPU usage, because it calls
		// transformation_depth_image_to_point_cloud_internal() and then
		// transformation_depth_to_xyz() in rgbz.c. These functions are 
		// optimized for SSE
		// see https://docs.microsoft.com/en-us/azure/kinect-dk/use-image-transformation 
		// The author of this article refers the reader to the https://github.com/Microsoft/Azure-Kinect-Sensor-SDK/tree/develop/examples/fastpointcloud
		// and says that this example code can be used to implement own GPU optimized code
		m_pointcloudImage = m_transformation.depth_image_to_point_cloud(m_depthMap, K4A_CALIBRATION_TYPE_DEPTH); 
	}
	catch (const std::exception& e)
	{
		//this try catch block creates a wierd sphere when return is called, which is also very slow
		//so I didn't call return and let it use the old pointcloud
		std::cerr << "ERROR depth_image_to_point_cloud(): " << e.what() << std::endl;
	}
	if (isMeteringOn)
		tw.endMetering();

	if (isMeteringOn)
		tw.startMetering("60", true, false);
	//gets the current OSG TriMesh of the cloth
	if (m_renderA)
	{
		m_currMesh = m_clothA->getMesh();
	}
	else
	{
		m_currMesh = m_clothB->getMesh();
	}
	if (isMeteringOn)
		tw.endMetering();
	if (isMeteringOn)
		tw.startMetering("convertPointCloudImageToVectorArray", true, false);
	//convert pointcloud image to Vector array
	convertPointCloudImageToVectorArray(m_pointcloudImage);
	if (isMeteringOn)
		tw.endMetering();


	//#####################
	//create faces in cloth
	//#####################
	if (isMeteringOn)
		tw.startMetering("create faces in cloth", true, false);
	//loop over all Vertices, except for the last row
	for (int i = 0; i < m_numVerts - m_depthMapWidth; i++)
	{
		//check if Vertex is NOT in last column
		//also check if neighbors (right and down) are valid
		if ((i + 1) % m_depthMapWidth != 0 && !m_nullVector[i + 1] && !m_nullVector[i + m_depthMapWidth])
		{
			//check if current Vertex is valid
			if (!m_nullVector[i])
			{
				//create top left triangle
				m_currMesh->add_face(m_partsOfTriangle.at(i + 1), m_partsOfTriangle.at(i + m_depthMapWidth), m_partsOfTriangle.at(i));
			}

			//check if diagonal Neigbor (down/left) of current Vertex is valid
			if (!m_nullVector[i + m_depthMapWidth + 1])
			{
				//create bottom right triangle
				m_currMesh->add_face(m_partsOfTriangle.at(i + m_depthMapWidth + 1), m_partsOfTriangle.at(i + m_depthMapWidth), m_partsOfTriangle.at(i + 1));
			}
		}
	}
	if (isMeteringOn)
		tw.endMetering();

	if (isMeteringOn)
		tw.startMetering("deleteLongEdges", true, false);
	//delete long edges
	deleteLongEdges(0.012f, m_currMesh);
	if (isMeteringOn)
		tw.endMetering();




	if (isMeteringOn)
		tw.startMetering("Hiermit gehe ich �ber alle faces", true, false);
	//Hiermit gehe ich �ber alle faces und lese jeweils die vertex ID der anliegenden verticies aus und schreibe damit ein neues IndexArray
	//Das war ein altes bekanntes Problem zwischen der Anbindung von OSG und OpenMesh. Das kostet in unserem Fall viel Zeit.
	//Daf�r gibt es sicherlich eine bessere L�sung. Ich wei� nicht mehr genau, wieso man das nochmal zuweisen muss,
	//aber das ist nicht die sch�nste L�sung.
	std::vector<OpenMesh::VertexHandle> vhandles/* = m_partsOfTriangle*/;


	unsigned int idx;

	//for every face
	for (int i = 0, nF = m_currMesh->n_faces(); i < nF; ++i)
	{
		vhandles.clear();
		//for every vertex in curr face
		for (auto fv_it = m_currMesh->cfv_iter(OpenMesh::FaceHandle(int(i))); fv_it.is_valid(); ++fv_it)
		{
			//add curr vertex to vhandles
			vhandles.push_back(*fv_it);
		}

		m_currMesh->get_property_handle(m_indicesPropH, "indices");
		m_indices = m_currMesh->property(m_indicesPropH);


		m_indices[3 * i] = vhandles[0].idx();
		m_indices[3 * i + 1] = vhandles[1].idx();
		m_indices[3 * i + 2] = vhandles[2].idx();


	}
	if (isMeteringOn)
		tw.endMetering();

	//if (isMeteringOn)
	//	tw.startMetering("update_normals", true, false);
	//Normalen updaten
	//m_currMesh->update_normals();
	//if (isMeteringOn)
	//	tw.endMetering();

	if (isMeteringOn)
		tw.startMetering("120", true, false);
	if (m_renderA)
	{
		//"dirty" ist ein flag, damit OpenGL wei�, dass sich etwas ge�ndert hat. Sonst werden �nderungen nicht �bernommen. Der Grund ist performance
		m_clothA->getNormalArray()->dirty();
		m_clothA->getVertexArray()->dirty();
		m_clothA->getPrimitiveSet(0)->dirty();
		// m_clothA->getTexCoordArray(0)->dirty(); -> Noch keins da


	}
	else
	{
		//"dirty" ist ein flag, damit OpenGL wei�, dass sich etwas ge�ndert hat. Sonst werden �nderungen nicht �bernommen. Der Grund ist performance
		m_clothB->getNormalArray()->dirty();
		m_clothB->getVertexArray()->dirty();
		m_clothB->getPrimitiveSet(0)->dirty();
		m_clothB->getTexCoordArray(0)->dirty();
	}

	if (isMeteringOn)
		tw.endMetering();

	//if (m_renderA)
	//{
	//	m_pointCloudGroup->removeChild(m_telePresenceGeodeB);
	//	m_pointCloudGroup->addChild(m_telePresenceGeodeA);
	//}
	//else
	//{
	//	m_pointCloudGroup->removeChild(m_telePresenceGeodeA);
	//	m_pointCloudGroup->addChild(m_telePresenceGeodeB);
	//}


	//###########
	//Smoothing
	//###########
	//if (isMeteringOn)
	//	tw.startMetering("Smoothing", true, false);
	//smooth(2);
	//if (isMeteringOn)
	//	tw.endMetering();


	//m_clothA->getVertexArray()->dirty();
	if (isMeteringOn)
		tw.startMetering("130", true, false);
	if (m_renderA)
	{
		m_clothB->getVertexArray()->dirty();
	}
	else
	{
		m_clothB->getVertexArray()->dirty();
	}

	//m_pointCloudGroup->addChild(m_telePresenceGeodeB);	//CAUSE FOR CRASH (obviously)


	//m_renderA = !m_renderA;


	if (m_firstFrame)
	{
		m_firstFrame = false;
	}

	if (isMeteringOn)
		tw.endMetering();

	// Debug Output current width and heights
	//calcImageSize(kinectCapture);
	m_mutex.lock();
	m_currentLiveMeshState = READY_TO_RENDER;
	m_mutex.unlock();

	tw.endMetering(); //LiveMeshFrame
	m_numberOfLoop++;

}

void LiveMesh::convertPointCloudImageToVectorArray(k4a::image point_cloud)
{


	//get Vertex Buffer and size
	uint8_t* points = point_cloud.get_buffer();
	size_t depth_buffer_size = point_cloud.get_size();


	//create temporal variables
	float x;
	float y;
	float z;
	int16_t x16;
	int16_t y16;
	int16_t z16;

	OpenMesh::ArrayKernel::VertexHandle currVHandle;

	//reset vertex handle array and nullVector flag array
	m_partsOfTriangle.clear();
	m_partsOfTriangle.reserve(depth_buffer_size);
	m_nullVector.clear();
	m_nullVector.reserve(depth_buffer_size);


	//loop over all values in the depth buffer
	for (size_t currVal = 0; currVal < depth_buffer_size; currVal += 6)
	{

		//cast 2 1-Byte values to a single 2-Byte value
		x16 = (int16_t)(points[currVal + 1] << 8 | points[currVal]);
		y16 = (int16_t)(points[currVal + 3] << 8 | points[currVal + 2]);
		z16 = (int16_t)(points[currVal + 5] << 8 | points[currVal + 4]);

		//scale millimeter to meter
		x = x16 * 0.001;
		y = y16 * (-0.001); //flip Y-axis, Microsoft makes Y -> down
		z = z16 * 0.001;

		//flag invalid and distant Vertices, so the adjacent Faces won't be rendered
		if (z == 0 || z > m_backgroundClippingDistance)
		{
			m_nullVector.push_back(true);
		}
		else
		{
			m_nullVector.push_back(false);

		}
		//TimeWriter tw;
		//tw.startMetering("add_vertex", 1, 0);
		//get current vertex handle while adding vertex to mesh
		currVHandle = m_currMesh->add_vertex(osg::Vec3(x, y, z));
		//tw.endMetering();

		//tw.startMetering("push_back", 1, 0);
		//push current vertex to Handle-Array
		m_partsOfTriangle.push_back(currVHandle);
		//tw.endMetering();


		try
		{
			//set Vertex Color
			//m_currMesh->set_color(currVHandle, m_colorArrayCurrNotBinded->at(currVal / 6));
			//m_currMesh->set_texcoord2D(currVHandle, m_texcoords->at(currVal / 6));
			//if (m_renderA)
			//{
			//	m_currMesh->set_color(currVHandle, OSG_Traits::Color(osg::Vec4f(1.0f, 0.0f, 0.0f, 1.0f)));
			//std::cout << "Has Vertex TexCoords: " << m_currMesh->has_vertex_texcoords2D() << std::endl;
			//}
			//else
			//{
			//	m_currMesh->set_color(currVHandle, OSG_Traits::Color(osg::Vec4f(0.0f, 0.0f, 1.0f, 1.0f)));

			//}

		}
		catch (const std::exception&)
		{
			std::cout << "FAILED: localColorArray.size() = " << m_colorArrayCurrNotBinded->size() << ", " << "currVal = " << currVal << std::endl;
		}
	}

}





void LiveMesh::startRendering()
{

	while (true)
	{
		m_mutex.lock();
		LiveMeshStates temp = m_currentLiveMeshState;
		m_mutex.unlock();
		if (temp == NEW_DATA_AVAILABLE)
		{
			m_mutex.lock();
			m_currentLiveMeshState = PROCESSING;
			m_mutex.unlock();
			//std::cout << "start rendering mesh " << m_id << std::endl;

			renderFrame();

			//std::cout << "finished rendering mesh " << m_id << std::endl;

		}

		std::this_thread::sleep_for(std::chrono::milliseconds(5)); // HYPERPARAMETER TUNING

	}

}

void LiveMesh::setKinectCapture(k4a::capture* kinectCapture)
{

	m_kinectCapture = kinectCapture;


	m_mutex.lock();
	m_currentLiveMeshState = NEW_DATA_AVAILABLE;
	m_mutex.unlock();

}

void LiveMesh::setMeshInactive()
{

	//m_pointCloudGroup->removeChild(m_telePresenceGeodeB);
	m_telePresenceGeodeB->setNodeMask(0x0);
	//m_mutex.lock();
	//m_currentLiveMeshState = ?;
	//m_mutex.unlock();


}

void LiveMesh::setMeshActive()
{


	m_telePresenceGeodeB->setNodeMask(0x1);

	m_mutex.lock();
	m_currentLiveMeshState = IN_SCENE_GRAPH;
	m_mutex.unlock();

	//std::cout << "set active mesh " << m_id << std::endl;

}








void LiveMesh::initCloth()
{

	//create empty mesh
	m_clothA = new osg::OpenMeshGeometry(GeometryType::MODELLING, m_configReader, "kester-cloth");

	//Es muss immer erst �ber den OSG-Weg jeweils ein VertexArray bzw. eine IndexListe erstellt werden, damit dort eine Speicheradresse 
	//   abgelegt ist. Mit dieser Speicheradresse kann OpenMesh dann arbeiten. Nicht ganz trivial.
	m_clothA->setVertexArray(m_vertexArrayA);
	m_clothA->addPrimitiveSet(new osg::DrawElementsUInt(osg::PrimitiveSet::TRIANGLES, 0));

	m_clothA->setColorArray(m_colorArrayA);
	m_clothA->setColorBinding(osg::Geometry::BIND_PER_VERTEX);
	//m_clothA->getMesh()->reserve(368640, 639 * 575 + 368640 / 4, 368640 / 2);



	//create empty mesh
	m_clothB = new osg::OpenMeshGeometry(GeometryType::MODELLING, m_configReader, "kester-clothB");

	//Es muss immer erst �ber den OSG-Weg jeweils ein VertexArray bzw. eine IndexListe erstellt werden, damit dort eine Speicheradresse 
	//   abgelegt ist. Mit dieser Speicheradresse kann OpenMesh dann arbeiten. Nicht ganz trivial.
	m_clothB->setVertexArray(m_vertexArrayB);
	m_clothB->addPrimitiveSet(new osg::DrawElementsUInt(osg::PrimitiveSet::TRIANGLES, 0));

	//m_clothB->setColorArray(m_colorArrayB);
	//m_clothB->setColorBinding(osg::Geometry::BIND_PER_VERTEX);
	//m_clothB->getMesh()->reserve(368640, 639 * 575 + 368640 / 4, 368640 / 2);
	m_clothB->setTexCoordArray(0, m_texcoords);
	m_texcoords->setBinding(osg::Array::BIND_PER_VERTEX);
	m_clothB->setColorBinding(osg::Geometry::BIND_PER_VERTEX);
	//m_clothB->getMesh()->request_vertex_texcoords2D();

}

// ToDo: Not used?
void LiveMesh::calcImageSize(k4a::capture* kinectCapture)
{

	m_depthMap = kinectCapture->get_depth_image();
	m_depthMapWidth = m_depthMap.get_width_pixels();
	m_depthMapHeight = m_depthMap.get_height_pixels();
	std::cout << "m_depthMapWidth: " << m_depthMapWidth << std::endl;
	std::cout << "m_depthMapHeight: " << m_depthMapHeight << std::endl;

	m_colorMap = kinectCapture->get_color_image();
	m_colorMapWidth = m_colorMap.get_width_pixels();
	m_colorMapHeight = m_colorMap.get_height_pixels();
	std::cout << "m_colorMapWidth: " << m_colorMapWidth << std::endl;
	std::cout << "m_colorMapHeight: " << m_colorMapHeight << std::endl;

	m_colorMapTransformedWidth = m_colorMapTransformed.get_width_pixels();
	m_colorMapTransformedHeight = m_colorMapTransformed.get_height_pixels();
	std::cout << "m_colorMapTransformedWidth: " << m_colorMapTransformedWidth << std::endl;
	std::cout << "m_colorMapTransformedHeight: " << m_colorMapTransformedHeight << std::endl;


	m_numVerts = m_depthMapWidth * m_depthMapHeight;
	std::cout << "number of vertices: " << m_numVerts << std::endl;

	m_partsOfTriangle.reserve(m_numVerts);
}


void LiveMesh::smooth(int iterations)
{

	//std::cout << "ClothVisualizer::smooth()" << std::endl;

	// this vector stores the computed centers of gravity
	std::vector<myMesh::Point>  cogs;
	std::vector<myMesh::Point>::iterator cog_it;	//iterator for cog

	cogs.reserve(m_currMesh->n_vertices());

	// smoothing mesh with number of iterations



	myMesh::VertexIter          v_it;	//vertex iterator
	myMesh::VertexIter			v_end(m_currMesh->vertices_end()); //end iterator after last vertex in the mesh
	myMesh::VertexVertexIter    vv_it;	//iterator for one ring neighbors
	myMesh::Point               cog;	//current barycebter
	myMesh::Scalar              valence;//counts the nuber of a vertices neighbor
	unsigned int                i, N(iterations); //loop iterator and stop int
	//std::cout << "new frame" << std::endl;

	//iterations loop
	for (i = 0; i < N; ++i)
	{
		//std::cout << "new iteration" << std::endl;


		cogs.clear();
		int j = 0;
		//loop over all vertices in the mesh
		for (v_it = m_currMesh->vertices_begin(); v_it != v_end; ++v_it)
		{
			//reset current barycenter
			cog[0] = cog[1] = cog[2] = valence = 0.0;

			//if (j == 180000)
			//{
			//	std::cout << "center: " << m_currMesh->point(v_it).x() << ", " << m_currMesh->point(v_it).y() << ", " << m_currMesh->point(v_it).z() << std::endl;
			//}

			//loop over all vertices in one ring neighborhood
			for (vv_it = m_currMesh->vv_iter(*v_it); vv_it.is_valid(); ++vv_it)
			{
				//if (j == 180000)
				//{
				//	std::cout << "neighbor: " << m_currMesh->point(vv_it).x() << ", " << m_currMesh->point(vv_it).y() << ", " << m_currMesh->point(vv_it).z() << std::endl;
				//}


				//add neighbor position to cog vector
				cog += m_currMesh->point(*vv_it);
				//increment valence to count neighbors
				++valence;
			}
			//nomalize cog vector and add it to cog array
			cogs.push_back(cog / valence/* + osg::Vec3(0, 5, 0)*/);


			j++;

		}

		//std::cout << "j = " << j << std::endl;

		//loop over all calculated barycenters
		for (v_it = m_currMesh->vertices_begin(), cog_it = cogs.begin(); v_it != v_end; ++v_it, ++cog_it)
		{

			if (!m_currMesh->is_boundary(*v_it))
			{
				//std::cout << "before: " << m_currMesh->point(v_it).x() << ", " << m_currMesh->point(v_it).y() << ", " << m_currMesh->point(v_it).z() << std::endl;

				//set current vertex to barycenter of it's neighbors
				m_currMesh->set_point(*v_it, *cog_it);



				//std::cout << "after: " << m_currMesh->point(v_it).x() << ", " << m_currMesh->point(v_it).y() << ", " << m_currMesh->point(v_it).z() << std::endl;
			}
		}
	}
}

//TODO: Make call by reference, not by value
void LiveMesh::calcColorList(k4a::image colorMap)
{

	//get color buffer
	uint8_t* colorBuffer = colorMap.get_buffer();
	//get color size
	size_t color_buffer_size = colorMap.get_size();

	m_colorArrayCurrNotBinded->clear();

	//loop over all Colors
	for (size_t i = 0; i < color_buffer_size; i += 4)
	{

		//add color components to list (BGRA)
		//convert from int (0-255) to normalized float-value
		m_colorArrayCurrNotBinded->push_back(Color(colorBuffer[i + 2] / 256.0f, colorBuffer[i + 1] / 256.0f, colorBuffer[i] / 256.0f, colorBuffer[i + 3] / 256.0f));
	}

}


void LiveMesh::scaleDownColorList(int SourceWidth, int SourceHeight)
{

	/*

	Depth resolution: 640  x 576
	Color resolution: 2560 x 1440

	For every depth pixel, there are 4 color pixels horizontally and 2,5 vertically.
	The goal is to add all colors up and divide by 10 (number of total colors per depth pixel)
	Because of that 2,5 pixel vertical scaling, we have a shared color line,
	which must be added to both scaled pixels, but only by 50%.
	We therefore operate on double lines and calculate two vertical adjacant pixels in one go.

	*/


	int currColorIdx = 0;

	//loop over all vertex double lines in the depth image
	for (int currVertLine = 0; currVertLine < m_depthMapHeight; currVertLine += 2)
	{

		//loop over all vertices in a double line
		for (int currVertIdx = 0; currVertIdx < m_depthMapWidth; currVertIdx++)
		{

			//loop over every color line for current vertex
			for (int currLineIdx = 0; currLineIdx < 5; currLineIdx++)
			{
				//reset color line
				m_colorLines[currLineIdx] = Color(0, 0, 0, 0);

				//loop over every color pixel in a line
				for (int i = 0; i < 4; i++)
				{
					//add current pixel value to current color line
					m_colorLines[currLineIdx] += m_colorList[currColorIdx + currLineIdx * SourceWidth + i];

					m_colorLines[currLineIdx] /= 4;

				}
			}


			//add colors to upper pixel
			m_colorListScaledDown[currVertIdx * currVertLine + currVertIdx] = m_colorLines[0] + m_colorLines[1] + m_colorLines[2] * 0.5;

			//add colors to lower pixel
			m_colorListScaledDown[currVertIdx * currVertLine + currVertIdx + m_depthMapWidth] = m_colorLines[4] + m_colorLines[5] + m_colorLines[2] * 0.5;



			currColorIdx += 4;
		}



		currColorIdx += 4 * SourceWidth;

	}

}


void LiveMesh::transformDepthImage(k4a::image* input, k4a::image* output)
{
	try
	{
		//Adpat the depth image to the size, aspect ratio and intrinsic parameters of the color cam
		*output = k4a::image::create(K4A_IMAGE_FORMAT_DEPTH16,
			m_calibration.color_camera_calibration.resolution_width,
			m_calibration.color_camera_calibration.resolution_height,
			m_calibration.color_camera_calibration.resolution_width * static_cast<int>(sizeof(uint16_t)));

		m_transformation.depth_image_to_color_camera(*input, output);
	}
	catch (const std::exception& e)
	{
		std::cout << "ERROR transformDepthImage(): " << e.what() << std::endl;
	}

}


void LiveMesh::transformColorImage(k4a::image* input, k4a::image* output)
{

	//std::cout << "color output size before = " << output->get_size() << std::endl;

	//std::cout << "image format: " <<input->get_format()			<< std::endl;
	//std::cout << "image format: " <<input->get_width_pixels()	<< std::endl;
	//std::cout << "image format: " <<input->get_height_pixels()	<< std::endl;
	//std::cout << "image format: " <<input->get_stride_bytes()	<< std::endl;


	//Adpat the color image to the size, aspect ratio and intrinsic parameters of the depth cam
	//* output = k4a::image::create(K4A_IMAGE_FORMAT_COLOR_BGRA32, m_width, m_height, m_width * static_cast<int>(sizeof(uint32_t)));


	//std::cout << "m_numVerts = " << m_numVerts << std::endl;

	try
	{
		m_colorMapTransformed = m_transformation.color_image_to_depth_camera(m_depthMap, m_colorMap);
		//std::cout << "color output size after = " << m_colorMapTransformed.get_size() / 4 << ", nuber of vertices: " << m_numVerts << std::endl;

	}
	catch (const std::exception& e)
	{
		std::cerr << "ERROR transformColorImage(): " << e.what() << std::endl;
	}

	//std::cout << "color output size = " << output->get_size() << std::endl;
}


void LiveMesh::deleteLongEdges(float maxLength, myMesh* mesh)
{
	//prepares mesh for edge deletion
	mesh->request_edge_status();

	// iterate over all edges
	for (myMesh::EdgeIter e_it = mesh->edges_begin(); e_it != mesh->edges_end(); ++e_it)
	{
		//if edge is longer than maxLength
		if ((osg::Vec3f::value_type)maxLength < mesh->calc_edge_vector(e_it).length())
		{

			//flag edge as deleted
			mesh->delete_edge(e_it, false);

		}

	}

	//actually delete flagged edges from memory
	mesh->garbage_collection();

}

void LiveMesh::renderKinectColorImage(k4a::image* m_colorMap)
{

	//std::cout << "m_colorMap->get_width_pixels()" << m_colorMap->get_width_pixels() << std::endl;
	//m_image0->allocateImage(m_colorMapWidth, m_colorMapHeight, 1, GL_BGRA, GL_UNSIGNED_BYTE);
	m_image0->setImage(m_colorMap->get_width_pixels(),
		m_colorMap->get_height_pixels(),
		1,
		GL_RGBA,
		GL_BGRA,
		GL_UNSIGNED_BYTE,
		const_cast<unsigned char*>(static_cast<const unsigned char*>(m_colorMap->get_buffer())),
		osg::Image::AllocationMode::NO_DELETE);
}

void LiveMesh::useProjectionMapping(bool onOrOff)
{

	m_useProjectionMapping = onOrOff;

}

std::chrono::microseconds LiveMesh::getKinectCaptureTimestamp()
{
	m_mutex.lock();
	std::chrono::microseconds temp = m_kinectCapture->get_color_image().get_device_timestamp();
	m_mutex.unlock();
	return temp;

}

void LiveMesh::setStateToRendered()
{
	m_mutex.lock();
	m_currentLiveMeshState = RENDERED;
	m_mutex.unlock();
}