// ClayMore - Immersive Mesh Modelling --- Copyright (c) 2014-2017 Philipp Ladwig, Jannik Fiedler, Jan Beutgen
#pragma once

#include "ConfigDllExportPointCloudManagement.h"

namespace rs2 {
	class config;
}

class imPointCloudManagement_DLL_import_export RealSenseUtils
{
	// #### CONSTRUCTOR & DESTRUCTOR ###############
public:
	RealSenseUtils();
	~RealSenseUtils();
	
private:
	// #### MEMBER VARIABLES ###############
	// #### MEMBER FUNCTIONS ###############

	
public:
	static void setUpConfig(rs2::config* config, unsigned int width, unsigned int height, unsigned int fps);
	static void warmUpRealSenses(int cycles);
};