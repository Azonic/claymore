#pragma once

#include "ColorTypes.h"
#include "DepthTypes.h"
#include "PointcloudTypes.h"
#include "PoseDriver.h"

#include <atomic>
#include <string>

namespace cv {
	class Mat;
}

class ScanDevice {
public:
	// ### Return Messages ###
	enum class StartDeviceResult {
		SUCCESS,
		ERROR_DEVICE_NOT_FOUND,
		ERROR_IS_RUNNING,
		FAILURE
	};
	enum class StopDeviceResult {
		SUCCESS,
		DEVICE_IS_NOT_RUNNING,
		FAILURE
	};
	enum class ScanResult {
		SUCCESS,
		TIMEOUT,
		TIMESTAMP_ERROR,
		DEVICE_IS_NOT_RUNNING,
		DEVICE_IS_ALREADY_SCANNING,
		UNSUPPORTED_PROFILE,
		ALLOCATION_ERROR,
		TRANSFORMATION_ERROR,
		FAILURE
	};
	enum class PointcloudCalculationResult {
		SUCCESS,
		MISSING_SCAN_DATA,
		ALLOCATION_ERROR,
		TRANSFORMATION_ERROR,
		FAILURE
	};
	enum class DownsamplePointCloudResult {
		SUCCESS,
		NO_POINT_CLOUD_DATA,
		DOWNSAMPLING_IMPOSSIBLE,
		FAILURE
	};
	// ######
protected:
	/**
	 * @brief The timestamp in microseconds 
	*/
	unsigned long long m_systemClockOrigin;
	/**
	 * @brief The timestamp in microseconds of the hardware's device clock; acquired when the ScanDevice is started.
	*/
	unsigned long long m_deviceClockOrigin;
	/**
	 * @brief Running state of the ScanDevice. True if the device is currently running, false otherwise.
	*/
	std::atomic_bool m_isRunning;
	/**
	 * @brief Scanning state of the ScanDevice. True if the device is currently scanning, false otherwise.
	*/
	std::atomic_bool m_isScanning;
	std::string m_serial;
	std::string m_name;
	PoseDriver* m_poseDriver;
	ColorData::ColorFormat m_colorFormat;
	DepthData::DepthFormat m_depthFormat;
	float m_depthScale;
	// ### Camera Settings ###
	unsigned int m_colorWidth;
	unsigned int m_colorHeight;
	unsigned int m_fps;
	unsigned int m_pointCloudWidth;
	unsigned int m_pointCloudHeight;
	// ### Data from last scan ###
	unsigned long long m_systemTimestamp;
	unsigned long long m_deviceTimestamp;
	ColorData* m_colorData;
	DepthData* m_depthData;
	PointcloudData* m_pointcloudData;
	PointcloudData* m_pointcloudDataDownsampled;
	float* m_pointCloudVerticesDownsampled;
	float* m_pointCloudTexCoordsDownsampled;
	// ### Camera Parameter for usage with OpenCV ###
	cv::Mat* m_cameraMatrix;
	cv::Mat* m_distortionCoefficients;
public:
	ScanDevice() = delete;
	/**
	 * @brief Creates a new ScanDevice.
	 * @param poseDriver determines the pose associated with this ScanDevice
	 * @param serial of the connected device
	 * @param name of the connected device
	*/
	ScanDevice(PoseDriver* poseDriver, const std::string& serial, const std::string& name, cv::Mat* cameraMatrix, cv::Mat* distortionCoefficients, const unsigned int& colorWidth = 0u, const unsigned int& colorHeight = 0u, const unsigned int& pointCloudWidth = 0u, const unsigned int& pointCloudHeight = 0u, const unsigned int& fps = 0u, const float& depthScale = 1.f);
	/**
	 * @brief Releases all remaining data before destroying the ScanDevice
	*/
	virtual ~ScanDevice();
	/**
	 * @brief Starts the device. Must be called before the device can start scanning.
	 * @return 
	*/
	virtual StartDeviceResult start() = 0;
	/**
	 * @brief Stops the device. The device will not be able to scan until it is restarted.
	 * @return 
	*/
	virtual StopDeviceResult stop() = 0;
	/**
	 * @brief Initiates a scan. If successful new color and depth images will be stored in the members m_colorData and m_depthData respectively.
	 * 		  Previously collected data will be released on the next call or via releaseScan().
	 *		  This function has to be implemented for each ScanDevice separately. However, each ScanDevice should call the protected function scanStart() at the beginning and directly terminate if the returned value is unequal to ScanResult::SUCCESS.
	 *		  Before returning the result the protected function scanEnd() should be called, unless scanStart() already caused the termination of the function.
	 * @param ms_timeout timeout in milliseconds determining the time to wait for the device to scan
	 * @return Indicates whether the scan was successful or an error was encountered
	*/
	virtual ScanResult scan(const unsigned int ms_timeout = 1000u) = 0;
	/**
	 * @brief Initiates the calculation of the pointcloud data. For this to work a scan must have been successfully executed previously.
	 * @return 
	*/
	virtual PointcloudCalculationResult calculatePointcloud() = 0;
	ScanDevice::DownsamplePointCloudResult downsamplePointCloud();
	/**
	 * @brief Releases the remaining data of the previous scan and pointcloudCalculation.
	*/
	virtual void releaseScan();
	/**
	 * @brief Grants access to the collected color data of the latest scan.
	 * @return m_colorData - might be nullptr
	*/
	ColorData* getColorData();
	/**
	 * @brief Grants access to the collected depth data of the latest scan.
	 * @return m_depthData - might be nullptr
	*/
	DepthData* getDepthData();
	/**
	 * @brief Grants access to the latest generated pointcloud data
	 * @return m_pointcloudData - might be nullptr
	*/
	PointcloudData* getPointcloudData(const bool downsampled = true) const;
	/**
	 * @brief Grants access to the camera matrix of the device.
	 * @return CameraMatrix of the device
	*/
	const cv::Mat* getCameraMatrix() const;
	/**
	 * @brief Separates the current downsampled pointcloudData object from the current scanDevice.
	 *		  The vertices and texture coordinates have to be deleted with delete[].
	 * @return Pointer to the unhooked pointcloudData object.
	*/
	PointcloudData* unhookDownsampledPointcloud();
	/**
	 * @brief Grants access to the distortion coefficients of the device.
	 * @return DistortionCoefficients of the device
	*/
	const cv::Mat* getDistortionCoefficients() const;
	/**
	 * @brief Indicates whether the device has already been started.
	 * @return bool value of m_isRunning
	*/
	bool isRunning();
	/**
	 * @brief 
	 * @return Serial number of the connected device  
	*/
	const std::string& getSerial();
	/**
	 * @brief 
	 * @return Name of the connected device 
	*/
	const std::string& getName();
	/**
	 * @brief Requests the transform of the assigned poseDriver.
	 * @return Matrix to transform the pointcloud data from the local sensor coordinate system to the global coordinate system.
	*/
	osg::Matrix getTransform() const;
	/**
	 * @brief Requests the pose of the assigned poseDriver.
	 * @return Current pose of the ScanDevice in global coordinates
	*/
	osg::Matrix getPose() const;
	osg::Matrix ScanDevice::getSensorToDriverMatrix() const;
	PoseDriver* getDriver() const;
	/**
	 * @return The ColorFormat the device was instantiated with.
	*/
	ColorData::ColorFormat getColorFormat() const;
	/**
	 * @return The DepthFormat the device was instantiated with.
	*/
	DepthData::DepthFormat getDepthFormat() const;
	unsigned int getColorWidth() const;
	unsigned int getColorHeight() const;
	unsigned int getPointCloudWidth(bool downsampled = true) const;
	unsigned int getPointCloudHeight(bool downsampled = true) const;
	/**
	 * @return The device timestamp in micorseconds corresponding to the the middle of exposure time of the most recent scan.
	*/
	unsigned long long getDeviceTimestamp();
	/**
	 * @return The system timestamp in microseconds corresponding to the the middle of exposure time of the most recent scan.
	*/
	unsigned long long getSystemTimestamp();
protected:
	/**
	 * @brief Attempt to start the device. If successful m_isRunning will be set to true in the same action via compare and exchange.
	 * @return true if the attempt was successful, false if the device was already running
	*/
	bool attemptToStart();
	/**
	 * @brief Transforms a device timestamp to a system timestamp.
	 * @param deviceTimestamp Timestamp in microseconds to convert
	 * @return System timestamp in microseconds
	*/
	unsigned long long deviceToSystemClock(unsigned long long deviceTimestamp);
	/**
	 * @brief Performs security checks to prevent false access to the underlying library or the execution of a scan on a device that is currently already scanning. In each implementation of scan() scanStart() should always be called first. 
	 *		  If the result is ScanResult::SUCCESS, it is safe to proceed with the scan. The device will then be marked as 'isScanning', which must be revoked by calling scanEnd() later.
	 *        If the result is not ScanResult::SUCCESS, scan() should terminate immediately returning the given result. In this case there is no need for calling scanEnd().
	 * @return Returns ScanResult::SUCCESS if no error was encountered.
	*/
	ScanDevice::ScanResult scanStart();
	/**
	 * @brief Executes the general cleanup functionality necessary for each ScanDevice after each scan, independant of the specific underlying library. 
	 *		  Unless scanStart() did not return ScanResult::SUCCESS, scanEnd() should always be called before returning the result of the scan.
	 * @param scanResult is the result that scan() will return; Unless it is equal to ScanResult::SUCCESS, releaseScan() will be called.
	 * @param releaseScan may be set to false to prevent releaseScan() from being called.
	 * @return Returns the value of the input parameter scanResult for convenience. This value is inteded to be returned by scan() after scanEnd() was executed.
	*/
	ScanDevice::ScanResult scanEnd(const ScanDevice::ScanResult scanResult, bool releaseScan = true);
private:
	void downsamplePointCloudPartial(const unsigned int yStart, const unsigned int ySlice);
};