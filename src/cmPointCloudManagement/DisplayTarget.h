#pragma once

#include <osg/geode>

class DisplayTarget {
private:
	osg::ref_ptr<osg::Geode> m_geode;
	osg::ref_ptr<osg::Geometry> m_geom;
	osg::ref_ptr<osg::Vec4Array> m_colors;
	osg::ref_ptr<osg::DrawArrays> m_drawArrays;
	osg::ref_ptr<osg::Vec3Array> m_vertices;
	bool m_isDirty;
	bool m_isDisplayed;
public:
	DisplayTarget(osg::ref_ptr<osg::MatrixTransform> matTrans, const bool isOverlay = false);
	~DisplayTarget();
	void drawLine(const osg::Vec3& start, const osg::Vec3& end, const osg::Vec4& colorStart, const osg::Vec4& colorEnd);
	void drawRay(const osg::Vec3& origin, const osg::Vec3& dir, const float length, const osg::Vec4& colorStart, const osg::Vec4& colorEnd);
	void setLine(const unsigned int index, const osg::Vec3& start, const osg::Vec3& end, const osg::Vec4& colorStart, const osg::Vec4& colorEnd);
	void setLine(const unsigned int index, const osg::Vec3& origin, const osg::Vec3& dir, const float length, const osg::Vec4& colorStart, const osg::Vec4& colorEnd);
	void drawQuad(const osg::Vec3& origin, osg::Vec3 normal, osg::Vec3 forward, const osg::Vec2 extents, const osg::Vec4& color);
	void drawBox(const osg::Vec3& origin, osg::Vec3 normal, osg::Vec3 forward, const osg::Vec3 extents, const osg::Vec4& color);
	void drawEmpty(const osg::Vec3& point, const float extent, const osg::Vec4& color);
	void redraw();
	void clear();
	inline size_t size() { return m_vertices->size(); };
	bool toggleDisplayState();
	void setDisplayState(bool displayState);
	bool getDisplayState();
};