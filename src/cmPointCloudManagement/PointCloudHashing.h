#pragma once

#include <iostream>
#include <xhash>
#include <osg/Vec3f>

/*struct Vec3fKey 
{
private:
	//Counts how many points have been identified with this specific key.
	int index;
	//Coordinates for the center of the gridUnit. These are used to generate the HashCode.
	float x, y, z;

public:
	Vec3fKey();
	Vec3fKey(float x, float y, float z);
	Vec3fKey(const osg::Vec3f& vec);
	
	float X() const;
	float Y() const;
	float Z() const;

	void AddVec3f(osg::Vec3f& vec, const osg::Vec3f& vecNew);

	bool Compare(float x, float y, float z) const;
	bool operator==(const Vec3fKey& other) const;
	bool operator==(const osg::Vec3f& other) const;
};

struct Vec3fHasher
{
	static float RoundFloat(const float value, const float gridSize, const float gridOffset);
	static std::size_t HashCode(float x, float y, float z);
	std::size_t operator()(const osg::Vec3f& vec) const;
	std::size_t operator()(const Vec3fKey& vec) const;
};*/

float RoundFloat(float f);
osg::Vec3f RoundVector(float x, float y, float z);
osg::Vec3f RoundVector(const osg::Vec3f& v);
void RoundVectorSet(osg::Vec3f& v);

struct Vec3fHasher
{
	size_t operator()(const osg::Vec3f& v) const;
};

struct Vec3fComparer 
{
	bool operator()(const osg::Vec3f& v1, const osg::Vec3f& v2) const;
};