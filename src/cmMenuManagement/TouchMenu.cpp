// ClayMore - Immersive Mesh Modelling --- Copyright (c) 2014-2017 Philipp Ladwig, Jannik Fiedler, Jan Beutgen
#include "TouchMenu.h"
#include "TouchMenuButton.h"
#include <osg/LineWidth>

#include "cmUtil/Textcreator.h"

TouchMenu::TouchMenu(MenuName menuNameIn, float menuFloatingHeightIn, osg::Vec4f defaultColorIn, osg::Vec4f highlightColorIn, CONTROLLER_TYPE controllerType, float textSizeIn) :
	Menu(menuNameIn, controllerType, textSizeIn),
	m_menuFloatingHeight(menuFloatingHeightIn),
	m_defaultColor(defaultColorIn),
	m_highlightColor(highlightColorIn),
	m_animationState(0.0),
	m_startScaleValue(0.0),
	m_targetScaleValue(0.09),
	m_idleFrameCounter(0),
	m_framesBeforeClose(0)
{
	m_menuTransform->setPosition(osg::Vec3f(ConfigReader::TOUCH_MENU_CORRECTION_POS[0],
		ConfigReader::TOUCH_MENU_CORRECTION_POS[1],
		ConfigReader::TOUCH_MENU_CORRECTION_POS[2]));

	m_menuTransform->setScale(osg::Vec3f(m_startScaleValue, m_startScaleValue, m_startScaleValue));

	m_isHelpTextInitialised = false;

	Menu::m_defaultColor = defaultColorIn;
	Menu::m_highlightColor = highlightColorIn;
}

TouchMenu::~TouchMenu()
{

}

void TouchMenu::attachToSceneGraph(osg::ref_ptr<osg::Group> parentNodeIn)
{
	parentNodeIn->addChild(m_menuTransform);
	//OSGShadow Test
	//m_menuTransform->setNodeMask(0x2); //this are globals from main //TODO: Make global: Config Manager?
	m_menuTransform->addChild(m_menuGeode);
}

void TouchMenu::detachFromSceneGraph()
{
	if (m_menuTransform != nullptr)
	{
		std::vector<osg::Group*> parents = m_menuTransform->getParents();

		std::vector<osg::Group*>::iterator paIt = parents.begin();

		while (paIt != parents.end())
		{
			(*paIt)->removeChild(m_menuTransform);
			paIt++;
		}

		m_menuTransform->removeChild(m_menuGeode);
	}
}


void TouchMenu::updateAnimations(double deltaTimeIn)
{
	if (m_isVisible)
	{
		if (m_animationState < 1.0)
		{
			m_animationState += deltaTimeIn / 50.0 * pow(1.0 - m_animationState, 1.0) + 0.000001;
			m_animationState = m_animationState < 1.0 ? m_animationState : 1.0;

			float currentScaleValue = m_startScaleValue + (m_targetScaleValue - m_startScaleValue) * m_animationState;
			m_menuTransform->setScale(osg::Vec3f(currentScaleValue, currentScaleValue, currentScaleValue));
		}
	}
	else if (!m_isVisible)
	{
		if (m_animationState > 0.0)
		{
			m_animationState -= deltaTimeIn / 40.0 * pow(m_animationState, 1.5) + 0.000001;
			m_animationState = m_animationState > 0.0 ? m_animationState : 0.0;

			float currentScaleValue = m_startScaleValue + (m_targetScaleValue - m_startScaleValue) * m_animationState;
			m_menuTransform->setScale(osg::Vec3f(currentScaleValue, currentScaleValue, currentScaleValue));
		}
	}

	std::map<BUTTON_ID, MenuButton*>::iterator buIt = m_menuButtons.begin();

	while (buIt != m_menuButtons.end())
	{
		static_cast<TouchMenuButton*>((*buIt).second)->updateButtonAnimation(m_animationState, deltaTimeIn, m_startScaleValue, m_targetScaleValue);
		buIt++;
	}

	if (m_framesBeforeClose > 0)
		idleCounterTick();
}

void TouchMenu::idleCounterTick()
{
	if (m_idleFrameCounter < 1)
	{
		if (m_isVisible)
			hide();
	}
	else
		--m_idleFrameCounter;
}

void TouchMenu::show()
{
	m_isVisible = true;
	refreshIdleCounter();
}

void TouchMenu::hide()
{
	m_idleFrameCounter = 0;
	m_isVisible = false;
	hideAllSubMenus();
	unhighlightAllButtons();
}

void TouchMenu::hideAllSubMenus()
{
	std::map<BUTTON_ID, MenuButton*>::iterator buIt = m_menuButtons.begin();

	while (buIt != m_menuButtons.end())
	{
		(*buIt).second->hideSubmenu();
		buIt++;
	}

	m_submenuActiveAtButton = INVALID_BUTTON_ID;
}

void TouchMenu::unhighlightAllButtons()
{
	std::map<BUTTON_ID, MenuButton*>::iterator buIt = m_menuButtons.begin();

	while (buIt != m_menuButtons.end())
	{
		(*buIt).second->unhighlight();
		buIt++;
	}
}

BUTTON_ID TouchMenu::createButton(std::string const & buttonText)
{
	if (m_idCounter < 4)
	{
		m_menuButtons[m_idCounter] = new TouchMenuButton(m_idCounter, m_textSize, buttonText, Menu::m_defaultColor, Menu::m_highlightColor);

		m_menuButtons[m_idCounter]->attachToSceneGraph(m_menuTransform);
		return m_idCounter++;
	}
	else
	{
		if (ConfigReader::DEBUG_LEVEL >= NOT_IN_LOOPS)
			std::cout << "Error in TouchMenu::createButton - Only adding a miximum of 4 buttons to the TouchMenu is allowed!" << std::endl;
		return -1;
	}
}

void TouchMenu::createSubButton(BUTTON_ID parentId, std::string const & buttonText)
{
	//TODO: Create Subbutton functionality or delete it
	std::cout << "SubButtons are currently not supported by TouchMenu. Function is still available because of inheritance have-to-implements" << std::endl;

	std::map<BUTTON_ID, MenuButton *>::const_iterator parentIt = m_menuButtons.find(parentId);

	if (parentIt == m_menuButtons.cend())
	{
		if (ConfigReader::DEBUG_LEVEL >= NOT_IN_LOOPS)
			std::cout << "Error in Menu::createSubButton - Parent id of " << parentId << " not found!" << std::endl;
	}
	else
		parentIt->second->createSubButton(buttonText);
}

//TODO: Make this nicer / more efficient
MenuButton* const TouchMenu::getMenuButtonAtAngle(float angleIn)
{
	int buttonsCount = m_menuButtons.size();
	float buttonWidth = 2 * M_PI / (float)buttonsCount;

	angleIn -= (buttonWidth / 2.f);

	if (angleIn < 0)
		angleIn = (2 * M_PI) + angleIn;

	int buttonID = floorf(angleIn / buttonWidth) + 1;
	buttonID = buttonID < buttonsCount ? buttonID : 0;

	//std::cout << "buttonID from TouchMenu:" << buttonID << std::endl;
	//Prevent crash -  sometimes SteamVR reports -2147483648 as buttonID while starting ClayMore and SteamVR
	if (buttonID >= 0)
		return m_menuButtons.at(buttonID);
	else
		return m_menuButtons.at(0);
}

void TouchMenu::initializeDrawables(float height)
{
	std::map<BUTTON_ID, MenuButton*>::iterator buIt = m_menuButtons.begin();

	while (buIt != m_menuButtons.end())
	{
		static_cast<TouchMenuButton*>((*buIt).second)->initDrawable(m_menuButtons.size(), height);
		buIt++;
	}
}

void TouchMenu::setHelpText(std::string rightIn, std::string leftIn, std::string upIn, std::string downIn)
{
	m_helpTextRight = rightIn;
	m_helpTextLeft = leftIn;
	m_helpTextUp = upIn;
	m_helpTextDown = downIn;
}

void TouchMenu::renderHelpText(BUTTON_ID buttonID)
{
	if (m_isHelpTextInitialised)
	{
		if (buttonID == 0)
		{
			m_helMatTransform->removeChildren(0, m_helMatTransform->getNumChildren());
			m_helMatTransform->addChild(m_helpTextGeodeUp);
		}
		if (buttonID == 1)
		{
			m_helMatTransform->removeChildren(0, m_helMatTransform->getNumChildren());
			m_helMatTransform->addChild(m_helpTextGeodeRight);
		}
		if (buttonID == 2)
		{
			m_helMatTransform->removeChildren(0, m_helMatTransform->getNumChildren());
			m_helMatTransform->addChild(m_helpTextGeodeDown);
		}
		if (buttonID == 3)
		{
			m_helMatTransform->removeChildren(0, m_helMatTransform->getNumChildren());
			m_helMatTransform->addChild(m_helpTextGeodeLeft);
		}
	}
}

void TouchMenu::removeHelpText()
{
	if (m_helMatTransform != nullptr)
	{
		m_helMatTransform->removeChildren(0, m_helMatTransform->getNumChildren());
	}
}

void TouchMenu::initializeHelpText()
{
	if (!m_isHelpTextInitialised)
	{
		//Help Text ####################################
		m_helMatTransform = new osg::MatrixTransform();
		m_helpTextGeodeRight = new osg::Geode();
		m_helpTextGeodeLeft = new osg::Geode();
		m_helpTextGeodeUp = new osg::Geode();
		m_helpTextGeodeDown = new osg::Geode();

		m_helpTextGeodeRight->getOrCreateStateSet()->setMode(GL_LIGHTING, osg::StateAttribute::OVERRIDE | osg::StateAttribute::OFF);
		m_helpTextGeodeLeft->getOrCreateStateSet()->setMode(GL_LIGHTING, osg::StateAttribute::OVERRIDE | osg::StateAttribute::OFF);
		m_helpTextGeodeUp->getOrCreateStateSet()->setMode(GL_LIGHTING, osg::StateAttribute::OVERRIDE | osg::StateAttribute::OFF);
		m_helpTextGeodeDown->getOrCreateStateSet()->setMode(GL_LIGHTING, osg::StateAttribute::OVERRIDE | osg::StateAttribute::OFF);

		m_linesForDescriptionTextRight = new osg::Geometry();
		m_linesForDescriptionTextLeft = new osg::Geometry();
		m_linesForDescriptionTextUp = new osg::Geometry();
		m_linesForDescriptionTextDown = new osg::Geometry();

		m_helMatTransform->addChild(m_helpTextGeodeRight);
		m_helMatTransform->addChild(m_helpTextGeodeLeft);
		m_helMatTransform->addChild(m_helpTextGeodeUp);
		m_helMatTransform->addChild(m_helpTextGeodeDown);

		osg::Matrix matrix;
		matrix.makeRotate(M_PI, 1.0f, 0.0f, 0.0f);
		m_helMatTransform->setMatrix(matrix);
		m_menuTransform->addChild(m_helMatTransform);
		m_helpCharakterSize = 0.45;

		m_helpTextGeodeRight->addDrawable(m_linesForDescriptionTextRight);
		m_helpTextGeodeLeft->addDrawable(m_linesForDescriptionTextLeft);
		m_helpTextGeodeUp->addDrawable(m_linesForDescriptionTextUp);
		m_helpTextGeodeDown->addDrawable(m_linesForDescriptionTextDown);

		osg::ref_ptr<osg::Vec3Array> lineVerticesRight = new osg::Vec3Array;
		osg::ref_ptr<osg::Vec3Array> lineVerticesLeft = new osg::Vec3Array;
		osg::ref_ptr<osg::Vec3Array> lineVerticesUp = new osg::Vec3Array;
		osg::ref_ptr<osg::Vec3Array> lineVerticesDown = new osg::Vec3Array;
		osg::Vec3f firstVertex;
		osg::Vec3f secondVertex;
		osg::Vec4 color = osg::Vec4(1.0f, 0.0f, 0.0f, 1.0f);
		osg::ref_ptr<osg::DrawElementsUInt> lineIndicies = new osg::DrawElementsUInt(osg::PrimitiveSet::LINES, 0);
		osg::Vec4Array* colorArray = new osg::Vec4Array;

		m_helpTextGeodeRight->addDrawable(TextCreator::createText(osg::Vec3(2.1f, -1.0f, 0.0f), m_helpTextRight, m_helpCharakterSize, osgText::TextBase::LEFT_CENTER));
		firstVertex.set(osg::Vec3(2.0f, -1.0f, 0.0f));
		secondVertex.set(osg::Vec3(0.7f, -0.1f, 0.0f));
		color = osg::Vec4(1.0f, 0.0f, 0.0f, 1.0f);
		lineVerticesRight->push_back(firstVertex);
		lineVerticesRight->push_back(secondVertex);
		m_linesForDescriptionTextRight->setVertexArray(lineVerticesRight);

		m_helpTextGeodeLeft->addDrawable(TextCreator::createText(osg::Vec3(-2.1f, -1.0f, 0.0f), m_helpTextLeft, m_helpCharakterSize, osgText::TextBase::RIGHT_CENTER));
		firstVertex.set(osg::Vec3(-2.0f, -1.0f, 0.0f));
		secondVertex.set(osg::Vec3(-0.7f, -0.1f, 0.0f));
		color = osg::Vec4(1.0f, 0.0f, 0.0f, 1.0f);
		lineVerticesLeft->push_back(firstVertex);
		lineVerticesLeft->push_back(secondVertex);
		m_linesForDescriptionTextLeft->setVertexArray(lineVerticesLeft);

		m_helpTextGeodeUp->addDrawable(TextCreator::createText(osg::Vec3(0.0f, -1.0f, 2.2f), m_helpTextUp, m_helpCharakterSize, osgText::TextBase::CENTER_CENTER));
		firstVertex.set(osg::Vec3(0.0f, -1.0f, 2.0f));
		secondVertex.set(osg::Vec3(0.0f, -0.1f, 0.7f));
		color = osg::Vec4(1.0f, 0.0f, 0.0f, 1.0f);
		lineVerticesUp->push_back(firstVertex);
		lineVerticesUp->push_back(secondVertex);
		m_linesForDescriptionTextUp->setVertexArray(lineVerticesUp);

		m_helpTextGeodeDown->addDrawable(TextCreator::createText(osg::Vec3(0.0f, -1.0f, -2.2f), m_helpTextDown, m_helpCharakterSize, osgText::TextBase::CENTER_CENTER));
		firstVertex.set(osg::Vec3(0.0f, -1.0f, -2.0f));
		secondVertex.set(osg::Vec3(0.0f, -0.1f, -0.7f));
		color = osg::Vec4(1.0f, 0.0f, 0.0f, 1.0f);
		lineVerticesDown->push_back(firstVertex);
		lineVerticesDown->push_back(secondVertex);
		m_linesForDescriptionTextDown->setVertexArray(lineVerticesDown);

		for (int i = 0; i < 2; i++)
		{
			lineIndicies->push_back(i);
			i++;
			lineIndicies->push_back(i);
			m_linesForDescriptionTextRight->addPrimitiveSet(lineIndicies);
			m_linesForDescriptionTextLeft->addPrimitiveSet(lineIndicies);
			m_linesForDescriptionTextUp->addPrimitiveSet(lineIndicies);
			m_linesForDescriptionTextDown->addPrimitiveSet(lineIndicies);
			colorArray->push_back(color);
			colorArray->push_back(color);
		}

		m_linesForDescriptionTextRight->setColorArray(colorArray);
		m_linesForDescriptionTextLeft->setColorArray(colorArray);
		m_linesForDescriptionTextUp->setColorArray(colorArray);
		m_linesForDescriptionTextDown->setColorArray(colorArray);

		m_linesForDescriptionTextRight->setColorBinding(osg::Geometry::BIND_PER_VERTEX);
		m_linesForDescriptionTextLeft->setColorBinding(osg::Geometry::BIND_PER_VERTEX);
		m_linesForDescriptionTextUp->setColorBinding(osg::Geometry::BIND_PER_VERTEX);
		m_linesForDescriptionTextDown->setColorBinding(osg::Geometry::BIND_PER_VERTEX);

		m_isHelpTextInitialised = true;
	}
}