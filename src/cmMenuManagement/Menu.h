// ClayMore - Immersive Mesh Modelling --- Copyright (c) 2014-2017 Philipp Ladwig, Jannik Fiedler, Jan Beutgen
#pragma once

#include "ConfigDllExportMenuManagement.h"

#include "MenuButton.h"
#include "../cmUtil/ConfigReader.h"

#include <osg/PositionAttitudeTransform>
#include <osg/Geode>

#include "cmUtil/clayMoreGlobals.h"

//Unique Menu Menu
enum MenuName
{
	TILT_MODE_SELECT,

	FENCE_GENERAL_COMMAND,

	DART_OBJMODE_TOOLS,
	FENCE_OBJMODE_DETAIL_COMMAND,
	TOUCH_OBJMODE_MAIN,
	TOUCH_OBJMODE_TRANS_ROT_SCALE,
	TOUCH_OBJMODE_SELECTION,
	FENCE_OBJMODE_ADD_MESH,
	
	DART_EDIMODE_TOOLS,
	FENCE_EDIMODE_DETAIL_COMMAND,
	FENCE_DELETE_MESH_ELEMENT_QUESTION_COMMAND,
	FENCE_EDIMODE_ADD_PRIMITIVE,
	TOUCH_EDIMODE_MAIN,
	TOUCH_EDIMODE_TRANS_ROT_SCALE,
	TOUCH_EDIMODE_SELECTION,
	TOUCH_EDIMODE_ADD_MESH_ELEMENTS,
	TOUCH_EDIMODE_PROBEDIT,

	TOUCH_CONSTRAINT,

	TOUCH_SET_SPECTATOR_CAM,

	TILT_AND_SHIFT_MODE,
	
	FENCE_SCAN_GENERAL_COMMAND,
	TOUCH_SCAN_POINT_CLOUD,
	TOUCH_SCAN_OCTREE,
	TOUCH_SCAN_POLYGONIZE,
	DART_SCAN_POINT_CLOUD_TOOLS,
	DART_SCAN_OCTREE_TOOLS,
	DART_SCAN_POLYGONIZE_TOOLS,
	TILT_AND_SHIFT_SCAN_MODE,

	// Maybe a very simple work-around to the TODO in line 46 in the MenuManager.cpp: 
	// If this element always is the last element in the enum, adding several new menu types before this element, won't affect the remaining code
	// Technically it's still the exact same for-loop, but testing against ENUM_END might be more clear than testing against TILT_AND_SHIFT_MODE while still keeping the simplicity of the code)
	ENUM_END
};

class imMenuManagement_DLL_import_export Menu
{
	// #### CONSTRUCTOR & DESTRUCTOR ###############

public:

	Menu(MenuName menuNameIn, CONTROLLER_TYPE controllerType, float textSizeIn);
	~Menu();

	// #### PUBLIC METHODS ################

	virtual void updateAnimations(double deltaTimeIn) = 0;

	virtual void attachToSceneGraph(osg::ref_ptr<osg::Group> parentNodeIn) = 0;
	virtual void detachFromSceneGraph() = 0;

	virtual void show() = 0;
	virtual void hide() = 0;
	inline bool isVisible() { return m_isVisible; }

	inline MenuName getMenuName() { return m_menuName; }
	inline void setPosition(osg::Vec3f* positionIn) { m_menuTransform->setPosition(*positionIn); }
	inline void setRotation(osg::Quat* rotationIn) { m_menuTransform->setAttitude(*rotationIn); }
	inline osg::ref_ptr<osg::PositionAttitudeTransform> getMenuTransform() { return m_menuTransform; }

	virtual void hideAllSubMenus() = 0;
	inline void setSubmenuActiveAtButton(BUTTON_ID buttonID) { m_submenuActiveAtButton = buttonID; }

	virtual void unhighlightAllButtons() = 0;

	MenuButton * getMenuButton(BUTTON_ID id);

	virtual BUTTON_ID createButton(std::string const & buttonText) = 0;
	virtual void createSubButton(BUTTON_ID parentId, std::string const & buttonText) = 0;

	inline unsigned int getNumberOfButtons() { return m_menuButtons.size(); }

	inline CONTROLLER_TYPE getControllerType() { return m_controllerType; }

	// #### PROTECTED VARIABLES ###############
protected:
	osg::ref_ptr<osg::PositionAttitudeTransform>	m_menuTransform;
	std::map<BUTTON_ID, MenuButton *>	m_menuButtons;
	unsigned int						m_idCounter;
	osg::ref_ptr<osg::Geode>			m_menuGeode;

	BUTTON_ID							m_submenuActiveAtButton;

	unsigned int m_buttonCountOnRootLevel; //Gets set via the config file
	bool		m_isVisible;
	float m_textSize;

	osg::Vec4f m_defaultColor;
	osg::Vec4f m_highlightColor;

	// #### MEMBER VARIABLES ###############
private:
	MenuName m_menuName;
	CONTROLLER_TYPE m_controllerType;
};