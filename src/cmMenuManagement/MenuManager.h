// ClayMore - Immersive Mesh Modelling --- Copyright (c) 2014-2017 Philipp Ladwig, Jannik Fiedler, Jan Beutgen
#pragma once

#include "ConfigDllExportMenuManagement.h"
#include "Menu.h"
#include "PieMenu.h"
#include "TiltMenu.h"
#include "DartMenu.h"
#include "FenceMenu.h"
#include "TouchMenu.h"

#include <osg/Group>

class imMenuManagement_DLL_import_export MenuManager
{
	// #### CONSTRUCTOR & DESTRUCTOR ###############

	public:
		MenuManager();
		~MenuManager();

	// #### PUBLIC METHODS ################
		void addMenuForUniqueName(Menu* menuIn);
		void showMenuWithUniqueName(MenuName menuNameIn);
		void hideMenuWithUniqueName(MenuName menuNameIn);
		void hideAllMenus();
		void hideAllPrimaryMenus();
		Menu* getMenuWithUniqueName(MenuName menuNameIn);

		void updateMenuAnimations(double deltaTimeIn);

	// #### MEMBER VARIABLES ###############
	private:

		std::map <MenuName, Menu*>		m_menuMap;
		osg::ref_ptr<osg::Group>		m_rootMenuNode;


	// #### MEMBER METHODS ################
};