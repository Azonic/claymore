// ClayMore - Immersive Mesh Modelling --- Copyright (c) 2014-2017 Philipp Ladwig, Jannik Fiedler, Jan Beutgen
#include "DartMenu.h"
#include <osg/LineWidth>

#include "../cmUtil/ConfigReader.h"

DartMenu::DartMenu(ConfigReader* configReaderIn,
	MenuName menuNameIn,
	osg::Vec4f defaultColorIn,
	osg::Vec4f highlightColorIn,
	osg::Vec4f menuPointerColorIn,
	CONTROLLER_TYPE controllerType,
	float textSizeIn) :
	Menu(menuNameIn, controllerType, textSizeIn),
	m_configReader(configReaderIn),
	m_animationState(0.0),
	m_startScaleValue(0.015f),
	m_targetScaleValue(0.15f),
	m_idleFrameCounter(0),
	m_framesBeforeClose(0)
{
	Menu::m_defaultColor = defaultColorIn;
	Menu::m_highlightColor = highlightColorIn;

	m_menuTransform->setAttitude(osg::Quat(M_PI_2, osg::Vec3d(1.0,0.0,0.0)));
	m_menuTransform->setPosition(osg::Vec3f(ConfigReader::TILT_SHIFT_MENU_CORRECTION_POS[0],
		ConfigReader::TILT_SHIFT_MENU_CORRECTION_POS[1],
		ConfigReader::TILT_SHIFT_MENU_CORRECTION_POS[2]));
	m_menuTransform->setScale(osg::Vec3f(m_startScaleValue, m_startScaleValue, m_startScaleValue));

	m_drawableControllerPointer = new osg::Geometry();
	m_geodeControllerPointer = new osg::Geode();
	m_geodeControllerPointer->addDrawable(m_drawableControllerPointer);

	m_controllerPointerVertices = new osg::Vec3Array;
	m_controllerPointerVertices->push_back(osg::Vec3f(0.0, -0.1, 0.0));
	m_controllerPointerVertices->push_back(osg::Vec3f(0.0, -0.1, 0.0));
	m_controllerPointerVertices->push_back(osg::Vec3f(0.0, -0.1, 0.0));
	m_controllerPointerVertices->push_back(osg::Vec3f(0.0, -0.1, 0.0));
	m_drawableControllerPointer->setVertexArray(m_controllerPointerVertices);

	m_contrPointerLine = new osg::DrawElementsUInt(osg::PrimitiveSet::QUADS, 0);
	m_contrPointerLine->push_back(3);
	m_contrPointerLine->push_back(2);
	m_contrPointerLine->push_back(1);
	m_contrPointerLine->push_back(0);
	m_drawableControllerPointer->addPrimitiveSet(m_contrPointerLine);

	osg::LineWidth* lineWidthPointer = new osg::LineWidth();
	lineWidthPointer->setWidth(2.0);
	m_geodeControllerPointer->getOrCreateStateSet()->setAttributeAndModes(lineWidthPointer, osg::StateAttribute::OVERRIDE | osg::StateAttribute::ON);
	m_geodeControllerPointer->getOrCreateStateSet()->setMode(GL_LIGHTING, osg::StateAttribute::OFF);

	m_colorArrayControllerPointer = new osg::Vec4Array;
	m_colorArrayControllerPointer->push_back(menuPointerColorIn);
	m_colorArrayControllerPointer->setBinding(osg::Array::BIND_OVERALL);
	m_drawableControllerPointer->setColorArray(m_colorArrayControllerPointer);

	m_menuTransform->addChild(m_geodeControllerPointer);
}

DartMenu::~DartMenu()
{
}

void DartMenu::setPosition(osg::Vec3d & position)
{
	m_menuTransform->setPosition(position);
}

void DartMenu::setRotation(osg::Quat & rotation)
{
	m_menuTransform->setAttitude(osg::Quat(M_PI_2, osg::Vec3d(1.0, 0.0, 0.0)) * rotation);
}

void DartMenu::updateAnimations(double deltaTimeIn)
{
	if (m_isVisible)
	{
		if (m_animationState < 1.0)
		{
			m_animationState += deltaTimeIn / 100.0 * pow(1.0 - m_animationState, 1.5) + 0.000001;
			m_animationState = m_animationState < 1.0 ? m_animationState : 1.0;

			float currentScaleValue = m_startScaleValue + (m_targetScaleValue - m_startScaleValue) * m_animationState;
			m_menuTransform->setScale(osg::Vec3f(currentScaleValue, currentScaleValue, currentScaleValue));
		}
	}
	else if (!m_isVisible)
	{
		if (m_animationState > 0.0)
		{
			m_animationState -= deltaTimeIn / 40.0 * pow(m_animationState, 1.5) + 0.000001;
			m_animationState = m_animationState > 0.0 ? m_animationState : 0.0;

			float currentScaleValue = m_startScaleValue + (m_targetScaleValue - m_startScaleValue) * m_animationState;
			m_menuTransform->setScale(osg::Vec3f(currentScaleValue, currentScaleValue, currentScaleValue));
		}
	}

	std::map<BUTTON_ID, MenuButton*>::iterator buIt = m_menuButtons.begin();

	while (buIt != m_menuButtons.end())
	{
		(*buIt).second->updateButtonAnimation(m_animationState, deltaTimeIn, m_startScaleValue, m_targetScaleValue);
		buIt++;
	}

	if (m_framesBeforeClose > 0)
		idleCounterTick();
}

void DartMenu::idleCounterTick()
{
	if (m_idleFrameCounter < 1)
	{
		if (m_isVisible)
			hide();
	}
	else
		--m_idleFrameCounter;
}


void DartMenu::attachToSceneGraph(osg::ref_ptr<osg::Group> parentNodeIn)
{
	parentNodeIn->addChild(m_menuTransform);
	//OSGShadow Test
	//m_menuTransform->setNodeMask(0x2); //this are globals from main //TODO: Make global: Config Manager?
	m_menuTransform->addChild(m_menuGeode);
}

void DartMenu::detachFromSceneGraph()
{
	if (m_menuTransform != nullptr)
	{
		std::vector<osg::Group*> parents = m_menuTransform->getParents();

		std::vector<osg::Group*>::iterator paIt = parents.begin();

		while (paIt != parents.end())
		{
			(*paIt)->removeChild(m_menuTransform);
			paIt++;
		}

		m_menuTransform->removeChild(m_menuGeode);
	}
}

void DartMenu::show()
{
	m_isVisible = true;

	//	osg::Matrix geomMatrix = osg::computeLocalToWorld(m_menuTransform->getParentalNodePaths()[0]);
	//and take the inverse with the start and end point to retain global points
	//rulerLineVertices->push_back(m_startPoint * osg::Matrix::inverse(m_geomMatrix));

	//m_menuTransform->setAttitude(osg::Quat(ConfigReader::TILT_SHIFT_MENU_CORRECTION_QUAT[0],
	//	osg::Vec3d(ConfigReader::TILT_SHIFT_MENU_CORRECTION_QUAT[1],
	//		ConfigReader::TILT_SHIFT_MENU_CORRECTION_QUAT[2],
	//		ConfigReader::TILT_SHIFT_MENU_CORRECTION_QUAT[3])));

	//osg::Quat test = osg::Quat(m_menuTransform->getAttitude().x(), 
	//	0.0,
	//	0.0, 
	//	0.0);
	//m_menuTransform->setAttitude(test);
	refreshIdleCounter();
}

//void TiltMenu::showAtPositionAndRotation(osg::Vec3 position, osg::Quat rotation)
//{
//	m_isVisible = true;
//	m_menuTransform->setPosition(position + osg::Vec3f(ConfigReader::TILT_SHIFT_MENU_CORRECTION_POS[0],
//		ConfigReader::TILT_SHIFT_MENU_CORRECTION_POS[1],
//		ConfigReader::TILT_SHIFT_MENU_CORRECTION_POS[2]));
//	m_menuTransform->setAttitude(rotation);
//
//	//	m_menuTransform->setAttitude(rotation * osg::Quat(ConfigReader::TILT_SHIFT_MENU_CORRECTION_QUAT[0],
//	//osg::Vec3d(ConfigReader::TILT_SHIFT_MENU_CORRECTION_QUAT[1],
//	//	ConfigReader::TILT_SHIFT_MENU_CORRECTION_QUAT[2],
//	//	ConfigReader::TILT_SHIFT_MENU_CORRECTION_QUAT[3])).inverse());
//
//	refreshIdleCounter();
//}

void DartMenu::rotateMenuBackOnYAxis(osg::Quat quat)
{
	osg::Quat xRot;
	xRot.makeRotate(osg::PI_2 * 0.75, osg::X_AXIS);
	osg::Quat fullRot = xRot * quat;

	m_menuTransform->setAttitude(fullRot);

	//This should always be the same
	//std::cout << "#######################x: " << m_menuTransform->getAttitude().x() 
	//	<< " y: " << m_menuTransform->getAttitude().y() 
	//	<< " z: " << m_menuTransform->getAttitude().z() 
	//	<< " w: " << m_menuTransform->getAttitude().w() << std::endl;
}

void DartMenu::hide()
{
	m_idleFrameCounter = 0;
	m_isVisible = false;
	hideAllSubMenus();
	unhighlightAllButtons();
}

void DartMenu::hideAllSubMenus()
{
	std::map<BUTTON_ID, MenuButton*>::iterator buIt = m_menuButtons.begin();

	while (buIt != m_menuButtons.end())
	{
		(*buIt).second->hideSubmenu();
		buIt++;
	}

	m_submenuActiveAtButton = INVALID_BUTTON_ID;
}

void DartMenu::unhighlightAllButtons()
{
	std::map<BUTTON_ID, MenuButton*>::iterator buIt = m_menuButtons.begin();

	while (buIt != m_menuButtons.end())
	{
		(*buIt).second->unhighlight();
		buIt++;
	}
}

BUTTON_ID DartMenu::createButton(std::string const & buttonText)
{
	m_menuButtons[m_idCounter] = new DartMenuButton(m_idCounter, m_textSize, 0, buttonText, Menu::m_defaultColor, Menu::m_highlightColor);
	m_menuButtons[m_idCounter]->attachToSceneGraph(m_menuTransform);
	return m_idCounter++;
}

void DartMenu::createSubButton(BUTTON_ID parentId, std::string const & buttonText)
{
	std::map<BUTTON_ID, MenuButton *>::const_iterator parentIt = m_menuButtons.find(parentId);

	if (parentIt == m_menuButtons.cend())
	{
		if (ConfigReader::DEBUG_LEVEL >= NOT_IN_LOOPS)
			std::cout << "Error in Menu::createSubButton - Parent id of " << parentId << " not found!" << std::endl;
	}
	else
		parentIt->second->createSubButton(buttonText);
}

//TODO: Make this nicer / more efficient
MenuButton* const DartMenu::getMenuButtonAtAngle(float angleIn)
{
	int buttonsCount = m_menuButtons.size();
	float buttonWidth = 2 * M_PI / (float)buttonsCount;

	angleIn -= (buttonWidth / 2.f);

	if (angleIn < 0)
		angleIn = (2 * M_PI) + angleIn;

	int buttonID = floorf(angleIn / buttonWidth) + 1;
	buttonID = buttonID < buttonsCount ? buttonID : 0;

	//std::cout << "buttonID" << buttonID << std::endl;

	if (m_submenuActiveAtButton != INVALID_BUTTON_ID)
		return m_menuButtons.at(m_submenuActiveAtButton)->getSubButton(buttonID);

	return m_menuButtons.at(buttonID);
}

void DartMenu::initializeDrawables(float height)
{
	std::map<BUTTON_ID, MenuButton*>::iterator buIt = m_menuButtons.begin();

	while (buIt != m_menuButtons.end())
	{
		static_cast<DartMenuButton*>((*buIt).second)->initDrawable(m_menuButtons.size(), height - 1.0f);
		buIt++;
	}
}

void DartMenu::activateBackground()
{
	m_backgroundGeometry = new osg::Geometry();

	unsigned int pointsPerCircle = ConfigReader::PIE_MENU_RESOLUTION;

	//Define vertices
	osg::ref_ptr<osg::Vec3Array> vertices = new osg::Vec3Array();
	osg::ref_ptr<osg::DrawElementsUInt> drawElements = new osg::DrawElementsUInt(osg::PrimitiveSet::TRIANGLE_FAN, 0);

	vertices->push_back(osg::Vec3f(0.0f, 0.0f, 0.0f));
	drawElements->push_back(0);
	float angle = 0.0f;
	for (unsigned int i = 0; i <= pointsPerCircle; ++i)
	{
		angle = (float)i / (float)pointsPerCircle * (M_PI * 2.0f);
		vertices->push_back(osg::Vec3f(cosf(angle), 0.0f, sinf(angle)));
		drawElements->push_back(i + 1);
	}

	m_backgroundGeometry->setVertexArray(vertices);
	m_backgroundGeometry->addPrimitiveSet(drawElements);


	//Define colorArray (white with BIND_OVERALL)
	osg::ref_ptr<osg::Vec4Array> colors = new osg::Vec4Array;
	colors->push_back(osg::Vec4(0.1f, 0.1f, 0.1f, 0.95f));
	m_backgroundGeometry->setColorArray(colors);
	m_backgroundGeometry->setColorBinding(osg::Geometry::BIND_OVERALL);

	//Create StateSet
	osg::ref_ptr<osg::StateSet> stateSet = new osg::StateSet();
	m_backgroundGeometry->setStateSet(stateSet);

	//Disable lighting, enable transparency
	stateSet->setMode(GL_LIGHTING, osg::StateAttribute::OFF);
	//stateSet->setMode(GL_BLEND, osg::StateAttribute::ON);
	//stateSet->setRenderingHint(osg::StateSet::TRANSPARENT_BIN);
	//stateSet->setRenderBinDetails(14, "DepthSortedBin");

	//osg::ref_ptr<osg::CullFace> cullFace = new osg::CullFace();
	//cullFace->setMode(osg::CullFace::FRONT);
	//stateSet->setAttributeAndModes(cullFace, osg::StateAttribute::ON);

	m_menuGeode->addDrawable(m_backgroundGeometry);
}

void DartMenu::renderControllerPointer(osg::Vec2f menuCoordSysOffsetFromController)
{
	m_controllerPointerVertices->clear();
	m_controllerPointerVertices->push_back(osg::Vec3f(menuCoordSysOffsetFromController.x() - 0.015, -0.99f, menuCoordSysOffsetFromController.y() - 0.015));
	m_controllerPointerVertices->push_back(osg::Vec3f(menuCoordSysOffsetFromController.x() + 0.015, -0.99f, menuCoordSysOffsetFromController.y() - 0.015));
	m_controllerPointerVertices->push_back(osg::Vec3f(menuCoordSysOffsetFromController.x() + 0.015, -0.99f, menuCoordSysOffsetFromController.y() + 0.015));
	m_controllerPointerVertices->push_back(osg::Vec3f(menuCoordSysOffsetFromController.x() - 0.015, -0.99f, menuCoordSysOffsetFromController.y() + 0.015));
	
	//OBSCURE: VertexArray must  call dirty() and must be set again, otherwise no render update:
	m_controllerPointerVertices->dirty();
	m_drawableControllerPointer->setVertexArray(m_controllerPointerVertices);
}
