// ClayMore - Immersive Mesh Modelling --- Copyright (c) 2014-2017 Philipp Ladwig, Jannik Fiedler, Jan Beutgen
#include "FenceMenu.h"
#include "FenceMenuButton.h"
#include <osg/LineWidth>

#include "../cmUtil/ConfigReader.h"

FenceMenu::FenceMenu(ConfigReader* configReaderIn,
	MenuName menuNameIn,
	osg::Vec4f defaultColorIn,
	osg::Vec4f highlightColorIn,
	osg::Vec4f menuPointerColorIn,
	CONTROLLER_TYPE controllerType,
	float textSizeIn,
	float buttonHeight,
	float buttonWidth,
	float buttonSpacerHeight,
	float buttonTextYOffset) :
	Menu(menuNameIn, controllerType, textSizeIn),
	m_configReader(configReaderIn),
	m_animationState(0.0),
	m_startScaleValue(0.0),
	m_targetScaleValue(0.09),
	m_idleFrameCounter(0),
	m_framesBeforeClose(0),
	m_buttonHeight(buttonHeight),
	m_buttonWidth(buttonWidth),
	m_buttonSpacerHeight(buttonSpacerHeight),
	m_buttonTextYOffset(buttonTextYOffset)
{
	m_defaultColor = defaultColorIn;
	m_highlightColor = highlightColorIn;

	m_menuTransform->setScale(osg::Vec3f(m_startScaleValue, m_startScaleValue, m_startScaleValue));

	m_drawableControllerPointer = new osg::Geometry();
	m_geodeControllerPointer = new osg::Geode();
	m_geodeControllerPointer->addDrawable(m_drawableControllerPointer);

	m_controllerPointerVertices = new osg::Vec3Array;
	m_controllerPointerVertices->push_back(osg::Vec3f(0.0, 0.0, 0.0 + 0.01));
	m_controllerPointerVertices->push_back(osg::Vec3f(0.0, 0.1, 0.0));
	m_drawableControllerPointer->setVertexArray(m_controllerPointerVertices);

	m_contrPointerLine = new osg::DrawElementsUInt(osg::PrimitiveSet::LINES, 0);
	m_contrPointerLine->push_back(1);
	m_contrPointerLine->push_back(0);
	m_drawableControllerPointer->addPrimitiveSet(m_contrPointerLine);

	osg::LineWidth* lineWidthPointer = new osg::LineWidth();
	lineWidthPointer->setWidth(2.0);
	m_geodeControllerPointer->getOrCreateStateSet()->setAttributeAndModes(lineWidthPointer, osg::StateAttribute::OVERRIDE | osg::StateAttribute::ON);
	m_geodeControllerPointer->getOrCreateStateSet()->setMode(GL_LIGHTING, osg::StateAttribute::OFF);

	m_colorArrayControllerPointer = new osg::Vec4Array;
	m_colorArrayControllerPointer->push_back(menuPointerColorIn);
	m_colorArrayControllerPointer->setBinding(osg::Array::BIND_OVERALL);
	m_drawableControllerPointer->setColorArray(m_colorArrayControllerPointer);

	m_menuTransform->addChild(m_geodeControllerPointer);
}

FenceMenu::~FenceMenu()
{

}

void FenceMenu::attachToSceneGraph(osg::ref_ptr<osg::Group> parentNodeIn)
{
	parentNodeIn->addChild(m_menuTransform);
	//OSGShadow Test
	//m_menuTransform->setNodeMask(0x2); //this are globals from main //TODO: Make global: Config Manager?
	m_menuTransform->addChild(m_menuGeode);
}

void FenceMenu::detachFromSceneGraph()
{
	if (m_menuTransform != nullptr)
	{
		std::vector<osg::Group*> parents = m_menuTransform->getParents();

		std::vector<osg::Group*>::iterator paIt = parents.begin();

		while (paIt != parents.end())
		{
			(*paIt)->removeChild(m_menuTransform);
			paIt++;
		}

		m_menuTransform->removeChild(m_menuGeode);
	}
}


void FenceMenu::updateAnimations(double deltaTimeIn)
{
	if (m_isVisible)
	{
		if (m_animationState < 1.0)
		{
			m_animationState += deltaTimeIn / 50.0 * pow(1.0 - m_animationState, 1.0) + 0.000001;
			m_animationState = m_animationState < 1.0 ? m_animationState : 1.0;

			float currentScaleValue = m_startScaleValue + (m_targetScaleValue - m_startScaleValue) * m_animationState;
			m_menuTransform->setScale(osg::Vec3f(currentScaleValue, currentScaleValue, currentScaleValue));
		}
	}
	else if (!m_isVisible)
	{
		if (m_animationState > 0.0)
		{
			m_animationState -= deltaTimeIn / 40.0 * pow(m_animationState, 1.5) + 0.000001;
			m_animationState = m_animationState > 0.0 ? m_animationState : 0.0;

			float currentScaleValue = m_startScaleValue + (m_targetScaleValue - m_startScaleValue) * m_animationState;
			m_menuTransform->setScale(osg::Vec3f(currentScaleValue, currentScaleValue, currentScaleValue));
		}
	}

	std::map<BUTTON_ID, MenuButton*>::iterator buIt = m_menuButtons.begin();

	while (buIt != m_menuButtons.end())
	{
		(*buIt).second->updateButtonAnimation(m_animationState, deltaTimeIn, m_startScaleValue, m_targetScaleValue);
		buIt++;
	}

	if (m_framesBeforeClose > 0)
		idleCounterTick();
}

void FenceMenu::idleCounterTick()
{
	if (m_idleFrameCounter < 1)
	{
		if (m_isVisible)
			hide();
	}
	else
		--m_idleFrameCounter;
}

void FenceMenu::show()
{
	m_isVisible = true;
	refreshIdleCounter();
}

void FenceMenu::hide()
{
	m_idleFrameCounter = 0;
	m_isVisible = false;
	hideAllSubMenus();
	unhighlightAllButtons();
}

void FenceMenu::hideAllSubMenus()
{
	std::map<BUTTON_ID, MenuButton*>::iterator buIt = m_menuButtons.begin();

	while (buIt != m_menuButtons.end())
	{
		(*buIt).second->hideSubmenu();
		buIt++;
	}

	m_submenuActiveAtButton = INVALID_BUTTON_ID;
}

void FenceMenu::unhighlightAllButtons()
{
	std::map<BUTTON_ID, MenuButton*>::iterator buIt = m_menuButtons.begin();

	while (buIt != m_menuButtons.end())
	{
		(*buIt).second->unhighlight();
		buIt++;
	}
}

BUTTON_ID FenceMenu::createButton(std::string const & buttonText)
{
	m_menuButtons[m_idCounter] = new FenceMenuButton(m_idCounter, m_textSize, 0, buttonText, m_defaultColor, m_highlightColor,
		m_buttonHeight, m_buttonWidth, m_buttonSpacerHeight, m_buttonTextYOffset);
	
	m_menuButtons[m_idCounter]->attachToSceneGraph(m_menuTransform);
	return m_idCounter++;
}

void FenceMenu::createSubButton(BUTTON_ID parentId, std::string const & buttonText)
{
	//TODO: Create Subbutton functionality or delete it
	std::cout << "SubButtons are currently not supported by FenceMenu. Function is still available because of inheritance have-to-implements" << std::endl;

	std::map<BUTTON_ID, MenuButton *>::const_iterator parentIt = m_menuButtons.find(parentId);

	if (parentIt == m_menuButtons.cend())
	{
		if (ConfigReader::DEBUG_LEVEL >= NOT_IN_LOOPS)
			std::cout << "Error in Menu::createSubButton - Parent id of " << parentId << " not found!" << std::endl;
	}
	else
		parentIt->second->createSubButton(buttonText);
}

//TODO: Make this nicer / more efficient
MenuButton* const FenceMenu::getMenuButtonAtPosition(float xPositionIn, float yPositionIn)
{
	int buttonsCount = m_menuButtons.size();

	//Bug fixed: The "(m_buttonSpacerHeight / 10.0f)" is just an necessary offset for the right selection for large menus
	//int buttonID = floorf((-yPositionIn + (m_buttonSpacerHeight / 2.0f)) / (m_buttonHeight + m_buttonSpacerHeight - (m_buttonSpacerHeight / 10.0f)));
	int buttonID = floorf((-yPositionIn + (m_buttonSpacerHeight / 2.0f)) / (m_buttonHeight + m_buttonSpacerHeight));
	//std::cout << "buttonID ohne floor:  " << yPositionIn / (m_buttonHeight + m_buttonSpacerHeight) << std::endl;
	//std::cout << "buttonID:  " << buttonID << std::endl;

	//If the buttonID is in a valid range and
	if (buttonID < m_menuButtons.size() && buttonID >= 0 &&
		//If the xPosition of the pointer on the menu
		xPositionIn > -(m_buttonWidth / 2.0) && xPositionIn < (m_buttonWidth / 2.0))
	{
		return m_menuButtons.at(buttonID);
	}
	else
	{
		return nullptr;
	}

}

void FenceMenu::initializeDrawables(float height)
{
	std::map<BUTTON_ID, MenuButton*>::iterator buIt = m_menuButtons.begin();

	while (buIt != m_menuButtons.end())
	{
		static_cast<FenceMenuButton*>((*buIt).second)->initDrawable(m_menuButtons.size(), height);
		buIt++;
	}
}

void FenceMenu::renderControllerPointer(osg::Vec3f menuCoordSysOffsetFromController)
{
	m_controllerPointerVertices->clear();
	m_controllerPointerVertices->push_back(osg::Vec3f(menuCoordSysOffsetFromController.x(), menuCoordSysOffsetFromController.y(), 0.0));
	m_controllerPointerVertices->push_back(osg::Vec3f(menuCoordSysOffsetFromController.x(), menuCoordSysOffsetFromController.y(), menuCoordSysOffsetFromController.z()));
	
	//OBSCURE: VertexArray must  call dirty() and must be set again, otherwise no render update:
	m_controllerPointerVertices->dirty();
	m_drawableControllerPointer->setVertexArray(m_controllerPointerVertices);

	//TODO: Some useful visual userfeedback -> lines on the Y-Z Plane -> could be implemented later and is a nice to have
	//if (menuCoordSysOffsetFromController.x() > (m_buttonWidth / 2) || 
	//	menuCoordSysOffsetFromController.x() < -(m_buttonWidth / 2) ||
	//	menuCoordSysOffsetFromController.y() < -(m_buttonHeight + m_buttonSpacerHeight) * m_menuButtons.size() + 2 * m_buttonSpacerHeight ||
	//	menuCoordSysOffsetFromController.y() > 0.0)
	//{
	//	std::cout << "Ausserhalb" << std::endl;
	//	controllerPointerVertices->push_back(osg::Vec3f(0.0, 0.0, 0.0));
	//	controllerPointerVertices->push_back(osg::Vec3f(menuCoordSysOffsetFromController.x(), menuCoordSysOffsetFromController.y(), menuCoordSysOffsetFromController.z()));

	//	contrPointerLine->push_back(3);
	//	contrPointerLine->push_back(2);
	//	m_drawableControllerPointer->addPrimitiveSet(contrPointerLine);
	//}
}