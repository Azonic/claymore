// ClayMore - Immersive Mesh Modelling --- Copyright (c) 2014-2017 Philipp Ladwig, Jannik Fiedler, Jan Beutgen
#pragma once

#include "ConfigDllExportMenuManagement.h"
#include "Menu.h"
#include "PieMenuButton.h"

#include <osg/PositionAttitudeTransform>
#include <math.h>
#include <list>

class ConfigReader;

class imMenuManagement_DLL_import_export PieMenu : public Menu
{
	// #### CONSTRUCTOR & DESTRUCTOR ###############

public:
	PieMenu(ConfigReader* configReaderIn,
		MenuName menuTypeIn,
		float yPosIn,
		osg::Vec4f defaultColorIn,
		osg::Vec4f highlightColorIn,
		osg::Vec4f menuPointerColorIn,
		CONTROLLER_TYPE controllerType,
		float textSizeIn = 0.1);
	~PieMenu();

	// #### MEMBER FUNCTIONS ################
	void updateAnimations(double deltaTimeIn);

	void hideAllSubMenus();

	void attachToSceneGraph(osg::ref_ptr<osg::Group> parentNodeIn);
	void detachFromSceneGraph();

	void show();
	void hide();
	void refreshIdleCounter() { m_idleFrameCounter = m_framesBeforeClose; }

	MenuButton* const getMenuButtonAtAngle(float angleIn);

	BUTTON_ID createButton(std::string const & buttonText);
	void createSubButton(BUTTON_ID parentId, std::string const & buttonText);

	void unhighlightAllButtons();

	//Must be called after all buttons where added in ordner to align buttons accordingly
	void initializeDrawables();

	void setFramesBeforeClose(int framesBeforeCloseIn) { m_framesBeforeClose = framesBeforeCloseIn; }

	void activateBackground();

	//Set scale value that an active menu will have
	inline void setTargetScaleValue(float targetScaleValue) { m_targetScaleValue = targetScaleValue; }
	//Set scale value that an inactive menu will have
	inline void setStartScaleValue(float startScaleValue) { m_startScaleValue = startScaleValue; }

	void renderControllerPointer(osg::Vec2f menuCoordSysOffsetFromController);
private:
	ConfigReader* m_configReader;

	void idleCounterTick();

	//Frames to wait before the menu will be automatically hidden
	//If set to 0, the menu will stay active (default behaviour)
	int m_framesBeforeClose, m_idleFrameCounter;

	double m_animationState;
	float m_targetScaleValue, m_startScaleValue, m_menuFloatingHeight; //m_menuFloatingHeight: like along the "normal" of vive touch pad ->it's the Y axis)

	osg::ref_ptr<osg::Geometry> m_backgroundGeometry;

	osg::ref_ptr<osg::Geometry> m_drawableControllerPointer;
	osg::ref_ptr<osg::Geode> m_geodeControllerPointer;
	osg::ref_ptr<osg::Vec3Array> m_controllerPointerVertices;
	osg::ref_ptr<osg::DrawElementsUInt> m_contrPointerLine;
	osg::ref_ptr<osg::Vec4Array> m_colorArrayControllerPointer;
};