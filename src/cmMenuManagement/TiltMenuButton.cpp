// ClayMore - Immersive Mesh Modelling --- Copyright (c) 2014-2017 Philipp Ladwig, Jannik Fiedler, Jan Beutgen
#include "TiltMenuButton.h"

TiltMenuButton::TiltMenuButton(BUTTON_ID id, float textSizeIn, unsigned int level, std::string const & buttonText, osg::Vec4f defaultColorIn, osg::Vec4f highlightColorIn) :
	MenuButton(id, textSizeIn, level, buttonText, defaultColorIn, highlightColorIn)
{
}


TiltMenuButton::~TiltMenuButton()
{
}

void TiltMenuButton::attachToSceneGraph(osg::ref_ptr<osg::Group> parentNodeIn)
{
	parentNodeIn->addChild(m_buttonTransform);
	m_buttonTransform->addChild(m_buttonGeode);
	m_parentNode = parentNodeIn;
}

void TiltMenuButton::detachFromSceneGraph()
{
	if (m_buttonTransform != nullptr)
	{
		std::vector<osg::Group*> parents = m_buttonTransform->getParents();

		std::vector<osg::Group*>::iterator paIt = parents.begin();

		while (paIt != parents.end())
		{
			(*paIt)->removeChild(m_buttonTransform);
			paIt++;
		}

		m_buttonTransform->removeChild(m_buttonGeode);
	}
}

void TiltMenuButton::showSubmenu()
{
	if (!m_submenuIsActive)
	{
		m_submenuIsActive = true;

		std::map<BUTTON_ID, MenuButton*>::iterator suBuIt = m_subButtons.begin();

		while (suBuIt != m_subButtons.end())
		{
			(*suBuIt).second->attachToSceneGraph(m_parentNode);
			suBuIt++;
		}
	}
}

void TiltMenuButton::hideSubmenu()
{
	if (m_submenuIsActive)
	{
		m_submenuIsActive = false;

		std::map<BUTTON_ID, MenuButton*>::iterator suBuIt = m_subButtons.begin();

		while (suBuIt != m_subButtons.end())
		{
			(*suBuIt).second->unhighlight();
			(*suBuIt).second->detachFromSceneGraph();
			suBuIt++;
		}
	}
}

void TiltMenuButton::createSubButton(std::string const & buttonText)
{
	m_subButtons.insert(std::pair<BUTTON_ID, MenuButton*>(m_subButtonIDCounter, new TiltMenuButton(m_subButtonIDCounter,
		m_textSize,
		m_subLevel + 1,
		buttonText,
		MenuButton::m_defaultColor,
		MenuButton::m_highlightColor)));
	m_subButtonIDCounter++;
}

//void TiltMenuButton::updateButtonAnimation(double menuAnimationStateIn, double deltaTimeIn, float startScaleValueIn, float targetScaleValueIn)
//{
//	MenuButton::m_buttonText->setColor(m_textColor);
//	switch (m_highlightState)
//	{
//	case ACTIVE:
//
//		if (m_buttonAnimationState < 1.5)
//		{
//			m_buttonAnimationState += deltaTimeIn / 70.0 * pow(1.5 - m_buttonAnimationState, 2) + 0.0001;
//			m_buttonAnimationState = m_buttonAnimationState < 1.5 ? m_buttonAnimationState : 1.5;
//
//			m_colorArray->at(0).set(m_defaultColor.x() + m_highlightColor.x() * m_buttonAnimationState, m_defaultColor.y(), m_defaultColor.z() - m_highlightColor.z() * m_buttonAnimationState, m_defaultColor.w());
//			m_geometry->setColorArray(m_colorArray);
//		}
//
//		break;
//
//	case HOVER:
//
//		if (m_buttonAnimationState < 1.0)
//		{
//			m_buttonAnimationState += deltaTimeIn / 70.0 * pow(1.0 - m_buttonAnimationState, 2) + 0.0001;
//			m_buttonAnimationState = m_buttonAnimationState < 1.0 ? m_buttonAnimationState : 1.0;
//
//			m_colorArray->at(0).set(m_defaultColor.x() + m_highlightColor.x() * m_buttonAnimationState, m_defaultColor.y(), m_defaultColor.z() - m_highlightColor.z() * m_buttonAnimationState, m_defaultColor.w());
//			m_geometry->setColorArray(m_colorArray);
//		}
//		else if (m_buttonAnimationState > 1.0)
//		{
//			m_buttonAnimationState -= deltaTimeIn / 70.0 * pow(1.5 - m_buttonAnimationState, 2) + 0.0001;
//			m_buttonAnimationState = m_buttonAnimationState > 1.0 ? m_buttonAnimationState : 1.0;
//
//			m_colorArray->at(0).set(m_defaultColor.x() + m_highlightColor.x() * m_buttonAnimationState, m_defaultColor.y(), m_defaultColor.z() - m_highlightColor.z() * m_buttonAnimationState, m_defaultColor.w());
//			m_geometry->setColorArray(m_colorArray);
//		}
//
//		break;
//
//	case NONE:
//	default:
//
//		if (m_buttonAnimationState > 0.0)
//		{
//			m_buttonAnimationState -= deltaTimeIn / 50.0 * pow(m_buttonAnimationState, 2) + 0.0001;
//			m_buttonAnimationState = m_buttonAnimationState > 0.0 ? m_buttonAnimationState : 0.0;
//
//			m_colorArray->at(0).set(m_defaultColor.x() + m_highlightColor.x() * m_buttonAnimationState, m_defaultColor.y(), m_defaultColor.z() - m_highlightColor.z() * m_buttonAnimationState, m_defaultColor.w());
//			m_geometry->setColorArray(m_colorArray);
//		}
//
//		break;
//	}
//
//	//TODO: Redo this... Scale is used because the pivot point is set to -z anyway - scale will cause translation
//	float currentScale = targetScaleValueIn + (m_scaleDelayValue * (1.0f - menuAnimationStateIn)) + menuAnimationStateIn * 0.85f;
//	currentScale = currentScale < 1.0f ? currentScale > targetScaleValueIn ? currentScale : targetScaleValueIn : 1.0f;
//	m_buttonTransform->setScale(osg::Vec3f(currentScale, currentScale, currentScale));
//
//	if (m_submenuIsActive)
//	{
//		std::map<BUTTON_ID, MenuButton*>::iterator suBuIt = m_subButtons.begin();
//
//		while (suBuIt != m_subButtons.end())
//		{
//			(*suBuIt).second->updateButtonAnimation(menuAnimationStateIn, deltaTimeIn, startScaleValueIn, targetScaleValueIn);
//			suBuIt++;
//		}
//
//		highlightActive();
//	}
//}

void TiltMenuButton::highlightHover()
{
	m_highlightState = HOVER;
}

void TiltMenuButton::highlightActive()
{
	m_highlightState = ACTIVE;
}

void TiltMenuButton::unhighlight()
{
	if (m_submenuIsActive)
	{
		std::map<BUTTON_ID, MenuButton*>::iterator suBuIt = m_subButtons.begin();

		while (suBuIt != m_subButtons.end())
		{
			(*suBuIt).second->unhighlight();
			suBuIt++;
		}
	}
	else
		m_highlightState = NONE;
}

void TiltMenuButton::setButtonUseTexture(std::string textureFilePath)
{
	//Create texture
	m_texture = new osg::Texture2D;
	m_texture->setDataVariance(osg::Object::DYNAMIC);

	//Read image
	osg::Image* image = osgDB::readImageFile(textureFilePath);
	m_texture->setImage(image);
}

void TiltMenuButton::initDrawable(int buttonsCount, float height)
{
	if (m_subButtons.size() > 0)
	{
		std::map<BUTTON_ID, MenuButton*>::iterator buIt = m_subButtons.begin();

		while (buIt != m_subButtons.end())
		{
			static_cast<TiltMenuButton*>((*buIt).second)->initDrawable(m_subButtons.size());
			buIt++;
		}
	}

	m_geometry = new osg::Geometry();
	m_geometryForTexture = new osg::Geometry();

	m_geometry->setUseDisplayList(false);
	m_geometryForTexture->setUseDisplayList(false);
	m_geometry->setUseVertexBufferObjects(true);
	m_geometryForTexture->setUseVertexBufferObjects(true);
	m_geometry->setDataVariance(osg::Object::DYNAMIC);
	m_geometryForTexture->setDataVariance(osg::Object::DYNAMIC);

	//Create StateSet
	osg::ref_ptr<osg::StateSet> stateSet = new osg::StateSet();
	stateSet->setMode(GL_LIGHTING, osg::StateAttribute::OFF);
	m_geometry->setStateSet(stateSet);

	osg::ref_ptr<osg::Vec3Array> vertices = new osg::Vec3Array();

	float buttonWidth = M_PI * 2.0f / (float)buttonsCount;


	m_scaleDelayValue = -(float)(m_buttonID) / ((float)buttonsCount * 0.5f);
	int pointsPerCircle = ConfigReader::PIE_MENU_RESOLUTION;
	//int buttonsCount = (ConfigReader::PIE_MENU_BUTTONS_ROOT_LEVEL) * pow(2, m_subLevel);
	//int buttonsCount = (ConfigReader::PIE_MENU_BUTTONS_ROOT_LEVEL);
	unsigned int buttonWidthInPoints = pointsPerCircle / buttonsCount;


	osg::ref_ptr<osg::DrawElementsUInt> drawElements = new osg::DrawElementsUInt(osg::PrimitiveSet::QUAD_STRIP, 0);

	float angle, angleCos, angleSin;
	float angleCorrection = -M_PI_2 - buttonWidth / 2.0f;

	for (unsigned int i = 1; i < buttonWidthInPoints; i += 2)
	{
		angle = (float)i / (float)pointsPerCircle * (M_PI * 2.0f) + angleCorrection;
		angleCos = cosf(angle);
		angleSin = sinf(angle);
		vertices->push_back(osg::Vec3f(angleCos * (0.98f + 0.75f * m_subLevel), height, angleSin * (0.98f + 0.75f * m_subLevel) - 0.5f));
		vertices->push_back(osg::Vec3f(angleCos * (0.25f + 0.75f * m_subLevel), height, angleSin * (0.25f + 0.75f * m_subLevel) - 0.5f));
		drawElements->push_back(i);
		drawElements->push_back(i - 1);
	}
	m_geometry->setVertexArray(vertices);
	m_geometry->addPrimitiveSet(drawElements);

	//Define colorArray (with BIND_OVERALL)
	m_colorArray = new osg::Vec4Array();
	m_colorArray->push_back(osg::Vec4f(m_defaultColor.x(), m_defaultColor.y(), m_defaultColor.z(), m_defaultColor.w()));
	m_geometry->setColorArray(m_colorArray);
	m_geometry->setColorBinding(osg::Geometry::BIND_OVERALL);

	m_buttonGeode->addDrawable(m_geometry);

	if (m_texture == nullptr)
	{
		//Buttontext
		MenuButton::m_buttonText->setRotation(osg::Quat(-M_PI_2, osg::Vec3f(1.0f, 0.0f, 0.0f), 0.0f, osg::Vec3f(0.0f, 1.0f, 0.0f), 0.0f, osg::Vec3f(0.0f, 0.0f, 1.0f)));
		MenuButton::m_buttonText->setPosition(osg::Vec3f(0.0f, height + 0.001f, -1.22f - 0.72f * m_subLevel));
		MenuButton::m_buttonText->setCharacterSize(m_textSize);
		MenuButton::m_buttonText->getOrCreateStateSet()->setRenderingHint(osg::StateSet::TRANSPARENT_BIN);
		MenuButton::m_buttonText->getOrCreateStateSet()->setRenderBinDetails(16, "DepthSortedBin");
		MenuButton::m_buttonText->setColor(osg::Vec4f(1.0f, 1.0f, 1.0f, 1.0f));
		m_buttonTransform->addChild(MenuButton::m_buttonText);
	}
	else
	{
		osg::ref_ptr<osg::Vec3Array> verticesTexture = new osg::Vec3Array();
		float textureOffset = 0.48;
		float floatingOffset = 0.02;

		//Old useful for TouchButton
		//vertices->push_back(osg::Vec3f(-1.0f, height, -1.0f));
		//vertices->push_back(osg::Vec3f(1.0f, height, -1.0f));
		//vertices->push_back(osg::Vec3f(1.0f, height, -3.0f));
		//vertices->push_back(osg::Vec3f(-1.0f, height, -3.0f));

		//Define vertices
		verticesTexture->push_back(osg::Vec3f(textureOffset * -0.30f, height + floatingOffset, textureOffset * -2.4f));
		verticesTexture->push_back(osg::Vec3f(textureOffset * 0.30f, height + floatingOffset, textureOffset * -2.4f));
		verticesTexture->push_back(osg::Vec3f(textureOffset * 0.30f, height + floatingOffset, textureOffset * -3.0f));
		verticesTexture->push_back(osg::Vec3f(textureOffset * -0.30f, height + floatingOffset, textureOffset * -3.0f));


		//Define rendering type (QUADS)
		osg::ref_ptr<osg::DrawElementsUInt> drawElementsTexture = new osg::DrawElementsUInt(osg::PrimitiveSet::QUADS, 0);
		drawElementsTexture->push_back(0);
		drawElementsTexture->push_back(1);
		drawElementsTexture->push_back(2);
		drawElementsTexture->push_back(3);
		m_geometryForTexture->addPrimitiveSet(drawElementsTexture);

		//Define textureCoordinates
		osg::Vec2Array* texcoords = new osg::Vec2Array(4);
		(*texcoords)[0].set(0.0f, 1.0f);
		(*texcoords)[1].set(0.0f, 0.0f);
		(*texcoords)[2].set(1.0f, 0.0f);
		(*texcoords)[3].set(1.0f, 1.0f);
		m_geometryForTexture->setTexCoordArray(0, texcoords);

		//Define colorArray (with BIND_OVERALL)
		//osg::ref_ptr<osg::Vec4Array> colorArrayTexture = new osg::Vec4Array();
		//colorArrayTexture->push_back(osg::Vec4f(1.0, 1.0, 1.0, 1.0));
		//m_geometryForTexture->setColorArray(colorArrayTexture);
		//m_geometryForTexture->setColorBinding(osg::Geometry::BIND_OVERALL);

		//Define texture mode, disable lighting, enable transparency
		//Create StateSet
		osg::ref_ptr<osg::StateSet> stateSetTexture = new osg::StateSet();
		stateSetTexture->setMode(GL_LIGHTING, osg::StateAttribute::OFF);
		stateSetTexture->setTextureAttributeAndModes(0, m_texture, osg::StateAttribute::ON);
		stateSetTexture->setMode(GL_BLEND, osg::StateAttribute::ON);
		stateSetTexture->setRenderingHint(osg::StateSet::TRANSPARENT_BIN);
		//stateSetTexture->setRenderBinDetails(5, "DepthSortedBin");
		m_geometryForTexture->setStateSet(stateSetTexture);

		m_geometryForTexture->setVertexArray(verticesTexture);
		m_buttonGeode->addDrawable(m_geometryForTexture);
	}

	m_buttonTransform->setPivotPoint(osg::Vec3f(0.0f, 0.0f, -0.5f));
	m_buttonTransform->setAttitude(osg::Quat(buttonWidth * m_buttonID, osg::Vec3f(0.0f, -1.0f, 0.0f)));

	//OSGShadow Test // //this are globals from main //TODO: Make global: Config Manager?
	//m_buttonGeode->setNodeMask(0x2);
	//m_buttonTransform->setNodeMask(0x2);
}


void TiltMenuButton::markAsSpacerButton(bool isSpacer)
{
	if (isSpacer)
	{
		detachFromSceneGraph();
		m_defaultColor = osg::Vec4f(0.1f, 0.1f, 0.1f, 0.95f);
		m_highlightColor = osg::Vec4f(0.0f, 0.0f, 0.0f, 0.0f);
	}
	else
	{
		m_defaultColor = osg::Vec4f(0.05f, 0.1f, 0.7f, 1.0f);
		m_highlightColor = osg::Vec4f(0.1f, 0.2f, 0.2f, 0.0f);
	}
}

void TiltMenuButton::markAsActive(bool isActive)
{
	if (isActive)
	{
		m_textColor = osg::Vec4f(1.0f, 1.0f, 1.0f, 1.0f);
	}
	else
	{
		m_textColor = osg::Vec4f(0.6f, 0.6f, 0.6f, 0.5f);
	}
}

void TiltMenuButton::highlightTextColor(bool isTextHighlighted)
{
	if (isTextHighlighted)
	{
		m_textColor = osg::Vec4f(0.8f, 0.5f, 0.0f, 1.0f);
		//m_textSize = 0.125;
	}
	else
	{
		m_textColor = osg::Vec4f(1.0f, 1.0f, 1.0f, 1.0f);
		//m_textSize = 0.1;
	}
}