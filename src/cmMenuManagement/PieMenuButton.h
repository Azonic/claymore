// ClayMore - Immersive Mesh Modelling --- Copyright (c) 2014-2017 Philipp Ladwig, Jannik Fiedler, Jan Beutgen
#pragma once

#include <osgDB/ReadFile>
#include <osg/Texture2D>
#include <osg/CullFace>
#include <osg/ShapeDrawable>
#include <osg/PositionAttitudeTransform>

#include <string>
#include <list>
#include <math.h>

#include "MenuButton.h"
#include "../cmUtil/Textcreator.h"


class imMenuManagement_DLL_import_export PieMenuButton : public MenuButton
{
	public:

		PieMenuButton(BUTTON_ID id, float textSizeIn, unsigned int level, std::string const & buttonText, osg::Vec4f defaultColorIn, osg::Vec4f highlightColorIn);
		~PieMenuButton();

		void createSubButton(std::string const & buttonText);

		//void updateButtonAnimation(double menuAnimationStateIn, double deltaTimeIn, float startScaleValueIn, float targetScaleValueIn);

		void highlightHover();
		void highlightActive();
		void unhighlight();

		void attachToSceneGraph(osg::ref_ptr<osg::Group> parentNodeIn);
		void detachFromSceneGraph();

		void showSubmenu();
		void hideSubmenu();

		void initDrawable(int buttonsCount, float height = 0.01f);

		//If a texture is set, no text or geometry will be display except for the image
		void setButtonUseTexture(std::string textureFilePath);

		void markAsActive(bool isActive);
		//inline void setTextSize(float characterSize) { m_characterSize = characterSize; }
		//inline float getTextSize() { return m_characterSize; }

	private:
		osg::ref_ptr<osg::Texture2D> m_texture;
		osg::ref_ptr<osg::Geometry> m_geometryForTexture;

};