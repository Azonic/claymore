// ClayMore - Immersive Mesh Modelling --- Copyright (c) 2014-2017 Philipp Ladwig, Jannik Fiedler, Jan Beutgen
#include "MenuButton.h"

#include <iostream>

#include "../cmUtil/Textcreator.h"

MenuButton::MenuButton(BUTTON_ID const buttonID,
	float textSizeIn,
	unsigned int level,
	std::string const & buttonText,
	osg::Vec4f defaultColorIn,
	osg::Vec4f highlightColorIn) :

	m_buttonID(buttonID),
	m_textSize(textSizeIn),
	m_subButtonIDCounter(0),
	m_subLevel(level),
	m_highlightState(NONE),
	m_buttonAnimationState(0.0),
	triggerCallback(nullptr),
	m_parentNode(nullptr),
	m_submenuIsActive(false),
	m_canBeTriggeredConstantly(false),
	m_scaleDelayValue(0.0f),
	m_active(true),

	m_buttonText(nullptr),
	m_colorArray(nullptr),
	m_geometry(nullptr),
	m_defaultColor(defaultColorIn),
	m_highlightColor(highlightColorIn)
{ 
	m_subButtons = std::map<BUTTON_ID, MenuButton *>();
	
	m_buttonText = TextCreator::createText(buttonText);

	m_textColor = osg::Vec4f(1.0, 1.0, 1.0, 1.0);

	m_buttonTransform = new osg::PositionAttitudeTransform();
	m_buttonGeode = new osg::Geode();
}
	
MenuButton::~MenuButton()
{
	for (std::map<BUTTON_ID, MenuButton*>::iterator it = m_subButtons.begin(); it != m_subButtons.end(); ++it)
		delete (*it).second;
}

MenuButton* MenuButton::getSubButton(BUTTON_ID id)
{
	if (m_subButtons.find(id) == m_subButtons.end() && ConfigReader::DEBUG_LEVEL >= NOT_IN_LOOPS)
	{
		std::cout << "Error in MenuButton::getSubButton - Id of " << id << " not found!" << std::endl;
		return nullptr;
	}
		
	return m_subButtons.at(id);
}

void MenuButton::updateButtonAnimation(double menuAnimationStateIn, double deltaTimeIn, float startScaleValueIn, float targetScaleValueIn)
{
	m_buttonText->setColor(m_textColor);
	m_buttonText->setCharacterSize(m_textSize);
	switch (m_highlightState)
	{
	case ACTIVE:

		if (m_buttonAnimationState < 1.5)
		{
			m_buttonAnimationState += deltaTimeIn / 70.0 * pow(1.5 - m_buttonAnimationState, 2) + 0.0001;
			m_buttonAnimationState = m_buttonAnimationState < 1.5 ? m_buttonAnimationState : 1.5;

			m_colorArray->at(0).set(m_defaultColor.x() + m_highlightColor.x() * m_buttonAnimationState,
				m_defaultColor.y(),
				m_defaultColor.z() - m_highlightColor.z() * m_buttonAnimationState, m_defaultColor.w());
			//std::cout << "ACTIVE" << std::endl;
			m_colorArray->dirty();
		}

		break;

	case HOVER:
		if (m_buttonAnimationState < 1.0)
		{
			m_buttonAnimationState += deltaTimeIn / 35.0 * pow(1.0 - m_buttonAnimationState, 2) + 0.0001;
			m_buttonAnimationState = m_buttonAnimationState < 1.0 ? m_buttonAnimationState : 1.0;

			m_colorArray->at(0).set(m_defaultColor.x() + m_highlightColor.x() * m_buttonAnimationState,
				m_defaultColor.y() + m_highlightColor.z() * m_buttonAnimationState,
				m_defaultColor.z() - m_highlightColor.z() * m_buttonAnimationState, m_defaultColor.w());
			//std::cout << "HOVER" << std::endl;
			m_colorArray->dirty();
		}
		else if (m_buttonAnimationState > 1.0)
		{
			m_buttonAnimationState -= deltaTimeIn / 35.0 * pow(1.5 - m_buttonAnimationState, 2) + 0.0001;
			m_buttonAnimationState = m_buttonAnimationState > 1.0 ? m_buttonAnimationState : 1.0;

			m_colorArray->at(0).set(m_defaultColor.x() + m_highlightColor.x() * m_buttonAnimationState, m_defaultColor.y(), m_defaultColor.z() - m_highlightColor.z() * m_buttonAnimationState, m_defaultColor.w());
			m_colorArray->dirty();
		}

		break;

	case NONE:
	default:

		if (m_buttonAnimationState > 0.0)
		{
			m_buttonAnimationState -= deltaTimeIn / 10.0 * pow(m_buttonAnimationState, 2) + 0.0001;
			m_buttonAnimationState = m_buttonAnimationState > 0.0 ? m_buttonAnimationState : 0.0;

			m_colorArray->at(0).set(m_defaultColor.x() + m_highlightColor.x() * m_buttonAnimationState, m_defaultColor.y(), m_defaultColor.z() - m_highlightColor.z() * m_buttonAnimationState, m_defaultColor.w());
			//std::cout << "NONE" << std::endl;
			m_colorArray->dirty();
		}

		break;
	}

	//TODO: Redo this... Scale is used because the pivot point is set to -z anyway - scale will cause translation
	float currentScale = targetScaleValueIn + (m_scaleDelayValue * (1.0f - menuAnimationStateIn)) + menuAnimationStateIn * 0.85f;
	currentScale = currentScale < 1.0f ? currentScale > targetScaleValueIn ? currentScale : targetScaleValueIn : 1.0f;
	m_buttonTransform->setScale(osg::Vec3f(currentScale, currentScale, currentScale));
}



BUTTON_ID MenuButton::trigger()
{
	if (triggerCallback != nullptr)
		triggerCallback();

	else if(m_subButtonIDCounter > 0)
	{
		showSubmenu();
		return m_buttonID;
	}

	return INVALID_BUTTON_ID;
}