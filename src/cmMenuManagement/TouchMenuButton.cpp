// ClayMore - Immersive Mesh Modelling --- Copyright (c) 2014-2017 Philipp Ladwig, Jannik Fiedler, Jan Beutgen
#include "TouchMenuButton.h"


TouchMenuButton::TouchMenuButton(BUTTON_ID id, float textSizeIn, std::string const & buttonText, osg::Vec4f defaultColorIn, osg::Vec4f highlightColorIn) :
	MenuButton(id, textSizeIn, 0, "0", defaultColorIn, highlightColorIn)
{
	//m_buttonText = TextCreator::createText(buttonText);
	//m_texture = new osg::Texture2D;

	m_defaultButtonImage = new osg::Image();
	m_hoverButtonImage = new osg::Image();
	m_activeButtonImage = new osg::Image();

	m_isHighlightActiveControlledFromExternal = nullptr;
}


TouchMenuButton::~TouchMenuButton()
{
}


void TouchMenuButton::attachToSceneGraph(osg::ref_ptr<osg::Group> parentNodeIn)
{
	parentNodeIn->addChild(m_buttonTransform);
	m_buttonTransform->addChild(m_buttonGeode);
	m_parentNode = parentNodeIn;
}

void TouchMenuButton::detachFromSceneGraph()
{
	if (m_buttonTransform != nullptr)
	{
		std::vector<osg::Group*> parents = m_buttonTransform->getParents();

		std::vector<osg::Group*>::iterator paIt = parents.begin();

		while (paIt != parents.end())
		{
			(*paIt)->removeChild(m_buttonTransform);
			paIt++;
		}

		m_buttonTransform->removeChild(m_buttonGeode);
	}
}

void TouchMenuButton::showSubmenu()
{
	if (!m_submenuIsActive)
	{
		m_submenuIsActive = true;

		std::map<BUTTON_ID, MenuButton*>::iterator suBuIt = m_subButtons.begin();

		while (suBuIt != m_subButtons.end())
		{
			(*suBuIt).second->attachToSceneGraph(m_parentNode);
			suBuIt++;
		}
	}
}

void TouchMenuButton::hideSubmenu()
{
	if (m_submenuIsActive)
	{
		m_submenuIsActive = false;

		std::map<BUTTON_ID, MenuButton*>::iterator suBuIt = m_subButtons.begin();

		while (suBuIt != m_subButtons.end())
		{
			(*suBuIt).second->unhighlight();
			(*suBuIt).second->detachFromSceneGraph();
			suBuIt++;
		}
	}
}

void TouchMenuButton::createSubButton(std::string const & buttonText)
{
	m_subButtons.insert(std::pair<BUTTON_ID, MenuButton*>(m_subButtonIDCounter, new TouchMenuButton(m_subButtonIDCounter, m_textSize, buttonText,
		m_defaultColor, m_highlightColor)));
	m_subButtonIDCounter++;
}

void TouchMenuButton::updateButtonAnimation(double menuAnimationStateIn, double deltaTimeIn, float startScaleValueIn, float targetScaleValueIn)
{
	//std::cout << "updateButtonAnimation updateButtonAnimation updateButtonAnimation" << std::endl;
	switch (m_highlightState)
	{
	case ACTIVE:
		updateTexture();
		break;

	case HOVER:
		updateTexture();
		break;

	case NONE:
	default:

		updateTexture();
		break;
	}

	//TODO: Redo this... Scale is used because the pivot point is set to -z anyway - scale will cause translation
	float currentScale = targetScaleValueIn + (m_scaleDelayValue * (1.0f - menuAnimationStateIn)) + menuAnimationStateIn * 0.85f;
	currentScale = currentScale < 1.0f ? currentScale > targetScaleValueIn ? currentScale : targetScaleValueIn : 1.0f;
	m_buttonTransform->setScale(osg::Vec3f(currentScale, currentScale, currentScale));
}

void TouchMenuButton::highlightHover()
{
	m_highlightState = HOVER;
}

void TouchMenuButton::highlightActive()
{
	m_highlightState = ACTIVE;
}

void TouchMenuButton::unhighlight()
{
	if (m_submenuIsActive)
	{
		std::map<BUTTON_ID, MenuButton*>::iterator suBuIt = m_subButtons.begin();

		while (suBuIt != m_subButtons.end())
		{
			(*suBuIt).second->unhighlight();
			suBuIt++;
		}
	}
	else
		m_highlightState = NONE;
}

void TouchMenuButton::setButtonUseTexture(std::string textureFilePath)
{
	//Create texture
	m_texture = new osg::Texture2D;
	m_texture->setDataVariance(osg::Object::DYNAMIC);

	//m_texture->setTextureSize(200, 200);
	//m_texture->setResizeNonPowerOfTwoHint(false);
	//m_texture->setInternalFormat(GL_RGBA);
	//m_texture->setFilter(osg::Texture2D::MIN_FILTER, osg::Texture2D::LINEAR);
	//m_texture->setFilter(osg::Texture2D::MAG_FILTER, osg::Texture2D::LINEAR);
	//m_texture->setWrap(osg::Texture::WRAP_S, osg::Texture::CLAMP_TO_EDGE);
	//m_texture->setWrap(osg::Texture::WRAP_T, osg::Texture::CLAMP_TO_EDGE);

	//Read image
	m_defaultButtonImage = osgDB::readImageFile(textureFilePath);
	m_texture->setImage(m_defaultButtonImage);
}

void TouchMenuButton::setButtonUseTextureHover(std::string textureFilePath)
{
	m_hoverButtonImage = osgDB::readImageFile(textureFilePath);
}

void TouchMenuButton::setButtonUseTextureActive(std::string textureFilePath)
{
	m_activeButtonImage = osgDB::readImageFile(textureFilePath);
}

void TouchMenuButton::initDrawable(int buttonsCount, float height)
{
	//if (m_subButtons.size() > 0)
	//{
	//	std::map<BUTTON_ID, MenuButton*>::iterator buIt = m_subButtons.begin();

	//	while (buIt != m_subButtons.end())
	//	{
	//		static_cast<TouchMenuButton*>((*buIt).second)->initDrawable(m_subButtons.size());
	//		buIt++;
	//	}
	//}

	m_geometry = new osg::Geometry();

	m_geometry->setUseDisplayList(false);
	m_geometry->setUseVertexBufferObjects(true);
	m_geometry->setDataVariance(osg::Object::DYNAMIC);

	//Create StateSet
	osg::ref_ptr<osg::StateSet> stateSet = m_geometry->getOrCreateStateSet();
	stateSet->setMode(GL_LIGHTING, osg::StateAttribute::OFF);

	osg::ref_ptr<osg::Vec3Array> vertices = new osg::Vec3Array();

	osg::ref_ptr<osg::DrawElementsUInt> drawElements = new osg::DrawElementsUInt(osg::PrimitiveSet::QUADS, 0);

	float offset = 0.035f;
	osg::Vec3f vertex0 = osg::Vec3f(0.0f + offset, 0.0f, 0.0f + offset);
	osg::Vec3f vertex1 = osg::Vec3f(1.0f + offset, 0.0f, 0.0f + offset);
	osg::Vec3f vertex2 = osg::Vec3f(1.0f + offset, 0.0f, 1.0f + offset);
	osg::Vec3f vertex3 = osg::Vec3f(0.0f + offset, 0.0f, 1.0f + offset);

	osg::Matrix rotOffset;
	rotOffset.makeRotate(M_PI_4 + M_PI + (float)m_buttonID * M_PI_2, 0.0f, 1.0f, 0.0f);

	//Define a quad
	vertices->push_back(rotOffset * vertex0);
	vertices->push_back(rotOffset * vertex1);
	vertices->push_back(rotOffset * vertex2);
	vertices->push_back(rotOffset * vertex3);

	drawElements->push_back(3);
	drawElements->push_back(2);
	drawElements->push_back(1);
	drawElements->push_back(0);

	m_geometry->setVertexArray(vertices);
	m_geometry->addPrimitiveSet(drawElements);

	m_colorArray = new osg::Vec4Array();
	m_colorArray->push_back(osg::Vec4f(m_defaultColor.x(), m_defaultColor.y(), m_defaultColor.z(), m_defaultColor.w()));
	m_geometry->setColorArray(m_colorArray);
	m_geometry->setColorBinding(osg::Geometry::BIND_OVERALL);

	m_buttonGeode->addDrawable(m_geometry);
	//if (m_texture == nullptr)
	//{
	//	//TODO5: Attach text
	//}
	//else
	//{
		//Define textureCoordinates
		osg::Vec2Array* texcoords = new osg::Vec2Array(4);
		(*texcoords)[0].set(0.0f, 0.0f);
		(*texcoords)[1].set(0.0f, 1.0f);
		(*texcoords)[2].set(1.0f, 1.0f);
		(*texcoords)[3].set(1.0f, 0.0f);
		m_geometry->setTexCoordArray(0, texcoords);

		stateSet->setTextureAttributeAndModes(0, m_texture, osg::StateAttribute::ON);
		stateSet->setMode(GL_BLEND, osg::StateAttribute::ON);
		stateSet->setRenderingHint(osg::StateSet::TRANSPARENT_BIN);
		//stateSet->setRenderBinDetails(16, "DepthSortedBin");
	//}
	//OSGShadow Test // //this are globals from main //TODO: Make global: Config Manager?
	//m_buttonGeode->setNodeMask(0x2);
	//m_buttonTransform->setNodeMask(0x2);
}

//PERFORMANCE TODO: Every frame the texture is set again and again... maybe only react if a change is triggered?
void TouchMenuButton::updateTexture()
{
	//std::cout << "frucht frucht frucht" << std::endl;
	//If the button state ISN'T controlled by external data, manage the state with the ObserverInformation of Push, Hold, Release....
	if (m_isHighlightActiveControlledFromExternal == nullptr)
	{
		switch (m_highlightState)
		{
		case ACTIVE:
			if (m_activeButtonImage != nullptr)
			{
				if (m_activeButtonImage->valid())
				{
					m_texture->setImage(m_activeButtonImage);
					//std::cout << "ACTIVE" << std::endl;
				}
			}
			break;

		case HOVER:
			if (m_hoverButtonImage != nullptr)
			{
				if (m_hoverButtonImage->valid()) {
					m_texture->setImage(m_hoverButtonImage);
					//std::cout << "HOVER" << std::endl;
				}
			}
			break;

		case NONE:
		default:
			if (m_defaultButtonImage != nullptr)
			{
				if (m_defaultButtonImage->valid()) {
					m_texture->setImage(m_defaultButtonImage);
					//std::cout << "NONE" << std::endl;
				}
			}
			break;
		}
	}
	//...if the button state IS controlled by external data, manage the state with the external bool. 
	//External state has priority and the active state is only controlled by the external data and do not relay on Observer information
	else
	{
		//std::cout << "else" << std::endl;
		//if (getButtonID() == 1)
		//{
		//	std::cout << "Also, m_highlightState ist: " << m_highlightState << "und m_isHighlightActiveControlledFromExternal ist: " << *m_isHighlightActiveControlledFromExternal << std::endl;
		//}
		//std::cout << "m_isHighlightActiveControlledFromExternal: " << *m_isHighlightActiveControlledFromExternal << std::endl;
		if (*m_isHighlightActiveControlledFromExternal)
		{
			if (m_activeButtonImage != nullptr)
			{
				if (m_activeButtonImage->valid())
					m_texture->setImage(m_activeButtonImage);
			}
		}
		else
		{
			//if (getButtonID() == 1)
			//{
			//	std::cout << "Sollte in else gehen...." << std::endl;
			//}
			if (m_highlightState == HOVER)
			{
				if (m_hoverButtonImage != nullptr)
				{
					if (m_hoverButtonImage->valid())
						m_texture->setImage(m_hoverButtonImage);
				}
			}
			if (m_highlightState == NONE)
			{
				if (m_defaultButtonImage != nullptr)
				{
					if (m_defaultButtonImage->valid())
						m_texture->setImage(m_defaultButtonImage);
				}
			}
		}
	}
}


