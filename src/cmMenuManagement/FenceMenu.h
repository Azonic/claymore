// ClayMore - Immersive Mesh Modelling --- Copyright (c) 2014-2017 Philipp Ladwig, Jannik Fiedler, Jan Beutgen
#pragma once

#include "ConfigDllExportMenuManagement.h"
#include "Menu.h"
#include "FenceMenuButton.h"

class ConfigReader;

class imMenuManagement_DLL_import_export FenceMenu : public Menu
{
	// #### CONSTRUCTOR & DESTRUCTOR ###############

public:

	FenceMenu(ConfigReader* configReaderIn,
		MenuName menuNameIn,
		osg::Vec4f defaultFenceColorIn,
		osg::Vec4f highlightFenceColorIn,
		osg::Vec4f menuPointerColorIn, 
		CONTROLLER_TYPE controllerType,
		float textSizeIn = 0.13,
		float buttonHeight = 0.15, float buttonWidth = 1.5, float buttonSpacerHeight = 0.045, float buttonTextYOffset = 0.075);
	~FenceMenu();

	void attachToSceneGraph(osg::ref_ptr<osg::Group> parentNodeIn);
	void detachFromSceneGraph();

	// #### MEMBER FUNCTIONS ################

	void updateAnimations(double deltaTimeIn);

	void hideAllSubMenus();

	void show();
	void hide();
	void refreshIdleCounter() { m_idleFrameCounter = m_framesBeforeClose; }

	MenuButton* const getMenuButtonAtPosition(float xPositionIn, float yPositionIn);

	BUTTON_ID createButton(std::string const & buttonText);
	void createSubButton(BUTTON_ID parentId, std::string const & buttonText);

	void unhighlightAllButtons();

	//Must be called after all buttons where added in ordner to align buttons accordingly
	void initializeDrawables(float height = 0.01f);

	void setFramesBeforeClose(int framesBeforeCloseIn) { m_framesBeforeClose = framesBeforeCloseIn; }

	//void activateBackground();

	//Set scale value that an active menu will have
	inline void setTargetScaleValue(float targetScaleValue) { m_targetScaleValue = targetScaleValue; }
	//Set scale value that an inactive menu will have
	inline void setStartScaleValue(float startScaleValue) { m_startScaleValue = startScaleValue; }

	void renderControllerPointer(osg::Vec3f menuCoordSysOffsetFromController);

	inline float getButtonHeight() { return m_buttonHeight; }
	inline float getButtonSpacerHeight() { return m_buttonSpacerHeight; }
	inline float getButtonWidth() { return m_buttonWidth; }
	inline float getTargetScaleValue() { return m_targetScaleValue; }

	inline double getAnimationState() { return m_animationState; }

	void(*closeCallback) ();

private:
	ConfigReader* m_configReader;

	void idleCounterTick();

	//Frames to wait before the menu will be automatically hidden
	//If set to 0, the menu will stay active (default behaviour)
	int m_framesBeforeClose, m_idleFrameCounter;

	double m_animationState;
	float m_targetScaleValue, m_startScaleValue;

	osg::ref_ptr<osg::Geometry> m_backgroundGeometry;

	float m_buttonHeight, m_buttonWidth, m_buttonSpacerHeight, m_buttonTextYOffset;

	osg::ref_ptr<osg::Geometry> m_drawableControllerPointer;
	osg::ref_ptr<osg::Geode> m_geodeControllerPointer;
	osg::ref_ptr<osg::Vec3Array> m_controllerPointerVertices;
	osg::ref_ptr<osg::DrawElementsUInt> m_contrPointerLine;
	osg::ref_ptr<osg::Vec4Array> m_colorArrayControllerPointer;
};