// ClayMore - Immersive Mesh Modelling --- Copyright (c) 2014-2017 Philipp Ladwig, Jannik Fiedler, Jan Beutgen
#pragma once

#include <osgDB/ReadFile>

#include "MenuButton.h"

#include "../cmUtil/Textcreator.h"


class imMenuManagement_DLL_import_export TouchMenuButton : public MenuButton
{
public:
	TouchMenuButton::TouchMenuButton(BUTTON_ID id, float textSizeIn, std::string const & buttonText, osg::Vec4f defaultColorIn, osg::Vec4f highlightColorIn);
	~TouchMenuButton();

	void attachToSceneGraph(osg::ref_ptr<osg::Group> parentNodeIn);
	void detachFromSceneGraph();

	void createSubButton(std::string const & buttonText);

	void updateButtonAnimation(double menuAnimationStateIn, double deltaTimeIn, float startScaleValueIn, float targetScaleValueIn);

	void highlightHover();
	void highlightActive();
	void unhighlight();

	void showSubmenu();
	void hideSubmenu();

	void initDrawable(int buttonsCount, float height = 0.01f);

	//If a texture is set, no text or geometry will be display except for the image
	void setButtonUseTexture(std::string textureFilePath);
	void setButtonUseTextureHover(std::string textureFilePath);
	void setButtonUseTextureActive(std::string textureFilePath);

	void updateTexture();

	//Allows setting the button higlight state from outside with an pointer to a bool
	//If this is nullptr pessing on the button will highlight it as active, otherwise highlighting will be done by an external state,
	//such as "the current constraint (X,Y or Z)" in geometryManager or current selected tool -> This function gives the user a feedback, what is activated
	inline void setHighlightActiveFromExternalBool(bool* highlightActiveControlledFromExternalIn)
		{ m_isHighlightActiveControlledFromExternal = highlightActiveControlledFromExternalIn;  }
	//inline bool getHighlightActiveFromExternal() { return m_isHighlightActiveControlledFromExternal; }
	
	
	//inline void setTextSize(float characterSize) { m_characterSize = characterSize; }
	//inline float getTextSize() { return m_characterSize; }

private:
	osg::ref_ptr<osg::Image> m_defaultButtonImage;
	osg::ref_ptr<osg::Image> m_hoverButtonImage;
	osg::ref_ptr<osg::Image> m_activeButtonImage;

	osg::ref_ptr<osg::Texture2D> m_texture;

	bool* m_isHighlightActiveControlledFromExternal;
};