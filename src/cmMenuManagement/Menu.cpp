// ClayMore - Immersive Mesh Modelling --- Copyright (c) 2014-2017 Philipp Ladwig, Jannik Fiedler, Jan Beutgen
#include "Menu.h"

#include <iostream>

Menu::Menu(MenuName menuTypeIn, CONTROLLER_TYPE controllerType, float textSizeIn) :
	m_menuName(menuTypeIn),
	m_textSize(textSizeIn),
	m_controllerType(controllerType),
	m_submenuActiveAtButton(INVALID_BUTTON_ID)
{
	m_menuButtons = std::map<BUTTON_ID, MenuButton *>();
	m_idCounter = 0;
	m_buttonCountOnRootLevel = 0;

	m_isVisible = false;
	m_menuTransform = new osg::PositionAttitudeTransform();
	m_menuGeode = new osg::Geode();
}


Menu::~Menu()
{
	for (std::map<BUTTON_ID, MenuButton *>::iterator it = m_menuButtons.begin(); it != m_menuButtons.end(); ++it)
		delete (*it).second;
}

MenuButton * Menu::getMenuButton(BUTTON_ID id)
{
	if (m_menuButtons.find(id) == m_menuButtons.end())
	{
		if(ConfigReader::DEBUG_LEVEL >= NOT_IN_LOOPS)
			std::cout << "Error in Menu::getMenuButton - Id of " << id << " not found!" << std::endl;
		return nullptr;
	}

	return m_menuButtons[id];
}