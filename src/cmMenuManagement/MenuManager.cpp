// ClayMore - Immersive Mesh Modelling --- Copyright (c) 2014-2017 Philipp Ladwig, Jannik Fiedler, Jan Beutgen
#include "MenuManager.h"

#include "cmInteractionManagement/Controller.h"

MenuManager::MenuManager()
{
	m_menuMap.clear();
}

MenuManager::~MenuManager()
{
}

void MenuManager::addMenuForUniqueName(Menu* menuIn)
{
	if (getMenuWithUniqueName(menuIn->getMenuName()) == nullptr)
	{
		m_menuMap.insert(std::make_pair(menuIn->getMenuName(), menuIn));
	}
}

void MenuManager::updateMenuAnimations(double deltaTimeIn)
{
	std::map<MenuName, Menu*>::iterator menuIt = m_menuMap.begin();

	while (menuIt != m_menuMap.end())
	{
		(*menuIt).second->updateAnimations(deltaTimeIn);

		menuIt++;
	}
}

void MenuManager::hideMenuWithUniqueName(MenuName menuNameIn)
{
	Menu* menu = getMenuWithUniqueName(menuNameIn);

	if (menu != nullptr)
	{
		menu->hideAllSubMenus();
		menu->hide();
	}
}

//TODO: Find an better way for iterating through all ints in the enum MenuName
//Maybe make a class for this with ++ operator for example
void MenuManager::hideAllMenus()
{
	//for (int fooInt = TILT_MODE_SELECT; fooInt != TILT_AND_SHIFT_MODE; fooInt++)
	for (int fooInt = TILT_MODE_SELECT; fooInt != ENUM_END; fooInt++)
	{
		Menu* menu = getMenuWithUniqueName(static_cast<MenuName>(fooInt));
		if (menu != nullptr)
		{
			menu->hideAllSubMenus();
			menu->hide();
		}
	}
}

void MenuManager::hideAllPrimaryMenus()
{
	//for (int fooInt = TILT_MODE_SELECT; fooInt != TILT_AND_SHIFT_MODE; fooInt++)
	for (int fooInt = TILT_MODE_SELECT; fooInt != ENUM_END; fooInt++)
	{
		Menu* menu = getMenuWithUniqueName(static_cast<MenuName>(fooInt));

		if (menu != nullptr && menu->getControllerType() == CONTROLLER_TYPE::PRIMARY)
		{
			menu->hideAllSubMenus();
			menu->hide();
		}
	}
}


void MenuManager::showMenuWithUniqueName(MenuName menuNameIn)
{
	Menu* menu = getMenuWithUniqueName(menuNameIn);

	if (menu != nullptr)
		menu->show();
}

Menu* MenuManager::getMenuWithUniqueName(MenuName menuNameIn)
{
	std::map<MenuName, Menu*>::iterator menuIt = m_menuMap.find(menuNameIn);

	if (menuIt != m_menuMap.end())
		return (*menuIt).second;

	return nullptr;
}

