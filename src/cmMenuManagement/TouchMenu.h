// ClayMore - Immersive Mesh Modelling --- Copyright (c) 2014-2017 Philipp Ladwig, Jannik Fiedler, Jan Beutgen
#pragma once

#include "ConfigDllExportMenuManagement.h"
#include "Menu.h"
#include "TouchMenuButton.h"

#include <osg\MatrixTransform>

class imMenuManagement_DLL_import_export TouchMenu : public Menu
{
	// #### CONSTRUCTOR & DESTRUCTOR ###############

public:

	TouchMenu(MenuName menuNameIn, float menuFloatingHeightIn, osg::Vec4f defaultColorIn, osg::Vec4f highlightColorIn, CONTROLLER_TYPE controllerType, float textSizeIn = 0.1);
	~TouchMenu();

	void attachToSceneGraph(osg::ref_ptr<osg::Group> parentNodeIn);
	void detachFromSceneGraph();

	// #### MEMBER FUNCTIONS ################

	void updateAnimations(double deltaTimeIn);

	void hideAllSubMenus();

	void show();
	void hide();
	void refreshIdleCounter() { m_idleFrameCounter = m_framesBeforeClose; }
	
	MenuButton* const getMenuButtonAtAngle(float angleIn);

	BUTTON_ID createButton(std::string const & buttonText);
	void createSubButton(BUTTON_ID parentId, std::string const & buttonText);

	void unhighlightAllButtons();

	//Must be called after all buttons where added in ordner to align buttons accordingly
	void initializeDrawables(float height = 0.01f);

	void initializeHelpText();

	void setFramesBeforeClose(int framesBeforeCloseIn) { m_framesBeforeClose = framesBeforeCloseIn; }

	//void activateBackground();

	//Set scale value that an active menu will have
	inline void setTargetScaleValue(float targetScaleValue) { m_targetScaleValue = targetScaleValue; }
	//Set scale value that an inactive menu will have
	inline void setStartScaleValue(float startScaleValue) { m_startScaleValue = startScaleValue; }

	inline float getButtonHeight() { return m_buttonHeight; }
	inline float getButtonSpacerHeight() { return m_buttonSpacerHeight; }
	inline float getTargetScaleValue() { return m_targetScaleValue; }

	//Set the Help Text - Will be displayed with lines and text hovering aboce the menu, if one touches a button
	void setHelpText(std::string rightIn, std::string leftIn, std::string upIn, std::string downIn);
	void renderHelpText(BUTTON_ID);
	void removeHelpText();

private:
	void idleCounterTick();

	//Frames to wait before the menu will be automatically hidden
	//If set to 0, the menu will stay active (default behaviour)
	int m_framesBeforeClose, m_idleFrameCounter;

	double m_animationState;
	float m_targetScaleValue, m_startScaleValue, m_menuFloatingHeight;

	osg::ref_ptr<osg::Geometry> m_backgroundGeometry;

	float m_buttonHeight, m_buttonWidth, m_buttonSpacerHeight, m_buttonTextYOffset;

	osg::Vec4f m_defaultColor;
	osg::Vec4f m_highlightColor;

	//Help Text Members ####################################
	std::string m_helpTextRight;
	std::string m_helpTextLeft;
	std::string m_helpTextUp;
	std::string m_helpTextDown;
	
	bool m_isHelpTextInitialised;
	float m_helpCharakterSize;

	osg::ref_ptr<osg::MatrixTransform> m_helMatTransform;
	osg::ref_ptr<osg::Geode> m_helpTextGeodeRight;
	osg::ref_ptr<osg::Geode> m_helpTextGeodeLeft;
	osg::ref_ptr<osg::Geode> m_helpTextGeodeUp;
	osg::ref_ptr<osg::Geode> m_helpTextGeodeDown;

	osg::ref_ptr<osg::Geometry> m_linesForDescriptionTextRight;
	osg::ref_ptr<osg::Geometry> m_linesForDescriptionTextLeft;
	osg::ref_ptr<osg::Geometry> m_linesForDescriptionTextUp;
	osg::ref_ptr<osg::Geometry> m_linesForDescriptionTextDown;
};