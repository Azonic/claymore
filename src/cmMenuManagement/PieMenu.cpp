// ClayMore - Immersive Mesh Modelling --- Copyright (c) 2014-2017 Philipp Ladwig, Jannik Fiedler, Jan Beutgen
#include "PieMenu.h"
#include <osg/LineWidth>

#include "../cmUtil/ConfigReader.h"

PieMenu::PieMenu(ConfigReader* configReaderIn,
	MenuName menuNameIn,
	float menuFloatingHeightIn,
	osg::Vec4f defaultColorIn,
	osg::Vec4f highlightColorIn,
	osg::Vec4f menuPointerColorIn,
	CONTROLLER_TYPE controllerType,
	float textSizeIn) :
	Menu(menuNameIn, controllerType, textSizeIn),
	m_configReader(configReaderIn),
	m_menuFloatingHeight(menuFloatingHeightIn),
	m_animationState(0.0),
	m_startScaleValue(0.015f),
	m_targetScaleValue(0.15f),
	m_idleFrameCounter(0),
	m_framesBeforeClose(0)
{
	Menu::m_defaultColor = defaultColorIn;
	Menu::m_highlightColor = highlightColorIn;

	m_menuTransform->setAttitude(osg::Quat(ConfigReader::PIE_MENU_CORRECTION_QUAT[0],
		osg::Vec3d(ConfigReader::PIE_MENU_CORRECTION_QUAT[1],
			ConfigReader::PIE_MENU_CORRECTION_QUAT[2],
			ConfigReader::PIE_MENU_CORRECTION_QUAT[3])));

	m_menuTransform->setPosition(osg::Vec3f(ConfigReader::PIE_MENU_CORRECTION_POS[0],
		ConfigReader::PIE_MENU_CORRECTION_POS[1],
		ConfigReader::PIE_MENU_CORRECTION_POS[2]));

	m_menuTransform->setScale(osg::Vec3f(m_startScaleValue, m_startScaleValue, m_startScaleValue));



	m_drawableControllerPointer = new osg::Geometry();
	m_geodeControllerPointer = new osg::Geode();
	m_geodeControllerPointer->addDrawable(m_drawableControllerPointer);

	m_controllerPointerVertices = new osg::Vec3Array;
	m_controllerPointerVertices->push_back(osg::Vec3f(0.0, -0.1, 0.0));
	m_controllerPointerVertices->push_back(osg::Vec3f(0.0, -0.1, 0.0));
	m_controllerPointerVertices->push_back(osg::Vec3f(0.0, -0.1, 0.0));
	m_controllerPointerVertices->push_back(osg::Vec3f(0.0, -0.1, 0.0));
	m_drawableControllerPointer->setVertexArray(m_controllerPointerVertices);

	m_contrPointerLine = new osg::DrawElementsUInt(osg::PrimitiveSet::QUADS, 0);
	m_contrPointerLine->push_back(3);
	m_contrPointerLine->push_back(2);
	m_contrPointerLine->push_back(1);
	m_contrPointerLine->push_back(0);
	m_drawableControllerPointer->addPrimitiveSet(m_contrPointerLine);

	osg::LineWidth* lineWidthPointer = new osg::LineWidth();
	lineWidthPointer->setWidth(2.0);
	m_geodeControllerPointer->getOrCreateStateSet()->setAttributeAndModes(lineWidthPointer, osg::StateAttribute::OVERRIDE | osg::StateAttribute::ON);
	m_geodeControllerPointer->getOrCreateStateSet()->setMode(GL_LIGHTING, osg::StateAttribute::OFF);

	m_colorArrayControllerPointer = new osg::Vec4Array;
	m_colorArrayControllerPointer->push_back(menuPointerColorIn);
	m_colorArrayControllerPointer->setBinding(osg::Array::BIND_OVERALL);
	m_drawableControllerPointer->setColorArray(m_colorArrayControllerPointer);

	m_menuTransform->addChild(m_geodeControllerPointer);
}

PieMenu::~PieMenu()
{
}

void PieMenu::updateAnimations(double deltaTimeIn)
{
	if (m_isVisible)
	{
		if (m_animationState < 1.0)
		{
			m_animationState += deltaTimeIn / 100.0 * pow(1.0 - m_animationState, 1.5) + 0.000001;
			m_animationState = m_animationState < 1.0 ? m_animationState : 1.0;

			float currentScaleValue = m_startScaleValue + (m_targetScaleValue - m_startScaleValue) * m_animationState;
			m_menuTransform->setScale(osg::Vec3f(currentScaleValue, currentScaleValue, currentScaleValue));
		}
	}
	else if (!m_isVisible)
	{
		if (m_animationState > 0.0)
		{
			m_animationState -= deltaTimeIn / 40.0 * pow(m_animationState, 1.5) + 0.000001;
			m_animationState = m_animationState > 0.0 ? m_animationState : 0.0;

			float currentScaleValue = m_startScaleValue + (m_targetScaleValue - m_startScaleValue) * m_animationState;
			m_menuTransform->setScale(osg::Vec3f(currentScaleValue, currentScaleValue, currentScaleValue));
		}
	}

	std::map<BUTTON_ID, MenuButton*>::iterator buIt = m_menuButtons.begin();

	while (buIt != m_menuButtons.end())
	{
		(*buIt).second->updateButtonAnimation(m_animationState, deltaTimeIn, m_startScaleValue, m_targetScaleValue);
		buIt++;
	}

	if (m_framesBeforeClose > 0)
		idleCounterTick();
}

void PieMenu::idleCounterTick()
{
	if (m_idleFrameCounter < 1)
	{
		if (m_isVisible)
			hide();
	}
	else
		--m_idleFrameCounter;
}


void PieMenu::attachToSceneGraph(osg::ref_ptr<osg::Group> parentNodeIn)
{
	parentNodeIn->addChild(m_menuTransform);
	//OSGShadow Test
	//m_menuTransform->setNodeMask(0x2); //this are globals from main //TODO: Make global: Config Manager?
	m_menuTransform->addChild(m_menuGeode);
}

void PieMenu::detachFromSceneGraph()
{
	if (m_menuTransform != nullptr)
	{
		std::vector<osg::Group*> parents = m_menuTransform->getParents();

		std::vector<osg::Group*>::iterator paIt = parents.begin();

		while (paIt != parents.end())
		{
			(*paIt)->removeChild(m_menuTransform);
			paIt++;
		}

		m_menuTransform->removeChild(m_menuGeode);
	}
}
void PieMenu::show()
{
	m_isVisible = true;
	refreshIdleCounter();
}

void PieMenu::hide()
{
	m_idleFrameCounter = 0;
	m_isVisible = false;
	hideAllSubMenus();
	unhighlightAllButtons();
}

void PieMenu::hideAllSubMenus()
{
	std::map<BUTTON_ID, MenuButton*>::iterator buIt = m_menuButtons.begin();

	while (buIt != m_menuButtons.end())
	{
		(*buIt).second->hideSubmenu();
		buIt++;
	}

	m_submenuActiveAtButton = INVALID_BUTTON_ID;
}

void PieMenu::unhighlightAllButtons()
{
	std::map<BUTTON_ID, MenuButton*>::iterator buIt = m_menuButtons.begin();

	while (buIt != m_menuButtons.end())
	{
		(*buIt).second->unhighlight();
		buIt++;
	}
}

BUTTON_ID PieMenu::createButton(std::string const & buttonText)
{
	m_menuButtons[m_idCounter] = new PieMenuButton(m_idCounter, m_textSize, 0, buttonText, Menu::m_defaultColor, Menu::m_highlightColor);
	m_menuButtons[m_idCounter]->attachToSceneGraph(m_menuTransform);
	return m_idCounter++;
}

void PieMenu::createSubButton(BUTTON_ID parentId, std::string const & buttonText)
{
	std::map<BUTTON_ID, MenuButton *>::const_iterator parentIt = m_menuButtons.find(parentId);

	if (parentIt == m_menuButtons.cend())
	{
		if (ConfigReader::DEBUG_LEVEL >= NOT_IN_LOOPS)
			std::cout << "Error in Menu::createSubButton - Parent id of " << parentId << " not found!" << std::endl;
	}
	else
		parentIt->second->createSubButton(buttonText);
}

//TODO: Make this nicer / more efficient
MenuButton* const PieMenu::getMenuButtonAtAngle(float angleIn)
{
	int buttonsCount = m_menuButtons.size();
	float buttonWidth = 2 * M_PI / (float)buttonsCount;

	angleIn -= (buttonWidth / 2.f);

	if (angleIn < 0)
		angleIn = (2 * M_PI) + angleIn;

	int buttonID = floorf(angleIn / buttonWidth) + 1;
	buttonID = buttonID < buttonsCount ? buttonID : 0;

	//std::cout << "buttonID" << buttonID << std::endl;

	if (m_submenuActiveAtButton != INVALID_BUTTON_ID)
	{
		std::cout << "In m_submenuActiveAtButton" << std::endl;
		return m_menuButtons.at(m_submenuActiveAtButton)->getSubButton(buttonID);
	}
	return m_menuButtons.at(buttonID);
}

void PieMenu::initializeDrawables()
{
	std::map<BUTTON_ID, MenuButton*>::iterator buIt = m_menuButtons.begin();

	while (buIt != m_menuButtons.end())
	{
		static_cast<PieMenuButton*>((*buIt).second)->initDrawable(m_menuButtons.size(), m_menuFloatingHeight);
		buIt++;
	}
}

void PieMenu::activateBackground()
{
	m_backgroundGeometry = new osg::Geometry();

	unsigned int pointsPerCircle = ConfigReader::PIE_MENU_RESOLUTION;

	//Define vertices
	osg::ref_ptr<osg::Vec3Array> vertices = new osg::Vec3Array();
	osg::ref_ptr<osg::DrawElementsUInt> drawElements = new osg::DrawElementsUInt(osg::PrimitiveSet::TRIANGLE_FAN, 0);

	vertices->push_back(osg::Vec3f(0.0f, m_menuFloatingHeight - 0.09, 0.0f));
	drawElements->push_back(0);
	float angle = 0.0f;
	for (unsigned int i = 0; i <= pointsPerCircle; ++i)
	{
		angle = (float)i / (float)pointsPerCircle * (M_PI * 2.0f);
		vertices->push_back(osg::Vec3f(cosf(angle), m_menuFloatingHeight - 0.09, sinf(angle)));
		drawElements->push_back(i + 1);
	}

	m_backgroundGeometry->setVertexArray(vertices);
	m_backgroundGeometry->addPrimitiveSet(drawElements);

	//Define colorArray (white with BIND_OVERALL)
	osg::ref_ptr<osg::Vec4Array> colors = new osg::Vec4Array;
	colors->push_back(osg::Vec4(0.1f, 0.1f, 0.1f, 0.95f));
	m_backgroundGeometry->setColorArray(colors);
	m_backgroundGeometry->setColorBinding(osg::Geometry::BIND_OVERALL);

	//Create StateSet
	osg::ref_ptr<osg::StateSet> stateSet = new osg::StateSet();
	m_backgroundGeometry->setStateSet(stateSet);

	//Disable lighting, enable transparency
	stateSet->setMode(GL_LIGHTING, osg::StateAttribute::OFF);
	//stateSet->setMode(GL_BLEND, osg::StateAttribute::ON);
	//stateSet->setRenderingHint(osg::StateSet::TRANSPARENT_BIN);
	//stateSet->setRenderBinDetails(14, "DepthSortedBin");

	//osg::ref_ptr<osg::CullFace> cullFace = new osg::CullFace();
	//cullFace->setMode(osg::CullFace::FRONT);
	//stateSet->setAttributeAndModes(cullFace, osg::StateAttribute::ON);

	m_menuGeode->addDrawable(m_backgroundGeometry);
}

void PieMenu::renderControllerPointer(osg::Vec2f menuCoordSysOffsetFromController)
{
	m_controllerPointerVertices->clear();
	m_controllerPointerVertices->push_back(osg::Vec3f(menuCoordSysOffsetFromController.x() - 0.015, m_menuFloatingHeight + 0.04, menuCoordSysOffsetFromController.y() - 0.015));
	m_controllerPointerVertices->push_back(osg::Vec3f(menuCoordSysOffsetFromController.x() + 0.015, m_menuFloatingHeight + 0.04, menuCoordSysOffsetFromController.y() - 0.015));
	m_controllerPointerVertices->push_back(osg::Vec3f(menuCoordSysOffsetFromController.x() + 0.015, m_menuFloatingHeight + 0.04, menuCoordSysOffsetFromController.y() + 0.015));
	m_controllerPointerVertices->push_back(osg::Vec3f(menuCoordSysOffsetFromController.x() - 0.015, m_menuFloatingHeight + 0.04, menuCoordSysOffsetFromController.y() + 0.015));
	
	//OBSCURE: VertexArray must  call dirty() and must be set again, otherwise no render update:
	m_controllerPointerVertices->dirty();
	m_drawableControllerPointer->setVertexArray(m_controllerPointerVertices);

}
