// ClayMore - Immersive Mesh Modelling --- Copyright (c) 2014-2017 Philipp Ladwig, Jannik Fiedler, Jan Beutgen
#pragma once
#include <iostream>

#include <osg/PositionAttitudeTransform>

#include "../../cmInteractionManagement/MenuObserver.h"

#include "../../cmMenuManagement/Menu.h"

class MenuManager;
class MiscDataStorage;

//Specialized TouchMenuObserver (but not inherits from TouchMenuObserver, because there is nothing to inherit)
//Specialized because it realises the sphere scaling (with dashed line between the controllers)
//TODO: Think about a better solution: Use the generic TouchMenuObserver an try to manage it with the registeredControllerIn and otherControllerIn
class imMenuManagement_DLL_import_export SelectionSphereTouchMenuObserver : public MenuObserver
{
public:
	SelectionSphereTouchMenuObserver(MenuName menuNameIn);
	~SelectionSphereTouchMenuObserver();

	inline void setMenuManager(MenuManager* menuManagerIn) { m_menuManager = menuManagerIn; };
	inline void setMiscDataStorage(MiscDataStorage* miscDataStorageIn) { m_miscDataStorage = miscDataStorageIn; };

protected:
	bool notify(Controller* registeredControllerIn, Controller* otherControllerIn, BUTTON_STATE registeredButtonState);

private:
		MiscDataStorage* m_miscDataStorage;
};