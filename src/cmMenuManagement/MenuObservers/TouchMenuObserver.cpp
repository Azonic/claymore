// ClayMore - Immersive Mesh Modelling --- Copyright (c) 2014-2017 Philipp Ladwig, Jannik Fiedler, Jan Beutgen
#include "TouchMenuObserver.h"
#include "cmGeometryManagement\GeometryManager.h"

TouchMenuObserver::TouchMenuObserver(MenuName menuNameIn, MenuManager* menuManager, MiscDataStorage* miscDataStorageIn) : m_miscDataStorage(miscDataStorageIn)
{
	m_lastButtonID = INVALID_BUTTON_ID;
	m_menuName = menuNameIn;
	m_menuManager = menuManager;
}

TouchMenuObserver::~TouchMenuObserver()
{

}

bool TouchMenuObserver::notify(Controller* registeredControllerIn, Controller* otherControllerIn, BUTTON_STATE registeredButtonState)
{
	TouchMenu* touchMenu = static_cast<TouchMenu*>(m_menuManager->getMenuWithUniqueName(m_menuName));
	if (touchMenu == nullptr)
	{
		std::cout << "Warning: nullptr from TouchMenu in TouchMenuObserver!" << std::endl;
	}
	else
	{
		if (touchMenu->isVisible())
		{
			float jX = registeredControllerIn->getJoystickX();
			float jY = registeredControllerIn->getJoystickY();

			touchMenu->unhighlightAllButtons();
			//if(g_isHelpTextActivated)
			touchMenu->removeHelpText();

			if (jX != 0.0f || jY != 0.0f)
			{
				touchMenu->refreshIdleCounter();

				float length = sqrtf(powf(jX, 2.0f) + powf(jY, 2.0f));

				jX /= length;
				jY /= length;

				float sin = asinf(jX);

				float angle = 0.0f;

				if (jY < 0.0f)
					angle = jX < 0.0f ? M_PI - 1.0f * sin : M_PI - sin;

				else
					angle = jX < 0.0f ? M_PI * 2.0f + sin : sin;

				MenuButton* const hitMenuButton = touchMenu->getMenuButtonAtAngle(angle);

				//bool isHighlightingControlledExternal = hitMenuButton->getHighlightActiveFromExternal();
				if (hitMenuButton != nullptr)
				{
					if (m_miscDataStorage != nullptr)
					{
						if (m_miscDataStorage->getIsHelpTextActivated())
						{
							touchMenu->renderHelpText(hitMenuButton->getButtonID());
						}
					}

					hitMenuButton->highlightHover();
					if (registeredControllerIn->getButtonState(BUTTON_TYPE::JOYSTICK) == HOLD)
					{
						hitMenuButton->highlightActive();
						if (hitMenuButton->canBeTriggeredConstantly())
						{
							hitMenuButton->trigger();
						}
					}
					else if (registeredControllerIn->getButtonState(BUTTON_TYPE::JOYSTICK) == PUSH)
					{
						hitMenuButton->highlightActive();
						registeredControllerIn->triggerHapticEvent(JOYSTICK_SHORT);
						BUTTON_ID submenuActiveAtButtonID = hitMenuButton->trigger();
						if (submenuActiveAtButtonID != INVALID_BUTTON_ID)
							touchMenu->setSubmenuActiveAtButton(submenuActiveAtButtonID);
					}

					if (m_lastButtonID != INVALID_BUTTON_ID && m_lastButtonID != hitMenuButton->getButtonID())
						registeredControllerIn->triggerHapticEvent(JOYSTICK_TINY);
					m_lastButtonID = hitMenuButton->getButtonID();
				}

				return false;
			}
		}
	}
	return true;
}