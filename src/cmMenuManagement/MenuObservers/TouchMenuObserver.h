// ClayMore - Immersive Mesh Modelling --- Copyright (c) 2014-2017 Philipp Ladwig, Jannik Fiedler, Jan Beutgen
#pragma once
#include <iostream>

#include <osg/PositionAttitudeTransform>

#include "../../cmInteractionManagement/MenuObserver.h"

#include "../../cmMenuManagement/Menu.h"

#include "cmMenuManagement\MenuManager.h"

#include "cmUtil/MiscDataStorage.h"

class imMenuManagement_DLL_import_export TouchMenuObserver : public MenuObserver
{
public:
	TouchMenuObserver(MenuName menuNameIn, MenuManager* menuManager, MiscDataStorage* miscDataStorageIn);
	~TouchMenuObserver();

protected:
	bool notify(Controller* registeredControllerIn, Controller* otherControllerIn, BUTTON_STATE registeredButtonState);

private:
	MiscDataStorage* m_miscDataStorage;
};