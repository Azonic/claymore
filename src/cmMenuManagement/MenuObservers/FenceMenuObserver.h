// ClayMore - Immersive Mesh Modelling --- Copyright (c) 2014-2017 Philipp Ladwig, Jannik Fiedler, Jan Beutgen
#pragma once
#include <iostream>

#include <osg/PositionAttitudeTransform>

#include "../../cmInteractionManagement/MenuObserver.h"

#include "cmMenuManagement\MenuManager.h"

class MenuManager;
class osgViewer::Viewer;

class imMenuManagement_DLL_import_export FenceMenuObserver : public MenuObserver
{
public:
	FenceMenuObserver(MenuName menuNameIn);
	~FenceMenuObserver();

	inline void setMenuManager(MenuManager* menuManager) { m_menuManager = menuManager; };
	inline void setMainViewer(osgViewer::Viewer* mainViewerIn) { m_mainViewer = mainViewerIn; };
	void triggerOpeningProcedureOnceFromExtern(Controller* registeredControllerIn, Controller* otherControllerIn);

protected:
	bool notify(Controller* registeredControllerIn, Controller* otherControllerIn, BUTTON_STATE registeredButtonState);

private:
	void showAndOpen(Controller* registeredControllerIn, Controller* otherControllerIn);

	MenuManager* m_menuManager;
	osgViewer::Viewer* m_mainViewer;
};