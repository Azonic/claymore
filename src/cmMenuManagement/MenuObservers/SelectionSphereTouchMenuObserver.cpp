// ClayMore - Immersive Mesh Modelling --- Copyright (c) 2014-2017 Philipp Ladwig, Jannik Fiedler, Jan Beutgen
#include "SelectionSphereTouchMenuObserver.h"
#include "cmGeometryManagement\GeometryManager.h"

#include "cmMenuManagement\MenuManager.h"

#include "../../cmUtil/MiscDataStorage.h"
#include "../../cmCore/ControllerTools/SphereControllerTool.h"
#include "../../cmCore/ControllerTools/DashedLineControllerTool.h"

SelectionSphereTouchMenuObserver::SelectionSphereTouchMenuObserver(MenuName menuNameIn)
{
	m_lastButtonID = INVALID_BUTTON_ID;
	m_menuName = menuNameIn;
	m_requiredControllerTools.push_back(ControllerTool::DASHED_LINE);
}

SelectionSphereTouchMenuObserver::~SelectionSphereTouchMenuObserver()
{

}

bool SelectionSphereTouchMenuObserver::notify(Controller* registeredControllerIn, Controller* otherControllerIn, BUTTON_STATE registeredButtonState)
{
	TouchMenu* touchMenu = static_cast<TouchMenu*>(m_menuManager->getMenuWithUniqueName(m_menuName));
	if (touchMenu == nullptr)
	{
		std::cout << "Warning: nullptr from TouchMenu in SelectionSphereTouchMenuObserver!" << std::endl;
	}
	else
	{
		if (touchMenu->isVisible())
		{
			float jX = registeredControllerIn->getJoystickX();
			float jY = registeredControllerIn->getJoystickY();

			touchMenu->unhighlightAllButtons();
			touchMenu->removeHelpText();

			if (jX != 0.0f || jY != 0.0f)
			{
				touchMenu->refreshIdleCounter();

				float length = sqrtf(powf(jX, 2.0f) + powf(jY, 2.0f));

				jX /= length;
				jY /= length;

				float sin = asinf(jX);

				float angle = 0.0f;

				if (jY < 0.0f)
					angle = jX < 0.0f ? M_PI - 1.0f * sin : M_PI - sin;

				else
					angle = jX < 0.0f ? M_PI * 2.0f + sin : sin;

				MenuButton* const hitMenuButton = touchMenu->getMenuButtonAtAngle(angle);

				if (hitMenuButton != nullptr)
				{
					if (m_miscDataStorage != nullptr)
					{
						if (m_miscDataStorage->getIsHelpTextActivated())
						{
							touchMenu->renderHelpText(hitMenuButton->getButtonID());
						}
					}

					hitMenuButton->highlightHover();
					if (registeredControllerIn->getButtonState(BUTTON_TYPE::JOYSTICK) == PUSH)
					{
						hitMenuButton->highlightActive();

						//If scale SphereSelection Tool Button is pressed do:
						if (hitMenuButton->getButtonID() == 0)
						{
							m_miscDataStorage->setLastSphereSelectionToolSize(static_cast<SphereControllerTool*>(
								registeredControllerIn->getControllerTool(ControllerTool::SPHERE))->getSphereScaleValue());
							m_miscDataStorage->setLastPriControllerPosition(registeredControllerIn->getPosition());
							m_miscDataStorage->setLastSecControllerPosition(otherControllerIn->getPosition());

							static_cast<DashedLineControllerTool*>(registeredControllerIn->getControllerTool(ControllerTool::DASHED_LINE))->setStartPoint(registeredControllerIn->getPosition());
							static_cast<DashedLineControllerTool*>(registeredControllerIn->getControllerTool(ControllerTool::DASHED_LINE))->setEndPoint(otherControllerIn->getPosition());
						}

						registeredControllerIn->triggerHapticEvent(JOYSTICK_SHORT);

						BUTTON_ID submenuActiveAtButtonID = hitMenuButton->trigger();

					}
					else if (registeredControllerIn->getButtonState(BUTTON_TYPE::JOYSTICK) == HOLD)
					{
						//If scale SphereSelection Tool Button is pressed do:
						if (hitMenuButton->getButtonID() == 0)
						{
							SphereControllerTool * sct = static_cast<SphereControllerTool*>(registeredControllerIn->getControllerTool(ControllerTool::SPHERE));

							//std::cout << "DRIN" << std::endl;
							osg::Vec3f lastControllerDistance = m_miscDataStorage->getLastPriControllerPosition() - m_miscDataStorage->getLastSecControllerPosition();
							osg::Vec3f currentControllerDistance = registeredControllerIn->getPosition() - otherControllerIn->getPosition();
							float lastControllerDistanceLength = lastControllerDistance.length() * 10.0f;
							float currentControllerDistanceLength = currentControllerDistance.length() * 10.0f;
							float finalScaleValue = m_miscDataStorage->getLastSphereSelectionToolSize() - (lastControllerDistanceLength - currentControllerDistanceLength);

							if (finalScaleValue >= 0.03f)
							{
								sct->setSphereScaleValue(finalScaleValue);
							}
							else {
								sct->setSphereScaleValue(0.03001);
							}

							//static_cast<DashedLineControllerTool*>(registeredControllerIn->getControllerTool(ControllerTool::DASHED_LINE))->set
							static_cast<DashedLineControllerTool*>(registeredControllerIn->getControllerTool(ControllerTool::DASHED_LINE))->setStartPoint(registeredControllerIn->getPosition());
							static_cast<DashedLineControllerTool*>(registeredControllerIn->getControllerTool(ControllerTool::DASHED_LINE))->setEndPoint(otherControllerIn->getPosition());
							static_cast<DashedLineControllerTool*>(registeredControllerIn->getControllerTool(ControllerTool::DASHED_LINE))->showLine();
						}
						hitMenuButton->highlightActive();
						if (hitMenuButton->canBeTriggeredConstantly())
						{
							hitMenuButton->trigger();
						}
					}
					else
					{
						static_cast<DashedLineControllerTool*>(registeredControllerIn->getControllerTool(ControllerTool::DASHED_LINE))->hideLine();
					}

					if (m_lastButtonID != INVALID_BUTTON_ID && m_lastButtonID != hitMenuButton->getButtonID())
						registeredControllerIn->triggerHapticEvent(JOYSTICK_TINY);
					m_lastButtonID = hitMenuButton->getButtonID();
				}
				return false;
			}
		}
	}
	return true;
}