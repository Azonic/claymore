// ClayMore - Immersive Mesh Modelling --- Copyright (c) 2014-2017 Philipp Ladwig, Jannik Fiedler, Jan Beutgen
#include "PieMenuObserver.h"
#include "cmGeometryManagement\GeometryManager.h"

#include "cmMenuManagement\MenuManager.h"

PieMenuObserver::PieMenuObserver(MenuName menuNameIn)
{
	m_lastButtonID = INVALID_BUTTON_ID;
	m_menuName = menuNameIn;
}

PieMenuObserver::~PieMenuObserver()
{

}

bool PieMenuObserver::notify(Controller* registeredControllerIn, Controller* otherControllerIn, BUTTON_STATE registeredButtonState)
{
	PieMenu* pieMenu = static_cast<PieMenu*>(m_menuManager->getMenuWithUniqueName(m_menuName));
	if (pieMenu == nullptr)
	{
		std::cout << "Warning: nullptr from PieMenu in PieMenuObserver!" << std::endl;
	}
	else {

		if (!pieMenu->isVisible())
		{
			if (registeredControllerIn->getButtonState(BUTTON_TYPE::JOYSTICK) == PUSH)
			{
				pieMenu->show();
				registeredControllerIn->triggerHapticEvent(JOYSTICK_LONG);
				m_lastButtonID = INVALID_BUTTON_ID;
			}
		}
		else
		{
			float jX = registeredControllerIn->getJoystickX();
			float jY = registeredControllerIn->getJoystickY();

			pieMenu->renderControllerPointer(osg::Vec2f(jX, -jY));

			pieMenu->unhighlightAllButtons();

			if (jX != 0.0f || jY != 0.0f)
			{
				pieMenu->refreshIdleCounter();

				float length = sqrtf(powf(jX, 2.0f) + powf(jY, 2.0f));

				jX /= length;
				jY /= length;

				float sin = asinf(jX);

				float angle = 0.0f;

				if (jY < 0.0f)
					angle = jX < 0.0f ? M_PI - 1.0f * sin : M_PI - sin;

				else
					angle = jX < 0.0f ? M_PI * 2.0f + sin : sin;

				MenuButton* const hitMenuButton = pieMenu->getMenuButtonAtAngle(angle);


				if (hitMenuButton != nullptr)
				{
					if (registeredControllerIn->getButtonState(REAR_TRIGGER) == HOLD)
						hitMenuButton->highlightActive();
					else if (registeredControllerIn->getButtonState(BUTTON_TYPE::JOYSTICK) == HOLD)
					{
						if (hitMenuButton->canBeTriggeredConstantly())
						{
							hitMenuButton->trigger();
							registeredControllerIn->triggerHapticEvent(JOYSTICK_TINY);
						}
					}
					else if (registeredControllerIn->getButtonState(REAR_TRIGGER) == RELEASE || registeredControllerIn->getButtonState(BUTTON_TYPE::JOYSTICK) == PUSH)
					{
						registeredControllerIn->triggerHapticEvent(JOYSTICK_SHORT);
						BUTTON_ID submenuActiveAtButtonID = hitMenuButton->trigger();
						if (submenuActiveAtButtonID != INVALID_BUTTON_ID)
							pieMenu->setSubmenuActiveAtButton(submenuActiveAtButtonID);
					}
					else
						hitMenuButton->highlightHover();

					if (m_lastButtonID != INVALID_BUTTON_ID && m_lastButtonID != hitMenuButton->getButtonID())
						registeredControllerIn->triggerHapticEvent(JOYSTICK_TINY);
					m_lastButtonID = hitMenuButton->getButtonID();
				}
				return false;
			}

			//PrimPieObjectSelectMenuObserver ISSUE
			//	if (hitMenuButton != nullptr)
			//	{
			//		if (registeredControllerIn->getButtonState(BUTTON_TYPE::JOYSTICK) == HOLD)
			//		{
			//			hitMenuButton->highlightActive();
			//			if (hitMenuButton->canBeTriggeredConstantly())
			//			{
			//				hitMenuButton->trigger();
			//				registeredControllerIn->triggerHapticEvent(JOYSTICK_TINY);
			//			}
			//		}
			//		else if (registeredControllerIn->getButtonState(BUTTON_TYPE::JOYSTICK) == RELEASE)
			//		{
			//			if (!hitMenuButton->canBeTriggeredConstantly())
			//				registeredControllerIn->triggerHapticEvent(JOYSTICK_SHORT);
			//			hitMenuButton->trigger();
			//		}
			//		else
			//			hitMenuButton->highlightHover();
			//	}
			//}
			//else if(registeredControllerIn->getButtonState(REAR_TRIGGER) == HOLD) //If the trigger ist still pressed, leave the menu open
			//	pieMenu->refreshIdleCounter();


			//if (m_menuName == PIE_OBJECT_SELECT)
			//{
			//	//If it is not some kind of "main" menu -> still pass the events down in interactionManagement::detectInteractionsAndNotify()
			//	return true;
			//}
			//else
			//{
			//	//If a "main" menu, stop here
			//	return false;
			//}

		}
	}
	return true;
}