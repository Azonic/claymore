// ClayMore - Immersive Mesh Modelling --- Copyright (c) 2014-2017 Philipp Ladwig, Jannik Fiedler, Jan Beutgen
#include "TiltMenuObserver.h"

#include "cmCore/defines.h"

#include "cmGeometryManagement/GeometryManager.h"

#include "cmMenuManagement/MenuManager.h"

#include <cmUtil/Quat2EulerAndBackConverter.h>

#include "cmGeometryManagement/GeometryManager.h"

#include "cmUtil/MiscDataStorage.h"




TiltMenuObserver::TiltMenuObserver(MenuName menuNameIn, bool isEasyModeActiveIn) : m_isEasyModeActive(isEasyModeActiveIn)
{
	m_lastButtonID = INVALID_BUTTON_ID;
	m_lastSelectedButton = 0;
	m_menuName = menuNameIn;
}

TiltMenuObserver::~TiltMenuObserver()
{

}

bool TiltMenuObserver::notify(Controller* registeredControllerIn, Controller* otherControllerIn, BUTTON_STATE registeredButtonState)
{
	TiltMenu* tiltMenu = static_cast<TiltMenu*>(m_menuManager->getMenuWithUniqueName(m_menuName));
	osg::Vec3f controllerPosition = osg::Vec3f(registeredControllerIn->getPosition());
	osg::Quat controllerRot = osg::Quat(registeredControllerIn->getRotation());

	if (!tiltMenu->isVisible())
	{
		//if (registeredControllerIn->getButtonState(BUTTON_TYPE::MENU) == PUSH)
		if (registeredButtonState == PUSH)
		{
			m_menuManager->hideAllMenus();
			tiltMenu->show();
			m_geometryManager->activateWireframeForAllGeometries();
			registeredControllerIn->triggerHapticEvent(TRIGGER_LONG);
			m_lastButtonID = INVALID_BUTTON_ID;

			Quat2EulerAndBackConverter quat2EulerAndBackConverter;

			double yaw, roll, pitch;
			quat2EulerAndBackConverter.getEulerFromQuat(controllerRot, yaw, roll, pitch);

			osg::Quat menuTargetRotation;
			quat2EulerAndBackConverter.getQuatFromEuler(yaw, 0.0, pitch, menuTargetRotation);

			tiltMenu->setRotation(&menuTargetRotation);

			//offsetOnZ is used for putting the menu behind the controller from the view of the user
			osg::Vec3f offsetOnZ;
			offsetOnZ.set(osg::Vec3f(0.0, 0.0, -0.15));
			offsetOnZ = controllerPosition + (controllerRot * offsetOnZ);
			tiltMenu->setPosition(&offsetOnZ);
		}

	}
	else
	{
		if (m_menuName == TILT_AND_SHIFT_MODE)
		{
			//Display the text of EditMode in grey, if nothing is selected
			if (m_geometryManager->getNameOfActiveGeometry().compare(g_stringNoSelectionAssertion) != 0 &&
				m_geometryManager->getNameOfActiveGeometry().compare(g_stringNothingInSceneAssertion) != 0)
			{
				static_cast<TiltMenuButton*>(tiltMenu->getMenuButton(1))->markAsActive(true); //EDIT MODE
				static_cast<TiltMenuButton*>(tiltMenu->getMenuButton(3))->markAsActive(false); //SCULPT MODE
			}
			else
			{
				//TODO: Better why for getting the MenuButton? Maybe with a a string "Edit\nMode"?
				static_cast<TiltMenuButton*>(tiltMenu->getMenuButton(1))->markAsActive(false); //EDIT MODE
				static_cast<TiltMenuButton*>(tiltMenu->getMenuButton(3))->markAsActive(false); //SCULPT MODE
			}

			//Highlight the approriate text, depending which mode is currently active
			static_cast<TiltMenuButton*>(tiltMenu->getMenuButton(0))->highlightTextColor(m_miscDataStorage->getGlobalApplicationMode() == ApplicationMode::OBJECT_MODE); //OBJECT MODE
			static_cast<TiltMenuButton*>(tiltMenu->getMenuButton(1))->highlightTextColor(m_miscDataStorage->getGlobalApplicationMode() == ApplicationMode::EDIT_MODE); //EDIT MODE
			static_cast<TiltMenuButton*>(tiltMenu->getMenuButton(2))->highlightTextColor(m_miscDataStorage->getGlobalApplicationMode() == ApplicationMode::SCAN_MODE); //OBJECT MODE

			/*if (m_miscDataStorage->getGlobalApplicationMode() == ApplicationMode::OBJECT_MODE)
			{
				static_cast<TiltMenuButton*>(tiltMenu->getMenuButton(0))->highlightTextColor(true); //OBJECT MODE
				static_cast<TiltMenuButton*>(tiltMenu->getMenuButton(1))->highlightTextColor(false); //EDIT MODE
			}
			if (m_miscDataStorage->getGlobalApplicationMode() == ApplicationMode::EDIT_MODE)
			{
				static_cast<TiltMenuButton*>(tiltMenu->getMenuButton(0))->highlightTextColor(false); //OBJECT MODE
				static_cast<TiltMenuButton*>(tiltMenu->getMenuButton(1))->highlightTextColor(true); //EDIT MODE
			}*/
		} else if (m_customBehaviourOnNotify != nullptr) {
			m_customBehaviourOnNotify(tiltMenu, m_miscDataStorage);
		}
		////todo: maybe better if a menu selection is performed when push menu button again
		//if (registeredcontrollerin->getbuttonstate(button_type::joystick) == push)
		//{
		//	tiltandshiftmenu->hide();
		//	registeredcontrollerin->triggerhapticevent(trigger_long);
		//	return false;
		//}

		tiltMenu->unhighlightAllButtons();
		tiltMenu->refreshIdleCounter();

		//Quat for getting a negativ or positive value (hanging around at 0 (and +-1)) in the dot product of line: float finalAngle = startRotVec * currentRotVec;
		//instead of "hanging" around only in <1
		osg::Quat offsetQuat;
		offsetQuat.makeRotate(osg::X_AXIS, osg::Y_AXIS);
		
		//Get a vector of the current rotation
		osg::Vec3f currentRotVec = controllerRot * osg::X_AXIS;
		//m_rotationAsButtonPushed saves the rotation in the moment where the menu pops up
		//Actually it's magic, because I dont know what I have done here. [Quat * Quat * Vec3f]
		//A Quat multiplyed by a Quad is a rotation. A Quat rotated with an Vec3f is a Vec3f rotated by the Quat
		osg::Vec3f startRotVec = offsetQuat *  m_rotationAsButtonPushed * osg::X_AXIS;
		//Dot product between the start and the current rotation vecor
		
		//NOTE from Damian: This did still have an issue with rotations over 180�; If the controller was rotated far enough the controllerpointer of the tilt menu would rotate in the inverse direction.
		//					I did find a solution to calculate the actual signed angle at: https://stackoverflow.com/questions/5188561/signed-angle-between-two-3d-vectors-with-same-origin-within-the-same-plane
		//					The implementation below is tested and does work correctly.

		/*float finalAngle = startRotVec * currentRotVec;

		MenuButton* const hitMenuButton = tiltMenu->getMenuButtonAtAngle(-finalAngle * (M_PI / 2.0), m_lastSelectedButton);
		m_lastSelectedButton = hitMenuButton->getButtonID();
		tiltMenu->renderControllerPointer(-finalAngle * (M_PI / 2.0) - (M_PI_2));*/

		//This code does work correctly
		osg::Vec3f n = tiltMenu->getMenuTransform()->getAttitude() * osg::Z_AXIS;										//Calculate the normal of the tilt menu plane
		n.normalize();																									// Just to make sure the normal is normalized, although this should most likely already be the case anyway.
		float finalAngle = atan2((startRotVec ^ currentRotVec) * n, startRotVec * currentRotVec);						//calculate the signed angle
		
		MenuButton* const hitMenuButton = tiltMenu->getMenuButtonAtAngle(-finalAngle - M_PI_2, m_lastSelectedButton);	//The angle had an actual offset of 90� and had to be inverted
		m_lastSelectedButton = hitMenuButton->getButtonID();
		tiltMenu->renderControllerPointer(-finalAngle - M_PI);															//The pointer itself had an 180� offset and had to be inverted aswell


		if (hitMenuButton != nullptr)
		{
			//if (registeredControllerIn->getButtonState(REAR_TRIGGER) == HOLD)
			if (registeredButtonState == HOLD)
			{
				hitMenuButton->highlightActive();
			}
			//Close menu if Menu Button is released
			//else if (registeredControllerIn->getButtonState(BUTTON_TYPE::MENU) == RELEASE && !m_isEasyModeActive)
			else if (registeredButtonState == RELEASE && !m_isEasyModeActive)
			{
				BUTTON_ID activeAtButtonID = hitMenuButton->trigger();
				registeredControllerIn->triggerHapticEvent(JOYSTICK_LONG);
				tiltMenu->hide();
				return true;
			}
			//Close menu if Menu Button is triggered again
			//else if ((registeredControllerIn->getButtonState(BUTTON_TYPE::MENU) == PUSH || registeredControllerIn->getButtonState(BUTTON_TYPE::REAR_TRIGGER) == PUSH) && m_isEasyModeActive)
			else if ((registeredButtonState == PUSH || registeredControllerIn->getButtonState(BUTTON_TYPE::REAR_TRIGGER) == PUSH) && m_isEasyModeActive)
			{
				BUTTON_ID activeAtButtonID = hitMenuButton->trigger();
				registeredControllerIn->triggerHapticEvent(JOYSTICK_LONG);
				tiltMenu->hide();
				return true;
			}
			else
			{
				hitMenuButton->highlightHover();
			}


			if (m_lastButtonID != INVALID_BUTTON_ID && m_lastButtonID != hitMenuButton->getButtonID())
			{
				registeredControllerIn->triggerHapticEvent(JOYSTICK_SHORT);
			}
			m_lastButtonID = hitMenuButton->getButtonID();
		}

		else if (registeredControllerIn->getButtonState(REAR_TRIGGER) == HOLD) //If the trigger ist still pressed, leave the menu open
		{
			tiltMenu->refreshIdleCounter();
		}

		return false;
	}

	return true;
}