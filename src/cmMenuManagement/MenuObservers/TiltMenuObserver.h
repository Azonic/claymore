// ClayMore - Immersive Mesh Modelling --- Copyright (c) 2014-2017 Philipp Ladwig, Jannik Fiedler, Jan Beutgen
#pragma once
#include <iostream>

#include <osg/PositionAttitudeTransform>

#include "../../cmInteractionManagement/MenuObserver.h"

#include "../../cmMenuManagement/Menu.h"

class GeometryManager;
class MenuManager;
class MiscDataStorage;

class imMenuManagement_DLL_import_export TiltMenuObserver : public MenuObserver
{
private:
	typedef void (*CustomBehaviourOnNotify) (TiltMenu* tiltMenu, MiscDataStorage* miscDataStorage);
public:
	TiltMenuObserver(MenuName menuNameIn, bool isEasyModeActiveIn);
	~TiltMenuObserver();

	inline void setGeometryManager(osg::ref_ptr<GeometryManager> geometryManager) { m_geometryManager = geometryManager; };
	inline void setMenuManager(MenuManager* menuManager) { m_menuManager = menuManager; };
	inline void setMiscDataStorage(MiscDataStorage* miscDataStorageIn) { m_miscDataStorage = miscDataStorageIn; };
	inline void setCustomBehaviourOnNotify(CustomBehaviourOnNotify customBehaviourOnNotify) { m_customBehaviourOnNotify = customBehaviourOnNotify; };

protected:
	bool notify(Controller* registeredControllerIn, Controller* otherControllerIn, BUTTON_STATE registeredButtonState);

private:
	osg::ref_ptr<GeometryManager> m_geometryManager;
	MenuManager* m_menuManager;
	MiscDataStorage* m_miscDataStorage;

	CustomBehaviourOnNotify m_customBehaviourOnNotify;

	osg::Quat m_rotationAsButtonPushed;
	unsigned int m_lastSelectedButton;
	bool m_isEasyModeActive;
};