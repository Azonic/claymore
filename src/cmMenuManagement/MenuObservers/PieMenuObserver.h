// ClayMore - Immersive Mesh Modelling --- Copyright (c) 2014-2017 Philipp Ladwig, Jannik Fiedler, Jan Beutgen
#pragma once
#include <iostream>

#include <osg/PositionAttitudeTransform>

#include "../../cmInteractionManagement/MenuObserver.h"

#include "../../cmMenuManagement/Menu.h"

class MenuManager;

class imMenuManagement_DLL_import_export PieMenuObserver : public MenuObserver
{
	public:
		PieMenuObserver(MenuName menuNameIn);
		~PieMenuObserver();

		inline void setMenuManager(MenuManager* menuManager) { m_menuManager = menuManager; };

	protected:
		bool notify(Controller* registeredControllerIn, Controller* otherControllerIn, BUTTON_STATE registeredButtonState);

};