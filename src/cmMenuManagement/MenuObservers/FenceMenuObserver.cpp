// ClayMore - Immersive Mesh Modelling --- Copyright (c) 2014-2017 Philipp Ladwig, Jannik Fiedler, Jan Beutgen
#include "FenceMenuObserver.h"
#include "cmGeometryManagement\GeometryManager.h"

#include "cmMenuManagement\MenuManager.h"
#include <osgViewer\Viewer>

#include <cmUtil\Quat2EulerAndBackConverter.h>

//TODO: Employ inheritance!
FenceMenuObserver::FenceMenuObserver(MenuName menuNameIn)
{
	m_lastButtonID = INVALID_BUTTON_ID;
	m_menuName = menuNameIn;
}

FenceMenuObserver::~FenceMenuObserver()
{

}

void FenceMenuObserver::showAndOpen(Controller* registeredControllerIn, Controller* otherControllerIn)
{
	FenceMenu* fenceMenu = static_cast<FenceMenu*>(m_menuManager->getMenuWithUniqueName(m_menuName));

	fenceMenu->show();
	registeredControllerIn->triggerHapticEvent(JOYSTICK_LONG);
	m_lastButtonID = INVALID_BUTTON_ID;

	//Set position of the menu
	osg::Vec3f controllerPosition = osg::Vec3f(registeredControllerIn->getPosition());
	//This could be dependent of the last Button, a user had pushed -> Menu grows at the last pushed button position

	//Set rotation of menu. First get the viewMatrix and use the inverse for facing the menu to the user
	std::vector<osg::Camera*> cameraList;
	m_mainViewer->getCameras(cameraList);
	osg::Matrixd viewMatrix;
	if (cameraList.at(0))
	{
		viewMatrix = cameraList.at(0)->getViewMatrix();
	}
	//OLD:    #######################################################################
	osg::Matrixd viewMatrixInv = osg::Matrix::inverse(viewMatrix);
	fenceMenu->setRotation(&viewMatrixInv.getRotate());

	//offsetOnZ is used for putting the menu behind the controller from the view of the user
	osg::Vec3f offsetOnZ;
	offsetOnZ.set(osg::Vec3f(0.0, 0.0, -0.2256));
	offsetOnZ = controllerPosition + (viewMatrixInv.getRotate() * offsetOnZ);

	float offsetY = fenceMenu->getTargetScaleValue() * (fenceMenu->getNumberOfButtons() * (fenceMenu->getButtonHeight() + fenceMenu->getButtonSpacerHeight()) / 2.0 - fenceMenu->getButtonSpacerHeight());
	//std::cout << "fenceMenu->getNumberOfButtons()" << fenceMenu->getNumberOfButtons() << std::endl;
	//std::cout << "fenceMenu->getButtonHeight()" << fenceMenu->getButtonHeight() << std::endl;
	//std::cout << "fenceMenu->getButtonSpacerHeight()" << fenceMenu->getButtonSpacerHeight() << std::endl;
	//std::cout << "offsetY" << offsetY << std::endl;
	offsetOnZ.set(offsetOnZ.x(), offsetOnZ.y() + offsetY, offsetOnZ.z());

	fenceMenu->setPosition(&offsetOnZ);

	//NEW TRY:    #######################################################################

	//osg::Vec3d vectorToEye = viewMatrix.getTrans() - controllerPosition;
	//vectorToEye.normalize();
	//osg::Vec3d sideVector = vectorToEye ^ osg::Y_AXIS; //Cross Product
	//sideVector.normalize();
	//osg::Vec3d upVector = sideVector ^ vectorToEye;
	//upVector.normalize();


	////osg::Quat testquat = osg::Quat(0.0, 0.0, 0.0, 1.0);
	////fenceMenu->setRotation(&testquat);

	//osg::Matrix matrix;
	//fenceMenu->setRotation(&osg::Quat(0.0, 0.0, 0.0, 1.0));
	//matrix = osg::Matrix::rotate(osg::Quat(0.0, 0.0, 0.0, 1.0) * osg::Vec3d(1.0, 0.0, 0.0), vectorToEye);
	////matrix.makeLookAt(test, viewMatrix.getTrans(), osg::Vec3d(osg::Y_AXIS));
	//fenceMenu->setRotation(&matrix.getRotate());
	//fenceMenu->setPosition(&controllerPosition);

}

//Triggering the menu for opening once at the controller position
//Is needed, if the fenceMenuObserver is just registered and it knows nothing about the "past" positions
void FenceMenuObserver::triggerOpeningProcedureOnceFromExtern(Controller* registeredControllerIn, Controller* otherControllerIn)
{
	showAndOpen(registeredControllerIn, otherControllerIn);
}

bool FenceMenuObserver::notify(Controller* registeredControllerIn, Controller* otherControllerIn, BUTTON_STATE registeredButtonState)
{
	FenceMenu* fenceMenu = static_cast<FenceMenu*>(m_menuManager->getMenuWithUniqueName(m_menuName));

	if (!fenceMenu->isVisible())
	{
		if (registeredControllerIn->getButtonState(BUTTON_TYPE::JOYSTICK) == PUSH)
		{
			showAndOpen(registeredControllerIn, otherControllerIn);
		}
	}
	else
	{
		fenceMenu->unhighlightAllButtons();
		fenceMenu->refreshIdleCounter();

		osg::Matrix W2LMenu = osg::computeWorldToLocal(fenceMenu->getMenuTransform()->getParentalNodePaths()[0]);

		osg::Vec3f controllerPosition = osg::Vec3f(registeredControllerIn->getPosition());
		osg::Vec3f menuCoordSysOffsetFromController = controllerPosition * W2LMenu;

		fenceMenu->renderControllerPointer(menuCoordSysOffsetFromController);

		MenuButton* const hitMenuButton = fenceMenu->getMenuButtonAtPosition(menuCoordSysOffsetFromController.x(), menuCoordSysOffsetFromController.y());

		float numButtons = fenceMenu->getNumberOfButtons();
		float buttonWidth = fenceMenu->getButtonWidth();
		float buttonHeight = fenceMenu->getButtonHeight();
		float buttonSpacerHeight = fenceMenu->getButtonSpacerHeight();

		if (hitMenuButton != nullptr) //If pointer is on the menu, than:
		{
			if (registeredControllerIn->getButtonState(BUTTON_TYPE::REAR_TRIGGER) == PUSH
				|| registeredControllerIn->getButtonState(BUTTON_TYPE::JOYSTICK) == PUSH) // Release would be the profi mode, PUSH the beginner
			{
				hitMenuButton->highlightActive();
				fenceMenu->hide();
				registeredControllerIn->triggerHapticEvent(JOYSTICK_SHORT);
				hitMenuButton->trigger();
			}
			else
				hitMenuButton->highlightHover();

			if (m_lastButtonID != INVALID_BUTTON_ID && m_lastButtonID != hitMenuButton->getButtonID())
				registeredControllerIn->triggerHapticEvent(JOYSTICK_TINY);
			m_lastButtonID = hitMenuButton->getButtonID();
		}
		else //If no button is hit (hence the pointer is off the menu) 
		{
			if (registeredControllerIn->getButtonState(BUTTON_TYPE::REAR_TRIGGER) == PUSH
				|| registeredControllerIn->getButtonState(BUTTON_TYPE::JOYSTICK) == PUSH) //... and the trigger or joystick is hit, the menu will be closed:
			{
				fenceMenu->hide();

				if (m_menuName == FENCE_EDIMODE_DETAIL_COMMAND)
				{
					fenceMenu->closeCallback();
				}
			}

		}
		return false;
	}
	return true;
}