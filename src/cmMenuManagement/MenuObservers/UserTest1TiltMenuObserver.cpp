// ClayMore - Immersive Mesh Modelling --- Copyright (c) 2014-2017 Philipp Ladwig, Jannik Fiedler, Jan Beutgen
#include "UserTest1TiltMenuObserver.h"
#include "cmGeometryManagement\GeometryManager.h"

#include "cmMenuManagement\MenuManager.h"

#include <cmUtil\Quat2EulerAndBackConverter.h>

#include "cmGeometryManagement/GeometryManager.h"


UserTest1TiltMenuObserver::UserTest1TiltMenuObserver(MenuName menuNameIn)
{
	m_lastButtonID = INVALID_BUTTON_ID;
	m_lastSelectedButton = 0;
	m_menuName = menuNameIn;
}

UserTest1TiltMenuObserver::~UserTest1TiltMenuObserver()
{

}

bool UserTest1TiltMenuObserver::notify(Controller* registeredControllerIn, Controller* otherControllerIn, BUTTON_STATE registeredButtonState)
{
	TiltMenu* tiltMenu = static_cast<TiltMenu*>(m_menuManager->getMenuWithUniqueName(m_menuName));

	osg::Vec3f controllerPosition = osg::Vec3f(registeredControllerIn->getPosition());
	osg::Quat controllerRot = osg::Quat(registeredControllerIn->getRotation());

	if (!tiltMenu->isVisible())
	{
		if (registeredControllerIn->getButtonState(BUTTON_TYPE::JOYSTICK) == PUSH)
		{
			tiltMenu->show();
			registeredControllerIn->triggerHapticEvent(TRIGGER_LONG);
			m_lastButtonID = INVALID_BUTTON_ID;
			//////m_menuManager->hideMenuWithUniqueName(PIE_EDIT);
			//////m_menuManager->hideMenuWithUniqueName(PIE_OBJECT);

			Quat2EulerAndBackConverter quat2EulerAndBackConverter;

			double yaw, roll, pitch;
			quat2EulerAndBackConverter.getEulerFromQuat(controllerRot, yaw, roll, pitch);

			osg::Quat menuTargetRotation;
			quat2EulerAndBackConverter.getQuatFromEuler(yaw, 0.0, pitch, menuTargetRotation);

			tiltMenu->setRotation(&menuTargetRotation);

			//offsetOnZ is used for putting the menu behind the controller from the view of the user
			osg::Vec3f offsetOnZ;
			offsetOnZ.set(osg::Vec3f(0.0, 0.0, -0.15));
			offsetOnZ = controllerPosition + (controllerRot * offsetOnZ);
			tiltMenu->setPosition(&offsetOnZ);
		}

	}
	else
	{
		////TODO: Maybe better if a menu selection is performed when push menu button again
		//if (registeredControllerIn->getButtonState(BUTTON_TYPE::JOYSTICK) == PUSH)
		//{
		//	tiltMenu->hide();
		//	registeredControllerIn->triggerHapticEvent(TRIGGER_LONG);
		//	return false;
		//}

		tiltMenu->unhighlightAllButtons();
		tiltMenu->refreshIdleCounter();

		//Quat for getting a negativ or positive value (hanging around at 0 (and +-1)) in the dot product of line: float finalAngle = startRotVec * currentRotVec;
		//instead of "hanging" around only in <1
		osg::Quat offsetQuat;
		offsetQuat.makeRotate(osg::X_AXIS, osg::Y_AXIS);

		//Get a vector of the current rotation
		osg::Vec3f currentRotVec = controllerRot * osg::X_AXIS;
		//m_rotationAsButtonPushed saves the rotation in the moment where the menu pops up
		//Actually it's magic, because I dont know what I have done here. [Quat * Quat * Vec3f]
		//A Quat multiplyed by a Quad is a rotation. A Quat rotated with an Vec3f is a Vec3f rotated by the Quat
		osg::Vec3f startRotVec = offsetQuat *  m_rotationAsButtonPushed * osg::X_AXIS;
		//Dot product between the start and the current rotation vecor
		float finalAngle = startRotVec * currentRotVec;

		MenuButton* const hitMenuButton = tiltMenu->getMenuButtonAtAngle(-finalAngle * (M_PI / 2.0), m_lastSelectedButton);
		m_lastSelectedButton = hitMenuButton->getButtonID();
		tiltMenu->renderControllerPointer(-finalAngle * (M_PI / 2.0) - (M_PI_2));

		if (hitMenuButton != nullptr)
		{
			if (registeredControllerIn->getButtonState(REAR_TRIGGER) == HOLD)
			{
				hitMenuButton->highlightActive();
			}

			else if (registeredControllerIn->getButtonState(REAR_TRIGGER) == PUSH || registeredControllerIn->getButtonState(BUTTON_TYPE::JOYSTICK) == PUSH)
			{
				registeredControllerIn->triggerHapticEvent(JOYSTICK_LONG);
				BUTTON_ID submenuActiveAtButtonID = hitMenuButton->trigger();
			}
			else
			{
				hitMenuButton->highlightHover();
			}


			if (m_lastButtonID != INVALID_BUTTON_ID && m_lastButtonID != hitMenuButton->getButtonID())
			{
				registeredControllerIn->triggerHapticEvent(JOYSTICK_SHORT);
			}
			m_lastButtonID = hitMenuButton->getButtonID();
		}

		else if (registeredControllerIn->getButtonState(REAR_TRIGGER) == HOLD) //If the trigger ist still pressed, leave the menu open
		{
			tiltMenu->refreshIdleCounter();
		}

		return false;
	}

	return true;
}