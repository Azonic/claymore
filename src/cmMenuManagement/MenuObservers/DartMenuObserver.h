// ClayMore - Immersive Mesh Modelling --- Copyright (c) 2014-2017 Philipp Ladwig, Jannik Fiedler, Jan Beutgen
#pragma once
#include <iostream>

#include <osg/PositionAttitudeTransform>

#include "../../cmInteractionManagement/MenuObserver.h"

class GeometryManager;
class MenuManager;
class MenuButton;

class imMenuManagement_DLL_import_export DartMenuObserver : public MenuObserver
{
	public:
		DartMenuObserver(MenuName menuNameIn);
		~DartMenuObserver();

		inline void setGeometryManager(osg::ref_ptr<GeometryManager> geometryManager) { m_geometryManager = geometryManager; };
		inline void setMenuManager(MenuManager* menuManager) { m_menuManager = menuManager; };
		
		void triggerOpeningProcedureOnceFromExtern(Controller* registeredControllerIn, Controller* otherControllerIn);
		
		//inline void waitAndDoNothingUntilTriggerPressedAgain(bool valueIn) { m_waitTriggerStateHasChanged = valueIn; }

	protected:
		bool notify(Controller* registeredControllerIn, Controller* otherControllerIn, BUTTON_STATE registeredButtonState);

	private:
		void showAndOpen(Controller* registeredControllerIn, Controller* otherControllerIn);

		osg::ref_ptr<GeometryManager> m_geometryManager;
		MenuManager* m_menuManager;
		unsigned int m_lastButtonID;
		MenuButton* m_lastHitMenuButton;

		osg::Quat m_triggerRotation;

		osg::Vec3d m_menuPlaneNormal, m_menuPlaneCenterPoint;

		//bool m_waitTriggerStateHasChanged;
};