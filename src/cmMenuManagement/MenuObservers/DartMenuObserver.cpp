// ClayMore - Immersive Mesh Modelling --- Copyright (c) 2014-2017 Philipp Ladwig, Jannik Fiedler, Jan Beutgen
#include "DartMenuObserver.h"
#include "cmGeometryManagement\GeometryManager.h"

#include "cmMenuManagement\MenuManager.h"

#include <cmUtil\Quat2EulerAndBackConverter.h>

#include "cmGeometryManagement/GeometryManager.h"


//This cpp is actually a copy of the TiltMenuObserver.cpp!
DartMenuObserver::DartMenuObserver(MenuName menuNameIn)
{
	m_lastButtonID = INVALID_BUTTON_ID;
	m_triggerRotation.set(0.0, 0.0, 0.0, 0.0);
	m_menuPlaneNormal.set(0.0, 0.0, 0.0);
	m_menuPlaneCenterPoint.set(0.0, 0.0, 0.0);

	m_lastHitMenuButton = nullptr;
	m_menuName = menuNameIn;

	//m_waitTriggerStateHasChanged = false;
}

DartMenuObserver::~DartMenuObserver()
{

}

void DartMenuObserver::showAndOpen(Controller* registeredControllerIn, Controller* otherControllerIn)
{
	DartMenu* dartMenu = static_cast<DartMenu*>(m_menuManager->getMenuWithUniqueName(m_menuName));

	m_triggerRotation = registeredControllerIn->getMatrixTransform()->getMatrix().getRotate();

	dartMenu->setPosition(registeredControllerIn->getMatrixTransform()->getMatrix().getTrans());
	dartMenu->setRotation(m_triggerRotation);
	dartMenu->show();

	registeredControllerIn->triggerHapticEvent(TRIGGER_LONG);

	m_lastButtonID = INVALID_BUTTON_ID;

	m_menuPlaneNormal.set(m_triggerRotation * osg::Vec3f(0.0, 0.0, 1.0));
	m_menuPlaneNormal.normalize();

	m_menuPlaneCenterPoint.set(registeredControllerIn->getPosition() - m_menuPlaneNormal * 1.0);
}

void DartMenuObserver::triggerOpeningProcedureOnceFromExtern(Controller* registeredControllerIn, Controller* otherControllerIn)
{
	showAndOpen(registeredControllerIn, otherControllerIn);
}

bool DartMenuObserver::notify(Controller* registeredControllerIn, Controller* otherControllerIn, BUTTON_STATE registeredButtonState)
{
	DartMenu* dartMenu = static_cast<DartMenu*>(m_menuManager->getMenuWithUniqueName(m_menuName));

	osg::Quat controllerRot = registeredControllerIn->getMatrixTransform()->getMatrix().getRotate();

	//if (!m_waitTriggerStateHasChanged)
	//{
		if (!dartMenu->isVisible())
		{
			showAndOpen(registeredControllerIn, otherControllerIn);
		}
		else
		{
			dartMenu->unhighlightAllButtons();

			osg::Quat rotation = registeredControllerIn->getMatrixTransform()->getMatrix().getRotate();

			osg::Vec3d currentViewVector;
			currentViewVector.set(rotation * osg::Vec3d(0.0, 0.0, -1.0));
			currentViewVector.normalize();

			osg::Vec3d lineStartPoint(registeredControllerIn->getPosition());


			double denom = currentViewVector * m_menuPlaneNormal;

			if (std::abs(denom) > 0.00001)
			{
				double t = (m_menuPlaneCenterPoint - lineStartPoint) * m_menuPlaneNormal / denom;

				osg::Vec3d intersectionPoint = lineStartPoint + currentViewVector * t;

				osg::Vec3d planeVector = m_triggerRotation.inverse() * ((intersectionPoint - m_menuPlaneCenterPoint) / dartMenu->getTargetScaleValue());
				//TODO: The " * dartMenu->getTargetScaleValue()" is just only a quick fix for userTest2 (smaller dart menu). 
				//	The controller position is not perfect translated but sufficient for UserTest
				dartMenu->renderControllerPointer(osg::Vec2f(planeVector.x(), -planeVector.y()) * dartMenu->getTargetScaleValue());
				double len2 = planeVector.length2();
				planeVector.normalize();

				if (len2 > 0.07)
				{
					float angle = 0.0f;
					float sin = asinf(planeVector.x());

					if (planeVector.y() < 0.0f)
						angle = planeVector.x() < 0.0f ? M_PI - 1.0f * sin : M_PI - sin;

					else
						angle = planeVector.x() < 0.0f ? M_PI * 2.0f + sin : sin;

					MenuButton* const hitMenuButton = dartMenu->getMenuButtonAtAngle(angle);

					if (registeredControllerIn->getButtonState(BUTTON_TYPE::REAR_TRIGGER) == RELEASE || registeredControllerIn->getButtonState(BUTTON_TYPE::JOYSTICK) == RELEASE)
					{
						registeredControllerIn->triggerHapticEvent(JOYSTICK_LONG);
						hitMenuButton->trigger();
						dartMenu->hide();
					}

					if (hitMenuButton != m_lastHitMenuButton)
					{
						registeredControllerIn->triggerHapticEvent(JOYSTICK_SHORT);
						m_lastHitMenuButton = hitMenuButton;
					}

					if (m_lastHitMenuButton != nullptr && m_lastHitMenuButton->isActive())
					{
						m_lastHitMenuButton->highlightHover();
					}
				}
			}
			return false;
		}
	/*}
	else
	{
		if (registeredControllerIn->getButtonState(BUTTON_TYPE::REAR_TRIGGER) == PUSH)
		{
			m_waitTriggerStateHasChanged = false;
			showAndOpen(registeredControllerIn, otherControllerIn);
		}
	}*/

	return true;
}