// ClayMore - Immersive Mesh Modelling --- Copyright (c) 2014-2017 Philipp Ladwig, Jannik Fiedler, Jan Beutgen
#pragma once
#include <iostream>

#include <osg/PositionAttitudeTransform>

#include "../../cmInteractionManagement/MenuObserver.h"

#include "../../cmMenuManagement/Menu.h"

class GeometryManager;
class MenuManager;

class imMenuManagement_DLL_import_export UserTest1TiltMenuObserver : public MenuObserver
{
public:
	UserTest1TiltMenuObserver(MenuName menuNameIn);
	~UserTest1TiltMenuObserver();

	inline void setGeometryManager(osg::ref_ptr<GeometryManager> geometryManager) { m_geometryManager = geometryManager; };
	inline void setMenuManager(MenuManager* menuManager) { m_menuManager = menuManager; };

protected:
	bool notify(Controller* registeredControllerIn, Controller* otherControllerIn, BUTTON_STATE registeredButtonState);

private:
	osg::ref_ptr<GeometryManager> m_geometryManager;
	MenuManager* m_menuManager;
	unsigned int m_lastButtonID;
	osg::Quat m_rotationAsButtonPushed;
	unsigned int m_lastSelectedButton;
	MenuName m_menuName;
};