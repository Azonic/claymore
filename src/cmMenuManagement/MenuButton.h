// ClayMore - Immersive Mesh Modelling --- Copyright (c) 2014-2017 Philipp Ladwig, Jannik Fiedler, Jan Beutgen
#pragma once

#include "ConfigDllExportMenuManagement.h"
#include <map>
#include "../cmUtil/ConfigReader.h"

#include <osg/Geode>
#include <osg/PositionAttitudeTransform>

#include <string>

#include <osgText/Text>

#include <osg/Texture>

typedef unsigned int BUTTON_ID;
static BUTTON_ID const INVALID_BUTTON_ID = 0xffffffff;

class imMenuManagement_DLL_import_export MenuButton
{
	public:

		enum HIGHLIGHT_STATE
		{
			NONE,
			HOVER,
			ACTIVE
		};

		MenuButton(BUTTON_ID const buttonID,
			float textSizeIn, 
			unsigned int level,
			std::string const & buttonText,
			osg::Vec4f defaultColor,
			osg::Vec4f highlightColor);

		~MenuButton();

		//virtual void updateButtonAnimation(double menuAnimationStateIn, double deltaTimeIn, float startScaleValueIn, float targetScaleValueIn) = 0;
		void updateButtonAnimation(double menuAnimationStateIn, double deltaTimeIn, float startScaleValueIn, float targetScaleValueIn);

		MenuButton* getSubButton(BUTTON_ID id);
		virtual void createSubButton(std::string const & buttonText) = 0;

		BUTTON_ID trigger();
		void(*triggerCallback) ();

		inline unsigned int getSubLevel() { return m_subLevel; }

		virtual void highlightHover()	= 0;
		virtual void highlightActive()	= 0;
		virtual void unhighlight()		= 0;

		virtual void setButtonUseTexture(std::string textureFilePath) = 0;

		virtual void showSubmenu() = 0;
		virtual void hideSubmenu() = 0;

		virtual void attachToSceneGraph(osg::ref_ptr<osg::Group> parentNodeIn) = 0;
		virtual void detachFromSceneGraph() = 0;

		inline void allowConstantTriggering() { m_canBeTriggeredConstantly = true; }
		inline void disallowConstantTriggering() { m_canBeTriggeredConstantly = false; }

		inline bool canBeTriggeredConstantly() { return m_canBeTriggeredConstantly; }

		inline BUTTON_ID getButtonID() { return m_buttonID; }

		inline bool isActive() { return m_active; }
		inline void markAsActive(bool isActive) { m_active = isActive; }


		inline void setTextSize(float characterSize) { m_textSize = characterSize; }
		inline float getTextSize() { return m_textSize; }
	protected:

		BUTTON_ID m_buttonID;
		BUTTON_ID m_subButtonIDCounter;
		std::map<BUTTON_ID, MenuButton *> m_subButtons;

		osg::ref_ptr<osg::Group>						m_parentNode;
		osg::ref_ptr<osg::Geode>						m_buttonGeode;
		osg::ref_ptr<osg::PositionAttitudeTransform>	m_buttonTransform;
		
		unsigned int m_subLevel; //hierarchy level - "0" is the main menu, "1" the first submenu, etc.

		float m_textSize, m_scaleDelayValue;
		double m_buttonAnimationState;
		bool m_submenuIsActive, m_canBeTriggeredConstantly, m_active;

		HIGHLIGHT_STATE m_highlightState;

		osg::ref_ptr<osg::Geometry> m_geometry;
		osg::ref_ptr<osg::Vec4Array> m_colorArray;
		osg::ref_ptr<osgText::Text> m_buttonText;

		osg::Vec4f m_defaultColor;
		osg::Vec4f m_highlightColor;
		osg::Vec4f m_textColor;
};