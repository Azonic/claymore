// ClayMore - Immersive Mesh Modelling --- Copyright (c) 2014-2017 Philipp Ladwig, Jannik Fiedler, Jan Beutgen
#pragma once

#include "ConfigDllExportMenuManagement.h"
#include "Menu.h"
#include "TiltMenuButton.h"

#include <osg/PositionAttitudeTransform>
#include <math.h>
#include <list>

class ConfigReader;

class imMenuManagement_DLL_import_export TiltMenu : public Menu
{
	// #### CONSTRUCTOR & DESTRUCTOR ###############

public:

	TiltMenu(ConfigReader* configReaderIn,
		MenuName menuNameIn,
		osg::Vec4f defaultColorIn,
		osg::Vec4f highlightColorIn,
		osg::Vec4f menuPointerColorIn,
		CONTROLLER_TYPE controllerType,
		float textSizeIn = 0.1);
	~TiltMenu();

	// #### MEMBER FUNCTIONS ################

	//interface functions by class menu
	void updateAnimations(double deltaTimeIn);
	void hideAllSubMenus();
	void attachToSceneGraph(osg::ref_ptr<osg::Group> parentNodeIn);
	void detachFromSceneGraph();
	void show();
	void hide();

	//void rotateMenuBackOnYAxis(osg::Quat quat);
	void refreshIdleCounter() { m_idleFrameCounter = m_framesBeforeClose; }

	MenuButton* const getMenuButtonAtAngle(float angleIn, unsigned int lastSelectedButtonIn);

	//interface functions by class menu
	BUTTON_ID createButton(std::string const & buttonText);
	void createSubButton(BUTTON_ID parentId, std::string const & buttonText);

	//interface function by class menu
	void unhighlightAllButtons();

	//Must be called after all buttons where added in ordner to align buttons accordingly
	void initializeDrawables(float height = 0.005f);

	void setFramesBeforeClose(int framesBeforeCloseIn) { m_framesBeforeClose = framesBeforeCloseIn; }

	void activateBackground();

	//Set scale value that an active menu will have
	inline void setTargetScaleValue(float targetScaleValue) { m_targetScaleValue = targetScaleValue; }
	//Set scale value that an inactive menu will have
	inline void setStartScaleValue(float startScaleValue) { m_startScaleValue = startScaleValue; }

	void renderControllerPointer(float rotationAngleIn);

private:
	ConfigReader* m_configReader;

	void idleCounterTick();

	//Frames to wait before the menu will be automatically hidden
	//If set to 0, the menu will stay active (default behaviour)
	int m_framesBeforeClose, m_idleFrameCounter;

	double m_animationState;
	float m_targetScaleValue, m_startScaleValue;

	osg::ref_ptr<osg::Geometry> m_backgroundGeometry;
	osg::ref_ptr<osg::PositionAttitudeTransform> m_menuTransformOnlyForRotation;

	osg::ref_ptr<osg::Geometry> m_drawableControllerPointer;
	osg::ref_ptr<osg::Geode> m_geodeControllerPointer;
	osg::ref_ptr<osg::Vec3Array> m_controllerPointerVertices;
	osg::ref_ptr<osg::DrawElementsUInt> m_contrPointerLine;
	osg::ref_ptr<osg::Vec4Array> m_colorArrayControllerPointer;

	std::vector<int> m_lastSelections;
};