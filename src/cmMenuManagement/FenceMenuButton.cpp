// ClayMore - Immersive Mesh Modelling --- Copyright (c) 2014-2017 Philipp Ladwig, Jannik Fiedler, Jan Beutgen
#include "FenceMenuButton.h"

#include <osg/io_utils>


FenceMenuButton::FenceMenuButton(BUTTON_ID id, float textSizeIn, unsigned int level, std::string const & buttonText, osg::Vec4f defaultColorIn, osg::Vec4f highlightColorIn,
	float buttonHeight, float buttonWidth, float buttonSpacerHeight, float buttonTextYOffset) :
	MenuButton(id, textSizeIn, level, "0", defaultColorIn, highlightColorIn),
	//m_defaultColor(defaultColor),
	//m_highlightColor(highlightColor),
	//m_buttonText(nullptr),
	//m_colorArray(nullptr),
	//m_geometry(nullptr),
	//m_texture(nullptr),
	m_buttonHeight(buttonHeight),
	m_buttonWidth(buttonWidth),
	m_buttonSpacerHeight(buttonSpacerHeight),
	m_buttonTextYOffset(buttonTextYOffset)
{
	m_textColor = osg::Vec4f(1.0f, 1.0f, 1.0f, 1.0f);
	m_buttonText = TextCreator::createText(buttonText);

	//m_defaultColor = defaultColorIn;
	//m_highlightColor = highlightColorIn;
}


FenceMenuButton::~FenceMenuButton()
{
}


void FenceMenuButton::attachToSceneGraph(osg::ref_ptr<osg::Group> parentNodeIn)
{
	parentNodeIn->addChild(m_buttonTransform);
	m_buttonTransform->addChild(m_buttonGeode);
	m_parentNode = parentNodeIn;
}

void FenceMenuButton::detachFromSceneGraph()
{
	if (m_buttonTransform != nullptr)
	{
		std::vector<osg::Group*> parents = m_buttonTransform->getParents();

		std::vector<osg::Group*>::iterator paIt = parents.begin();

		while (paIt != parents.end())
		{
			(*paIt)->removeChild(m_buttonTransform);
			paIt++;
		}

		m_buttonTransform->removeChild(m_buttonGeode);
	}
}
//
void FenceMenuButton::showSubmenu()
{
	if (!m_submenuIsActive)
	{
		m_submenuIsActive = true;

		std::map<BUTTON_ID, MenuButton*>::iterator suBuIt = m_subButtons.begin();

		while (suBuIt != m_subButtons.end())
		{
			(*suBuIt).second->attachToSceneGraph(m_parentNode);
			suBuIt++;
		}
	}
}

void FenceMenuButton::hideSubmenu()
{
	if (m_submenuIsActive)
	{
		m_submenuIsActive = false;

		std::map<BUTTON_ID, MenuButton*>::iterator suBuIt = m_subButtons.begin();

		while (suBuIt != m_subButtons.end())
		{
			(*suBuIt).second->unhighlight();
			(*suBuIt).second->detachFromSceneGraph();
			suBuIt++;
		}
	}
}

void FenceMenuButton::createSubButton(std::string const & buttonText)
{
	m_subButtons.insert(std::pair<BUTTON_ID, MenuButton*>(m_subButtonIDCounter, new FenceMenuButton(m_subButtonIDCounter, m_textSize, m_subLevel + 1, buttonText,
		m_defaultColor, m_highlightColor, m_buttonHeight, m_buttonWidth, m_buttonSpacerHeight, m_buttonTextYOffset)));
	m_subButtonIDCounter++;
}

//TODO: A lot of fixed variables, and every menu button have it's own... move to define.h 
// and create in MenuButon a updateButtonAnimation() function ->it's always the same...
//void FenceMenuButton::updateButtonAnimation(double menuAnimationStateIn, double deltaTimeIn, float startScaleValueIn, float targetScaleValueIn)
//{
//	m_buttonText->setColor(m_textColor);
//	switch (m_highlightState)
//	{
//	case ACTIVE:
//
//		if (m_buttonAnimationState < 1.5)
//		{
//			m_buttonAnimationState += deltaTimeIn / 70.0 * pow(1.5 - m_buttonAnimationState, 2) + 0.0001;
//			m_buttonAnimationState = m_buttonAnimationState < 1.5 ? m_buttonAnimationState : 1.5;
//
//			m_colorArray->at(0).set(m_defaultColor.x() + m_highlightColor.x() * m_buttonAnimationState,
//				m_defaultColor.y(),
//				m_defaultColor.z() - m_highlightColor.z() * m_buttonAnimationState, m_defaultColor.w());
//			//std::cout << "ACTIVE" << std::endl;
//			m_colorArray->dirty();
//		}
//
//		break;
//
//	case HOVER:
//		if (m_buttonAnimationState < 1.0)
//		{
//			m_buttonAnimationState += deltaTimeIn / 35.0 * pow(1.0 - m_buttonAnimationState, 2) + 0.0001;
//			m_buttonAnimationState = m_buttonAnimationState < 1.0 ? m_buttonAnimationState : 1.0;
//
//			m_colorArray->at(0).set(m_defaultColor.x() + m_highlightColor.x() * m_buttonAnimationState,
//				m_defaultColor.y() + m_highlightColor.z() * m_buttonAnimationState,
//				m_defaultColor.z() - m_highlightColor.z() * m_buttonAnimationState, m_defaultColor.w());
//			//std::cout << "HOVER" << std::endl;
//			m_colorArray->dirty();
//		}
//		else if (m_buttonAnimationState > 1.0)
//		{
//			m_buttonAnimationState -= deltaTimeIn / 35.0 * pow(1.5 - m_buttonAnimationState, 2) + 0.0001;
//			m_buttonAnimationState = m_buttonAnimationState > 1.0 ? m_buttonAnimationState : 1.0;
//
//			m_colorArray->at(0).set(m_defaultColor.x() + m_highlightColor.x() * m_buttonAnimationState, m_defaultColor.y(), m_defaultColor.z() - m_highlightColor.z() * m_buttonAnimationState, m_defaultColor.w());
//			m_colorArray->dirty();
//		}
//
//		break;
//
//	case NONE:
//	default:
//
//		if (m_buttonAnimationState > 0.0)
//		{
//			m_buttonAnimationState -= deltaTimeIn / 10.0 * pow(m_buttonAnimationState, 2) + 0.0001;
//			m_buttonAnimationState = m_buttonAnimationState > 0.0 ? m_buttonAnimationState : 0.0;
//
//			m_colorArray->at(0).set(m_defaultColor.x() + m_highlightColor.x() * m_buttonAnimationState, m_defaultColor.y(), m_defaultColor.z() - m_highlightColor.z() * m_buttonAnimationState, m_defaultColor.w());
//			//std::cout << "m_defaultColor: " << m_defaultColor << std::endl;
//			//std::cout << "m_highlightColor: " << m_highlightColor << std::endl;
//			//std::cout << "NONE" << std::endl;
//			m_colorArray->dirty();
//		}
//
//		break;
//	}
//
//	//TODO: Redo this... Scale is used because the pivot point is set to -z anyway - scale will cause translation
//	float currentScale = targetScaleValueIn + (m_scaleDelayValue * (1.0f - menuAnimationStateIn)) + menuAnimationStateIn * 0.85f;
//	currentScale = currentScale < 1.0f ? currentScale > targetScaleValueIn ? currentScale : targetScaleValueIn : 1.0f;
//	m_buttonTransform->setScale(osg::Vec3f(currentScale, currentScale, currentScale));
//}

void FenceMenuButton::highlightHover()
{
	m_highlightState = HOVER;
}

void FenceMenuButton::highlightActive()
{
	m_highlightState = ACTIVE;
}

void FenceMenuButton::unhighlight()
{
	if (m_submenuIsActive)
	{
		std::map<BUTTON_ID, MenuButton*>::iterator suBuIt = m_subButtons.begin();

		while (suBuIt != m_subButtons.end())
		{
			(*suBuIt).second->unhighlight();
			suBuIt++;
		}
	}
	else
		m_highlightState = NONE;
}

void FenceMenuButton::setButtonUseTexture(std::string textureFilePath)
{
	//Create texture
	m_texture = new osg::Texture2D;
	m_texture->setDataVariance(osg::Object::DYNAMIC);

	//m_texture->setTextureSize(200, 200);
	//m_texture->setResizeNonPowerOfTwoHint(false);
	//m_texture->setInternalFormat(GL_RGBA);
	//m_texture->setFilter(osg::Texture2D::MIN_FILTER, osg::Texture2D::LINEAR);
	//m_texture->setFilter(osg::Texture2D::MAG_FILTER, osg::Texture2D::LINEAR);
	//m_texture->setWrap(osg::Texture::WRAP_S, osg::Texture::CLAMP_TO_EDGE);
	//m_texture->setWrap(osg::Texture::WRAP_T, osg::Texture::CLAMP_TO_EDGE);

	//Read image
	osg::Image* image = osgDB::readImageFile(textureFilePath);
	m_texture->setImage(image);
}

void FenceMenuButton::initDrawable(int buttonsCount, float height)
{
	if (m_subButtons.size() > 0)
	{
		std::map<BUTTON_ID, MenuButton*>::iterator buIt = m_subButtons.begin();

		while (buIt != m_subButtons.end())
		{
			static_cast<FenceMenuButton*>((*buIt).second)->initDrawable(m_subButtons.size());
			buIt++;
		}
	}

	m_geometry = new osg::Geometry();
	m_geometryForTexture = new osg::Geometry();

	m_geometry->setUseDisplayList(false);
	m_geometryForTexture->setUseDisplayList(false);
	m_geometry->setUseVertexBufferObjects(true);
	m_geometryForTexture->setUseVertexBufferObjects(true);
	m_geometry->setDataVariance(osg::Object::DYNAMIC);
	m_geometryForTexture->setDataVariance(osg::Object::DYNAMIC);

	//Create StateSet
	osg::ref_ptr<osg::StateSet> stateSet = new osg::StateSet();
	stateSet->setMode(GL_LIGHTING, osg::StateAttribute::OFF);
	m_geometry->setStateSet(stateSet);

	osg::ref_ptr<osg::Vec3Array> vertices = new osg::Vec3Array();

	osg::ref_ptr<osg::DrawElementsUInt> drawElements = new osg::DrawElementsUInt(osg::PrimitiveSet::QUADS, 0);



	vertices->push_back(osg::Vec3f(-(m_buttonWidth / 2.0f), m_buttonID * -(m_buttonHeight + m_buttonSpacerHeight), 0.0f));					//left
	vertices->push_back(osg::Vec3f(-(m_buttonWidth / 2.0f), m_buttonID * -(m_buttonHeight + m_buttonSpacerHeight) - m_buttonHeight, 0.0f));	//left
	vertices->push_back(osg::Vec3f(m_buttonWidth / 2.0f, m_buttonID * -(m_buttonHeight + m_buttonSpacerHeight) - m_buttonHeight, 0.0f));	//right
	vertices->push_back(osg::Vec3f(m_buttonWidth / 2.0f, m_buttonID * -(m_buttonHeight + m_buttonSpacerHeight), 0.0f));						//right

	//CCW order
	drawElements->push_back(0);
	drawElements->push_back(1);
	drawElements->push_back(2);
	drawElements->push_back(3);


	m_geometry->addPrimitiveSet(drawElements);

	//m_buttonTransform->setPivotPoint(osg::Vec3f(0.0f, 0.0f, -0.5f));
	//m_buttonTransform->setAttitude(osg::Quat(buttonWidth * m_buttonID, osg::Vec3f(0.0f, -1.0f, 0.0f)));

	m_geometry->setVertexArray(vertices);

	//Define colorArray (with BIND_OVERALL)
	m_colorArray = new osg::Vec4Array();
	m_colorArray->push_back(osg::Vec4f(m_defaultColor.x(), m_defaultColor.y(), m_defaultColor.z(), m_defaultColor.w()));
	//m_colorArray->push_back(osg::Vec4f(1.0f, 1.0f ,1.0f, 1.0f));
	m_geometry->setColorArray(m_colorArray);
	m_geometry->setColorBinding(osg::Geometry::BIND_OVERALL);


	//Disable lighting, enable transparency
	//stateSet->setMode(GL_LIGHTING, osg::StateAttribute::OFF);
	/*stateSet->setMode(GL_BLEND, osg::StateAttribute::ON);
	stateSet->setRenderingHint(osg::StateSet::TRANSPARENT_BIN);
	stateSet->setRenderBinDetails(15, "DepthSortedBin");

	osg::ref_ptr<osg::CullFace> cullFace = new osg::CullFace();
	cullFace->setMode(osg::CullFace::FRONT);
	stateSet->setAttributeAndModes(cullFace, osg::StateAttribute::OFF);*/

	m_buttonGeode->addDrawable(m_geometry);

	if (m_texture == nullptr)
	{
		//Buttontext
		m_buttonText->setAlignment(osgText::TextBase::CENTER_CENTER);
		m_buttonText->setRotation(osg::Quat(0.0f, osg::Vec3f(1.0f, 0.0f, 0.0f), 0.0f, osg::Vec3f(0.0f, 1.0f, 0.0f), 0.0f, osg::Vec3f(0.0f, 0.0f, 1.0f)));
		m_buttonText->setPosition(osg::Vec3f(0.0f, -(float)m_buttonID * (m_buttonHeight + m_buttonSpacerHeight) - m_buttonTextYOffset, 0.001f));
		m_buttonText->setCharacterSize(m_textSize);
		m_buttonText->getOrCreateStateSet()->setRenderingHint(osg::StateSet::TRANSPARENT_BIN);
		m_buttonText->getOrCreateStateSet()->setRenderBinDetails(16, "DepthSortedBin");
		m_buttonText->setColor(osg::Vec4f(1.0f, 1.0f, 1.0f, 1.0f));
		m_buttonTransform->addChild(m_buttonText);
	}
	else
	{
		osg::ref_ptr<osg::Vec3Array> verticesTexture = new osg::Vec3Array();
		float textureOffset = 0.48;
		float floatingOffset = 0.02;

		//Define vertices
		m_buttonHeight += 0.05;
		m_buttonSpacerHeight -= 0.05;
		verticesTexture->push_back(osg::Vec3f(m_buttonHeight / 2, m_buttonID * -(m_buttonHeight + m_buttonSpacerHeight) - m_buttonHeight, 0.02));
		verticesTexture->push_back(osg::Vec3f(-(m_buttonHeight / 2), m_buttonID * -(m_buttonHeight + m_buttonSpacerHeight) - m_buttonHeight, 0.02));
		verticesTexture->push_back(osg::Vec3f(-(m_buttonHeight / 2), m_buttonID * -(m_buttonHeight + m_buttonSpacerHeight), 0.02));
		verticesTexture->push_back(osg::Vec3f(m_buttonHeight / 2, m_buttonID * -(m_buttonHeight + m_buttonSpacerHeight), 0.02));
		m_buttonSpacerHeight += 0.05;
		m_buttonHeight -= 0.05;

		//Define rendering type (QUADS)
		osg::ref_ptr<osg::DrawElementsUInt> drawElementsTexture = new osg::DrawElementsUInt(osg::PrimitiveSet::QUADS, 0);
		drawElementsTexture->push_back(3);
		drawElementsTexture->push_back(2);
		drawElementsTexture->push_back(1);
		drawElementsTexture->push_back(0);
		m_geometryForTexture->addPrimitiveSet(drawElementsTexture);

		//Define textureCoordinates
		osg::Vec2Array* texcoords = new osg::Vec2Array(4);
		(*texcoords)[0].set(0.0f, 1.0f);
		(*texcoords)[1].set(0.0f, 0.0f);
		(*texcoords)[2].set(1.0f, 0.0f);
		(*texcoords)[3].set(1.0f, 1.0f);
		m_geometryForTexture->setTexCoordArray(0, texcoords);

		//Define texture mode, disable lighting, enable transparency
		osg::ref_ptr<osg::StateSet> stateSetTexture = new osg::StateSet();
		stateSetTexture->setMode(GL_LIGHTING, osg::StateAttribute::OFF);
		stateSetTexture->setTextureAttributeAndModes(0, m_texture, osg::StateAttribute::ON);
		stateSetTexture->setMode(GL_BLEND, osg::StateAttribute::ON);
		stateSetTexture->setRenderingHint(osg::StateSet::TRANSPARENT_BIN);
		//stateSetTexture->setRenderBinDetails(5, "DepthSortedBin");
		m_geometryForTexture->setStateSet(stateSetTexture);

		m_geometryForTexture->setVertexArray(verticesTexture);
		m_buttonGeode->addDrawable(m_geometryForTexture);
	}

	//OSGShadow Test // //this are globals from main //TODO: Make global: Config Manager?
	//m_buttonGeode->setNodeMask(0x2);
	//m_buttonTransform->setNodeMask(0x2);
}

void FenceMenuButton::markAsActive(bool isActive)
{
	if (isActive)
	{
		m_textColor = osg::Vec4f(1.0f, 1.0f, 1.0f, 1.0f);
	}
	else
	{
		m_textColor = osg::Vec4f(0.6f, 0.6f, 0.6f, 0.5f);
	}
}