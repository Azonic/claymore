//
//  debugOutput.cpp
//  AutoRigger
//
//  Created by Alexander Pech on 25.02.19.
//  Copyright © 2019 Alexander Pech. All rights reserved.
//
#include "model.h"

void Model::debugOutput(){
    // DEBUG-Ausgabe:
    // Verticies:
    /*
    std::cout<< "VERTICES: " << verticesComplete.size() << std::endl;
    for (int i = 0; i < verticesComplete.size(); i++) {
        std::cout << "V" << i+1 << " " << "Pos.:" << " " << spacer(verticesComplete[i].position[0]) << " " << spacer(verticesComplete[i].position[1]) << " " << spacer(verticesComplete[i].position[2]) << " RGB: " << verticesComplete[i].color[0] << " " << verticesComplete[i].color[1] << " " << verticesComplete[i].color[2] << std::endl;
    }
    std::cout << std::endl << std::endl << "TEXTUR: " << texturesComplete.size() << std::endl;
    for (int i = 0; i < texturesComplete.size(); i++) {
        std::cout << "T" << i+1 << " " << "Pos.:" << " " << spacer(texturesComplete[i].position[0]) << " " << spacer(texturesComplete[i].position[1]) << std::endl;
    }
    std::cout << std::endl << std::endl << "NORMALS: " << normalsComplete.size() << std::endl;
    for (int i = 0; i < normalsComplete.size(); i++) {
        std::cout << "V" << i+1 << " " << "Pos.:" << " " << spacer(normalsComplete[i].direction[0]) << " " << spacer(normalsComplete[i].direction[1]) << " " << spacer(normalsComplete[i].direction[2]) << std::endl;
    }
    std::cout << std::endl << std::endl << "FACES: (V,T,N) " << facesComplete.size() << std::endl;
    for (int i = 0; i < facesComplete.size(); i++) {
        std::cout << "F" << i+1 << " ";
        for (int j = 0; j < facesComplete[i].vcount; j++) {
            std::cout << "\t" << facesComplete[i].point[j][0] << " " << facesComplete[i].point[j][1] << " " << facesComplete[i].point[j][2] ;
        }
        std::cout << std:: endl;
    }
    
    std::cout << std::endl << std::endl << "Skeleton:" << skeletonOutput.size() << std::endl;
    for (int i = 0; i < skeletonOutput.size(); i++) {
        
        std::cout << "J" << i << "\t" << skeletonOutput[i].name << "\t" << "Pos.: " << spacer(skeletonOutput[i].position[0]) << " " << spacer(skeletonOutput[i].position[1]) << " " << spacer(skeletonOutput[i].position[0]) << "\t" << "Parent: J" << skeletonOutput[i].parentJoint << endl;
    }
    
    std::cout << std::endl << std::endl << "Weigthing:" << verticesComplete.size() << std::endl;
    for (int i = 0; i < verticesComplete.size(); i++) {
        
        std::cout << "W" << i << " ";
        for(int j = 0; j < (int)verticesComplete[i].weighting.size();j++){
            std::cout << "\t" << verticesComplete[i].weighting[j];
        }
        std::cout << endl;
    }
    
    std::cout << "Gesamt: " << endl;
    std::cout << "VERTICES: " << verticesComplete.size() << std::endl;
    std::cout << "TEXTUR: " << texturesComplete.size() << std::endl;
    std::cout << "NORMALS: " << normalsComplete.size() << std::endl;
    std::cout << "FACES: (V,T,N) " << facesComplete.size() << std::endl;
    std::cout << "Skeleton:" << skeletonOutput.size() << std::endl;
    std::cout << "Weigthing:" << verticesComplete.size() << std::endl;
    */
   /* std::cout << endl;
    for(int i=0; i<4890;i++)std::cout << "3 ";
    std::cout << endl;*/
}
