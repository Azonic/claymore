//
//  vertex Weighting.cpp
//  AutoRigger
//
//  Created by Alexander Pech on 08.03.19.
//  Copyright © 2019 Alexander Pech. All rights reserved.
//

#include <stdio.h>
#include <vector>
#include <iostream>
#include <math.h>
#include <string>

#include "model.h"

float Model::distance(Vector3 vertexPosition,Vector3 jointPosition){
    return sqrtf(powf(vertexPosition[0]-jointPosition[0],2.f)+powf(vertexPosition[1]-jointPosition[1],2.f)+powf(vertexPosition[2]-jointPosition[2],2.f));
};

Vector3 crossP(Vector3 a, Vector3 b){
    Vector3 ab;
    ab[0] = a[1]*b[2]-a[2]*b[1];
    ab[1] = a[2]*b[0]-a[0]*b[2];
    ab[2] = a[0]*b[1]-a[1]*b[0];
    return ab;
}
float dotP(Vector3 a, Vector3 b){
    return (float)(a[0]*b[0]+a[1]*b[1]+a[2]*b[2]);
}
Vector3 sub(Vector3 a, Vector3 b){
    Vector3 ab;
    ab[0] = a[0]-b[0];
    ab[1] = a[1]-b[1];
    ab[2] = a[2]-b[2];
    return ab;
}
float length(Vector3 a){
    return sqrtf(pow(a[0], 2)+pow(a[1], 2)+pow(a[2], 2));
}


float Model::distanceVertexBone(Vector3 vertex, int nearestJoint, int secoundNearestJoint){
    //Abstand Punkt gerade https://www.mathematik-oberstufe.de/vektoren/a/abstand-punkt-gerade-formel.html
    
    Vector3 p,a,u;
    
    p[0] = vertex[0];//verticesComplete[vertex].position[0];
    p[1] = vertex[1];//verticesComplete[vertex].position[1];
    p[2] = vertex[2];//verticesComplete[vertex].position[2];
    
    a[0] = skeletonOutput[nearestJoint].position[0];
    a[1] = skeletonOutput[nearestJoint].position[1];
    a[2] = skeletonOutput[nearestJoint].position[2];
    
    u[0] = skeletonOutput[secoundNearestJoint].position[0] - skeletonOutput[nearestJoint].position[0];
    u[1] = skeletonOutput[secoundNearestJoint].position[1] - skeletonOutput[nearestJoint].position[1];
    u[2] = skeletonOutput[secoundNearestJoint].position[2] - skeletonOutput[nearestJoint].position[2];
    
    if((nearestJoint == 0 && secoundNearestJoint == 14) || (nearestJoint == 14 && secoundNearestJoint == 0)){
        u[0] = 0;
        u[2] = 0;
    }
    if((nearestJoint == 0 && secoundNearestJoint == 18) || (nearestJoint == 18 && secoundNearestJoint == 0)){
        u[0] = 0;
        u[2] = 0;
    }
    return length(crossP(sub(p, a), u))/length(u);
}


Vector3 Model::projectionVertexJointTriangle(int vertex, int j1Index, int j2Index, int j3Index){
    
    Vector3 v, j1, j2, j3, j1j2, j1j3, n, o;
    
    v[0] = verticesComplete[vertex].position[0];
    v[1] = verticesComplete[vertex].position[1];
    v[2] = verticesComplete[vertex].position[2];
    
    j1[0] = skeletonOutput[j1Index].position[0];
    j1[1] = skeletonOutput[j1Index].position[1];
    j1[2] = skeletonOutput[j1Index].position[2];
    
    j2[0] = skeletonOutput[j2Index].position[0];
    j2[1] = skeletonOutput[j2Index].position[1];
    j2[2] = skeletonOutput[j2Index].position[2];
    
    j3[0] = skeletonOutput[j3Index].position[0];
    j3[1] = skeletonOutput[j3Index].position[1];
    j3[2] = skeletonOutput[j3Index].position[2];
    
    j1j2[0] = j2[0] - j1[0];
    j1j2[1] = j2[1] - j1[1];
    j1j2[2] = j2[2] - j1[2];
    
    j1j3[0] = j3[0] - j1[0];
    j1j3[1] = j3[1] - j1[1];
    j1j3[2] = j3[2] - j1[2];
    
    n = crossP(j1j2, j1j3);
    
    /*
     j = Punkt (hier j1)
     Ebene: n*(x-j)=0
     Gerade: x=v+t*n
     
     Koordinatenform: j=(j1/j2/j3)
     n1 * (v1+t*n1 - j1) + n2 * (v2+t*n2 - j2) + n3 * (v3+t*n2 - j2) = 0
     n1 * (v1 - j1)      + n2 * (v2 - j2)      + n3 * (v3 - j3)      = -t * n1^2 -t * n2^2 -t * n3^2
     (n1 * (v1 - j1)      + n2 * (v2 - j2)      + n3 * (v3 - j3))/(-n1^2-n2^2-n^3)      = t
     */
    
    float t = (float)((n[0]*(v[0]-j1[0])+n[1]*(v[1]-j1[1])+n[2]*(v[2]-j1[2]))/(-pow(n[0], 2)-pow(n[1], 2)-pow(n[2], 2)));
    
    
    o[0] = v[0] + t*n[0];
    o[1] = v[1] + t*n[1];
    o[2] = v[2] + t*n[2];
    
    return o;
    
}

bool pointInPolygon(Vector3 point, Vector3 a, Vector3 b, Vector3 c){
    
    float angleSum = 0;
    const float pi = 3.14159265;
    
    Vector3 pa, pb, pc, ab, bc, ca;
    
    pa[0] = a[0] - point[0];
    pa[1] = a[1] - point[1];
    pa[2] = a[2] - point[2];
    
    pb[0] = b[0] - point[0];
    pb[1] = b[1] - point[1];
    pb[2] = b[2] - point[2];
    
    pc[0] = c[0] - point[0];
    pc[1] = c[1] - point[1];
    pc[2] = c[2] - point[2];
   /*
    ab[0] = b[0] - a[0];
    ab[1] = b[1] - a[1];
    ab[2] = b[2] - a[2];
    
    bc[0] = c[0] - b[0];
    bc[1] = c[1] - b[1];
    bc[2] = c[2] - b[2];
    
    ca[0] = a[0] - c[0];
    ca[1] = a[1] - c[1];
    ca[2] = a[2] - c[2];
    
    float angle[6];
    
    angle[0] = acos(dotP(pa,ab)/(length(pa)*length(ab)))*180/pi;
    angle[1] = acos(dotP(pa,ca)/(length(pa)*length(ca)))*180/pi;
    
    angle[2] = acos(dotP(pb,ab)/(length(pb)*length(ab)))*180/pi;
    angle[3] = acos(dotP(pb,bc)/(length(pc)*length(bc)))*180/pi;
    
    angle[4] = acos(dotP(pc,bc)/(length(pc)*length(bc)))*180/pi;
    angle[5] = acos(dotP(pc,ca)/(length(pc)*length(ca)))*180/pi;
    
    for(int h = 0; h < 6; h++){
        if(angle[h] > 90)
            angle[h] = 180 - angle[h];
        else if (isnan(angle[h])) angle[h] = 180/pi;
        
        angleSum += angle[h];
    }*/
    float angle[3];
    
    angle[0] = acos(dotP(pa,pb)/(length(pa)*length(pb)))*180/pi;
    angle[1] = acos(dotP(pb,pc)/(length(pb)*length(pc)))*180/pi;
    
    angle[2] = acos(dotP(pc,pa)/(length(pc)*length(pa)))*180/pi;
    
    for(int h = 0; h < 3; h++){

        
        angleSum += angle[h];
    }
    
    std::cout << "AngleSum: " << angleSum << " " << angle[0] << " " << angle[1] << " " << angle[2] << " "/* << angle[3] << " " << angle[4] << " " << angle[4] */<< std::endl;
    
    if(angleSum > 359) return true;
    else return false;
}




/*
float Model::jointDistance(int start, int end){
    
    float r = 0;
    std::string sG = "XX";
    std::string eG = "XX";
    
    if(0 <= start && start <= 5){
        sG = "K";
    }
    else if(6 <= start && start <= 9){
        sG = "AL";
    }
    else if(10 <= start && start <= 13){
        sG = "AR";
    }
    else if(14 <= start && start <= 17){
        sG = "BL";
    }
    else if(18 <= start && start <= 21){
        sG = "BR";
    }
    
    if(0 <= end && end <= 5){
        eG = "K";
    }
    else if(6 <= end && end <= 9){
        eG = "AL";
    }
    else if(10 <= end && end <= 13){
        eG = "AR";
    }
    else if(14 <= end && end <= 17){
        eG = "BL";
    }
    else if(18 <= end && end <= 21){
        eG = "BR";
    }
    
    
    if(sG[0] == 'K'){
        switch (eG[0]) {
            case 'K':
                r = distance(skeletonOutput[start].position, skeletonOutput[end].position);
                break;
            case 'A':
                r = distance(skeletonOutput[start].position, skeletonOutput[3].position) + distance(skeletonOutput[3].position, skeletonOutput[end].position);
                break;
            case 'B':
                r = distance(skeletonOutput[start].position, skeletonOutput[0].position) + distance(skeletonOutput[0].position, skeletonOutput[end].position);
                break;
            default:
                std::cout << "Error sG eG" ;
                break;
        }
    }
    if(sG[0] == 'A'){
        switch (eG[0]) {
            case 'K':
                r = distance(skeletonOutput[start].position, skeletonOutput[3].position) + distance(skeletonOutput[3].position, skeletonOutput[end].position);
                break;
            case 'A':
                r = distance(skeletonOutput[start].position, skeletonOutput[3].position) + distance(skeletonOutput[3].position, skeletonOutput[end].position);
                break;
            case 'B':
                r = distance(skeletonOutput[start].position, skeletonOutput[3].position) + distance(skeletonOutput[3].position, skeletonOutput[0].position) + distance(skeletonOutput[0].position, skeletonOutput[end].position);
                break;
            default:
                std::cout << "Error sG eG" ;
                break;
        }
    }
    if(sG[0] == 'B'){
        switch (eG[0]) {
            case 'K':
                r = distance(skeletonOutput[start].position, skeletonOutput[0].position) + distance(skeletonOutput[0].position, skeletonOutput[end].position);
                break;
            case 'A':
                r = distance(skeletonOutput[start].position, skeletonOutput[0].position) + distance(skeletonOutput[0].position, skeletonOutput[3].position) + distance(skeletonOutput[3].position, skeletonOutput[end].position);
                break;
            case 'B':
                r = distance(skeletonOutput[start].position, skeletonOutput[0].position) + distance(skeletonOutput[0].position, skeletonOutput[end].position);
                break;
            default:
                std::cout << "Error sG eG" ;
                break;
        }
    }
    
    return r;
}
*/

/*
//https://ieeexplore.ieee.org.ezp.hs-duesseldorf.de/document/5759655 Funktiniert nicht :(
vector<float> Model::computeWeight(int vertexIndex, vector<int> joints){
    
    vector<float> w;
    int N = (int)joints.size();
    
    //std::cout << "k-size " << joints.size();
    
    for (int j = 0; j < N; j++) { //für jeden Joint durchlaufen
        
        float zaehler = 0;
        for (int k = 0; k < N; k++) {
            if(k==j) continue;
            float a = distance(verticesComplete[vertexIndex].position, skeletonOutput[joints[k]].position);
            //std::cout << "a " << a << std::endl;
            float b = distance(verticesComplete[vertexIndex].position, skeletonOutput[joints[j]].position);
            //std::cout << "b " << b << std::endl;
            zaehler += a-b;//abs(a-b);
        }
        float nenner = 0;
        for (int k = 0; k < N; k++){
            if(k==j) continue;
            nenner += distance(verticesComplete[vertexIndex].position, skeletonOutput[joints[k]].position);
        }
        nenner = (N - 1) * nenner;
        
        w.resize(w.size() + 1);
        w.back() = zaehler/nenner;
        
    }
    
    
    return w;
}*/


void Model::vertexWeight(){
    float jointVertexDistance[22];
    int nearest = 0;
    int secoundNearest = 0;
    vector<int> blends;
    
    Vector3 pseudoJoint;
    pseudoJoint[0] = skeletonOutput[0].position[0];
    pseudoJoint[1] = (skeletonOutput[14].position[1]+skeletonOutput[18].position[1])/2;
    pseudoJoint[2] = skeletonOutput[0].position[2];
    
    
    for (int i = 0; i < (int)verticesComplete.size(); i++) {
    
        nearest = 0;
        for(int j = 0; j < (int)skeletonOutput.size();j++){
            jointVertexDistance[j] = distance(verticesComplete[i].position, skeletonOutput[j].position);
            
            if(jointVertexDistance[nearest] >= jointVertexDistance[j]){
                nearest = j;
            }
        }
        
        //Bereich unter den Armen korregieren
        
       if (nearest == 6 && verticesComplete[i].position[1] < skeletonOutput[3].position[1]) {
            if(jointVertexDistance[8] > jointVertexDistance[3])
                nearest = 3;
           // std::cout << "Korregiert(6)" << std::endl;
            //verticesComplete[i].color[1] = 255;
        }
        if (nearest == 7 && verticesComplete[i].position[1] < skeletonOutput[3].position[1]) {
            if(jointVertexDistance[8] > jointVertexDistance[3])
            nearest = 3;
           // std::cout << "Korregiert(7)" << std::endl;
           // verticesComplete[i].color[0] = 255;
        }
        if (nearest == 10 && verticesComplete[i].position[1] < skeletonOutput[3].position[1]) {
            if(jointVertexDistance[12] > jointVertexDistance[3] && verticesComplete[i].position[0] < skeletonOutput[10].position[0])
                nearest = 3;
           // std::cout << "Korregiert(10)" << std::endl;
           // verticesComplete[i].color[1] = 255;
        }
        if (nearest == 11 && verticesComplete[i].position[1] < skeletonOutput[3].position[1]) {
            if(jointVertexDistance[12] > jointVertexDistance[3])
                nearest = 3;
            //std::cout << "Korregiert(11)" << std::endl;
            //verticesComplete[i].color[0] = 255;
        }
        
        if((nearest == 14 && verticesComplete[i].position[1] > pseudoJoint[1]) || (nearest == 18 && verticesComplete[i].position[1] > pseudoJoint[1])) {
            if(jointVertexDistance[0] < distance(verticesComplete[i].position, pseudoJoint)){
                nearest = 0;
              //  std::cout << "PseudoJ: " << i <<  std::endl;
                //verticesComplete[i].color[2] = 255;
            }
        }
        
        switch (nearest) {
            case 0:
                //secoundNearest = 1;
                if(verticesComplete[i].position[1] > skeletonOutput[0].position[1])
                    secoundNearest = 1;
                else if(jointVertexDistance[14] <= jointVertexDistance[18])
                    secoundNearest = 14;
                else
                    secoundNearest = 18;
                break;
            case 1:
                if(verticesComplete[i].position[1] <= skeletonOutput[nearest].position[1])/*{
                    if(jointVertexDistance[14] <= jointVertexDistance[18])
                        secoundNearest = 14;
                    else
                        secoundNearest = 18;
                }*/
                    secoundNearest = nearest-1;
                else
                    secoundNearest = nearest+1;
                break;
            case 2:
                if(verticesComplete[i].position[1] <= skeletonOutput[nearest].position[1])
                    secoundNearest = nearest-1;
                else
                    secoundNearest = nearest+1;
                break;
            case 3:
                if(verticesComplete[i].position[1] <= skeletonOutput[nearest].position[1])
                    secoundNearest = nearest-1;
                else
                    secoundNearest = nearest+1;
                break;
            case 4:
                if(verticesComplete[i].position[1] <= skeletonOutput[nearest].position[1]){
                     secoundNearest = nearest-1;
                
                    if(jointVertexDistance[6] < jointVertexDistance[3]) secoundNearest = 6;
                    if(jointVertexDistance[10] < jointVertexDistance[3]) secoundNearest = 10;
                
                }
                else
                    secoundNearest = nearest+1;
                break;
            case 5:
                secoundNearest = 4;
                break;
            case 6:
                //if(jointVertexDistance[3] < jointVertexDistance[nearest + 1])
                if(verticesComplete[i].position[0] > skeletonOutput[nearest].position[0]){ //gehe zu größerem x wert
                    if (skeletonOutput[nearest+1].position[0] > skeletonOutput[3].position[0]) {
                        secoundNearest = nearest + 1;
                    }
                    else
                        secoundNearest = 3;
                }
                else{
                    if (skeletonOutput[nearest+1].position[0] > skeletonOutput[3].position[0]) {
                        secoundNearest = 3;
                    }
                    else
                        secoundNearest = nearest + 1;
                }
                break;
            case 7:
                //if(jointVertexDistance[3] < jointVertexDistance[nearest + 1])
                if(verticesComplete[i].position[0] > skeletonOutput[nearest].position[0]){ //gehe zu größerem x wert
                    if (skeletonOutput[nearest+1].position[0] > skeletonOutput[nearest-1].position[0]) {
                        secoundNearest = nearest + 1;
                    }
                    else
                        secoundNearest = nearest - 1;
                }
                else{
                    if (skeletonOutput[nearest+1].position[0] > skeletonOutput[nearest-1].position[0]) {
                        secoundNearest = nearest - 1;
                    }
                    else
                        secoundNearest = nearest + 1;
                }
                break;
            case 8:
                //if(jointVertexDistance[3] < jointVertexDistance[nearest + 1])
                if(verticesComplete[i].position[0] > skeletonOutput[nearest].position[0]){ //gehe zu größerem x wert
                    if (skeletonOutput[nearest+1].position[0] > skeletonOutput[nearest-1].position[0]) {
                        secoundNearest = nearest + 1;
                    }
                    else
                        secoundNearest = nearest - 1;
                }
                else{
                    if (skeletonOutput[nearest+1].position[0] > skeletonOutput[nearest-1].position[0]) {
                        secoundNearest = nearest - 1;
                    }
                    else
                        secoundNearest = nearest + 1;
                }
                break;
            case 9:
                secoundNearest = 8;
                break;
            case 10:
                if(verticesComplete[i].position[0] < skeletonOutput[nearest].position[0]){ //gehe zu größerem x wert
                    if (skeletonOutput[nearest+1].position[0] < skeletonOutput[3].position[0]) {
                        secoundNearest = nearest + 1;
                    }
                    else
                        secoundNearest = 3;
                }
                else{
                    if (skeletonOutput[nearest+1].position[0] < skeletonOutput[3].position[0]) {
                        secoundNearest = 3;
                    }
                    else
                        secoundNearest = nearest + 1;
                }
                break;
            case 11:
                //if(jointVertexDistance[3] < jointVertexDistance[nearest + 1])
                if(verticesComplete[i].position[0] < skeletonOutput[nearest].position[0]){ //gehe zu größerem x wert
                    if (skeletonOutput[nearest+1].position[0] < skeletonOutput[nearest-1].position[0]) {
                        secoundNearest = nearest + 1;
                    }
                    else
                        secoundNearest = nearest - 1;
                }
                else{
                    if (skeletonOutput[nearest+1].position[0] < skeletonOutput[nearest-1].position[0]) {
                        secoundNearest = nearest - 1;
                    }
                    else
                        secoundNearest = nearest + 1;
                }
                break;
            case 12:
                //if(jointVertexDistance[3] < jointVertexDistance[nearest + 1])
                if(verticesComplete[i].position[0] < skeletonOutput[nearest].position[0]){ //gehe zu größerem x wert
                    if (skeletonOutput[nearest+1].position[0] < skeletonOutput[nearest-1].position[0]) {
                        secoundNearest = nearest + 1;
                    }
                    else
                        secoundNearest = nearest - 1;
                }
                else{
                    if (skeletonOutput[nearest+1].position[0] < skeletonOutput[nearest-1].position[0]) {
                        secoundNearest = nearest - 1;
                    }
                    else
                        secoundNearest = nearest + 1;
                }
                break;
            case 13:
                secoundNearest = 12;
                break;
            case 14:
                if(verticesComplete[i].position[1] < skeletonOutput[14].position[1])
                    secoundNearest = 15;
                else
                    secoundNearest = 0;
                break;
            case 15:
                if(verticesComplete[i].position[1] < skeletonOutput[15].position[1])
                    secoundNearest = 16;
                else
                    secoundNearest = 14;
                    break;
            case 16:
                if(verticesComplete[i].position[1] > skeletonOutput[16].position[1] + distance(skeletonOutput[16].position, skeletonOutput[17].position))
                    secoundNearest = 15;
                else{
                    if(jointVertexDistance[nearest - 1] < jointVertexDistance[nearest + 1])
                        secoundNearest = nearest-1;
                    else
                        secoundNearest = nearest + 1;
                }
                break;
            case 17:
                secoundNearest = 16;
                break;
            case 18:
                if(verticesComplete[i].position[1] <= skeletonOutput[nearest].position[1])
                    secoundNearest = 19;
                else
                    secoundNearest = 0;
                break;
            case 19:
                if(verticesComplete[i].position[1] <= skeletonOutput[nearest].position[1])
                    secoundNearest = 20;
                else
                    secoundNearest = 18;
                break;
            case 20:
                if(verticesComplete[i].position[1] <= skeletonOutput[nearest].position[1])
                    secoundNearest = 21;
                else
                    secoundNearest = 19;
                break;
            case 21:
                secoundNearest = 20;
                break;
                
            default:
                if(jointVertexDistance[nearest - 1] < jointVertexDistance[nearest + 1])
                    secoundNearest = nearest-1;
                else
                    secoundNearest = nearest + 1;
                
                break;
        }
        
        float vertexBoneDis = distanceVertexBone(verticesComplete[i].position, nearest, secoundNearest);
        float JJDis = distance(skeletonOutput[nearest].position,skeletonOutput[secoundNearest].position);
      /*
        if((nearest == 0 && secoundNearest == 14) or (nearest == 14 && secoundNearest == 0)){
            JJDis = abs(skeletonOutput[nearest].position[1]-skeletonOutput[secoundNearest].position[1]);
        }
        if((nearest == 0 && secoundNearest == 18) or (nearest == 18 && secoundNearest == 0)){
            JJDis = abs(skeletonOutput[nearest].position[1]-skeletonOutput[secoundNearest].position[1]);
        }*/
        
       /* std::cout << i << " V: " << verticesComplete[i].position[0] << " " << verticesComplete[i].position[1] << " " << verticesComplete[i].position[2] << " " << "J1 " << skeletonOutput[nearest].position[0] << " " << skeletonOutput[nearest].position[1] << " " << skeletonOutput[nearest].position[2] << " " << "J2 " << skeletonOutput[secoundNearest].position[0] << " " << skeletonOutput[secoundNearest].position[1] << " " << skeletonOutput[secoundNearest].position[2] << "VB: " << vertexBoneDis << " JJ: " << JJDis << " Near: " << nearest << " sec: " << secoundNearest << " jVD1: " << jointVertexDistance[nearest]<< " jVD2: " << jointVertexDistance[secoundNearest] << std::endl;
        */
        
        for(int l = 0; l < (int)verticesComplete[i].weighting.size();l++){
            
            verticesComplete[i].weighting[l] = 0;
            
            if(nearest == l){
                
                verticesComplete[i].weighting[l] = 1 - sqrtf(pow(jointVertexDistance[nearest],2)-pow(vertexBoneDis, 2))/JJDis;
               
                if(isnan(verticesComplete[i].weighting[l])) {
                    std::cout << "isNaN: " << i <<std::endl;
                    verticesComplete[i].weighting[l] = 1;
                }
                
                /* if(nearest == 5 || nearest == 9 || nearest == 13 || nearest == 17 || nearest == 21 )
                 verticesComplete[i].weighting[l] = 1; */
                if(nearest == 5 && verticesComplete[i].position[1] > skeletonOutput[5].position[1])
                    verticesComplete[i].weighting[l] = 1;
                
            }
            else if(secoundNearest == l){
              
                verticesComplete[i].weighting[l] = 1 - sqrtf(pow(jointVertexDistance[secoundNearest],2)-pow(vertexBoneDis, 2))/JJDis;
                
                if((nearest == 0 && l == 14) || (nearest == 0 && l == 18)){
                    verticesComplete[i].weighting[l] = 1 - sqrtf(pow(distance(verticesComplete[i].position, pseudoJoint),2)-pow(vertexBoneDis, 2))/JJDis;
                }
               
                /*
                if(nearest == 5 || nearest == 9 || nearest == 13 || nearest == 17 || nearest == 21 )
                    verticesComplete[i].weighting[l] = 0;*/
                
                //if(verticesComplete[i].weighting[l] < 0) verticesComplete[i].weighting[l] = 0;
                
            }
        }
        
        if((nearest == 0 && secoundNearest == 14) || (nearest == 14 && secoundNearest == 0) || (nearest == 0 && secoundNearest == 18) || (nearest == 18 && secoundNearest == 0) || (nearest == 14 && secoundNearest == 18) || (nearest == 18 && secoundNearest == 14)){
            
            Vector3 p = projectionVertexJointTriangle(i, 0, 14, 18);
            if(pointInPolygon(p, skeletonOutput[0].position, skeletonOutput[14].position, skeletonOutput[18].position))
            {
                verticesComplete[i].weighting[0] = distanceVertexBone(p, 14, 18)/distanceVertexBone(skeletonOutput[0].position, 14, 18);
                verticesComplete[i].weighting[14] = distanceVertexBone(p, 0, 18)/distanceVertexBone(skeletonOutput[14].position, 0, 18);
                verticesComplete[i].weighting[18] = distanceVertexBone(p, 0, 14)/distanceVertexBone(skeletonOutput[18].position, 0, 14);
               // verticesComplete[i].color[1] = 255;
            }
        }
        if((nearest == 6 && secoundNearest !=7)/* || (nearest == 6 && secoundNearest == 3) || (nearest == 0 && secoundNearest == 18) || (nearest == 18 && secoundNearest == 0) || (nearest == 14 && secoundNearest == 18) || (nearest == 18 && secoundNearest == 14)*/){
            
            Vector3 p = projectionVertexJointTriangle(i, 4, 3, 6);
            if(pointInPolygon(p, skeletonOutput[4].position, skeletonOutput[3].position, skeletonOutput[6].position))
            {
                verticesComplete[i].weighting[4] = distanceVertexBone(p, 3, 6)/distanceVertexBone(skeletonOutput[2].position, 3, 6);
                verticesComplete[i].weighting[3] = distanceVertexBone(p, 4, 6)/distanceVertexBone(skeletonOutput[3].position, 4, 6);
                verticesComplete[i].weighting[6] = distanceVertexBone(p, 4, 3)/distanceVertexBone(skeletonOutput[6].position, 4, 3);
                //verticesComplete[i].color[1] = 255;
            }
        }/*
        if(nearest == 7 && secoundNearest !=8){
            
            Vector3 p = projectionVertexJointTriangle(i, 4, 3, 7);
            if(pointInPolygon(p, skeletonOutput[4].position, skeletonOutput[3].position, skeletonOutput[7].position))
            {
                verticesComplete[i].weighting[4] = distanceVertexBone(p, 3, 7)/distanceVertexBone(skeletonOutput[2].position, 3, 7);
                verticesComplete[i].weighting[3] = distanceVertexBone(p, 4, 7)/distanceVertexBone(skeletonOutput[3].position, 4, 7);
                verticesComplete[i].weighting[7] = distanceVertexBone(p, 4, 3)/distanceVertexBone(skeletonOutput[6].position, 4, 3);
                verticesComplete[i].color[2] = 255;
            }
        }*/
        if((nearest == 10 && secoundNearest != 11)/* || (nearest == 6 && secoundNearest == 3) || (nearest == 0 && secoundNearest == 18) || (nearest == 18 && secoundNearest == 0) || (nearest == 14 && secoundNearest == 18) || (nearest == 18 && secoundNearest == 14)*/){
            
            Vector3 p = projectionVertexJointTriangle(i, 4, 3, 10);
            if(pointInPolygon(p, skeletonOutput[4].position, skeletonOutput[3].position, skeletonOutput[10].position))
            {
                verticesComplete[i].weighting[4] = distanceVertexBone(p, 3, 10)/distanceVertexBone(skeletonOutput[2].position, 3, 10);
                verticesComplete[i].weighting[3] = distanceVertexBone(p, 4, 10)/distanceVertexBone(skeletonOutput[3].position, 4, 10);
                verticesComplete[i].weighting[10] = distanceVertexBone(p, 4, 3)/distanceVertexBone(skeletonOutput[6].position, 4, 3);
                //verticesComplete[i].color[2] = 255;
            }
        }
        
        
     /*
      //Auf 1 normieren
        float sumWeights = 0;
        for(int l = 0; l < verticesComplete[i].weighting.size();l++){
            sumWeights += verticesComplete[i].weighting[l];
        }
        if(sumWeights<=0) verticesComplete[i].color[0] = 255;
        
       for(int l = 0; l < verticesComplete[i].weighting.size();l++){
            verticesComplete[i].weighting[l] = verticesComplete[i].weighting[l]/sumWeights;
        }*/
       //std::cout << std::endl;
        blends.clear();
    
    }
}
