//
//  stringProcessing.hpp
//  AutoRigger
//
//  Created by Alexander Pech on 10.03.19.
//  Copyright © 2019 Alexander Pech. All rights reserved.
//

#ifndef stringProcessing_h
#define stringProcessing_h

#include <stdio.h>

std::vector<std::string> split(std::string stringToBeSplitted, std::string delimeter);

std::string replace(std::string base, const std::string from, const std::string to);

std::string spacer(float i);

#endif /* stringProcessing_h */
