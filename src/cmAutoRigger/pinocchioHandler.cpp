//
//  main.cpp
//  AutoRigger
//  Created by Alexander Pech on 20.02.19.
//  Copyright © 2019 Alexander Pech. All rights reserved.
//

#include <algorithm>
//#include <ext/hash_map>
//#include <ext/hash_set>
#include <functional>
#include <fstream>
#include <iostream>
#include <istream>
#include <map>
#include <math.h>
#include <numeric>
#include <queue>
#include <set>
#include <sstream>
#include <string>
#include <vector>


#include "model.h"

#include "skeleton.h"
#include "mesh.h"
/*
#include "../revisedPinocchio/mesh.cpp"
#include "../revisedPinocchio/skeleton.cpp"
#include "../revisedPinocchio/graphutils.cpp"

#include "../revisedPinocchio/indexer.cpp"
#include "../revisedPinocchio/intersector.cpp"
#include "../revisedPinocchio/attachment.cpp"
#include "../revisedPinocchio/lsqSolver.cpp"
#include "../revisedPinocchio/discretization.cpp"
#include "../revisedPinocchio/embedding.cpp"
#include "../revisedPinocchio/refinement.cpp"
#include "../revisedPinocchio/pinocchioApi.hpp"
#include "../revisedPinocchio/pinocchioApi.cpp"
*/


PinocchioOutput Model::pinocchioHandler(Skeleton &given) { //int argc, const char * argv[]
  
    
    skeletonInput = given;
    
    centerMesh();
    
    //######
    Mesh m;
    for(int i = 0; i < verticesComplete.size();i++){
        m.vertices.resize(m.vertices.size() + 1);
        m.vertices.back().pos = Vector3(verticesComplete[i].position[0], verticesComplete[i].position[1], verticesComplete[i].position[2]);
    }
    for(int q = 0; q < facesComplete.size();q++){
        int a[16];
        for(int i = 0; i < facesComplete[q].vcount; ++i){
            std::stringstream s;
            s << facesComplete[q].point[i][0];
            sscanf(s.str().c_str(), "%d", a + i);
        }
        for(int j = 2; j < facesComplete[q].vcount; ++j) {
            int first = (int) m.edges.size();
            m.edges.resize(m.edges.size() + 3);
            m.edges[first].vertex = a[0] - 1;
            m.edges[first + 1].vertex = a[j - 1] - 1;
            m.edges[first + 2].vertex = a[j] - 1;
        }
    }
    //reconstruct the rest of the information
     int verts = (int) m.vertices.size();
     
     for(int i = 0; i < (int)m.edges.size(); ++i) { //make sure all vertex indices are valid
         if(m.edges[i].vertex < 0 || m.edges[i].vertex >= verts) {
             std::cout << "Error: invalid vertex index " << m.edges[i].vertex << endl;
             //OUT;
         }
     }
     
     m.fixDupFaces();
     
     m.computeTopology();
     
     if(m.integrityCheck())
     std::cout << "Successfully read " << /*file <<*/ ": " << m.vertices.size() << " vertices, " << m.edges.size() << " edges" << endl;
     else
     std::cout << "Somehow read " << /*file <<*/ ": " << m.vertices.size() << " vertices, " << m.edges.size() << " edges" << endl;
     
     m.computeVertexNormals();
    
    //#######
    
    //Mesh
    //Mesh m(file);
    if(m.vertices.size() == 0) {
        cout << "Error reading file.  Aborting." << endl;
        exit(0);
    }
    m.normalizeBoundingBox();
    m.computeVertexNormals();
    
    given.scale(0.7);
    
    //AutoRig
    PinocchioOutput o;
    o = autorig(given, m);
    
    if(o.embedding.size() == 0) {
        cout << "Error embedding" << endl;
        exit(0);
    }
    //output skeleton embedding
    int i;
    for(i = 0; i < (int)o.embedding.size(); ++i)
        o.embedding[i] = (o.embedding[i] - m.toAdd) / m.scale;
    //ofstream os("/Users/alexander/Documents/AutoRigger/skeleton.out");
    for(i = 0; i < (int)o.embedding.size(); ++i) {
      //  os << i << " " << o.embedding[i][0] << " " << o.embedding[i][1] << " " << o.embedding[i][2] << " " << given.fPrev()[i] << endl;
      
        skeletonOutput.resize(skeletonOutput.size()+1);
        skeletonOutput.back().name = skeletonInput.getNameForJoint(i);
        skeletonOutput.back().position[0] = o.embedding[i][0];
        skeletonOutput.back().position[1] = o.embedding[i][1];
        skeletonOutput.back().position[2] = o.embedding[i][2];
        skeletonOutput.back().parentJoint = given.fPrev()[i];
    }
    
    //output attachment
    //std::ofstream astrm("/Users/alexander/Documents/AutoRigger/attachment.out");
    for(i = 0; i < (int)m.vertices.size(); ++i) {
        Vector<double, -1> v = o.attachment->getWeights(i);
        
       verticesComplete[i].weighting.resize(verticesComplete[i].weighting.size()+1);
        verticesComplete[i].weighting.back() = 0;
        
        
        for(int j = 0; j < v.size(); ++j) {
            double d = floor(0.5 + v[j] * 10000.) / 10000.;
            //astrm << d << " ";
            
            verticesComplete[i].weighting.resize(verticesComplete[i].weighting.size()+1);
            
            if(d < 0) {d = 0;}
            
            verticesComplete[i].weighting.back() = d;
        }
       // astrm << endl;
    }
    
    delete o.attachment;
    
    return o;
}

