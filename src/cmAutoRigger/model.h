//
//  modelProcessor.hpp
//  AutoRigger
//
//  Created by Alexander Pech on 25.02.19.
//  Copyright © 2019 Alexander Pech. All rights reserved.
//

#pragma once
#include "ConfigDllExportAutoRigger.h"

#include <stdio.h>
#include <vector>
#include <string>

#include "skeleton.h"
#include "pinocchioApi.h"

#include "stringProcessing.h"

//const int anzJoints = 17;
const int maxVertexPerFace = 4;

//Datenstruktur;
struct Vertex {
	Vector3 position; //x y z
	//float normal[3];
	//float textur[2];
	float color[3]; //RGB
	vector<double> weighting;
};
struct Normal {
	float direction[3];
};
struct Textur {
	float position[2];
};
struct Face {
	short vcount;     // 3 oder 4
	int point[maxVertexPerFace][3]; //Position/Textchord/Normale
};

struct Joints {
	std::string name;
	Vector3 position;
	int parentJoint;
};

class cmAutoRigger_DLL_import_export Model {
private:
	std::vector<Vertex> verticesComplete;
	std::vector<Face>   facesComplete;
	std::vector<Normal> normalsComplete;
	std::vector<Textur> texturesComplete;

	std::vector<Joints> skeletonOutput;

public:
	Skeleton skeletonInput;

public:
	Model() {
		skeletonInput = HumanSkeletonUnity();
	};
	Model(std::string s)
	{
		//this->file=s;
		objImporter(s);
		skeletonInput = HumanSkeletonUnity();
	}
	int modelProcessor();
	void objImporter(std::string &file);
	PinocchioOutput pinocchioHandler(Skeleton &given);
	void vertexWeight();
	void debugOutput();
	void daeExporter(string);


	void vertexImport(float x, float y, float z, float r, float g, float b) {
		verticesComplete.resize(verticesComplete.size() + 1);
		verticesComplete.back().position[0] = x;
		verticesComplete.back().position[1] = y;
		verticesComplete.back().position[2] = z;
		verticesComplete.back().color[0] = r;
		verticesComplete.back().color[1] = g;
		verticesComplete.back().color[2] = b;
	}
	void texturImport(float u, float v) {
		texturesComplete.resize(texturesComplete.size() + 1);
		texturesComplete.back().position[0] = u;
		texturesComplete.back().position[1] = v;
	}
	void normalImport(float x, float y, float z) {
		normalsComplete.resize(normalsComplete.size() + 1);
		normalsComplete.back().direction[0] = x;
		normalsComplete.back().direction[1] = y;
		normalsComplete.back().direction[2] = z;
	}
	void faceImport(vector<Vector3> f) {
		facesComplete.resize(facesComplete.size() + 1);
		for (int i = 0; i < f.size(); i++) {
			facesComplete.back().point[i][0] = f[i][0];
			facesComplete.back().point[i][1] = f[i][1];
			facesComplete.back().point[i][2] = f[i][2];
		}
		facesComplete.back().vcount = f.size();
		f.clear();
	}

	void centerMesh() {

		float x, y, z;
		float x_min = 0;
		float y_min = 0;
		float z_min = 0;
		float x_max = 0;
		float y_max = 0;
		float z_max = 0;

		for (int i = 0; i < (int)verticesComplete.size(); i++) {

			x = verticesComplete[i].position[0];
			y = verticesComplete[i].position[1];
			z = verticesComplete[i].position[2];

			if (x > x_max) x_max = x;
			else if (x < x_min) x_min = x;
			if (y > y_max) y_max = y;
			else if (y < y_min) y_min = y;
			if (z > z_max) z_max = z;
			else if (z < z_min) z_min = z;
		}

		for (int i = 0; i < (int)verticesComplete.size(); i++) {

			//X zentrieren
			verticesComplete[i].position[0] += ((x_max - x_min) / 2 - x_max);

			//Y Über 0
			verticesComplete[i].position[1] -= y_min;

			//Z zentrieren
			verticesComplete[i].position[2] += ((z_max - z_min) / 2 - z_max);

		}
	}


private:
	std::string linkAsset();
	std::string linkEffects();
	std::string linkMaterials();
	std::string linkGeometries();
	std::string linkController();
	std::string linkVisualScene();

	std::string NodeMaker(int, float, float, float);

	float distance(Vector3, Vector3);
	//vector<float> computeWeight(int, vector<int>);
	float jointDistance(int start, int end);
	float distanceVertexBone(Vector3, int, int);

	Vector3 projectionVertexJointTriangle(int, int, int, int);
};
