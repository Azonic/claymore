//
//  stringProcessing.cpp
//  AutoRigger
//
//  Created by Alexander Pech on 10.03.19.
//  Copyright © 2019 Alexander Pech. All rights reserved.
//

#include <string>
#include <vector>
#include "stringProcessing.h"

//https://thispointer.com/how-to-split-a-string-in-c/
std::vector<std::string> split(std::string stringToBeSplitted, std::string delimeter)
{
    std::vector<std::string> splittedString;
    int startIndex = 0;
    int  endIndex = 0;
    while( (endIndex = (int) stringToBeSplitted.find(delimeter, startIndex)) < stringToBeSplitted.size() )
    {
        std::string val = stringToBeSplitted.substr(startIndex, endIndex - startIndex);
        splittedString.push_back(val);
        startIndex = endIndex + (int) delimeter.size();
    }
    if(startIndex < stringToBeSplitted.size())
    {
        std::string val = stringToBeSplitted.substr(startIndex);
        splittedString.push_back(val);
    }
    return splittedString;
}
//https://stackoverflow.com/questions/3418231/replace-part-of-a-string-with-another-string/3418285 (Lucas Cavil)
std::string replace(std::string base, const std::string from, const std::string to) {
    std::string SecureCopy = base;
    
    for (size_t start_pos = SecureCopy.find(from); start_pos != std::string::npos; start_pos = SecureCopy.find(from,start_pos))
    {
        SecureCopy.replace(start_pos, from.length(), to);
    }
    return SecureCopy;
}

std::string spacer(float i){
    if(i < 0) return std::to_string(i);
    else return " " + std::to_string(i);
}
