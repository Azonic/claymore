//
//  daeExporter.cpp
//  AutoRigger
//
//  Created by Alexander Pech on 23.02.19.
//  Copyright © 2019 Alexander Pech. All rights reserved.
//

#include <stdio.h>
#include <iostream>
#include <fstream>
#include <ctime>
#include <sstream>
#include <iomanip>

#include "model.h"

using namespace std;

string tab(int e){
    string ts = "";
    for (int i = 0; i<e; i++) {
        ts += "\t";
    }
    return ts;
}

void Model::daeExporter(string outputFile){

        
        std::ofstream dae(outputFile);
        
        dae << "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" << endl;
        dae << "<COLLADA xmlns=\"http://www.collada.org/2005/11/COLLADASchema\" version=\"1.4.1\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\">" << endl;
        
        dae << linkAsset();
        dae << linkEffects();
        dae << linkMaterials();
        dae << linkGeometries();
        dae << linkController();
        dae << linkVisualScene();
       // dae << "<library_visual_scenes>\n\t<visual_scene id=\"VisualSceneNode\" name=\"VisualSceneNode\">\n\t\t<node id=\"Object\" name=\"Object\" type=\"NODE\">\n\t\t\t<matrix sid=\"transform\">1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1</matrix>\n\t\t\t<instance_geometry url=\"#geo_mesh\" name=\"Cube\">\n\t\t\t\t<bind_material>\n\t\t\t\t\t<technique_common>\n\t\t\t\t\t\t<instance_material symbol=\"Material-material\" target=\"#Material-material\"/>\n\t\t\t\t\t</technique_common>\n\t\t\t\t</bind_material>\n\t\t\t</instance_geometry>\n\t\t</node>\n\t</visual_scene>\n</library_visual_scenes>";
        dae << "<scene>"<< endl;
        dae << "\t<instance_visual_scene url=\"#VisualSceneNode\"/>" << endl;
        dae << "</scene>" << endl;
        dae << "</COLLADA>";
}

string Model::linkAsset(){
        
    time_t Zeitstempel;
    tm *nun;
    Zeitstempel = time(0);
    nun = localtime(&Zeitstempel);
    
        std::ostringstream oss;
        oss << "<asset>" << endl;
        oss << tab(1) << "<contributor>" << endl;
        oss << tab(2) <<     "<author>AutoRigger by Alexander Pech</author>" << endl;
        oss << tab(1) << "</contributor>" << endl;
        oss << tab(1) << "<created>" << /*std::put_time(&tm, "%d-%m-%Y %H-%M-%S") */ asctime(nun)<< "</created>" << endl;
        oss << tab(1) << "<modified>" << /*std::put_time(&tm, "%d-%m-%Y %H-%M-%S") */ asctime(nun)<< "</modified>" << endl;
        oss << tab(1) << "<unit name=\"meter\" meter=\"1\"></unit>" << endl;
        oss << tab(1) << "<up_axis>Y_UP</up_axis>" << endl;
        oss << "</asset>" << endl;
        return oss.str();
}
    
string Model::linkEffects(){
        
        std::ostringstream oss;
        oss << "<library_effects>" << endl;
        oss << tab(1) << "<effect id=\"Material-effect\">" << endl;
        oss << tab(2) <<    "<profile_COMMON>" << endl;
        oss << tab(3) <<        "<technique sid=\"common\">" << endl;
        oss << tab(4) <<            "<phong>" << endl;
        oss << tab(5) <<                "<emission>" << endl;
        oss << tab(6) <<                    "<color sid=\"emission\">0 0 1 1</color>" << endl;
        oss << tab(5) <<                "</emission>" << endl;
        oss << tab(5) <<                "<ambient>" << endl;
        oss << tab(6) <<                    "<color sid=\"ambient\">0 0 0 1</color>" << endl;
        oss << tab(5) <<                "</ambient>" << endl;
        oss << tab(5) <<                "<diffuse>" << endl;
        oss << tab(6) <<                    "<color sid=\"diffuse\">0.64 0.64 0.64 1</color>" << endl;
        oss << tab(5) <<                "</diffuse>" << endl;
        oss << tab(5) <<                "<specular>" << endl;
        oss << tab(6) <<                    "<color sid=\"specular\">0.5 0.5 0.5 1</color>" << endl;
        oss << tab(5) <<                "</specular>" << endl;
        oss << tab(5) <<                "<shininess>" << endl;
        oss << tab(6) <<                    "<float sid=\"shininess\">50</float>" << endl;
        oss << tab(5) <<                "</shininess>" << endl;
        oss << tab(5) <<                "<index_of_refraction>" << endl;
        oss << tab(6) <<                    "<float sid=\"index_of_refraction\">1</float>" << endl;
        oss << tab(5) <<                "</index_of_refraction>" << endl;
        oss << tab(4) <<            "</phong>" << endl;
        oss << tab(3) <<        "</technique>" << endl;
        oss << tab(2) <<    "</profile_COMMON>" << endl;
        oss << tab(1) << "</effect>" << endl;
        oss << "</library_effects>" << endl;
  
        return oss.str();
        
}
    
string Model::linkMaterials(){
        
        std::ostringstream oss;
        oss << "<library_materials>" << endl;
        oss << tab(1) << "<material id=\"Material-material\" name=\"Material\">" << endl;
        oss << tab(2) <<     "<instance_effect url=\"#Material-effect\"/>" << endl;
        oss << tab(1) << "</material>" << endl;
        oss << "</library_materials>" << endl;
        return oss.str();
}

string Model::linkGeometries(){
    
    int v_num = (int)verticesComplete.size();
    int t_num = (int)texturesComplete.size();
    int n_num = (int)normalsComplete.size();
    int f_num = (int)facesComplete.size();
    
    
    std::ostringstream oss;
    oss << "<library_geometries>" << endl;
    oss << tab(1) << "<geometry id=\"geo_mesh\" name=\"geo_mesh\">" << endl;
    oss << tab(2) <<     "<mesh>" << endl;
    oss << tab(3) <<        "<source id=\"vertex_positions\">" << endl;
    oss << tab(4) <<            "<float_array id=\"vertex-positions-array\" count=\"" << v_num*3 << "\">";// << endl;
        for(int i=0;i<v_num;i++){
            oss << /*tab(5) <<*/ verticesComplete[i].position[0] << " " << verticesComplete[i].position[1] << " " << verticesComplete[i].position[2] << " ";// << endl;
        }
    oss << /*tab(4) <<*/           "</float_array>" << endl;
    oss << tab(4) <<            "<technique_common>" << endl;
    oss << tab(5) <<                "<accessor source=\"#vertex-positions-array\" count=\"" << v_num <<"\" stride=\"3\">" << endl;
    oss << tab(6) <<                    "<param name=\"X\" type=\"float\"/>" << endl;
    oss << tab(6) <<                    "<param name=\"Y\" type=\"float\"/>" << endl;
    oss << tab(6) <<                    "<param name=\"Z\" type=\"float\"/>" << endl;
    oss << tab(5) <<                "</accessor>" << endl;
    oss << tab(4) <<            "</technique_common>" << endl;
    oss << tab(3) <<        "</source>" << endl;
    oss << tab(3) <<        "<source id=\"vertex_normal\">" << endl;
    oss << tab(4) <<            "<float_array id=\"vertex-normal-array\" count=\"" << n_num*3 << "\">";// << endl;
        for(int i=0;i<n_num;i++){
            oss << tab(0) << normalsComplete[i].direction[0] << " " << normalsComplete[i].direction[1] << " " << normalsComplete[i].direction[2] << " ";// << endl;
        }
    oss << tab(0) <<            "</float_array>" << endl;
    oss << tab(4) <<            "<technique_common>" << endl;
    oss << tab(5) <<                "<accessor source=\"#vertex-normal-array\" count=\"" << n_num <<"\" stride=\"3\">" << endl;
    oss << tab(6) <<                    "<param name=\"X\" type=\"float\"/>" << endl;
    oss << tab(6) <<                    "<param name=\"Y\" type=\"float\"/>" << endl;
    oss << tab(6) <<                    "<param name=\"Z\" type=\"float\"/>" << endl;
    oss << tab(5) <<                "</accessor>" << endl;
    oss << tab(4) <<            "</technique_common>" << endl;
    oss << tab(3) <<        "</source>" << endl;
    oss << tab(3) <<        "<source id=\"vertex_textur\">" << endl;
    oss << tab(4) <<            "<float_array id=\"vertex-textur-array\" count=\"" << t_num*2 << "\">" << endl;
        for(int i=0;i<t_num;i++){
            oss << tab(5) << texturesComplete[i].position[0] << " " << texturesComplete[i].position[1] << " " << endl;
        }
    oss << endl;
    oss << tab(4) <<            "</float_array>" << endl;
    oss << tab(4) <<            "<technique_common>" << endl;
    oss << tab(5) <<                "<accessor source=\"#vertex-textur-array\" count=\"" << t_num <<"\" stride=\"2\">" << endl;
    oss << tab(6) <<                    "<param name=\"U\" type=\"float\"/>" << endl;
    oss << tab(6) <<                    "<param name=\"V\" type=\"float\"/>" << endl;
    oss << tab(5) <<                "</accessor>" << endl;
    oss << tab(4) <<            "</technique_common>" << endl;
    oss << tab(3) <<        "</source>" << endl;
    oss << tab(3) <<        "<source id=\"vertex_color\">" << endl;
    oss << tab(4) <<            "<float_array id=\"vertex-color-array\" count=\"" << v_num*3 << "\">"; // << endl;
        for(int i=0;i<v_num;i++){
            oss << /*tab(5) <<*/ verticesComplete[i].color[0] << " " << verticesComplete[i].color[1] << " " << verticesComplete[i].color[2] << " ";// << endl;
        }
    oss << /*tab(4) <<*/            "</float_array>" << endl;
    oss << tab(4) <<            "<technique_common>" << endl;
    oss << tab(5) <<                "<accessor source=\"#vertex-color-array\" count=\"" << v_num <<"\" stride=\"3\">" << endl;
    oss << tab(6) <<                    "<param name=\"R\" type=\"float\"/>" << endl;
    oss << tab(6) <<                    "<param name=\"G\" type=\"float\"/>" << endl;
    oss << tab(6) <<                    "<param name=\"B\" type=\"float\"/>" << endl;
    oss << tab(5) <<                "</accessor>" << endl;
    oss << tab(4) <<            "</technique_common>" << endl;
    oss << tab(3) <<        "</source>" << endl;
    oss << tab(3) <<        "<vertices id=\"mesh-vertices\">" << endl;
    oss << tab(4) <<            "<input semantic=\"POSITION\" source=\"#vertex_positions\"/>" << endl;
    oss << tab(3) <<        "</vertices>" << endl;
    oss << tab(3) <<        "<polylist material=\"Material-material\" count=\"" << f_num << "\">" << endl;
    oss << tab(4) <<            "<input semantic=\"VERTEX\" source=\"#mesh-vertices\" offset=\"0\"/>" << endl;
       int offset = 1;
         if(t_num > 0)
        {
            oss << tab(4) <<    "<input semantic=\"TEXTUR\" source=\"#vertex_textur\" offset=\"" << offset <<"\"/>" << endl;
            offset++;
        }
        if(n_num > 0)
        {
            oss << tab(4) <<    "<input semantic=\"NORMAL\" source=\"#vertex_normal\" offset=\"" << offset <<"\"/>" << endl;
            offset++;
        }
    oss << tab(4) <<           "<input semantic=\"COLOR\" source=\"#vertex_color\" offset=\"" << offset << "\"/>" << endl;
    oss << tab(4) <<           "<vcount>";// << endl;
    //oss << tab(5);
        for (int i = 0; i < f_num; i++) {
            oss <<                 facesComplete[i].vcount << " ";
        }
    //oss << endl;
    oss << /*tab(4) <<*/          "</vcount>" << endl;
    oss << tab(4) <<          "<p>";// << endl;
            for(int i = 0; i < f_num; i++){
                //oss << tab(5);
                for (int j = 0; j < facesComplete[i].vcount; j++) {
                    oss << facesComplete[i].point[j][0] - 1 << " ";
                    if(t_num != 0) oss << facesComplete[i].point[j][1] - 1 << " ";
                    if(n_num != 0) oss << facesComplete[i].point[j][2] - 1 << " ";
                    oss << facesComplete[i].point[j][0] - 1 << " "; //VertexColor
                }
                //oss << endl;
            }
    oss << /*tab(4) <<*/          "</p>" << endl;
    oss << tab(3) <<        "</polylist>" << endl;
    oss << tab(2) <<     "</mesh>" << endl;
    oss << tab(1) << "</geometry>" << endl;
    oss << "</library_geometries>" << endl;
    return oss.str();
}

std::string Model::linkController(){
    
    std::ostringstream oss;
    oss << "<library_controllers>" << endl;
    oss << tab(1) << "<controller id=\"Skin_Controller\" name=\"Skin_Controller\">" << endl;
    oss << tab(2) <<    "<skin source=\"#geo_mesh\">" << endl;
    oss << tab(3) <<        "<bind_shape_matrix>" ;///<< endl;
    oss << /*tab(4) <<        */    "1 0 0 0 "; // << endl;
    oss << /*tab(4) <<      */      "0 1 0 0 ";//  << endl;
    oss << /*tab(4) <<    */        "0 0 1 0 ";// << endl;
    oss << /*tab(4) <<*/            "0 0 0 1 ";// << endl;
    oss << /*tab(3) <<*/        "</bind_shape_matrix>" << endl;
    oss << tab(3) <<        "<source id=\"skin-joints\">" << endl;
    oss << tab(4) <<            "<Name_array id=\"skin-joints-array\" count=\"" << (int)skeletonOutput.size() << "\">"; // << endl;
    for(int i = 0; i < (int)skeletonOutput.size();i++){
        oss << /*tab(5) <<*/            skeletonOutput[i].name << " ";//endl;
    }
    oss << /*tab(4) <<*/            "</Name_array>" << endl;
    oss << tab(4) <<            "<technique_common>" << endl;
    oss << tab(5) <<                "<accessor source=\"#skin-joints-array\" count=\"" << (int)skeletonOutput.size() << "\" stride=\"1\">" << endl;
    oss << tab(6) <<                    "<param name=\"JOINT\" type=\"Name\"></param>" << endl;
    oss << tab(5) <<                "</accessor>" << endl;
    oss << tab(4) <<            "</technique_common>" << endl;
    oss << tab(3) <<        "</source>" << endl;
    oss << tab(3) <<        "<source id=\"skin-bind_poses\">" << endl;
    oss << tab(4) <<            "<float_array id=\"skin-bind_poses-array\" count=\"" << (int)skeletonOutput.size() * 16 << "\">";// << endl;
    for (int i = 0; i < (int)skeletonOutput.size(); i++) {
       // oss << tab(5) << "<!-- " << skeletonOutput[i].name << " -->" << endl;
        oss /* << tab(5)*/ << "1 0 0 " << -skeletonOutput[i].position[0] << " ";// endl;
        oss /* << tab(5)*/ << "0 1 0 " << -skeletonOutput[i].position[1] << " ";//endl;
        oss /* << tab(5)*/ << "0 0 1 " << -skeletonOutput[i].position[2] << " ";//endl;
        oss /* << tab(5)*/ << "0 0 0 1 ";// << endl;
    }
    oss << /*tab(4) <<*/            "</float_array>" << endl;
    oss << tab(4) <<            "<technique_common>" << endl;
    oss << tab(5) <<                "<accessor source=\"#skin-bind_poses-array\" count=\"" << (int)skeletonOutput.size() << "\" stride=\"16\">" << endl;
    oss << tab(6) <<                    "<param name=\"TRANSFORM\" type=\"float4x4\"></param>" << endl;
    oss << tab(5) <<                "</accessor>" << endl;
    oss << tab(4) <<            "</technique_common>" << endl;
    oss << tab(3) <<        "</source>" << endl;
    oss << tab(3) <<        "<source id=\"skin-weights\">" << endl;
   /* oss << tab(4) <<            "<float_array id=\"skin-weights-array\" count=\"" << verticesComplete.size() * skeletonOutput.size() << "\">" << endl;
    for (int i = 0; i < verticesComplete.size(); i++) {
         oss << tab(5);
        for(int j = 0; j < skeletonOutput.size(); j++){
            oss << verticesComplete[i].weighting[j] << " ";
        }
         oss << endl;
    }*/
            std::ostringstream w;
            int c = 0;
            for (int i = 0; i < verticesComplete.size(); i++) {
                for(int j = 0; j < skeletonOutput.size(); j++){
                    if(verticesComplete[i].weighting[j]>0){
                        w /* << tab(5)*/ << verticesComplete[i].weighting[j] << " ";
                        c++;
                    }
                }
                //w   << endl;
            }
    oss << tab(4) <<            "<float_array id=\"skin-weights-array\" count=\"" << c << "\">"; ///< endl;
     oss << tab(0) << w.str();
     //oss << endl;
    

   
    oss << /*tab(4) <<*/            "</float_array>" << endl;
    oss << tab(4) <<            "<technique_common>" << endl;
    oss << tab(5) <<                "<accessor source=\"#skin-weights-array\" count=\"" << /*verticesComplete.size() * skeletonOutput.size()*/ c << "\" stride=\"1\">" << endl;
    oss << tab(6) <<                    "<param name=\"WEIGHT\" type=\"float\"></param>" << endl;
    oss << tab(5) <<                "</accessor>" << endl;
    oss << tab(4) <<            "</technique_common>" << endl;
    oss << tab(3) <<        "</source>" << endl;
    oss << tab(3) <<        "<joints>" << endl;
    oss << tab(4) <<            "<input semantic=\"JOINT\" source=\"#skin-joints\"></input>" << endl;
    oss << tab(4) <<            "<input semantic=\"INV_BIND_MATRIX\" source=\"#skin-bind_poses\"></input>" << endl;
    oss << tab(3) <<        "</joints>" << endl;
    oss << tab(3) <<        "<vertex_weights count=\"" << (int)verticesComplete.size() << "\">" << endl;
    oss << tab(4) <<            "<input semantic=\"JOINT\" source=\"#skin-joints\" offset=\"0\"></input>" << endl;
    oss << tab(4) <<            "<input semantic=\"WEIGHT\" source=\"#skin-weights\" offset=\"1\"></input>" << endl;
    oss << tab(4) <<            "<vcount>";// << endl;
    //oss << tab(5);
    for(int i = 0; i < (int)verticesComplete.size();i++)
    {
        //oss << (int)skeletonOutput.size() << " ";
        int q = 0;
        for(int j = 0; j < verticesComplete[i].weighting.size(); j++){
            if(verticesComplete[i].weighting[j] > 0) q++;
        }
        oss << q << " ";
    }
   // oss << endl;
    oss << /*tab(4) <<*/            "</vcount>" << endl;
    oss << tab(4) <<            "<v>" ;// << endl;
    /*
    for (int i = 0; i < verticesComplete.size(); i++) {
        oss << tab(5);
        for(int j = 0; j < skeletonOutput.size(); j++){
            oss << j << " " << i * skeletonOutput.size() + j << " ";
        }
        oss << endl;
    }*/
    int weightIndex = 0;
    for (int i = 0; i < verticesComplete.size(); i++) {
       // oss << tab(5);
        for(int j = 0; j < skeletonOutput.size(); j++){
            if (verticesComplete[i].weighting[j] > 0){
                oss << j << " " << weightIndex << " ";
                weightIndex++;
            }
        }
        //oss << endl;
    }
   
    oss << /*tab(4) <<*/            "</v>" << endl;
    oss << tab(3) <<        "</vertex_weights>" << endl;
    oss << tab(2) <<    "</skin>" << endl;
    oss << tab(1) << "</controller>" << endl;
    oss << "</library_controllers>" << endl;
    
    return oss.str();
    
}

string Model::NodeMaker(int jointID, float x, float y, float z){
    //int jointID = jID;
    
    std::ostringstream oss;
    oss << tab(0 + jointID*0) << "<node "; if(jointID==0) oss << "id=\"skeleton_root\" "; oss << "name=\"" << skeletonOutput[jointID].name << "\" sid=\"" << skeletonOutput[jointID].name << "\" type=\"JOINT\">" << endl;
   /* oss << tab(1 + jointID*0) << "<translate sid=\"translate\">" << skeletonOutput[jointID].position[0] - x << " " << skeletonOutput[jointID].position[1] - y << " " << skeletonOutput[jointID].position[2] - z << "</translate>" << endl;
    oss << tab(1+ jointID*0) << "<rotate sid=\"jointOrientX\">1 0 0 0</rotate>" << endl;
    oss << tab(1+ jointID*0) << "<rotate sid=\"jointOrientY\">0 1 0 0</rotate>" << endl;
    oss << tab(1+ jointID*0) << "<rotate sid=\"jointOrientZ\">0 0 1 0</rotate>" << endl;*/
    oss << tab(1) << "<matrix sid=\"transform\"> 1 0 0 " << skeletonOutput[jointID].position[0] - x << " 0 1 0 " << skeletonOutput[jointID].position[1] - y << " 0 0 1 " << skeletonOutput[jointID].position[2] - z << " 0 0 0 1</matrix>" << endl;
    
    for (int i = 0 ; i < (int)skeletonOutput.size(); i++) {
        if(skeletonOutput[i].parentJoint == jointID){
           oss << NodeMaker(i,skeletonOutput[jointID].position[0],skeletonOutput[jointID].position[1],skeletonOutput[jointID].position[2]);
        }
    }
    oss << tab(0 + jointID*0) << "</node>" << endl;
    
    return oss.str();
}

string Model::linkVisualScene(){
    
    std::ostringstream oss;
    oss << "<library_visual_scenes>" << endl;
    oss << tab(1) << "<visual_scene id=\"VisualSceneNode\" name=\"bind_sample\">" << endl;
    oss << tab(0) <<    NodeMaker(0, 0, 0, 0);
    oss << tab(2) <<    "<node id=\"Output\" name=\"Output\" type=\"NODE\">" << endl;
    oss << tab(3) <<        "<transform>" ;// << endl;
    oss << /*tab(4) <<        */    "1 0 0 0 "; // << endl;
    oss << /*tab(4) <<      */      "0 1 0 0 ";//  << endl;
    oss << /*tab(4) <<    */        "0 0 1 0 ";// << endl;
    oss << /*tab(4) <<*/            "0 0 0 1 ";// << endl;
    oss << /*tab(3) <<*/        "</transform>" << endl;
    oss << tab(3) <<        "<instance_controller url=\"#Skin_Controller\">" << endl;
    oss << tab(4) <<            "<skeleton>#skeleton_root</skeleton>" << endl;
    oss << tab(4) <<            "<bind_material>" << endl;
    oss << tab(5) <<                "<technique_common>" << endl;
    oss << tab(6) <<                    "<instance_material symbol=\"initialShadingGroup\" target=\"#Material-material\"></instance_material>" << endl;
    oss << tab(5) <<                "</technique_common>" << endl;
    oss << tab(4) <<            "</bind_material>" << endl;
    oss << tab(2) <<        "</instance_controller>" << endl;
    oss << tab(2) <<    "</node>" << endl;
    
    //oss << "\n\t\t<extra><technique profile=\"FCOLLADA\"><start_time>0.041666</start_time><end_time>2</end_time></technique></extra>";
    
    oss << tab(1) << "</visual_scene>" << endl;
    oss << "</library_visual_scenes>" << endl;
    return oss.str();
}




