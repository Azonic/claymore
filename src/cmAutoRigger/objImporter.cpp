//
//  objImporter.cpp
//  AutoRigger
//
//  Created by Alexander Pech on 24.02.19.
//  Copyright © 2019 Alexander Pech. All rights reserved.
//

#include <stdio.h>
#include <vector>

#include <iostream>
#include <fstream>
#include <sstream>
#include <map>
#include <set>
#include <algorithm>
#include <string>

#include "model.h"
#include "stringProcessing.h"

void Model::objImporter(std::string &file){

    std::ifstream strm(file.c_str());
    
    
    int lineNum = 0;
    //while(!strm.eof()) {
    std::string s;
    while (std::getline(strm, s)){
        ++lineNum;
        
        s = replace(s, "  ", " ");
        s = replace(s, "\a", "");
        s = replace(s, "\b", "");
        s = replace(s, "\t", "");
        s = replace(s, "\n", "");
        s = replace(s, "\v", "");
        s = replace(s, "\f", "");
        s = replace(s, "\r", "");
       
        std::vector<std::string> words = split(s, " ");
   
        
        if(words.size() == 0)
            continue;
        
        //deal with the line based on the first word
        if(words[0] == "v"){//if(words[0][0] == 'v') {
            if(words.size() < 4) {
                std::cout << "(v)-Error on line " << lineNum << words.size() <<std::endl;
                verticesComplete.clear();
                facesComplete.clear();
                texturesComplete.clear();
                normalsComplete.clear();
                //return 0;
            }
            
            float x, y, z, r, g, b;
            sscanf(words[1].c_str(), "%f", &x);
            sscanf(words[2].c_str(), "%f", &y);
            sscanf(words[3].c_str(), "%f", &z);
            
           // std::cout << words[1].c_str() << " " << std::stod(words[1].c_str()) << " " << x <<endl;
            /*
            verticesComplete.resize(verticesComplete.size() + 1);
            verticesComplete.back().position[0] = x;
            verticesComplete.back().position[1] = y;
            verticesComplete.back().position[2] = z;
            */
            r = 0;
            g = 0;
            b = 0;
    
            if (words.size()>5) {
                sscanf(words[4].c_str(), "%f", &r);
                sscanf(words[5].c_str(), "%f", &g);
                sscanf(words[6].c_str(), "%f", &b);
                
                //verticesComplete.back().color[0] = r;
                //verticesComplete.back().color[1] = g;
                //verticesComplete.back().color[2] = b;
            }/*
            else{
                verticesComplete.back().color[0] = 0;
                verticesComplete.back().color[1] = 0;
                verticesComplete.back().color[2] = 0;
            }*/
            vertexImport(x, y, z, r, g, b);
            
        }
        
        else if(words[0] == "vt") {
            if(words.size() != 3) {
                std::cout << "(vt)-Error on line " << lineNum << words.size() <<std::endl;
                verticesComplete.clear();
                facesComplete.clear();
                texturesComplete.clear();
                normalsComplete.clear();
                //return 0;
            }
            
            float u,v;
            sscanf(words[1].c_str(), "%f", &u);
            sscanf(words[2].c_str(), "%f", &v);
            
            texturImport( u, v);
        }
        
        else if(words[0] == "vn") {
            if(words.size() != 4) {
                std::cout << "(vn)-Error on line " << lineNum << words.size() <<std::endl;
                verticesComplete.clear();
                facesComplete.clear();
                texturesComplete.clear();
                normalsComplete.clear();
                //return 0;
            }
            
            float x, y, z;
            sscanf(words[1].c_str(), "%f", &x);
            sscanf(words[2].c_str(), "%f", &y);
            sscanf(words[3].c_str(), "%f", &z);
            
            normalImport(x, y, z);
        }
       
        if(words[0][0] == 'f') {
            if(words.size() < 4 || words.size() > 15) {
                std::cout << "(f)-Error on line " << lineNum << std::endl;
                verticesComplete.clear();
                facesComplete.clear();
                texturesComplete.clear();
                normalsComplete.clear();
                //return 0;
            }
            
            //facesComplete.resize(facesComplete.size()+1);
            vector<Vector3> f;
            
            for (int i = 1; i < (int)words.size(); i++) {
                //std::cout << words[i] << std::endl;
                int v = 0;
                int t = 0;
                int n = 0;
                std::vector<std::string> wordsSplitted1 = split(words[i], "//");
                if(wordsSplitted1.size()==2){
                    //std::cout << "Vextex and Normal" << std::endl;
                    v = atoi(wordsSplitted1[0].c_str());
                    n = atoi(wordsSplitted1[1].c_str());
                }
                else {
                    std::vector<std::string> wordsSplitted2 = split(words[i], "/");
                    if(wordsSplitted2.size()==1){
                        //std::cout << "Vextex" << std::endl;
                        v = atoi(wordsSplitted2[0].c_str());
                    }
                    else if(wordsSplitted2.size()==2){
                        //std::cout << "Vextex and Textur" << std::endl;
                        v = atoi(wordsSplitted2[0].c_str());
                        t = atoi(wordsSplitted2[1].c_str());
                    }
                    else if(wordsSplitted2.size()==3){
                        //std::cout << "Vextex, Textur and Normal" << std::endl;
                        v = atoi(wordsSplitted2[0].c_str());
                        t = atoi(wordsSplitted2[1].c_str());
                        n = atoi(wordsSplitted2[2].c_str());
                    }
                }
                
               // facesComplete.back().point[i-1][0] = v;
               //facesComplete.back().point[i-1][1] = t;
               // facesComplete.back().point[i-1][2] = n;
                f.resize(f.size()+1);
                f.back() = Vector3(v,t,n);
                
            }
            faceImport(f);
            f.clear();
            //facesComplete.back().vcount = words.size()-1;
        }
    }
}
