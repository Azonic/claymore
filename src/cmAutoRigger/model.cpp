//
//  modelProcessor.cpp
//  AutoRigger
//
//  Created by Alexander Pech on 25.02.19.
//  Copyright © 2019 Alexander Pech. All rights reserved.
//

#include "model.h"
/*
#include "stringProcessing.cpp"
#include "objImporter.cpp"
#include "vertexWeighting.cpp"
#include "debugOutput.cpp"
#include "pinocchioHandler.cpp"
#include "daeExporter.cpp"
*/
int Model::modelProcessor(){
    
    std::string inputFile = "C:/Users/Neon/Desktop/stickman.obj";
    std::string outputFile1, outputFile2;
    
// if(argc > 1)
//	       inputFile = argv[1];
   
    outputFile1 = replace(inputFile,".obj","Rigged_Pinocchio_Weights.dae");
    outputFile2 = replace(inputFile,".obj","Rigged_alternative_Weights.dae");
    
    Model model(inputFile); //use objImporter
    /*
     Alternativ:
     Model model();
     
     model.vertexImport(float x, float y, float z, float r, float g, float b);
     model.normalImport(float x, float y, float z);
     model.texturImport(float u, float v);
     
     faceImport(vector<Vector3>) 3x oder 4x Vector3(V,T,N)
    */
    
    model.pinocchioHandler(model.skeletonInput);
    model.debugOutput();
    model.daeExporter(outputFile1);
    model.vertexWeight();
    model.daeExporter(outputFile2);
    std::cout << "OUTPUT: " << outputFile1 << " " << outputFile2  << std::endl;
    
    //std::cin.get();
    return 0;
}

