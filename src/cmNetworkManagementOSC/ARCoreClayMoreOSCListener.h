// ClayMore - Immersive Mesh Modelling --- Copyright (c) 2014-2017 Philipp Ladwig, Jannik Fiedler, Jan Beutgen
#pragma once

#include "ClayMoreOSCListener.h"

//TODO: Seems to be the only include of OSG in NetworkManagerOSC -> Offer a more generic non-osg matrix class? This .dll would be more independent
#include <osg/MatrixTransform>

class ARCoreClayMoreOSCListener : public osc::OscPacketListener
{
	// #### CONSTRUCTOR & DESTRUCTOR ###############
public:
	ARCoreClayMoreOSCListener();
	~ARCoreClayMoreOSCListener(void);

	// #### MEMBER VARIABLES ###############
private:
	osg::ref_ptr<osg::MatrixTransform> m_viewMatrixTransform;
	osg::Matrix m_posMatrix;
	osg::Matrix m_rotMatrix;

	// #### FUNCTIONS ###############
public:
	inline osg::ref_ptr<osg::MatrixTransform> getViewMatrixTransform() { return m_viewMatrixTransform; }

protected:
	void ProcessMessage(const osc::ReceivedMessage& m, const IpEndpointName& remoteEndpoint);
};