#pragma once

const unsigned short MAX_NUM_CLIENTS = 10;
const unsigned short RECVPORT = 8000; // ARCore = 7008?
const unsigned short SENDPORT = 7008; // ARCore = 7008?

const unsigned short ARCORE_SENDPORT = 7008; // ARCore = 7008?
const unsigned short ARCORE_RECVPORT = 7008; // ARCore = 7008?

const unsigned short CONTROL_COMMAND_SENDPORT = 7010;
const unsigned short CONTROL_COMMAND_RECVPORT = 7010;

const unsigned short STATUS_UPDATE_SENDPORT = 7020;
const unsigned short STATUS_UPDATE_RECVPORT = 7020;

const unsigned short LANDMARK70_RECVPORT = 9000;
const unsigned short EYE_TRACKING_SENDPORT = 5005;

const unsigned short BUFFER = 55000;

const std::string ipAdress = "127.0.0.1"; //TODO3: Move specific adresses for the sender functions to config.xml

