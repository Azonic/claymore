// ClayMore - Immersive Mesh Modelling --- Copyright (c) 2014-2017 Philipp Ladwig, Jannik Fiedler, Jan Beutgen
#pragma once

#include "ConfigDllExporNetworkManagementOSC.h"
#include "cmUtil/Landmark.h"

//TODO: Seems to be the only include of OSG in NetworkManagerOSC -> Offer a more generic non-osg matrix class? This .dll would be more independent
#include <osg/MatrixTransform>

class ConfigReader;
class ARCoreClayMoreOSCListener;
class ControlCommandListener;
class StatusUpdateListener;
class SeventyFacialLandMarksOverOSCListener;
class MagicLeapListener;
class UdpListeningReceiveSocket;


namespace  osg
{
	class DrawElementsUInt;
}


class imNetworkManagementOSC_DLL_import_export NetworkManagerOSC
{
	// #### CONSTRUCTOR & DESTRUCTOR ###############
public:
	//NetworkManagerOSC(bool startARCoreListener, bool startMagicLeapListener, bool startControlCommandListener, bool startStatusUpdateListener);
	NetworkManagerOSC(ConfigReader* configReaderIn);
	~NetworkManagerOSC();
	
	// #### MEMBER VARIABLES ###############
private:
	ConfigReader* m_configReaderIn;

	ARCoreClayMoreOSCListener* m_arCoreClayMoreOSCListener;
	UdpListeningReceiveSocket* m_socket;
	
	ControlCommandListener* m_controlCommandListener;
	UdpListeningReceiveSocket* m_socketControlCommandListener;

	StatusUpdateListener* m_statusUpdateListener;
	UdpListeningReceiveSocket* m_socketStatusUpdateListener;

	SeventyFacialLandMarksOverOSCListener* m_seventyFacialLandMarksOverOSCListener;
	UdpListeningReceiveSocket* m_socketSeventyFacialLandMarksOverOSCListener;
	
	MagicLeapListener* m_magicLeapListener;

	bool m_startARCoreListener;
	bool m_startMagicLeapListener;
	bool m_startControlCommandListener;
	bool m_startStatusUpdateListener;
	bool m_startSeventyFacialLandMarksOverOSCListener;
	
	//IPs
	std::string m_sranipalEyeTrackingSenderToFacialLandmarksOscClientIP;

	// #### MEMBER FUNCTIONS ###############
public:
	//TODO2: Officially there is no possibilty to open multiple OSC listiners...its a limition of oscPack. Therefore,
	// 		it would be better if there is only one run() method and it listen to different channels (based on the 
	// 		OSC adress patterns such as /landmarks/...
	void runARCoreListener();
	void runMagicLeapListener();
	void runControlCommandListener();
	void runStatusUpdateListener();
	void runSeventyFacialLandMarksOverOSCListener();

	void activateARCoreListener();
	void activateMagicLeapListener();
	void activateControlCommandListener();
	void activateStatusUpdateListener();
	void activateSeventyFacialLandMarksOverOSCListener();

	void sendVertexArray(osg::Vec3Array* arrayIn);
	void sendIndexArray(osg::DrawElementsUInt* arrayIn);
	void sendColorArray(osg::Vec4Array* arrayIn);
	void sendNormalArray(osg::Vec3Array* arrayIn);
	
	osg::ref_ptr<osg::MatrixTransform> getARCoreViewMatrix();
	std::vector<FacialLandmark> getSeventyFacialLandmarks();

	void sendControlCommand(std::string commandName, std::string commandValue);
	void sendStatusUpdate(std::string statusName, std::string statusValue);
	void registerCallbackForControlCommandListener(void(*callback)(), std::string name);

	void setIpAddressForSRanipalEyeSender(std::string ip);
	void sendEyeTrackingData(float leftX,
		float leftY,
		float leftOpeness,
		float rightX,
		float rightY,
		float rightOpeness);
};