#include "ClayMoreOSCListener.h"

#include <iostream>
#include <cstring>

#include "defines.h"

#include "osc/OscPacketListener.h"
#include "ip/UdpSocket.h"

ClayMoreOSCListener::ClayMoreOSCListener(int maxNumClients, int port)
{

}

ClayMoreOSCListener::~ClayMoreOSCListener()
{
	//TODO: Close thread and connection
}

//void ClayMoreOSCListener::ProcessMessage(const osc::ReceivedMessage& m, const IpEndpointName& remoteEndpoint)
//{
//	//The following code is based on SimpleReceive from oscpack
//	try {
//		// example of parsing single messages. osc::OsckPacketListener
//		// handles the bundle traversal.
//
//		if (std::strcmp(m.AddressPattern(), "/camera/time/1") == 0) {
//			// example #1 -- argument stream interface
//			osc::ReceivedMessageArgumentStream args = m.ArgumentStream();
//			__int64 time;
//			args >> time >> osc::EndMessage;
//			std::cout << "received '/camera/time/1' message with arguments: " << time << "\n";
//		}
//		else if (std::strcmp(m.AddressPattern(), "/camera/pos") == 0) {
//			// example #2 -- argument iterator interface, supports
//			// reflection for overloaded messages (eg you can call 
//			// (*arg)->IsBool() to check if a bool was passed etc).
//			osc::ReceivedMessage::const_iterator arg = m.ArgumentsBegin();
//			float posX = (arg++)->AsFloat();
//			float posY = (arg++)->AsFloat();
//			float posZ = (arg++)->AsFloat();
//			//const char *a4 = (arg++)->AsString();
//			if (arg != m.ArgumentsEnd())
//				throw osc::ExcessArgumentException();
//
//			std::cout << "received '/camera/pos' message with arguments: "
//				<< posX << " " << posY << " " << posZ << " " << "\n";
//		}
//		else if (std::strcmp(m.AddressPattern(), "/camera/rot") == 0) {
//			// example #2 -- argument iterator interface, supports
//			// reflection for overloaded messages (eg you can call 
//			// (*arg)->IsBool() to check if a bool was passed etc).
//			osc::ReceivedMessage::const_iterator arg = m.ArgumentsBegin();
//			float quat0 = (arg++)->AsFloat();
//			float quat1 = (arg++)->AsFloat();
//			float quat2 = (arg++)->AsFloat();
//			float quat3 = (arg++)->AsFloat();
//			//const char *a4 = (arg++)->AsString();
//			if (arg != m.ArgumentsEnd())
//				throw osc::ExcessArgumentException();
//
//			std::cout << "received '/camera/rot' message with arguments: "
//				<< quat0 << " " << quat1 << " " << quat2 << " " << quat3 << "\n";
//		}
//		else if (std::strcmp(m.AddressPattern(), "/camera/time/2") == 0) {
//			// example #1 -- argument stream interface
//			osc::ReceivedMessageArgumentStream args = m.ArgumentStream();
//			__int64 time;
//			args >> time >> osc::EndMessage;
//			std::cout << "received '/camera/time/2' message with arguments: " << time << "\n";
//		}
//	}
//	catch (osc::Exception& e) {
//		// any parsing errors such as unexpected argument types, or 
//		// missing arguments get thrown as exceptions.
//		std::cout << "error while parsing message: "
//			<< m.AddressPattern() << ": " << e.what() << "\n";
//	}
//}
