 // ClayMore - Immersive Mesh Modelling --- Copyright (c) 2014-2017 Philipp Ladwig, Jannik Fiedler, Jan Beutgen
 #pragma once

 #include "ClayMoreOSCListener.h"

 class StatusUpdateListener : public osc::OscPacketListener
 {
	 // #### CONSTRUCTOR & DESTRUCTOR ###############
 public:
	 StatusUpdateListener();
	 ~StatusUpdateListener(void);

	 // #### MEMBER VARIABLES ###############
 private:

	 // #### FUNCTIONS ###############
 public:
	 //inline osg::ref_ptr<osg::MatrixTransform> getViewMatrixTransform() { return m_viewMatrixTransform; }

 protected:
	 void ProcessMessage(const osc::ReceivedMessage& m, const IpEndpointName& remoteEndpoint);
 };