// ClayMore - Immersive Mesh Modelling --- Copyright (c) 2014-2017 Philipp Ladwig, Jannik Fiedler, Jan Beutgen
#pragma once

#include "osc/OscPacketListener.h"

#include <iostream>
#include <cstring>

class ClayMoreOSCListener : public osc::OscPacketListener
{
	// #### CONSTRUCTOR & DESTRUCTOR ###############
public:
	ClayMoreOSCListener(int maxNumClients, int port);
	~ClayMoreOSCListener(void);

	// #### MEMBER VARIABLES ###############
private:

	// #### FUNCTIONS ###############
protected:
	virtual void ProcessMessage(const osc::ReceivedMessage& m, const IpEndpointName& remoteEndpoint) = 0;
};