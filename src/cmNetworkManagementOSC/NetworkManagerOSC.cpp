//#include "pch.h"
//TODO: Insert compiler switch for UWP and Desktop

#include "NetworkManagerOSC.h"
#include "../cmUtil/ConfigReader.h"

#include "defines.h"

#include "ARCoreClayMoreOSCListener.h"
#include "ControlCommandListener.h"
#include "StatusUpdateListener.h"
#include "MagicLeapListener.h"
#include "SeventyFacialLandMarksOverOSCListener.h"

#include "osc/OscReceivedElements.h"
#include "osc/OscPacketListener.h"
#include "osc/OscOutboundPacketStream.h"
#include "ip/UdpSocket.h"

#include "osg/Geometry"

#include <stdio.h>
#include <iostream>

//TODO4: Actually it is stupid to initilize the NetworkManager with multiple bools. I'm sure a modul-based concept would be intuitiver and better style.
//		Something like: m_NetworkManagerOSC.addListener
//		Also the concept of Listener and also Sender would make sense... but actually the current concept of just pushing a specific struct or class
//		into NetworkManagerOSC.send() is quite easy 

NetworkManagerOSC::NetworkManagerOSC(ConfigReader* configReaderIn)
	//bool startARCoreListener,
	//bool startMagicLeapListener,
	//bool startControlCommandListener,
	//bool startStatusUpdateListener) :
	//m_startARCoreListener(startARCoreListener),
	//m_startMagicLeapListener(startMagicLeapListener),
	//m_startControlCommandListener(startControlCommandListener),
	//m_startStatusUpdateListener(startStatusUpdateListener)
{
	m_configReaderIn = configReaderIn;
	m_startARCoreListener = false;
	m_startMagicLeapListener = false;
	m_startControlCommandListener = false;
	m_startStatusUpdateListener = false;
	m_startSeventyFacialLandMarksOverOSCListener = false;

	//	if (m_startARCoreListener)
	//	{
	//		
	//	}
	//
	//	if (m_startMagicLeapListener)
	//	{
	//		//m_magicLeapListener = new MagicLeapListener(MAX_NUM_CLIENTS, RECVPORT);
	//		//m_socket = new UdpListeningReceiveSocket(
	//			//IpEndpointName(IpEndpointName::ANY_ADDRESS, RECVPORT), m_magicLeapListener);
	//	}
	//
	//	if (m_startControlCommandListener)
	//	{
	//
	//	}
	//
	//	if (m_startStatusUpdateListener)
	//	{
	//
	//	}
}

void NetworkManagerOSC::activateARCoreListener()
{
	if (m_configReaderIn->DEBUG_LEVEL >= NOT_IN_LOOPS)
	{
		std::cout << "In activateARCoreListener" << std::endl;
	}
	m_startARCoreListener = true;
	m_arCoreClayMoreOSCListener = new ARCoreClayMoreOSCListener();
	m_socket = new UdpListeningReceiveSocket(IpEndpointName(IpEndpointName::ANY_ADDRESS, ARCORE_RECVPORT), m_arCoreClayMoreOSCListener);
}

void NetworkManagerOSC::activateMagicLeapListener()
{
	if (m_configReaderIn->DEBUG_LEVEL >= NOT_IN_LOOPS)
	{
		std::cout << "In activateMagicLeapListener" << std::endl;
	}
	m_startMagicLeapListener = true;
	//m_magicLeapListener = new MagicLeapListener(MAX_NUM_CLIENTS, RECVPORT);
		//m_socket = new UdpListeningReceiveSocket(
			//IpEndpointName(IpEndpointName::ANY_ADDRESS, RECVPORT), m_magicLeapListener);
}

void NetworkManagerOSC::activateControlCommandListener()
{
	if (m_configReaderIn->DEBUG_LEVEL >= NOT_IN_LOOPS)
	{
		std::cout << "In activateControlCommandListener" << std::endl;
	}
	m_startControlCommandListener = true;
	m_controlCommandListener = new ControlCommandListener();
	m_socketControlCommandListener = new UdpListeningReceiveSocket(IpEndpointName(IpEndpointName::ANY_ADDRESS, CONTROL_COMMAND_RECVPORT), m_controlCommandListener);
}

void NetworkManagerOSC::activateStatusUpdateListener()
{
	if (m_configReaderIn->DEBUG_LEVEL >= NOT_IN_LOOPS)
	{
		std::cout << "In activateStatusUpdateListener" << std::endl;
	}
	m_startStatusUpdateListener = true;
	m_statusUpdateListener = new StatusUpdateListener();
	m_socketStatusUpdateListener = new UdpListeningReceiveSocket(IpEndpointName(IpEndpointName::ANY_ADDRESS, STATUS_UPDATE_RECVPORT), m_statusUpdateListener);
}

void NetworkManagerOSC::activateSeventyFacialLandMarksOverOSCListener()
{
	if (m_configReaderIn->DEBUG_LEVEL >= NOT_IN_LOOPS)
	{
		std::cout << "In activat70FacialLandMarksOverOSCListener" << std::endl;
	}
	m_startSeventyFacialLandMarksOverOSCListener = true;
	m_seventyFacialLandMarksOverOSCListener = new SeventyFacialLandMarksOverOSCListener();
	m_socketSeventyFacialLandMarksOverOSCListener = new UdpListeningReceiveSocket(IpEndpointName(IpEndpointName::ANY_ADDRESS, LANDMARK70_RECVPORT), m_seventyFacialLandMarksOverOSCListener);
}



NetworkManagerOSC::~NetworkManagerOSC()
{
	if (m_startARCoreListener)
	{
		m_socket->Break();
	}

	if (m_startMagicLeapListener)
	{
		//m_socket->Break();
	}

	if (m_startControlCommandListener)
	{
		m_socketControlCommandListener->Break();
	}

	if (m_startStatusUpdateListener)
	{
		m_socketStatusUpdateListener->Break();
	}

	if (m_startSeventyFacialLandMarksOverOSCListener)
	{
		m_socketSeventyFacialLandMarksOverOSCListener->Break();
	}
}

void NetworkManagerOSC::setIpAddressForSRanipalEyeSender(std::string ip)
{
	m_sranipalEyeTrackingSenderToFacialLandmarksOscClientIP = ip;
}


void NetworkManagerOSC::runARCoreListener()
{
	m_socket->RunUntilSigInt();
}


void NetworkManagerOSC::runMagicLeapListener()
{
	//m_socket->RunUntilSigInt();
}


void NetworkManagerOSC::runControlCommandListener()
{
	m_socketControlCommandListener->RunUntilSigInt();
}


void NetworkManagerOSC::runStatusUpdateListener()
{
	m_socketStatusUpdateListener->RunUntilSigInt();
}


void NetworkManagerOSC::runSeventyFacialLandMarksOverOSCListener()
{
	m_socketSeventyFacialLandMarksOverOSCListener->RunUntilSigInt();
}


void NetworkManagerOSC::sendVertexArray(osg::Vec3Array* arrayIn)
{
	UdpTransmitSocket transmitSocket(IpEndpointName(ipAdress.c_str(), SENDPORT));

	char buffer[BUFFER];
	osc::OutboundPacketStream p(buffer, BUFFER);

	//std::cout << "arrayIn->getNumElements(): " << arrayIn->getNumElements() << std::endl;

	p << osc::BeginBundleImmediate;
	p << osc::BeginMessage("/v");
	for (unsigned int i = 0; i < arrayIn->getNumElements(); ++i)
	{
		p << arrayIn->at(i).x() << arrayIn->at(i).y() << arrayIn->at(i).z();
	}
	p << osc::EndMessage << osc::EndBundle;
	transmitSocket.Send(p.Data(), p.Size());
}

void NetworkManagerOSC::sendIndexArray(osg::DrawElementsUInt* arrayIn)
{
	UdpTransmitSocket transmitSocket(IpEndpointName(ipAdress.c_str(), SENDPORT));

	char buffer[BUFFER];
	osc::OutboundPacketStream p(buffer, BUFFER);

	//std::cout << "arrayIn->getNumElements(): " << arrayIn->getNumIndices() << std::endl;

	p << osc::BeginBundleImmediate;
	p << osc::BeginMessage("/i");
	for (unsigned int i = 0; i < arrayIn->getNumIndices(); ++i)
	{
		p << (int)arrayIn->index(i);
	}
	p << osc::EndMessage << osc::EndBundle;
	transmitSocket.Send(p.Data(), p.Size());
}

void NetworkManagerOSC::sendColorArray(osg::Vec4Array* arrayIn)
{
	UdpTransmitSocket transmitSocket(IpEndpointName(ipAdress.c_str(), SENDPORT));

	char buffer[BUFFER];
	osc::OutboundPacketStream p(buffer, BUFFER);

	//std::cout << "arrayIn->getNumElements(): " << arrayIn->getNumElements() << std::endl;

	p << osc::BeginBundleImmediate;
	p << osc::BeginMessage("/c");
	//std::cout << "Colors: " << arrayIn->getNumElements() << std::endl;
	for (unsigned int i = 0; i < arrayIn->getNumElements(); ++i)
	{
		p << arrayIn->at(i).x() << arrayIn->at(i).y() << arrayIn->at(i).z() << arrayIn->at(i).w();
	}
	p << osc::EndMessage << osc::EndBundle;
	transmitSocket.Send(p.Data(), p.Size());
}


void NetworkManagerOSC::sendNormalArray(osg::Vec3Array* arrayIn)
{
	UdpTransmitSocket transmitSocket(IpEndpointName(ipAdress.c_str(), SENDPORT));

	char buffer[BUFFER];
	osc::OutboundPacketStream p(buffer, BUFFER);

	//std::cout << "arrayIn->getNumElements(): " << arrayIn->getNumElements() << std::endl;

	p << osc::BeginBundleImmediate;
	p << osc::BeginMessage("/n");
	for (unsigned int i = 0; i < arrayIn->getNumElements(); ++i)
	{
		p << arrayIn->at(i).x() << arrayIn->at(i).y() << arrayIn->at(i).z();
	}
	p << osc::EndMessage << osc::EndBundle;
	transmitSocket.Send(p.Data(), p.Size());
}


osg::ref_ptr<osg::MatrixTransform> NetworkManagerOSC::getARCoreViewMatrix()
{
	return m_arCoreClayMoreOSCListener->getViewMatrixTransform();
}

std::vector<FacialLandmark> NetworkManagerOSC::getSeventyFacialLandmarks()
{
	return  m_seventyFacialLandMarksOverOSCListener->getLandmarks();
}


void NetworkManagerOSC::sendControlCommand(std::string commandName, std::string commandValue)
{
	std::cout << "sendControlCommand" << std::endl;
	UdpTransmitSocket transmitSocket(IpEndpointName(ipAdress.c_str(), CONTROL_COMMAND_SENDPORT));

	char buffer[BUFFER];
	osc::OutboundPacketStream p(buffer, BUFFER);

	//std::cout << "arrayIn->getNumElements(): " << arrayIn->getNumElements() << std::endl;

	p << osc::BeginBundleImmediate;
	p << osc::BeginMessage("/n");
	p << commandName.c_str();
	p << commandValue.c_str();

	//for (unsigned int i = 0; i < arrayIn->getNumElements(); ++i)
	//{
	//	p << arrayIn->at(i).x() << arrayIn->at(i).y() << arrayIn->at(i).z();
	//}
	p << osc::EndMessage << osc::EndBundle;
	transmitSocket.Send(p.Data(), p.Size());
}


void NetworkManagerOSC::sendStatusUpdate(std::string statusName, std::string statusValue)
{
	//UdpTransmitSocket transmitSocket(IpEndpointName(ipAdress.c_str(), SENDPORT));

	//char buffer[BUFFER];
	//osc::OutboundPacketStream p(buffer, BUFFER);

	////std::cout << "arrayIn->getNumElements(): " << arrayIn->getNumElements() << std::endl;

	//p << osc::BeginBundleImmediate;
	//p << osc::BeginMessage("/n");
	//for (unsigned int i = 0; i < arrayIn->getNumElements(); ++i)
	//{
	//	p << arrayIn->at(i).x() << arrayIn->at(i).y() << arrayIn->at(i).z();
	//}
	//p << osc::EndMessage << osc::EndBundle;
	//transmitSocket.Send(p.Data(), p.Size());
}

void NetworkManagerOSC::registerCallbackForControlCommandListener(void(*callback)(), std::string name)
{
	m_controlCommandListener->registerCallback(name, callback);
}

void NetworkManagerOSC::sendEyeTrackingData(float leftX,
	float leftY,
	float leftOpeness,
	float rightX,
	float rightY,
	float rightOpeness)
{
	//std::cout << "sendEyeTrackingData" << std::endl;
	UdpTransmitSocket transmitSocket(IpEndpointName(m_sranipalEyeTrackingSenderToFacialLandmarksOscClientIP.c_str(), EYE_TRACKING_SENDPORT));

	char buffer[BUFFER];
	osc::OutboundPacketStream p(buffer, BUFFER);

	//std::cout << "arrayIn->getNumElements(): " << arrayIn->getNumElements() << std::endl;

	//p << osc::BeginBundleImmediate;
	//p << osc::BeginMessage("/n");
	//p << leftX;
	//p << leftY;
	//p << leftZ;
	//p << leftOpeness;
	//p << rightX;
	//p << rightY;
	//p << rightZ;
	//p << rightOpeness;
	//p << osc::EndMessage << osc::EndBundle;
	//transmitSocket.Send(p.Data(), p.Size());


	float lm37X = 0.0f;
	float lm37Y = 0.0f;
	float lm38X = 0.0f;
	float lm38Y = 0.0f;
	float lm39X = 0.0f;
	float lm39Y = 0.0f;
	float lm40X = 0.0f;
	float lm40Y = 0.0f;
	float lm41X = 0.0f;
	float lm41Y = 0.0f;
	float lm42X = 0.0f;
	float lm42Y = 0.0f;
	float lm43X = 0.0f;
	float lm43Y = 0.0f;
	float lm44X = 0.0f;
	float lm44Y = 0.0f;
	float lm45X = 0.0f;
	float lm45Y = 0.0f;
	float lm46X = 0.0f;
	float lm46Y = 0.0f;
	float lm47X = 0.0f;
	float lm47Y = 0.0f;
	float lm48X = 0.0f;
	float lm48Y = 0.0f;

	p << osc::BeginBundleImmediate;
	p << osc::BeginMessage("/faciallandmarks/eyes");
	p << leftX; //0
	p << leftY;
	p << leftOpeness; //2
	p << rightX;
	p << rightY;	//4
	p << rightOpeness;
	p << osc::EndMessage << osc::EndBundle;
	transmitSocket.Send(p.Data(), p.Size());
}

//void NetworkManagerOSC::registerCallbackForSeventyFacialLandMarksOverOSCListener(void(*callback)(), std::string name)
//{
//	m_seventyFacialLandMarksOverOSCListener->registerCallback(name, callback);
//}
