// ClayMore - Immersive Mesh Modelling --- Copyright (c) 2014-2017 Philipp Ladwig, Jannik Fiedler, Jan Beutgen
#pragma once

#include "ClayMoreOSCListener.h"
#include "cmUtil/Landmark.h"
#include <vector>
#include <mutex>

class SeventyFacialLandMarksOverOSCListener : public osc::OscPacketListener
{
public:
	SeventyFacialLandMarksOverOSCListener();
	~SeventyFacialLandMarksOverOSCListener(void);
	std::vector<FacialLandmark> getLandmarks();

protected:
	void ProcessMessage(const osc::ReceivedMessage& message, const IpEndpointName& remoteEndpoint);

private:
	void processSensorValuesMessage(const osc::ReceivedMessage& message);
	void processLandmarksMessage(const osc::ReceivedMessage& message);

	const int m_numberOfLandmarks = 70;
	std::vector<FacialLandmark> m_landmarks;
	std::mutex m_landmarksMutex;
};