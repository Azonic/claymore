// ClayMore - Immersive Mesh Modelling --- Copyright (c) 2014-2017 Philipp Ladwig, Jannik Fiedler, Jan Beutgen
#ifndef cmNetworkManagementOSC_EXPORTS
	#define imNetworkManagementOSC_DLL_import_export __declspec(dllimport)
#else
	#define imNetworkManagementOSC_DLL_import_export __declspec(dllexport)
#endif