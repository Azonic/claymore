#include "ARCoreClayMoreOSCListener.h"

#include <iostream>
#include <cstring>

#include "defines.h"

#include "osc/OscPacketListener.h"
#include "ip/UdpSocket.h"

ARCoreClayMoreOSCListener::ARCoreClayMoreOSCListener()
{
	m_viewMatrixTransform = new osg::MatrixTransform();
}

ARCoreClayMoreOSCListener::~ARCoreClayMoreOSCListener()
{
	//TODO: Close thread and connection
}

void ARCoreClayMoreOSCListener::ProcessMessage(const osc::ReceivedMessage& m, const IpEndpointName& remoteEndpoint)
{
	//The following code is based on SimpleReceive from oscpack
	try {
		// example of parsing single messages. osc::OsckPacketListener
		// handles the bundle traversal.

		if (std::strcmp(m.AddressPattern(), "/camera/time/1") == 0) {
			// example #1 -- argument stream interface
			osc::ReceivedMessageArgumentStream args = m.ArgumentStream();
			__int64 time;
			args >> time >> osc::EndMessage;
			//std::cout << "received '/camera/time/1' message with arguments: " << time << "\n";
		}
		else if (std::strcmp(m.AddressPattern(), "/camera/pos") == 0) {
			// example #2 -- argument iterator interface, supports
			// reflection for overloaded messages (eg you can call 
			// (*arg)->IsBool() to check if a bool was passed etc).
			osc::ReceivedMessage::const_iterator arg = m.ArgumentsBegin();
			float posX = (arg++)->AsFloat();
			float posY = (arg++)->AsFloat();
			float posZ = (arg++)->AsFloat();
			//const char *a4 = (arg++)->AsString();
			if (arg != m.ArgumentsEnd())
				throw osc::ExcessArgumentException();
			m_posMatrix.makeTranslate(posX, posY, posZ);
			//std::cout << "received '/camera/pos' message with arguments: " << posX << " " << posY << " " << posZ << " " << "\n";
		}
		else if (std::strcmp(m.AddressPattern(), "/camera/rot") == 0) {
			// example #2 -- argument iterator interface, supports
			// reflection for overloaded messages (eg you can call 
			// (*arg)->IsBool() to check if a bool was passed etc).
			osc::ReceivedMessage::const_iterator arg = m.ArgumentsBegin();
			float quat0 = (arg++)->AsFloat();
			float quat1 = (arg++)->AsFloat();
			float quat2 = (arg++)->AsFloat();
			float quat3 = (arg++)->AsFloat();

			if (arg != m.ArgumentsEnd())
				throw osc::ExcessArgumentException();
			osg::Quat rotation(quat0, -quat1, -quat2, quat3);
			m_rotMatrix.makeRotate(rotation);
			//std::cout << "quat0  " << quat0 << "\n";
			//std::cout << "quat1  " << quat1 << "\n";
			//std::cout << "quat2  " << quat2 << "\n";
			//std::cout << "quat3  " << quat3 << "\n";
			//std::cout << "----------------" << std::endl;
			//m_rotMatrix.makeRotate(quat3, quat2, quat1, quat0);
			//std::cout << "received '/camera/rot' message with arguments: " << quat0 << " " << quat1 << " " << quat2 << " " << quat3 << "\n";
		}
		else if (std::strcmp(m.AddressPattern(), "/camera/time/2") == 0) {
			// example #1 -- argument stream interface
			osc::ReceivedMessageArgumentStream args = m.ArgumentStream();
			__int64 time;
			args >> time >> osc::EndMessage;
			//std::cout << "received '/camera/time/2' message with arguments: " << time << "\n";
		}
		osg::Matrix arCoreViewMatrix;
		arCoreViewMatrix.makeIdentity();
		arCoreViewMatrix = m_rotMatrix * m_posMatrix;
		m_viewMatrixTransform->setMatrix(arCoreViewMatrix);
		//MAKE SURE THAT m_ARCoreTracking is true in the Initialization List of PointCloudVisulizer.cpp
	}
	catch (osc::Exception& e) {
		// any parsing errors such as unexpected argument types, or 
		// missing arguments get thrown as exceptions.
		std::cout << "error while parsing message: " << m.AddressPattern() << ": " << e.what() << "\n";
	}
}
