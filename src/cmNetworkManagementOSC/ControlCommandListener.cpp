 #include "ControlCommandListener.h"

 //#include <iostream>
 //#include <cstring>

 #include "defines.h"

#include "osc/OscPacketListener.h"
#include "ip/UdpSocket.h"
#include <sstream>

ControlCommandListener::ControlCommandListener()
 {

 }

ControlCommandListener::~ControlCommandListener()
 {
	//TODO: Close thread and connection
 }

void ControlCommandListener::registerCallback(std::string commandName, void (*callback)())
{
	m_callbackPool.insert(std::pair<std::string, void(*)()>(commandName, callback));
}

void ControlCommandListener::ProcessMessage(const osc::ReceivedMessage& m, const IpEndpointName& remoteEndpoint)
 {
	osc::ReceivedMessage::const_iterator arg = m.ArgumentsBegin();
	m_callbackPool[arg->AsString()]();

	std::cout << arg->AsString() << " <-received " << std::endl;
	////The following code is based on SimpleReceive from oscpack
	// try {
	//	//example of parsing single messages. osc::OsckPacketListener
	//	//handles the bundle traversal.

	//	 if (std::strcmp(m.AddressPattern(), "/ml/eyegaze") == 0) {
	//		//example #1 -- argument stream interface
	//		 //osc::ReceivedMessageArgumentStream args = m.ArgumentStream();
	//		 //__int64 time;
	//		 //args >> time >> osc::EndMessage;
	//		std::cout << "Incomming EyeGaze Data" << time << "\n";
	//	 }
	// }
	// catch (osc::Exception& e) {
	//	//any parsing errors such as unexpected argument types, or 
	//	//missing arguments get thrown as exceptions.
	//	 std::cout << "error while parsing message: " << m.AddressPattern() << ": " << e.what() << "\n";
	// }
 }
