 // ClayMore - Immersive Mesh Modelling --- Copyright (c) 2014-2017 Philipp Ladwig, Jannik Fiedler, Jan Beutgen
 #pragma once

 #include "ClayMoreOSCListener.h"
#include <map>
#include <string>

 class ControlCommandListener : public osc::OscPacketListener
 {
	 // #### CONSTRUCTOR & DESTRUCTOR ###############
 public:
	 ControlCommandListener();
	 ~ControlCommandListener(void);

	 // #### MEMBER VARIABLES ###############
 private:
	 std::string m_commandName;
	 std::string m_commandValue;
     std::map<std::string, void(*)()> m_callbackPool;

	 // #### FUNCTIONS ###############
 public:
	 //inline osg::ref_ptr<osg::MatrixTransform> getViewMatrixTransform() { return m_viewMatrixTransform; }
     void registerCallback(std::string commandName, void(*callback)());

 protected:
	 void ProcessMessage(const osc::ReceivedMessage& m, const IpEndpointName& remoteEndpoint);
     
 };