#include "SeventyFacialLandMarksOverOSCListener.h"

//#include <iostream>
//#include <cstring>

#include "defines.h"

#include "osc/OscPacketListener.h"
#include "ip/UdpSocket.h"

SeventyFacialLandMarksOverOSCListener::SeventyFacialLandMarksOverOSCListener()
{
	std::cout << "SeventyFacialLandMarksOverOSCListener" << std::endl;
}

SeventyFacialLandMarksOverOSCListener::~SeventyFacialLandMarksOverOSCListener()
{
	//TODO: Close thread and connection
}

std::vector<FacialLandmark> SeventyFacialLandMarksOverOSCListener::getLandmarks()
{
	const std::lock_guard<std::mutex> lock(m_landmarksMutex);
	return m_landmarks;
}

void SeventyFacialLandMarksOverOSCListener::ProcessMessage(const osc::ReceivedMessage& message, const IpEndpointName& remoteEndpoint)
{
	try {
		if (std::strcmp(message.AddressPattern(), "/OSC/SensorValues") == 0) {
			processSensorValuesMessage(message);
		}
		else if (std::strcmp(message.AddressPattern(), "/landmarks") == 0) {
			processLandmarksMessage(message);
		}
	} catch (osc::Exception& e) {
		std::cout << "error while parsing message: " << message.AddressPattern() << ": " << e.what() << "\n";
	}
}

void SeventyFacialLandMarksOverOSCListener::processSensorValuesMessage(const osc::ReceivedMessage& message)
{
	if (m_landmarks.size() > 0) {
		//std::cout << "/OSC/SensorValues received" << std::endl;
		osc::ReceivedMessage::const_iterator arg = message.ArgumentsBegin();
		//EyeBrowTracking via pressure sensors in foam
		float scaleFactor = 600;
		for (int i = 0; i < message.ArgumentCount(); i++)
		{
			std::vector<int> point;
			if (i == 2) {
				float value = 200 - (int)(arg++)->AsInt32();
				m_landmarks.at(17).y = 23 - (value / scaleFactor);
				m_landmarks.at(18).y = 7 - (value / scaleFactor);
				m_landmarks.at(19).y = 3 - (value / scaleFactor);
				m_landmarks.at(20).y = 3 - (value / scaleFactor);
				m_landmarks.at(21).y = 7 - (value / scaleFactor);
			}
			else if (i == 1) {
				float value = 1000 - (int)(arg++)->AsInt32();
				m_landmarks.at(22).y = 7 - (value / scaleFactor);
				m_landmarks.at(23).y = 3 - (value / scaleFactor);
				m_landmarks.at(24).y = 0 - (value / scaleFactor);
				m_landmarks.at(25).y = 7 - (value / scaleFactor);
				m_landmarks.at(26).y = 23 - (value / scaleFactor);
			}
			else {
				arg++;
			}

		}
		if (arg != message.ArgumentsEnd())
			throw osc::ExcessArgumentException();
	}
}

void SeventyFacialLandMarksOverOSCListener::processLandmarksMessage(const osc::ReceivedMessage& message)
{
	//std::cout << "processLandmarksMessage" << std::endl;
	const std::lock_guard<std::mutex> lock(m_landmarksMutex);
	osc::ReceivedMessage::const_iterator arg = message.ArgumentsBegin();

	if (m_landmarks.size() != m_numberOfLandmarks)
		m_landmarks.resize(m_numberOfLandmarks);

	for (int i = 0; i < m_numberOfLandmarks; i++)
	{
		//Rene's Pipeline
		//m_landmarks.at(i).x = (arg++)->AsFloat();
		//m_landmarks.at(i).y = (arg++)->AsFloat();

		//UCP's Pipeline
		m_landmarks.at(i).x = (arg++)->AsInt32();
		m_landmarks.at(i).y = (arg++)->AsInt32();
	}

	/*for (int i = 0; i < m_landmarks.size(); i++) {
		std::cout << i;
		std::cout << " x " << m_landmarks.at(i).x;
		std::cout << " y " << m_landmarks.at(i).y;
		std::cout << std::endl;
	}*/

	if (arg != message.ArgumentsEnd())
		throw osc::ExcessArgumentException();
}