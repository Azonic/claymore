// ClayMore - Immersive Mesh Modelling --- Copyright (c) 2014-2017 Philipp Ladwig, Jannik Fiedler, Jan Beutgen
#include "GeometryMainProcessor.h"

#include "UtilProcessor.h"
#include "GrabRotateScaleProcessor.h"
#include "MeshHistoryProcessor.h"
#include "DeleteProcessor.h"
#include "ExtrudeProcessor.h"
#include "SubdivideProcessor.h"
#include "ImportProcessor.h"
#include "ExportProcessor.h"
#include "SculptProcessor.h"
#include "JoinGeometriesProcessor.h"
#include "DuplicateProcessor.h"

#include <iostream>

GeometryMainProcessor::GeometryMainProcessor()
{
	m_utilProc = new UtilProcessor();
	addProcessor(ProcessorType::UTIL, m_utilProc);

	m_grabRotateScaleProc = new GrabRotateScaleProcessor(m_utilProc);
	addProcessor(ProcessorType::GRAB_ROT_SCALE, m_grabRotateScaleProc);

	m_meshHistoryProc = new MeshHistoryProcessor(m_utilProc);
	addProcessor(ProcessorType::HISTORY, m_meshHistoryProc);

	m_deleteProc = new DeleteProcessor(m_utilProc);
	addProcessor(ProcessorType::DELETE, m_deleteProc);

	m_extrudeProc = new ExtrudeProcessor(m_utilProc);
	addProcessor(ProcessorType::EXTRUDE, m_extrudeProc);

	m_subdivideProc = new SubdivideProcessor(m_utilProc);
	addProcessor(ProcessorType::SUBDIVIDE, m_subdivideProc);

	m_importProc = new ImportProcessor(m_utilProc);
	addProcessor(ProcessorType::IMPORT, m_importProc);

	m_exportProc = new ExportProcessor();
	addProcessor(ProcessorType::EXPORT, m_exportProc);

	m_sculptProc = new SculptProcessor(m_utilProc);
	addProcessor(ProcessorType::SCULPT, m_sculptProc);

	m_joinGeometriesProc = new JoinGeometriesProcessor(m_utilProc);
	addProcessor(ProcessorType::JOINGEOMETRIES, m_joinGeometriesProc);
	 
	m_duplicateProc = new DuplicateProcessor(m_utilProc);
	addProcessor(ProcessorType::DUPLICATE, m_duplicateProc);
}

GeometryMainProcessor::~GeometryMainProcessor()
{

}

void GeometryMainProcessor::addProcessor(ProcessorType type, Processor* processor)
{
	//What are the difference?:
	//m_processorMap[type] = processor;

	//And:
	m_processorMap.insert(std::pair<ProcessorType, Processor*>(type, processor));
}

//Update the activeGeometry for every processor
void GeometryMainProcessor::setActiveGeometry(osg::ref_ptr<osg::OpenMeshGeometry> omGeom)
{ 
	m_omGeom = omGeom; 

	std::map<ProcessorType, Processor*>::iterator it;

	for (it = m_processorMap.begin(); it != m_processorMap.end(); ++it)
	{
		it->second->setActiveGeometry(omGeom);
	}
}

//UTIL ##############################################################################
osg::Vec3d GeometryMainProcessor::getBarycentrumOfSELECTEDVerts()
{
	//Complex way of access to a processor:
	//std::map<ProcessorType, Processor*>::iterator it;
	//it = m_processorMap.find(ProcessorType::UTIL);
	//dynamic_cast<UtilProcessor*>(it->second)

	//Easy way:
	return m_utilProc->getBarycentrumOfSELECTEDVerts();
}

osg::Vec3d GeometryMainProcessor::getBarycentrumOfALLVerts()
{
	//Complex way of access to a processor:
	//std::map<ProcessorType, Processor*>::iterator it;
	//it = m_processorMap.find(ProcessorType::UTIL);
	//dynamic_cast<UtilProcessor*>(it->second)

	//Easy way:
	return m_utilProc->getBarycentrumOfALLVerts();
}

void GeometryMainProcessor::calcNormals()
{
	m_utilProc->calcNormals();
}

void GeometryMainProcessor::sync_indices(size_t face, size_t v1, size_t v2, size_t v3)
{
	m_utilProc->sync_indices(face, v1, v2, v3);

}

float GeometryMainProcessor::calculateDistance(OpenMesh::OSG_TriMesh_BindableArrayKernelT::VertexIter vIt,
	OpenMesh::OSG_TriMesh_BindableArrayKernelT::VertexHandle vh)
{
	return m_utilProc->calculateDistance(vIt, vh);
}

float GeometryMainProcessor::calculateDistance(OpenMesh::OSG_TriMesh_BindableArrayKernelT::VertexIter vIt, osg::Vec3f positionIn)
{
	return m_utilProc->calculateDistance(vIt, positionIn);
}

float GeometryMainProcessor::calculateDistance(osg::Vec3f position0, osg::Vec3f position1)
{
	return m_utilProc->calculateDistance(position0, position1);
}

void GeometryMainProcessor::addFaceBasedOnSelectedVerts()
{
	return m_utilProc->addFaceBasedOnSelectedVerts();
}


//GRAB_SCALE_ROT ##############################################################################
void GeometryMainProcessor::grabVerticesStart(osg::Vec3f grabStartPositionIn)
{
	m_grabRotateScaleProc->grabVerticesStart(grabStartPositionIn);
	//m_meshHistoryProc->copyAndStoreVertexArray();
	//m_meshHistoryProc->copyAndStoreIndiciesArray();
	//m_meshHistoryProc->copyAndStoreColorArray();
	m_meshHistoryProc->storeMeshDataAsHistory();
}

//NOTE: Good Function to test runtimes of different solutions with 126000verts monkey in debug:
void GeometryMainProcessor::grabVertices(osg::Vec3f newPositionIn, bool isLocalCoordSysSelectedIn, bool isSnappingActiveIn, Constraints constraintIn, osg::Matrix matrixWorldToLocal)
{
	m_grabRotateScaleProc->grabVertices(newPositionIn,
		isLocalCoordSysSelectedIn,
		isSnappingActiveIn,
		constraintIn,
		matrixWorldToLocal,
		m_meshHistoryProc->getCurrentVertexArray());
}

void GeometryMainProcessor::grabPropEditStart(osg::Vec3f grabStartPosition, float radius, float shape)
{
	m_grabRotateScaleProc->grabPropEditStart(grabStartPosition, radius, shape);
	m_meshHistoryProc->copyAndStoreVertexArray();
	m_meshHistoryProc->copyAndStoreIndiciesArray();
	m_meshHistoryProc->copyAndStoreColorArray();
}

void GeometryMainProcessor::grabPropEdit(osg::Vec3f newPositionIn,	bool isLocalCoordSysSelectedIn, Constraints constraintIn, osg::Matrix matrixWorldToLocal)
{
	m_grabRotateScaleProc->grabPropEdit(newPositionIn,
		isLocalCoordSysSelectedIn,
		constraintIn,
		matrixWorldToLocal,
		m_meshHistoryProc->getCurrentVertexArray());
}

void GeometryMainProcessor::grabProbEditSetValues(float radius, float shape)
{
	m_grabRotateScaleProc->grabProbEditSetValues(radius, shape);
}

void GeometryMainProcessor::rotateVerticesStart()
{
	//Store original geometry
	//m_meshHistoryProc->copyAndStoreVertexArray();
	//m_meshHistoryProc->copyAndStoreIndiciesArray();
	//m_meshHistoryProc->copyAndStoreColorArray();
	m_meshHistoryProc->storeMeshDataAsHistory();
}

void GeometryMainProcessor::rotateVertices(osg::Matrixd rotMatIn, bool isSnappingActiveIn, Constraints constraintIn, osg::Vec3d barycentrum)
{
	m_grabRotateScaleProc->rotateVertices(rotMatIn, isSnappingActiveIn, constraintIn, barycentrum, m_meshHistoryProc->getCurrentVertexArray());
}

void GeometryMainProcessor::rotateEntireGeometry(osg::Matrixd rotMatIn, osg::Vec3d barycentrum)
{
	m_grabRotateScaleProc->rotateEntireGeometry(rotMatIn, m_meshHistoryProc->getCurrentVertexArray(), barycentrum);
}

void GeometryMainProcessor::scaleVerticesStart()
{
	//Store original geometry
	//m_meshHistoryProc->copyAndStoreVertexArray();
	//m_meshHistoryProc->copyAndStoreIndiciesArray();
	//m_meshHistoryProc->copyAndStoreColorArray();
	m_meshHistoryProc->storeMeshDataAsHistory();
}

void GeometryMainProcessor::scaleVertices(float scaleValueIn, bool isLocalIn, bool isSnappingActiveIn, Constraints constraintIn, osg::Matrix matrixWorldToLocalIn, osg::Matrix matrixLocalToWorldIn, osg::Vec3f barycentrum)
{
	m_grabRotateScaleProc->scaleVertices(scaleValueIn, isLocalIn, isSnappingActiveIn, constraintIn, matrixWorldToLocalIn, matrixLocalToWorldIn, barycentrum);
}

void GeometryMainProcessor::resetScaleGeometry()
{
	m_grabRotateScaleProc->resetScaleGeometry(m_meshHistoryProc->getCurrentVertexArray());
}

void GeometryMainProcessor::scaleEntireGeometryBasedOnBaryOfALLVerts(float scaleValueIn)
{
	m_grabRotateScaleProc->scaleEntireGeometryBasedOnBaryOfALLVerts(scaleValueIn, m_meshHistoryProc->getCurrentVertexArray());
}

void GeometryMainProcessor::scaleEntireGeometryBasedOnBaryOfSELECTEDVerts(float scaleValueIn)
{
	m_grabRotateScaleProc->scaleEntireGeometryBasedOnBaryOfSELECTEDVerts(scaleValueIn, m_meshHistoryProc->getCurrentVertexArray());
}


//DELETE ##############################################################################
void GeometryMainProcessor::deleteSelectedMeshElements(bool verticesIn, bool edgesIn, bool facesIn)
{
	m_deleteProc->deleteSelectedMeshElements(verticesIn, edgesIn, facesIn);
}

//EXTRUDE ##############################################################################
void GeometryMainProcessor::extrude()
{
	//m_extrudeProc->extrude();
	std::cout << "MultiThread!" << std::endl;
	std::thread thread0(&ExtrudeProcessor::extrude, m_extrudeProc);
	thread0.detach();
}

void GeometryMainProcessor::subdivide()
{
	std::cout << "MultiThread!" << std::endl;
	std::thread thread0(&SubdivideProcessor::subdivide, m_subdivideProc);
	thread0.detach();
}


//HISTORY ##############################################################################
void GeometryMainProcessor::cancelAnyTransformation()
{
	m_meshHistoryProc->cancelAnyTransformation();
}

void GeometryMainProcessor::storeMeshDataAsHistory()
{
	m_meshHistoryProc->storeMeshDataAsHistory();
}

void GeometryMainProcessor::undo()
{
	m_meshHistoryProc->undo();
}

//IMPORTER ##############################################################################
void GeometryMainProcessor::readFileFromDisk(std::string readPath)
{
	//TODO: Trying multi thread execution, but no really success
	//There are std::thread::join() and std::thread::detach()
	//Join: The main thread will wait until the "child" thread has finished -> senseless here
	//Detach: The main thread creates new independant threat and wont wait until the child is finished.
	//Problem with detach: When child is finished, all allocated data will be deleted... soo all what OpenMesh
	//	allocates in read_mesh() will be deleted I think...
	//Maybe the better way is to start from the beginning an absolute independent geometry thread from the render thread. 
	//Render thread will be the main thread and the geometry thread will be not killed until ClayMore is quitted
	//std::cout << "MultiThread!" << std::endl;
	//Even with this no success:
	//m_omGeom->getMesh()->resize(1000000, 1000000, 1000000);
	//m_omGeom->getVertexArray()->reserveArray(1000000);
	//m_omGeom->getVertexArray()->resizeArray(1000000);
	//std::thread thread(&ImportProcessor::readFileFromDisk, m_importProc, readPath);
	//thread.detach();

	//Single thread execution
	m_importProc->readFileFromDisk(readPath);
}

//EXPORTER ##############################################################################
void GeometryMainProcessor::writeFileToDisk(std::string savePath)
{
	m_exportProc->writeFileToDisk(savePath);
}

//SCULPT ##############################################################################
void GeometryMainProcessor::sculptStart(float radius, float strength)
{
	//m_meshHistoryProc->copyAndStoreVertexArray();
	m_meshHistoryProc->storeMeshDataAsHistory();
	//m_meshHistoryProc->copyAndStoreIndiciesArray();
	//m_meshHistoryProc->copyAndStoreColorArray();
	m_sculptProc->sculptStart(radius, strength);
}


void GeometryMainProcessor::sculptAddOrSubtract(float radius, float strength, osg::Vec3 cursorTrans, osg::Matrix geomLocal2World, bool isAddBrush)
{
	m_sculptProc->sculptAddOrSubtract(radius, strength, cursorTrans, geomLocal2World, isAddBrush, m_meshHistoryProc->getCurrentVertexArray());
}

void GeometryMainProcessor::joinGeometries(osg::ref_ptr<osg::OpenMeshGeometry> geometryToJoin)
{
	m_joinGeometriesProc->joinGeometries(geometryToJoin);
}

void GeometryMainProcessor::duplicate()
{
	m_duplicateProc->duplicate();
}
