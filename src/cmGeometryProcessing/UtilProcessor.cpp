// ClayMore - Immersive Mesh Modelling --- Copyright (c) 2014-2017 Philipp Ladwig, Jannik Fiedler, Jan Beutgen
#include "UtilProcessor.h"

//TODO: Remove this include later:
#include <iostream>


UtilProcessor::UtilProcessor()
{

}

UtilProcessor::~UtilProcessor()
{

}

osg::Vec3d UtilProcessor::getBarycentrumOfALLVerts()
{
	Mesh::VertexIter vIt, vBegin, vEnd;

	vBegin = m_omGeom->getMesh()->vertices_begin();
	vEnd = m_omGeom->getMesh()->vertices_end();

	unsigned int sumOfSelectedVertices = m_omGeom->getMesh()->n_vertices();
	double xSum = 0.0;
	double ySum = 0.0;
	double zSum = 0.0;

	for (vIt = vBegin; vIt != vEnd; ++vIt)
	{
		xSum += m_omGeom->getMesh()->point(*vIt).x();
		ySum += m_omGeom->getMesh()->point(*vIt).y();
		zSum += m_omGeom->getMesh()->point(*vIt).z();
	}

	xSum = xSum / sumOfSelectedVertices;
	ySum = ySum / sumOfSelectedVertices;
	zSum = zSum / sumOfSelectedVertices;

	return osg::Vec3d(xSum, ySum, zSum);
}

osg::Vec3d UtilProcessor::getBarycentrumOfSELECTEDVerts()
{
	Mesh::VertexIter vIt, vBegin, vEnd;

	vBegin = m_omGeom->getMesh()->vertices_begin();
	vEnd = m_omGeom->getMesh()->vertices_end();

	unsigned int i = 0;
	unsigned int sumOfSelectedVertices = 0;
	double xSum = 0.0;
	double ySum = 0.0;
	double zSum = 0.0;

	for (vIt = vBegin; vIt != vEnd; ++vIt)
	{
		//if (selectedVerticesIn.at(i))
		if (m_omGeom->getMesh()->status(*vIt).selected())
		{
			xSum += m_omGeom->getMesh()->point(*vIt).x();
			ySum += m_omGeom->getMesh()->point(*vIt).y();
			zSum += m_omGeom->getMesh()->point(*vIt).z();
			sumOfSelectedVertices++;
		}
		i++;
	}
	if (sumOfSelectedVertices == 0)
	{
		xSum = 0;
		ySum = 0;
		zSum = 0;
	}
	else {
		xSum = xSum / sumOfSelectedVertices;
		ySum = ySum / sumOfSelectedVertices;
		zSum = zSum / sumOfSelectedVertices;
	}

	return osg::Vec3d(xSum, ySum, zSum);
}

float UtilProcessor::calculateDistance(OpenMesh::OSG_TriMesh_BindableArrayKernelT::VertexIter vIt, OpenMesh::OSG_TriMesh_BindableArrayKernelT::VertexHandle vh)
{
	float squareDistanceX = pow(m_omGeom->getMesh()->point(*vIt).x() - m_omGeom->getMesh()->point(vh).x(), 2);
	float squareDistanceY = pow(m_omGeom->getMesh()->point(*vIt).y() - m_omGeom->getMesh()->point(vh).y(), 2);
	float squareDistanceZ = pow(m_omGeom->getMesh()->point(*vIt).z() - m_omGeom->getMesh()->point(vh).z(), 2);

	float distance = sqrt(squareDistanceX + squareDistanceY + squareDistanceZ);

	return distance;
}

float UtilProcessor::calculateDistance(OpenMesh::OSG_TriMesh_BindableArrayKernelT::VertexIter vIt, osg::Vec3f positionIn)
{
	//TODO: Could be possible, that positionIn must be multiplied with world2local Matrix
	float squareDistanceX = pow(m_omGeom->getMesh()->point(*vIt).x() - positionIn.x(), 2);
	float squareDistanceY = pow(m_omGeom->getMesh()->point(*vIt).y() - positionIn.y(), 2);
	float squareDistanceZ = pow(m_omGeom->getMesh()->point(*vIt).z() - positionIn.z(), 2);

	float distance = sqrt(squareDistanceX + squareDistanceY + squareDistanceZ);

	return distance;
}

float UtilProcessor::calculateDistance(osg::Vec3f position0, osg::Vec3f position1)
{
	float squareDistanceX = pow(position0.x() - position1.x(), 2);
	float squareDistanceY = pow(position0.y() - position1.y(), 2);
	float squareDistanceZ = pow(position0.z() - position1.z(), 2);

	//TODO: Could it be possible, to avoid the sqrt?
	float distance = sqrt(squareDistanceX + squareDistanceY + squareDistanceZ);

	return distance;
}

void UtilProcessor::cleanUpIndicies()
{
	std::vector<OpenMesh::VertexHandle> vhandles;
	unsigned int idx;
	for (size_t i = 0, nF = m_omGeom->getMesh()->n_faces(); i < nF; ++i)
	{
		vhandles.clear();
		for (Mesh::CFVIter fv_it = m_omGeom->getMesh()->cfv_iter(OpenMesh::FaceHandle(unsigned int(i))); fv_it.is_valid(); ++fv_it)
		{
			vhandles.push_back(*fv_it);
		}

		std::vector<unsigned int> indiciesCleanedUp;
		for (size_t j = 0; j < vhandles.size(); ++j)
		{
			idx = vhandles[j].idx(); //+ 1;
			indiciesCleanedUp.push_back(idx);
		}
		//TODO2: Look in 	
		//m_omGeom->getMesh()->sync_indices
		//What ist that? My class or made by RWTH?
		sync_indices((unsigned int)i, indiciesCleanedUp[0], indiciesCleanedUp[1], indiciesCleanedUp[2]);
	}

	//m_omGeom->getMesh()->update_normals();
	m_omGeom->getNormalArray()->dirty();
	m_omGeom->getVertexArray()->dirty();
	m_omGeom->getPrimitiveSet(0)->dirty();
}

// set the indices of face to (v1,v2,v3)
void UtilProcessor::sync_indices(size_t face, size_t v1, size_t v2, size_t v3)
{
	OpenMesh::FPropHandleT<GLuint> indicesPropH;
	m_omGeom->getMesh()->get_property_handle(indicesPropH, "indices");
	OpenMesh::PropertyT<GLuint> indices = m_omGeom->getMesh()->property(indicesPropH);

	indices[3 * face] = v1;
	indices[3 * face + 1] = v2;
	indices[3 * face + 2] = v3;
}

void UtilProcessor::calcNormals()
{
	//What are exactly the difference to?:
	//mesh.update_face_normals();

	Mesh::FaceIter  fIt, fBegin, fEnd;
	fBegin = m_omGeom->getMesh()->faces_begin();
	fEnd = m_omGeom->getMesh()->faces_end();
	int i = 0;
	for (fIt = fBegin; fIt != fEnd; ++fIt)
	{
		Mesh::FaceVertexIter fvIter = m_omGeom->getMesh()->fv_iter(*fIt);
		Mesh::FaceVertexIter fvEnd = m_omGeom->getMesh()->fv_end(*fIt);
		for (; fvIter != fvEnd; ++fvIter)
			//Old Version:
			//Mesh::FaceVertexIter fvIter = m_omGeom->getMesh()->fv_iter(fIt);
			//for (; fvIter; ++fvIter)
		{
			m_omGeom->getMesh()->set_normal(*fvIter, m_omGeom->getMesh()->calc_face_normal(*fIt));
		}
	}
	m_omGeom->getNormalArray()->dirty();
}

//Uses brute force face adding
void UtilProcessor::addFaceBasedOnSelectedVerts()
{
	Mesh::VertexIter vIt, vBegin, vEnd;
	vBegin = m_omGeom->getMesh()->vertices_begin();
	vEnd = m_omGeom->getMesh()->vertices_end();

	osg::Vec4Array *colorArray = dynamic_cast<osg::Vec4Array*>(m_omGeom->getColorArray());

	colorArray->dirty();

	std::vector<Mesh::VertexHandle> faceIndicies;
	for (vIt = vBegin; vIt != vEnd; ++vIt)
	{
		if (m_omGeom->getMesh()->status(*vIt).selected())
		{
			faceIndicies.push_back(*vIt);
		}
	}
	std::cout << "faceIndicies Anzahl: " << faceIndicies.size() << std::endl;

	bruteForceAddFace(faceIndicies.at(0), faceIndicies.at(1), faceIndicies.at(2));
	cleanUpIndicies();
}

void UtilProcessor::bruteForceAddFace(Mesh::VertexHandle v0, Mesh::VertexHandle v1, Mesh::VertexHandle v2, short iterationIn)
{
	std::cout << "brute force face adding starts..." << std::endl;
	//Mesh::FaceHandle fh = m_omGeom->getMesh()->add_face(v0, v1, v2);

	std::vector<OpenMesh::VertexHandle> vertexHandles;
	vertexHandles.push_back(v0);
	vertexHandles.push_back(v1);
	vertexHandles.push_back(v2);
	Mesh::FaceHandle fh = m_omGeom->getMesh()->add_face(vertexHandles);
	OpenMesh::VertexHandle vh;
	//Mesh::FaceHandle fh = m_omGeom->getMesh()->add_face(;
	if (!fh.is_valid())
	{
		std::cout << "FAIL1" << std::endl;
		fh = m_omGeom->getMesh()->add_face(v2, v1, v0);
		if (!fh.is_valid())
		{
			std::cout << "FAIL2" << std::endl;
			fh = m_omGeom->getMesh()->add_face(v2, v0, v1);
			if (!fh.is_valid())
			{
				std::cout << "FAIL3" << std::endl;
				fh = m_omGeom->getMesh()->add_face(v0, v2, v1);
				if (!fh.is_valid())
				{
					std::cout << "FAIL4" << std::endl;
					fh = m_omGeom->getMesh()->add_face(v1, v2, v0);
					if (!fh.is_valid())
					{
						std::cout << "FAIL5" << std::endl;
						fh = m_omGeom->getMesh()->add_face(v1, v0, v2);
						if (!fh.is_valid())
						{
							std::cout << "FAIL6" << std::endl;
							if (iterationIn == 0)
							{
								vh = m_omGeom->getMesh()->add_vertex(m_omGeom->getMesh()->point(v0));
								if (!vh.is_valid())
								{
									std::cout << "VERTEX Fail" << std::endl;
								}
								bruteForceAddFace(vh, v1, v2, 1);
							}
							else if (iterationIn == 1)
							{
								vh = m_omGeom->getMesh()->add_vertex(m_omGeom->getMesh()->point(v1));
								if (!vh.is_valid())
								{
									std::cout << "VERTEX Fail" << std::endl;
								}
								bruteForceAddFace(v0, vh, v2, 2);
							}
							else if (iterationIn == 2)
							{
								vh = m_omGeom->getMesh()->add_vertex(m_omGeom->getMesh()->point(v2));
								if (!vh.is_valid())
								{
									std::cout << "VERTEX Fail" << std::endl;
								}
								bruteForceAddFace(v0, v1, vh, 3);
							}
							else if (iterationIn == 3)
							{
								return;
							}
						}
					}
				}
			}
		}
	}
	return;
}