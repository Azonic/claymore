// ClayMore - Immersive Mesh Modelling --- Copyright (c) 2014-2017 Philipp Ladwig, Jannik Fiedler, Jan Beutgen
#include "SmoothProcessor.h"

#include "UtilProcessor.h"

#include "iostream"

#include <OpenMesh/Tools/Smoother/JacobiLaplaceSmootherT.hh>

SmoothProcessor::SmoothProcessor(UtilProcessor* utilProcessor) : m_utilProcessor(utilProcessor)
{

}

SmoothProcessor::~SmoothProcessor()
{

}

//WIP:Error-Prone -> Actually this works only on boundary vertices, but there is no query for it
void SmoothProcessor::smooth()
{
	// Initialize smoother with input mesh
	OpenMesh::Smoother::JacobiLaplaceSmootherT<Mesh> smoother(*m_omGeom->getMesh());
	//smoother.initialize(Tangential_and_Normal, C0); //Smooth direction  //Continuity 
	
	//TODO:Find input paramters for initialize()
	//smoother.initialize();

	smoother.smooth(3); // Execute 3 smooth steps


}