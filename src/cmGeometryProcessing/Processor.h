// ClayMore - Immersive Mesh Modelling --- Copyright (c) 2014-2017 Philipp Ladwig, Jannik Fiedler, Jan Beutgen
#pragma once

#include "ConfigDllExportGeometryProcessing.h"

#include "../cmOpenMeshBinding/OpenMeshGeometry.h"

#include "cmGeometryManagement\Constraints.h"

enum ProcessorType {
	UTIL,
	GRAB_ROT_SCALE,
	DELETE,
	EXTRUDE,
	SUBDIVIDE,
	EXPORT,
	IMPORT,
	HISTORY,
	SCULPT,
	JOINGEOMETRIES,
	DUPLICATE

};

class imGeometryProcessing_DLL_import_export Processor
{

public:
	// #### CONSTRUCTOR & DESTRUCTOR ###############
	Processor();
	~Processor();


	// #### MEMBER VARIABLES ###############
protected:
	osg::ref_ptr<osg::OpenMeshGeometry> m_omGeom;

	// #### MEMBER FUNCTIONS ###############
public:
	virtual inline void setActiveGeometry(osg::ref_ptr<osg::OpenMeshGeometry> omGeom) = 0;
};


