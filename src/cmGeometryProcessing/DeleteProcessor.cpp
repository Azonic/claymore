// ClayMore - Immersive Mesh Modelling --- Copyright (c) 2014-2017 Philipp Ladwig, Jannik Fiedler, Jan Beutgen
#include "DeleteProcessor.h"
#include "UtilProcessor.h"


DeleteProcessor::DeleteProcessor(UtilProcessor* utilProcessor) : m_utilProcessor(utilProcessor)
{

}

DeleteProcessor::~DeleteProcessor()
{

}

void DeleteProcessor::deleteSelectedMeshElements(bool verticesIn, bool edgesIn, bool facesIn)
{
	//NotePL: First delete faces, than vertices - it will otherwise end in an assert in polyconnectivity --- assert(_fh.is_valid() && !status(_fh).deleted()); ---
	Mesh::VertexIter vIt, vBegin, vEnd;
	Mesh::FaceIter  fIt, fBegin, fEnd;
	int j = 0;

	//TODO: Need this? Anyway better: Ask isThisTrait set?!
	m_omGeom->getMesh()->request_edge_status();
	m_omGeom->getMesh()->request_halfedge_status();
	m_omGeom->getMesh()->request_face_status();
	m_omGeom->getMesh()->request_vertex_status();

	//unsigned int i;

	if (verticesIn)
	{
		vBegin = m_omGeom->getMesh()->vertices_begin();
		vEnd = m_omGeom->getMesh()->vertices_end();

		for (vIt = vBegin; vIt != vEnd; ++vIt)
		{
			if (m_omGeom->getMesh()->status(*vIt).selected())
			{
				m_omGeom->getMesh()->delete_vertex(*vIt, true);
			}
		}
	}

	if (facesIn)
	{
		fBegin = m_omGeom->getMesh()->faces_begin();
		fEnd = m_omGeom->getMesh()->faces_end();

		for (fIt = fBegin; fIt != fEnd; ++fIt)
		{
			if (m_omGeom->getMesh()->status(*fIt).selected())
			{
				m_omGeom->getMesh()->delete_face(*fIt, true);
			}
		}
	}


	m_omGeom->getMesh()->garbage_collection(true, true, true);

	m_utilProcessor->cleanUpIndicies();

	//FIXME: After deleting there are some normals pointing in the wrong direction
	// I think, sync_indicies() mixed up some verts and when we calculate normals, there will be wrong normals
	//The solution is something like in blender "calculate normals outside"
	//Other solution will be that every normals pointing in and out - a "double normal" - maybe good for modeling, you dont want to see black faces...
	//the following lines dont help:
	//TODO: Agree on which function should be used: calcNormals  or update_normals()?
	m_omGeom->getMesh()->update_normals();
}
