// ClayMore - Immersive Mesh Modelling --- Copyright (c) 2014-2017 Philipp Ladwig, Jannik Fiedler, Jan Beutgen
#include "MeshHistoryProcessor.h"
#include "UtilProcessor.h"

#include <iostream>



MeshHistoryProcessor::MeshHistoryProcessor(UtilProcessor* utilProcessor) : m_utilProcessor(utilProcessor)
{
	maxSteps = 3;
	undoStep = 0;

	m_initialTopologyAtStartOfTransform = new osg::Vec3Array();
	m_initialIndiciesAtStartOfTransform = new osg::DrawElementsUInt(osg::PrimitiveSet::TRIANGLES, 0);
	m_initialColorAtStartOfTransform = new osg::Vec4Array();

	//Adding three vec3Arrays for going back three times
	//m_historyOfVerticies.push_back(new osg::Vec3Array());
	//m_historyOfVerticies.push_back(new osg::Vec3Array());
	//m_historyOfVerticies.push_back(new osg::Vec3Array());

	m_historyOfIndicies.push_back(new osg::DrawElementsUInt(osg::PrimitiveSet::TRIANGLES, 0));
	m_historyOfIndicies.push_back(new osg::DrawElementsUInt(osg::PrimitiveSet::TRIANGLES, 0));
	m_historyOfIndicies.push_back(new osg::DrawElementsUInt(osg::PrimitiveSet::TRIANGLES, 0));
}

MeshHistoryProcessor::~MeshHistoryProcessor()
{

}

void MeshHistoryProcessor::copyAndStoreVertexArray()
{
	osg::Vec3Array *vertexArray = dynamic_cast<osg::Vec3Array*>(m_omGeom->getVertexArray());
	osg::MixinVector<osg::Vec3>::iterator it = vertexArray->begin();
	m_initialTopologyAtStartOfTransform->assign(it, it + vertexArray->size());
}

void MeshHistoryProcessor::copyAndStoreIndiciesArray()
{
	//m_omGeom->getMesh()->

	//size_t nF;
	//std::vector<OpenMesh::VertexHandle> vhandles;
	//unsigned int idx;
	//for (size_t i = 0, nF = m_omGeom->getMesh()->n_faces(); i < nF; ++i)
	//{
	//	vhandles.clear();
	//	for (Mesh::CFVIter fv_it = m_omGeom->getMesh()->cfv_iter(OpenMesh::FaceHandle(unsigned int(i))); fv_it.is_valid(); ++fv_it)
	//	{
	//		vhandles.push_back(*fv_it);
	//	}

	//	std::vector<unsigned int> indiciesCleanedUp;
	//	for (size_t j = 0; j < vhandles.size(); ++j)
	//	{
	//		idx = vhandles[j].idx(); //+ 1;
	//		indiciesCleanedUp.push_back(idx);
	//	}
	//	sync_indices((unsigned int)i, indiciesCleanedUp[0], indiciesCleanedUp[1], indiciesCleanedUp[2]);
	//}






	//std::cout << "copyAndStoreIndiciesArray" << std::endl;

	osg::DrawElementsUInt* indiciesArray = dynamic_cast<osg::DrawElementsUInt*>(m_omGeom->getPrimitiveSet(0));
	//std::cout << "indiciesArray->size()" << indiciesArray->size() << std::endl;
	osg::DrawElementsUInt::iterator it = indiciesArray->begin();
	//osg::MixinVector<osg::DrawElementsUInt::TRIANGLES>::iterator it = indiciesArray->
	m_initialIndiciesAtStartOfTransform->assign(it, it + indiciesArray->size());
}

void MeshHistoryProcessor::copyAndStoreColorArray()
{
	//std::cout << "copyAndStoreColorArray" << std::endl;
	osg::Vec4Array *colorArray = dynamic_cast<osg::Vec4Array*>(m_omGeom->getColorArray());
	osg::MixinVector<osg::Vec4>::iterator it = colorArray->begin();
	m_initialColorAtStartOfTransform->assign(it, it + colorArray->size());
}

inline osg::Vec3Array* MeshHistoryProcessor::getCurrentVertexArray()
{
	//return m_initialTopologyAtStartOfTransform;
	return m_historyOfVerticies.at(m_historyOfVerticies.size() - 1);
}

void MeshHistoryProcessor::cancelAnyTransformation()
{
	//TODO: All the //// are an old version which tries to rebuild the entire mesh, but it takes very long and is error-prone with deleting mesh elements and restore them...
	//I guess, this was something with normal array, which is empty after garbageCollection, if the entire mesh was deleted...
	//so, I think I have to process the normals again... but at this point I stopped and tried the "generative mesh history approach" which seems also be used in Blender
	//Currently its a dump restoring of vertexArray and ColorArray... works only for grab, scale and rotate.

	////Mesh::FaceIter  fIt, fBegin, fEnd;
	//Mesh::VertexIter vIt, vBegin, vEnd;

	//for (vIt = vBegin; vIt != vEnd; ++vIt)
	//{
	//	m_omGeom->getMesh()->delete_vertex(*vIt, true);
	//}	
	//m_omGeom->getMesh()->garbage_collection(true, true, true);

	osg::Vec3Array *vertexArray = dynamic_cast<osg::Vec3Array*>(m_omGeom->getVertexArray());
	osg::MixinVector<osg::Vec3>::iterator it = m_initialTopologyAtStartOfTransform->begin();
	vertexArray->assign(it, it + m_initialTopologyAtStartOfTransform->size());
	vertexArray->dirty();

	////osg::DrawElementsUInt *indiciesArray = dynamic_cast<osg::DrawElementsUInt*>(m_omGeom->getPrimitiveSet(0));
	////std::cout << "CopyAnd Restore: indiciesArray->size()" << indiciesArray->size() << std::endl;
	////osg::DrawElementsUInt::iterator itIndicies = m_initialIndiciesAtStartOfTransform->begin();
	////std::cout << "CopyAnd Restore: m_initialIndiciesAtStartOfTransform->size()" << m_initialIndiciesAtStartOfTransform->size() << std::endl;
	////indiciesArray->assign(itIndicies, itIndicies + m_initialIndiciesAtStartOfTransform->size());
	//////indiciesArray->dirty();
	////std::cout << "CopyAnd Restore: indiciesArray->size() Danach?" << indiciesArray->size() << std::endl;
	////m_omGeom->getPrimitiveSet(0)->dirty();
	////m_omGeom->getMesh()->garbage_collection(true, true, true);
	////m_utilProcessor->cleanUpIndicies();
	////m_utilProcessor->calcNormals();
	////m_omGeom->getPrimitiveSet(0)->dirty();


	//for (unsigned int i = 0; i < m_initialIndiciesAtStartOfTransform->getNumIndices(); ++i)
	//{
	//	OpenMesh::VertexHandle v1((*m_initialIndiciesAtStartOfTransform)[i]);
	//	++i;
	//	OpenMesh::VertexHandle v2((*m_initialIndiciesAtStartOfTransform)[i]);
	//	++i;
	//	OpenMesh::VertexHandle v3((*m_initialIndiciesAtStartOfTransform)[i]);
	//	m_omGeom->getMesh()->add_face(v1, v2, v3);
	//}
	////m_omGeom->getMesh()->update_normals();
	////osg::ref_ptr<Vec3Array> normalArray = new Vec3Array();
	////Geometry is the base class
	////Geometry::setNormalArray(();
	////Geometry::setNormalBinding(osg::OpenMeshGeometry::BIND_PER_VERTEX);
	////m_omGeom->getMesh()->bind<osg::Vec3>(Mesh::VProp, "v:normals", m_omGeom->getNormalArray());
	////mesh.bind<Vec3>(Mesh::VProp, "v:normals", *normalArray);

	////m_omGeom->getPrimitiveSet(0)->dirty();
	//m_omGeom->getMesh()->garbage_collection(true, true, true);
	//m_utilProcessor->cleanUpIndicies();
	//m_omGeom->getMesh()->update_normals();
	////m_omGeom->getPrimitiveSet(0)->dirty();

	osg::Vec4Array *colorArray = dynamic_cast<osg::Vec4Array*>(m_omGeom->getColorArray());
	osg::MixinVector<osg::Vec4>::iterator itColor = m_initialColorAtStartOfTransform->begin();
	colorArray->assign(itColor, itColor + m_initialColorAtStartOfTransform->size());
	colorArray->dirty();
}

void MeshHistoryProcessor::undo()
{
	undoStep++;
	std::cout << "undoStep nach ++: " << undoStep << std::endl;
	if (undoStep > maxSteps)
		undoStep = maxSteps;
	if (undoStep < 0)
		undoStep = 0;
	if (undoStep <= maxSteps)
	{
		std::cout << "Restore at position: " << maxSteps - undoStep << std::endl;
		osg::Vec3Array *vertexArray = dynamic_cast<osg::Vec3Array*>(m_omGeom->getVertexArray());
		osg::MixinVector<osg::Vec3>::iterator it;
		if (m_historyOfVerticies.size() == maxSteps)
		{
			it = m_historyOfVerticies.at(maxSteps - undoStep)->begin();
			vertexArray->assign(it, it + m_historyOfVerticies.at(maxSteps - undoStep)->size());
		}
		else
		{
			it = m_historyOfVerticies.at(m_historyOfVerticies.size() - 1)->begin();
			vertexArray->assign(it, it + m_historyOfVerticies.at(m_historyOfVerticies.size() - 1)->size());
		}


		vertexArray->dirty();

		//osg::Vec4Array *colorArray = dynamic_cast<osg::Vec4Array*>(m_omGeom->getColorArray());
		//osg::MixinVector<osg::Vec4>::iterator itColor = m_initialColorAtStartOfTransform->begin();
		//colorArray->assign(itColor, itColor + m_initialColorAtStartOfTransform->size());
		//colorArray->dirty();
	}
}

//void MeshHistoryProcessor::redo()
//{
//	undoStep--;
//	osg::Vec3Array *vertexArray = dynamic_cast<osg::Vec3Array*>(m_omGeom->getVertexArray());
//	osg::MixinVector<osg::Vec3>::iterator it = m_historyOfVerticies.at(maxSteps - undoStep)->begin();
//	vertexArray->assign(it, it + m_historyOfVerticies.at(maxSteps - undoStep)->size());
//	vertexArray->dirty();
//
//	//osg::Vec4Array *colorArray = dynamic_cast<osg::Vec4Array*>(m_omGeom->getColorArray());
//	//osg::MixinVector<osg::Vec4>::iterator itColor = m_initialColorAtStartOfTransform->begin();
//	//colorArray->assign(itColor, itColor + m_initialColorAtStartOfTransform->size());
//	//colorArray->dirty();
//}

//void OpenMeshGeometry::restoreLastTopology()
//{
//	int sizeOfArray = m_topologyHistory.size();
//	setVertexArray(m_topologyHistory.at(sizeOfArray - 2));
//}


void MeshHistoryProcessor::storeMeshDataAsHistory()
{
	if (undoStep > 0)
	{
		m_historyOfVerticies.clear();
		//std::cout << "m_historyOfVerticies.size() after clear: " << m_historyOfVerticies.size() << std::endl;
		undoStep = 0;
	}
	//if (undoStep > maxSteps)
	//	undoStep = maxSteps;
	//if (undoStep < 0)
	//	undoStep = 0;
	//std::cout << "storeMeshDataAsHistory--------------------" << std::endl;
	osg::Vec3Array *vertexArray = dynamic_cast<osg::Vec3Array*>(m_omGeom->getVertexArray());
	osg::MixinVector<osg::Vec3>::iterator itVerts = vertexArray->begin();
	//TODO: Be aware of memory hole!!!
	osg::Vec3Array* newArray = new osg::Vec3Array();
	newArray->assign(itVerts, itVerts + vertexArray->size());
	m_historyOfVerticies.push_back(newArray);

	//osg::Vec4Array *colorArray = dynamic_cast<osg::Vec4Array*>(m_omGeom->getColorArray());
	//osg::MixinVector<osg::Vec4>::iterator itVColors = colorArray->begin();
	////TODO: Be aware of memory hole!!!
	//osg::Vec4Array* newColorArray = new osg::Vec4Array();
	//newColorArray->assign(itVColors, itVColors + colorArray->size());
	//m_historyOfVertColors.push_back(newColorArray);


	//osg::Vec4Array *colorArray = dynamic_cast<osg::Vec4Array*>(m_omGeom->getColorArray());
	//osg::MixinVector<osg::Vec4>::iterator itVColors = colorArray->begin();
	//(*m_historyOfVertColors.end())->assign(itVColors, itVColors + colorArray->size());


	//Delete the last element, if there are more than 3 elements in this vector
	//Verts
	if (m_historyOfVerticies.size() > 3)
	{
		m_historyOfVerticies.erase(m_historyOfVerticies.begin());
	}

	//VertColors
	//if (m_historyOfVertColors.size() > 3)
	//{
	//	m_historyOfVertColors.erase(m_historyOfVertColors.begin());
	//}
}