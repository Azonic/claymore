// ClayMore - Immersive Mesh Modelling --- Copyright (c) 2014-2017 Philipp Ladwig, Jannik Fiedler, Jan Beutgen
#include "MergeProcessor.h"

#include "UtilProcessor.h"

#include "iostream"

MergeProcessor::MergeProcessor(UtilProcessor* utilProcessor) : m_utilProcessor(utilProcessor)
{

}

MergeProcessor::~MergeProcessor()
{

}

void MergeProcessor::merge()
{
	//A solution in fractal2Mesh with Cuda is already available which is based on thrust::lower_bound()
}