// ClayMore - Immersive Mesh Modelling --- Copyright (c) 2014-2017 Philipp Ladwig, Jannik Fiedler, Jan Beutgen
#include "ImportProcessor.h"

#include "UtilProcessor.h"
#include <OpenMesh/Core/IO/MeshIO.hh>


ImportProcessor::ImportProcessor(UtilProcessor* utilProcessor) : m_utilProcessor(utilProcessor)
{

}

ImportProcessor::~ImportProcessor()
{

}

void ImportProcessor::readFileFromDisk(std::string readPath)
{
	if (!OpenMesh::IO::read_mesh(*m_omGeom->getMesh(), readPath))
	{
		std::cerr << "Read error in ImportProcessor\n";
		exit(1);
	}

	//EXP:Vertex Array will be updated automatically if some new or changed data will come in
	//		But it doesn't work for the PrimitiveSets. So, first delete the PrimitiveSet and set a new one
	//TODO: Verify, that removePrimitiveSet() deletes the memory on the heap! Try it with huge meshes and task analysis ->Test on 141016 ->Seems to be OK! But there are no ref_ptr -> C++ automatic garbage collection?
	//TODO: Try: By the way: I guess now, the removePrimitiveSet is only for already existing stuff in the PrimitiveArray. Maybe not neccessary but usefull? Is some kind of  error handling?
	m_omGeom->removePrimitiveSet(0);
	osg::ref_ptr<osg::DrawElementsUInt> drawElement = new osg::DrawElementsUInt(osg::PrimitiveSet::TRIANGLES, 0);
	m_omGeom->addPrimitiveSet(drawElement);

	//NOTE: Important: Clean up indicies
	std::vector<OpenMesh::VertexHandle> vhandles;
	unsigned int idx;
	for (int i = 0, nF = m_omGeom->getMesh()->n_faces(); i < nF; ++i)
	{
		vhandles.clear();
		for (Mesh::CFVIter fv_it = m_omGeom->getMesh()->cfv_iter(OpenMesh::FaceHandle(int(i))); fv_it.is_valid(); ++fv_it)
		{
			vhandles.push_back(*fv_it);
		}

		std::vector<int> indiciesCleanedUp;
		for (int j = 0; j < vhandles.size(); ++j)
		{
			idx = vhandles[j].idx(); //+ 1;
			indiciesCleanedUp.push_back(idx);
		}
		m_utilProcessor->sync_indices((int)i, indiciesCleanedUp[0], indiciesCleanedUp[1], indiciesCleanedUp[2]);
	}
	//FIXME: After deleting there are some normals pointing in the wrong direction
	// I think, sync_indicies() mixed up some verts and when we calculate normals, there will be wrong normals
	//The solution is something like in blender "calculate normals outside"
	//Other solution will be that every normals pointing in and out - a "double normal" - maybe good for modeling, you dont want to see black faces...
	m_omGeom->getMesh()->update_normals();
}
