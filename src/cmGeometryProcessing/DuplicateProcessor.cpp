// ClayMore - Immersive Mesh Modelling --- Copyright (c) 2014-2017 Philipp Ladwig, Jannik Fiedler, Jan Beutgen
#include "DuplicateProcessor.h"

#include "UtilProcessor.h"

#include "iostream"

DuplicateProcessor::DuplicateProcessor(UtilProcessor* utilProcessor) : m_utilProcessor(utilProcessor)
{

}

DuplicateProcessor::~DuplicateProcessor()
{

}

void DuplicateProcessor::duplicate()
{
	//Iterate through all faces...
	Mesh::FaceIter fIt, fBegin, fEnd;
	fBegin = m_omGeom->getMesh()->faces_begin();
	fEnd = m_omGeom->getMesh()->faces_end();

	int tempCounter1 = 0;
	int tempCounter2 = 0;
	for (fIt = fBegin; fIt != fEnd; ++fIt)
	{
		tempCounter1++;
	}
	//std::cout << "tempCounter1: " << tempCounter1 << std::endl;

	for (fIt = fBegin; fIt != fEnd; ++fIt)
	{
		//...and look if there is a face marked as selected...
		if (m_omGeom->getMesh()->status(*fIt).selected())
		{
			//std::cout << "Bei Vertex Nummer: " << tempCounter2 << std::endl;
			//...if so, iterate through all face's vertices...
			std::vector<OpenMesh::VertexHandle> faceVerts;

			//Old version with compiler warning:
			//Mesh::FaceVertexIter fvIter = m_omGeom->getMesh()->fv_iter(fIt);
			//for (; fvIter; ++fvIter)

			Mesh::FaceVertexIter fvIter = m_omGeom->getMesh()->fv_iter(*fIt);
			Mesh::FaceVertexIter fvBegin = m_omGeom->getMesh()->fv_begin(*fIt);
			Mesh::FaceVertexIter fvEnd = m_omGeom->getMesh()->fv_end(*fIt);
			for (fvIter = fvBegin; fvIter != fvEnd; ++fvIter)
			{
				//...and push them into an vertexHandleArray...
				//(Sometimes chrash here, if all is selected an duplicated)
				//TODO1: Find reason why it chrashs here sometimes (also for large duplicating areas -> 4 high res monkey scene)
				if (fvIter.is_valid()) {
					OpenMesh::VertexHandle vh;

					vh = m_omGeom->getMesh()->add_vertex(m_omGeom->getMesh()->point(*fvIter) * 1.001f);

					if (vh.is_valid()) {
						faceVerts.push_back(vh); //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
					}
				}
				//...and deselect the vertex
				m_omGeom->getMesh()->status(*fvIter).set_selected(false);
			}
			//...and eventually add a face with the collected vertexHandles
			OpenMesh::FaceHandle newAddedFace;
			if (faceVerts.size() == 3)
			{
				m_omGeom->getMesh()->status(faceVerts.at(0)).set_selected(true);
				m_omGeom->getMesh()->status(faceVerts.at(1)).set_selected(true);
				m_omGeom->getMesh()->status(faceVerts.at(2)).set_selected(true);
				newAddedFace = m_omGeom->getMesh()->add_face(faceVerts.at(0), faceVerts.at(1), faceVerts.at(2)); //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
				m_omGeom->getMesh()->status(newAddedFace).set_selected(true);
			}

			//Mesh::FaceVertexIter fvIter2 = m_omGeom->getMesh()->fv_iter(newAddedFace);
			//for (; fvIter2; ++fvIter2)
			//{
			//	m_omGeom->getMesh()->status(*fvIter2).set_selected(true);
			//}
		}
		m_omGeom->getMesh()->status(*fIt).set_selected(false);
		tempCounter2++;
	}

	std::cout << "tempCounter1: " << tempCounter1 << std::endl;
	std::cout << "tempCounter2: " << tempCounter2 << std::endl;

	//Clean up vertices
	//FIXME: I think I need this more than one time ->make an extra function for this and reduce code
	int i = 0;
	std::vector<OpenMesh::VertexHandle> vhandles;
	unsigned int idx;
	for (int i = 0, nF = m_omGeom->getMesh()->n_faces(); i < nF; ++i)
	{
		vhandles.clear();
		for (Mesh::CFVIter fv_it = m_omGeom->getMesh()->cfv_iter(OpenMesh::FaceHandle(int(i))); fv_it.is_valid(); ++fv_it)
		{
			vhandles.push_back(*fv_it);
		}

		std::vector<int> indiciesCleanedUp;
		for (int j = 0; j < vhandles.size(); ++j)
		{
			idx = vhandles[j].idx(); //+ 1;
			indiciesCleanedUp.push_back(idx);
		}
		m_utilProcessor->sync_indices((int)i, indiciesCleanedUp[0], indiciesCleanedUp[1], indiciesCleanedUp[2]);
	}

	//TODO: Agree on which type of calcNormals we should use
	m_omGeom->getMesh()->update_normals();
	m_omGeom->getNormalArray()->dirty();
	m_omGeom->getVertexArray()->dirty();

	for (int i = 0; i < m_omGeom->getPrimitiveSetList().size(); i++)
	{
		m_omGeom->getPrimitiveSet(i)->dirty();
	}

	//std::cout << "Vorbei" << std::endl;
	//colorArray->dirty();
	//This is taken from JoinGeometriesProcessor
	//osg::Vec3Array* vertexArrayMemberMesh = dynamic_cast<osg::Vec3Array*>(m_omGeom->getVertexArray());
	////osg::Vec3Array* vertexArrayJoinMesh = dynamic_cast<osg::Vec3Array*>(geometryToJoin->getVertexArray());

	//std::cout << std::endl << "Number of Vertices before: " << vertexArrayMemberMesh->size() << std::endl;
	//int vertexArraySizeBeforeMerge = vertexArrayMemberMesh->size();

	//for (int i = 0; i < vertexArrayJoinMesh->size(); ++i)
	//{
	//	//std::cout << "i: " << i << std::endl;
	//	m_omGeom->getMesh()->add_vertex(vertexArrayJoinMesh->at(i));
	//}
	//std::cout << std::endl << "Insert finished..." << std::endl;
	//std::cout << std::endl << "Number of Vertices after: " << vertexArrayMemberMesh->size() << std::endl;

	//osg::DrawElementsUInt* indicesToJoin = dynamic_cast<osg::DrawElementsUInt*>(geometryToJoin->getPrimitiveSet(0));

	//for (unsigned int i = 0; i < indicesToJoin->getNumIndices(); ++i)
	//{
	//	OpenMesh::VertexHandle v1((*indicesToJoin)[i] + vertexArraySizeBeforeMerge);
	//	//std::cout << "v1: " << (*indicesToJoin)[i] + vertexArraySizeBeforeMerge << std::endl;
	//	++i;
	//	OpenMesh::VertexHandle v2((*indicesToJoin)[i] + vertexArraySizeBeforeMerge );
	//	//std::cout << "v2: " << (*indicesToJoin)[i] + vertexArraySizeBeforeMerge << std::endl;
	//	++i;
	//	OpenMesh::VertexHandle v3((*indicesToJoin)[i] + vertexArraySizeBeforeMerge);
	//	//std::cout << "v3: " << (*indicesToJoin)[i] + vertexArraySizeBeforeMerge << std::endl;
	//	m_omGeom->getMesh()->add_face(v1, v2, v3);
	//}

	////std::cout << std::endl << "Number of Vertices after: " << vertexArrayMemberMesh->size() << std::endl;
	//std::vector<OpenMesh::VertexHandle> vhandles;
	//unsigned int idx;
	//for (int i = 0, nF = m_omGeom->getMesh()->n_faces(); i < nF; ++i)
	//{
	//	vhandles.clear();
	//	for (Mesh::CFVIter fv_it = m_omGeom->getMesh()->cfv_iter(OpenMesh::FaceHandle(int(i))); fv_it.is_valid(); ++fv_it)
	//	{
	//		vhandles.push_back(*fv_it);
	//	}

	//	std::vector<int> indiciesCleanedUp;
	//	for (int j = 0; j < vhandles.size(); ++j)
	//	{
	//		idx = vhandles[j].idx(); //+ 1;
	//		indiciesCleanedUp.push_back(idx);
	//	}
	//	m_utilProcessor->sync_indices((int)i, indiciesCleanedUp[0], indiciesCleanedUp[1], indiciesCleanedUp[2]);
	//}

	////TODO: Agree on which type of calcNormals we should use
	//m_omGeom->getMesh()->update_normals();
	//m_omGeom->getNormalArray()->dirty();
	//m_omGeom->getVertexArray()->dirty();

	//for (int i = 0; i < m_omGeom->getPrimitiveSetList().size(); i++)
	//{
	//	m_omGeom->getPrimitiveSet(i)->dirty();
	//}
}