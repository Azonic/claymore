// ClayMore - Immersive Mesh Modelling --- Copyright (c) 2014-2017 Philipp Ladwig, Jannik Fiedler, Jan Beutgen
#pragma once

#include "ConfigDllExportGeometryProcessing.h"

#include "../cmOpenMeshBinding/OpenMeshGeometry.h"
#include "Processor.h"

class UtilProcessor;
class GrabRotateScaleProcessor;
class MeshHistoryProcessor;
class DeleteProcessor;
class ExtrudeProcessor;
class SubdivideProcessor;
class ImportProcessor;
class ExportProcessor;
class SculptProcessor;
class JoinGeometriesProcessor;
class DuplicateProcessor;


class imGeometryProcessing_DLL_import_export GeometryMainProcessor : public Processor
{

public:
	// #### CONSTRUCTOR & DESTRUCTOR ###############
	GeometryMainProcessor();
	~GeometryMainProcessor();

	void setActiveGeometry(osg::ref_ptr<osg::OpenMeshGeometry> omGeom);


	// #### MEMBER VARIABLES ###############
private:
	//All processors:
	UtilProcessor* m_utilProc;
	GrabRotateScaleProcessor* m_grabRotateScaleProc;
	MeshHistoryProcessor* m_meshHistoryProc;
	DeleteProcessor* m_deleteProc;
	ExtrudeProcessor* m_extrudeProc;
	ImportProcessor* m_importProc;
	ExportProcessor* m_exportProc;
	SubdivideProcessor* m_subdivideProc;
	SculptProcessor* m_sculptProc;
	JoinGeometriesProcessor* m_joinGeometriesProc;
	DuplicateProcessor* m_duplicateProc;

	typedef OpenMesh::OSG_TriMesh_BindableArrayKernelT Mesh;
	void addProcessor(ProcessorType type, Processor* processor);

	//std::map for iterating easily over all present processors (e.g: setActiveGeometry())
	std::map<ProcessorType, Processor*> m_processorMap;

	// #### MEMBER FUNCTIONS ###############
public:
	//UTIL
	osg::Vec3d getBarycentrumOfSELECTEDVerts();
	osg::Vec3d getBarycentrumOfALLVerts();
	void calcNormals();
	void sync_indices(size_t face, size_t v1, size_t v2, size_t v3);
	float calculateDistance(OpenMesh::OSG_TriMesh_BindableArrayKernelT::VertexIter vIt, OpenMesh::OSG_TriMesh_BindableArrayKernelT::VertexHandle vh);
	float calculateDistance(OpenMesh::OSG_TriMesh_BindableArrayKernelT::VertexIter vIt, osg::Vec3f positionIn);
	float calculateDistance(osg::Vec3f position0, osg::Vec3f position1);
	void addFaceBasedOnSelectedVerts();

	//GRAB_SCALE_ROT
	void grabVerticesStart(osg::Vec3f grabStartPositionIn);
	void grabVertices(osg::Vec3f newPositionIn, bool isLocalCoordSysSelectedIn, bool isSnappingActiveIn, Constraints constraintIn, osg::Matrix matrixWorldToLocal);

	void grabPropEditStart(osg::Vec3f grabStartPositionIn, float radiusIn, float shapeIn);
	void grabPropEdit(osg::Vec3f newPositionIn, bool isLocalCoordSysSelectedIn, Constraints constraintIn, osg::Matrix matrixWorldToLocal);
	void grabProbEditSetValues(float radiusIn, float shapeIn);

	void rotateVerticesStart();
	void rotateVertices(osg::Matrixd rotMatIn, bool isSnappingActiveIn, Constraints constraintIn, osg::Vec3d barycentrum);
	void rotateEntireGeometry(osg::Matrixd rotMatIn, osg::Vec3d barycentrum);

	void scaleVerticesStart();
	void scaleVertices(float scaleValueIn, bool isLocalIn, bool isSnappingActiveIn, Constraints constraintIn, osg::Matrix matrixWorldToLocalIn, osg::Matrix matrixLocalToWorldIn, osg::Vec3f barycentrum);
	void resetScaleGeometry();
	void scaleEntireGeometryBasedOnBaryOfALLVerts(float scaleValueIn);
	void scaleEntireGeometryBasedOnBaryOfSELECTEDVerts(float scaleValueIn);

	//DELETE
	void deleteSelectedMeshElements(bool verticesIn, bool edgesIn, bool facesIn);

	//EXTRUDE
	void extrude();

	//SUBDIVIDE
	void subdivide();

	//HISTORY
	void cancelAnyTransformation();
	void storeMeshDataAsHistory();
	void undo();

	//IMPORTER
	void readFileFromDisk(std::string readPath);

	//EXPORTER
	void writeFileToDisk(std::string savePath);

	//SCULPT
	void sculptStart(float radiusIn, float strengthIn);
	void sculptAddOrSubtract(float radiusIn, float strengthIn, osg::Vec3 cursorTrans, osg::Matrix geomLocal2World, bool isAddBrush);

	//JOIN TWO GEOMETRIES
	void joinGeometries(osg::ref_ptr<osg::OpenMeshGeometry> geometryToJoin);

	//DUPLICATE PROCESSOR
	void duplicate();
};