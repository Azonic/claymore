// ClayMore - Immersive Mesh Modelling --- Copyright (c) 2014-2017 Philipp Ladwig, Jannik Fiedler, Jan Beutgen
#pragma once

#include "ConfigDllExportGeometryProcessing.h"
#include "Processor.h"

class UtilProcessor;

enum TrigonometryQuarter
{
	Q1,
	Q2,
	Q3,
	Q4
};

class imGeometryProcessing_DLL_import_export GrabRotateScaleProcessor : public Processor
{

public:
	// #### CONSTRUCTOR & DESTRUCTOR ###############
	GrabRotateScaleProcessor(UtilProcessor* utilProcessor);
	~GrabRotateScaleProcessor();


	// #### MEMBER VARIABLES ###############
private:
	typedef OpenMesh::OSG_TriMesh_BindableArrayKernelT Mesh;
	UtilProcessor* m_utilProcessor;

	osg::Vec3f m_grabStartPosition;
	std::vector<bool> m_selectedPropEditVerts;
	std::vector<float> m_distanceFactorsForPropEdit;

	// #### MEMBER FUNCTIONS ###############
public:
	inline virtual void setActiveGeometry(osg::ref_ptr<osg::OpenMeshGeometry> omGeom) { m_omGeom = omGeom; }

	// ### Grab Functions ###
	void grabVerticesStart(osg::Vec3f grabStartPositionIn);
	void grabVertices(osg::Vec3f newPositionIn,
		bool isLocalCoordSysSelectedIn,
		bool isSnappingActive,
		Constraints constraintIn,
		osg::Matrix matrixWorldToLocal,
		osg::Vec3Array* initialVertexArray);

	void grabPropEditStart(osg::Vec3f grabStartPosition, float radius, float shape);
	void grabPropEdit(osg::Vec3f newPositionIn,
		bool isLocalCoordSysSelectedIn,
		Constraints constraintIn,
		osg::Matrix matrixWorldToLocal,
		osg::Vec3Array* initialVertexArray);
	void grabProbEditSetValues(float radiusIn, float shapeIn);


	// ### Rotate Functions ###
	//PARAM0: -
	//PARAM1: initialVertexArray is 
	//PARAM2: -
	//PARAM3: -
	//PARAM4: Barycentrum is important to calculate in Observer once at start ("Trigger PUSH"), 
	//giving it to the controller tools, store it as Observer member and if state is changed to "HOLD"�pass tthis stored member every frame
	//If one calculates every frame the barycentrum in the processor, it will "glitch away"
	//PARAM5: initialVertexArray is the last stored topology of the mesh. Must be allocated in a xxxStart(). Is used, because we need an "original" mesh
	//which we change every frame, but this changed must not be stored in the active, selected mesh, because the idea is to get the difference between the start controller rotation (several frames ago)
	// and the current controller rotation. This rotation must be applied to the mesh, instead of adding the rotation difference between frame o frame, so in this old
	//approach we "forgot" the changes 3 frames ago...here we save it from "PUSH", while HOLD, until RELEASE is emitted.
	void rotateVertices(osg::Matrixd rotMatIn, bool isSnappingActive, Constraints constraintIn, osg::Vec3d barycentrum, osg::Vec3Array* initialVertexArray);
	void rotateEntireGeometry(osg::Matrixd rotMatIn, osg::Vec3Array* initialVertexArray, osg::Vec3d barycentrum);

	// ### ScaleFunctions ###
	void scaleVertices(float scaleValueIn, bool isLocalIn, bool isSnappingActive, Constraints constraintIn, osg::Matrix matrixWorldToLocalIn, osg::Matrix matrixLocalToWorldIn, osg::Vec3f barycentrum);
	void resetScaleGeometry(osg::Vec3Array* initialVertexArray);
	void scaleEntireGeometryBasedOnBaryOfSELECTEDVerts(float valueIn, osg::Vec3Array* initialVertexArray);
	void scaleEntireGeometryBasedOnBaryOfALLVerts(float valueIn, osg::Vec3Array* initialVertexArray);
};


