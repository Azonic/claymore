// ClayMore - Immersive Mesh Modelling --- Copyright (c) 2014-2017 Philipp Ladwig, Jannik Fiedler, Jan Beutgen
#pragma once

#include "ConfigDllExportGeometryProcessing.h"
#include "Processor.h"

#include <osg/Geode>
#include <osg/Vec3>
#include "cmOpenMeshBinding/OpenMeshGeometry.h"

#include "../cmUtil/OptimizedBoolVector.h"

//#include "../cmPointCloudManagement/Octree.h"
#include "../cmPointCloudManagement/OctreeVolume.cpp" //cpp & h are needed since this is a template class; the cpp itself includes the h

#include <unordered_map>

class UtilProcessor;

struct GridCell {
	osg::Vec3f p[8];
	float val[8];
	unsigned char dep[8];
};

struct KernelCross {
	// 3D-positioning of the indices:
	//		4  6
	//      | /
	// 1 -- 0 -- 2
	//    / |
	//   5  3
	Octree<float>* octrees[7]; 

	bool testOctreeDepth() {
		for (Octree<float>* o : octrees) {
			if (o != nullptr && o->isMaxDepth()) {
				return true;
			}
		}
		return false;
	}
	float trilinearInterpolation() {
		return 1.0f;
	}
};

class imGeometryProcessing_DLL_import_export BoolMarchingCubesProcessor : public Processor
{
	// #### CONSTRUCTOR & DESTRUCTOR ###############
public:
	BoolMarchingCubesProcessor(ConfigReader* configReaderIn, UtilProcessor* utilProcessorIn);
	~BoolMarchingCubesProcessor();


	// #### MEMBER VARIABLES ###############
private:
	typedef OpenMesh::OSG_TriMesh_BindableArrayKernelT Mesh;
	UtilProcessor* m_utilProcessor;
	ConfigReader* m_configReader;

	//osg::ref_ptr<osg::OpenMeshGeometry> m_geometry;
	osg::Geometry* m_geometry;
	osg::Geode* m_surfaceGeode;
	int* m_edgeTable = new int[256];
	int* m_triTable = new int[256 * 16];
	double m_stepSize;
	double m_stepSizeHalf;
	double m_stepSizeZ;
	double m_stepSizeZHalf;
	std::vector<std::vector<std::vector<bool> > > m_volume;
	unsigned int m_counter;
	int m_argc;
	char** m_argv;
	size_t m_volumeDimX;
	size_t m_volumeDimY;
	size_t m_volumeDimZ;
	size_t m_volumeStepYZ;
	size_t m_volumeStepXY;
	//float m_volumeStepZ;
	double m_orthoSize;


	osg::Vec3Array* m_verticesThread0;
	osg::Vec3Array* m_verticesThread1;
	osg::Vec3Array* m_verticesThread2;
	osg::Vec3Array* m_verticesThread3;
	osg::Vec3Array* m_verticesThread4;
	osg::Vec3Array* m_verticesThread5;
	osg::Vec3Array* m_verticesThread6;
	osg::Vec3Array* m_verticesThread7;

	osg::Vec3Array* m_normalsThread0;
	osg::Vec3Array* m_normalsThread1;
	osg::Vec3Array* m_normalsThread2;
	osg::Vec3Array* m_normalsThread3;
	osg::Vec3Array* m_normalsThread4;
	osg::Vec3Array* m_normalsThread5;
	osg::Vec3Array* m_normalsThread6;
	osg::Vec3Array* m_normalsThread7;

	// #### MEMBER FUNCTIONS ###############
private:
	inline virtual void setActiveGeometry(osg::ref_ptr<osg::OpenMeshGeometry> omGeom) { m_omGeom = omGeom; }
	void process(size_t start, size_t end, osg::Vec3Array* m_verticesThreadIn, osg::Vec3Array* m_normalsThreadIn, OptimizedBoolVector* vaVec);
	void processFloat(size_t start, size_t end, osg::Vec3Array* m_verticesThreadIn, osg::Vec3Array* m_normalsThreadIn, std::vector<std::pair<float, unsigned char>>* vaVec, unsigned char maxDepth);
	void processFloat(unsigned int start, unsigned int end, osg::Vec3Array* m_verticesThreadIn, osg::Vec3Array* m_normalsThreadIn, Octree<float>* octree);
	void processFloatHierarchical(Octree<float>* octree, osg::Vec3f globalOffset, osg::Vec3i start, osg::Vec3i size, osg::Vec3Array* m_verticesThreadIn, osg::Vec3Array* m_normalsThreadIn);
	osg::Vec3f calcNormal(int x, int y, int z);
	inline osg::Vec3f vertexInterp(float isolevel, osg::Vec3f p1, osg::Vec3f p2, float valp1, float valp2);
	void writeVertexColors(osg::Vec3Array* verticesIn, OctreeVolume<float>* octreeVolume, osg::Vec4Array* vertexColorsOut, size_t start, size_t end);
public:
	void setVolumeDimensions(const size_t x, const size_t y, const size_t z);
	void setZstepSizeAndOrthoSize(float zStepSizeIn, float orthoSizeIn);
	void calcStepSizeXY();
	void setUnifiedStepSize(double stepSize);
	void polygonize(OptimizedBoolVector* vaVec);
	void polygonizeCUDA(OptimizedBoolVector* vaVec);
	void polygonizeFloatCUDA(std::vector<std::pair<float, unsigned char>>* vaVec, unsigned char maxDepth);
	void polygonizeFloatCUDA(Octree<float>* octree);
	void polygonizeFloatCUDAHierarchical(OctreeVolume<float>* octreeVolume, bool enableTextureProcessing = false, unsigned int outputTextureSize = 0u);

	bool testGridCellDepth(const GridCell& cell, int v, int i, int j, int k, unsigned char maxDepth) const;

	//inline osg::ref_ptr<osg::OpenMeshGeometry> getOpenMeshGeometry() { return m_geometry; };
	//inline osg::OpenMeshGeometry* getOpenMeshGeometryAsOSGRefPtr() { return m_geometry; };
	//inline osg::ref_ptr<osg::Geometry> getOpenMeshGeometry() { return m_geometry; };
	inline osg::Geometry* getOpenMeshGeometry() { return m_geometry; };
};