// ClayMore - Immersive Mesh Modelling --- Copyright (c) 2014-2017 Philipp Ladwig, Jannik Fiedler, Jan Beutgen
#include "GrabRotateScaleProcessor.h"

#include "UtilProcessor.h"

#include <iostream>

//#include <osg/io_utils>



GrabRotateScaleProcessor::GrabRotateScaleProcessor(UtilProcessor* utilProcessor) : m_utilProcessor(utilProcessor)
{

}

GrabRotateScaleProcessor::~GrabRotateScaleProcessor()
{

}

//##########################################################################################################################################################
//##########################################################################################################################################################
//#####################################################################   Grab Functions   للللللللللللللل##################################################
//##########################################################################################################################################################
//##########################################################################################################################################################

void GrabRotateScaleProcessor::grabVerticesStart(osg::Vec3f grabStartPositionIn)
{
	m_grabStartPosition = grabStartPositionIn;
	//EXP: Store original geometry
	//copyAndStoreVertexArray();
}

void GrabRotateScaleProcessor::grabVertices(osg::Vec3f newPositionIn,
	bool isLocalCoordSysSelectedIn,
	bool isSnappingActive,
	Constraints constraintIn,
	osg::Matrix matrixWorldToLocal,
	osg::Vec3Array* initialVertexArray)
{
	osg::Vec3f deltaPosition = newPositionIn - m_grabStartPosition;
	//float threshold = 0.02;
	//if (!isLocalCoordSysSelectedIn)
	//{
	//	if (abs(deltaPosition.y()) < threshold && abs(deltaPosition.z()) < threshold)
	//	{
	//		deltaPosition.y() = 0.0f;
	//		deltaPosition.z() = 0.0f;
	//	}
	//	else if (abs(deltaPosition.x()) < threshold && abs(deltaPosition.z()) < threshold)
	//	{
	//		deltaPosition.x() = 0.0f;
	//		deltaPosition.z() = 0.0f;
	//	}
	//	else if (abs(deltaPosition.x()) < threshold && abs(deltaPosition.y()) < threshold)
	//	{
	//		deltaPosition.x() = 0.0f;
	//		deltaPosition.y() = 0.0f;
	//	}
	//}
	//EXP: Get the rotation of the geometry, to translate deltaPosition from global to local space

	deltaPosition = matrixWorldToLocal.getRotate() * deltaPosition;

	osg::Vec3Array *vertexArray = dynamic_cast<osg::Vec3Array*>(m_omGeom->getVertexArray());

	int i = 0;
	Mesh::VertexIter vIt, vBegin, vEnd;
	vBegin = m_omGeom->getMesh()->vertices_begin();
	vEnd = m_omGeom->getMesh()->vertices_end();
	for (vIt = vBegin; vIt != vEnd; ++vIt)
	{
		if (m_omGeom->getMesh()->status(*vIt).selected())
		{
			if (isLocalCoordSysSelectedIn)
			{
				if (isSnappingActive)
				{
					//TODO: Make an adjustable grid size - here it is fixed to 10cm..
					deltaPosition.x() = floorf(deltaPosition.x()*10.0 + 0.5f) / 10.0;

					deltaPosition.y() = floorf(deltaPosition.y()*10.0 + 0.5f) / 10.0;

					deltaPosition.z() = floorf(deltaPosition.z()*10.0 + 0.5f) / 10.0;

				}
				if (constraintIn == X_AXIS)
				{
					deltaPosition.y() = 0.0f;
					deltaPosition.z() = 0.0f;
				}
				else if (constraintIn == Y_AXIS)
				{
					deltaPosition.x() = 0.0f;
					deltaPosition.z() = 0.0f;
				}
				else if (constraintIn == Z_AXIS)
				{
					deltaPosition.x() = 0.0f;
					deltaPosition.y() = 0.0f;
				}

			}
			//NOTE: The old way of transforming (debug - 126000verts monkey - select all - grab it) -> 36 - 38fps!!! new way about 25fps...


			//osg::Vec3f  delta = m_initialTopologyAtStartOfTransform->at(i) + deltaPosition;
			//NOTE: Without delta 24.8fps, with delta ~24.7fps
			vertexArray->at(i) = initialVertexArray->at(i) + deltaPosition;;

			//NOTE: 17fps -> no difference between vIt.handle(), vIt und *vIt
			//mesh.set_point(*vIt,m_initialTopologyAtStartOfTransform->at(i) + deltaPosition);
		}
		i++;
	}
	vertexArray->dirty();
}


//##########################################################################################################################################################
//##########################################################################################################################################################
//#####################################################################   Grab Functions   للللللللللللللل##################################################
//##########################################################################################################################################################
//##########################################################################################################################################################

void GrabRotateScaleProcessor::grabPropEditStart(osg::Vec3f grabStartPosition, float radius, float shape)
{
	m_grabStartPosition = grabStartPosition;

	m_selectedPropEditVerts.resize(m_omGeom->getMesh()->n_vertices());
	m_distanceFactorsForPropEdit.resize(m_omGeom->getMesh()->n_vertices());

	Mesh::VertexHandle vSelected;
	Mesh::VertexIter vIt, vBegin, vEnd;
	vBegin = m_omGeom->getMesh()->vertices_begin();
	vEnd = m_omGeom->getMesh()->vertices_end();

	//ToDo: What to do when multiple points are selected? Barycentrum?
	//Check which point is selected and store this as a vertexHandle
	for (vIt = vBegin; vIt != vEnd; ++vIt)
	{
		if (m_omGeom->getMesh()->status(*vIt).selected())
		{
			vSelected = *vIt;
		}
	}

	//Determine distanceFactors - this factor is the value between 0 and 1 which indicates how far away is a point of the mesh to the single selected point
	//When value is near by 1 - the related point will be more transformed than a value near by 0
	if (vSelected.is_valid())
	{
		int i = 0;
		for (vIt = vBegin; vIt != vEnd; ++vIt, i++)
		{
			m_selectedPropEditVerts.at(i) = false;
			if (m_omGeom->getMesh()->status(*vIt).selected())
			{
				m_selectedPropEditVerts.at(i) = true;
				m_distanceFactorsForPropEdit.at(i) = 1.0f;
			}
			else
			{
				float distance = m_utilProcessor->calculateDistance(vIt, vSelected);
				if (distance <= radius)
				{
					m_selectedPropEditVerts.at(i) = true;
					m_distanceFactorsForPropEdit.at(i) = 1 - ((pow(distance, shape) / pow(radius, shape)));
				}
			}
		}
	}
}

void GrabRotateScaleProcessor::grabPropEdit(osg::Vec3f newPositionIn,
	bool isLocalCoordSysSelectedIn,
	Constraints constraintIn,
	osg::Matrix matrixWorldToLocal,
	osg::Vec3Array* initialVertexArray)
{
	osg::Vec3f deltaPosition = newPositionIn - m_grabStartPosition;
	if (!isLocalCoordSysSelectedIn)
	{
		if (constraintIn == X_AXIS)
		{
			deltaPosition.y() = 0.0f;
			deltaPosition.z() = 0.0f;
		}
		else if (constraintIn == Y_AXIS)
		{
			deltaPosition.x() = 0.0f;
			deltaPosition.z() = 0.0f;
		}
		else if (constraintIn == Z_AXIS)
		{
			deltaPosition.x() = 0.0f;
			deltaPosition.y() = 0.0f;
		}
	}
	//EXP: Get the rotation of the geometry, to translate deltaPosition from global to local space
	deltaPosition = matrixWorldToLocal.getRotate() * deltaPosition;

	osg::Vec3Array *vertexArray = dynamic_cast<osg::Vec3Array*>(m_omGeom->getVertexArray());

	int i = 0;
	Mesh::VertexIter vIt, vBegin, vEnd;
	vBegin = m_omGeom->getMesh()->vertices_begin();
	vEnd = m_omGeom->getMesh()->vertices_end();
	for (vIt = vBegin; vIt != vEnd; ++vIt)
	{
		if (m_selectedPropEditVerts.at(i))
		{
			if (isLocalCoordSysSelectedIn)
			{
				if (constraintIn == X_AXIS)
				{
					deltaPosition.y() = 0.0f;
					deltaPosition.z() = 0.0f;
				}
				else if (constraintIn == Y_AXIS)
				{
					deltaPosition.x() = 0.0f;
					deltaPosition.z() = 0.0f;
				}
				else if (constraintIn == Z_AXIS)
				{
					deltaPosition.x() = 0.0f;
					deltaPosition.y() = 0.0f;
				}
				vertexArray->at(i) = initialVertexArray->at(i) + deltaPosition * m_distanceFactorsForPropEdit.at(i);
			}
		}
		i++;
	}
	vertexArray->dirty();
}

void GrabRotateScaleProcessor::grabProbEditSetValues(float radiusIn, float shapeIn)
{
	m_selectedPropEditVerts.resize(m_omGeom->getMesh()->n_vertices());
	m_distanceFactorsForPropEdit.resize(m_omGeom->getMesh()->n_vertices());

	Mesh::VertexHandle vSelected;
	Mesh::VertexIter vIt, vBegin, vEnd;
	vBegin = m_omGeom->getMesh()->vertices_begin();
	vEnd = m_omGeom->getMesh()->vertices_end();

	//Check which point is selected and store this as a vertexHandle
	for (vIt = vBegin; vIt != vEnd; ++vIt)
	{
		if (m_omGeom->getMesh()->status(*vIt).selected())
		{
			vSelected = *vIt;
		}
	}

	if (vSelected.is_valid())
	{
		int i = 0;
		for (vIt = vBegin; vIt != vEnd; ++vIt, i++)
		{
			m_selectedPropEditVerts.at(i) = false;
			if (m_omGeom->getMesh()->status(*vIt).selected())
			{
				m_selectedPropEditVerts.at(i) = true;
				m_distanceFactorsForPropEdit.at(i) = 1.0f;
			}
			else
			{
				float distance = m_utilProcessor->calculateDistance(vIt, vSelected);
				if (distance <= radiusIn)
				{
					m_selectedPropEditVerts.at(i) = true;
					m_distanceFactorsForPropEdit.at(i) = 1 - ((pow(distance, shapeIn) / pow(radiusIn, shapeIn)));
				}
			}
		}
	}
}

//##########################################################################################################################################################
//##########################################################################################################################################################
//############################################################   Rotate Functions   للللللللللللللل#########################################################
//##########################################################################################################################################################
//##########################################################################################################################################################

//Test
void GrabRotateScaleProcessor::rotateVertices(osg::Matrixd rotMatIn, bool isSnappingActive, Constraints constraintIn, osg::Vec3d barycentrum, osg::Vec3Array* initialVertexArray)
{
	Mesh::VertexIter vIt, vBegin, vEnd;
	vBegin = m_omGeom->getMesh()->vertices_begin();
	vEnd = m_omGeom->getMesh()->vertices_end();

	osg::Quat justAQuat = rotMatIn.getRotate();

	osg::Matrixd transMatrix;

	transMatrix.makeTranslate(barycentrum);

	//Snapping value in rad
	double snappingValue = M_PI_2 / 6.0; //15 degrees

	if (constraintIn == X_AXIS)
	{
		//Set the y and z component to zero and pass only the values of...
		justAQuat.y() = 0.0;
		justAQuat.z() = 0.0;

		//x and w, but normalize the Quat again with the sqrt()
		double mag = sqrt(justAQuat.w() * justAQuat.w() + justAQuat.x() * justAQuat.x());
		justAQuat.x() /= mag;
		justAQuat.w() /= mag;

		//If snapping is active, snap to every "snappingValue" in [rad]. Therefore convert in rad, snap in rad an convert it back to between +1.0 and -1.0
		if (isSnappingActive)
		{
			double angX = 2 * acos(justAQuat.x());
			double snappedAngX = (floorf((angX / snappingValue) + 0.5) * snappingValue);
			justAQuat.x() = cos(snappedAngX / 2.0);
		}
	}
	else if (constraintIn == Y_AXIS)
	{
		justAQuat.x() = 0.0;
		justAQuat.z() = 0.0;

		double mag = sqrt(justAQuat.w() * justAQuat.w() + justAQuat.y() * justAQuat.y());
		justAQuat.y() /= mag;
		justAQuat.w() /= mag;

		if (isSnappingActive)
		{
			double angY = 2 * acos(justAQuat.y());
			double snappedAngY = (floorf((angY / snappingValue) + 0.5) * snappingValue);
			justAQuat.y() = cos(snappedAngY / 2.0);
		}

	}
	else if (constraintIn == Z_AXIS)
	{
		justAQuat.x() = 0.0;
		justAQuat.y() = 0.0;

		double mag = sqrt(justAQuat.w() * justAQuat.w() + justAQuat.z() * justAQuat.z());
		justAQuat.z() /= mag;
		justAQuat.w() /= mag;

		if (isSnappingActive)
		{
			double angZ = 2 * acos(justAQuat.z());
			double snappedAngZ = (floorf((angZ / snappingValue) + 0.5) * snappingValue);
			justAQuat.z() = cos(snappedAngZ / 2.0);
		}
	}
	else if (constraintIn == NO_CONSTRAIN)
	{
		if (isSnappingActive)
		{
			double angX = 2 * acos(justAQuat.x());
			double snappedAngX = (floorf((angX / snappingValue) + 0.5) * snappingValue);
			justAQuat.x() = cos(snappedAngX / 2.0);

			double angY = 2 * acos(justAQuat.y());
			double snappedAngY = (floorf((angY / snappingValue) + 0.5) * snappingValue);
			justAQuat.y() = cos(snappedAngY / 2.0);

			double angZ = 2 * acos(justAQuat.z());
			double snappedAngZ = (floorf((angZ / snappingValue) + 0.5) * snappingValue);
			justAQuat.z() = cos(snappedAngZ / 2.0);
		}
	}

	//Code reducing: If snapping active, always the w component have to be snapped as well (necessary for each x, y, z)
	if (isSnappingActive)
	{
		double angW = 2 * acos(justAQuat.w());
		double snappedAngW = (floorf((angW / snappingValue) + 0.5) * snappingValue);
		justAQuat.w() = cos(snappedAngW / 2.0);
	}

	rotMatIn.makeIdentity();
	rotMatIn.makeRotate(justAQuat);

	for (vIt = vBegin; vIt != vEnd; ++vIt)
	{
		//if (selectedVerticesIn.at(i))
		if (m_omGeom->getMesh()->status(*vIt).selected())
		{
			//initialVertexArray is necessary (and not m_omGeom->getMesh()->point(*vIt)
			//because otherwise it would add every frame a rotation -> results in an undesired and uncontrolled rotation
			m_omGeom->getMesh()->set_point(*vIt, initialVertexArray->at(vIt->idx()) * (osg::Matrix::inverse(transMatrix) * rotMatIn * transMatrix));
		}
	}
	m_omGeom->getVertexArray()->dirty();
}

void GrabRotateScaleProcessor::rotateEntireGeometry(osg::Matrixd rotMatIn, osg::Vec3Array* initialVertexArray, osg::Vec3d barycentrum)
{
	Mesh::VertexIter vIt, vBegin, vEnd;
	vBegin = m_omGeom->getMesh()->vertices_begin();
	vEnd = m_omGeom->getMesh()->vertices_end();

	osg::Quat justAQuat = rotMatIn.getRotate();

	osg::Matrixd transMatrix;

	//TODO: If m_utilProcessor->getBarycentrumOfAllVerts() will work fine and there are no glitches, use this, instead of calc in the observer the barycentrum
	//transMatrix.makeTranslate(barycentrum);
	transMatrix.makeTranslate(barycentrum);

	Constraints constraintIn = Y_AXIS;
	bool isSnappingActive = true;

	//Snapping value in rad
	double snappingValue = M_PI_4; //45 degrees

	if (constraintIn == X_AXIS)
	{
		//Set the y and z component to zero and pass only the values of...
		justAQuat.y() = 0.0;
		justAQuat.z() = 0.0;

		//x and w, but normalize the Quat again with the sqrt()
		double mag = sqrt(justAQuat.w() * justAQuat.w() + justAQuat.x() * justAQuat.x());
		justAQuat.x() /= mag;
		justAQuat.w() /= mag;

		//If snapping is active, snap to every "snappingValue" in [rad]. Therefore convert in rad, snap in rad an convert it back to between +1.0 and -1.0
		if (isSnappingActive)
		{
			double angX = 2 * acos(justAQuat.x());
			double snappedAngX = (floorf((angX / snappingValue) + 0.5) * snappingValue);
			justAQuat.x() = cos(snappedAngX / 2.0);
		}
	}
	else if (constraintIn == Y_AXIS)
	{
		justAQuat.x() = 0.0;
		justAQuat.z() = 0.0;

		double mag = sqrt(justAQuat.w() * justAQuat.w() + justAQuat.y() * justAQuat.y());
		justAQuat.y() /= mag;
		justAQuat.w() /= mag;

		if (isSnappingActive)
		{
			double angY = 2 * acos(justAQuat.y());
			double snappedAngY = (floorf((angY / snappingValue) + 0.5) * snappingValue);
			justAQuat.y() = cos(snappedAngY / 2.0);
		}

	}
	else if (constraintIn == Z_AXIS)
	{
		justAQuat.x() = 0.0;
		justAQuat.y() = 0.0;

		double mag = sqrt(justAQuat.w() * justAQuat.w() + justAQuat.z() * justAQuat.z());
		justAQuat.z() /= mag;
		justAQuat.w() /= mag;

		if (isSnappingActive)
		{
			double angZ = 2 * acos(justAQuat.z());
			double snappedAngZ = (floorf((angZ / snappingValue) + 0.5) * snappingValue);
			justAQuat.z() = cos(snappedAngZ / 2.0);
		}
	}

	//Code reducing: If snapping active, always the w component have to be snapped as well (necessary for each x, y, z)
	if (isSnappingActive)
	{
		double angW = 2 * acos(justAQuat.w());
		double snappedAngW = (floorf((angW / snappingValue) + 0.5) * snappingValue);
		justAQuat.w() = cos(snappedAngW / 2.0);
	}

	rotMatIn.makeIdentity();
	rotMatIn.makeRotate(justAQuat);

	if (initialVertexArray->getNumElements() == m_omGeom->getVertexArray()->getNumElements())
	{
		for (vIt = vBegin; vIt != vEnd; ++vIt)
		{
			//initialVertexArray is necessary (and not m_omGeom->getMesh()->point(*vIt)
			//because otherwise it would add every frame a rotation -> results in an undesired and uncontrolled rotation
			m_omGeom->getMesh()->set_point(*vIt, initialVertexArray->at(vIt->idx()) * (osg::Matrix::inverse(transMatrix) * rotMatIn * transMatrix));
		}
	}
	m_omGeom->getVertexArray()->dirty();
}

//##########################################################################################################################################################
//##########################################################################################################################################################
//############################################################   Scale Functions   للللللللللللللل##########################################################
//##########################################################################################################################################################
//##########################################################################################################################################################

//TODO: I get relative values from scaleValueIn, like -0.064. Relative is bad, because we cant determine how far we have already scaled the geometry.
//		When we get absolute values, we can calculate every time the new position of a vertex and for this we can use the originalTopology.
//		When we use the originalTopology, we can set it easily back, when we cancel the transformation. But this is not the real benefit:
//		Everytime when we switch the axes, we can restore the originalTopology easily.... but , to be honest, this is almost the same application like this before
//		Anyway, I will move the grab functionality in this class. I think I need an originalTopology as VertexArray or the entiry Mesh from this class. Both as deep copy.
//		First I have to figure out how big is mesh and how big will be a vertexArray. Is the mesh much bigger? Maybe it is better to take a mesh anyway, 
//		because we can restore normals, UVCoords and all the stuff, if that is included in mesh...
//		When we take mesh, I have to figure out how to make a deep copy of the mesh object..
void GrabRotateScaleProcessor::scaleVertices(float scaleValueIn, bool isLocalIn, bool isSnappingActive, Constraints constraintIn, osg::Matrix matrixWorldToLocalIn, osg::Matrix matrixLocalToWorldIn, osg::Vec3f barycentrum)
{
	Mesh::VertexIter vIt, vBegin, vEnd;

	vBegin = m_omGeom->getMesh()->vertices_begin();
	vEnd = m_omGeom->getMesh()->vertices_end();

	if (isLocalIn)
	{
		for (vIt = vBegin; vIt != vEnd; ++vIt)
		{
			//if (selectedVerticesIn.at(i))
			if (m_omGeom->getMesh()->status(*vIt).selected())
			{
				osg::Vec3d newPosition;
				osg::Vec3d vectorToBarycentrum;
				vectorToBarycentrum = barycentrum - m_omGeom->getMesh()->point(*vIt);

				if (isSnappingActive)
				{
					//TODO: I think I have to write a similar function like grab(), because I need absolute values depending on the originalMesh. Currently it is relative... 
					//I take the change from frame to frame into account....

					////std::cout << "x: " << floorf((vectorToBarycentrum.x() * scaleValueIn)*10.0 + 0.5f) / 10.0 << std::endl;
					//std::cout << "vectorToBarycentrum.x() * scaleValueIn: " << vectorToBarycentrum.x() * scaleValueIn << std::endl;
					//
					//newPosition.x() = floorf((vectorToBarycentrum.x() * scaleValueIn)*10.0 + 0.5f) / 10.0;

					////std::cout << "y: " << floorf((vectorToBarycentrum.y() * scaleValueIn)*10.0 + 0.5f) / 10.0 << std::endl;
					//newPosition.y() = floorf((vectorToBarycentrum.y() * scaleValueIn)*10.0 + 0.5f) / 10.0;

					////std::cout << "z: " << floorf((vectorToBarycentrum.z() * scaleValueIn)*10.0 + 0.5f) / 10.0 << std::endl;
					//newPosition.z() = floorf((vectorToBarycentrum.z() * scaleValueIn)*10.0 + 0.5f) / 10.0;
				}

				if (constraintIn == NO_CONSTRAIN)
				{
					newPosition = vectorToBarycentrum * scaleValueIn;
				}
				else if (constraintIn == X_AXIS)
				{
					newPosition.x() = vectorToBarycentrum.x() * scaleValueIn;
					newPosition.y() = 0.0f;
					newPosition.z() = 0.0f;
				}
				else if (constraintIn == Y_AXIS)
				{
					newPosition.x() = 0.0f;
					newPosition.y() = vectorToBarycentrum.y() * scaleValueIn;
					newPosition.z() = 0.0f;
				}
				else if (constraintIn == Z_AXIS)
				{
					newPosition.x() = 0.0f;
					newPosition.y() = 0.0f;
					newPosition.z() = vectorToBarycentrum.z() * scaleValueIn;
				}

				//m_initialTopologyAtStartOfTransform
				m_omGeom->getMesh()->set_point(*vIt, m_omGeom->getMesh()->point(*vIt) + (newPosition));
			}
		}
	}
	//FIXME: global scaling dont work at this time - selection of global axes are disabled within the event "Hydra_Button_Joystick_Controller1"
	else
	{
		for (vIt = vBegin; vIt != vEnd; ++vIt)
		{
			//if (selectedVerticesIn.at(i))
			if (m_omGeom->getMesh()->status(*vIt).selected())
			{
				osg::Vec3d newPosition;
				osg::Vec3d vectorToBarycentrum;
				vectorToBarycentrum = barycentrum - m_omGeom->getMesh()->point(*vIt);
				vectorToBarycentrum = vectorToBarycentrum * matrixLocalToWorldIn;
				if (constraintIn == NO_CONSTRAIN)
				{
					newPosition = vectorToBarycentrum * scaleValueIn;
				}
				else if (constraintIn == X_AXIS)
				{
					newPosition.x() = vectorToBarycentrum.x() * scaleValueIn;
					newPosition.y() = 0.0f;
					newPosition.z() = 0.0f;
				}
				else if (constraintIn == Y_AXIS)
				{
					newPosition.x() = 0.0f;
					newPosition.y() = vectorToBarycentrum.y() * scaleValueIn;
					newPosition.z() = 0.0f;
				}
				else if (constraintIn == Z_AXIS)
				{
					newPosition.x() = 0.0f;
					newPosition.y() = 0.0f;
					newPosition.z() = vectorToBarycentrum.z() * scaleValueIn;
				}
				//newPosition = newPosition * matrixWorldToLocal;
				m_omGeom->getMesh()->set_point(*vIt, m_omGeom->getMesh()->point(*vIt) + newPosition);
			}
		}
	}
	m_omGeom->getVertexArray()->dirty();
}

void GrabRotateScaleProcessor::resetScaleGeometry(osg::Vec3Array* initialVertexArray)
{
	Mesh::VertexIter vIt, vBegin, vEnd;
	vBegin = m_omGeom->getMesh()->vertices_begin();
	vEnd = m_omGeom->getMesh()->vertices_end();
	for (vIt = vBegin; vIt != vEnd; ++vIt)
	{
		//if (selectedVerticesIn.at(i))
		if (m_omGeom->getMesh()->status(*vIt).selected())
		{
			//initialVertexArray is necessary (and not m_omGeom->getMesh()->point(*vIt)
			//because otherwise it would add every frame a rotation -> results in an undesired and uncontrolled rotation
			m_omGeom->getMesh()->set_point(*vIt, initialVertexArray->at(vIt->idx()));
		}
	}
	m_omGeom->getVertexArray()->dirty();
}

void GrabRotateScaleProcessor::scaleEntireGeometryBasedOnBaryOfSELECTEDVerts(float valueIn, osg::Vec3Array* initialVertexArray)
{
	osg::Vec3f barycentrum = m_utilProcessor->getBarycentrumOfSELECTEDVerts();

	Mesh::VertexIter vIt, vBegin, vEnd;

	vBegin = m_omGeom->getMesh()->vertices_begin();
	vEnd = m_omGeom->getMesh()->vertices_end();

	for (vIt = vBegin; vIt != vEnd; ++vIt)
	{
		osg::Vec3d newPosition;
		osg::Vec3d vectorToBarycentrum;
		vectorToBarycentrum = barycentrum - m_omGeom->getMesh()->point(*vIt);

		newPosition = vectorToBarycentrum * valueIn;

		m_omGeom->getMesh()->set_point(*vIt, m_omGeom->getMesh()->point(*vIt) + (newPosition));
	}

	m_omGeom->getVertexArray()->dirty();
}

void GrabRotateScaleProcessor::scaleEntireGeometryBasedOnBaryOfALLVerts(float valueIn, osg::Vec3Array* initialVertexArray)
{
	osg::Vec3f barycentrum = m_utilProcessor->getBarycentrumOfALLVerts();

	//std::cout << barycentrum << std::endl;

	Mesh::VertexIter vIt, vBegin, vEnd;

	vBegin = m_omGeom->getMesh()->vertices_begin();
	vEnd = m_omGeom->getMesh()->vertices_end();

	for (vIt = vBegin; vIt != vEnd; ++vIt)
	{
		osg::Vec3d newPosition;
		osg::Vec3d vectorToBarycentrum;
		vectorToBarycentrum = barycentrum - m_omGeom->getMesh()->point(*vIt);

		newPosition = vectorToBarycentrum * valueIn;

		m_omGeom->getMesh()->set_point(*vIt, m_omGeom->getMesh()->point(*vIt) + (newPosition));
	}

	m_omGeom->getVertexArray()->dirty();
}
