// ClayMore - Immersive Mesh Modelling --- Copyright (c) 2014-2017 Philipp Ladwig, Jannik Fiedler, Jan Beutgen
#include "SubdivideProcessor.h"

#include "UtilProcessor.h"

#include "iostream"

#include <OpenMesh/Tools/Subdivider/Uniform/LoopT.hh>
#include <OpenMesh/Tools/Subdivider/Uniform/CompositeLoopT.hh>
#include <OpenMesh/Tools/Subdivider/Uniform/ModifiedButterFlyT.hh>
#include <OpenMesh/Tools/Subdivider/Uniform/LongestEdgeT.hh>

SubdivideProcessor::SubdivideProcessor(UtilProcessor* utilProcessor) : m_utilProcessor(utilProcessor)
{

}

SubdivideProcessor::~SubdivideProcessor()
{

}

//WIP:Error-Prone -> Actually this works only on boundary vertices, but there is no query for it
void SubdivideProcessor::subdivide()
{
	//// Initialize subdivider
	//OpenMesh::Subdivider::Uniform::LoopT<Mesh> loopDevide;
	//// Execute 3 subdivision steps
	//loopDevide.attach(*m_omGeom->getMesh());
	//loopDevide(3);
	//loopDevide.detach();


	//// Initialize subdivider
	//OpenMesh::Subdivider::Uniform::CompositeLoopT<Mesh> loopDevide;
	//// Execute 3 subdivision steps
	//loopDevide.attach(*m_omGeom->getMesh());
	//loopDevide(3);
	//loopDevide.detach();

	//// Initialize subdivider
	//OpenMesh::Subdivider::Uniform::ModifiedButterflyT<Mesh> loopDevide;
	//// Execute 3 subdivision steps
	//loopDevide.attach(*m_omGeom->getMesh());
	//loopDevide(3);
	//loopDevide.detach();

	// Initialize subdivider
	//OpenMesh::Subdivider::Uniform::LongestEdgeT<Mesh> loopDevide;
	//// Execute 3 subdivision steps
	//loopDevide.attach(*m_omGeom->getMesh());
	//loopDevide(3);
	//loopDevide.detach();
}