// ClayMore - Immersive Mesh Modelling --- Copyright (c) 2014-2017 Philipp Ladwig, Jannik Fiedler, Jan Beutgen
#pragma once

#include "ConfigDllExportGeometryProcessing.h"
#include "Processor.h"

class UtilProcessor;

class imGeometryProcessing_DLL_import_export SculptProcessor : public Processor
{

public:
	// #### CONSTRUCTOR & DESTRUCTOR ###############
	SculptProcessor(UtilProcessor* utilProcessor);
	~SculptProcessor();


	// #### MEMBER VARIABLES ###############
private:
	typedef OpenMesh::OSG_TriMesh_BindableArrayKernelT Mesh;
	UtilProcessor* m_utilProcessor;
	std::vector<float> m_sculptingDistanceProgessUntilMax;

	// #### MEMBER FUNCTIONS ###############
public:
	inline virtual void setActiveGeometry(osg::ref_ptr<osg::OpenMeshGeometry> omGeom) { m_omGeom = omGeom; }
	void sculptStart(float radiusIn, float strengthIn);
	void SculptProcessor::sculptAddOrSubtract(float radiusIn,
		float strengthIn,
		osg::Vec3 cursorTrans,
		osg::Matrix geomLocal2World,
		bool isAddBrush,
		osg::Vec3Array* initialVertexArray);
};


