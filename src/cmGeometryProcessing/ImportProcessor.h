// ClayMore - Immersive Mesh Modelling --- Copyright (c) 2014-2017 Philipp Ladwig, Jannik Fiedler, Jan Beutgen
#pragma once

#include "ConfigDllExportGeometryProcessing.h"
#include "Processor.h"

class UtilProcessor;

class imGeometryProcessing_DLL_import_export ImportProcessor : public Processor
{

public:
	// #### CONSTRUCTOR & DESTRUCTOR ###############
	ImportProcessor(UtilProcessor* utilProcessor);
	~ImportProcessor();


	// #### MEMBER VARIABLES ###############
private:
	typedef OpenMesh::OSG_TriMesh_BindableArrayKernelT Mesh;
	UtilProcessor* m_utilProcessor;


	// #### MEMBER FUNCTIONS ###############
public:
	inline virtual void setActiveGeometry(osg::ref_ptr<osg::OpenMeshGeometry> omGeom) { m_omGeom = omGeom; }
	void readFileFromDisk(std::string readPath);
};


