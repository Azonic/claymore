// ClayMore - Immersive Mesh Modelling --- Copyright (c) 2014-2017 Philipp Ladwig, Jannik Fiedler, Jan Beutgen
#include "ExtrudeProcessor.h"

#include "UtilProcessor.h"

#include "iostream"

ExtrudeProcessor::ExtrudeProcessor(UtilProcessor* utilProcessor) : m_utilProcessor(utilProcessor)
{

}

ExtrudeProcessor::~ExtrudeProcessor()
{

}

//WIP:Error-Prone -> Actually this works only on boundary vertices, but there is no query for it
void ExtrudeProcessor::extrude()
{
	//FIXME: Check, if something is really selected
	std::map<Mesh::VertexHandle, Mesh::VertexHandle> extrudeVertexMap;
	std::vector<std::vector<Mesh::VertexHandle> > extrudedQuads;
	std::vector<Mesh::VertexHandle> alreadyProcessedVerts;

	Mesh::VertexIter vIt, vBegin, vEnd;
	vBegin = m_omGeom->getMesh()->vertices_begin();
	vEnd = m_omGeom->getMesh()->vertices_end();
	for (vIt = vBegin; vIt != vEnd; ++vIt)
	{
		if (m_omGeom->getMesh()->status(*vIt).selected())
		{
			extrudeVertexMap[*vIt] = m_omGeom->getMesh()->add_vertex(m_omGeom->getMesh()->point(*vIt) * 1.001f);
		}
	}

	//Go again over all verts in mesh...
	unsigned int numExtrudedQuads = 0;
	for (vIt = vBegin; vIt != vEnd; ++vIt)
	{
		//...and if it is selected...
		if (m_omGeom->getMesh()->status(*vIt).selected())
		{
			//...then create an vertexIter and look around on each neighboor verts....
			Mesh::VertexVertexIter vvEnd = m_omGeom->getMesh()->vv_end(*vIt);
			for (Mesh::VertexVertexIter vvIter = m_omGeom->getMesh()->vv_iter(*vIt); vvIter != vvEnd; ++vvIter)
			{
				bool isAlreadyProcessed = false;
				//Das macht keinen sinn...
				//Wenn ich hier rein gehe, ist alreadyProcessedVerts immer 0!
				//Das bedeutet er geht einmal durch
				//Aber unten steht ja noch mehr und das ist eine for schleife
				for (unsigned int ente = 0; ente < alreadyProcessedVerts.size(); ente++)
				{
					if (*vvIter == alreadyProcessedVerts.at(ente))
					{
						isAlreadyProcessed = true;
					}
				}

				//Ich mach hier mit mal weiter: 
				//Wenn der mesh status selectiert ist und dieser Vertex noch nicht verarbeitet wurden ist..
				//Also das bedeutet, das ist der nachbar vertex, welcher auch selektiert aber nicht extrudiert ist
				if (m_omGeom->getMesh()->status(*vvIter).selected() && !isAlreadyProcessed)
				{
					//Wenn er selektiert ist und noch nicht verarbeitet, dann
					//f�lle nun das QuadSpeicherfeld-> Hier wird frischer Vector mit VertexHandlen gepusht
					extrudedQuads.push_back(std::vector<Mesh::VertexHandle>());
					//Und es wird nach und nach der selektiert Vert..
					extrudedQuads.at(numExtrudedQuads).push_back(*vIt);
					//..der zugeh�rige extrudierte Vert...
					extrudedQuads.at(numExtrudedQuads).push_back(extrudeVertexMap[*vIt]);
					//...der erkannte, selektierte (aber noch nicht verarbeitete) NachbarVert...
					extrudedQuads.at(numExtrudedQuads).push_back(*vvIter);
					//..mit seinem extrudiertem Vert
					extrudedQuads.at(numExtrudedQuads).push_back(extrudeVertexMap[*vvIter]);
					//Dann z�hle die anzahl der Quads hoch
					numExtrudedQuads++;
					//Und f�ge den verarbeiteten Vert zu den im n�chsten loop ausgeschlossenen
					alreadyProcessedVerts.push_back(*vIt);
					//Aber das bedeutet, dass wenn ich einmal im Kreis laufe, ich den hier garnict mehr als sekund�ren Verts mit verwurste?
				}
			}
		}
	}
	//Nun habe ich eine Sammlung aller Vertices mit Beziehungen, welche zu extrudieren sind. 
	//In den Beziehungen steht, welche die alten Verts und die neuen sind und welche benachbart sind
	//Wie f�ge ich jetzt aber dem Graphen die neuen Verts in ccw hinzu?

	//Berechne das Baryzentrm der Quads.
	//Was kann ich nun damit machen? K�nnte ich mich nicht daran orientieren, wie die schon verbundenen Verts orienteirt sind?
	//Ich habe Beziehungen als Edges zwischen zwei Verts in den Quads. Wenn ich heruasfinde, in welcher Richtung diese Verst zusammenh�ngen
	//kann ich den Rest mit dieser Information zusammenf�gen. Daf�r brauche ich kein Baryzentrum.
	//Eine Funktion k�nnte bereits unten stehen, wie ich das boundary Edge herausfinde!

	//mesh.add_face(extrudedQuads[0]);
	Mesh::VertexIter vIt2;
	for (vIt2 = vBegin; vIt2 != vEnd; ++vIt2)
	{
		m_omGeom->getMesh()->status(*vIt2).set_selected(false);
	}

	for (unsigned int i = 0; i < numExtrudedQuads; i++)
	{
		std::cout << "in face add schleife" << std::endl;
		std::cout << "numExtrudedQuads: " << numExtrudedQuads << std::endl;
		Mesh::HalfedgeHandle hh = m_omGeom->getMesh()->find_halfedge(extrudedQuads[i].at(0), extrudedQuads[i].at(2));
		Mesh::HalfedgeHandle hh2 = m_omGeom->getMesh()->find_halfedge(extrudedQuads[i].at(2), extrudedQuads[i].at(0));

		if (hh.is_valid())
		{
			std::cout << "hh is boundary" << std::endl;
			m_omGeom->getMesh()->add_face(extrudedQuads[i].at(0), extrudedQuads[i].at(2), extrudedQuads[i].at(3));
			//m_utilProcessor->bruteForceAddFace(extrudedQuads[i].at(0), extrudedQuads[i].at(2), extrudedQuads[i].at(3));
			std::cout << "first Point added" << std::endl;
			m_omGeom->getMesh()->add_face(extrudedQuads[i].at(3), extrudedQuads[i].at(1), extrudedQuads[i].at(0));
			//m_utilProcessor->bruteForceAddFace(extrudedQuads[i].at(3), extrudedQuads[i].at(1), extrudedQuads[i].at(0));
			std::cout << "second Point added" << std::endl;
			m_omGeom->getMesh()->status(extrudedQuads[i].at(1)).set_selected(true);
			m_omGeom->getMesh()->status(extrudedQuads[i].at(3)).set_selected(true);
		}

		if (hh2.is_valid())
		{
			std::cout << "hh2 is boundary" << std::endl;
			//m_utilProcessor->bruteForceAddFace(extrudedQuads[i].at(3), extrudedQuads[i].at(0), extrudedQuads[i].at(1));
			m_omGeom->getMesh()->add_face(extrudedQuads[i].at(3), extrudedQuads[i].at(0), extrudedQuads[i].at(1));
			std::cout << "first Point added" << std::endl;
			//m_utilProcessor->bruteForceAddFace(extrudedQuads[i].at(3), extrudedQuads[i].at(2), extrudedQuads[i].at(0));
			m_omGeom->getMesh()->add_face(extrudedQuads[i].at(3), extrudedQuads[i].at(2), extrudedQuads[i].at(0));
			std::cout << "second Point added" << std::endl;
			m_omGeom->getMesh()->status(extrudedQuads[i].at(1)).set_selected(true);
			m_omGeom->getMesh()->status(extrudedQuads[i].at(3)).set_selected(true);
		}
	}
	m_omGeom->translateSelOMStatusBitToColor();


	//Old first try:
	//Mesh::VertexIter vIt, vBegin, vEnd;
	//vBegin = mesh.vertices_begin();
	//vEnd = mesh.vertices_end();

	//Mesh::VertexHandle alreadyProcessedVertex;
	//for (vIt = vBegin; vIt != vEnd; ++vIt)
	//{
	//	if (mesh.status(*vIt).selected() && alreadyProcessedVertex != vIt)
	//	{
	//		for (Mesh::VertexVertexIter vvIter = mesh.vv_iter(*vIt); vvIter; ++vvIter)
	//		{
	//			if (mesh.status(*vvIter).selected())
	//			{
	//				//found second selected vertex
	//				//duplicate the selection
	//				Mesh::VertexHandle vertexC = mesh.add_vertex(mesh.point(*vIt) * 1.1f);
	//				Mesh::VertexHandle vertexD = mesh.add_vertex(mesh.point(*vvIter) * 1.1f);
	//				std::cout << "added points" << std::endl;

	//				Mesh::HalfedgeHandle hh = mesh.find_halfedge(*vIt, *vvIter);
	//				Mesh::HalfedgeHandle hh2 = mesh.find_halfedge(*vvIter, *vIt);

	//				if (mesh.is_boundary(hh))
	//				{
	//					std::cout << "hh is boundary" << std::endl;
	//					mesh.add_face(*vIt, *vvIter, vertexD);
	//					std::cout << "first Point added" << std::endl;
	//					mesh.add_face(vertexD, vertexC, *vIt);
	//					std::cout << "second Point added" << std::endl;
	//				}

	//				if (mesh.is_boundary(hh2))
	//				{
	//					std::cout << "hh2 is boundary" << std::endl;
	//					mesh.add_face(vertexD, *vIt, vertexC);
	//					std::cout << "first Point added" << std::endl;
	//					mesh.add_face(vertexD, *vvIter, *vIt);
	//					std::cout << "second Point added" << std::endl;
	//				}

	//				//add new faces with respect to the right order of the added vertices


	//				//Dont do it again:
	//				alreadyProcessedVertex = vvIter;

	//				//....activate grab mode....
	//				Mesh::VertexIter vIt2;
	//				for (vIt2 = vBegin; vIt2 != vEnd; ++vIt2)
	//				{
	//					mesh.status(*vIt2).set_selected(false);
	//				}

	//				mesh.status(vertexC).set_selected(true);
	//				mesh.status(vertexD).set_selected(true);
	//				translateSelOMStatusBitToColor();
	//				break;
	//			}
	//		}
	//	}
	//}

	//FIXME: I think I need this more than one time ->make an extra function for this and reduce code
	//size_t nF;
	//std::vector<OpenMesh::VertexHandle> vhandles;
	//unsigned int idx;
	//for (int i = 0, nF = m_omGeom->getMesh()->n_faces(); i < nF; ++i)
	//{
	//	vhandles.clear();
	//	for (Mesh::CFVIter fv_it = m_omGeom->getMesh()->cfv_iter(OpenMesh::FaceHandle(int(i))); fv_it.is_valid(); ++fv_it)
	//	{
	//		vhandles.push_back(*fv_it);
	//	}

	//	std::vector<int> indiciesCleanedUp;
	//	for (int j = 0; j < vhandles.size(); ++j)
	//	{
	//		idx = vhandles[j].idx(); //+ 1;
	//		indiciesCleanedUp.push_back(idx);
	//	}
	//	m_utilProcessor->sync_indices((int)i, indiciesCleanedUp[0], indiciesCleanedUp[1], indiciesCleanedUp[2]);
	//}

	////TODO: Agree on which type of calcNormals we should use
	//m_omGeom->getMesh()->update_normals();
	//m_omGeom->getNormalArray()->dirty();
	//m_omGeom->getVertexArray()->dirty();

	//for (int i = 0; i < m_omGeom->getPrimitiveSetList().size(); i++)
	//{
	//	m_omGeom->getPrimitiveSet(i)->dirty();
	//}

	m_utilProcessor->cleanUpIndicies();
	m_utilProcessor->calcNormals();

}