// ClayMore - Immersive Mesh Modelling --- Copyright (c) 2014-2017 Philipp Ladwig, Jannik Fiedler, Jan Beutgen
#include "ExportProcessor.h"

#include <OpenMesh/Core/IO/MeshIO.hh>


ExportProcessor::ExportProcessor()
{

}

ExportProcessor::~ExportProcessor()
{

}

void ExportProcessor::writeFileToDisk(std::string savePath)
{
	std::cout << "Start exporting mesh with OpenMesh" << std::endl;
	std::cout << savePath << std::endl;
	//OpenMesh::IO::Options opt;

	//opt += OpenMesh::IO::Options::Binary;
	if (!OpenMesh::IO::write_mesh(*m_omGeom->getMesh(), savePath))
	{
		std::cerr << "write error\n";
		exit(1);
	}
	std::cout << "Exproting finished" << std::endl;
}