// ClayMore - Immersive Mesh Modelling --- Copyright (c) 2014-2017 Philipp Ladwig, Jannik Fiedler, Jan Beutgen
#include "JoinGeometriesProcessor.h"

#include "UtilProcessor.h"

#include "iostream"

JoinGeometriesProcessor::JoinGeometriesProcessor(UtilProcessor* utilProcessor) : m_utilProcessor(utilProcessor)
{

}

JoinGeometriesProcessor::~JoinGeometriesProcessor()
{

}

void JoinGeometriesProcessor::joinGeometries(osg::ref_ptr<osg::OpenMeshGeometry> geometryToJoin)
{
	//TEMP
	bool selectNewVertices = true;







	osg::Vec3Array* vertexArrayMemberMesh = dynamic_cast<osg::Vec3Array*>(m_omGeom->getVertexArray());
	osg::Vec3Array* vertexArrayJoinMesh = dynamic_cast<osg::Vec3Array*>(geometryToJoin->getVertexArray());

	std::cout << std::endl << "Number of Vertices before: " << vertexArrayMemberMesh->size() << std::endl;
	int vertexArraySizeBeforeMerge = vertexArrayMemberMesh->size();

	//TODO: Transform Meshes to gobal space and join there
	osg::Matrix local2WorldMemberMesh = osg::computeLocalToWorld(m_omGeom->getParentalNodePaths()[0]);
	osg::Matrix local2WorldJoinMesh = osg::computeLocalToWorld(geometryToJoin->getParentalNodePaths()[0]);
	

	for (int i = 0; i < vertexArrayJoinMesh->size(); ++i)
	{
		//std::cout << "i: " << i << std::endl;
		Mesh::VertexHandle vh = m_omGeom->getMesh()->add_vertex(vertexArrayJoinMesh->at(i) * local2WorldJoinMesh * osg::Matrix::inverse(local2WorldMemberMesh));
		if(selectNewVertices)
			m_omGeom->getMesh()->status(vh).set_selected(true);
	}
	std::cout << std::endl << "Insert finished..." << std::endl;
	std::cout << std::endl << "Number of Vertices after: " << vertexArrayMemberMesh->size() << std::endl;

	osg::DrawElementsUInt* indicesToJoin = dynamic_cast<osg::DrawElementsUInt*>(geometryToJoin->getPrimitiveSet(0));

	for (unsigned int i = 0; i < indicesToJoin->getNumIndices(); ++i)
	{
		OpenMesh::VertexHandle v1((*indicesToJoin)[i] + vertexArraySizeBeforeMerge);
		//std::cout << "v1: " << (*indicesToJoin)[i] + vertexArraySizeBeforeMerge << std::endl;
		++i;
		OpenMesh::VertexHandle v2((*indicesToJoin)[i] + vertexArraySizeBeforeMerge );
		//std::cout << "v2: " << (*indicesToJoin)[i] + vertexArraySizeBeforeMerge << std::endl;
		++i;
		OpenMesh::VertexHandle v3((*indicesToJoin)[i] + vertexArraySizeBeforeMerge);
		//std::cout << "v3: " << (*indicesToJoin)[i] + vertexArraySizeBeforeMerge << std::endl;
		m_omGeom->getMesh()->add_face(v1, v2, v3);
	}

	//std::cout << std::endl << "Number of Vertices after: " << vertexArrayMemberMesh->size() << std::endl;


	std::vector<OpenMesh::VertexHandle> vhandles;
	unsigned int idx;
	for (int i = 0, nF = m_omGeom->getMesh()->n_faces(); i < nF; ++i)
	{
		vhandles.clear();
		for (Mesh::CFVIter fv_it = m_omGeom->getMesh()->cfv_iter(OpenMesh::FaceHandle(int(i))); fv_it.is_valid(); ++fv_it)
		{
			vhandles.push_back(*fv_it);
		}

		std::vector<int> indiciesCleanedUp;
		for (int j = 0; j < vhandles.size(); ++j)
		{
			idx = vhandles[j].idx(); //+ 1;
			indiciesCleanedUp.push_back(idx);
		}
		m_utilProcessor->sync_indices((int)i, indiciesCleanedUp[0], indiciesCleanedUp[1], indiciesCleanedUp[2]);
	}

	//TODO: Agree on which type of calcNormals we should use
	m_omGeom->getMesh()->update_normals();
	m_omGeom->getNormalArray()->dirty();
	m_omGeom->getVertexArray()->dirty();

	for (int i = 0; i < m_omGeom->getPrimitiveSetList().size(); i++)
	{
		m_omGeom->getPrimitiveSet(i)->dirty();
	}
}