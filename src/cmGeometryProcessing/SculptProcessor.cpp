// ClayMore - Immersive Mesh Modelling --- Copyright (c) 2014-2017 Philipp Ladwig, Jannik Fiedler, Jan Beutgen
#include "SculptProcessor.h"

#include "UtilProcessor.h"

SculptProcessor::SculptProcessor(UtilProcessor* utilProcessor) : m_utilProcessor(utilProcessor)
{

}

SculptProcessor::~SculptProcessor()
{

}

void SculptProcessor::sculptStart(float radiusIn, float strengthIn)
{
	m_sculptingDistanceProgessUntilMax.resize(m_omGeom->getMesh()->n_vertices());
	for (unsigned int i = 0; i < m_omGeom->getMesh()->n_vertices(); i++)
	{
		m_sculptingDistanceProgessUntilMax.at(i) = 0.0;
	}
}

void SculptProcessor::sculptAddOrSubtract(float radiusIn,
	float strengthIn,
	osg::Vec3 cursorTrans,
	osg::Matrix geomLocal2World,
	bool isAddBrush,
	osg::Vec3Array* initialVertexArray)
{
	Mesh::VertexIter vIt, vBegin, vEnd;
	vBegin = m_omGeom->getMesh()->vertices_begin();
	vEnd = m_omGeom->getMesh()->vertices_end();

	std::vector<osg::Vec3f> averageNormal;

	for (vIt = vBegin; vIt != vEnd; ++vIt)
	{
		float distance = m_utilProcessor->calculateDistance(m_omGeom->getMesh()->point(*vIt) * geomLocal2World, cursorTrans);
		if (distance <= radiusIn)
		{
			//First select all vertcies within "action radius"
			m_omGeom->getMesh()->status(*vIt).set_selected(true);

			//Allocate all normals from selected vertices
			averageNormal.push_back(m_omGeom->getMesh()->normal(*vIt));
		}
		else
		{
			m_omGeom->getMesh()->status(*vIt).set_selected(false);
		}
	}

	float avgNormalx = 0.f;
	float avgNormaly = 0.f;
	float avgNormalz = 0.f;
	for (unsigned int j = 0; j < averageNormal.size(); j++)
	{
		avgNormalx = avgNormalx + averageNormal.at(j).x();
		avgNormaly = avgNormaly + averageNormal.at(j).y();
		avgNormalz = avgNormalz + averageNormal.at(j).z();
	}

	osg::Vec3f avgNormal;
	avgNormal.set(osg::Vec3f(avgNormalx / averageNormal.size(),
		avgNormaly / averageNormal.size(),
		avgNormalz / averageNormal.size()));

	avgNormal.normalize();

	unsigned int i = 0;
	for (vIt = vBegin; vIt != vEnd; ++vIt, i++)
	{
		if (m_omGeom->getMesh()->status(*vIt).selected())
		{
			float distance = m_utilProcessor->calculateDistance(m_omGeom->getMesh()->point(*vIt) * geomLocal2World, cursorTrans);
			//(distance/radius)^1 means: if vertex is near the border of the action sphere-> close to 1
			//if vertex close to center of action sphere-> close to 0
			float impact = 1 - pow(distance / radiusIn, strengthIn);

			osg::Vec3f sculptProgressVector = m_omGeom->getMesh()->point(*vIt) - initialVertexArray->at(i);
			float magnitudeSculptProgress = sculptProgressVector.length() * 1.5 + 0.001;
			float attenuation = 1 - magnitudeSculptProgress / impact;
			if (attenuation > 1.0)
			{
				attenuation = 1.0;
			}
			else if (attenuation < 0.0)
			{
				attenuation = 0.0;
			}

			//std::cout << attenuation << std::endl;
			//std::cout << impact << std::endl;
			//std::cout << "s" << std::endl;
			//Invert the distanceFactor and use it as factor for movement amount along the vertex normal
			if(isAddBrush)
				m_omGeom->getMesh()->set_point(*vIt, m_omGeom->getMesh()->point(*vIt) + avgNormal * 0.1f * (impact * attenuation));
			else
				m_omGeom->getMesh()->set_point(*vIt, m_omGeom->getMesh()->point(*vIt) - avgNormal * 0.1f * (impact * attenuation));

			//std::cout << m_magnitudeSculptProgress << std::endl;
		}
	}
	m_omGeom->getVertexArray()->dirty();
	m_omGeom->getNormalArray()->dirty();
	m_omGeom->computeBound();
}
