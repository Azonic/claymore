// ClayMore - Immersive Mesh Modelling --- Copyright (c) 2014-2017 Philipp Ladwig, Jannik Fiedler, Jan Beutgen
#ifndef cmGeometryProcessing_EXPORTS
	#define imGeometryProcessing_DLL_import_export __declspec(dllimport)
#else
	#define imGeometryProcessing_DLL_import_export __declspec(dllexport)
#endif