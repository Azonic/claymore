// ClayMore - Immersive Mesh Modelling --- Copyright (c) 2014-2017 Philipp Ladwig, Jannik Fiedler, Jan Beutgen
#pragma once

#include "ConfigDllExportGeometryProcessing.h"
#include "Processor.h"

class imGeometryProcessing_DLL_import_export UtilProcessor : public Processor
{

public:
	// #### CONSTRUCTOR & DESTRUCTOR ###############
	UtilProcessor();
	~UtilProcessor();


	// #### MEMBER VARIABLES ###############
private:
	typedef OpenMesh::OSG_TriMesh_BindableArrayKernelT Mesh;

	// #### MEMBER FUNCTIONS ###############
public:
	inline virtual void setActiveGeometry(osg::ref_ptr<osg::OpenMeshGeometry> omGeom) { m_omGeom = omGeom; }

	osg::Vec3d getBarycentrumOfALLVerts();
	osg::Vec3d getBarycentrumOfSELECTEDVerts();
	float calculateDistance(OpenMesh::OSG_TriMesh_BindableArrayKernelT::VertexIter vIt, OpenMesh::OSG_TriMesh_BindableArrayKernelT::VertexHandle vh);
	float calculateDistance(OpenMesh::OSG_TriMesh_BindableArrayKernelT::VertexIter vIt, osg::Vec3f positionIn);
	float calculateDistance(osg::Vec3f position0, osg::Vec3f position1);
	void cleanUpIndicies();
	void sync_indices(size_t face, size_t v1, size_t v2, size_t v3);
	void calcNormals();
	void addFaceBasedOnSelectedVerts();

	//Experimental function for adding a face in whatever vertex direction -> it's not guranteed that it will work
	void bruteForceAddFace(Mesh::VertexHandle v0, Mesh::VertexHandle v1, Mesh::VertexHandle v2, short iterationIn = 0);
};


