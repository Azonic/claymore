// ClayMore - Immersive Mesh Modelling --- Copyright (c) 2014-2017 Philipp Ladwig, Jannik Fiedler, Jan Beutgen
#pragma once

#include "ConfigDllExportGeometryProcessing.h"
#include "Processor.h"

class UtilProcessor;

class imGeometryProcessing_DLL_import_export MeshHistoryProcessor : public Processor
{

public:
	// #### CONSTRUCTOR & DESTRUCTOR ###############
	MeshHistoryProcessor(UtilProcessor* utilProcessor);
	~MeshHistoryProcessor();


	// #### MEMBER VARIABLES ###############
private:
	typedef OpenMesh::OSG_TriMesh_BindableArrayKernelT Mesh;
	UtilProcessor* m_utilProcessor;
	
	//Basic Tests
	osg::Vec3Array* m_initialTopologyAtStartOfTransform;
	osg::DrawElementsUInt* m_initialIndiciesAtStartOfTransform;
	osg::Vec4Array* m_initialColorAtStartOfTransform;

	std::vector<osg::Vec3Array*> m_historyOfVerticies;
	std::vector<osg::Vec4Array*> m_historyOfVertColors;
	std::vector<osg::DrawElementsUInt*> m_historyOfIndicies;


	// #### MEMBER FUNCTIONS ###############
public:
	inline virtual void setActiveGeometry(osg::ref_ptr<osg::OpenMeshGeometry> omGeom) { m_omGeom = omGeom; }
	void copyAndStoreVertexArray();
	void copyAndStoreIndiciesArray();
	void copyAndStoreColorArray();
	inline osg::Vec3Array* getCurrentVertexArray();
	void cancelAnyTransformation();
	//void restoreLastTopology();
	void storeMeshDataAsHistory();
	void undo();

	short maxSteps;
	short undoStep;
};


