// ClayMore - Immersive Mesh Modelling --- Copyright (c) 2014-2017 Philipp Ladwig, Jannik Fiedler, Jan Beutgen
#pragma once

enum CONTROLLER_TYPE
{
	PRIMARY,
	SECONDARY,
	GENERIC_TRACKER
};