// ClayMore - Immersive Mesh Modelling --- Copyright (c) 2014-2017 Philipp Ladwig, Jannik Fiedler, Jan Beutgen
#pragma once

#include "ConfigDllExportUtil.h"
#include "tinyxml2.h"
#include <string>
#include <map>
#include <osg\MatrixTransform>
#include "../cmUtil/Textcreator.h"

enum UserTestStatus
{
	NOT_YET_STARTED,
	STARTED,
	TESTPHASE,
	COUNTDOWN,
	GET_READY_BUTTON4,
	BUTTON4,
	GET_READY_BUTTON6,
	BUTTON6,
	GET_READY_BUTTON8,
	BUTTON8,
	GET_READY_BUTTON10,
	BUTTON10,
	GET_READY_BUTTON12,
	BUTTON12,
	WAIT_ON_CALLBACK,
	FINISHED,
};

class imUtil_DLL_import_export UserTestLogger
{
	// #### CONSTRUCTOR & DESTRUCTOR ###############
public:

	UserTestLogger();
	~UserTestLogger();

	bool readUserTest1ControlXML(const char * filePath);
	void startUserTest(const char * filePath);
	void calledCallback(std::string calledCallbackNameIn);
	void update();
	bool writeUserTestLogXML();

	void userTrigger();

	void setCallback(std::string speechCommand, void(*callback)());

	inline void attachToSceneGraph(osg::ref_ptr<osg::Group> rootNodeIn) { rootNodeIn->addChild(m_matrixTransform); }

private:
	osg::ref_ptr<osg::MatrixTransform> m_matrixTransform;
	std::string m_currentCommand;
	UserTestStatus m_status;
	osg::ref_ptr<osgText::Text> m_textShownInSG;
	osg::Timer_t m_tick = osg::Timer::instance()->tick();

	//std::map<unsigned short, std::map<unsigned short, std::map<unsigned short, std::string>>> m_xmlAsEnumerableMap;
	std::vector<std::vector<std::vector<unsigned short>>> m_xmlAsEnumerableMap;

	std::map<std::string, void(*)()> m_callbacks;

	std::vector<float> m_loggedTimes;
	//std::vector<bool> m_loggedFails;

	//New Callback must be process if it is true
	bool m_processingCallback;
	//User have pressed Menu Button for "I am ready"
	bool m_userTrigger;
	//Just a helper bool for triggering things just once
	bool m_triggerOnce;
	//TextSize for normal info text
	float m_smallChracterSize;
	//TextSize for numbers in XML in SceneGraph
	float m_bigCharacterSize;

	unsigned short m_numberOfIterationInLevel2, m_numberOfIterationInLevel1, m_numberOfIterationInLebel0;

	unsigned short m_xmlMenuType, m_xmlButtonCount, m_xmlItemLayer;

	bool m_onlyWriteOnce;
};