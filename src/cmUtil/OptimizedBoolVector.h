#pragma once

#include "ConfigDllExportUtil.h"

//  Optimized Vector of bools storing 8 values within a single byte similar to the std::Vector<bool>.
//  However this class is more performant and the data is directly written to and read from the memory via the begin pointer.
//  Thus, multiple threads can simultaneously accesss its data.
//  As long as two different bytes are accessed there should be no data race at all.
//  If, however, two threads write to bits withihn the same byte, the behaviour is not defined.
//  When iterating through the vector with multiple threads, one should always consider the bytes for splitting the data, rather than the acutal elements of the vector.
class imUtil_DLL_import_export OptimizedBoolVector {
	// #### CONSTRUCTOR & DESTRUCTOR ###############
public:
	OptimizedBoolVector(const size_t size);
	OptimizedBoolVector(char* begin, const size_t size);
	~OptimizedBoolVector();
	// #### MEMBER VARIABLES ###############
private:
	char* begin;
	size_t bitSize;
	size_t byteSize;
	// #### MEMBER FUNCTIONS ###############
	inline void getIndices(const size_t index, size_t& byteOut, unsigned char& bitOut) const;
public:
	//void resize(const size_t size);
	void fill(const bool value);
	void set(const size_t index, const bool value);
	void set(const size_t byte, const unsigned char bit, const bool value);
	void set(char* byte, const unsigned char bit, const bool value);
	bool at(const size_t index) const;
	bool at(const size_t byte, const unsigned char bit) const;
	size_t bytes() const;
	size_t bits() const;
	char* getBegin();
};