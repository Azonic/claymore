#pragma once

#include "ConfigDllExportUtil.h"

// Stores a time stamp and a vr::HmdMatrix34_t for sync with depth sensor
#include <iostream>

struct Matrix34
{
	float m[3][4];
};

struct TimedOpenVRMatrix{
	unsigned long long timestamp;
	Matrix34 matrix;
	
	TimedOpenVRMatrix(unsigned long long t, Matrix34 m) : timestamp(t), matrix(m) { 
		/*std::cout << "timestamp: " << timestamp << std::endl;
		std::cout << "m[1][2]: " << matrix.m[1][2] << std::endl;
		std::cout << "m[2][3]: " << matrix.m[2][3] << std::endl;*/
	}
};