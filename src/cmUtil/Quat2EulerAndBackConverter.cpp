// ClayMore - Immersive Mesh Modelling --- Copyright (c) 2014-2017 Philipp Ladwig, Jannik Fiedler, Jan Beutgen
#include "Quat2EulerAndBackConverter.h"
#include <osgUtil/Optimizer>
#include <iostream>
#include <osg/MatrixTransform>

void Quat2EulerAndBackConverter::getEulerFromQuat(osg::Quat q, double& yaw, double& roll, double& pitch)

{
	double limit = 0.499999;

	double sqx = q.x()*q.x();
	double sqy = q.y()*q.y();
	double sqz = q.z()*q.z();

	double t = q.x()*q.y() + q.z()*q.w();

	if (t > limit) // gimbal lock ?
	{
		yaw = 2 * atan2(q.x(), q.w());
		roll = osg::PI_2;
		pitch = 0;
	}
	else if (t < -limit)
	{

		yaw = -2 * atan2(q.x(), q.w());
		roll = -osg::PI_2;
		pitch = 0;

	}
	else
	{
		yaw = atan2(2 * q.y()*q.w() - 2 * q.x()*q.z(), 1 - 2 * sqy - 2 * sqz);
		roll = asin(2 * t);
		pitch = atan2(2 * q.x()*q.w() - 2 * q.y()*q.z(), 1 - 2 * sqx - 2 * sqz);

	}

}



void Quat2EulerAndBackConverter::getQuatFromEuler(double heading, double attitude, double bank, osg::Quat& q)

{

	double c1 = cos(heading / 2);

	double s1 = sin(heading / 2);

	double c2 = cos(attitude / 2);

	double s2 = sin(attitude / 2);

	double c3 = cos(bank / 2);

	double s3 = sin(bank / 2);

	double c1c2 = c1*c2;

	double s1s2 = s1*s2;





	double w = c1c2*c3 - s1s2*s3;

	double x = c1c2*s3 + s1s2*c3;

	double y = s1*c2*c3 + c1*s2*s3;

	double z = c1*s2*c3 - s1*c2*s3;



	q[0] = x; q[1] = y;

	q[2] = z; q[3] = w;

}

