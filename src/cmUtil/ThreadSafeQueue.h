#pragma once

#include <queue>
#include <atomic>

template<class T>
class ThreadSafeQueue {
private:
	std::atomic<bool> m_lock;
	std::queue<T> m_queue;

	void lock();
	void unlock();
public:
	ThreadSafeQueue();
	~ThreadSafeQueue();
	
	void push(T element);
	bool pop(T& result);
	void pushBatch(T* elements, unsigned int count);
};