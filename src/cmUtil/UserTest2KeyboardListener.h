// ClayMore - Immersive Mesh Modelling --- Copyright (c) 2014-2017 Philipp Ladwig, Jannik Fiedler, Jan Beutgen

//FIXME: Do we really need this class/functionality? We should get more performance, when dont look after this Event Sender
#pragma once

#include <osgGA/GUIEventHandler>

#include "ConfigDllExportUtil.h"

#include <string>

class ClayMoreVideoPlayer;
class MiscDataStorage;

class imUtil_DLL_import_export UserTest2KeyboardListener : public osgGA::GUIEventHandler
{
	// #### CONSTRUCTOR & DESTRUCTOR ###############


	// #### MEMBER VARIABLES ###############
private:


	// #### MEMBER FUNCTIONS ###############
public:
	UserTest2KeyboardListener(ClayMoreVideoPlayer* videoPlayerIn, MiscDataStorage* miscDataStorageIn, bool *g_writeAutoSaveFileUserTest2, bool *g_readAutoSaveFileUserTest2);
	virtual bool handle(const osgGA::GUIEventAdapter& ea, osgGA::GUIActionAdapter&);

	ClayMoreVideoPlayer* m_videoPlayer;
	MiscDataStorage* m_miscDataStorage;
	bool *m_writeAutoSave;
	bool *m_readAutoSave;

	unsigned short m_clipNumber;

	std::vector<std::string> m_filePathes;
};