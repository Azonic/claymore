#include "Ray.h"

Ray::Ray(osg::Vec3f origin, osg::Vec3f direction, float distance) : m_origin(origin), m_direction(direction), m_distance(distance) {
	m_direction.normalize();
}

osg::Vec3f Ray::getOrigin() const {
	return m_origin;
}

osg::Vec3f Ray::getDirection() const {
	return m_direction;
}

float Ray::getDistance() const {
	return m_distance;
}