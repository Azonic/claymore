#pragma once

#include "ConfigDllExportUtil.h"

#include "osg/Vec3f"

struct imUtil_DLL_import_export Ray {
private:
	osg::Vec3f m_origin;
	osg::Vec3f m_direction;
	float m_distance;
public:
	Ray(osg::Vec3f origin, osg::Vec3f direction, float distance);
	inline osg::Vec3f getOrigin() const;
	inline osg::Vec3f getDirection() const;
	inline float getDistance() const;
};