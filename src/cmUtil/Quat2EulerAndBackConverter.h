// ClayMore - Immersive Mesh Modelling --- Copyright (c) 2014-2017 Philipp Ladwig, Jannik Fiedler, Jan Beutgen
#pragma once

#include <string>
#include <osg\Timer>

#include <iostream>
#include <fstream>

#include <osg/matrix>
#include <osg/quat>

#include "ConfigDllExportUtil.h"

class imUtil_DLL_import_export Quat2EulerAndBackConverter
{
	// #### CONSTRUCTOR & DESTRUCTOR ###############
public:

	inline Quat2EulerAndBackConverter() {};
	inline ~Quat2EulerAndBackConverter() {};


	// #### MEMBER VARIABLES ###############
private:



	// #### MEMBER FUNCTIONS ###############
public:
	static void getEulerFromQuat(osg::Quat q, double& yaw, double& roll, double& pitch);
	static void getQuatFromEuler(double yaw, double roll, double pitch, osg::Quat& q);



};