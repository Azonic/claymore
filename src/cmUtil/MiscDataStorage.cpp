// ClayMore - Immersive Mesh Modelling --- Copyright (c) 2014-2017 Philipp Ladwig, Jannik Fiedler, Jan Beutgen
#include "MiscDataStorage.h"

MiscDataStorage::MiscDataStorage()
{
	m_lastPriControllerPosition = osg::Vec3f(0.0f, 0.0f, 0.0f);
	m_lastSecControllerPosition = osg::Vec3f(0.0f, 0.0f, 0.0f);
	m_lastSphereSelectionToolSize = 0.0f;
	m_rumbleShortState = new bool(false);
	//*m_rumbleShortState = false;

	m_isOBJTranslateToolActive = false;
	m_isOBJRotateToolActive = false;
	m_isOBJScaleToolActive = false;

	m_isEDITranslateToolActive = false;
	m_isEDIRotateToolActive = false;
	m_isEDIScaleToolActive = false;

	m_IsEDIAddVertsToolActive = false;
	m_IsEDIConnectMeshElementToolActive = false;

	m_isEDISelectionConfirm = false;
	m_isEDISelectionNegate = false;

	m_isHelpTextActivated = false;

	m_IsTriggerLeftIndicated = false;
	m_IsSqueezeLeftIndicated = false;
	m_IsTriggerRightIndicated = false;
	m_IsSqueezeRightIndicated = false;

	m_scanContextAddress = ScanContext::POINT_CLOUD_MODE;
}

MiscDataStorage::~MiscDataStorage()
{

}

void MiscDataStorage::activateEDITransTool()
{
	m_isEDITranslateToolActive = true;
	m_isEDIRotateToolActive = false;
	m_isEDIScaleToolActive = false;
}

void MiscDataStorage::activateEDIRotateTool()
{
	m_isEDITranslateToolActive = false;
	m_isEDIRotateToolActive = true;
	m_isEDIScaleToolActive = false;
}

void MiscDataStorage::activateEDIScaleTool()
{
	m_isEDITranslateToolActive = false;
	m_isEDIRotateToolActive = false;
	m_isEDIScaleToolActive = true;
}


void MiscDataStorage::activateEDISelectionConfirmTool()
{
	m_isEDISelectionConfirm = true;
	m_isEDISelectionNegate = false;
}

void MiscDataStorage::activateEDISelectionNegateTool()
{
	m_isEDISelectionConfirm = false;
	m_isEDISelectionNegate = true;
}

void MiscDataStorage::activateConnectMeshElementTool()
{
	m_IsEDIConnectMeshElementToolActive = true;
	m_IsEDIAddVertsToolActive = false;
}

void MiscDataStorage::activateAddVertsTool()
{
	m_IsEDIConnectMeshElementToolActive = false;
	m_IsEDIAddVertsToolActive = true;
}
