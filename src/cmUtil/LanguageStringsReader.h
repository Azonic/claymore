// ClayMore - Immersive Mesh Modelling --- Copyright (c) 2014-2017 Philipp Ladwig, Jannik Fiedler, Jan Beutgen
#pragma once
#pragma warning (disable : 4251)	//--> It's for surpressing the wanring about the need of a dll-interface for the STL map class...
									//... but can be ignored, because theoretically no "client" of configReader will ...
									//... ever use the member m_startupConfig. This means, there is no need for exporting Map to the dll.
									//Before january 2017, it was a static public STL Map, which is now a private member and can't directly accessed
									//by "clients"/"users" of ConfigReader. The warning could be some kind of minor bug in the compiler


#include "ConfigDllExportUtil.h"

#include <map>
#include <string>
#include <iostream>

#include "tinyxml2.h"

class imUtil_DLL_import_export LanguageStringsReader
{
	// #### CONSTRUCTOR & DESTRUCTOR ###############
public:
	LanguageStringsReader();
	~LanguageStringsReader();

private:
	std::map<std::string, std::string> m_strings;

public:
	//TODO: Remove Warning for not dll exported stl class ->look here: http://stackoverflow.com/questions/4145605/stdvector-needs-to-have-dll-interface-to-be-used-by-clients-of-class-xt-war
	const char* getString(std::string mapKeyIn) {
		return m_strings[mapKeyIn].c_str();
	}

	bool readLanguageStringsFile(const char * filePath, std::string languageIDIn);

	static void printDebugMessage(const char * message);
};