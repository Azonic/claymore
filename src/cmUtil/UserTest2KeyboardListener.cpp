// ClayMore - Immersive Mesh Modelling --- Copyright (c) 2014-2017 Philipp Ladwig, Jannik Fiedler, Jan Beutgen
#include "UserTest2KeyboardListener.h"

#include "../cmUtil/ClayMoreVideoPlayer.h"
#include "../cmUtil/MiscDataStorage.h"

#include <iostream>

UserTest2KeyboardListener::UserTest2KeyboardListener(ClayMoreVideoPlayer* videoPlayerIn, MiscDataStorage* miscDataStorageIn,
	bool *g_writeAutoSaveFileUserTest2, bool *g_readAutoSaveFileUserTest2)
	: m_videoPlayer(videoPlayerIn),
	m_miscDataStorage(miscDataStorageIn),
	m_writeAutoSave(g_writeAutoSaveFileUserTest2),
	m_readAutoSave(g_readAutoSaveFileUserTest2)
{
	//Networks streams work with HTTP + Transcoding on with VLC 2.2.1
	//filePathes.push_back("http://192.168.178.34:8080");

	m_filePathes.push_back("data/UserTests/instructionVideosUT2/1.mp4");
	m_filePathes.push_back("data/UserTests/instructionVideosUT2/2.mp4");
	m_filePathes.push_back("data/UserTests/instructionVideosUT2/3.mp4");
	m_filePathes.push_back("data/UserTests/instructionVideosUT2/4.mp4");
	m_filePathes.push_back("data/UserTests/instructionVideosUT2/5.mp4");
	m_filePathes.push_back("data/UserTests/instructionVideosUT2/6.mp4");
	m_filePathes.push_back("data/UserTests/instructionVideosUT2/7.mp4");
	m_filePathes.push_back("data/UserTests/instructionVideosUT2/8.mp4");
	m_filePathes.push_back("data/UserTests/instructionVideosUT2/9.mp4");
	m_filePathes.push_back("data/UserTests/instructionVideosUT2/10.mp4");
	m_filePathes.push_back("data/UserTests/instructionVideosUT2/11.mp4");
	m_filePathes.push_back("data/UserTests/instructionVideosUT2/12.mp4");
	m_filePathes.push_back("data/UserTests/instructionVideosUT2/13.mp4");
	m_filePathes.push_back("data/UserTests/instructionVideosUT2/14.mp4");
	m_filePathes.push_back("data/UserTests/instructionVideosUT2/15.mp4");
	m_filePathes.push_back("data/UserTests/instructionVideosUT2/16.mp4");
	m_filePathes.push_back("data/UserTests/instructionVideosUT2/17.mp4");
	m_filePathes.push_back("data/UserTests/instructionVideosUT2/18.mp4");
	m_filePathes.push_back("data/UserTests/instructionVideosUT2/19.mp4");
	m_filePathes.push_back("data/UserTests/instructionVideosUT2/20.mp4");

	m_clipNumber = 0;
}

bool UserTest2KeyboardListener::handle(const osgGA::GUIEventAdapter& ea, osgGA::GUIActionAdapter& aa)
{
	switch (ea.getEventType())
	{
	case(osgGA::GUIEventAdapter::KEYDOWN):

		switch (ea.getKey())
		{
		case osgGA::GUIEventAdapter::KEY_A:

			std::cout << "KEY_A - Back" << std::endl;

			--m_clipNumber;
			if (m_clipNumber >= 0 && m_clipNumber < m_filePathes.size())
			{
				m_videoPlayer->open(m_filePathes.at(m_clipNumber));
				std::cout << "Video: " << m_clipNumber + 1 << ".mp4" << std::endl;
			}
			std::cout << "--------" << m_clipNumber << std::endl;

			break;

		case osgGA::GUIEventAdapter::KEY_D:

			std::cout << "KEY_D - Foward" << std::endl;

			++m_clipNumber;
			if (m_clipNumber >= 0 && m_clipNumber < m_filePathes.size())
			{
				m_videoPlayer->open(m_filePathes.at(m_clipNumber));
				std::cout << "Video: " << m_clipNumber + 1 << ".mp4" << std::endl;
			}
			std::cout << "--------" << m_clipNumber << std::endl;

			break;

		case osgGA::GUIEventAdapter::KEY_P:

			std::cout << "Play" << std::endl;
			//if (!(m_videoPlayer->getPlayedTime() > 0))
			//{
				m_videoPlayer->play();
			//}


			break;

		case osgGA::GUIEventAdapter::KEY_B:

			std::cout << "Break" << std::endl;
			m_videoPlayer->pause();

			break;

		case osgGA::GUIEventAdapter::KEY_H:

			if (m_miscDataStorage->getIsHelpTextActivated())
			{
				m_miscDataStorage->setIsHelpTextActivated(false);
				std::cout << "Help Off" << std::endl;
			}
			else
			{
				m_miscDataStorage->setIsHelpTextActivated(true);
				std::cout << "Help On" << std::endl;
			}

			break;
		case osgGA::GUIEventAdapter::KEY_S:

			*m_writeAutoSave = true;
			

			break;

		case osgGA::GUIEventAdapter::KEY_L:

			*m_readAutoSave = true;

			break;

		case osgGA::GUIEventAdapter::KEY_F5:
			if (m_miscDataStorage->getIsTriggerLeftIndicated())
			{
				m_miscDataStorage->setIsTriggerLeftIndicated(false);
				std::cout << "Trigger Left Indicated Off" << std::endl;
			}
			else
			{
				m_miscDataStorage->setIsTriggerLeftIndicated(true);
				std::cout << "Trigger Left Indicated On" << std::endl;
			}
			break;

		case osgGA::GUIEventAdapter::KEY_F6:
			if (m_miscDataStorage->getIsSqueezeLeftIndicated())
			{
				m_miscDataStorage->setIsSqueezeLeftIndicated(false);
				std::cout << "Squeeze Left Indicated Off" << std::endl;
			}
			else
			{
				m_miscDataStorage->setIsSqueezeLeftIndicated(true);
				std::cout << "Squeeze Left Indicated On" << std::endl;
			}
			break;

		case osgGA::GUIEventAdapter::KEY_F7:
			if (m_miscDataStorage->getIsTriggerRightIndicated())
			{
				m_miscDataStorage->setIsTriggerRightIndicated(false);
				std::cout << "Trigger Right Indicated Off" << std::endl;
			}
			else
			{
				m_miscDataStorage->setIsTriggerRightIndicated(true);
				std::cout << "Trigger Right Indicated On" << std::endl;
			}
			break;

		case osgGA::GUIEventAdapter::KEY_F8:
			if (m_miscDataStorage->getIsSqueezeRightIndicated())
			{
				m_miscDataStorage->setIsSqueezeRightIndicated(false);
				std::cout << "Squeeze Right Indicated Off" << std::endl;
			}
			else
			{
				m_miscDataStorage->setIsSqueezeRightIndicated(true);
				std::cout << "Squeeze Right Indicated On" << std::endl;
			}
			break;

			
		default:

			break;

		}

		break;

	default:

		break;
	}
	return false;
}