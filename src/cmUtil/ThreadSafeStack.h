#pragma once

#include <stack>
#include <atomic>

template<class T>
class ThreadSafeStack {
private:
	std::atomic<bool> m_lock;
	std::stack<T> m_stack;

	void lock();
	void unlock();
public:
	ThreadSafeStack();
	~ThreadSafeStack();
	
	void push(T element);
	bool pop(T& result);
	void pushBatch(T* elements, unsigned int count);
};