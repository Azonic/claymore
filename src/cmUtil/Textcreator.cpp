// ClayMore - Immersive Mesh Modelling --- Copyright (c) 2014-2017 Philipp Ladwig, Jannik Fiedler, Jan Beutgen
#include "TextCreator.h"

osg::ref_ptr<osgText::Font> TextCreator::FONT = nullptr;

TextCreator::TextCreator()
{

}

TextCreator::~TextCreator()
{
}

osgText::Text* TextCreator::createText(std::string const & textIn)
{
	osg::ref_ptr<osgText::Text> text = new osgText::Text();

	if (TextCreator::FONT == nullptr)
		TextCreator::FONT = osgText::readFontFile("fonts/arial.ttf");
		//TextCreator::FONT = osgText::readFontFile("data/fonts/fontawesome-webfont.ttf");

	text->setFont(FONT);
	text->setCharacterSize(0.05);
	text->setAxisAlignment(osgText::TextBase::YZ_PLANE);
	text->setAlignment(osgText::TextBase::CENTER_CENTER);
	text->setText(textIn);
	text->getOrCreateStateSet()->setMode(GL_LIGHTING, osg::StateAttribute::OVERRIDE | osg::StateAttribute::OFF);

	return text.release();
}

osgText::Text* TextCreator::createText(const osg::Vec3& posIn, const std::string& contentIn, float sizeIn, osgText::TextBase::AlignmentType alignIn) {
	osg::ref_ptr<osgText::Font> font = osgText::readFontFile("fonts/arial.ttf");
	osg::ref_ptr<osgText::Text> text = new osgText::Text;
	text->setFont(font.get());
	text->setCharacterSize(sizeIn);
	text->setAxisAlignment(osgText::TextBase::XZ_PLANE);
	text->setAlignment(alignIn);
	text->setPosition(posIn);
	text->setText(contentIn);
	return text.release();
}