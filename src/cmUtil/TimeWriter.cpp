// ClayMore - Immersive Mesh Modelling --- Copyright (c) 2014-2017 Philipp Ladwig, Jannik Fiedler, Jan Beutgen
#include "TimeWriter.h"
#include "ctime"


TimeWriter::TimeWriter()
{
}

TimeWriter::~TimeWriter()
{
}

void TimeWriter::startMetering(std::string nameIn, bool enablePrintToConsoleIn, bool enableWriteToFileIn)
{
	m_meteringName = nameIn;
	m_enablePrintToConsole = enablePrintToConsoleIn;
	//m_enablePrintToConsole = false;
	m_enableWriteToFile = enableWriteToFileIn;
	time_t t = time(0);   // get time now

	//struct tm * now = localtime(&t);
	struct tm now;
	localtime_s(&now, &t);

	if (m_enablePrintToConsole)
	{
		std::cout << "##### metering of " << m_meteringName << " starts...." << std::endl;
	}

	if (m_enableWriteToFile)
	{
		m_meteringOutputFile.open("data/meteringOutput.txt", std::ios::app);
		//m_meteringOutputFile << " ########### metering name: " << m_meteringName << " //// date: " << now.tm_mday << "." << (now.tm_mon + 1) << "." << (now.tm_year + 1900) << " //// time: " << now.tm_hour << ":" << now.tm_min << ":" << now.tm_sec << " ###########" << std::endl;
	}
	m_startTick = osg::Timer::instance()->tick();
}

double TimeWriter::endMetering()
{
	m_endTick = osg::Timer::instance()->tick();
	double timeDiff = osg::Timer::instance()->delta_m(m_startTick, m_endTick);

	if (m_enablePrintToConsole)
	{
		std::cout << "Measurement finished. Measured time[ms] for " << m_meteringName << " is: " << timeDiff << std::endl;
	}

	if (m_enableWriteToFile)
	{
		m_meteringOutputFile << "Measured time [ms] for " << m_meteringName << " is: " << timeDiff << std::endl;
		m_meteringOutputFile.close();
	}
	return timeDiff;
}