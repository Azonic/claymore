// ClayMore - Immersive Mesh Modelling --- Copyright (c) 2014-2017 Philipp Ladwig, Jannik Fiedler, Jan Beutgen
#pragma once

//Doxygen Example for a class
/**
 * \class ConfigReader
 *
 * \brief This class reads the ClayMore XML configuration file located in /data
 *
 * \note TODO: Actually it would be useful, if there is a generic parent of 
 * this class (reads every xml and list all its tags) and a specific ClayMore class
 *
 * \author Philipp Ladwig
 *
 * \version $Revision: 1.0 $
 *
 * \date $Date: 2018/12/16 10:20 $
 *
 * Contact: philipp.ladwig@hs-duesseldorf.de	
 *
 * Created on: Before 2017
 *
 */

#include "ConfigDllExportUtil.h"

#include <osg/ref_ptr>
#include <osgText/Text>

class imUtil_DLL_import_export DebugAndStatisticsPlugin
{
	// #### CONSTRUCTOR & DESTRUCTOR ###############
public:
	DebugAndStatisticsPlugin();
	~DebugAndStatisticsPlugin();

private:
	//osg::ref_ptr<osg::MatrixTransform> m_frameCounterMatrixTransform;
	osg::ref_ptr<osgText::Text> m_FrameCounterText;
	

public:
	osg::ref_ptr<osg::MatrixTransform> createFrameCounter(float charakterSizeIn, std::string fontPathsIn);
	void DebugAndStatisticsPlugin::setFrameCounterText(std::string textIn);
};