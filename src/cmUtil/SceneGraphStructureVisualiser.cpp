//How to use this Util (Example from ClayMores main.cpp):
//#include "imUtil\SceneGraphStructureVisualiser.h"
//.
//.
//.
//SceneGraphStructureVisualiser* sGvis = new SceneGraphStructureVisualiser();
//sGvis->visualise(g_sceneRoot);
//################################################################################

#include "SceneGraphStructureVisualiser.h"

#include <iostream>

#include "tinyxml2.h"

SceneGraphStructureVisualiser::SceneGraphStructureVisualiser()
{
	m_currentNodeID = 0;
	m_xmlFileName = "__SavedData.xml"; //TODO: Currently hardcoded, better read from config.xml
}

SceneGraphStructureVisualiser::~SceneGraphStructureVisualiser()
{

}

//If return value = true = everything is fine
bool SceneGraphStructureVisualiser::visualise(osg::ref_ptr<osg::Group> rootNodeIn)
{
	// check to see if we have a valid (non-NULL) node.
	if (!rootNodeIn)
	{
		return false;
	}

	std::cout << "Writing scenegraph structure into " << m_xmlFileName << " next to ClayMore.exe" << std::endl;
	addIDToRootNode(rootNodeIn);
	addIDsToRemainingNodes(rootNodeIn);
	writeTopologyMap(rootNodeIn);
	removeIDs(rootNodeIn);
	saveTopologyAsXML();

	//DEBUG
	//std::cout << "m_topology: " << m_topology.size() << std::endl;
	//for (unsigned int i = 1; i <= m_topology.size(); i++)
	//{
	//	std::cout << "NodeID: " << i << " Parent: " << m_topology.at(i) << std::endl;
	//}

	//for (unsigned int i = 0; i < m_nodeNames.size(); i++)
	//{
	//	std::cout << "NodeName: " << m_nodeNames.at(i) << std::endl;
	//}
	return true;
}

//Adds a unique ID to the root node in the scene graph. This is made because every node needs a unique identifier for creating the m_topology map
void SceneGraphStructureVisualiser::addIDToRootNode(osg::ref_ptr<osg::Node> currNodeIn)
{
	// check to see if we have a valid (non-NULL) node.
	if (!currNodeIn)
	{
		//std::cout << "ERROR: SceneGraphStructureVisualiser::addIDToRootNode(osg::ref_ptr<osg::Node> currNodeIn) was null!" << std::endl;
		return;
	}

	osg::ref_ptr<osg::Group> currGroup;
	currGroup = currNodeIn->asGroup(); // returns NULL if not a group.

	if (currGroup)
	{
		std::string nodeName = currGroup->getName();
		std::string addString;
		addString.append(std::to_string(m_currentNodeID));
		addString.append(" - ");
		nodeName.insert(0, addString);
		currGroup->setName(nodeName);

		m_nodeNames.push_back(currGroup->getName());
	}
}

//Adds a unique ID to every node in the scene graph. This is made because every node needs a unique identifier for creating the m_topology map
void SceneGraphStructureVisualiser::addIDsToRemainingNodes(osg::ref_ptr<osg::Node> currNodeIn)
{
	// check to see if we have a valid (non-NULL) node.
	if (!currNodeIn)
	{
		std::cout << "ERROR: SceneGraphStructureVisualiser::addIDsToRemainingNodes(osg::ref_ptr<osg::Node> currNodeIn) was null!" << std::endl;
		return;
	}

	osg::ref_ptr<osg::Group> currGroup;
	currGroup = currNodeIn->asGroup(); // returns NULL if not a group.

	if (currGroup)
	{
		for (unsigned int i = 0; i < currGroup->getNumChildren(); i++)
		{
			m_currentNodeID++;
			std::string nodeName = currGroup->getChild(i)->getName();
			std::string addString;
			addString.append(std::to_string(m_currentNodeID));
			addString.append(" - ");
			nodeName.insert(0, addString);
			currGroup->getChild(i)->setName(nodeName);

			m_nodeNames.push_back(currGroup->getChild(i)->getName());
		}

		for (unsigned int i = 0; i < currGroup->getNumChildren(); i++)
		{
			addIDsToRemainingNodes(currGroup->getChild(i));
		}
	}
}

//Requires run of addIDsToAllNodes() before. This function visit (its not a visiter pattern) every node and check vor child and parents to fill the m_topology map
void SceneGraphStructureVisualiser::writeTopologyMap(osg::ref_ptr<osg::Node> currNodeIn)
{
	unsigned int currNodeID;
	unsigned int nodeID;

	// check to see if we have a valid (non-NULL) node.
	if (!currNodeIn)
	{
		std::cout << "ERROR: SceneGraphStructureVisualiser::writeTopologyMap(osg::ref_ptr<osg::Node> currNodeIn) was null!" << std::endl;
		return;
	}

	osg::ref_ptr<osg::Group> currGroup;
	currGroup = currNodeIn->asGroup(); // returns NULL if not a group.

	if (currGroup)
	{
		for (unsigned int i = 0; i < currGroup->getNumChildren(); i++)
		{
			//Read ID of currNodeIn
			{
				std::string currNodeName = currGroup->getName();
				std::size_t currNodeFoundAt = currNodeName.find_first_of(" ");
				//std::cout << "currNodeFoundAt: " << currNodeFoundAt << std::endl;
				std::string currNodeIDAsString;
				currNodeIDAsString = currNodeName.substr(0, currNodeFoundAt);

				currNodeID = std::stoi(currNodeIDAsString);
			}

			//Read ID of currNodeIn's -->CHILD<--
			{
				std::string currNodeName = currGroup->getChild(i)->getName();
				std::size_t currNodeFoundAt = currNodeName.find_first_of(" ");
				//std::cout << "currNodeFoundAt: " << currNodeFoundAt << std::endl;
				std::string currNodeIDAsString;
				currNodeIDAsString = currNodeName.substr(0, currNodeFoundAt);
				nodeID = std::stoi(currNodeIDAsString);
				m_topology.insert(std::pair<unsigned int, unsigned int>(nodeID, currNodeID));
			}
		}

		if (currGroup)
		{
			for (unsigned int i = 0; i < currGroup->getNumChildren(); i++)
			{
				writeTopologyMap(currGroup->getChild(i));
			}
		}
	}
	else
	{
		//Read ID of currNodeIn
		{
			std::string currNodeName = currNodeIn->getName();
			std::size_t currNodeFoundAt = currNodeName.find_first_of(" ");
			//std::cout << "currNodeFoundAt: " << currNodeFoundAt << std::endl;
			std::string currNodeIDAsString;
			currNodeIDAsString = currNodeName.substr(0, currNodeFoundAt);
			currNodeID = std::stoi(currNodeIDAsString);
		}

		//Read ID of currNodeIn's -->PARENT<--
		{
			std::string currNodeName = currNodeIn->getParent(0)->getName();
			std::size_t currNodeFoundAt = currNodeName.find_first_of(" ");
			//std::cout << "currNodeFoundAt: " << currNodeFoundAt << std::endl;
			std::string currNodeIDAsString;
			currNodeIDAsString = currNodeName.substr(0, currNodeFoundAt);
			nodeID = std::stoi(currNodeIDAsString);
		}
		m_topology.insert(std::pair<unsigned int, unsigned int>(currNodeID, nodeID));
	}
}

void SceneGraphStructureVisualiser::saveTopologyAsXML()
{
	tinyxml2::XMLDocument xmlDoc;

	tinyxml2::XMLNode* pRoot = xmlDoc.NewElement("Root");
	xmlDoc.InsertFirstChild(pRoot);

	tinyxml2::XMLElement * pElement = xmlDoc.NewElement("IntValue");

	pElement->SetText(10);

	pRoot->InsertEndChild(pElement);

	pElement = xmlDoc.NewElement("FloatValue");
	pElement->SetText(0.5f);

	pRoot->InsertEndChild(pElement);

	//Cascade Start
	pElement = xmlDoc.NewElement("Outer");
	tinyxml2::XMLElement * pElementInner = xmlDoc.NewElement("Inner");
	pElementInner->SetText("Alpaca");
	pElement->InsertEndChild(pElementInner); //Important: Insert inner into Outer
	pRoot->InsertEndChild(pElement);
	//Cascade End

	pElement = xmlDoc.NewElement("Date");
	pElement->SetAttribute("day", 26);
	pElement->SetAttribute("month", "April");
	pElement->SetAttribute("year", 2014);
	pElement->SetAttribute("dateFormat", "26/04/2014");

	pRoot->InsertEndChild(pElement);
	tinyxml2::XMLError eResult = xmlDoc.SaveFile(m_xmlFileName.c_str());

}

//Remove all IDs again, 
void SceneGraphStructureVisualiser::removeIDs(osg::ref_ptr<osg::Node> currNodeIn)
{
	// check to see if we have a valid (non-NULL) node.
	if (!currNodeIn)
	{
		std::cout << "ERROR: SceneGraphStructureVisualiser::removeIDs(osg::ref_ptr<osg::Node> currNodeIn) was null!" << std::endl;
		return;
	}

	osg::ref_ptr<osg::Group> currGroup;
	currGroup = currNodeIn->asGroup(); // returns NULL if not a group.

	if (currGroup)
	{
		for (unsigned int i = 0; i < currGroup->getNumChildren(); i++)
		{
			std::string currNodeName = currGroup->getName();
			std::size_t currNodeFoundAt = currNodeName.find_first_of(" - ");
			currNodeName.erase(0, currNodeFoundAt+1);
			currGroup->setName(currNodeName);
		}

		for (unsigned int i = 0; i < currGroup->getNumChildren(); i++)
		{
			removeIDs(currGroup->getChild(i));
		}
	}
}

