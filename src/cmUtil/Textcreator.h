// ClayMore - Immersive Mesh Modelling --- Copyright (c) 2014-2017 Philipp Ladwig, Jannik Fiedler, Jan Beutgen
#pragma once

#include "ConfigDllExportUtil.h"

#include <string>

#include <osgText/Text>
#include <osgText/Font>


class imUtil_DLL_import_export TextCreator
{
	public:
		TextCreator();
		~TextCreator();

		static osgText::Text* createText(std::string const & textIn);
		static osgText::Text* createText(const osg::Vec3& posIn, const std::string& contentIn, float sizeIn, osgText::TextBase::AlignmentType alignIn);
		
		static osg::ref_ptr<osgText::Font> FONT;
		
	private:

};