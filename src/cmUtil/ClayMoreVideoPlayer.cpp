﻿// ClayMore - Immersive Mesh Modelling --- Copyright (c) 2014-2017 Philipp Ladwig, Jannik Fiedler, Jan Beutgen
#include "ClayMoreVideoPlayer.h"

#include <osg/Geometry>
#include <osgDB/WriteFile>


ClayMoreVideoPlayer::ClayMoreVideoPlayer(std::string sgName) :
	m_sgName(sgName)
{
	m_posAtt = new osg::PositionAttitudeTransform();
}

ClayMoreVideoPlayer::~ClayMoreVideoPlayer()
{

}

void ClayMoreVideoPlayer::init(std::string pathToVideoFile)
{
	open(pathToVideoFile);
	//play();
	setLoopingMode(VideoPlayer::NO_LOOPING);
	setVolume(0.0);

	texture00 = new osg::Texture2D;
	texture00->setTextureSize(1920, 1080);
	texture00->setResizeNonPowerOfTwoHint(true);
	texture00->setInternalFormat(GL_RGBA);
	texture00->setFilter(osg::Texture2D::MIN_FILTER, osg::Texture2D::LINEAR);
	texture00->setFilter(osg::Texture2D::MAG_FILTER, osg::Texture2D::LINEAR);
	texture00->setWrap(osg::Texture::WRAP_S, osg::Texture::CLAMP_TO_EDGE);
	texture00->setWrap(osg::Texture::WRAP_T, osg::Texture::CLAMP_TO_EDGE);
	//texture00->setImage(osgDB::readImageFile("data/UserTests/imagePlane.jpg"));
	texture00->setImage(this);
	

	osg::ref_ptr<osg::StateSet> videoStateSet = new osg::StateSet;
	videoStateSet->setTextureAttributeAndModes(0, texture00.get(), osg::StateAttribute::ON);
	videoStateSet->setMode(GL_LIGHTING, osg::StateAttribute::OFF);

	osg::Geometry* imagePlaneGeom = osg::createTexturedQuadGeometry(osg::Vec3(0.0f, 1.0f, 0.0f),
		osg::Vec3f(4.425f, 0.0f, 0.0f),
		osg::Vec3f(0.0f, -2.5f, 0.0f),
		0.0f, 0.0f, 1.0f, 1.0f);
	imagePlaneGeom->setStateSet(videoStateSet);

	m_posAtt->addChild(imagePlaneGeom);
}

void ClayMoreVideoPlayer::setPosition(osg::Vec3f positionIn)
{
	m_posAtt->setPosition(positionIn);
}

void ClayMoreVideoPlayer::setRotationAroundYAxis(float rotationYIn)
{
	osg::Quat imagePlaneRotation = osg::Quat((rotationYIn / 180.0)*M_PI, osg::Vec3d(0.0f, 1.0f, 0.0f));
	m_posAtt->setAttitude(imagePlaneRotation);
}

void ClayMoreVideoPlayer::setScale(float scaleIn)
{
	m_posAtt->setScale(osg::Vec3f(scaleIn, scaleIn, scaleIn));
}

void ClayMoreVideoPlayer::saveCurrentImage(std::string dirSavePath)
{
	osg::Image* osgDepthImage = new osg::Image();
	osgDepthImage->setImage(texture00->getTextureWidth(),
		texture00->getTextureHeight(),
		1,
		texture00->getInternalFormat(),
		GL_RGBA,
		GL_UNSIGNED_BYTE,
		this->data(),
		osg::Image::AllocationMode::NO_DELETE);

	dirSavePath.append("_MouthCam.png");

	osgDB::writeImageFile(*osgDepthImage, dirSavePath);
}