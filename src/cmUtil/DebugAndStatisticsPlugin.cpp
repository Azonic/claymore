// ClayMore - Immersive Mesh Modelling --- Copyright (c) 2014-2017 Philipp Ladwig, Jannik Fiedler, Jan Beutgen
#include "DebugAndStatisticsPlugin.h"

#include "osg/Timer"
#include <osgText/Font>
#include <osg/MatrixTransform>


DebugAndStatisticsPlugin::DebugAndStatisticsPlugin()
{

}


DebugAndStatisticsPlugin::~DebugAndStatisticsPlugin()
{

}

osg::ref_ptr<osg::MatrixTransform> DebugAndStatisticsPlugin::createFrameCounter(float charakterSizeIn, std::string fontPathsIn)
{
	osg::ref_ptr<osgText::Font> debugFont = osgText::readFontFile(fontPathsIn);
	m_FrameCounterText = new osgText::Text;

	m_FrameCounterText->setFont(debugFont.get());
	m_FrameCounterText->setName("FrameCounterText");
	m_FrameCounterText->setCharacterSize(charakterSizeIn);
	m_FrameCounterText->setAxisAlignment(osgText::TextBase::YZ_PLANE);
	float fps = 0.0;
	m_FrameCounterText->setText(std::to_string(fps));
	m_FrameCounterText->getOrCreateStateSet()->setMode(GL_CULL_FACE, osg::StateAttribute::ON);
	m_FrameCounterText->getOrCreateStateSet()->setMode(GL_LIGHTING, osg::StateAttribute::OVERRIDE | osg::StateAttribute::OFF);
	m_FrameCounterText->getOrCreateStateSet()->setRenderBinDetails(5, "DepthSortedBin");

	osg::ref_ptr<osg::MatrixTransform> frameCounterMatrixTransform = new osg::MatrixTransform();
	osg::Matrix rotateDebugText1 = osg::Matrix::rotate(osg::DegreesToRadians(270.0), 0.0, 0.0, -1.0);
	osg::Matrix rotateDebugText2 = osg::Matrix::rotate(osg::DegreesToRadians(180.0), 0.0, 1.0, 0.0);
	osg::Matrix positionDebugTextFPS = osg::Matrix::translate(osg::Vec3(0.2, -0.05, 0.10));
	osg::Matrix finalTransformFPS = rotateDebugText2 * rotateDebugText1 * positionDebugTextFPS;
	frameCounterMatrixTransform->setMatrix(finalTransformFPS);
	frameCounterMatrixTransform->addChild(m_FrameCounterText);

	return frameCounterMatrixTransform.get();
}


void DebugAndStatisticsPlugin::setFrameCounterText(std::string textIn)
{
	m_FrameCounterText->setText(textIn);
}
