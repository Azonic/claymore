// ClayMore - Immersive Mesh Modelling --- Copyright (c) 2014-2017 Philipp Ladwig, Jannik Fiedler, Jan Beutgen
#pragma once

#include <string>
#include <osg\Timer>

#include <iostream>
#include <fstream>

#include "ConfigDllExportUtil.h"

//	class for measurement of the runtime between two points in the code.
// How to use:
//	TimeWriter* timeWriter = new TimeWriter();
//	timeWriter->startMetering("Name of metering", true, true);
//  #-- stuff which shall be measured --#
//	timeWriter->endMetering();
class imUtil_DLL_import_export TimeWriter
{
	// #### CONSTRUCTOR & DESTRUCTOR ###############
public:

	TimeWriter();
	~TimeWriter();


	// #### MEMBER VARIABLES ###############
private:
	std::string m_meteringName;
	bool m_enablePrintToConsole;
	bool m_enableWriteToFile;
	std::ofstream m_meteringOutputFile;
	osg::Timer_t m_startTick;
	osg::Timer_t m_endTick;


	// #### MEMBER FUNCTIONS ###############
public:
	//@param Give the meteringProcess a name, to find it after the mesurement in the meteringOutput.txt
	//@param print to console?
	//@param write and append to meteringOutput.txt?
	void startMetering(std::string nameIn, bool enablePrintToConsoleIn, bool enableWriteToFileIn); 
	double endMetering();
	

};