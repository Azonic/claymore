// ClayMore - Immersive Mesh Modelling --- Copyright (c) 2014-2017 Philipp Ladwig, Jannik Fiedler, Jan Beutgen
#pragma once

#include "ConfigDllExportUtil.h"

//#include "imCore\defines.h"

#include <osg/Array>

//TODO: IS there a better, more central place for this?
enum ApplicationMode {
	OBJECT_MODE,
	EDIT_MODE,
	SCULPT_MODE,
	TEXTURE_MODE,
	PAINT_MODE,
	SKIN_RIG_MODE,
	SCAN_MODE
};

enum class ScanContext {
	POINT_CLOUD_MODE,
	OCTREE_MODE,
	MARCHING_CUBES_MODE
};

//Class mainly for communication between Observers and Controller Tools
class imUtil_DLL_import_export MiscDataStorage
{

public:
	//TODO: Need config manager
	MiscDataStorage();
	~MiscDataStorage();

	inline void setIsHelpTextActivated(bool isActiveIn) { m_isHelpTextActivated = isActiveIn; }
	inline bool getIsHelpTextActivated() { return m_isHelpTextActivated; }

	inline void setLastSphereSelectionToolSize(float lastSphereSelectionToolSizeIn) { m_lastSphereSelectionToolSize = lastSphereSelectionToolSizeIn; }
	inline float getLastSphereSelectionToolSize() { return m_lastSphereSelectionToolSize; }

	inline void setLastPriControllerPosition(osg::Vec3f positionIn) { m_lastPriControllerPosition = positionIn; }
	inline void setLastSecControllerPosition(osg::Vec3f positionIn) { m_lastSecControllerPosition = positionIn; }

	inline osg::Vec3f getLastPriControllerPosition() { return m_lastPriControllerPosition; }
	inline osg::Vec3f getLastSecControllerPosition() { return m_lastSecControllerPosition; }

	inline void setLastBaryCentrum(osg::Vec3f baryCentrumIn) { m_lastBaryCentrum = baryCentrumIn; }
	inline osg::Vec3f getLastBaryCentrum() { return m_lastBaryCentrum; }

	void setRumbleShortState(bool rumbleStateIn) { *m_rumbleShortState = rumbleStateIn; }
	bool* getRumbleShortState() { return m_rumbleShortState; }

	inline void setIsEDITranslateToolActive(bool isActiveIn) { m_isEDITranslateToolActive = isActiveIn; }
	inline bool* getAddressOfIsEDITranslateToolActive() { return &m_isEDITranslateToolActive; }

	inline void setIsEDIRotateToolActive(bool isActiveIn) { m_isEDIRotateToolActive = isActiveIn; }
	inline bool* getAddressOfIsEDIRotateToolActive() { return &m_isEDIRotateToolActive; }

	inline void setIsEDIScaleToolActive(bool isActiveIn) { m_isEDIScaleToolActive = isActiveIn; }
	inline bool* getAddressOfIsEDIScaleToolActive() { return &m_isEDIScaleToolActive; }



	inline void setIsOBJTranslateToolActive(bool isActiveIn) { m_isOBJTranslateToolActive = isActiveIn; }
	inline bool getIsOBJTranslateToolActive() { return m_isOBJTranslateToolActive; }

	inline void setIsOBJRotateToolActive(bool isActiveIn) { m_isOBJRotateToolActive = isActiveIn; }
	inline bool getIsOBJRotateToolActive() { return m_isOBJRotateToolActive; }

	inline void setIsOBJScaleToolActive(bool isActiveIn) { m_isOBJScaleToolActive = isActiveIn; }
	inline bool getIsOBJScaleToolActive() { return m_isOBJScaleToolActive; }

	
	inline void setIsEDISelectionConfirmToolActive(bool isActiveIn) { m_isEDISelectionConfirm = isActiveIn; }
	inline bool* getAddressOfIsEDISelectionConfirmToolActive() { return &m_isEDISelectionConfirm; }

	inline void setIsEDISelectionNegateToolActive(bool isActiveIn) { m_isEDISelectionNegate = isActiveIn; }
	inline bool* getAddressOfIsEDISelectionNegateToolActive() { return &m_isEDISelectionNegate; }

	void activateEDITransTool();
	void activateEDIRotateTool();
	void activateEDIScaleTool();

	void activateEDISelectionConfirmTool();
	void activateEDISelectionNegateTool();

	void activateConnectMeshElementTool();
	void activateAddVertsTool();

	inline void setGlobalApplicationMode(ApplicationMode isActiveIn) { m_applicationModeAddress = isActiveIn; }
	inline ApplicationMode getGlobalApplicationMode() { return m_applicationModeAddress; }

	//AddVertObserver
	inline void setIsEDIAddVertsToolActive(bool isActiveIn) { m_IsEDIAddVertsToolActive = isActiveIn; }
	inline bool* getAddressOfIsEDIAddVertsToolActive() { return &m_IsEDIAddVertsToolActive; }

	//ConnectMeshElementObserver
	inline void setIsEDIConnectMeshElementToolActive(bool isActiveIn) { m_IsEDIConnectMeshElementToolActive = isActiveIn; }
	inline bool* getAddressOfIsEDIConnectMeshElementToolActive() { return &m_IsEDIConnectMeshElementToolActive; }

	inline void setIsTriggerLeftIndicated(bool isActiveIn) { m_IsTriggerLeftIndicated = isActiveIn; }
	inline bool getIsTriggerLeftIndicated() { return m_IsTriggerLeftIndicated; }

	inline void setIsSqueezeLeftIndicated(bool isActiveIn) { m_IsSqueezeLeftIndicated = isActiveIn; }
	inline bool getIsSqueezeLeftIndicated() { return m_IsSqueezeLeftIndicated; }

	inline void setIsTriggerRightIndicated(bool isActiveIn) { m_IsTriggerRightIndicated = isActiveIn; }
	inline bool getIsTriggerRightIndicated() { return m_IsTriggerRightIndicated; }

	inline void setIsSqueezeRightIndicated(bool isActiveIn) { m_IsSqueezeRightIndicated = isActiveIn; }
	inline bool getIsSqueezeRightIndicated() { return m_IsSqueezeRightIndicated; }

	inline void setScanContext(ScanContext isActiveIn) { m_scanContextAddress = isActiveIn; }
	inline ScanContext getScanContext() { return m_scanContextAddress; }

private:
	ApplicationMode m_applicationModeAddress;
	bool m_isHelpTextActivated;
	osg::Vec3f m_lastPriControllerPosition, m_lastSecControllerPosition, m_lastBaryCentrum;
	float m_lastSphereSelectionToolSize;
	bool *m_rumbleShortState;

	//Maybe better using an enum, because it could always be only one active of the three...
	bool m_isOBJTranslateToolActive, m_isOBJRotateToolActive, m_isOBJScaleToolActive;
	bool m_isEDITranslateToolActive, m_isEDIRotateToolActive, m_isEDIScaleToolActive;
	bool m_IsEDIAddVertsToolActive, m_IsEDIConnectMeshElementToolActive;
	bool m_isEDISelectionConfirm, m_isEDISelectionNegate;

	bool m_IsTriggerLeftIndicated, m_IsSqueezeLeftIndicated, m_IsTriggerRightIndicated, m_IsSqueezeRightIndicated;
	
	ScanContext m_scanContextAddress;
};