#include "OptimizedBoolVector.h"

OptimizedBoolVector::OptimizedBoolVector(const size_t size) {
	byteSize = (size_t)(size / 8ULL);
	if (size % 8ULL > 0) byteSize++;
	bitSize = size;
	begin = new char[byteSize]();
}

OptimizedBoolVector::OptimizedBoolVector(char* begin, const size_t size) {
	byteSize = (size_t)(size / 8ULL);
	if (size % 8ULL > 0) byteSize++;
	bitSize = size;
	this->begin = begin;
}

OptimizedBoolVector::~OptimizedBoolVector() {
	delete[] begin;
}

inline void OptimizedBoolVector::getIndices(const size_t index, size_t& byteOut, unsigned char& bitOut) const {
	byteOut = (size_t)(index / 8ULL);
	bitOut = index % 8ULL;
}

/*void OptimizedBoolVector::resize(const size_t size) {
	if (bitSize == size) return;
	byteSize = (size_t) (size / 8ULL);
	if (size % 8ULL > 0) byteSize++;
	bitSize = size;

	char* newBegin = new char[byteSize]();
	std::memcpy(newBegin, begin, size > bitSize ? );
}*/

void OptimizedBoolVector::fill(const bool value) {
	const char target = value ? 255 : 0;
	for (size_t byte = 0; byte < bytes(); byte++)
		begin[byte] = target;
}

void OptimizedBoolVector::set(const size_t index, const bool value) {
	size_t byte;
	unsigned char bit;
	getIndices(index, byte, bit);
	set(byte, bit, value);
}

void OptimizedBoolVector::set(const size_t byte, const unsigned char bit, const bool value) {
	//if (value) begin[byte] |= 1 << bit;
	//else begin[byte] &= ~(1 << bit);
	set(begin + byte, bit, value);
}

void OptimizedBoolVector::set(char* byte, const unsigned char bit, const bool value) {
	if (value) *byte |= 1 << bit;
	else *byte &= ~(1 << bit);
}

bool OptimizedBoolVector::at(const size_t index) const {
	size_t byte;
	unsigned char bit;
	getIndices(index, byte, bit);
	return at(byte, bit);
}

bool OptimizedBoolVector::at(const size_t byte, const unsigned char bit) const {
	return (*(begin + byte) >> bit) & 1;
}

size_t OptimizedBoolVector::bytes() const {
	return byteSize;
}

size_t OptimizedBoolVector::bits() const {
	return bitSize;
}

char* OptimizedBoolVector::getBegin() {
	return begin;
}