#pragma once

#include "ThreadSafeQueue.h"

template<class T>
ThreadSafeQueue<T>::ThreadSafeQueue() : m_lock(false) { }

template<class T>
ThreadSafeQueue<T>::~ThreadSafeQueue() { }

template<class T>
void ThreadSafeQueue<T>::lock() {
	bool expected;
	while (!m_lock.compare_exchange_weak(expected = false, true));
}

template<class T>
void ThreadSafeQueue<T>::unlock() {
	m_lock = false;
}

template<class T>
void ThreadSafeQueue<T>::push(T element) {
	lock();
	m_queue.push(element);
	unlock();
}

template<class T>
bool ThreadSafeQueue<T>::pop(T& result) {
	lock();
	bool isEmpty = m_queue.empty();
	if (!isEmpty) {
		result = m_queue.front();
		m_queue.pop();
	}
	unlock();
	return isEmpty;
}

template<class T>
void ThreadSafeQueue<T>::pushBatch(T* elements, unsigned int count) {
	lock();
	for (unsigned int i = 0u; i < count; ++i) {
		m_queue.push(elements[i]);
	}
	unlock();
}
