//############################################################################################
// Immersive Modelling Application - Copyright (C) 2014-2015 Philipp Ladwig and Jannik Fiedler
//############################################################################################

#pragma once

#include "ConfigDllExportUtil.h"

#include <map>
#include <osg\Group>

class imUtil_DLL_import_export SceneGraphStructureVisualiser
{
	public:

		SceneGraphStructureVisualiser();
		~SceneGraphStructureVisualiser();
		bool visualise(osg::ref_ptr<osg::Group> rootNode);
		
	private:
		void addIDToRootNode(osg::ref_ptr<osg::Node> childNodeIn);
		void addIDsToRemainingNodes(osg::ref_ptr<osg::Node> childNodeIn);
		void writeTopologyMap(osg::ref_ptr<osg::Node> currNodeIn);
		void saveTopologyAsXML();
		void removeIDs(osg::ref_ptr<osg::Node> currNodeIn);
		std::map<unsigned int, unsigned int> m_topology;
		std::vector<std::string> m_nodeNames;
		unsigned int m_currentNodeID;
		std::string m_xmlFileName;
};