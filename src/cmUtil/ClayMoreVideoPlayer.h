﻿// ClayMore - Immersive Mesh Modelling --- Copyright (c) 2014-2017 Philipp Ladwig, Jannik Fiedler, Jan Beutgen
#pragma once

#include "VideoPlayer.h"
#include <osg/Group>
#include <osg/Texture2D>
#include <osg/PositionAttitudeTransform>

#include "ConfigDllExportUtil.h"

class imUtil_DLL_import_export ClayMoreVideoPlayer : public VideoPlayer
{
public:
	ClayMoreVideoPlayer(std::string sgName);
	~ClayMoreVideoPlayer();

	void init(std::string pathToVideoFile);
	inline void attachToSceneGraph(osg::Group* parentGroupIn) { parentGroupIn->addChild(m_posAtt); }
	void setPosition(osg::Vec3f positionIn);
	void setRotationAroundYAxis(float rotationYIn);
	void setScale(float scaleIn);
	void saveCurrentImage(std::string fileName);

private:
	osg::ref_ptr<osg::Texture2D> texture00;
	std::string m_sgName;
	std::string m_pathToVideoFile;
	osg::ref_ptr<osg::PositionAttitudeTransform> m_posAtt;

};
