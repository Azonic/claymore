// ClayMore - Immersive Mesh Modelling --- Copyright (c) 2014-2017 Philipp Ladwig, Jannik Fiedler, Jan Beutgen
#include "UserTestLogger.h"

#include <iostream>
#include <fstream>
#include <sstream>

//TODO:Delete include
//#include <osgDB/ReadFile>

#include <math.h>

UserTestLogger::UserTestLogger()
{
	m_matrixTransform = new osg::MatrixTransform();
	m_status = NOT_YET_STARTED;
	m_userTrigger = false;
	m_processingCallback = false;

	m_xmlMenuType = 0;
	m_xmlButtonCount = 0;
	m_xmlItemLayer = 0;

	m_triggerOnce = true;

	m_numberOfIterationInLevel2 = 10; //10 Items
	m_numberOfIterationInLevel1 = 5; //5 different counts of Button for every of the...
	m_numberOfIterationInLebel0 = 4; //4 menus

	m_smallChracterSize = 0.35;
	m_bigCharacterSize = 2.0;

	m_onlyWriteOnce = true;
}

UserTestLogger::~UserTestLogger()
{

}

bool UserTestLogger::readUserTest1ControlXML(const char * filePath)
{
	//Read XML file
	tinyxml2::XMLDocument xmlFile;
	int res = xmlFile.LoadFile(filePath);

	if (res != tinyxml2::XMLError::XML_SUCCESS)
	{
		std::string errorMessage = " ### ERROR in UserTestLogger::readUserTest1ControlXML() - ErrorCode: ";
		errorMessage.append(std::to_string(res));
		errorMessage.append(" from enum tinyxml2::XMLError::XML_SUCCESS");
		std::cerr << errorMessage; // ### <--- Have a look in the console ###
		throw(std::exception(errorMessage.c_str()));
	}


	//Loop over all first level nodes
	tinyxml2::XMLElement const *rootElement = xmlFile.RootElement();
	short level0 = 0;
	for (tinyxml2::XMLElement const *firstLevelNodes = rootElement->FirstChildElement();
		firstLevelNodes != NULL;
		firstLevelNodes = firstLevelNodes->NextSiblingElement())
	{
		short level1 = 0;
		std::vector<std::vector<unsigned short>> level0PushBack;
		m_xmlAsEnumerableMap.push_back(level0PushBack);
		//Hier 4 pie, tiltshift, fence, dart
		for (tinyxml2::XMLElement const *secondLevelNodes = firstLevelNodes->FirstChildElement();
			secondLevelNodes != NULL;
			secondLevelNodes = secondLevelNodes->NextSiblingElement())
		{
			short level2 = 0;
			std::vector<unsigned short> level1PushBack;
			m_xmlAsEnumerableMap.at(level0).push_back(level1PushBack);
			//Hier test_pie_4_number, etc
			for (tinyxml2::XMLElement const *thirdLevelNodes = secondLevelNodes->FirstChildElement();
				thirdLevelNodes != NULL;
				thirdLevelNodes = thirdLevelNodes->NextSiblingElement())
			{
				//1, 2, 3, etc
				m_xmlAsEnumerableMap.at(level0).at(level1).push_back(std::stoi(thirdLevelNodes->GetText()));
				level2++;
			}
			level1++;
		}
		level0++;
	}
	return true;
}

void UserTestLogger::startUserTest(const char * filePath)
{
	m_status = STARTED;

	m_textShownInSG = TextCreator::createText("User test 1 is active.\nHit the start button, if you are ready.");
	m_textShownInSG->setCharacterSize(m_smallChracterSize);
	m_textShownInSG->getOrCreateStateSet();

	osg::Matrix rotateDebugText1 = osg::Matrix::rotate(osg::DegreesToRadians(90.0), 0.0, 1.0, 0.0);
	osg::Matrix rotateDebugText2 = osg::Matrix::rotate(osg::DegreesToRadians(90.0), -1.0, 0.0, 0.0);
	osg::ref_ptr<osg::MatrixTransform> speechTextTransform = new osg::MatrixTransform();
	osg::Matrix positionDebugTextSpeech = osg::Matrix::translate(osg::Vec3(0.0, 2.5, 5.0));
	osg::Matrix finalTransformSpeech = rotateDebugText2 * rotateDebugText1 * positionDebugTextSpeech;
	speechTextTransform->setMatrix(finalTransformSpeech);
	//speechTextTransform->addChild(speechCommandText);
	speechTextTransform->addChild(m_textShownInSG);
	m_matrixTransform->addChild(speechTextTransform);
	m_status = TESTPHASE;
}

void UserTestLogger::update()
{
	if (m_status == TESTPHASE)
	{
		std::cout << "in TESTPHASE" << std::endl;
		if (m_triggerOnce)
		{
			if (m_xmlMenuType == 0)
			{
				m_textShownInSG->setCharacterSize(m_smallChracterSize);
				m_textShownInSG->setText("Hallo :)\nDr�ck auf das gelbe Touchpad! Dies ist das Torten Men�.\nWenn du bereit bist, dr�ck auf den roten Button und ein Countdown startet.\n Nach Ende jeden Countdowns wird das Menu geschlossen!");
				m_callbacks["MENU_STRUCTURE_PIE_NUM_4"]();
				m_callbacks["REG_USER_TEST_1_OBSERVER"]();
			}
			if (m_xmlMenuType == 1)
			{
				m_textShownInSG->setCharacterSize(m_smallChracterSize);
				m_textShownInSG->setText("Dr�ck auf das gelbe Touchpad zum Ausprobieren.\nWenn du bereit bist, dr�ck auf den roten Button.");
				m_callbacks["MENU_STRUCTURE_TILT_SHIFT_NUM_4"](); //############################################################### PENDING
				m_callbacks["REG_USER_TEST_1_OBSERVER"]();
			}
			if (m_xmlMenuType == 2)
			{
				m_textShownInSG->setCharacterSize(m_smallChracterSize);
				m_textShownInSG->setText("Dr�ck auf das gelbe Touchpad zum Ausprobieren.\nWenn du bereit bist, dr�ck auf den roten Button.");
				m_callbacks["MENU_STRUCTURE_FENCE_NUM_4"](); //############################################################### PENDING
				m_callbacks["REG_USER_TEST_1_OBSERVER"]();
			}
			if (m_xmlMenuType == 3)
			{
				m_textShownInSG->setCharacterSize(m_smallChracterSize);
				m_textShownInSG->setText("Dr�ck auf das gelbe Touchpad zum Ausprobieren.\nWenn du bereit bist, dr�ck auf den roten Button.");
				m_callbacks["MENU_STRUCTURE_DART_NUM_4"](); //############################################################### PENDING
				m_callbacks["REG_USER_TEST_1_OBSERVER"]();
			}
			m_triggerOnce = false;
		}
		if (m_userTrigger)
		{
			std::cout << "in TESTPHASE zu COUNTDOWN" << std::endl;
			m_status = COUNTDOWN;
			m_userTrigger = false;
			m_triggerOnce = true;
			m_tick = osg::Timer::instance()->tick();
			return;
		}
	}

	if (m_status == GET_READY_BUTTON4)
	{
		//PIE MENU
		if (m_xmlMenuType == 0)
		{
			if (m_triggerOnce)
			{
				m_textShownInSG->setCharacterSize(m_smallChracterSize);
				m_textShownInSG->setText("Jetzt kommt das Torten Men�!\nProbier es aus. Dr�ck auf die gelbe Fl�che.\nWenn du bereit bist, dr�ck auf den roten Button.");
				m_callbacks["HIDE_ALL_MENUS"]();
				m_callbacks["MENU_STRUCTURE_PIE_NUM_4"]();
				m_callbacks["REG_USER_TEST_1_OBSERVER"]();
				m_triggerOnce = false;
			}
		}
		//TILT SHIFT MENU
		else if (m_xmlMenuType == 1)
		{
			if (m_triggerOnce)
			{
				m_textShownInSG->setCharacterSize(m_smallChracterSize);
				m_textShownInSG->setText("Jetzt kommt das Tempomat Men�! \nProbier es aus. Dr�ck auf die gelbe Fl�che.\nWenn du den roten Button dr�ckst, geht's weiter.");
				m_callbacks["HIDE_ALL_MENUS"]();
				m_callbacks["MENU_STRUCTURE_TILT_SHIFT_NUM_4"]();
				m_callbacks["REG_USER_TEST_1_OBSERVER"]();
				m_triggerOnce = false;
			}
		}
		//FENCE MENU
		else if (m_xmlMenuType == 2)
		{
			if (m_triggerOnce)
			{
				m_textShownInSG->setCharacterSize(m_smallChracterSize);
				m_textShownInSG->setText("Jetzt kommt das Gartenzaun Men�!\n Probier es aus :) \nWeiter mit dem roten Button.");
				m_callbacks["HIDE_ALL_MENUS"]();
				m_callbacks["MENU_STRUCTURE_FENCE_NUM_4"]();
				m_callbacks["REG_USER_TEST_1_OBSERVER"]();
				m_triggerOnce = false;
			}
		}
		//DART MENU -> DART MENU
		else if (m_xmlMenuType == 3)
		{
			if (m_triggerOnce)
			{
				m_textShownInSG->setCharacterSize(m_smallChracterSize);
				m_textShownInSG->setText("Jetzt kommt das Dart Men�!\nWeiter mit dem roten Button.");
				m_callbacks["HIDE_ALL_MENUS"]();
				m_callbacks["MENU_STRUCTURE_DART_NUM_4"]();
				m_callbacks["REG_USER_TEST_1_OBSERVER"]();
				m_triggerOnce = false;
			}
		}

		if (m_userTrigger)
		{
			m_status = COUNTDOWN;
			m_userTrigger = false;
			m_triggerOnce = true;
			m_tick = osg::Timer::instance()->tick();
		}
	}

	if (m_status == GET_READY_BUTTON6)
	{
		//PIE MENU
		if (m_xmlMenuType == 0)
		{
			if (m_triggerOnce)
			{
				m_textShownInSG->setCharacterSize(m_smallChracterSize);
				m_textShownInSG->setText("Jetzt 6 Buttons!\nWeiter mit dem roten Button.");
				m_callbacks["HIDE_ALL_MENUS"]();
				m_callbacks["SHOW_CURRENT_MENU"](); //########################################################################WITHOUT ONLY FUNCTION !!! Only called here
				m_callbacks["MENU_STRUCTURE_PIE_NUM_6"]();
				m_callbacks["REG_USER_TEST_1_OBSERVER"]();
				m_triggerOnce = false;
			}
		}
		//TILT SHIFT MENU
		else if (m_xmlMenuType == 1)
		{
			if (m_triggerOnce)
			{
				m_textShownInSG->setCharacterSize(m_smallChracterSize);
				m_textShownInSG->setText("6 Buttons\nWeiter mit dem roten Button.");
				m_callbacks["HIDE_ALL_MENUS"]();
				m_callbacks["MENU_STRUCTURE_TILT_SHIFT_NUM_6"]();
				m_callbacks["REG_USER_TEST_1_OBSERVER"]();
				m_triggerOnce = false;
			}
		}
		//FENCE MENU
		else if (m_xmlMenuType == 2)
		{
			if (m_triggerOnce)
			{
				m_textShownInSG->setCharacterSize(m_smallChracterSize);
				m_textShownInSG->setText("6 Buttons\nWeiter mit dem roten Button.");
				m_callbacks["HIDE_ALL_MENUS"]();
				m_callbacks["MENU_STRUCTURE_FENCE_NUM_6"]();
				m_callbacks["REG_USER_TEST_1_OBSERVER"]();
				m_triggerOnce = false;
			}
		}
		//DART MENU -> DART MENU
		else if (m_xmlMenuType == 3)
		{
			if (m_triggerOnce)
			{
				m_textShownInSG->setCharacterSize(m_smallChracterSize);
				m_textShownInSG->setText("6 Buttons\nWeiter mit dem roten Button.");
				m_callbacks["HIDE_ALL_MENUS"]();
				m_callbacks["MENU_STRUCTURE_DART_NUM_6"]();
				m_callbacks["REG_USER_TEST_1_OBSERVER"]();
				m_triggerOnce = false;
			}
		}

		if (m_userTrigger)
		{
			m_status = COUNTDOWN;
			m_userTrigger = false;
			m_triggerOnce = true;
			m_tick = osg::Timer::instance()->tick();
		}
	}

	if (m_status == GET_READY_BUTTON8)
	{
		//PIE MENU
		if (m_xmlMenuType == 0)
		{
			if (m_triggerOnce)
			{
				m_textShownInSG->setCharacterSize(m_smallChracterSize);
				m_textShownInSG->setText("8 Buttons\nWeiter mit dem roten Button.");
				m_callbacks["HIDE_ALL_MENUS"]();
				m_callbacks["MENU_STRUCTURE_PIE_NUM_8"]();
				m_callbacks["REG_USER_TEST_1_OBSERVER"]();
				m_triggerOnce = false;
			}
		}
		//TILT SHIFT MENU
		else if (m_xmlMenuType == 1)
		{
			if (m_triggerOnce)
			{
				m_textShownInSG->setCharacterSize(m_smallChracterSize);
				m_textShownInSG->setText("8 Buttons\nWeiter mit dem roten Button.");
				m_callbacks["HIDE_ALL_MENUS"]();
				m_callbacks["MENU_STRUCTURE_TILT_SHIFT_NUM_8"]();
				m_callbacks["REG_USER_TEST_1_OBSERVER"]();
				m_triggerOnce = false;
			}
		}
		//FENCE MENU
		else if (m_xmlMenuType == 2)
		{
			if (m_triggerOnce)
			{
				m_textShownInSG->setCharacterSize(m_smallChracterSize);
				m_textShownInSG->setText("8 Buttons\nWeiter mit dem roten Button.");
				m_callbacks["HIDE_ALL_MENUS"]();
				m_callbacks["MENU_STRUCTURE_FENCE_NUM_8"]();
				m_callbacks["REG_USER_TEST_1_OBSERVER"]();
				m_triggerOnce = false;
			}
		}
		//DART MENU -> DART MENU
		else if (m_xmlMenuType == 3)
		{
			if (m_triggerOnce)
			{
				m_textShownInSG->setCharacterSize(m_smallChracterSize);
				m_textShownInSG->setText("8 Buttons\nWeiter mit dem roten Button.");
				m_callbacks["HIDE_ALL_MENUS"]();
				m_callbacks["MENU_STRUCTURE_DART_NUM_8"]();
				m_callbacks["REG_USER_TEST_1_OBSERVER"]();
				m_triggerOnce = false;
			}
		}

		if (m_userTrigger)
		{
			m_status = COUNTDOWN;
			m_userTrigger = false;
			m_triggerOnce = true;
			m_tick = osg::Timer::instance()->tick();
		}
	}

	if (m_status == GET_READY_BUTTON10)
	{
		//PIE MENU
		if (m_xmlMenuType == 0)
		{
			if (m_triggerOnce)
			{
				m_textShownInSG->setCharacterSize(m_smallChracterSize);
				m_textShownInSG->setText("10 Buttons\nWeiter mit dem roten Button.");
				m_callbacks["HIDE_ALL_MENUS"]();
				m_callbacks["MENU_STRUCTURE_PIE_NUM_10"]();
				m_callbacks["REG_USER_TEST_1_OBSERVER"]();
				m_triggerOnce = false;
			}
		}
		//TILT SHIFT MENU
		else if (m_xmlMenuType == 1)
		{
			if (m_triggerOnce)
			{
				m_textShownInSG->setCharacterSize(m_smallChracterSize);
				m_textShownInSG->setText("10 Buttons\nWeiter mit dem roten Button.");
				m_callbacks["HIDE_ALL_MENUS"]();
				m_callbacks["MENU_STRUCTURE_TILT_SHIFT_NUM_10"]();
				m_callbacks["REG_USER_TEST_1_OBSERVER"]();
				m_triggerOnce = false;
			}
		}
		//FENCE MENU
		else if (m_xmlMenuType == 2)
		{
			if (m_triggerOnce)
			{
				m_textShownInSG->setCharacterSize(m_smallChracterSize);
				m_textShownInSG->setText("10 Buttons\nWeiter mit dem roten Button.");
				m_callbacks["HIDE_ALL_MENUS"]();
				m_callbacks["MENU_STRUCTURE_FENCE_NUM_10"]();
				m_callbacks["REG_USER_TEST_1_OBSERVER"]();
				m_triggerOnce = false;
			}
		}
		//DART MENU -> DART MENU
		else if (m_xmlMenuType == 3)
		{
			if (m_triggerOnce)
			{
				m_textShownInSG->setCharacterSize(m_smallChracterSize);
				m_textShownInSG->setText("10 Buttons\nWeiter mit dem roten Button.");
				m_callbacks["HIDE_ALL_MENUS"]();
				m_callbacks["MENU_STRUCTURE_DART_NUM_10"]();
				m_callbacks["REG_USER_TEST_1_OBSERVER"]();
				m_triggerOnce = false;
			}
		}

		if (m_userTrigger)
		{
			m_status = COUNTDOWN;
			m_userTrigger = false;
			m_triggerOnce = true;
			m_tick = osg::Timer::instance()->tick();
		}
	}

	if (m_status == GET_READY_BUTTON12)
	{
		//PIE MENU
		if (m_xmlMenuType == 0)
		{
			if (m_triggerOnce)
			{
				m_textShownInSG->setCharacterSize(m_smallChracterSize);
				m_textShownInSG->setText("12 Buttons\nWeiter mit dem roten Button.");
				m_callbacks["HIDE_ALL_MENUS"]();
				m_callbacks["MENU_STRUCTURE_PIE_NUM_12"]();
				m_callbacks["REG_USER_TEST_1_OBSERVER"]();
				m_triggerOnce = false;
			}
		}
		//TILT SHIFT MENU
		else if (m_xmlMenuType == 1)
		{
			if (m_triggerOnce)
			{
				m_textShownInSG->setCharacterSize(m_smallChracterSize);
				m_textShownInSG->setText("12 Buttons\nWeiter mit dem roten Button.");
				m_callbacks["HIDE_ALL_MENUS"]();
				m_callbacks["MENU_STRUCTURE_TILT_SHIFT_NUM_12"]();
				m_callbacks["REG_USER_TEST_1_OBSERVER"]();
				m_triggerOnce = false;
			}
		}
		//FENCE MENU
		else if (m_xmlMenuType == 2)
		{
			if (m_triggerOnce)
			{
				m_textShownInSG->setCharacterSize(m_smallChracterSize);
				m_textShownInSG->setText("12 Buttons\nWeiter mit dem roten Button.");
				m_callbacks["HIDE_ALL_MENUS"]();
				m_callbacks["MENU_STRUCTURE_FENCE_NUM_12"]();
				m_callbacks["REG_USER_TEST_1_OBSERVER"]();
				m_triggerOnce = false;
			}
		}
		//DART MENU -> DART MENU
		else if (m_xmlMenuType == 3)
		{
			if (m_triggerOnce)
			{
				m_textShownInSG->setCharacterSize(m_smallChracterSize);
				m_textShownInSG->setText("12 Buttons\nWeiter mit dem roten Button.");
				m_callbacks["HIDE_ALL_MENUS"]();
				m_callbacks["MENU_STRUCTURE_DART_NUM_12"]();
				m_callbacks["REG_USER_TEST_1_OBSERVER"]();
				m_triggerOnce = false;
			}
		}

		if (m_userTrigger)
		{
			m_status = COUNTDOWN;
			m_userTrigger = false;
			m_triggerOnce = true;
			m_tick = osg::Timer::instance()->tick();
		}
	}

	if (m_status == COUNTDOWN)
	{
		//std::cout << "in COUNTDOWN" << std::endl;
		if (osg::Timer::instance()->delta_m(m_tick, osg::Timer::instance()->tick()) < 1000)
		{
			m_textShownInSG->setCharacterSize(m_smallChracterSize);
			m_textShownInSG->setText("Noch 2sek...");
		}
		if (osg::Timer::instance()->delta_m(m_tick, osg::Timer::instance()->tick()) > 1000 && osg::Timer::instance()->delta_m(m_tick, osg::Timer::instance()->tick()) < 2000)
		{
			m_textShownInSG->setCharacterSize(m_smallChracterSize);
			m_textShownInSG->setText("Noch 1sek!");
		}

		if (osg::Timer::instance()->delta_m(m_tick, osg::Timer::instance()->tick()) > 2000)
		{
			m_callbacks["HIDE_ALL_MENUS"]();
			m_status = WAIT_ON_CALLBACK;
			m_tick = osg::Timer::instance()->tick();
		}
		return;
	}

	if (m_status == WAIT_ON_CALLBACK)
	{
		//Set new Text
		m_textShownInSG->setCharacterSize(2.0);
		m_textShownInSG->setText(std::to_string(m_xmlAsEnumerableMap.at(m_xmlMenuType).at(m_xmlButtonCount).at(m_xmlItemLayer)));

		//std::cout << "in WAIT_ON_CALLBACK" << std::endl;
		if (m_processingCallback)
		{
			//std::cout << "in if(m_processingCallback)" << std::endl;
			if (m_currentCommand.compare(std::to_string(m_xmlAsEnumerableMap.at(m_xmlMenuType).at(m_xmlButtonCount).at(m_xmlItemLayer))) == 0)
			{
				std::cout << "in RICHTIG GEDR�CKT. Taken " << osg::Timer::instance()->delta_m(m_tick, osg::Timer::instance()->tick()) << "ms" << std::endl;
				//LOG THE TIME
				m_loggedTimes.push_back(osg::Timer::instance()->delta_m(m_tick, osg::Timer::instance()->tick()));
				m_processingCallback = false;
			}
			else
			{
				std::cout << "in FALSCH GEDR�CKT" << std::endl;
				m_loggedTimes.push_back(0.0);
				m_processingCallback = false;
			}

			m_xmlItemLayer++;

			//If all items are iterared through, m_xmlButtonCount will be incremented and...
			//...the appropriate menus structure is loaded (or rather the prepartion mode for the next mode is loaded)...
			//..and the item counter is set back to 0 for incremeting it again between 0 and 5 (depending how the xml look like(and what is set in CTOR))
			if (m_xmlItemLayer == m_numberOfIterationInLevel2)
			{
				m_xmlButtonCount++;
				if (m_xmlButtonCount == 1)
				{
					m_status = GET_READY_BUTTON6;
				}
				else if (m_xmlButtonCount == 2)
				{
					m_status = GET_READY_BUTTON8;
				}
				else if (m_xmlButtonCount == 3)
				{
					m_status = GET_READY_BUTTON10;
				}
				else if (m_xmlButtonCount == 4)
				{
					m_status = GET_READY_BUTTON12;
				}
				m_xmlItemLayer = 0;
			}

			if (m_xmlButtonCount == m_numberOfIterationInLevel1)
			{
				//std::cout << "############################# MENU CHANGE ######################" << std::endl;
				m_xmlButtonCount = 0;
				m_xmlMenuType++;
				m_status = GET_READY_BUTTON4;
				if (m_xmlMenuType == m_numberOfIterationInLebel0)
				{
					m_status = FINISHED;
				}
			}
			//Reset command
			m_currentCommand = "NULL";
			//Reset counter
			m_tick = osg::Timer::instance()->tick();
		}
	}

	if (m_status == FINISHED)
	{
		if (m_onlyWriteOnce)
		{
			writeUserTestLogXML();
			m_textShownInSG->setCharacterSize(m_bigCharacterSize);
			m_textShownInSG->setText("Ende des Tests!\n \n Danke :)"); m_textShownInSG->setCharacterSize(m_smallChracterSize);
			m_onlyWriteOnce = false;
		}
	}
}

void UserTestLogger::calledCallback(std::string calledCallbackNameIn)
{
	if (m_status == WAIT_ON_CALLBACK)
	{
		m_currentCommand = calledCallbackNameIn;
		m_processingCallback = true;
	}
	//if (m_currentCommand.compare(calledCallbackNameIn) == 0)
	//{
	//	std::cout << "Is gleich" << std::endl;
	//}

}

bool UserTestLogger::writeUserTestLogXML()
{
	//Create filename based on the current date and time
	time_t t = time(0);   // get time now
	struct tm * now = localtime(&t);
	std::stringstream finalFileSavePath;
	finalFileSavePath <<
		"usertest1-"
		<< now->tm_year + 1900 << "-"
		<< now->tm_mon + 1 << "-"
		<< now->tm_mday << "---"
		<< now->tm_hour << "-"
		<< now->tm_min << "-"
		<< now->tm_sec << ".txt";

	std::ofstream myfile;
	myfile.open(finalFileSavePath.str());

	unsigned short itemCounter = 0;
	unsigned short buttonCounter = 0;
	unsigned short menuCounter = 0;
	myfile << "####################################### -- PIE \n";
	for (unsigned int i = 0; i < m_loggedTimes.size(); ++i)
	{
		itemCounter++;
		if (itemCounter == 5)
			myfile << "---\n";
		myfile << (int)m_loggedTimes.at(i) << "\n";

		//Inserting some structure into the file
		if (itemCounter == m_numberOfIterationInLevel2)
		{
			if (buttonCounter == 0)
				myfile << "__________6-BUTTONS \n";
			if (buttonCounter == 1)
				myfile << "__________8-BUTTONS \n";
			if (buttonCounter == 2)
				myfile << "__________10-BUTTONS \n";
			if (buttonCounter == 3)
				myfile << "__________12-BUTTONS \n";
			itemCounter = 0;
			buttonCounter++;
		}

		if (buttonCounter == m_numberOfIterationInLevel1)
		{
			if (menuCounter == 0) {
				myfile << "####################################### -- TILT \n";
			}
			if (menuCounter == 1) {
				myfile << "####################################### -- FENCE \n";
			}
			if (menuCounter == 2) {
				myfile << "####################################### -- DART \n";
			}
			buttonCounter = 0;
			menuCounter++;
		}
	}
	myfile << "END.\n";
	myfile.close();
	return true;
}

void UserTestLogger::setCallback(std::string speechCommand, void(*callback)())
{
	m_callbacks.insert(std::pair<std::string, void(*)()>(speechCommand, callback));
}

void UserTestLogger::userTrigger()
{
	if (m_status != WAIT_ON_CALLBACK)
	{
		m_userTrigger = true;
	}
}