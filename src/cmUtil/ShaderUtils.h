// ClayMore - Immersive Mesh Modelling --- Copyright (c) 2014-2017 Philipp Ladwig, Jannik Fiedler, Jan Beutgen
#pragma once

#include "ConfigDllExportUtil.h"

#include <osg/ShadeModel>
#include <string>

class imUtil_DLL_import_export ShaderUtils
{
public:

	ShaderUtils();
	~ShaderUtils();

static bool loadShaderSource(osg::Shader* obj, const std::string& fileName);

private:
};