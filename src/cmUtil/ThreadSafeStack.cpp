#pragma once

#include "ThreadSafeStack.h"

template<class T>
ThreadSafeStack<T>::ThreadSafeStack() : m_lock(false) { }

template<class T>
ThreadSafeStack<T>::~ThreadSafeStack() { }

template<class T>
void ThreadSafeStack<T>::lock() {
	bool expected;
	while (!m_lock.compare_exchange_weak(expected = false, true));
}

template<class T>
void ThreadSafeStack<T>::unlock() {
	m_lock = false;
}

template<class T>
void ThreadSafeStack<T>::push(T element) {
	lock();
	m_stack.push(element);
	unlock();
}

template<class T>
bool ThreadSafeStack<T>::pop(T& result) {
	lock();
	bool notEmpty = !m_stack.empty();
	if (notEmpty) {
		result = m_stack.top();
		m_stack.pop();
	}
	unlock();
	return notEmpty;
}

template<class T>
void ThreadSafeStack<T>::pushBatch(T* elements, unsigned int count) {
	lock();
	for (unsigned int i = 0u; i < count; ++i) m_stack.push(elements[i]);
	unlock();
}
