#include "ATen/cuda/CUDAMultiStreamGuard.h"
#include "rgbdFaceAvatarGenerator.h"
#include "cmUtil/TimeWriter.h"
//#include "cuda_profiler_api.h"

RGBDFaceAvatarGenerator::RGBDFaceAvatarGenerator(string pathToModel, int imageSize, 
	NetColorFormat netColorFormat, int heatmapLineThickness, bool visualisation) {
	m_modelPath = pathToModel;
	m_imageSize = imageSize;
	m_netColorFormat = netColorFormat;
	m_visualisation = visualisation;
	m_heatmapLineThickness = heatmapLineThickness;

	m_heatmap = Mat::zeros(m_imageSize, m_imageSize, CV_32FC1);
	m_PointA = Point(0, 0);
	m_PointB = Point(0, 0);

	m_depth = GpuMat(m_imageSize, m_imageSize, CV_8UC1);
	m_notMaskedRGBDImage = GpuMat(m_imageSize, m_imageSize, CV_8UC4);
	for (int i = 0; i < numbersOfDepthMasks; i++)
		m_depthMask[i] = GpuMat(m_imageSize, m_imageSize, CV_8UC1);
	m_erode5x5 = cuda::createMorphologyFilter(
		MORPH_ERODE,
		m_depthMask[0].type(),
		getStructuringElement(MORPH_RECT, Size(5, 5)),
		Point(-1, -1),
		1);

	m_outputRGBDImage = GpuMat(m_imageSize, m_imageSize, CV_8UC4);
	cuda::setDevice(0);

	loadTorchModel();
}

void RGBDFaceAvatarGenerator::loadTorchModel()
{
	cout << "Load model (" << m_modelPath << ")." << endl;

	try {
		// Deserialize the ScriptModule from a file using torch::jit::load().
		m_module = torch::jit::load(m_modelPath);
		m_module.to(at::kCUDA);
		m_module.eval();
		cout << "Loading was sucessfull." << endl;
	}
	catch (const c10::Error& e) {
		cerr << "error loading the model: " << e.msg() << endl;
		std::exit(1);
	}
}


GpuMat* RGBDFaceAvatarGenerator::updateWithLandmarks(std::vector<FacialLandmark>& landmarks, cuda::Stream cudaStream) {
	m_currentCudaStream = cudaStream;
	m_outputRGBDImage.setTo(0, m_currentCudaStream);
	drawHeatmap(landmarks);

	//cudaProfilerStart();
	passThroughGenerator();
	calculateAndApplyDepthMasks();
	//cudaProfilerStop();

	if (m_netColorFormat == BGR)
		cuda::cvtColor(m_outputRGBDImage, m_outputRGBDImage, cv::COLOR_BGRA2RGBA, 0, m_currentCudaStream);

	if (m_visualisation) 
		visualize();

	m_currentCudaStream.waitForCompletion();
	return &m_outputRGBDImage;
}

GpuMat* RGBDFaceAvatarGenerator::updateWithRGBInput(Mat rgbInputImage, cuda::Stream cudaStream)
{
	m_currentCudaStream = cudaStream;

	//TODO-AM-DO - ist das so richtig? Ich muss oben aus der funktion updateWithLandmarks das noch runterholen
	//m_heatmap32 = rgbInputImage;
	m_outputRGBDImage = GpuMat(rgbInputImage);
	return &m_outputRGBDImage;
}

void RGBDFaceAvatarGenerator::drawHeatmap(std::vector<FacialLandmark>& landmarks)
{
	//clear heatmap image
	m_heatmap.setTo(-1);

	//draw heatmap
	for (int i = 1; i < landmarks.size(); i++)
	{
		if (i == 17 || i == 22 || i == 27 || i == 31)
			continue;

		m_prevLandmarkIndex = i - 1;
		if (i == 36)
			m_prevLandmarkIndex = 41;
		else if (i == 42)
			m_prevLandmarkIndex = 47;
		else if (i == 48)
			m_prevLandmarkIndex = 59;
		else if (i == 60)
			m_prevLandmarkIndex = 67;

		m_PointA.x = static_cast<int>(round(landmarks.at(m_prevLandmarkIndex).x));
		m_PointA.y = static_cast<int>(round(landmarks.at(m_prevLandmarkIndex).y));
		m_PointB.x = static_cast<int>(round(landmarks.at(i).x));
		m_PointB.y = static_cast<int>(round(landmarks.at(i).y));

		if (i == 68 || i == 69)
			circle(m_heatmap, m_PointB, static_cast<int>(m_heatmapLineThickness / 2), Scalar(1), m_heatmapLineThickness);
		else
			line(m_heatmap, m_PointA, m_PointB, Scalar(1), m_heatmapLineThickness);
	}
}

void RGBDFaceAvatarGenerator::passThroughGenerator()
{
	at::cuda::CUDAStream stream = at::cuda::getCurrentCUDAStream();
	AT_CUDA_CHECK(cudaStreamSynchronize(stream));

	m_imageTensor = torch::from_blob(m_heatmap.ptr<float>(), { 1, 1, m_heatmap.rows, m_heatmap.cols });
	m_imageTensor = m_imageTensor.pin_memory();
	
	std::vector<torch::jit::IValue> inputs;
	inputs.push_back(m_imageTensor.to(at::kCUDA, m_imageTensor.dtype(), true, false));

	try {
		m_outputTensor = m_module.forward(inputs).toTensor();
	}
	catch (std::runtime_error& e) {
		std::cerr << e.what() << std::endl;
	}

	m_outputTensor = m_outputTensor.squeeze().detach();
	m_outputTensor = m_outputTensor.add(1).mul(127.5).clamp(0, 255).to(torch::kU8);

	m_depthTensor = m_outputTensor[3];
	m_depthTensor = m_depthTensor.reshape({ m_imageSize, m_imageSize, 1 });

	cudaMemcpy((void*)m_depth.data, m_depthTensor.data_ptr(),
		sizeof(torch::kU8) * m_depthTensor.numel(), cudaMemcpyDefault);

	m_outputTensor = m_outputTensor.permute({ 1, 2, 0 });
	m_outputTensor = m_outputTensor.reshape({ 4, m_imageSize, m_imageSize });
	m_outputTensor = m_outputTensor.permute({ 1, 2, 0 });

	cudaMemcpy((void*)m_notMaskedRGBDImage.data, m_outputTensor.data_ptr(),
		sizeof(torch::kU8) * m_outputTensor.numel(), cudaMemcpyDefault);

}

void RGBDFaceAvatarGenerator::calculateAndApplyDepthMasks()
{
	//cudaProfilerStart();

	//calculate depth mask
	//mask 0
	cuda::threshold(m_depth, m_depthMask[0], 1, 255, THRESH_BINARY, m_currentCudaStream);
	m_erode5x5->apply(m_depthMask[0], m_depthMask[0], m_currentCudaStream);

	//merge masks
	for (int i = 1; i < numbersOfDepthMasks; i++) 
		cuda::bitwise_and(m_depthMask[0], m_depthMask[i], m_depthMask[0], noArray(), m_currentCudaStream);

	//apply masks
	m_notMaskedRGBDImage.copyTo(m_outputRGBDImage, m_depthMask[0], m_currentCudaStream);
}

void RGBDFaceAvatarGenerator::visualize()
{
	Mat cmpl;
	Mat rgb, depth, heatmap, mask;

	Mat rgbd[4];

	split(Mat(m_outputRGBDImage), rgbd);
	heatmap = (m_heatmap + 1) * 127.5;
	heatmap.convertTo(heatmap, CV_8UC1);
	cv::cvtColor(heatmap, heatmap, cv::COLOR_GRAY2BGR);

	vector<Mat> tempRGB, tempDepth, tempMask;
	tempRGB.push_back(rgbd[0]);
	tempRGB.push_back(rgbd[1]);
	tempRGB.push_back(rgbd[2]);
	merge(tempRGB, rgb);
	tempDepth.push_back(rgbd[3]);
	tempDepth.push_back(rgbd[3]);
	tempDepth.push_back(rgbd[3]);
	cv::merge(tempDepth, depth);
	if (numbersOfDepthMasks > 0) {
		tempMask.push_back(Mat(m_depthMask[0]));
		tempMask.push_back(Mat(m_depthMask[0]));
		tempMask.push_back(Mat(m_depthMask[0]));
		merge(tempMask, mask);
	}

	hconcat(heatmap, rgb, cmpl);
	hconcat(cmpl, depth, cmpl);
	if (numbersOfDepthMasks > 0)
		hconcat(cmpl, mask, cmpl);

	namedWindow("Output Image", WINDOW_AUTOSIZE);
	imshow("Output Image", cmpl);
	waitKey(1);
}

void RGBDFaceAvatarGenerator::saveErodedDiffImage(std::string savePath)
{
	time_t t = time(0);   // get time now
	struct tm* now = localtime(&t);
	std::stringstream dirSavePath;
	dirSavePath <<
		"erodedDiffDepth"
		<< now->tm_year + 1900 << "-"
		<< now->tm_mon + 1 << "-"
		<< now->tm_mday << "---"
		<< now->tm_hour << "-"
		<< now->tm_min << "-"
		<< now->tm_sec << "-"
		<< "erodedDiff.png";

	imwrite(dirSavePath.str(), Mat(m_outputRGBDImage));
}

