// ClayMore - Immersive Mesh Modelling --- Copyright (c) 2014-2017 Philipp Ladwig, Jannik Fiedler, Jan Beutgen
#include "TensorVisualizer.h"
#include <vector>
//#include <opencv2/opencv.hpp>
#include "../cmUtil/ConfigReader.h"
#include <iostream>
#include <opencv2/core/types.hpp>


TensorVisualizer::TensorVisualizer(ConfigReader* configReaderIn, osg::ref_ptr<osg::Group> tensorGroup)
{
	//Rene first GAN: m_depthScale = 0.0010f;

	m_depthScale = 0.0035f;
	//depthScale = 0.0010;
	m_xyScale = 0.0015f;
	m_useHMDPose = false;

	m_tensorVertices = new osg::Vec3Array; //Das ist eine VertexListe. Also die 3D Punkte des Meshes. Das ist unser VBO = VertexBufferObject (ist ein OpenGL Begriff)
	m_tensorIndicies = new osg::DrawElementsUInt(osg::PrimitiveSet::POINTS, 0); //Das sind die Indizies. Ein Dreieck hat z.B. drei Indizies. Ein Dreieck ist ein Face (oder auch Polygon genannt). Jedes Face besitzt eine Anzahl von Indizies (Dreiecke halt drei), mit denen es definiert ist. Ein Indizie beschreibt einen Vertex einmalig. So kann einem Face klare 3DPunkte im Raum zugewiesen werden
	m_tensorColorArray = new osg::Vec4Array; //Das ColorArray speichert f�r jeden Vertex eine Farbe in RGBA
	m_tensorColorArray->setBinding(osg::Array::BIND_PER_VERTEX);
	m_tensorNormals = new osg::Vec3Array;

	m_tensorGeometry = new osg::Geometry(); //Die Geometrie bekommt sp�ter das VertexArray, IndexArray und ColorArray und ist halt die zu rendernde Geometry
	m_tensorGeode = new osg::Geode(); //An die Geode kann man Geometrien "childen". An Geoden kann man Shader packen, die dann definieren, wie die angehangenden (gechildeten) Geometrien aussehen
	//m_tensorTexGeode = new osg::Geode();

	m_matTrans = new osg::MatrixTransform(); //Die Matrix, woran diese ganze Klasse h�ngt.
	m_matTrans->setName("MatTransTensorVisulizer");
	//Static position in scene of face
	if (!m_useHMDPose)
	{
		osg::Matrix matrixRot;
		matrixRot.makeRotate(osg::DegreesToRadians(180.0f), osg::Vec3f(0.0, 0.0, 1.0));
		osg::Matrix matrixTrans;
		//matrixTrans.makeTranslate(osg::Vec3f(-1.5, -1.6, 1.6));
		matrixTrans.makeTranslate(osg::Vec3f(0.0, -1.7, 0.0));
		m_matTrans->setMatrix(matrixTrans * matrixRot);
	}


	//osg::Matrix matrix;
	//matrix.makeRotate(osg::DegreesToRadians(180.0f), osg::Vec3f(0.0, 0.0, 1.0)); //Rotationsmatrix um 180Grad um positive Z Achse (Die PointCloud stand urspr�nglich auf dem Kopf)
	//m_matTrans->setMatrix(matrix);
	//m_PcGeode->setCullingActive(false);
	m_matTrans->addChild(m_tensorGeode);
	//m_matTrans->addChild(m_PcTexGeode);
	//m_geometryManager->addChild(m_matTrans);
	tensorGroup->addChild(m_matTrans);

	//For real sense depth frame data
	//In diesem Abschnitt wird ein PolyMode erstellt, der an die Geode gehangen wird. Anstatt hier ein Shader einzuh�ngen, sage ich manuell, 
	// dass die Geomtrie als einfach Punkte gerendert werden soll.
	osg::ref_ptr<osg::PolygonMode> polymode = new osg::PolygonMode;
	//##########################################################################################
	// BE AWARE OF THE UGLY NVIDIA QUADRO HACK!!!!
	//##########################################################################################
	polymode->setMode(osg::PolygonMode::FRONT, osg::PolygonMode::POINT);
	m_tensorGeode->getOrCreateStateSet()->setAttributeAndModes(polymode.get(), osg::StateAttribute::OVERRIDE | osg::StateAttribute::ON);
	m_pointSize = 1.0f; //TODO: Move into config
	m_tensorGeode->getOrCreateStateSet()->setAttributeAndModes(new osg::Point(m_pointSize), osg::StateAttribute::OVERRIDE | osg::StateAttribute::ON);
	m_tensorGeode->getOrCreateStateSet()->setMode(GL_LIGHTING, osg::StateAttribute::OFF);
	//m_Geode->getOrCreateStateSet()->setTextureMode(0, GL_TEXTURE_2D, osg::StateAttribute::OVERRIDE | osg::StateAttribute::OFF);

	//1. EXTREME PERFORMANCE BOOST: Use VBOs!
	m_tensorGeometry->setUseDisplayList(false);
	m_tensorGeometry->setUseVertexBufferObjects(true);
	m_tensorGeometry->setDataVariance(osg::Object::DYNAMIC);

	//2. EXTREME PERFORMANCE BOOST: Don't do this every frame. This is neseccary every frame if using displayList, but not for using VBO
	m_tensorGeometry->setVertexArray(m_tensorVertices);
	m_tensorGeometry->addPrimitiveSet(m_tensorIndicies);
	m_tensorGeometry->setColorArray(m_tensorColorArray);


	//Do we need Light? (from PointCloudVisulizer)
	//osg::ref_ptr<osg::Light> light = new osg::Light;
	////light->setLightNum(num);
	////light->setDiffuse(color);
	//light->setPosition(osg::Vec4(1.0f, 2.0f, 0.0f, 1.0f));
	//osg::ref_ptr<osg::LightSource> lightSource = new osg::LightSource;
	//lightSource->setLight(light);
	////light->setDirection(osg::Vec3f(0.0, -1.0, 0.0));
	//osg::ref_ptr<osg::MatrixTransform> sourceTrans = new osg::MatrixTransform;
	//sourceTrans->setMatrix(osg::Matrix::translate(osg::Vec3f(0.0f, 0.0f, 0.0f)));
	//sourceTrans->addChild(lightSource.get());
	//m_matTrans->addChild(sourceTrans);
	//m_matTrans->getOrCreateStateSet()->setMode(GL_LIGHT1, osg::StateAttribute::ON);
}

void TensorVisualizer::update(cv::Mat rgbdImage)
{
	//std::cout << "TensorVisualizer::update" << std::endl;
	clearPointCloud();
	visualizeTensorAsPointCloud(&rgbdImage);

	if (m_useHMDPose)
	{
		m_matTrans->setMatrix(m_hmdViewMatrix);
	}

}


void TensorVisualizer::visualizeTensorAsPointCloud(cv::Mat* rgbdImage)
{
	//Wenn in m_PcGeode keine Geometrie (auch Drawable genannt) gechildet ist...
	if (m_tensorGeode->getNumDrawables() == 0)
	{
		//... dann f�ge eine hinzu
		m_tensorGeode->addDrawable(m_tensorGeometry);
		//...und auch das "PrimitiveSet". Das PrimitiveSet ist die IndexListe, welche die Triangles (Faces) bzw. hier im Fall die Punkte visualisert
		m_tensorGeometry->addPrimitiveSet(m_tensorIndicies);
	}

	int count = m_tensorVertices->getNumElements();
	int k = 0;

	uint8_t* pixelPtr = rgbdImage->data;
	int cn = rgbdImage->channels();
	//std::cout << "channels: " << cn << std::endl;
	cv::Scalar_<uint8_t> bgrdPixel;
	for (int i = 0; i < rgbdImage->rows; i++)
	{
		for (int j = 0; j < rgbdImage->cols; j++)
		{
			bgrdPixel.val[0] = pixelPtr[i * rgbdImage->cols * cn + j * cn + 0]; // B
			bgrdPixel.val[1] = pixelPtr[i * rgbdImage->cols * cn + j * cn + 1]; // G
			bgrdPixel.val[2] = pixelPtr[i * rgbdImage->cols * cn + j * cn + 2]; // R
			bgrdPixel.val[3] = pixelPtr[i * rgbdImage->cols * cn + j * cn + 3]; // D



			if (bgrdPixel.val[3] > 0)
			{
				m_tensorIndicies->push_back(count + k);
				m_tensorVertices->push_back(osg::Vec3f(m_xyScale * (float)j, m_xyScale * (float)i, bgrdPixel.val[3] * m_depthScale));
				m_tensorColorArray->push_back(osg::Vec4f(bgrdPixel.val[2] / 256.0f, bgrdPixel.val[1] / 256.0f, bgrdPixel.val[0] / 256.0f, 1.0f));
				m_tensorNormals->push_back(osg::Vec3f(0.0f, 0.0f, 1.0f));
				k++;
			}

			//BGR?
			//std::cout << "i: " << i << std::endl;
			//std::cout << "j: " << j << std::endl;
			//std::cout << "float?: " << xyScale * (float)i << std::endl;
			//
			//std::cout << "bgrdPixel.val[0] " << bgrdPixel.val[0] / 256.0f << std::endl;
			//std::cout << "bgrdPixel.val[1] " << bgrdPixel.val[1] / 256.0f << std::endl;
			//std::cout << "bgrdPixel.val[2] " << bgrdPixel.val[2] / 256.0f << std::endl;
			//std::cout << "bgrdPixel.val[3] " << bgrdPixel.val[3] / 256.0f << std::endl;


		}
	}

	m_tensorGeometry->getPrimitiveSet(0)->dirty();
	m_tensorVertices->dirty(); //dirty hei�t, dass ein flag gesetzt wird, dass sich das Objekt nun ver�ndert hat, damit der neue Stand dargestellt wird
	m_tensorColorArray->dirty();
	m_tensorNormals->dirty();
	//m_tensorTexcoords->dirty();
	//m_imageStream->dirty();
	//m_texture->dirtyTextureParameters();
	//m_texture->dirtyTextureObject();
}

void TensorVisualizer::clearPointCloud()
{
	m_tensorVertices->clear();
	m_tensorIndicies->clear();
	m_tensorColorArray->clear();
	m_tensorNormals->clear();
	//m_tensorTexcoords->clear();


	m_tensorVertices->dirty();
	m_tensorIndicies->dirty();
	m_tensorColorArray->dirty();
	m_tensorNormals->dirty();
	//m_tensorTexcoords->dirty();
}

void TensorVisualizer::updateHMDPose(osg::Matrixd viewMatrix)
{
	if (m_useHMDPose)
	{
		osg::Matrix offset;
		offset.makeTranslate(osg::Vec3(0.0, 0.0, 0.0));
		m_hmdViewMatrix = offset * viewMatrix.inverse(viewMatrix) * osg::Matrix::inverse(offset);
	}
}