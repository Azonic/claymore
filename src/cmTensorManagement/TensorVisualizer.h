// ClayMore - Immersive Mesh Modelling --- Copyright (c) 2014-2017 Philipp Ladwig, Jannik Fiedler, Jan Beutgen
#pragma once

#include "ConfigDllExportTensorManagement.h"
#include "TensorVisualizer.h"
#include <opencv2/opencv.hpp>

#include <osg/Vec3d>
#include <osg/Geode>
#include <osg/Geometry>
#include <osg/MatrixTransform>
#include <osg/Point>
#include <osg/PolygonMode>
#include <osg/ImageStream>
#include <osg/Texture2D>
#include <osg/ShapeDrawable>

class ConfigReader;

class cmTensorManagement_DLL_import_export TensorVisualizer
{
	// #### CONSTRUCTOR & DESTRUCTOR ###############
public:
	TensorVisualizer(ConfigReader* configReaderIn, osg::ref_ptr<osg::Group> tensorGroup);
	~TensorVisualizer() {};


	// #### MEMBER VARIABLES ###############
private:
	osg::ref_ptr<osg::Group> m_tensorGroup;
	ConfigReader* m_configReader;

	osg::ref_ptr<osg::Geode> m_tensorGeode;
	//osg::ref_ptr<osg::Geode> m_tensorTexGeode;
	osg::ref_ptr<osg::Geometry> m_tensorGeometry;
	osg::ref_ptr<osg::Vec3Array> m_tensorVertices;
	osg::ref_ptr<osg::DrawElementsUInt> m_tensorIndicies;
	osg::ref_ptr<osg::Vec4Array> m_tensorColorArray;
	osg::ref_ptr<osg::Vec3Array> m_tensorNormals;
	//TEMP: Do I need them
	//osg::ref_ptr<osg::Vec2Array> m_tensorTexcoords;
	float m_pointSize;

	osg::ref_ptr<osg::MatrixTransform> m_matTrans;

	float m_depthScale;
	float m_xyScale;

	bool m_useHMDPose;
	osg::Matrixd m_hmdViewMatrix;

	// #### MEMBER FUNCTIONS ###############
private:
	void visualizeTensorAsPointCloud(cv::Mat* rgbdImage);
	void clearPointCloud();

public:
	void update(cv::Mat rgbdImage);
	void updateHMDPose(osg::Matrixd viewMatrix);
};
