// ClayMore - Immersive Mesh Modelling --- Copyright (c) 2014-2017 Philipp Ladwig, Jannik Fiedler, Jan Beutgen
#include "TensorManager.h"
#include <vector>
#include "TensorVisualizer.h"
#include "cmUtil/TimeWriter.h"
#include "../cmUtil/ConfigReader.h"


TensorManager::TensorManager(ConfigReader* configReaderIn, osg::ref_ptr<osg::Group> tensorGroup) : m_configReader(configReaderIn), m_tensorGroup(tensorGroup)
{
    m_tensorVisualizer = new TensorVisualizer(m_configReader, tensorGroup);
    std::vector<std::vector<int>> landmarks(70, std::vector<int>(2));

    m_ganInputType = static_cast<GANInputType>(m_configReader->getIntFromStartupConfig("gan_input_format"));
    if (m_ganInputType == LANDMARKS)
    {
        string fag_pathToModel = m_configReader->getStringFromStartupConfig("path_to_rgbd_face_avatar_gan_generator");
        int fag_imageSize = m_configReader->getIntFromStartupConfig("rgbd_face_avatar_gan_generator_output_pixel");
        NetColorFormat fag_netColorFormat = static_cast<NetColorFormat>(m_configReader->getIntFromStartupConfig("rgbd_face_avatar_gan_net_color_format"));
        int fag_heatmap_line_thickness = m_configReader->getIntFromStartupConfig("rgbd_face_avatar_gan_heatmap_line_thickness");
        
        m_fag = new RGBDFaceAvatarGenerator(fag_pathToModel, fag_imageSize, fag_netColorFormat, fag_heatmap_line_thickness, true);
    }
    else if (m_ganInputType == RGB)
    {
       /* m_fag = new RGBDFaceAvatarGenerator("data/tensors/tracedGenerator---nurRGB-ersterVersuch-Datensatz_knownSettings3.9.20",
            m_configReader->getIntFromStartupConfig("rgbd_face_avatar_gan_generator_output_pixel"),
            true);*/
    }
}

void TensorManager::updateSeventyFacialLandmarks(std::vector<FacialLandmark>& landmarks)
{
    //TimeWriter tw;

    cv::cuda::Stream cudaStream = cv::cuda::Stream();

    if (m_ganInputType == LANDMARKS)
    {
        //tw.startMetering("update(landmarks);", true, true);
        m_rgbdImage = m_fag->updateWithLandmarks(landmarks, cudaStream);
        //tw.endMetering();
    }
    else if (m_ganInputType == RGB)
    {
        //TODO-AM-DO Hier muss ich das webcam bild auslesen und an updateWithRGBInput �bergeben
        //m_rgbdImage = m_fag->updateWithRGBInput();
    }

    //tw.startMetering("m_tensorVisualizer->update()", true, true);
    //TODO adjust pipeline to use GpuMat only so that there is no data transfer between cpu and gpu
    m_tensorVisualizer->update(cv::Mat(*m_rgbdImage));
    //tw.endMetering();

    //HWM:
    //Die Mat muss ich visualisieren
    //Da steht jeder Kanal drin in unsigned char werten
    //Das k�nnte ich in eine Punktwolke schreiben
    //Diese muss ich aber vorher erstellen (abschauen, wie wir das um PointCloudManagement machen)
    //
}

void TensorManager::saveGeneratedRGBDImageToDisk(std::string savePath)
{
    if (m_fag != nullptr) {
        std::cout << "### ### Captured single generated image for IEEE AIVR paper ### ###" << std::endl;
        m_fag->saveErodedDiffImage("test");
    }
}

void TensorManager::updateHMDPose(osg::Matrixd viewMatrix)
{
    m_tensorVisualizer->updateHMDPose(viewMatrix);
}