#include <opencv2/opencv.hpp>
#include <opencv2/core/cuda.hpp>
#include <opencv2/cudaarithm.hpp>
#include <opencv2/cudafilters.hpp>
#include <opencv2/cudaimgproc.hpp>
#include <torch/script.h>
#include <vector>
#include <cuda_runtime.h> 
#include <math.h>
#include "cmUtil/Landmark.h"

using namespace cv;
using namespace cv::cuda;
using namespace std;

enum NetColorFormat {
	RGB,
	BGR
};

class RGBDFaceAvatarGenerator
{
public:
	RGBDFaceAvatarGenerator(string pathToModel, int imageSize = 512, NetColorFormat netColorFormat = RGB, 
		int heatmapLineThickness = 4, bool visualisation = false);
	GpuMat* updateWithLandmarks(std::vector<FacialLandmark>& landmarks, cuda::Stream cudaStream = cuda::Stream::Null());
	GpuMat* updateWithRGBInput(Mat rgbInputImage, cuda::Stream cudaStream = cuda::Stream::Null());
	void saveErodedDiffImage(std::string savePath);

private:
	void loadTorchModel();
	void drawHeatmap(std::vector<FacialLandmark>& landmarks);
	void passThroughGenerator();
	void calculateAndApplyDepthMasks();
	void visualize();

	string m_modelPath;
	torch::jit::script::Module m_module;
	int m_imageSize;
	NetColorFormat m_netColorFormat;
	bool m_visualisation;

	cuda::Stream m_currentCudaStream;

	Mat m_heatmap;
	int m_heatmapLineThickness;
	int m_prevLandmarkIndex;
	Point m_PointA, m_PointB;

	torch::Tensor m_imageTensor;
	torch::Tensor m_depthTensor;
	torch::Tensor m_outputTensor;

	GpuMat m_notMaskedRGBDImage;
	GpuMat m_depth;

	const static int numbersOfDepthMasks = 1;
	GpuMat m_depthMask[numbersOfDepthMasks];
	Ptr<cuda::Filter> m_erode5x5;

	GpuMat m_outputRGBDImage;
};