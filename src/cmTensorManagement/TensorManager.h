// ClayMore - Immersive Mesh Modelling --- Copyright (c) 2014-2017 Philipp Ladwig, Jannik Fiedler, Jan Beutgen
#pragma once

#include "ConfigDllExportTensorManagement.h"
#include "rgbdFaceAvatarGenerator.h"
#include "cmUtil/Landmark.h"
#include <vector>
#include <osg/Group>
#include <opencv2/opencv.hpp>
#include <opencv2/core/cuda.hpp>

class ConfigReader;
class TensorVisualizer;

using namespace cv::cuda;

class cmTensorManagement_DLL_import_export TensorManager
{


	// #### CONSTRUCTOR & DESTRUCTOR ###############
public:
	TensorManager(ConfigReader* configReaderIn, osg::ref_ptr<osg::Group> tensorGroup);
	~TensorManager(){};
	enum GANInputType
	{
		LANDMARKS,
		RGB
	};

	// #### MEMBER VARIABLES ###############
private:
	ConfigReader* m_configReader;
	GANInputType m_ganInputType;
	TensorVisualizer* m_tensorVisualizer;
	osg::ref_ptr<osg::Group> m_tensorGroup;
	GpuMat* m_rgbdImage;
	RGBDFaceAvatarGenerator* m_fag;

	// #### MEMBER FUNCTIONS ###############
public:
	void updateSeventyFacialLandmarks(std::vector<FacialLandmark>& landmarks);
	void saveGeneratedRGBDImageToDisk(std::string);
	void updateHMDPose(osg::Matrixd viewMatrix);
};