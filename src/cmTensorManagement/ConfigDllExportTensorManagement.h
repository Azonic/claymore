// ClayMore - Immersive Mesh Modelling --- Copyright (c) 2014-2017 Philipp Ladwig, Jannik Fiedler, Jan Beutgen
#ifndef cmTensorManagement_EXPORTS
	#define cmTensorManagement_DLL_import_export __declspec(dllimport)
#else
	#define cmTensorManagement_DLL_import_export __declspec(dllexport)
#endif