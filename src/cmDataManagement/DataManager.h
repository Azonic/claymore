// ClayMore - Immersive Mesh Modelling --- Copyright (c) 2014-2017 Philipp Ladwig, Jannik Fiedler, Jan Beutgen
#pragma once

//#include <fstream> //old stuff
//#include "Mesh.h"
#include "..\cmGeometryManagement\GeometryManager.h"
#include "..\cmOpenMeshBinding\OpenMeshGeometry.h"

#include "ConfigDllExportDataManagement.h"

class cmDataManagement_DLL_import_export DataManager
{
	// #### CONSTRUCTOR & DESTRUCTOR ###############
public:
	DataManager(osg::ref_ptr<GeometryManager> geometryManagerIn);
	~DataManager(void){};


	// #### MEMBER VARIABLES ###############
private:
	osg::ref_ptr<GeometryManager> m_geometryManager;


	// #### MEMBER FUNCTIONS ###############
public:
	void setGeometryManager(osg::ref_ptr<GeometryManager> geometryManagerIn);
	void loadData(std::string filename);
};