// ClayMore - Immersive Mesh Modelling --- Copyright (c) 2014-2017 Philipp Ladwig, Jannik Fiedler, Jan Beutgen
#include "DataManager.h"
#include <OpenMesh/Core/IO/MeshIO.hh>

#include "../cmGeometryManagement/GeometryData.h"
DataManager::DataManager(osg::ref_ptr<GeometryManager> geometryManagerIn) : m_geometryManager(geometryManagerIn)
{

}

void DataManager::setGeometryManager(osg::ref_ptr<GeometryManager> geometryManagerIn)
{
	m_geometryManager = geometryManagerIn;
}

//TODO: Move this to Import Processor, cause its some kind of processing
void DataManager::loadData(std::string filename)
{
	//EXP: Create a GeometryData instance and assign VertexArray and a PrimitiveSet. Is automatically the active geometry now.
	m_geometryManager->addGeometry(osg::Vec3f(0.0f, 0.0f, 0.0f)); //TODO: Position never used in addGeometryFunction()
	
	m_geometryManager->getActiveGeometryData()->getOpenMeshGeo()->setVertexArray(new osg::Vec3Array());
	m_geometryManager->getActiveGeometryData()->getOpenMeshGeo()->addPrimitiveSet(new osg::DrawElementsUInt(osg::PrimitiveSet::TRIANGLES, 0));

	osg::ref_ptr<osg::Vec4Array> colorArray = new osg::Vec4Array();
	for (unsigned int i = 0; i < m_geometryManager->getActiveGeometryData()->getOpenMeshGeo()->getVertexArray()->getNumElements(); i++)
	{
		colorArray->push_back(osg::Vec4(0.0f, 0.0f, 0.0f, 1.0f));
	}
	m_geometryManager->getActiveGeometryData()->getOpenMeshGeo()->setColorArray(colorArray);
	m_geometryManager->getActiveGeometryData()->getOpenMeshGeo()->setColorBinding(osg::Geometry::BIND_PER_VERTEX);

	//EXP: Call initializeAndDisplay() not before you've assign a VertexArray and a PrimitiveSet.
	//std::cout << "m_geometryManager->getNameOfActiveGeometry())->getOpenMeshGeo()->getVertexArray()->getNumElements(): " << 
	//	m_geometryManager->getActiveGeometryData()->getOpenMeshGeo()->getVertexArray()->getNumElements() << std::endl;
	//m_geometryManager->getActiveGeometryData()->initializeAndDisplay();
	m_geometryManager->readFileFromDisk(filename);
	m_geometryManager->getActiveGeometryData()->initializeAndDisplay();
}