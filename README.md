# ClayMore Engine

ClayMore is rudimentary Render Engine written in C++. It started 2015 as a fun and spare time project by three computer scientist and has been growing for more than 3 years.

It has full automatic build scripts based on CMake and uses following dependencies:
- OpenSceneGraph - http://www.openscenegraph.org/
- Boost - https://www.boost.org/
- QT 5 - https://www.qt.io/
- PCL 1.9 - http://pointclouds.org/
- OpenMesh - https://www.openmesh.org/
- OpenVR - https://github.com/ValveSoftware/openvr
- VLC SDK - https://www.videolan.org/developers/vlc.html
- Intel RealSense SDK - https://github.com/IntelRealSense/librealsense
- LeapMotion SDK - https://www.leapmotion.com/
- RakNet - https://github.com/facebookarchive/RakNet

Projects such as the following were made with ClayMore:
- https://mirevi.de/projects/claymore
- https://mirevi.de/projects/collaborative-mesh-modeling

Full Paper:
- https://www.researchgate.net/publication/322926204_Towards_Precise_Fast_and_Comfortable_Immersive_Polygon_Mesh_Modelling_Capitalising_the_Results_of_Past_Research_and_Analysing_the_Needs_of_Professionals
- https://www.researchgate.net/publication/324452471_A_Literature_Review_on_Collaboration_in_Mixed_Reality

# How to build
Currently ClayMore is only available for Win10 x64, because OpenVR has still the best support in this OS.

You Need (install in this order):
- Git (default settings during installation are fine)
- TortoiseGit (If you are not fimilar with the GIT bash)
- Visual Studio 2019 Community with "Desktop Development with C++"
- CMake (at least 3.13)
- CUDA 10 (works with 10.2 - never tested with 11)

Building process:
- First clone ClayMore (for example in your local directory "C:/devel/claymore") Make sure you have around 15GB free disk, since ClayMore needs to download a lot of dependencies (2.8GB compressed packages)
- Start Cmake and put in text field "Where is the source code": "C:/devel/claymore" . Note that CMake and the used libraries are senstive to special character (such as ö, ä or ü) and blank spaces. Avoid such characters in the pathes, otherwise the build may fail.
- ...and in "Where to build the binaries": "C:/devel/claymore/build"
- Hit the button "Configure". A window appears. Set "Visual Studio 2019" and "x64" in the drop down menus and grab a coffee. Cmake will download all the dependencies (~5GB), extract it and set it up for you
- If the internet connection is poor, CMake may throw an error. If so, hit "Configure" again (and grab another coffee)
- After successfull configuring, hit "Generate" and "Open Project". ClayMore will be opened in Visual Studio 2017
- Set the "Solution Configuration" from "Debug" to "RelWithDebugInfos" (it is in the upper area, near the green triangle ;)) This will build ClayMore and will take another 20minutes. Coffee?
- If everything runs fine, you have build ClayMore. Before you start ClayMore (with the button "local Windows Debugger" in Visual Studio): There is a configuration file in C:/devel/claymore/bin/data/ with the name "minimal_config.xml". Copy it (in the same folder) and rename it to "config.xml". This file enabling/disabling input and output devices such as RealSense, LeapMotion, Vive etc.
- Optional: Convenience tools can be downloaded from: https://claymore.mirevi.com/ClayMoreConfigAndRemoteControlToolsv1.0.zip Unzip it in "C:/devel/claymore/bin/". The ClayMoreConfigTool let you change the config file in a more comfortable manner instead of editing the config.xml manually.


Question? Write me: https://mirevi.de/team/philipp-ladwig

# Acknowledgements:
This project is sponsored by: German Federal Ministry of Education and Research (BMBF) under the project number 13FH022IX6. Project name: Interactive body-near production technology 4.0 (German: Interaktive körpernahe Produktionstechnik 4.0 (iKPT4.0))


![](bmbflogo.jpg) ![](fafh.jpg)

