//############################################################################################
// ClayMoreSpeech - Copyright (C) 2016 Philipp Ladwig
//############################################################################################
#pragma once

#include <Windows.h>
#include <sphelper.h>

class NetworkManager;
class QCoreApplication;

class SpeechProcessor
{

public:
	// #### CONSTRUCTOR & DESTRUCTOR ###############
	SpeechProcessor(QCoreApplication* qtCoreApp, NetworkManager* networkManager);
	~SpeechProcessor();


	// #### MEMBER VARIABLES ###############
private:
	QCoreApplication* m_qtCoreApp;
	NetworkManager* m_networkManager;
	LPWSTR pwszText;
	HANDLE hEvent;
	HRESULT hr = S_OK;
	CSpEvent evt;
	SPPHRASE *pParts;
	ISpPhrase *pPhrase;
	ULONGLONG ullEvents;
	CComPtr<ISpRecognizer> cpEngine;
	CComPtr<ISpRecoContext> cpRecoCtx;
	CComPtr<ISpRecoGrammar> cpGram;
	bool done = false;

	// #### MEMBER FUNCTIONS ###############
public:
	unsigned int SpeechProcessor::runRecognition();
};


