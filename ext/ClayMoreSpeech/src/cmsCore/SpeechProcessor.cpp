//############################################################################################
// ClayMoreSpeech - Copyright (C) 2016 Philipp Ladwig
//############################################################################################

#include "SpeechProcessor.h"

#include "imNetworkManagement/NetworkManager.h"
#include "imNetworkManagement/SpeechPacket.h"
#include <QtCore/qcoreapplication.h>


#include <iostream>

#include <atlstr.h>

SpeechProcessor::SpeechProcessor(QCoreApplication* qtCoreApp, NetworkManager* networkManager) : m_qtCoreApp(qtCoreApp), m_networkManager(networkManager)
{
	done = false;
}


SpeechProcessor::~SpeechProcessor()
{

}


unsigned int SpeechProcessor::runRecognition()
{
	//SetConsoleCtrlHandler(HandlerRoutine, TRUE);

	std::wcout << "Speak phrases defined in the grammar being tested" << std::endl;
	std::wcout << "Press strg/c to quit" << std::endl;

	CoInitialize(0);

	hr = cpEngine.CoCreateInstance(CLSID_SpInprocRecognizer);
	if (FAILED(hr)) {
		std::wcout << "Could not create engine instance." << std::endl;
		return 0;
	}

	hr = cpEngine->CreateRecoContext(&cpRecoCtx);
	if (FAILED(hr)) {
		std::wcout << "Could not create recognition context." << std::endl;
		return 0;
	}

	//Define, which window will receive the notifications
	hr = cpRecoCtx->SetNotifyWin32Event();
	if (FAILED(hr)) {
		std::wcout << "Could not set context handler." << std::endl;
		return 0;
	}

	cpRecoCtx->Pause(NULL);//Do not listen during initialization

						   //InputAudio Start
	CComPtr<ISpObjectToken> pAudioToken;
	CComPtr<ISpAudio> pAudio;
	hr = SpGetDefaultTokenFromCategoryId(SPCAT_AUDIOIN, &pAudioToken);// Use the default input
	if (FAILED(hr)) {
		std::wcout << "Could not set default token for Audio in." << std::endl;
		return 0;
	}
	hr = cpEngine->SetInput(pAudioToken, TRUE);
	if (FAILED(hr)) {
		std::wcout << "Could not set input channel for audio token (could not connect to device)." << std::endl;
		return 0;
	}
	hr = SpCreateDefaultObjectFromCategoryId(SPCAT_AUDIOIN, &pAudio);
	if (FAILED(hr)) {
		std::wcout << "Could not create default input channel." << std::endl;
		return 0;
	}
	hr = cpEngine->SetInput(pAudio, TRUE);
	if (FAILED(hr)) {
		std::wcout << "Could not set input channel for audio channel." << std::endl;
		return 0;
	}
	//InputAudio Ends

	hEvent = cpRecoCtx->GetNotifyEventHandle();

	ullEvents = SPFEI(SPEI_RECOGNITION) | SPFEI(SPEI_FALSE_RECOGNITION);
	hr = cpRecoCtx->SetInterest(ullEvents, ullEvents);
	if (FAILED(hr)) {
		std::wcout << "Could not set interests." << std::endl;
		return 0;
	}


	hr = cpRecoCtx->CreateGrammar(NULL, &cpGram);
	if (FAILED(hr)) {
		std::wcout << "Could not set grammar." << std::endl;
		return 0;
	}

	hr = cpGram->LoadCmdFromFile(L"Grammar.grxml", SPLO_STATIC);
	if (FAILED(hr)) {
		std::wcout << "Could not read file name." << std::endl;
		return 0;
	}

	hr = cpGram->SetRuleState(0, 0, SPRS_ACTIVE);
	if (FAILED(hr)) {
		std::wcout << "Could not set rule states as 'active'." << std::endl;
		return 0;
	}

	fprintf(stdout, "Speech recognizer is listening...^\n");
	cpRecoCtx->Resume(NULL);
	//Unless stop, i.e. 'listen(false)' was called
	while (!done) {
		//While could successfully listen to context
		while (evt.GetFrom(cpRecoCtx) == S_OK) {
			//Context recognition event
			cpRecoCtx->Pause(NULL); //Stop listening while analyzing
			if (evt.eEventId == SPEI_FALSE_RECOGNITION) {
				//Not understood phrase
				std::wcout << "No recognition" << std::endl;

				SpeechPacket* testPacket = new SpeechPacket("NO_RECOG", 0.0, 0.0, 0.0, 0.0, 0.0);
				//Send Paket
				m_networkManager->sendPacketOnAllConnectedSockets(testPacket);
			}
			else {
				//Understood phrase: store recognition result in variable
				pPhrase = evt.RecoResult();
				hr = pPhrase->GetPhrase(&pParts);

				hr = pPhrase->GetText(SP_GETWHOLEPHRASE, SP_GETWHOLEPHRASE, FALSE, &pwszText, 0);

				const SPPHRASEPROPERTY* pSemanticTag = pParts->pProperties->pFirstChild;
				const float confidence = pSemanticTag->SREngineConfidence;

				std::wcout << pwszText << " " << pSemanticTag->pszValue << " (" << confidence << ") " << std::endl;
				std::string command = CW2A(pSemanticTag->pszValue);

				//Create Packet
				SpeechPacket* testPacket = new SpeechPacket(command, 1.1, 2.22, 3.333, 4.4444, 5.55555);
				//Send Paket
				m_networkManager->sendPacketOnAllConnectedSockets(testPacket);
				//m_qtCoreApp->processEvents();

				CoTaskMemFree(pParts);
				CoTaskMemFree(pwszText);
			}
			cpRecoCtx->Resume(NULL); //Restart listening after analysis
		}
	}
}