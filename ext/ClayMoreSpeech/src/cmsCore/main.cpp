//############################################################################################
// ClayMoreSpeech - Copyright (C) 2016 Philipp Ladwig
//############################################################################################

//############################################################################################
//Terms used in this soruce:
//FIXME: means, that something can be written in better code and should changed in future
//TODO: means, that there are more to do but it is not compelling
//EXP: stands for Explanation. It explains code
//OBSCURE: stands for obscure behaviour of the programm and the reason is not known
//############################################################################################

#include "SpeechProcessor.h"
#include "imNetworkManagement/NetworkManager.h"
#include <QtCore\qcoreapplication.h>

#include <iostream>

//static const char* IP = "127.0.0.1";
static const char* IP = "169.254.113.185";
static int PORT = 7005;

int main(int argc, char** argv)
{
	QCoreApplication* qtCoreApp = new QCoreApplication(argc, argv);
	NetworkManager* networkManager = new NetworkManager();
	networkManager->createAndConnectTCPClient(IP, PORT);
	
	SpeechProcessor* speechProcessor = new SpeechProcessor(qtCoreApp, networkManager);
	speechProcessor->runRecognition();

	return (0);
}