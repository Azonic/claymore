#pragma once

#include <QtWidgets/QMainWindow>
#include "ui_ClayMoreGui.h"

//NOTE for Build:
//In CMakeLists.txt is a copy command for the Qt5 Binaries into the claymore/build/bin dir

class NetworkManagerOSC;
class ConfigReader;

class ClayMoreGui : public QMainWindow
{
	Q_OBJECT

public:
	ClayMoreGui(QWidget *parent = Q_NULLPTR);
	~ClayMoreGui();

private slots:
	//-------Button events----------//
	void buttonClickedConnect();
	void buttonClicked_sendCommand();
	void buttonClicked_activateScanContextPointCloud();
	void buttonClicked_activateScanContextMarchingCubes();
	void buttonClicked_activateScanContextOctree();
	
	void buttonClicked_togglePointCloudDisplay();
	void buttonClicked_toggleOctreeDisplay();
	void buttonClicked_toggleDDADisplay();
	void buttonClicked_toggleTSDFDisplay();
	void buttonClicked_toggleMarchingCubesDisplay();
	void buttonClicked_toggleHelpForScanContext();

	void buttonClicked_drawOctreeVolume();
	void buttonClicked_drawTSDF();


	void buttonClicked_clearOctreeVolumeDisplay();
	void buttonClicked_clearTSDFDisplay();
	void buttonClicked_clearDDADisplay();
	void buttonClicked_clearMarchingCubes();
	void buttonClicked_clearPointCloud();

	void buttonClicked_openDartMenuForScanPointCloudContext();
	void buttonClicked_openDartMenuForScanOctreeContext();
	void buttonClicked_openDartMenuForScanMarchingCubesContext();
	void buttonClicked_openFenceMenuForScanGeneralCommands();

	void buttonClicked_polygonizePointCloud();
	

private:
	Ui::ClayMoreGuiClass ui;
	NetworkManagerOSC* m_networkManagerOSC;
	ConfigReader* m_configReader;
	void connectButtons();	
};
