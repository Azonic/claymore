#   CmakeLists for ClayMoreRemoteGUI

# Find includes in corresponding build directories
set(CMAKE_INCLUDE_CURRENT_DIR ON)
# Instruct CMake to run moc automatically when needed.
set(CMAKE_AUTOMOC ON)
# Instructs Cmake/VisualS Studio to add a extra folder for the moc files
#set(AUTOGEN_TARGETS_FOLDER ON)
set(CMAKE_AUTORCC ON)
set(CMAKE_AUTOUIC ON)

include_directories(${CMAKE_BINARY_DIR}/src ${Qt5ForClayMoreRemoteGUI_INCLUDE_DIR})
link_directories(${CMAKE_BINARY_DIR}/lib "${CMAKE_BINARY_DIR}/lib" ${Qt5ForClayMoreRemoteGUI_LIBRARIES_DIR})

include_directories(${CMAKE_BINARY_DIR}/src ${CLAYMORE_NETWORK_MANAGEMENT_OSC_INCLUDE_DIR})
link_directories(${CMAKE_BINARY_DIR}/lib "${CMAKE_BINARY_DIR}/lib" ${CLAYMORE_NETWORK_MANAGEMENT_OSC_LIBRARIES_DIR})

include_directories(${CMAKE_BINARY_DIR}/src ${OSG_INCLUDE_DIR})
link_directories(${CMAKE_BINARY_DIR}/lib "${CMAKE_BINARY_DIR}/lib" ${OSG_LIBRARIES_DIR})

include_directories(${CMAKE_BINARY_DIR}/src ${CLAYMORE_UTIL_INCLUDE_DIR})
link_directories(${CMAKE_BINARY_DIR}/lib "${CMAKE_BINARY_DIR}/lib" ${CLAYMORE_UTIL_LIBRARIES_DIR})

include_directories(${CMAKE_BINARY_DIR}/src ${VLC_SDK_INCLUDE_DIR})
link_directories(${CMAKE_BINARY_DIR}/lib "${CMAKE_BINARY_DIR}/lib" ${VLC_SDK_LIBRARIES_DIR})
 
 
file(GLOB_RECURSE HEADER "${CMAKE_CURRENT_SRC_DIR}" *.h)
file(GLOB_RECURSE SRC "${CMAKE_CURRENT_SRC_DIR}" *.cpp)
file(GLOB_RECURSE QRC "${CMAKE_CURRENT_SRC_DIR}" *.qrc)
file(GLOB_RECURSE UI "${CMAKE_CURRENT_SRC_DIR}" *.ui)
add_executable(ClayMoreRemoteGUI ${HEADER} ${SRC} ${QRC} ${UI})

 
target_link_libraries(ClayMoreRemoteGUI
	Qt5::Widgets
	debug Qt5Xml${CMAKE_DEBUG_POSTFIX} optimized Qt5Xml
	
	
	debug cmNetworkManagementOSC${CMAKE_DEBUG_POSTFIX} optimized cmNetworkManagementOSC
	
	debug osg${CMAKE_DEBUG_POSTFIX} optimized osg
	debug osgDB${CMAKE_DEBUG_POSTFIX} optimized osgDB
	debug osgGA${CMAKE_DEBUG_POSTFIX} optimized osgGA
	debug osgFX${CMAKE_DEBUG_POSTFIX} optimized osgFX
	debug osgText${CMAKE_DEBUG_POSTFIX} optimized osgText
	debug osgUtil${CMAKE_DEBUG_POSTFIX} optimized osgUtil
	debug osgViewer${CMAKE_DEBUG_POSTFIX} optimized osgViewer
	debug osgShadow${CMAKE_DEBUG_POSTFIX} optimized osgShadow
	debug OpenThreads${CMAKE_DEBUG_POSTFIX} optimized OpenThreads
	
	debug cmUtil${CMAKE_DEBUG_POSTFIX} optimized cmUtil
	debug libvlccore optimized libvlccore
	debug libvlc optimized libvlc
)

set_target_properties(ClayMoreRemoteGUI PROPERTIES VS_DEBUGGER_WORKING_DIRECTORY "${CMAKE_BINARY_DIR}/../../../build/bin")
set_property(DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR} PROPERTY VS_STARTUP_PROJECT ClayMoreRemoteGUI)
set(CMAKE_BUILD_TYPE "RelWithDebInfo" CACHE STRING "Build type (default RelWithDepInfo)" FORCE)