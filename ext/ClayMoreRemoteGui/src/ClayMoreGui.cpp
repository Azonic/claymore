#include "ClayMoreGui.h"

#include <iostream>

#include <NetworkManagerOSC.h>
#include "../cmUtil/ConfigReader.h"
#include "defines.h"
#include <osc/OscReceivedElements.h>
#include <osc/OscPacketListener.h>
#include <osc/OscOutboundPacketStream.h>
#include <ip/UdpSocket.h>

#include <thread>

ClayMoreGui::ClayMoreGui(QWidget* parent) : QMainWindow(parent)
{
	ui.setupUi(this);
	connectButtons();
	g_debugOutput = true;
	m_configReader = new ConfigReader();
	if (!m_configReader->readConfigFile(CONFIG_FILE_PATH))
	{
		system("pause");
		return;
	}
}

ClayMoreGui::~ClayMoreGui()
{
}

void ClayMoreGui::buttonClickedConnect()
{
	std::cout << "buttonClickedConnect ...." << std::endl;
	m_networkManagerOSC = new NetworkManagerOSC(m_configReader);
	m_networkManagerOSC->activateStatusUpdateListener();
	std::thread thread0(&NetworkManagerOSC::run, m_networkManagerOSC);
	thread0.detach();
}

void ClayMoreGui::buttonClicked_sendCommand()
{
	if (g_debugOutput) {
		std::cout << "send openFenceMenuForScanGeneralCommands" << std::endl;
	}
	m_networkManagerOSC->sendControlCommand("openFenceMenuForScanGeneralCommands", "0.0");
	//m_networkManagerOSC->sendControlCommand("emptyCallback", "0.0");
	//m_networkManagerOSC->sendControlCommand("emptyCallback", "0.0");
}

void ClayMoreGui::buttonClicked_activateScanContextPointCloud()
{
	if (g_debugOutput) {
		std::cout << "send activateScanContextPointCloud" << std::endl;
	}
	m_networkManagerOSC->sendControlCommand("activateScanContextPointCloud", "0.0");
}

void ClayMoreGui::buttonClicked_activateScanContextMarchingCubes()
{
	if (g_debugOutput) {
		std::cout << "send activateScanContextMarchingCubes" << std::endl;
	}
	m_networkManagerOSC->sendControlCommand("activateScanContextMarchingCubes", "0.0");
}

void ClayMoreGui::buttonClicked_activateScanContextOctree()
{
	if (g_debugOutput) {
		std::cout << "send activateScanContextOctree" << std::endl;
	}
	m_networkManagerOSC->sendControlCommand("activateScanContextOctree", "0.0");
}



void ClayMoreGui::buttonClicked_togglePointCloudDisplay()
{
	if (g_debugOutput) {
		std::cout << "send togglePointCloudDisplay" << std::endl;
	}
	m_networkManagerOSC->sendControlCommand("togglePointCloudDisplay", "0.0");
}

void ClayMoreGui::buttonClicked_toggleOctreeDisplay()
{
	if (g_debugOutput) {
		std::cout << "send toggleOctreeDisplay" << std::endl;
	}
	m_networkManagerOSC->sendControlCommand("toggleOctreeDisplay", "0.0");
}

void ClayMoreGui::buttonClicked_toggleDDADisplay()
{
	if (g_debugOutput) {
		std::cout << "send toggleDDADisplay" << std::endl;
	}
	m_networkManagerOSC->sendControlCommand("toggleDDADisplay", "0.0");
}

void ClayMoreGui::buttonClicked_toggleTSDFDisplay()
{
	if (g_debugOutput) {
		std::cout << "send toggleTSDFDisplay" << std::endl;
	}
	m_networkManagerOSC->sendControlCommand("toggleTSDFDisplay", "0.0");
}

void ClayMoreGui::buttonClicked_toggleMarchingCubesDisplay()
{
	if (g_debugOutput) {
		std::cout << "send toggleMarchingCubesDisplay" << std::endl;
	}
	m_networkManagerOSC->sendControlCommand("toggleMarchingCubesDisplay", "0.0");
}

void ClayMoreGui::buttonClicked_toggleHelpForScanContext()
{
	if (g_debugOutput) {
		std::cout << "send toggleHelpForScanContext" << std::endl;
	}
	m_networkManagerOSC->sendControlCommand("toggleHelpForScanContext", "0.0");
}



void ClayMoreGui::buttonClicked_drawOctreeVolume()
{
	if (g_debugOutput) {
		std::cout << "send drawOctreeVolume" << std::endl;
	}
	m_networkManagerOSC->sendControlCommand("drawOctreeVolume", "0.0");
}

void ClayMoreGui::buttonClicked_drawTSDF()
{
	if (g_debugOutput) {
		std::cout << "send drawTSDF" << std::endl;
	}
	m_networkManagerOSC->sendControlCommand("drawTSDF", "0.0");
}



void ClayMoreGui::buttonClicked_clearOctreeVolumeDisplay()
{
	if (g_debugOutput) {
		std::cout << "send clearOctreeVolumeDisplay" << std::endl;
	}
	m_networkManagerOSC->sendControlCommand("clearOctreeVolumeDisplay", "0.0");
}

void ClayMoreGui::buttonClicked_clearTSDFDisplay()
{
	if (g_debugOutput) {
		std::cout << "send clearTSDFDisplay" << std::endl;
	}
	m_networkManagerOSC->sendControlCommand("clearTSDFDisplay", "0.0");
}

void ClayMoreGui::buttonClicked_clearDDADisplay()
{
	if (g_debugOutput) {
		std::cout << "send clearDDADisplay" << std::endl;
	}
	m_networkManagerOSC->sendControlCommand("clearDDADisplay", "0.0");
}

void ClayMoreGui::buttonClicked_clearMarchingCubes()
{
	if (g_debugOutput) {
		std::cout << "send clearMarchingCubes" << std::endl;
	}
	m_networkManagerOSC->sendControlCommand("clearMarchingCubes", "0.0");
}

void ClayMoreGui::buttonClicked_clearPointCloud()
{
	if (g_debugOutput) {
		std::cout << "send clearPointCloud" << std::endl;
	}
	m_networkManagerOSC->sendControlCommand("clearPointCloud", "0.0");
}


void ClayMoreGui::buttonClicked_openDartMenuForScanPointCloudContext()
{
	if (g_debugOutput) {
		std::cout << "send openDartMenuForScanPointCloudContext" << std::endl;
	}
	m_networkManagerOSC->sendControlCommand("openDartMenuForScanPointCloudContext", "0.0");
}

void ClayMoreGui::buttonClicked_openDartMenuForScanOctreeContext()
{
	if (g_debugOutput) {
		std::cout << "send openDartMenuForScanOctreeContext" << std::endl;
	}
	m_networkManagerOSC->sendControlCommand("openDartMenuForScanOctreeContext", "0.0");
}

void ClayMoreGui::buttonClicked_openDartMenuForScanMarchingCubesContext()
{
	if (g_debugOutput) {
		std::cout << "send openDartMenuForScanMarchingCubesContext" << std::endl;
	}
	m_networkManagerOSC->sendControlCommand("openDartMenuForScanMarchingCubesContext", "0.0");
}

void ClayMoreGui::buttonClicked_openFenceMenuForScanGeneralCommands()
{
	if (g_debugOutput) {
		std::cout << "send openFenceMenuForScanGeneralCommands" << std::endl;
	}
	m_networkManagerOSC->sendControlCommand("openFenceMenuForScanGeneralCommands", "0.0");
}


void ClayMoreGui::buttonClicked_polygonizePointCloud()
{
	if (g_debugOutput) {
		std::cout << "send polygonizePointCloud" << std::endl;
	}
	m_networkManagerOSC->sendControlCommand("polygonizePointCloud", "0.0");
}



//connect the inputs to the functions
void ClayMoreGui::connectButtons()
{
	//for debug
	std::cout << "Connecting buttons! ...." << std::endl;
	QObject::connect(ui.pushButton_connect, SIGNAL(clicked()), this, SLOT(buttonClickedConnect()));
	QObject::connect(ui.pushButton_send_command, SIGNAL(clicked()), this, SLOT(buttonClicked_sendCommand()));
	QObject::connect(ui.pushButton_activateScanContextPointCloud, SIGNAL(clicked()), this, SLOT(buttonClicked_activateScanContextPointCloud()));
	QObject::connect(ui.pushButton_activateScanContextMarchingCubes, SIGNAL(clicked()), this, SLOT(buttonClicked_activateScanContextMarchingCubes()));
	QObject::connect(ui.pushButton_activateScanContextOctree, SIGNAL(clicked()), this, SLOT(buttonClicked_activateScanContextOctree()));
	
	QObject::connect(ui.pushButton_togglePointCloudDisplay, SIGNAL(clicked()), this, SLOT(buttonClicked_togglePointCloudDisplay()));
	QObject::connect(ui.pushButton_toggleOctreeDisplay, SIGNAL(clicked()), this, SLOT(buttonClicked_toggleOctreeDisplay()));
	QObject::connect(ui.pushButton_toggleDDADisplay, SIGNAL(clicked()), this, SLOT(buttonClicked_toggleDDADisplay()));
	QObject::connect(ui.pushButton_toggleTSDFDisplay, SIGNAL(clicked()), this, SLOT(buttonClicked_toggleTSDFDisplay()));
	QObject::connect(ui.pushButton_toggleMarchingCubesDisplay, SIGNAL(clicked()), this, SLOT(buttonClicked_toggleMarchingCubesDisplay()));
	QObject::connect(ui.pushButton_toggleHelpForScanContext, SIGNAL(clicked()), this, SLOT(buttonClicked_toggleHelpForScanContext()));
	
	QObject::connect(ui.pushButton_drawOctreeVolume, SIGNAL(clicked()), this, SLOT(buttonClicked_drawOctreeVolume()));
	QObject::connect(ui.pushButton_drawTSDF, SIGNAL(clicked()), this, SLOT(buttonClicked_drawTSDF()));


	QObject::connect(ui.pushButton_clearOctreeVolumeDisplay, SIGNAL(clicked()), this, SLOT(buttonClicked_clearOctreeVolumeDisplay()));
	QObject::connect(ui.pushButton_clearTSDFDisplay, SIGNAL(clicked()), this, SLOT(buttonClicked_clearTSDFDisplay()));
	QObject::connect(ui.pushButton_clearDDADisplay, SIGNAL(clicked()), this, SLOT(buttonClicked_clearDDADisplay()));
	QObject::connect(ui.pushButton_clearMarchingCubes, SIGNAL(clicked()), this, SLOT(buttonClicked_clearMarchingCubes()));
	QObject::connect(ui.pushButton_clearPointCloud, SIGNAL(clicked()), this, SLOT(buttonClicked_clearPointCloud()));

	QObject::connect(ui.pushButton_openDartMenuForScanPointCloudContext, SIGNAL(clicked()), this, SLOT(buttonClicked_openDartMenuForScanPointCloudContext()));
	QObject::connect(ui.pushButton_openDartMenuForScanOctreeContext, SIGNAL(clicked()), this, SLOT(buttonClicked_openDartMenuForScanOctreeContext()));
	QObject::connect(ui.pushButton_openDartMenuForScanMarchingCubesContext, SIGNAL(clicked()), this, SLOT(buttonClicked_openDartMenuForScanMarchingCubesContext()));
	QObject::connect(ui.pushButton_openFenceMenuForScanGeneralCommands, SIGNAL(clicked()), this, SLOT(buttonClicked_openFenceMenuForScanGeneralCommands()));
	
	QObject::connect(ui.pushButton_polygonizePointCloud, SIGNAL(clicked()), this, SLOT(buttonClicked_polygonizePointCloud()));



	
	//QObject::connect(ui.pushButton_emptyCallback, SIGNAL(clicked()), this, SLOT(buttonClicked_EmptyCallback()));
	//QObject::connect(ui.pushButton_emptyCallback, SIGNAL(clicked()), this, SLOT(buttonClicked_EmptyCallback()));
	//QObject::connect(ui.pushButton_emptyCallback, SIGNAL(clicked()), this, SLOT(buttonClicked_EmptyCallback()));
	//QObject::connect(ui.pushButton_emptyCallback, SIGNAL(clicked()), this, SLOT(buttonClicked_EmptyCallback()));
	//QObject::connect(ui.pushButton_emptyCallback, SIGNAL(clicked()), this, SLOT(buttonClicked_EmptyCallback()));
	//QObject::connect(ui.pushButton_emptyCallback, SIGNAL(clicked()), this, SLOT(buttonClicked_EmptyCallback()));

}