#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

#include <QFile>
#include <QFileDialog>
#include <QMessageBox>
#include <QDebug>
#include <QtXml/QDomDocument>

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

public slots:
    void OpenLoadConfigDialog();
    void OpenSaveConfigDialog();

    void selectXMLItem();
    void applyValue();

    void search(QString);
    void readConfigFile();

    void customView();

private:
    Ui::MainWindow *ui;

    QFile configFile;
    QFile descriptionFile;
    QString configFilePath = "data/config.xml";
    QString descriptionFilePath = "data/descriptions.xml";
    QString customViewFolder = "data/customViews/";

    QString customViewComparison = "";
    QString searchComparison = "";

    QDomDocument doc;
    QDomDocument docDescription;

    //void readConfigFile(QString searchText);
    QDomElement searchDocElement (QString);
    QDomElement findDescription (QString);

    QString xmlParser(QString str, QString mode);

    void readCostumViews();

};
#endif // MAINWINDOW_H
