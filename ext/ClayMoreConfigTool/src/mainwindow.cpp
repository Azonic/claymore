#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <iostream>

MainWindow::MainWindow(QWidget* parent)
	: QMainWindow(parent)
	, ui(new Ui::MainWindow)
{
	ui->setupUi(this);
	ui->action_loadconfig->setShortcut(tr("CTRL+O"));
	connect(ui->action_loadconfig, SIGNAL(triggered()), this, SLOT(OpenLoadConfigDialog()));
	ui->action_saveconfig->setShortcut(tr("CTRL+S"));
	connect(ui->action_saveconfig, SIGNAL(triggered()), this, SLOT(OpenSaveConfigDialog()));

	configFile.setFileName(configFilePath);
	descriptionFile.setFileName(descriptionFilePath);
	readConfigFile();

	connect(ui->configItemList, SIGNAL(itemClicked(QListWidgetItem*)), this, SLOT(selectXMLItem()));
	connect(ui->configItemList, SIGNAL(itemSelectionChanged()), this, SLOT(selectXMLItem()));

	connect(ui->search, SIGNAL(textChanged(const QString&)), this, SLOT(search(const QString&)));

	ui->currentItemValue->setEnabled(false);	//Edit Sektion ausgrauen
	ui->applyItemValue->setEnabled(false);

	connect(ui->applyItemValue, SIGNAL(clicked()), this, SLOT(applyValue()));

	connect(ui->customViewMenu, SIGNAL(activated(int)), this, SLOT(customView()));

	readCostumViews();
	std::cout << "Console of ClayMore's ConfigTool" << std::endl;
}

MainWindow::~MainWindow()
{
	delete ui;
}

void MainWindow::OpenLoadConfigDialog()
{
	/*LoadConfigDialog loadConfigDialog;
	loadConfigDialog.setModal(true);
	loadConfigDialog.exec();
	*/
	QString fileName = QFileDialog::getOpenFileName(this, tr("Open File"), "", tr("ConfigFile (*.xml)"));
	if (fileName.isEmpty())
		return;
	else {
		QFile::remove(configFilePath);
		QFile::copy(fileName, configFilePath);
		readConfigFile();
	}
}

void MainWindow::OpenSaveConfigDialog()
{
	//SaveConfigDialog saveConfigDialog;
	//saveConfigDialog.setModal(true);
	//saveConfigDialog.exec();

	QString fileName = QFileDialog::getSaveFileName(this,
		tr("Save File"), "",
		tr("ConfigFile (*.xml)"));

	if (fileName.isEmpty())
		return;
	else {
		if (QFile::exists(fileName))
		{
			QFile::remove(fileName);
		}
		QFile::copy(configFilePath, fileName);
	}
}

void MainWindow::readConfigFile() {

	ui->configItemList->clear();

	if (configFile.open(QIODevice::ReadOnly)) {
		doc.setContent(&configFile);
		configFile.close();
	}
	else
	{
		configFile.close();
		std::cout << "Can't open ConfigFile (from readConfigFile())" << std::endl;
		return;
	}

	QDomElement docElem = doc.documentElement();
	QDomNode configNode = docElem.firstChild();
	QDomNode startupConfigNode = configNode.firstChild();

	int i = 0;
	while (!startupConfigNode.isNull()) {
		QDomElement domElement = startupConfigNode.toElement(); // try to convert the node to an element.
		if (!domElement.isNull()) {
			//Der liste hinzufügen
			if (searchComparison != "" && domElement.tagName().toStdString().find(xmlParser(searchComparison, "useable").toStdString()) > 100)
			{

			}
			else if (customViewComparison != "" && customViewComparison.toStdString().find(domElement.tagName().toStdString()) > 100)
			{

			}
			else {
				ui->configItemList->addItem(xmlParser(domElement.tagName(), "readable"));
			}
			//ui.line_CurrentValue->text() = domElement.tagName();

		}
		i++;
		startupConfigNode = startupConfigNode.nextSibling();
	}

	ui->currentItemLabel->clear();
	ui->currentItemValue->setText("");
	ui->currentItemValue->setEnabled(false);
	ui->applyItemValue->setEnabled(false);

}

void MainWindow::selectXMLItem()
{//temporary variable to get the selected docElement
	QDomElement selectedElement;
	selectedElement = searchDocElement(xmlParser(ui->configItemList->currentItem()->text(), "useable"));

	QDomElement selectedElementDescription;
	selectedElementDescription = findDescription(xmlParser(ui->configItemList->currentItem()->text(), "useable"));
	ui->InfoText->setText(selectedElementDescription.firstChild().toText().substringData(0, 255));

	//setting currentValue and selectedItem
	ui->currentItemLabel->setText(xmlParser(selectedElement.tagName(), "readable"));
	ui->currentItemValue->setText(selectedElement.firstChild().toText().substringData(0, 14));

	//Enabling the edit line
	ui->currentItemValue->setEnabled(true);	//Edit Sektion ausgrauen
	ui->applyItemValue->setEnabled(true);
	ui->InfoText->setEnabled(true);
}

QDomElement MainWindow::searchDocElement(QString tagname)
{
	//-----------------same procedure as readFile()-----------------------------------
	//Following example code is from http://doc.qt.io/archives/qt-4.8/qdomdocument.html
	QDomElement docElem = doc.documentElement();
	QDomNode configNode = docElem.firstChild();
	QDomNode startupConfigNode = configNode.firstChild();
	QDomElement foundedDocElement;


	while (!startupConfigNode.isNull()) {
		QDomElement e = startupConfigNode.toElement();
		if (!e.isNull()) {
			//if the tagname match write into the return variable
			if (e.tagName() == tagname) {
				foundedDocElement = e;
			}
		}

		startupConfigNode = startupConfigNode.nextSibling();
	}

	//-------------------------------------------------------------------------------

	return foundedDocElement;
}

QDomElement MainWindow::findDescription(QString tagname) {

	if (descriptionFile.open(QIODevice::ReadOnly)) {
		docDescription.setContent(&descriptionFile);
		descriptionFile.close();
	}
	else {
		descriptionFile.close();
		std::cout << "Can't open ConfigFile" << std::endl;
	}

	QDomElement docElem = docDescription.documentElement();
	QDomNode configNode = docElem.firstChild();
	QDomNode startupConfigNode = configNode.firstChild();
	QDomElement foundedDocElement;

	while (!startupConfigNode.isNull()) {
		QDomElement e = startupConfigNode.toElement();
		if (!e.isNull()) {
			//if the tagname match write into the return variable
			if (e.tagName() == tagname) {
				foundedDocElement = e;
			}
		}
		startupConfigNode = startupConfigNode.nextSibling();
	}
	//-------------------------------------------------------------------------------

	return foundedDocElement;
}

void MainWindow::applyValue()
{
	//Get element in question
	QString nameSelectedItem = xmlParser(ui->currentItemLabel->text(), "useable");
	QDomElement elementToReplace = searchDocElement(nameSelectedItem);
	QString newValue = ui->currentItemValue->text();

	elementToReplace.firstChild().setNodeValue(newValue);

	// Write changes to same file
	configFile.open(QIODevice::ReadWrite | QIODevice::Text);
	QTextStream stream;
	stream.setDevice(&configFile);
	doc.save(stream, QDomNode::EncodingFromDocument);

	configFile.close();
	ui->applyItemValue->setDisabled(true);
}

void MainWindow::search(QString text) {
	//std::cout << text.toStdString() << std::endl;
	searchComparison = text;

	readConfigFile();
}

void MainWindow::customView() {

	QString text = ui->customViewMenu->currentText();

	if (text == "Default") {
		customViewComparison = "";
		readConfigFile();
		return;
	}

	QString filename = customViewFolder + text + ".txt";
	//std::cout << filename.toStdString() << std::endl;

	QFile file(filename);
	if (!file.open(QIODevice::ReadOnly | QIODevice::Text))
		return;

	QTextStream in(&file);
	while (!in.atEnd()) {
		QString line = in.readLine();
		customViewComparison = customViewComparison + line;
	}

	//std::cout << customViewComparison.toStdString() << std::endl;

	readConfigFile();
}

QString MainWindow::xmlParser(QString str, QString mode) {

	if (mode == "readable")
	{
		str = str.replace("_", " ");
		str[0] = str[0].toUpper();

		for (int i = 1; i < str.length(); i++) {

			if (str[i - 1] == " ") {
				str[i] = str[i].toUpper();
			}
		}
	}
	else if (mode == "useable") {
		str = str.toLower();
		str = str.replace(" ", "_");
	}

	return str;
}

void MainWindow::readCostumViews() {

	QDir folder = QDir(customViewFolder);
	QStringList files = folder.entryList(QDir::Files);

	for (const auto& i : files)
	{
		QString temp = i;
		ui->customViewMenu->addItem(temp.replace(".txt", ""));
	}

}
